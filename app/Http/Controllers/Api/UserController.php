<?php 
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;// Employee
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;// EmployeeResource
use Illuminate\Support\Facades\Validator;

class UserController extends Controller{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(){
		$users = User::all();// Employee
		// return response([
			// 'users' => UserResource::collection($users),
			// 'message' => 'Successful'
		// ], 200);
		
		return response(UserResource::collection($users), 200);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request){
		$data = $request->all();

		$validator = Validator::make($data, [
			'name' => 'required|max:50',
			'age' => 'required|max:50',
			'job' => 'required|max:50',
			'salary' => 'required|50'
		]);

		if($validator->fails()){
			return response([
				'error' => $validator->errors(),
				'Validation Error'
			]);
		}

		$user = User::create($data);// Employee

		return response([
			'user' => new UserResource($user),
			'message' => 'Success'
		], 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Employee  $employee
	 * @return \Illuminate\Http\Response
	 */
	public function show(User $user){
		return response([
			'user' => new UserResource($user),
			'message' => 'Success'
		], 200);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Employee  $employee
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, User $user){
		$user->update($request->all());

		return response([ 
			'user' => new UserResource($user), 
			'message' => 'Success']
		, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Employee  $employee
	 * @return \Illuminate\Http\Response
	 */
	public function remove(User $user){// destroy
		$user->delete();
		return response(['message' => 'User deleted']);
	}
}
