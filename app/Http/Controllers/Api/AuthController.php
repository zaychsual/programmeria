<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\Passport;
use App\Models\User;
use Carbon\Carbon;
use Hash;

class AuthController extends Controller{
	public function register(Request $request){
		$data = $request->validate([
			'name' => 'required|max:255',
			'email' => 'required|email|unique:users',
			'username' => 'required',
			'password' => 'required',
			'c_password' => 'required|same:password',
			'role_id' => 'required',
		]);

		$data['password'] = Hash::make($request->password);

		$user = User::create($data);

		$token = $user->createToken('API Token')->accessToken;

		return response(['user' => $user, 'token' => $token]);
	}

	public function login(Request $request){
		$data = $request->validate([
			'email' => 'email|required',
			'password' => 'required|min:'.$request->passwordMinLength
		]);

		if(!auth()->attempt($data)){
			return response(['error' => 'Incorrect Details. Please try again']);// error_message
		}

		if(!$request->remember){ // remember_me
			Passport::personalAccessTokensExpireIn(Carbon::now()->addMinutes(2));// addDays | 
		}

		$token = auth()->user()->createToken('API Token')->accessToken;

		return response(['user' => auth()->user(), 'token' => $token]);
		// ->cookie(
			// 'token', $token, 5000, "", null, true, true
		// );
	}
	
	/* public function isLogin(Request $request){
		$token = functionCookie('token');
		// $id = functionCookie('userId');
		if($token && $id){
			$user = functionUserByToken($id);
			if($user){
				return response->user();
			}
		}
	} */

	public function logout(Request $request){
		// $logout = $request->user()->token()->revoke();
		// if($logout){
			// return response()->json([
				// 'message' => 'Successfully logged out'
			// ]);
		// }
		
		$logout = $request->user()->token()->revoke();
		return response()->json($logout ? true : false);
	}
}
