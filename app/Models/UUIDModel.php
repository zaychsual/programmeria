<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid as Generator;
use Carbon\Carbon;

class UUIDModel extends Model
{
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $user              = Auth::user();
            $model->id         = strtoupper(Generator::uuid4()->toString());
            $model->created_by = $user->id ?? '8924c6fa-3065-4541-8609-8bc1b86a1349';
            $model->created_at = Carbon::now();
            $model->updated_at = Carbon::now();
        });
        static::updating(function ($model) {
            $user              = Auth::user();
            $model->updated_by = $user->id ?? '8924c6fa-3065-4541-8609-8bc1b86a1349';
            $model->updated_at = Carbon::now();
        });
    }
}
