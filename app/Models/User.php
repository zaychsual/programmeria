<?php
namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid as Generator;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable{
	use HasApiTokens, SoftDeletes, HasFactory, Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'username',
		'email',
		'username',
		'password',
		'updated_by',
		'deleted_at'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token'
	];

	public function getIncrementing(){
		return false;
	}

	public function getKeyType(){
		return 'string';
	}

	public static function boot(){
		parent::boot();
		static::creating(function ($model) {
				$model->id = strtoupper(Generator::uuid4()->toString());
		});
		/* static::updating(function ($model) {
				$user              = Auth::user();
				$model->updated_by = $user->id ?? '9999';
		});
		static::deleting(function ($model) {
				$user              = Auth::user();
				$model->updated_by = $user->id ?? '9999';
		}); */
	}

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];
}
