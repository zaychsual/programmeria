<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends BaseModel{
	use SoftDeletes, HasFactory;

	protected $fillable = [
		'id',
		'name',
		'age',
		'job',
		'salary',
		'created_by',
		'updated_by',
		'created_at',
		'updated_at'
	];
}
