<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends UUIDModel
{
    use HasFactory, SoftDeletes;

    public $incrementing = false;
    protected $keyType = 'string';

    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = "id";

    protected $fillable = [
        'name',
        'level_menu',
        'master_menu',
        'url',
        'icon',
        'is_active',
        'no_urut',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at',
        'menu_aktif',
    ];
}
