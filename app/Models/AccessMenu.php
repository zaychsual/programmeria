<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccessMenu extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'id',
        'role_id',
        'menu_id',
        'akses',
        'add',
        'edit',
        'destroy',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
