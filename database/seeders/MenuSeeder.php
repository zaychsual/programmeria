<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid as Generator;
use Illuminate\Support\Facades\Schema;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Menu::query()->truncate();
        Schema::enableForeignKeyConstraints();

        $data = [
            ['id' => '0adc7891-2854-4107-a9d0-77529f140bd6'	,'name' =>	'Laporan Per Wilayah'	,'level_menu' =>	'sub_menu'	,'master_menu' =>	'58f8700f-dad2-4145-b724-6a9b37e994ba'	,'url' =>	'report-wilayah'	,'icon' =>	null	,'is_active' => 1	,'no_urut' =>	2	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'report-wilayah'	,'created_at' => Carbon::now()],
            ['id' => '140e30c3-b6a3-43c3-b653-292db0441479'	,'name' =>	'Laporan Per Kategori'	,'level_menu' =>	'sub_menu'	,'master_menu' =>	'58f8700f-dad2-4145-b724-6a9b37e994ba'	,'url' =>	'report-kategori'	,'icon' =>	null	,'is_active' => 1	,'no_urut' =>	3	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'report-kategori'	,'created_at' => Carbon::now()],
            ['id' => '3bdf758c-dfe4-41fa-8f5a-726f2d9bee26'	,'name' =>	'Laporan Komplain'	,'level_menu' =>	'sub_menu'	,'master_menu' =>	'58f8700f-dad2-4145-b724-6a9b37e994ba'	,'url' =>	'report-komplain'	,'icon' =>	null	,'is_active' => 1	,'no_urut' =>	1	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'report-komplain'	,'created_at' => Carbon::now()],
            ['id' => '3f3128f3-43ab-49f1-aee2-dca3af000102'	,'name' =>	'User'	,'level_menu' =>	'sub_menu'	,'master_menu' =>	'5a55b825-0d1f-498c-b51a-a1f118e4b86b'	,'url' =>	'user.index'	,'icon' =>	null	,'is_active' => 1	,'no_urut' =>	7	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'user'	,'created_at' => Carbon::now()],
            ['id' => '58f8700f-dad2-4145-b724-6a9b37e994ba'	,'name' =>	'Report'	,'level_menu' =>	'main_menu'	,'master_menu' =>	null	,'url' =>	null	,'icon' =>	'fas fa-file'	,'is_active' => 1	,'no_urut' =>	3	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	null	,'created_at' => Carbon::now()],
            ['id' => '5a55b825-0d1f-498c-b51a-a1f118e4b86b'	,'name' =>	'Konfigurasi'	,'level_menu' =>	'main_menu'	,'master_menu' =>	null	,'url' =>	null	,'icon' =>	'fas fa-cogs'	,'is_active' => 1	,'no_urut' =>	4	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	null	,'created_at' => Carbon::now()],
            ['id' => '729e8fc7-c88b-459c-a948-c5f8b8f5a86c'	,'name' =>	'Dashboard'	,'level_menu' =>	'main_menu'	,'master_menu' =>	null	,'url' =>	'dashboard'	,'icon' =>	'fas fa-pencil-ruler'	,'is_active' => 1	,'no_urut' =>	1	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'dashboard'	,'created_at' => Carbon::now()],
            ['id' => '904cf889-deb7-444e-8d7e-092f1d5daa8a'	,'name' =>	'Branch'	,'level_menu' =>	'sub_menu'	,'master_menu' =>	'5a55b825-0d1f-498c-b51a-a1f118e4b86b'	,'url' =>	'branch.index'	,'icon' =>	null	,'is_active' => 1	,'no_urut' =>	3	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'branch'	,'created_at' => Carbon::now()],
            ['id' => '93f19321-7461-477c-b27e-a1606e67ccba'	,'name' =>	'Variable Complain'	,'level_menu' =>	'sub_menu'	,'master_menu' =>	'5a55b825-0d1f-498c-b51a-a1f118e4b86b'	,'url' =>	'complain.index'	,'icon' =>	null	,'is_active' => 1	,'no_urut' =>	4	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'complain'	,'created_at' => Carbon::now()],
            ['id' => '9496ee48-f148-4b45-be9d-2751b1f29757'	,'name' =>	'Menu'	,'level_menu' =>	'sub_menu'	,'master_menu' =>	'5a55b825-0d1f-498c-b51a-a1f118e4b86b'	,'url' =>	'menu.index'	,'icon' =>	null	,'is_active' => 1	,'no_urut' =>	5	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'menu'	,'created_at' => Carbon::now()],
            ['id' => 'a6e40bfb-0eb2-4919-bc64-67d90559cf38'	,'name' =>	'Tiket'	,'level_menu' =>	'main_menu'	,'master_menu' =>	null	,'url' =>	'ticket.index'	,'icon' =>	'fas fa-ticket-alt'	,'is_active' => 1	,'no_urut' =>	2	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'ticket'	,'created_at' => Carbon::now()],
            ['id' => 'b189682c-9094-458f-939b-381cb512ab20'	,'name' =>	'Region'	,'level_menu' =>	'sub_menu'	,'master_menu' =>	'5a55b825-0d1f-498c-b51a-a1f118e4b86b'	,'url' =>	'region.index'	,'icon' =>	null	,'is_active' => 1	,'no_urut' =>	2	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'region'	,'created_at' => Carbon::now()],
            ['id' => 'bd016a09-6667-40af-b2d8-d12b84cbf6d8'	,'name' =>	'Setting'	,'level_menu' =>	'sub_menu'	,'master_menu' =>	'5a55b825-0d1f-498c-b51a-a1f118e4b86b'	,'url' =>	'setting.index'	,'icon' =>	null	,'is_active' => 1	,'no_urut' =>	8	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'setting'	,'created_at' => Carbon::now()],
            ['id' => 'ce8764f0-1d42-4085-8d3f-6bf995f121a6'	,'name' =>	'Roles'	,'level_menu' =>	'sub_menu'	,'master_menu' =>	'5a55b825-0d1f-498c-b51a-a1f118e4b86b'	,'url' =>	'role.index'	,'icon' =>	null	,'is_active' => 1	,'no_urut' =>	6	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'role'	,'created_at' => Carbon::now()],
            ['id' => 'e59b674e-d007-45c7-b8cc-c3a0473c8ea3'	,'name' =>	'Customer'	,'level_menu' =>	'sub_menu'	,'master_menu' =>	'5a55b825-0d1f-498c-b51a-a1f118e4b86b'	,'url' =>	'cust.index'	,'icon' =>	null	,'is_active' => 1	,'no_urut' =>	1	,'created_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'menu_aktif' =>	'cust'	,'created_at' => Carbon::now()],
        ];

        Menu::insert($data);
    }
}
