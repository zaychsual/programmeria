<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid as Generator;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->truncate();
        $users = [
            [
                'id' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166',
                'name' => strtoupper('Admin'),
                'email' => 'admin@admin.com',
                'username' => strtolower('admin'),
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('password'),
                'remember_token' => Str::random(10),
                'role_id' => 1,
                'updated_by' => null,
                'created_at' => Carbon::now(),
                'deleted_at' => null,

            ],
            [
                'id' => '8924c6fa-3065-4541-8609-8bc1b86a1349',
                'name' => strtoupper('Admin'),
                'email' => 'husein@faker.com',
                'username' => strtolower('husein'),
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('password'),
                'remember_token' => Str::random(10),
                'role_id' => 1,
                'updated_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
            ],
        ];
        User::insert($users);
    }
}
