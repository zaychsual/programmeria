<?php

namespace Database\Seeders;

use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Role::query()->truncate();
        Schema::enableForeignKeyConstraints();

        if(env('DB_CONNECTION') == 'pgsql') {
            DB::statement('TRUNCATE TABLE roles RESTART IDENTITY;');
        } elseif (env('DB_CONNECTION') == 'mysql') {
            DB::statement('ALTER TABLE access_menus AUTO_INCREMENT = 1;');
        }

        $roles = [
            [
                'name' => 'Admin',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'User',
                'created_at' => Carbon::now(),
            ],
        ];
        Role::insert($roles);
    }
}
