const mix = require('laravel-mix');

// const IS_ADMIN = false; // true | false

if (mix.inProduction()) {
  mix.webpackConfig({
    output: {
      chunkFilename: 'public/js/chunks/[name].[chunkhash].chunk.js', // 'js/chunk/[name].[chunkhash].chunk.js'
      publicPath: ''
    }
  });
} else {
  mix.webpackConfig({
    output: {
      chunkFilename: 'public/js/chunks/[name].chunk.js', // 'js/chunk/[name].chunk.js'
      publicPath: ''
    }
  });
}

// if(IS_ADMIN){
  // ====== Admin App ======
  mix.js('resources/js/Admin.js', 'public/js/admin').react().sourceMaps(false,'source-map')
      .sass('resources/scss/app.scss', 'public/css') // Global Style App
      .sass('resources/scss/admin/admin.scss', 'public/css/admin') // Admin App
      // .sass('resources/scss/front.scss', 'public/css') // Front App (OPTION: USAGE in Admin App, NOTE: separate if too much)
      .sass('resources/scss/preload/preload.scss', 'public/css/preload'); // preload (NOT IMPORTANT / USE LATER)
// }
// else{
//   // ====== Front App ======
//   mix.js('resources/js/App.js', 'public/js').react() // .sourceMaps(false,'source-map')
//       .sass('resources/scss/app.scss', 'public/css') // Front App
//       .sass('resources/scss/admin-ui/admin-ui.scss', 'public/css/admin') // Admin App
//       .sass('resources/scss/preload/preload.scss', 'public/css/preload'); // preload (NOT IMPORTANT / USE LATER)
//       // .reactCSSModules();
// }

