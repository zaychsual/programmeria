<?php 
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\page\PhpinfoController;

/*-------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great! */

// Route::get('/',function(){
  // return view('welcome');
// });

// Admin App (NOT FIX: check again)
Route::prefix('admin')->group(function(){
  Route::get('{uri?}',function(){
    // $agent = new Agent();

    // if($agent->isRobot()){
      // abort(404);
    // }
    
    $content = view('admin');
    return response($content)->withHeaders([
      'Accept-Ranges' => 'bytes',
      'Access-Control-Allow-Origin' => '*',
      'X-Frame-Options' => 'SAMEORIGIN',
      // 'Vary' => 'Accept-Encoding',
      'X-Robots-Tag' => 'none,noarchive',
      'X-Powered-By' => 'Programmeria', // REQUIRE
      'X-XSS-Protection' => '1; mode=block'
      // 'Feature-Policy' => "display-capture 'self'"
    ]);

    // return view('admin');

    // return response($content)->header('Access-Control-Allow-Origin', '*');
  })->where('uri', '(.*)');
});

// For super admin
Route::get('/phpinfo', [PhpinfoController::class, 'index']);

// App Pages For User
Route::get('/{uri?}',function(){
	/** 
		@NOTES: Get App Setting For SEO Here 
		
	**/
	
	// NOTES: Check User Agent For SEO
  // $agent = new Agent();

  /* if($agent->isRobot()){
    // Facebook Bot:
    // facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)
    // facebookexternalhit/1.1
    // Facebot
    $r = $agent->robot();
    // $fb = $r === "Facebookexternalhit" || $r === "Facebot";
    return "BOT: " . $r;
  } */
  
  return view('welcome');
})->where('uri', '(.*)');

