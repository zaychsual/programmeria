<?php 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\EmployeeController;

use App\Http\Controllers\Api\UserController;

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::group(['middleware' => 'auth:api'], function(){
	Route::apiResource('/employee', EmployeeController::class);
	Route::apiResource('/users', UserController::class);// NOT WORK token Bearer ???
	Route::post('/logout', [AuthController::class, 'logout']);
	
});

// Husein Add
// Route::apiResource('/users', UserController::class);