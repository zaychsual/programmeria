const mix = require('laravel-mix');

/*--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files */

if(mix.inProduction()){
  mix.webpackConfig({
    output: {
      chunkFilename: 'js/chunks/[name]_[chunkhash].js', // 'js/chunks/[name].[chunkhash].chunk.js' | 'js/chunk/[name].[chunkhash].chunk.js'
      // publicPath: ''
    }
  });
}else{
  mix.webpackConfig({
    output: {
      chunkFilename: 'js/chunks/dev/[name]_[chunkhash].js', // 'js/chunk/[name].chunk.js'
    }
  });
}

mix.js('resources/js/Admin.js', 'public/js/admin').react().sourceMaps(false,'source-map') // Admin App
   .js('resources/js/App.js', 'public/js').react().sourceMaps(false,'source-map') // Front App
   .sass('resources/scss/app.scss', 'public/css') // Global Style App
   .sass('resources/scss/admin-ui/admin-ui.scss', 'public/css/admin') // Admin App
   .sass('resources/scss/front.scss', 'public/css') // Front App (OPTION: USAGE in Admin App, NOTE: separate if too much)
   .sass('resources/scss/preload/preload.scss', 'public/css/preload') // preload (NOT IMPORTANT / USE LATER)
   .version();

// mix.js('resources/js/app.js', 'public/js')
  // .postCss('resources/css/app.css', 'public/css', [
  //   //
  // ]);