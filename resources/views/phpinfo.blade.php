<?php header("X-Frame-Options: SAMEORIGIN");
header("X-Robots-Tag: none,noarchive");
header("X-XSS-Protection: 1; mode=block");?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="none,nosnippet,noarchive,noimageindex">
<meta name="googlebot" content="none,nosnippet,noarchive,noimageindex">
<meta name="AdsBot-Google" content="none,nosnippet,noarchive,noimageindex">
<meta name="googlebot-news" content="none,nosnippet,noarchive,noimageindex">
<meta name="bing" content="none,nosnippet,noarchive,noimageindex">
<meta name="baidu" content="none,nosnippet,noarchive,noimageindex">
<meta name="description" content="Programmeria admin">
<meta name="author" content="Programmeria">
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta name="mobile-web-app-capable" content="yes">
<meta name="msapplication-TileColor" content="#e3f2fd" />
<meta name="theme-color" content="#e3f2fd">
<link rel="shortcut icon" type="image/x-icon" href="/logo/favicon.ico">
<title>Php info</title>
</head>
<body data-nosnippet="true"><?php phpinfo();?></body></html>