import React from 'react';

// const loadingStyles = {
  // display: 'flex',
  // height: '100%',
  // width: '100%',
  // justifyContent: 'center',
  // alignItems: 'center'
// };

// <div style={loadingStyles}>{content}</div>;
export default function Loading({ content, className, style }){
  return <div className={Q.Cx('d-flex justify-content-center align-items-center w-100 h-100 monacoLoader', className)} style={style}>{content}</div>;
}

// export default Loading;
