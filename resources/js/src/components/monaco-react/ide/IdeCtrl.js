import React from 'react';
// import Btn from '../../components/q-ui-bootstrap/Btn';
import ControlledEditor from '../ControlledEditor/ControlledEditor';

/* const IDE_OPTIONS = {
	fontSize: 14,
	tabSize: 2, // tabSize
	// dragAndDrop: false,
	minimap: {
		enabled: false, // Defaults = true
		// side: minimapSide, // Default = right | left
		// showSlider: 'always', // Default = mouseover
		// renderCharacters: false, // Default =  true
	},
	// colorDecorators: false, // Css color preview
	// readOnly: true,
	// mouseWheelZoom: zoomIde
}; */

export default function IdeCtrl({value, required, onMount, onChange, ...etc}){
	const [val, setVal] = React.useState(value);
	const [ready, setReady] = React.useState(false);
  
	// const ideRef = React.useRef(null);
	
	// React.useEffect(() => {
		// console.log('%cuseEffect in IdeCtrl','color:yellow;');
	// }, []);

/** CREATE component / method */
	const ideMount = (v, editor) => {
    if(!ready){
      setReady(true);
      // ideRef.current = editor;// editor | v
      
      if(editor){ // this.ideMain.current
				// OPTION DEV: set attribute required in monaco textarea
				if(required){
					const ta = domQ("textarea", editor.getDomNode());
					if(ta) setAttr(ta, {required:''});
				}
			
				// Get bind editor contextmenu FROM https://github.com/Microsoft/monaco-editor/issues/484
				editor.onContextMenu(e => {
					let ctxMenu = domQ(".monaco-menu-container", editor.getDomNode());// this.ideMain.current
					if(ctxMenu){
						// window.outerHeight
            let ee = e.event,
                ch = ctxMenu.clientHeight,
                cw = ctxMenu.clientWidth,
                cs = ctxMenu.style;
						const posY = (ee.posy + ch) > window.innerHeight ? ee.posy - ch : ee.posy;
						// window.outerWidth
						const posX = (ee.posx + cw) > document.body.clientWidth ? ee.posx - cw : ee.posx;
				
						cs.position = "fixed";// OPTIONS: set in css internal
						cs.top = Math.max(0, Math.floor(posY)) + "px";
						cs.left = Math.max(0, Math.floor(posX)) + "px";
					}
				});
				
				onMount(v, editor);
      }
    }
	}	
	
	return (
		<ControlledEditor 
			{...etc} 
			value={val} 
			onChange={(e, v) => {
				setVal(v);
				onChange(e, v);
			}} 
			editorDidMount={ideMount} 
			// options={{
				// fontSize: 14, // fontSize
				// tabSize: 2, // tabSize
				// minimap: {
					// enabled: false
				// }
			// }}
		/>
	);
}

IdeCtrl.defaultProps = {
	onMount: noop,
	onChange: noop,
};

/*
<React.Fragment></React.Fragment>
*/
