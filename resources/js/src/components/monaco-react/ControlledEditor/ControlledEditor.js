import React, { useRef, useEffect, useCallback } from 'react';
// import P from 'prop-types';

import Editor from '..';
// import { noop } from '../utils';

function ControlledEditor({ 
	value: providedValue, 
	onChange, 
	editorDidMount, 
	...etc
}){
  const editor = useRef(null);
  const listener = useRef(null);
  const value = useRef(providedValue);

  // to avoid unnecessary updates in `onEditorModelChange`
  // (that depends on the `current value` and will trigger to update `attachChangeEventListener`,
  // thus, the listener will be disposed and attached again for every value change)
  // the current value is stored in ref (useRef) instead of being a dependency of `onEditorModelChange`
  value.current = providedValue;

  const onEditorModelChange = useCallback(e => {
    const editorValue = editor.current.getValue();
		
    if (value.current !== editorValue) {
      const directChange = onChange(e, editorValue);

      if (typeof directChange === 'string' && editorValue !== directChange) {
        editor.current.setValue(directChange);
      }
    }
  }, [onChange]);

  const attachChangeEventListener = useCallback(() => {
    listener.current = editor.current?.onDidChangeModelContent(onEditorModelChange);
  }, [onEditorModelChange]);

  useEffect(() => {
    attachChangeEventListener();
    return () => listener.current?.dispose();
  }, [attachChangeEventListener]);

  const onEditorDidMount = useCallback((getValue, _editor) => {
    editor.current = _editor;
    attachChangeEventListener();

    editorDidMount(getValue, _editor);
  }, [attachChangeEventListener, editorDidMount]);

  return (
    <Editor
      value={providedValue}
      editorDidMount={onEditorDidMount}
      _isControlledMode={true}
      {...etc}
    />
  );
}

ControlledEditor.defaultProps = {
  editorDidMount: Q.noop,
  onChange: Q.noop
};

export default ControlledEditor;

// ControlledEditor.propTypes = {
  // value: P.string,
  // editorDidMount: P.func,
  // onChange: P.func
// };
