// The source (has been changed) is https://github.com/facebook/react/issues/5465#issuecomment-157888325

/* const CANCEL_MESSAGE = 'operation is manually canceled';// CANCELATION_MESSAGE

const makeCancelable = promise => {
  let hasCanceled_ = false;

  const wrapPromise = new Promise((resolve, reject) => {
    promise.then(val => hasCanceled_ ? reject(CANCEL_MESSAGE) : resolve(val));
    promise.catch(err => reject(err));
  });

  return (wrapPromise.cancel = _ => (hasCanceled_ = true), wrapPromise);
};

export default makeCancelable; */

const CANCELATION_MESSAGE = {
  type: 'cancelation',
  msg: 'operation is manually canceled'
};

const makeCancelable = promise => {
  let hasCanceled_ = false;

  const wrappedPromise = new Promise((resolve, reject) => {
    promise.then(val => hasCanceled_ ? reject(CANCELATION_MESSAGE) : resolve(val));
    promise.catch(reject);
  });

  return (wrappedPromise.cancel = () => (hasCanceled_ = true), wrappedPromise);
};

export default makeCancelable;
