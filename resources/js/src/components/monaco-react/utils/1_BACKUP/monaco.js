import config from '../config';
import { deepMerge, makeCancelable } from '../utils';

class Monaco{
  constructor(config = {}){
    this._config = config;
  }

  config(config){
    if(config){
      this._config = deepMerge(this._config, config);
			// 3.3.0
			// this._config = deepMerge(this._config, this.validateConfig(config));
    }
    return this;
  }
	
// 3.3.0
  // validateConfig(config){
    // if(config.urls){
      // this.informAboutDepreciation();
      // return { paths: { vs: config.urls.monacoBase } };
    // }
    // return config;
  // }
	
// 3.3.0
  // informAboutDepreciation(){
    // console.warn(`Deprecation warning!
      // You are using deprecated way of configuration.

      // Instead of using
        // monaco.config({ urls: { monacoBase: '...' } })
      // use
        // monaco.config({ paths: { vs: '...' } })

      // For more please check the link https://github.com/suren-atoyan/monaco-react#config`);
  // }

  injectScripts(script){
		setTimeout(() => {// Q-CUSTOM
			document.body.appendChild(script);
			// Q-CUSTOM
			// if(window.require) delete window.require;
			// if(window.exports) delete window.exports;
			// if(window.module) delete window.module;
		}, 1000);
  }

  handleMainScriptLoad = _ => {
    document.removeEventListener('monaco_init', this.handleMainScriptLoad);
    this.resolve(window.monaco);
  }

  createScript(src){
    const s = document.createElement('script');
		s.async = 1;// Q-CUSTOM
		// if(!src) s.id = 'requireMonaco';// Q-CUSTOM
			
    return (src && (s.src = src), s);
  }

  createMonacoLoaderScript(mainScript){
    // const loaderScript = this.createScript(this._config.urls.monacoLoader);
		// 3.3.0
		const loaderScript = this.createScript(this._config.paths.vs + '/loader.js');
		
    loaderScript.onload = _ => this.injectScripts(mainScript);
    loaderScript.onerror = this.reject;

    return loaderScript;
  }

  createMainScript(){
		const s = this.createScript();
	// mainScript.innerHTML
		// s.text = `require.config({paths:{'vs':'${this._config.urls.monacoBase}'}});
// require(['vs/editor/editor.main'],function(){
	// document.dispatchEvent(new Event('monaco_init'));
// });`;

// 3.3.0
    s.text = `require.config({paths:{'vs':'${this._config.paths.vs}'}});
require(['vs/editor/editor.main'],function(){
	document.dispatchEvent(new Event('monaco_init'));
});`;

		s.onerror = this.reject;
		return s;
  }

  isInitialized = false;

  wrapPromise = new Promise((res, rej) => {
    this.resolve = res;
    this.reject = rej;
  });

  init(){
    if(!this.isInitialized){
      if(window.monaco && window.monaco.editor){
        return new Promise((res, rej) => res(window.monaco));
      }

      document.addEventListener('monaco_init', this.handleMainScriptLoad);

      const mainScript = this.createMainScript();
			const loaderScript = this.createMonacoLoaderScript(mainScript);
			
			this.injectScripts(loaderScript);
    }
		
		// Q-CUSTOM: For more inject / get script
		// console.log('%cafter if: ',LOG_DEV, window.monaco);
		if(window.require) delete window.require;
		if(window.exports) delete window.exports;
		if(window.module) delete window.module;
		
		// if(window.monaco && window.define){
			// window.define = null;
		// }

    this.isInitialized = true;
		
    return makeCancelable(this.wrapPromise);
  }
}

export default new Monaco(config);
