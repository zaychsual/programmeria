// import noop from './noop';
import compose from './compose';
import deepMerge from './deepMerge';
import makeCancelable from './makeCancelable';

import monaco from './monaco';

export { compose, deepMerge, makeCancelable, monaco };// noop, 