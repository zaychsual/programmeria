import React, { useState, useRef, useCallback, useEffect } from 'react';// {useState, useRef, useCallback, useEffect}
// import P from 'prop-types';

import MonacoContainer from '../MonacoContainer';

import { monaco } from '../utils';// noop
import { useMount, useUpdate } from '../utils/hooks';

import themes from '../config/themes';

const DiffEditor =
  ({
    original,
    modified,
    language,
    originalLanguage,
    modifiedLanguage,
    editorDidMount,
    theme,
    width,
    height,
    loading,
    options,
    className,
    // wrapperClassName,
  }) =>
{
  const [isEditorReady, setIsEditorReady] = useState(false);
  const [isMonacoMounting, setIsMonacoMounting] = useState(true);
  const editorRef = useRef();
  const monacoRef = useRef();
  const containerRef = useRef();

  useMount(() => {
    const cancelable = monaco.init();

    cancelable
      .then(monaco => ((monacoRef.current = monaco) && setIsMonacoMounting(false)))
      .catch(error => error?.type !== 'cancelation' &&
        console.error('Monaco initialization: error:', error));

    return () => editorRef.current ? disposeEditor() : cancelable.cancel();
  });

  useUpdate(() => {
    editorRef.current.getModel().modified.setValue(modified);
  }, [modified], isEditorReady);

  useUpdate(() => {
    editorRef.current.getModel().original.setValue(original);
  }, [original], isEditorReady);

  useUpdate(() => {
    const { original, modified } = editorRef.current.getModel();

    monacoRef.current.editor.setModelLanguage(original, originalLanguage || language);
    monacoRef.current.editor.setModelLanguage(modified, modifiedLanguage || language);
  }, [language, originalLanguage, modifiedLanguage], isEditorReady);

  useUpdate(() => {
    monacoRef.current.editor.setTheme(theme);
  }, [theme], isEditorReady);

  useUpdate(() => {
    editorRef.current.updateOptions(options);
  }, [options], isEditorReady);

  const setModels = useCallback(() => {
    const originalModel = monacoRef.current.editor.createModel(original, originalLanguage || language);

    const modifiedModel = monacoRef.current.editor.createModel(modified, modifiedLanguage || language);

    editorRef.current.setModel({ original: originalModel, modified: modifiedModel });
  }, [language, modified, modifiedLanguage, original, originalLanguage]);

  const createEditor = useCallback(() => {
    editorRef.current = monacoRef.current.editor.createDiffEditor(containerRef.current, {
      automaticLayout: true,
      ...options,
    });

    setModels();

    const { original, modified } = editorRef.current.getModel();
    editorDidMount(
      modified.getValue.bind(modified),
      original.getValue.bind(original),
      editorRef.current,
    );

    monacoRef.current.editor.defineTheme('dark', themes['night-dark']);
    monacoRef.current.editor.setTheme(theme);

    setIsEditorReady(true);
  }, [editorDidMount, options, theme, setModels]);

  useEffect(() => {
    !isMonacoMounting && !isEditorReady && createEditor();
  }, [isMonacoMounting, isEditorReady, createEditor]);

  const disposeEditor = () => editorRef.current.dispose();

  return (
		<MonacoContainer
			width={width}
			height={height}
			isEditorReady={isEditorReady}
			loading={loading}
			_ref={containerRef} 
			className={className}
			// wrapperClassName={wrapperClassName}
		/>
	);
};

// DiffEditor.propTypes = {
  // original: P.string,
  // modified: P.string,
  // language: P.string,
  // originalLanguage: P.string,
  // modifiedLanguage: P.string,
  // editorDidMount: P.func,
  // theme: P.string,
  // width: P.oneOfType([P.number, P.string]),
  // height: P.oneOfType([P.number, P.string]),
  // loading: P.oneOfType([P.element, P.string]),
  // options: P.object
// };

DiffEditor.defaultProps = {
  editorDidMount: Q.noop,
  theme: 'dark', // light
  width: '100%',
  height: '100%',
  loading: 'Loading...',
  options: {}
};

export default DiffEditor;
