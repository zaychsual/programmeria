import React from 'react';
// import P from 'prop-types';

// import Loading from '../Loading';
import SpinLoader from '../../q-ui-react/SpinLoader';
import styles from './styles';

// import {Cx} from '../../utils/Q';

// ** forwardref render functions do not support proptypes or defaultprops **
// one of the reasons why we use a separate prop for passing ref instead of using forwardref

// {!isEditorReady && <Loading content={loading} />}
const MonacoContainer = ({ 
	width, 
	height, 
	isEditorReady, 
	loading, 
	_ref, 
	className, 
	// wrapperClassName, 
	loadBg
}) => (
  <section 
		style={{ ...styles.wrapper, width, height }} 
		className={className}
	>
		{/* !isEditorReady && <SpinLoader bg={loadBg} tip={loading} /> */}
    {(loading && !isEditorReady) && <SpinLoader className="bg-programmeria-logo bg-size-36" bg={loadBg} tip={loading} />}
    
		<div ref={_ref}
      style={{ ...styles.fullWidth, ...(!isEditorReady && styles.hide) }} 
			// className={className} 
    />
  </section>
);

// Q-CUSTOM For loading backgroundImage
// MonacoContainer.defaultProps = {
	// loadBg: "/icon/android-icon-36x36.png"
// };

// MonacoContainer.propTypes = {
  // width: P.oneOfType([P.number, P.string]).isRequired,
  // height: P.oneOfType([P.number, P.string]).isRequired,
  // loading: P.oneOfType([P.element, P.string]).isRequired,
  // isEditorReady: P.bool.isRequired
// };

export default MonacoContainer;
