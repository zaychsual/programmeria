import React, { useState } from 'react';// forwardRef, useRef, useState, useEffect
import Editor, { loader } from '@monaco-editor/react';// useMonaco
// import Dropdown from 'react-bootstrap/Dropdown';
// import Tab from 'react-bootstrap/Tab';
import Collapse from 'react-bootstrap/Collapse';
// import Nav from 'react-bootstrap/Nav';

import Flex from '../q-ui-react/Flex';
import Btn from '../q-ui-react/Btn';
import Input from '../q-ui-react/Input';
import onDidMount from './onDidMount';
import uid from '../../utils/UniqueComponentId';
import { LANGS, THEMES } from '../../data/monaco';// 

// const THEMES = ["dark", "light"];
// const ACCEPT_MIME = [...(new Set(LANGS.map(v => v.type)))];// Array.from(new Set(MONACO_LANGS.map(v => v.type)))
const MONACO_LANGS = [...(new Set(LANGS.map(v => v.name)))];

const IDE_OPTIONS = {
	fontSize: 14, 
  fontFamily: "Monaco", 
	tabSize: 2, 
	dragAndDrop: true, // Defaults = false
	// colorDecorators: false, // Enable inline color decorators and color picker rendering. (Css color preview)
	// readOnly: false, // Should the editor be read only. See also domReadOnly. Defaults = false.
  // domReadOnly: false, // Should the textarea used for input use the DOM readonly attribute. Defaults = false.
	mouseWheelZoom: true, // false
	minimap: {
		enabled: false, // Defaults = true
		side: "right", // Default = right | left
    size: "actual", // Control the minimap rendering mode. Default = actual | 'proportional' | 'fill' | 'fit'
		showSlider: "mouseover", // Default = mouseover | always
		renderCharacters: true, // Default = true
    maxColumn: 120, // Limit the width of the minimap to render at most a certain number of columns. Default = 120
    scale: 1 // Relative size of the font in the minimap. Default = 1
	},
};

// console.log('%cMONACO_LANGS: ','color:yellow', MONACO_LANGS);

export default function MonacoEditor({
  vs = "/js/libs/monaco-editor/min/vs", 
  theme = "dark", // THEMES[0], // "vs-dark", // "light" | "vs-dark"
  config, 
  options, 
  language = "plaintext", 
  required, 
  disabled = false, 
  height = 400, // "50vh"
  changeLang = true, 
  wrapClass, 
  nav, 
  ...etc
}){
  loader.config({ paths: { vs }, ...config });

  const ID = uid();
  const [themes, setThemes] = useState(theme);
  const [lang, setLang] = useState(language);
  // const [isDisabled,  setIsDisabled] = useState(disabled);
  const [settings, setSettings] = useState({ ...options, ...IDE_OPTIONS, readOnly: disabled, domReadOnly: disabled });
  const [menu, setMenu] = useState("");

  const onMountEditor = (editor, monaco) => {
    onDidMount(editor, monaco, required);
    // console.log('onMountEditor editor: ', editor);
    // console.log('onMountEditor monaco: ', monaco);
  }

  const onClickMenu = (val) => {
    setMenu(val !== menu ? val : "");
  }

  const onChangeSets = (val, key) => {
    setSettings({ ...settings, [key]: val });// Q.isNum(val) ? Number(val) : val
  }

  let parseTheme = themes === "dark" ? "secondary":"light";// themes === "light" ? "light":"dark"

  return (
    <Flex dir="row" align="stretch" 
      style={{ height: height }} 
      className={Q.Cx("bg-" + parseTheme + " monacoEditor text-" + (themes === "dark" ? "light" : "dark"), wrapClass)}
    >
      <nav className="nav flex-column">
        {Q.isFunc(nav) && nav(themes)} 

        <Btn onClick={() => onClickMenu("Info")} active={menu === "Info"} className="mt-auto border-0 rounded-0 qi qi-info" kind={parseTheme} />
        <Btn onClick={() => onClickMenu("Settings")} active={menu === "Settings"} className="border-0 rounded-0 qi qi-cog" kind={parseTheme} />

        {/* <Dropdown drop="right" className="mt-auto">
          <Dropdown.Toggle bsPrefix="qi qi-cog" variant="light" />
          <Dropdown.Menu align="right">
            <Nav.Link eventKey="Settings">Settings</Nav.Link>
            <Dropdown.Item {...Q.DD_BTN}>Action</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown> */}
      </nav>

      <div className="position-relative flex-grow-1">
        <Collapse mountOnEnter 
          timeout={150} 
          in={menu === "Info"} 
          className={"position-absolute inset-0 zi-1 bg-" + themes} 
        >
          <div>
            Info
          </div>
        </Collapse>

        <Collapse mountOnEnter 
          timeout={150} 
          in={menu === "Settings"} 
          className={"position-absolute inset-0 zi-1 bg-" + themes} 
        >
          <div>
            <div className="p-3 ovyauto h-100">
              <Btn onClick={() => onClickMenu("Settings")} outline size="sm" kind={themes === "dark"? "light":"dark"} className="float-right qi qi-close" />
              <h5>Settings</h5>
              <div className="row">
                <div className="col-md-3 mb-3">
                  Language 
                  <select className="custom-select custom-select-sm mt-1" 
                    disabled={!changeLang} 
                    value={lang} 
                    onChange={e => setLang(e.target.value)} 
                  >
                    {changeLang ? 
                      MONACO_LANGS.map(v => <option key={v} value={v}>{v}</option>)
                      : 
                      <option value={lang}>{lang}</option>
                    }
                  </select>
                </div>

                <div className="col-md-3 mb-3">
                  Theme 
                  <select className="custom-select custom-select-sm mt-1" 
                    value={themes} 
                    onChange={e => setThemes(e.target.value)} 
                  >
                    {THEMES.map(v => <option key={v} value={v}>{v}</option>)}
                  </select>
                </div>

                <div className="col-md-3 mb-3">
                  Font Size 
                  <Input className="mt-1" isize="sm" type="number" min={10} max={20} inputMode="numeric" // decimal
                    value={settings.fontSize} 
                    onChange={e => onChangeSets(e.target.value, "fontSize")} 
                  />
                </div>

                <div className="col-md-3 mb-3">
                  Font Family 
                  <select className="custom-select custom-select-sm mt-1"
                    value={settings.fontFamily} 
                    onChange={e => onChangeSets(e.target.value, "fontFamily")} 
                  >
                    {["Monaco", "Consolas", "Courier New", "monospace", "Menlo", "Liberation Mono", "SFMono-Regular"].map(v => 
                      <option key={v} value={v}>{v}</option>
                    )}
                  </select>
                </div>

                <div className="col-md-3 mb-3">
                  Tab Size 
                  <Input className="mt-1" isize="sm" type="number" min={2} max={6} inputMode="numeric" // decimal
                    value={settings.tabSize} 
                    onChange={e => onChangeSets(e.target.value, "tabSize")} 
                  />
                </div>

                <div className="col-md-3 mb-3">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id={"mouseWheelZoom" + ID} 
                      value={settings.mouseWheelZoom} 
                      onChange={e => onChangeSets(e.target.checked, "mouseWheelZoom")} 
                    />
                    <label className="custom-control-label" htmlFor={"mouseWheelZoom" + ID}>Mouse Wheel Zoom</label>
                  </div>
                  <small className="form-text">Zoom the font in the editor when using the mouse wheel in combination with holding Ctrl</small>
                </div>

                <div className="col-md-3 mb-3">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id={"dragAndDrop" + ID} 
                      value={settings.dragAndDrop} 
                      onChange={e => onChangeSets(e.target.checked, "dragAndDrop")} 
                    />
                    <label className="custom-control-label" htmlFor={"dragAndDrop" + ID}>Drag And Drop</label>
                  </div>
                  <small className="form-text">Controls if the editor should allow to move selections via drag and drop</small>
                </div>

                <div className="col-12 border-top my-3" />

                {/* minimap : enabled */}
                <div className="col-md-3 mb-3">
                  Minimap 
                  <div className="custom-control custom-checkbox mt-1">
                    <input type="checkbox" className="custom-control-input" id={"minimapEnabled" + ID} 
                      value={settings.minimap.enabled} 
                      onChange={e => setSettings({ ...settings, minimap: { ...settings.minimap, enabled: e.target.checked } })} 
                    />
                    <label className="custom-control-label" htmlFor={"minimapEnabled" + ID}>Enable</label>
                  </div>
                </div>

                <div className="col-md-3 mb-3">
                  Minimap Position 
                  <select className="custom-select custom-select-sm mt-1" 
                    value={settings.minimap.side} 
                    onChange={e => setSettings({ ...settings, minimap: { ...settings.minimap, side: e.target.value } })} 
                  >
                    {["right","left"].map(v => <option key={v} value={v}>{v}</option>)}
                  </select>
                </div>

                <div className="col-md-3 mb-3">
                  Minimap Size  
                  <select className="custom-select custom-select-sm mt-1" 
                    value={settings.minimap.size} 
                    onChange={e => setSettings({ ...settings, minimap: { ...settings.minimap, size: e.target.value } })} 
                  >
                    {["actual","proportional","fill","fit"].map(v => <option key={v} value={v}>{v}</option>)}
                  </select>
                </div>

                <div className="col-md-3 mb-3">
                  Minimap Slider  
                  <select className="custom-select custom-select-sm mt-1" 
                    value={settings.minimap.showSlider} 
                    onChange={e => setSettings({ ...settings, minimap: { ...settings.minimap, showSlider: e.target.value } })} 
                  >
                    {["mouseover","always"].map(v => <option key={v} value={v}>{v}</option>)}
                  </select>
                </div>

                <div className="col-md-3 mb-3">
                  Minimap Characters
                  <div className="custom-control custom-checkbox mt-1">
                    <input type="checkbox" className="custom-control-input" id={"renderCharacters" + ID} 
                      value={settings.minimap.renderCharacters} 
                      onChange={e => setSettings({ ...settings, minimap: { ...settings.minimap, renderCharacters: e.target.checked } })} 
                    />
                    <label className="custom-control-label" htmlFor={"renderCharacters" + ID}>Enable</label>
                  </div>
                </div>

                <div className="col-md-3 mb-3">
                  Minimap Max Column 
                  <Input className="mt-1" isize="sm" type="number" min={10} max={150} inputMode="numeric" // decimal
                    value={settings.minimap.maxColumn} 
                    onChange={e => setSettings({ ...settings, minimap: { ...settings.minimap, maxColumn: e.target.value } })} 
                  />
                </div>

                <div className="col-md-3 mb-3">
                  Minimap Scale 
                  <Input className="mt-1" isize="sm" type="number" min={5} max={10} inputMode="numeric" // decimal
                    value={settings.minimap.scale} 
                    onChange={e => setSettings({ ...settings, minimap: { ...settings.minimap, scale: e.target.value } })} 
                  />
                </div>

              </div>
            </div>
          </div>
        </Collapse>

        <Editor 
          {...etc} 
          wrapperClassName={Q.Cx("flex-grow-1 position-absolute inset-0 ", etc.wrapperClassName)} // Q.Cx("flex-grow-1", etc.wrapperClassName)
          theme={themes === "dark" ? "vs-dark":"light"} // themes
          language={lang} 
          options={settings} 
          onMount={onMountEditor} 
          // width="auto" 
        />
      </div>
    </Flex>
  );
}

/*
// [
//   { v: "vs-dark", l: "Dark" }, 
//   { v: "light", l: "Light" }
// ]

      <Tab.Container 
        mountOnEnter
        id={"tabMonacoEditor" + uid()} 
        activeKey={tab} // defaultActiveKey
        onSelect={setTab} 
      >

      </Tab.Container>

        <Tab.Content>
          <Tab.Pane eventKey="Settings">
            <div>
              Settings
            </div>
          </Tab.Pane>
        </Tab.Content>

const MonacoEditor = forwardRef(
  ({
    vs = "public/js/libs/monaco-editor/min/vs", 
    theme = "vs-dark", // "light" | "vs-dark"
    wrapClass, 
    ...etc
  }, 
  ref
) => {
  const [settings, setSettings] = useState({ theme });

  loader.config({ paths: { vs } });

  return (
    <div ref={ref} 
      className={Q.Cx("monacoEditor", wrapClass)}
    >
      <Editor 
        {...etc} 
        theme={settings.theme}  
        // height="60vh" 
        // defaultValue="// some comment" 
        // defaultLanguage="javascript" 
      />
    </div>
  );
});;

export default MonacoEditor;
*/
