import React from 'react';// , { useState }
import Draggable from 'react-draggable';
import { DropTarget } from 'react-dnd';

import { ItemTypes } from './ItemTypes';

function Resizer({
  // FOR react-dnd: 
	canDrop, 
	isOver, 
	connectDropTarget, 

  // data, 
  // sectionIndex, 
  colIndex, // rowIndex

  onResize = () => {}, 
  // onDoubleClick = () => {}, 
}){
  // const [pos, setPos] = useState(data.columnWidth);
  const isActive = canDrop && isOver;
  // console.log('data: ', data);

  return (
    <Draggable 
      axis="x" 
      handle=".resizerTbstyle" 
      position={{ x: 0 }} // pos
      // onMouseDown={e => e.stopPropagation()} 
      // onStart={this.handleStart} 
      onDrag={(e, obj) => {
        e.stopPropagation();
        // console.log('onDrag obj: ', obj);// columnWidth
        // console.log('onDrag obj.deltaX: ', obj.deltaX);// obj.deltaX + pos
        onResize(e, obj);
        // setPos(obj.x + pos);
        // const thead = Q.domQ(".thead", obj.node.nextElementSibling);
        // if(thead){
        //   thead.style = (obj.x + data.columnWidth) + "px";
        // }
      }} 
      // onStop={this.handleStop} 
    >
      <div tabIndex={-1} 
        ref={connectDropTarget} 
        // data-sectionindex={sectionIndex} 
        data-colindex={colIndex} // rowIndex | rowindex | colindex

        className={ // col-resize
          Q.Cx("position-absolute b0 h-100 zi-3 ceresize resizerTbstyle", {
            "active": isActive, // !children && 
            "canDrop": canDrop, // !children && 
            // "d-none": data.fid
          })
        } 
        // onDoubleClick={(e) => { // 
        //   const tw = Q.domQ(".thead-label span", e.target.nextElementSibling);
        //   // console.log('onDoubleClick resizerTbstyle tw : ', tw);
        //   onDoubleClick(tw);// 200
        // }}
      />
    </Draggable>
  );
}

export default DropTarget(
  ItemTypes.FIELD, 
  {
		// drop: (props) => ({ data: props }) 
		drop: (data) => ({ data }) // | data
    /* drop: (item, monitor) => {
			const data = { ...item };
			// monitor.onDrop(data);
			console.log('data: ', data);
			console.log('monitor: ', monitor);
			return data;
		} */
  },
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  })
)(Resizer);