import React from 'react';// , { useRef }
import { DragSource } from 'react-dnd';

import Flex from '../q-ui-react/Flex';
import { ItemTypes } from './ItemTypes';

function Col({
  isDragging, 
	connectDragSource, 

  draggable, 
  children, 
  active, 
  onClick = () => {}
}){
  const Click = e => {
    // e.stopPropagation();
    const et = e.target;
    // const thead = Q.hasClass(et, "thead-label");
    // if(thead) e.stopPropagation();

    // !Q.hasClass(et, "thead-label") && 
    if(!Q.hasClass(et, "btnRemoveTcol") && !Q.hasClass(et, "resizerTbstyle")){
      const parent = Q.hasClass(et, ".tcol") ? et : et.closest(".tcol");
      // document.body.click();
      // const rect = parent.getBoundingClientRect();
      // const secRef = sectionTableStyleRef.current;
      // // const isScroll = secRef.scrollLeft > 0 && secRef.scrollWidth > secRef.offsetWidth;// clientWidth
      // // console.log('isScroll: ', isScroll);

      // sectionTableStyleRef.current.scroll({
      //   left: (rect.x - rect.width) - 57, // scrollHeight
      //   // behavior: "smooth"
      // });

      // Q.domQ(".thead-label", parent)?.click();
      // setTimeout(() => Q.domQ(".thead-label", parent)?.click(), 700);

      // if(isScroll){
      //   const rect = parent.getBoundingClientRect();
      //   // console.log('rect: ', rect);

      //   secRef.scroll({
      //     left: (rect.x - rect.width) - 57, // scrollHeight
      //     behavior: "smooth"
      //   });

      //   setTimeout(() => Q.domQ(".thead-label", parent)?.click(), 700);
      // }else{
      //   Q.domQ(".thead-label", parent)?.click();
      // }
      
      // NOT SUPPORT: IE, Safari, Safari on iOS
      parent.scrollIntoView({ behavior: "smooth", block: "end" });// behavior: "smooth", inline: "nearest"

      setTimeout(() => {
        // Q.domQ(".thead-label", parent)?.click();
        onClick();// parent, e
      }, 9);
    }
  }

  return (
    <Flex dir="column" 
      className={"position-relative cpoin tcol tcol-field" + (active ? " zi-5":"")} // active
      onClick={Click}
    >
      {draggable}

      <Flex tabIndex="-1" 
        ref={connectDragSource} 
        dir="column" 
        className={"position-relative" + (isDragging ? " isDragging":"")} // col px-0 
      >
        {children}
      </Flex>
    </Flex>
  );
}

export default DragSource(
  ItemTypes.FIELD,
  {
		// beginDrag: (data) => ({ ...data }), // data: props.data
		beginDrag: (props) => {//props, monitor, component
			if(props.onDrag){
				props.onDrag(props.data);
			}
			return { ...props };
		}, 
    endDrag(props, monitor){
      // const item = monitor.getItem();
      // const dropResult = monitor.getDropResult();
			// const didDrop = monitor.didDrop();
			// console.log(didDrop);
			
			if(props.onDrop){
				const dropResult = monitor.getDropResult();
				if(dropResult){
					// const item = monitor.getItem();
          // console.log('item: ', item);
          // console.log('dropResult: ', dropResult);
					props.onDrop(Q.omit(dropResult.data, ["onResize"]));// item.data, 
				}
				else props.onDrop();
			}
    }
  },
  (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
		// didDrop: monitor.didDrop()
  })
)(Col);