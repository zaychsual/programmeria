import React, { useRef, useState } from 'react';// , useState, useEffect
import Dropdown from 'react-bootstrap/Dropdown';
// import Draggable from 'react-draggable';

import Flex from '../q-ui-react/Flex';
import Btn from '../q-ui-react/Btn';

import Col from './Col';
import Resizer from './Resizer';

export default function FormTableEditor({
  data, 
  onResize = () => {}, 
  // onDoubleClick = () => {}, 
  onAdd = () => {}, 
  onDelete = () => {}, 
  onDrop = () => {}, 
}){
  const toggleRef = useRef();
  const sectionTableRef = useRef();
  const [dataField, setDataField] = useState({});// null

  const onHideShowResizer = (i, hide = false) => {
    const resizer = Q.domQall(".tcol-field", sectionTableRef.current)?.[i]?.firstElementChild;
    if(resizer) resizer.hidden = hide;
  }

  const renderTd = (c = "p-3") => [1,2,3,4].map((v) => <Flex key={v} className={"border " + c} />);//  </Flex>

  return (
    <div className="w-100 position-relative tb-editor">
      <Dropdown className="position-absolute t0 l0 r0 w-100" //  d-flex flex1 text-truncate | position-static
        onToggle={(open) => {
          // open && document.body.click();
          // console.log('open: ', open);
          if(!open) setDataField({});
        }} 
      >
        <Dropdown.Toggle size="sm" // variant="flat" 
          ref={toggleRef} 
          className="invisible w-100" 
          // bsPrefix={"point-no flex1 mw-100 text-truncate text-left bg-transparent rounded-0 shadow-none thead-label q-mr qi qi-" + v.icon}
        >
          {/* <span className={"d-inline-block point-no  mw-100 text-truncate q-mr qi qi-" + v.icon}>{v.fieldName}</span> */}
          {/* {v.fieldName} */}
        </Dropdown.Toggle>

        <Dropdown.Menu className="w-100">
          <div>
            Name : {dataField.fieldName} 
          </div>

          {Array.from({length:5}).map((_, i2) => <Dropdown.Item key={i2} as="button" type="button">Action {i2 + 1}</Dropdown.Item>)}
        </Dropdown.Menu>
      </Dropdown>

      <Flex nowrap dir="row" align="stretch" 
        ref={sectionTableRef} 
        className="table-responsive pb-2 q-scroll scroll-eee table-style" 
      >
        {data.map((v, i) => 
          <Col key={i} 
            // data={{ ...v, colIndex: i }} 
            draggable={
              <Resizer 
                // sectionIndex={} 
                colIndex={i} // rowIndex | colIndex
                data={v} 
                onResize={(e, obj) => {
                  const columnWidth = obj.deltaX + v.columnWidth;
                  // console.log('onResize obj.deltaX: ', obj.deltaX);
                  // if(columnWidth > 72){
                    // console.log('onResize columnWidth: ', columnWidth);
                    onResize(data.map((v2, i2) => ( i2 === i ? { ...v2, columnWidth } : v2 ) ));
                  // }
                }} 
                // onDoubleClick={tw => onDoubleClick(data.map((v2, i2) => ( i2 === i ? { ...v2, columnWidth: tw ? tw.clientWidth + 38 : 200 } : v2 ) ))}
              />
            } 
            active={v.fid === dataField.fid} 
            onClick={() => { // parent, e
              // console.log('onClick Col parent: ', parent);
              // console.log('onClick Col e: ', e.target);
              setDataField(v);
              setTimeout(() => toggleRef.current?.click(), 9);
            }}
            onDrag={() => { // data
              // console.log('onDrag data: ', data);
              onHideShowResizer(i, true);
            }}
            onDrop={(data) => {
              onHideShowResizer(i);
              onDrop(data, { data: v, colIndex: i });
            }} 
            // onDrop={(dropData, dropResult) => onDrop(dropData, dropResult)} // () => onDrop(v, i)
          >
            <>
              <Flex nowrap className="border thead" // shadow-sm position-relative  
                style={{ width: v.columnWidth }} // Control width here
                title={v.fieldName} 
              >
                <div className={"btn btn-sm text-left flex1 mw-100 text-truncate select-no thead-label q-mr qi qi-" + v.icon}>{v.fieldName}</div>
                
                {data.length > 1 && 
                  <Btn kind="flat" size="sm" className="flexno qi qi-trash xx btnRemoveTcol" title="Remove" 
                    onClick={() => {
                      if(window.confirm("Are you sure to delete this field?")){
                        onDelete(data.filter((_, i2) => i2 !== i))
                      }
                    }}
                  />
                }
              </Flex>

              {renderTd()}
            </>
          </Col>
        )}

        <Flex dir="column" className="position-sticky r0 zi-4 cpoin tcol tcol-add" tabIndex="-1" // col px-0 
          onClick={() => {
            const newData = { fieldName: "Untitled field", fieldType: "text", icon: "mail", columnWidth: 200, fid: Q.Qid() };
            onAdd([ ...data, newData ]);// , colIndex: 
            setTimeout(() => {
              const secRef = sectionTableRef.current;
              secRef.scroll({
                left: secRef.scrollWidth, // scrollHeight
                behavior: "smooth"
              });
              
              // console.log('secRef.scrollWidth: ', secRef.scrollWidth);
              // console.log('secRef.scrollLeft: ', secRef.scrollLeft);
              // Open Dropdown new field here
              setTimeout(() => {
                // Q.domQ(".thead-label", document.querySelectorAll(".tcol-field")[data.length])?.click();
                setDataField(newData);
                toggleRef.current?.click();
              }, 9);// behavior: "smooth", 
            }, 9);
          }}
        >
          <Flex justify="center" align="center" className="border px-2 select-no thead thead-add qi qi-plus">
            {/* <div className="thead-label flex1 p-2 select-no cpoin qi qi-plus" /> */}
          </Flex>

          {renderTd()}
        </Flex>

        <Flex dir="column" className="col px-0 tcol tcol-add tcol-empty">
          <Flex justify="center" align="center" className="border py-2 select-no thead thead-add w-auto" />
          {renderTd("py-3")}
        </Flex>
      </Flex>
    </div>
  );
}

/*   
          // align="right" 
          // flip={false} 
          // popperConfig={{
          //   strategy: "fixed", 
          //   // modifiers: [
          //   //   {
          //   //     name: "flip", 
          //   //     options: {
          //   //       boundary: parentRef, // sectionTableRef.current,
          //   //     },
          //   //   },
          //   // ],
          //   // modifiers: [{
          //   //   name: "offset",
          //   //   options: {
          //   //     offset: [30, 0] // ddFieldOffset | NOT RUN: to change dropdown offset
          //   //   }
          //   // }]
          // }}
          // style={{ width: parentRef?.clientWidth ?? 750 }} // 1013 | 750
*/



