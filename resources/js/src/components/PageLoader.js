import React from 'react';

import SpinLoader from './q-ui-react/SpinLoader';
// import { Cx } from '../utils/Q';

export default function PageLoader({
	top, 
	bottom, 
	left, 
	className, 
	// progress, 
	w, 
	h, 
	...etc
}){
  // const [data, setData] = React.useState();
	
	// React.useEffect(() => {
	// 	console.log('%cuseEffect in PageLoader','color:yellow;');
	// }, []);

	return (
		<SpinLoader 
			{...etc} 
			
			className={
				Q.Cx("position-fixed r0 zi-upModal bg-programmeria-logo bg-size-36 page-loader", {
					"t0": top,
					"b0": bottom,
					"l0": left
				}, className)
			} 
			// bg="/icon/android-icon-36x36.png" // "/icon/programmeria-36x36.png" | 
			style={{
				width: w, // 'calc(100% - 220px)',
				// height: bottom ? 'calc(100% - 48px)' : null
				height: h
			}}
			// appendChild={} 
		>
			
		</SpinLoader>
	);
}

/*
{progress && 
	<div 
		className="q-progress position-absolute t0 l0 r0" 
		role="progressbar" 
		aria-valuemin="0" 
		aria-valuemax="100" 
		aria-valuenow={"50"} // progress 
		// aria-label={"50"} // {progress + "%"} 
		style={{ height: 3, "--val": "50%" }} // {{ "--val": progress + "%" }}
	/>
}
*/

/*
<progress 
	className="q-progress position-absolute t0 l0 r0" 
	style={{ height: 3 }} 
	dir="ltr" 
	value="50" 
	max="100" 
/>

<div className="progress rounded-0 position-absolute t0 l0 r0" style={{height: 3}}>
	<div className="progress-bar" role="progressbar" 
		style={{width:'25%'}} 
		aria-valuenow="25" 
		aria-valuemin="0" 
		aria-valuemax="100"
	/>
</div>

<div className="page-loader"></div>
<React.Fragment></React.Fragment>
*/
