import React, { useRef, useState } from 'react';
import Draggable from 'react-draggable';

function Column({
  size, 
  boundsRef, 
  // prevEl, 
  // nextEl, 
  totalCol, 
  index, 
}){
  const parentRef = useRef(null);
  const [width, setWidth] = useState(null);

  const onStart = (e, obj, pos) => {
    // console.log('onStart e: ', e);

    const parent = parentRef.current;
    // console.log('onStart parent.getBoundingClientRect().width: ', parent.getBoundingClientRect().width);
    setWidth(parent.getBoundingClientRect().width);

    if(pos !== "left"){ // nextEl
      if(parent.nextElementSibling){
        // parent.nextElementSibling.style.flexBasis = "0";
        // parent.nextElementSibling.style.flexGrow = "1";
        // parent.nextElementSibling.style.maxWidth = "100%";
        // parent.nextElementSibling.classList.toggle("col");
        Q.setClass(parent.nextElementSibling, "col-md-" + parent.nextElementSibling.dataset.col, "remove");
        Q.setClass(parent.nextElementSibling, "col");
      }else{
        Q.setClass(parent.previousElementSibling, "col-md-" + parent.previousElementSibling.dataset.col, "remove");
        Q.setClass(parent.previousElementSibling, "col");
      }
    }
    else{
      if(parent.previousElementSibling){
        Q.setClass(parent.previousElementSibling, "col-md-" + parent.previousElementSibling.dataset.col, "remove");
        Q.setClass(parent.previousElementSibling, "col");
      }else{
        Q.setClass(parent.nextElementSibling, "col-md-" + parent.nextElementSibling.dataset.col, "remove");
        Q.setClass(parent.nextElementSibling, "col");
      }
    }
  }

  const onDrag = (e, obj, pos) => {
    e.stopPropagation();
    // console.log('onDrag obj: ', obj);// columnWidth
    // console.log('onDrag obj.deltaX: ', obj.deltaX);// obj.deltaX + pos
    // onResize(e, obj);
    // setPos(obj.x + pos);
    // const thead = Q.domQ(".thead", obj.node.nextElementSibling);
    // if(thead){
    //   thead.style = (obj.x + data.columnWidth) + "px";
    // }

    const colSize = pos === "left" ? width - obj.deltaX : width + obj.deltaX;

    // const snap2Left = obj.deltaX < 0 && obj.lastX <= 85.275;
    // const snap2Right = obj.deltaX > 0 && obj.lastX >= 85.275;
    
    // console.log('obj: ', obj);
    // console.log('obj.lastX: ', obj.lastX);
    // console.log('colSize: ', colSize);// 341.104 | 85.275
    // console.log('snap2Left: ', snap2Left);
    // console.log('snap2Right: ', snap2Right);
    setWidth(colSize);
  }

  const onStop = (e, obj, pos) => {
    // console.log('onStop e: ', e);
    // console.log('onStop obj: ', obj);
    setWidth(null);

    const parent = parentRef.current;
    // if(pos !== "left" && parent.nextElementSibling){
    //   Q.setClass(parent.nextElementSibling, "col-md-" + parent.nextElementSibling.dataset.col);
    //   Q.setClass(parent.nextElementSibling, "col", "remove");
    // }
    // else if(pos === "left" && parent.previousElementSibling){
    //   Q.setClass(parent.previousElementSibling, "col-md-" + parent.previousElementSibling.dataset.col);
    //   Q.setClass(parent.previousElementSibling, "col", "remove");
    // }

    if(pos !== "left"){
      if(parent.nextElementSibling){
        Q.setClass(parent.nextElementSibling, "col-md-" + parent.nextElementSibling.dataset.col);
        Q.setClass(parent.nextElementSibling, "col", "remove");
      }else{
        Q.setClass(parent.previousElementSibling, "col-md-" + parent.previousElementSibling.dataset.col);
        Q.setClass(parent.previousElementSibling, "col", "remove");
      }
    }
    else{
      if(parent.previousElementSibling){
        Q.setClass(parent.previousElementSibling, "col-md-" + parent.previousElementSibling.dataset.col);
        Q.setClass(parent.previousElementSibling, "col", "remove");
      }else{
        Q.setClass(parent.nextElementSibling, "col-md-" + parent.nextElementSibling.dataset.col);
        Q.setClass(parent.nextElementSibling, "col", "remove");
      }
    }
  }

  const renderResize = (pos) => {
    return (
      <Draggable 
        bounds={boundsRef} // parentRef.current
        axis="x" 
        handle=".fd-handle" 
        position={{ x: 0 }} // pos
        // onMouseDown={e => e.stopPropagation()} 
        onStart={(e, obj) => onStart(e, obj, pos)} 
        onDrag={(e, obj) => onDrag(e, obj, pos)} 
        onStop={(e, obj) => onStop(e, obj, pos)} 
      >
        <div tabIndex={-1} 
          // ref={connectDropTarget} 
          // data-sectionindex={sectionIndex} 
          // data-colindex={colIndex} // rowIndex | rowindex | colindex

          className={"flexno my-auto border rounded ceresize qi qi-ellipsis-v fd-handle r-" + pos} 
          // className={ // col-resize
          //   Q.Cx("position-absolute b0 h-100 zi-3 ceresize fd-handle", {
          //     "active": isActive, // !children && 
          //     "canDrop": canDrop, // !children && 
          //     // "d-none": data.fid
          //   })
          // } 
          // onDoubleClick={(e) => { // 
          //   const tw = Q.domQ(".thead-label span", e.target.nextElementSibling);
          //   // console.log('onDoubleClick resizerTbstyle tw : ', tw);
          //   onDoubleClick(tw);// 200
          // }}
        />
      </Draggable>
    )
  }

  return (
    <div 
      ref={parentRef} 
      className={"d-flex flex-row h-100 col-md-" + size} 
      data-col={size} 
      style={{
        flexBasis: width, 
        maxWidth: width
      }}
    >
      {index > 0 && renderResize("left")}

      <div className="form-control w-100 flex1">{size}</div>

      {(totalCol !== index + 1) && renderResize("right")}
    </div>
  );
}

export default function FormDesigner({
  data, 
  className, 
}){
  const wrapRef = useRef(null);
  const [forms, setForms] = useState(data);
  

  return (
    <div 
      ref={wrapRef} 
      className={Q.Cx("form-designer", className)}
    >
      <div className="form-row">
        {forms.map((v, i) => 
          <Column key={i} 
            boundsRef={wrapRef.current}  
            size={v.size} 
            totalCol={forms.length} 
            index={i} 
            // prevEl={forms[i - 1]} 
            // nextEl={forms[i + 1]} 
          />
        )}
      </div>
    </div>
  );
}