import React from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import Dropdown from 'react-bootstrap/Dropdown';
// import Immutable from 'Immu'

import Flex from '../q-ui-react/Flex';
import Img from '../q-ui-react/Img';
import Btn from '../q-ui-react/Btn';

import { fileRead } from '../../utils/file/fileRead';

const TOOLS = [
	{label:"Polygon"},
	{label:"Rectangle"},
	{label:"Circle"},
	{label:"Select"},
	{label:"Delete"},
	// {label:"Test"},
];

export default class ImgMap extends React.Component{
  constructor(props){
		super(props);
		this.state = {
			files: null, 
			tool: null,

		};
	}
	
	// componentDidMount(() => {
	// 	console.log('%ccomponentDidMount in ImgMap','color:yellow;');
	// }

// 
	render(){
		const { files, tool } = this.state;

		return (
			<Flex dir="row" style={{ minHeight: 'calc(100vh - 48px)' }}>
				<div className="col-9 d-grid place-center bg-strip" style={{ minHeight: 'calc(100vh - 48px)' }}>
					{files && 
						<Img frame fluid 
							WrapAs="div" 
							alt={files.file.name} 
							src={files.result} 
						>
							<svg 
								className="position-absolute position-full w-100 h-100 svg-map"  
								// width="200" 
								// height="200" 
								xmlns="http://www.w3.org/2000/svg" 
							>

							</svg>
						</Img>	
					}
				</div>

				<div className="col-3 bg-light" style={{ minHeight: 'calc(100vh - 48px)' }}>
					<div className="btn-group-vertical w-100">
						<Dropdown bsPrefix="btn-group">
							<Dropdown.Toggle variant="light" className="text-left">Insert File</Dropdown.Toggle>
							<Dropdown.Menu className="w-400">{/* v-dd-sets */}
								<Dropdown.Item as="label" className="qi qi-upload">
									Upload
									<input type="file" accept=".jpg,.JPG,.jpeg,.JPEG,.png,.PNG,.gif,.GIF,.bmp,.BMP,.svg,.SVG,.webp,.WEBP" hidden 
										onChange={e => {
											let file = e.target.files[0];
											if(file){
												console.log(file);
												fileRead(file).then(r => {
													console.log(r);
													this.setState({ files: r });
												}).catch(e => console.log(e));
											}
										}}
									/>
								</Dropdown.Item>
								<hr/>
								<h6 className="dropdown-header">Insert URL</h6>
								<label className="input-group input-group-sm px-3">
									<input type="search" className="form-control" />
									<div className="input-group-append">
										<Btn As="b" kind="light" className="qi qi-check" />
									</div>
								</label>
							</Dropdown.Menu>
						</Dropdown>

						{TOOLS.map((v, i) => 
							<Btn key={i} onClick={() => this.setState({ tool: v.label })} blur kind="light" className="text-left" active={tool === v.label}>{v.label}</Btn>)
						}
					</div>
				</div>
			</Flex>
		);
	}
}

/*
<svg 
	className="img-fluid" 
	// width="200" 
	// height="200" 
	xmlns="http://www.w3.org/2000/svg" 
>       
	<image className="" href={files.result} 
		// height="200" 
		// width="200" 
	/>
</svg>

<React.Fragment></React.Fragment>
*/
