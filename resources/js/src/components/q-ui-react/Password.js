import React, { forwardRef, useState } from 'react';// { useState, useEffect }

import Btn from './Btn';

const Password = forwardRef(
	({
		wrapClass, 
		className, 
		prepend, 
		append, 
		inputSize, 
		// children, 
		// btnProps, 
		btnProps = {
			kind: "light"
		}, 
		// kind = "light", 
		...etc
	}, 
	ref
) => {
  const [see, setSee] = useState(false);

	return (
		<label className={Q.Cx("input-group", { ["input-group-" + inputSize]: inputSize }, wrapClass)}>
			{prepend}

			<input 
				{...etc} 
				ref={ref} 
				type={see ? "text":"password"} 

				// OPTION: From Gmail, toggle or default render
				// spellCheck={see ? false : undefined} 
				// autoComplete={see ? "off" : undefined} 
				// autoCapitalize={see ? "off" : undefined} 
				className={Q.Cx("form-control", className)} 
				spellCheck={false} 
				autoComplete="off" 
				autoCapitalize="off" 
			/>

			<div className="input-group-append">
				{append} 

				<Btn As="div" tabIndex="-1" 
					{...btnProps} 
					// variant={variant} 
					className={Q.Cx("tip tipTR q-fw qi qi-eye" + (see ? "-slash":""), btnProps?.className)} // btn btn-light 
					aria-label={(see ? "Hide":"Show") + " Password"}
					onClick={() => setSee(!see)} 
				/>
			</div>
		</label>
	);
});

export default Password;

/*
		<label 
			// ref={ref} 
			className={Q.Cx("q-input", className)} 
			// aria-label={label} 
		>
		</label>
*/
