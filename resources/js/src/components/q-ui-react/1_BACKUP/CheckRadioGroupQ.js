import React from 'react';// { useState, useEffect }
// import Btn from '../../components/q-ui-bootstrap/Btn';

export default function CheckRadioGroupQ({
	As = "div", 
	labelledby, // OPTION
	disabled, // OPTION
	...etc
}){
	// const nRef = useRef(null);
	// const [scripts, setScripts] = useState(false);

	// useEffect(() => {
	// 	console.log('%cuseEffect in CheckRadioGroupQ','color:yellow;');

	// 	[
	// 		{src:"/js/libs/q/radio/checkRadio.js", "data-js":"RadioButton"},
	// 		{src:"/js/libs/q/radio/checkRadioGroup.js", "data-js":"RadioGroup"}
	// 	].forEach((v, i) => {
	// 		Q.getScript(v).then(s => {
	// 			// console.log('Success get scripts CheckRadio & Group: ',s);
	// 			if(i > 0 && RadioButton && RadioGroup){
	// 				setScripts(true);

	// 				// document.getElementById('rg1') | nRef
	// 				const runGroup = new RadioGroup(nRef.current);
  // 				runGroup.init();
	// 			}
	// 		}).catch(e => {
	// 			console.log('Error get scripts CheckRadio & Group: ',e);
	// 		})
	// 	});

	// }, []);

	// if(!scripts) return null;

	return (
		<As 
			{...etc} 
			// ref={nRef} 
			role="radiogroup" 
			aria-labelledby={labelledby} // OPTION 
			aria-disabled={disabled ? true : null} // OPTION": For disabled
		/>
	);
}

/*
<React.Fragment></React.Fragment>
*/
