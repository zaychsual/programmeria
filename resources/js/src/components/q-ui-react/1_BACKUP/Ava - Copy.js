import React from 'react';// { useRef, useEffect }

// import { mapToCssModules } from '../q-react-bootstrap/utils';
// import { Cx, setClass, setAttr, isStr, cached } from '../../utils/Q.js'; // , hasClass
import darkOrLight from '../../utils/darkOrLight';

// FROM https://github.com/chakra-ui/chakra-ui/blob/master/packages/chakra-ui/src/Avatar/index.js
const getInitials = (name, no = '?') => {
	// if(!name || !Q.isStr(name) || name.length < 1) return no;
	if(!name || !Q.isStr(name) || name === " " || name.length < 1) return no;
	// Destruct 
  let [first, last] = name.split(" ");

  if(first && last){
    return `${first[0]}${last[0]}`;// first.charAt(0)}${last.charAt(0)
  }
	return first[0];// first.charAt(0)
};

// FROM https://github.com/segmentio/evergreen/blob/master/src/avatar/src/utils/getInitials.js
// function getInitials(name, fallback = '?'){
  // if(!name || typeof name !== 'string') return fallback
  // return name
    // .replace(/\s+/, ' ')
    // .split(' ') // Repeated spaces results in empty strings
    // .slice(0, 2)
    // .map(v => v && v[0].toUpperCase()) // Watch out for empty strings
    // .join('')
// }

// FROM: chakra-ui
function str2Hex(str, no = "5a6268"){
	if (!str || str.length === 0) return no;

  let hash = 0,
			sl = str.length;

  for(let i = 0; i < sl; i++){
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
    hash = hash & hash;
	}

	let color = "";// #

  for(let j = 0; j < 3; j++){
    let val = (hash >> (j * 8)) & 255;
    color += ("00" + val.toString(16)).substr(-2);
  }
  return color;
}

export default function Ava({
	inRef, 
	w = 30, 
	h = 30, 
	src, // OPTION
	alt = " ", 
	loading = "lazy", 
	drag = false, 
	className, 
	wrapClass, 
	wrapProps, 
	bg, // DEFINE background-color
	circle, round, thumb, contain, // fluid
	onError, 
	onLoad, 
	...etc
}){
	// DEV OPTION: random color error image with initial alt
	// const randomColor = Math.floor(Math.random() * 16777215).toString(16);

	/* useEffect(() => {
		// console.log('%cuseEffect in Ava','color:yellow;');
	}, []); */

	const removeLoadClass = (et) => {
		// Q.setClass(et.parentElement, 'ava-loader', 'remove');// ovhide ava-loader
		Q.setClass(et, 'ava-loader', 'remove');
	}

	const Err = e => {
		let et = e.target;

		removeLoadClass(et);
		
		if(Q.hasClass(et, "text-dark")) Q.setClass(et, "text-dark", "remove");
		if(Q.hasClass(et, "text-white")) Q.setClass(et, "text-white", "remove");
		
		// let setWidth = w ? w : h ? h : 50; // (et.offsetWidth || et.clientWidth);

		let fs = '--fs:calc(';
		if(isNaN(w)){ // setWidth
			let num = parseFloat(w);
			let unit = w.replace("" + num, ''); // `${num}`, '')
			fs += num + unit + " / 2.25);"; // 2.5 | `${num}${unit} / 2.25);`
		}else{
			fs += w + "px / 2.25);"; // 2.5 | `${w}px / 2.25);`
		}
		
		// const setAlt = et.alt || alt;// OPTION: if change alt (Directly in DOM)
		let trm = alt.trim();
		let color = bg ? bg.replace("#","") : str2Hex(trm);
		
		// if(!parseFloat(et.getAttribute("width"))) et.width = setWidth; // OPTION
		
		Q.setAttr(et, {
			'aria-label': getInitials(trm), // alt | setAlt
			style: fs + "--bg:#" + color // alt | setAlt | randomColor
		});
		
		Q.setClass(et, darkOrLight(color) === "dark" ? "text-white":"text-dark");

		// DEV OPTION: Change src with svg
		// et.src = `data:image/svg+xml,%3Csvg width="100%25" height="100%25" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"%3E%3Cstyle%3E%0Asvg%7Bfont-family:sans-serif;font-size:1rem;text-anchor:middle%7D%0A%3C/style%3E%3Crect width="100%25" height="100%25" fill="%2355595c"%3E%3C/rect%3E%3Ctext x="50%25" y="50%25" fill="%23eceeef" dy=".35em"%3E${getInitials(alt)}%3C/text%3E%3C/svg%3E`;

		// FROM MDN https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onerror
		// if(et.onerror) et.onerror = null;// DEV OPTION
		
		if(onError) onError(e);
		
		return;// null
	}

	const Load = e => {
		removeLoadClass(e.target);
		if(onLoad) onLoad(e);
	}

	// const sameCx = {
		// 'rounded-circle': circle,
		// 'rounded': round
	// };// , cssModule

	return (
		<div // figure | div | picture
			{...wrapProps} 
			className={
				// Q.Cx("img-frame ava-frame ava-loader", sameCx, frameClass)
				Q.Cx("img-frame ava-frame", {
					'img-thumbnail': thumb, 
					// 'rounded-circle': circle,
					// 'rounded': round
				}, wrapClass)
			}
			// draggable={!!drag} // OPTION
		>
			<img 
				{...etc} 
				ref={inRef} 
				loading={loading} // OPTIONS: native lazy load html
				width={w} 
				height={h} 
				src={Q.newURL(src).href} // OPTION
				alt={alt} 
				className={
					Q.Cx("ava ava-loader", {
						// ...sameCx,

						// "rounded-pill": pill, 
						'rounded': round, 
						'rounded-circle': circle, 
						// 'img-fluid': fluid,
						// 'ava-thumb': thumb, // img-thumbnail
						// 'img-thumbnail': thumb, 
						'of-con': contain
					}, className)
				}
				draggable={drag} // !!drag
				onError={Err}
				onLoad={Load}
				// onContextMenu={e => preventQ(e)}
			/>
		</div>
	);
}

// Ava.defaultProps = {
	// alt: ''
// };


