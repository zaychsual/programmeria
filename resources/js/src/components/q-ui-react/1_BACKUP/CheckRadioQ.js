import React, { useRef, useEffect } from 'react';// { useState, useEffect }

export default function CheckRadioQ({
	As = "div", 
	type = "checkbox", // role
	checked, //  = false
	indeterminate, 
	tabIndex, //  = "-1"
	value = "", 
	id = "", 
	name = "", 
	disabled = false, 
	required = false, 
	invalid, 
	onKeyDown, 
	onClick, 
	onChange = () => {}, 
	...etc
}){
	const ref = useRef(null);
  // const [check, setCheck] = useState(checked);
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in CheckRadioQ','color:yellow;');
	// }, []);

	const fakeDom = { // fakeDom
		type, 
		value, 
		// checked, 
		disabled, 
		required, 
		id, 
		name, 
		// invalid 
	};

	// OPTION: Change props function name to onChecked
	const Click = e => {
		if(disabled){
			e.stopPropagation();// OPTION
			e.preventDefault();
			return;
		}

		// console.log('setCheck isBool: ', !Q.isBool(checked));
		let et = e.target;
		let t = { checked: false, ...fakeDom };

		if(!disabled && !Q.isBool(checked)){
			if(Q.getAttr(et, 'aria-checked') === "true"){
				et.ariaChecked = false;
				onChange({ target: t });
			}else{
				et.ariaChecked = true;
				onChange({ target: { ...t, checked: true }});
			}
		}else{
			onChange({ target: { ...t, checked: !checked }});
		}

		if(onClick) onClick(e);
	}

	const keyDown = e => {
		let et = e.target;

		// console.log('keyDown et: ', et);
		// console.log('keyDown e.keyCode: ', e.keyCode);
		// console.log('keyDown et.which: ', et.which);// shiftKey
		// console.log('keyDown et.ariaDisabled: ', et.ariaDisabled);
		
		// Enter
		// et.ariaDisabled !== "true" && e.keyCode === 13
		if(!Boolean(et.ariaDisabled) && e.keyCode === 13){
			et.click();
		}

		if(onKeyDown) onKeyDown(e);
	}

// OPTION: For disabled from fieldset, can use CSS only pointer-event: none, but cursor can't be set
	// useEffect(() => {
	// 	const fieldset = ref.current.closest('fieldset');
	// 	if(disabled !== null && fieldset?.disabled){
	// 		ref.current.ariaDisabled = true;
	// 		console.log(fieldset.disabled);
	// 	}
	// 	// console.log(ref.current);
	// }, [disabled]);

	return (
		<As 
			{...etc} 
			ref={ref} 
			role={type} 
			aria-checked={indeterminate ? "mixed" : checked} // For checked state 
			aria-valuetext={value?.length > 0 ? value : null} // For value
			aria-disabled={disabled ? true : null} // For disabled
			aria-required={required ? true : null} // OPTION
			aria-invalid={invalid} // OPTION |  && required
			
			id={id?.length > 0 ? id : null}
			aria-label={name?.length > 0 ? name : null} // OPTION
			
			tabIndex={disabled ? "-1" : tabIndex ? tabIndex : "0"} // null 
			onKeyDown={keyDown} 
			onClick={Click} 
		/>
	);
}

/*
<React.Fragment></React.Fragment>
*/
