import React from 'react';
// import {Cn} from '../utils/Q.js';

// errorText, errorDesc, onError, src
export default function Img({
	inRef, alt, w, h, fluid, thumb, circle, round, className, frameClass, noDrag, frame, caption, captionClass, children, ...etc
}){
	/* const error = e => {
		if(onError){
			onError(e);
		}else{
			// data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' style='text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23868e96'%3E%3C/rect%3E%3Ctext x='50%25' y='50%25' fill='%23fff' dy='.3em' style='font-family:sans-serif'%3E${this.props.alt}%3C/text%3E%3C/svg%3E
			e.target.src = `data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' focusable='false' style='text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23868e96'%3E%3C/rect%3E%3Ctext x='50%25' y='45%25' fill='%23fff' dy='.3em' style='font-family:sans-serif'%3E${errorText ? errorText : alt}%3C/text%3E%3Ctext x='50%25' y='55%25' fill='%23fff' dy='.5em' style='font-size:0.7rem;font-family:sans-serif'%3E${errorDesc ? errorDesc : 'Image not found'}%3C/text%3E%3C/svg%3E`;// "/img/imgError.svg";
			return null;// FROM MDN https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onerror
		}
	} */

	const img = () => (
		<img
			{...etc}
			ref={inRef}
			alt={alt} 
			width={w} 
			height={h} 
			// src={src} 
			className={
				Cn({
					'img-fluid': fluid,
					'img-thumbnail': thumb,
					'rounded-circle': circle,
					'rounded': round
				}, className)
			}
			draggable={noDrag ? false : null}
			// style={style}
			// onError={error}
			// onLoad={load}
			//onDrag={this.noop}
		/>
	);

	if(frame){
		const bsFigure = frame === 'bs';

		return (
			<div
				className={
					Cn("img-frame", {
						'figure': bsFigure
					}, frameClass)
				}
				draggable="false"
			>
				{img()}

				{(bsFigure && caption) && <figcaption className={Cn("figure-caption", captionClass)}>{caption}</figcaption>}
				
				{children} 
			</div>
		)
	}else{
		return img();
	}
}

Img.defaultProps = {
	alt: '',
  noDrag: true
};

/* const strNum = P.oneOfType([P.string, P.number]);

Img.propTypes = {
	frame: P.bool,
  inRef: P.oneOfType([P.object, P.func, P.string]),
  alt: P.string,

  w: strNum,
  h: strNum,
	// src: P.string, // .isRequired

  fluid: P.bool,
  thumb: P.bool,
  circle: P.bool,
  round: P.bool,
  noDrag: P.bool,
	onError: P.func,
	onLoad: P.func
}; */
