import React from 'react';// , { useState }
// import { Cx } from '../../utils/Q.js';

// errorText, errorDesc, onError, src, 
export default function Img({
		// frameRef, 
    inRef,
    alt = "",
		src, // OPTION
		loading = "lazy",
		drag = false,
		// onLoad = noop,
		// label, 
		WrapAs, 
    w, h, 
		fluid, thumb, circle, round, 
		className, frameClass, 
		frame, 
		caption, captionClass, 
		onLoad, onError, 
		children, 
		...etc
}){
	// const [load, setLoad] = useState(false);
	// const [err, setErr] = useState(null);

	// const support = 'loading' in HTMLImageElement.prototype;
	// const isLazy = SUPPORT_LOADING && loading === 'lazy';
	
	const bsFigure = frame === 'bs';
	const As = bsFigure ? "figure" : WrapAs ? WrapAs : "picture";
	
	const setCx = et => {
		Q.setClass(et, "ava-loader", "remove");
		
		// if(fluid || thumb) Q.setAttr(et, "height");
	}
	
	const Load = e => {
		// Q.setClass(e.target, "ava-loader", "remove");// OPTION: remove or Not
		setCx(e.target);
		
		if(onLoad) onLoad(e);
	}
	
	// DEV: 
	const Error = e => {
		let et = e.target;
		// Q.setClass(et, "img-err");
		// Q.setAttr(et, {"aria-label":`&#xF03E;`});
		// setErr("&#xF03E;"); 
		
		setCx(et);
		
		et.src = "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='95' viewBox='0 0 32 32'%3E%3Cg%3E%3Cpath d='M29,4H3A1,1,0,0,0,2,5V27a1,1,0,0,0,1,1H29a1,1,0,0,0,1-1V5A1,1,0,0,0,29,4ZM28,26H4V6H28Z'/%3E%3Cpath d='M18.29,12.29a1,1,0,0,1,1.42,0L26,18.59V9a1,1,0,0,0-1-1H7A1,1,0,0,0,6,9v1.58l7,7Z'/%3E%3Cpath d='M7,24H25a1,1,0,0,0,1-1V21.41l-7-7-5.29,5.3a1,1,0,0,1-1.42,0L6,13.42V23A1,1,0,0,0,7,24Z'/%3E%3C/g%3E%3C/svg%3E";
	
		if(onError) onError(e);
		
		return;// null
	}

	const img = () => (
		<img 
			{...etc}
      ref={inRef} 
      loading={loading} 
			alt={alt} // OPTION
			width={w} // isLazy && !w && !load ? 40 : w
			height={h} // isLazy && !h && !load ? 40 : h
			src={Q.newURL(src).href} // OPTION: Q.isDomain(src) ? src : Q.newURL(src).href
			className={
				Q.Cx("ava-loader", { // holder thumb| ava-loader
          'img-fluid': fluid,// fluid && !isLazy
					'img-thumbnail': thumb,
					'rounded-circle': circle,
					'rounded': round,
					'figure-img': bsFigure,
					// 'fal img-err': err
				}, className)
			}
			draggable={drag} // !!drag 
			// aria-label={label || err} 
			
			onError={Error} 
			onLoad={Load} 
			
			// OPTION: 
			// onContextMenu={e => {
				// Q.preventQ(e);
			// }}
			
			/* onLoad={e => {
				if(isLazy){
					setLoad(true);
					let et = e.target;
					// setAttr(et, 'width height');
					setClass(et, 'isLoad');//  + (fluid ? ' img-fluid' : '')
				}

				onLoad(e);
			}} */
		/>
	);
	
	if(frame){
		// const bsFigure = frame === 'bs';

		return (
			<As // figure | div 
				// ref={frameRef} 
				className={
					// thumb
					Q.Cx("img-frame", {
						'figure': bsFigure
					}, frameClass)
				}
				// draggable="false" // OPTION
			>
				{img()}

				{(bsFigure && caption) && <figcaption className={Q.Cx("figure-caption", captionClass)}>{caption}</figcaption>}

				{children}
			</As>
		)
	}

    return img();
}

// Img.defaultProps = {
    // loading: 'lazy',
    // alt: '',
    // onLoad: noop
  // noDrag: true
// };

	/* const error = e => {
		if(onError){
			onError(e);
		}else{
			// data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' style='text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23868e96'%3E%3C/rect%3E%3Ctext x='50%25' y='50%25' fill='%23fff' dy='.3em' style='font-family:sans-serif'%3E${this.props.alt}%3C/text%3E%3C/svg%3E
			e.target.src = `data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' focusable='false' style='text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23868e96'%3E%3C/rect%3E%3Ctext x='50%25' y='45%25' fill='%23fff' dy='.3em' style='font-family:sans-serif'%3E${errorText ? errorText : alt}%3C/text%3E%3Ctext x='50%25' y='55%25' fill='%23fff' dy='.5em' style='font-size:0.7rem;font-family:sans-serif'%3E${errorDesc ? errorDesc : 'Image not found'}%3C/text%3E%3C/svg%3E`;// "/img/imgError.svg";
			return null;// FROM MDN https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onerror
		}
    } */

/* const strNum = P.oneOfType([P.string, P.number]);

Img.propTypes = {
	frame: P.bool,
  inRef: P.oneOfType([P.object, P.func, P.string]),
  alt: P.string,

  w: strNum,
  h: strNum,
	// src: P.string, // .isRequired

  fluid: P.bool,
  thumb: P.bool,
  circle: P.bool,
  round: P.bool,
  noDrag: P.bool,
	onError: P.func,
	onLoad: P.func
}; */
