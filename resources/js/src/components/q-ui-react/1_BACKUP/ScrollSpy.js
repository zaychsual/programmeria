import React, { useRef, useState, useEffect } from 'react';

import Flex from './Flex';

// const SECTIONS = [
//   { id: "section-1", displayName: "One" },
//   { id: "section-2", displayName: "Two" }, 
//   { id: "section-3", displayName: "Three" },
//   { id: "section-4", displayName: "Four" },
//   { id: "section-5", displayName: "Five" },
//   { id: "section-6", displayName: "Six" },
//   { id: "section-7", displayName: "Seven" },
//   { id: "section-8", displayName: "Eight" },
//   { id: "section-9", displayName: "Nine" },
//   { id: "section-10", displayName: "Ten" },
//   { id: "section-11", displayName: "Elevent" },
//   { id: "section-12", displayName: "Twelve" },
//   { id: "section-13", displayName: "Thirteen" },
//   { id: "section-14", displayName: "Fourteen" },
//   { id: "section-15", displayName: "Fifteen" },
// ];

export default function ScrollSpy({
  data, 
  active, 
  dir = "column", 
  className, 
}){
  const scrollRef = useRef(null);
  const navRef = useRef(null);
  const secObj = useRef({});// null
  const [isClick, setIsClick] = useState(false);
  const [view, setView] = useState(active || data[0]);// {} | null
  // const [doms, setDoms] = useState([]);

  useEffect(() => {
    // SECTIONS.forEach((f, i) => {
    //   const btn = document.querySelector(`[data-href="section-${f.id}"]`);
    //   // console.log('btn: ', btn);
    //   if(i === 0){
    //     setView(f);// Q.setClass(btn, "active");
    //   }
    // });
    // setView(SECTIONS[0]);
    // let contents = [];
    scrollRef.current.querySelectorAll(".contentSpy").forEach(node => {
      // contents.push(node);
      secObj.current[node.id] = node.offsetTop;// node.getBoundingClientRect().y;
    });// { node, rect: node.getBoundingClientRect() }
    // const contents = SECTIONS.map(n => {
    //   const node = document.querySelector("#section-" + n.id);
    //   return {
    //     node, 
    //     rect: node.getBoundingClientRect()
    //   }
    // });
    // console.log('%cuseEffect in ScrollSpy contents: ','color:yellow',contents);
    // setDoms(contents);
  }, [scrollRef]);

  // const inViewport = (parent, el) => {
  //   const rect = el.getBoundingClientRect();
  //   return (
  //     rect.top >= 0 && 
  //     rect.left >= 0 && 
  //     rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) // && 
  //     // rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  //   );
  // }

  return (
    <Flex dir={dir} wrap className={Q.Cx("q-scrollspy border", className)}
      style={{ height: 400 }}
    >
      <Flex ref={navRef} dir="column" style={{ width: 200 }} className="h-100 ovyauto q-scroll">
        {data.map((v, i) => 
          <button key={i} type="button" 
            className={"btn nav-link" + (view.id === v.id ? " active":"")} 
            data-href={v.id} // "section-" + 
            onClick={() => {
              setIsClick(true);// Prevent onScroll
              // isClick = false;
              const el = document.querySelector("#" + v.id);// "#section-" + v.id
              console.log('onClick el: ', el);// `[data-href="${f.id}"]`
              if(el){
                setView(v);
                // el.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });// 
                const rect = el.getBoundingClientRect();
                scrollRef.current.scrollBy({
                  top: rect.y - rect.height, // top | bottom | y | height
                  behavior: "smooth"
                });
                setTimeout(() => setIsClick(false), 750);// Activate onScroll
              }
            }}
          >
            {v.displayName}
          </button>
        )}
      </Flex>

      <Flex dir="column" className="flex-grow-1 h-100 ovyauto q-scroll"
        ref={scrollRef} 
        // onScroll={e => throttle(onScroll(e), 100)} 
        onScroll={(e) => {
          if(!isClick){
            const et = e.target;
            const { scrollTop } = et;// scrollTop, scrollHeight
            let i = 0;
            for(i in secObj.current){
              // secObj.current[i] < scrollHeight - 200
              if(secObj.current[i] <= scrollTop + 170){ //  + 150
                // document.querySelector('.active').setAttribute('class', ' ');
                // document.querySelector('a[href*=' + i + ']').setAttribute('class', 'active');
                setView({ id: i });
                // console.log('onScroll secObj: ', secObj);
                console.log('onScroll i: ', i);
                navRef.current.querySelector(`[data-href="${i}"]`).scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });// 
              }
            }
          }
        }}
      >
        {data.map((v, i) => 
          <section key={i}  
            className="contentSpy card" 
            id={v.id} // "section-" + v.id
          >
            <div className="card-body">
              <h1>{v.displayName}</h1>
              <p className="lead">{i + 1}</p>
            </div>
          </section>
        )}
      </Flex>
    </Flex>
  );
}

