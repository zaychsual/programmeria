const Select = React.forwardRef(
  ({
    options = [], 
    optionKey = {
      value: "value", 
      label: "label", 
      title: "title"
    }, 
    prefixClass = "custom-select", 
    size, 
    className, 
    children, 
    onClick = () => {}, 
    value, 
    defaultValue, 
    ...etc
  }, 
  ref
) => {
  const [once, setOnce] = useState(false);
  // const [val, setVal] = useState("");
  const myRef = ref || useRef();
  const val = value || defaultValue;

  // useEffect(() => {
  //   if(!once){

  //   }
  // }, [once]);

  const Click = e => {
    console.log('Click');
    if(!once){
      // setVal(value || defaultValue);
      setOnce(true);
      setTimeout(() => {
        console.log('myRef: ', myRef.current.options);// Q.domQall("option", myRef.current)
        const myVal = (value && value[optionKey.value]) || (defaultValue && defaultValue[optionKey.value]);
        myRef.current.options.forEach(f => {
          if(f.value === myVal){
            f.selected = true;// "";// true
            // console.log(f.getBoundingClientRect());
          }
        });

        myRef.current.scroll(0, 50);
      }, 100);
    }

    onClick(e);
  }

  // const Focus = e => {
  //   console.log('Focus');
  //   if(!once){
  //     setOnce(true);
  //     // setTimeout(() => {
  //     //   console.log('myRef: ', myRef.current.options);// Q.domQall("option", myRef.current)
  //     //   const myVal = (value && value[optionKey.value]) || (defaultValue && defaultValue[optionKey.value]);
  //     //   myRef.current.options.forEach(f => {
  //     //     if(f.value === myVal){
  //     //       f.selected = true;// "";// true
  //     //       console.log(f.getBoundingClientRect());
  //     //     }
  //     //   });
  //     // }, 300);
  //   }
  // }

  // const renderOptions = () => {
  //   return options.map((v, i) => 
  //     v.options ? 
  //       <optgroup key={i} label={v[optionKey.label]}>
  //         {v.options.map((v2, i2) => 
  //           <option key={i2} value={v2[optionKey.value]} title={v2[optionKey.title]}>{v2[optionKey.label]}</option>
  //         )}
  //       </optgroup>
  //       : 
  //       <option key={i} value={v[optionKey.value]} title={v[optionKey.title]}>{v[optionKey.label]}</option>
  //   )
  // }

  // const renderSelect = (child) => {
  //   return (
  //     <select 
  //       {...etc} 
  //       ref={ref} // myRef
  //       className={Q.Cx(prefixClass, { [prefixClass + "-" + size]: size }, className)} 
  //       onClick={Click} 
  //       value={value && value[optionKey.value]} // value[optionKey.value] | value
  //       defaultValue={(defaultValue && defaultValue[optionKey.value]) || ""} // defaultValue[optionKey.value] | defaultValue
  //     >
  //       {child}
  //     </select>
  //   );
  // }

  // if(once){
  //   return renderSelect(
  //     options.map((v, i) => 
  //     v.options ? 
  //       <optgroup key={i} label={v[optionKey.label]}>
  //         {v.options.map((v2, i2) => 
  //           <option key={i2} 
  //             value={v2[optionKey.value]} 
  //             title={v2[optionKey.title]} 
  //             // selected={defaultValue && defaultValue[optionKey.value] === v2[optionKey.value]} 
  //           >
  //             {v2[optionKey.label]}
  //           </option>
  //         )}
  //       </optgroup>
  //       : 
  //       <option key={i} 
  //         value={v[optionKey.value]} 
  //         title={v[optionKey.title]} 
  //         // selected={defaultValue && defaultValue[optionKey.value] === v[optionKey.value]} 
  //       >
  //         {v[optionKey.label]}
  //       </option>
  //     )
  //   );
  // }

  // return renderSelect(
  //   <option 
  //     value={val[optionKey.value]} 
  //     title={val[optionKey.title]}
  //   >
  //     {val[optionKey.label]}
  //   </option>
  // );

  return (
    <select 
      {...etc} 
      ref={myRef} 
      className={Q.Cx(prefixClass, { [prefixClass + "-" + size]: size }, className)} 
      onClick={Click} 
      // onFocus={Focus} 
      value={value && value[optionKey.value]} // value[optionKey.value] | value
      defaultValue={(defaultValue && defaultValue[optionKey.value]) || ""} // defaultValue[optionKey.value] | defaultValue
    >
      {/* {!once && 
        <option value={val}>{val}</option>
      } */}

      {/* {once ? children : <option value={val}>{val}</option>} */}

      {once ? 
        options.map((v, i) => 
          v.options ? 
            <optgroup key={i} label={v[optionKey.label]}>
              {v.options.map((v2, i2) => 
                <option key={i2} 
                  value={v2[optionKey.value]} 
                  title={v2[optionKey.title]} 
                  // selected={defaultValue && defaultValue[optionKey.value] === v2[optionKey.value]} 
                >
                  {v2[optionKey.label]}
                </option>
              )}
            </optgroup>
            : 
            <option key={i} 
              value={v[optionKey.value]} 
              title={v[optionKey.title]} 
              // selected={defaultValue && defaultValue[optionKey.value] === v[optionKey.value]} 
            >
              {v[optionKey.label]}
            </option>
        )
        :
        <option 
          value={val[optionKey.value]} 
          title={val[optionKey.title]}
        >
          {val[optionKey.label]}
        </option>
      }
    </select>
	);
});

<h6>{"<Select />"}</h6>
      <Select 
        options={DATA_TYPE} 
        optionKey={{
          value: "v", 
          label: "l", 
          title: "t"
        }}
        // value={select} // defaultValue 
        // onChange={e => setSelect(e.target.value)} 
        defaultValue={{
          v: "VARCHAR", 
          l: "VARCHAR", 
          t: "A variable-length (0-65,535) string, the effective maximum length is subject to the maximum row size"
        }}
      >
        {DATA_TYPE.map((v, i) => 
          v.options ? 
          <optgroup key={i} label={v.label}>
            {v.options.map((v2, i2) => 
              <option key={i2} value={v2.v} title={v2.t}>{v2.v}</option>
            )}
          </optgroup>
          : 
          <option key={i} value={v.v} title={v.t}>{v.v}</option>
        )}
      </Select>
      <hr/>