import React, { forwardRef, useRef, useState,  } from 'react';// useEffect
import { ReactSketchCanvas } from 'react-sketch-canvas';
import debounce from 'lodash/throttle';// debounce | throttle

import Btn from './Btn';
import { fileRead } from '../../utils/file/fileRead';

// let curSignature = "";// For undo

const Signature = forwardRef(
  ({
    height = 150, 
    strokeWidth = 1.5,  
    strokeColor = "black", 
    wait = 500, // 300
    ext = "png", // png | jpeg | svg
    tabIndex = 0, 
    accept = "image/png,image/jpeg", 
    // fileReader, 
    wrapClass, 
    // download, 
    name, 
    id, 
    value, 
    onChange = () => {}, 
    onBlur = () => {}, 
    ...etc
  },
  ref
) => {
  // const styles = {
  //   border: "0.0625rem solid #9c9c9c",
  //   borderRadius: "0.25rem"
  // };
  const canvasRef = ref || useRef(null);// ??
  const fileRef = useRef(null);
  const [data, setData] = useState(value);
  const [isClear, setIsClear] = useState(false);
  const [paths, setPaths] = useState([]);
  const [img, setImg] = useState({});
  const curSignature = useRef(value);

  // useEffect(() => {
  //   if(value){
  //     curSignature = value;
  //   }
  //   return () => curSignature = "";
  // }, []);// value

  // const onExport = () => {
  //   const cref = canvasRef.current;
  //   const fn = ext === "svg" ? cref.exportSvg() : cref.exportImage(ext);
  //   fn.then(data => {
  //     console.log(data);
  //   })
  //   .catch(e => {
  //     console.log(e);
  //   })
  // }

  const setVal = (value) => {
    const NAME = name ? { name } : {};
    const ID = id ? { id } : {};
    return { target: { value }, ...NAME, ...ID };
  }

  const getData = async () => {
    const cref = canvasRef.current;
    const fn = ext === "svg" ? cref.exportSvg() : cref.exportImage(ext);
    // canvasRef.current.exportImage(ext) // await 
    const resData = await fn;
    // console.log('resData: ', resData);
    return resData;
    // fn.then(res => {
    //   return res;
    // })
    // .catch(e => {
    //   console.warn(e);
    //   return null;
    // });
  }

  const onWrite = debounce(async (val) => { //  
    if(!isClear){
      // const val = canvasRef.current.getSketchingTime();
      // console.log('onWrite val: ', val);
      if(val.length){
        // const cref = canvasRef.current;
        // const fn = ext === "svg" ? cref.exportSvg() : cref.exportImage(ext);
        // // canvasRef.current.exportImage(ext) // await 
        // fn.then(res => onChange(res))
        // .catch(e => {
        //   onChange(null);
        //   console.warn(e);
        // });
        const newData = await getData();
        setPaths(val);
        onChange(setVal(newData));// { value: newData, ...getNameID() }
      }else{
        onChange(setVal(null));// { value: null, ...getNameID() }
      }
    }
  }, wait);// 300

  const Blur = async () => { // e
    // e.stopPropagation();
    const curVal = curSignature.current;
    let newData;
    if(!data && paths.length){
      newData = await getData();
    }
    else if(img.file || img.name){
      console.log('Blur else if');
      newData = img;
    }
    else{
      newData = paths.length > 0 ? await getData() : data === null ? data : curVal;
    }

    onBlur(setVal(newData));// { value: newData, ...getNameID() }
  }

  const onAddFile = (e) => {
    // if(img.name){
    //   fileRef.current.value = "";
    // }

    const val = e.target.files[0];
    console.log('onAddFile val: ', val);
    setIsClear(true);// Prevent onWrite trigger:
    if(val){
      fileRead(val, { 
        onLoad: () => {
          console.log('onLoad fileRead');
        } 
      }).then(res => {
        console.log('res: ', res);
        setImg(res);// { file: val, base64: res.result }
        // canvasRef.current.clearCanvas();
        const draw = canvasRef.current;
        if(draw) draw.clearCanvas();

        onChange(setVal(val));
      }).catch(e => {
        console.warn(e);
      });

      // const draw = canvasRef.current;
      // if(draw) draw.clearCanvas();

      // setImg(val);
      // onChange(setVal(val));// { value: val, ...getNameID() }
    }
    setTimeout(() => setIsClear(false), 9);// Prevent onWrite trigger:
  }

  const onClear = () => {
    setIsClear(true);// Prevent onWrite trigger:

    if(img.name || img.file || data){ // file.name || data
      fileRef.current.value = "";
      setImg({});
      setData(null);
    }else{
      setPaths([]);
      canvasRef.current.clearCanvas();
    }
    onChange(setVal(null));// { value: null, ...getNameID() }
    setTimeout(() => setIsClear(false), 9);// Prevent onWrite trigger:
  }

	const onReset = () => {
		fileRef.current.value = "";
		setImg({});
    setPaths([]);

    const curVal = curSignature.current;
    setData(curVal);
    onChange(setVal(curVal));// { value: curVal, ...getNameID() }
	}

  const onLoadImg = e => {
    const et = e.target;
    if(img.name){ // img.file
      window.URL.revokeObjectURL(et.src);
    }
    et.classList.remove("bg-light");
  }

  const onErrorImg = e => {
    console.log('onErrorImg e: ', e);
  }

  // console.log('curSignature: ', curSignature);
  // console.log('data: ', data);
  // console.log('value: ', value);
  // console.log('paths: ', paths);
  // console.log('canvasRef.current: ', canvasRef.current);
  // console.log('=======================================================');
  return (
    <div tabIndex={tabIndex} 
      className={Q.Cx("form-control h-auto p-0", wrapClass)} 
      onBlur={Blur} 
    >
      {(img.name || img.file || data) && 
        <img draggable={false} 
          className="w-100 of-con bg-light" // d-none-next img-fluid 
          height={height} // 150 
          src={img.name ? window.URL.createObjectURL(img) : img.file ? img.result : data} 
          onLoad={onLoadImg} 
          onError={onErrorImg} 
          alt="Signature" 
        />
      }

      {!data && 
        <ReactSketchCanvas 
          {...etc} 
          ref={canvasRef} 
          height={height} 
          strokeWidth={strokeWidth} 
          strokeColor={strokeColor} 
          className={"border-0 rounded-0" + (img.name || img.file ? " d-none":"")} 
          onUpdate={onWrite} 
        />
      }

      <div className="text-right border-top bg-light">
        {(curSignature.current && (data !== curSignature.current || !value || img.file || img.name || paths.length > 0)) && 
          <Btn onClick={onReset}  size="sm" kind="flat" className="border-0 qi qi-undo" title="Undo" tabIndex="-1" />
        }

        {(value || data || paths.length > 0) && 
          <Btn onClick={onClear} size="sm" kind="flat" className="border-0 qi qi-close xx" title="Clear" tabIndex="-1" />
        }

        {/* <Btn onClick={onClear} size="sm" kind="flat" tabIndex="-1" className="border-0 qi qi-undo" /> */}
        {/* <Btn onClick={onExport} size="sm" kind="flat">Export</Btn> */}
        <Btn As="label" size="sm" kind="flat" className="border-0 btnFile qi qi-upload" title="Upload file" tabIndex="-1">
          <input ref={fileRef} onChange={onAddFile} accept={accept} type="file" hidden 
            // name={name} id={id}
          />
        </Btn>
      </div>
    </div>
	);
});

export default Signature;

/* 
        {(download && (paths.length > 0 || data || img.name || img.file)) &&  
          <Btn onClick={onDownload} As="a" size="sm" kind="flat" className="border-0 qi qi-download" title="Download" tabIndex="-1" />
        }

const onDownload = async (e) => {
  // e.preventDefault();
  const et = e.target;

  let blob;
  let mime = ext;
  if(img.name || img.file){
    blob = img.name ? img : img.file;
    mime = blob.name.split(".").pop();
  }else{
    const file = await getData();
    // ext === "svg" ? 
    let type = "image/";
    if(ext === "svg"){
      type += "svg+xml";
    }else if(ext && ext !== "svg"){
      type += ext;
    }else if(data){
      type += data.split(".").pop();
    }

    blob = new Blob([file], { type });
  }

  // console.log('onDownload file: ', file);
  console.log('onDownload mime: ', mime);
  console.log('onDownload blob: ', blob);

  et.download = "signature." + mime;
  et.href = window.URL.createObjectURL(blob);
  setTimeout(() => window.URL.revokeObjectURL(et.href), 1);

  // import("file-saver").then(m => {
  //   if(m?.default){
  //     const file = data ? data : img.name ? img : img.file;
  //     const extn = ext ? ext : data ? data.split(".").pop() : "";
  //     console.log('onDownload file: ', file);
  //     console.log('onDownload extn: ', extn);
  //     m.default(file, "signature." + extn);
  //   }
  // }).catch(e => {
  //   console.warn(e);
  // });
}
*/