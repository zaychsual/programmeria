import React, { forwardRef, useState } from 'react';// { useState, useEffect }

import InputGroup from './InputGroup';
import Input from './Input';// InputQ
import Btn from './Btn';

// export default function Password({
const Password = forwardRef(
	({
		className, 
		// label, 
		// children, 
		...etc
	}, 
	ref
) => {
  const [see, setSee] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Password','color:yellow;');
	// }, []);

	return (
		<InputGroup
			As="label" 
			append={
				<Btn As="div" kind="light" tabIndex="0" 
					className={"tip tipTR q-fw qi qi-eye" + (see ? "-slash":"")} 
					aria-label={(see ? "Hide":"Show") + " Password"}
					onClick={() => setSee(!see)} 
				/>
			}
		>
			<Input // InputQ 
				{...etc} 
				ref={ref} 
				// label={label} // "Password" 
				type={see ? "text":"password"} 
				// name="password" 
				// required 
				// minLength={5} 
				// pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 

				// OPTION: From Gmail, toggle or default render
				// spellCheck={see ? false : undefined} 
				// autoComplete={see ? "off" : undefined} 
				// autoCapitalize={see ? "off" : undefined} 
		
				spellCheck={false} 
				autoComplete="off" 
				autoCapitalize="off" 

				// value={formik.values.password} 
				// onChange={formik.handleChange}
			/>

			{/* {children}  */}
		</InputGroup>
	);
});

export default Password;

/*
		<label 
			// ref={ref} 
			className={Q.Cx("q-input", className)} 
			// aria-label={label} 
		>
		</label>
*/
