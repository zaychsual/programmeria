import React from 'react';
import Img from './Img';
import {setClass, setAttr, noop} from '../../utils/Q';

export default function ImgLazy({
	w = 300, 
	h = 300, 
	loading = 'lazy', 
	onLoad = noop, // () => {}, 
	...etc
}){
  // const [data, setData] = React.useState();
	// const elRef = React.useRef(null);
	
	/* React.useEffect(() => {
		// console.log('%cuseEffect in ImgLazy','color:yellow;');
		// console.log(elRef);
		const ref = elRef.current;
		if(ref){
			setAttr(ref.firstElementChild, {width: ref.offsetWidth, height: h});// w, h
		}
	}, []); */

	return (
		<Img {...etc} 
			// frameRef={elRef} // inRef
			frame  
			w={w} // 
			h={h} // 
			loading={loading}  
			onLoad={e => {
				if('loading' in HTMLImageElement.prototype){// SUPPORT_LOADING
					let et = e.target;
					// console.log(et.parentElement);
					setAttr(et, 'width height');
					setClass(et, 'isLoad');//  img-fluid
				}
				
				onLoad(e);
			}}
		/>
	);

}

/*
<React.Fragment>/React.Fragment>
*/
