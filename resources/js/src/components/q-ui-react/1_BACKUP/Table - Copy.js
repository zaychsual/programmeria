import React from 'react';// { useState, useEffect }

// import ResizeSensor from './ResizeSensor';

export default function Table({
	responsive = true, 
	responsiveClass, 
	responsiveSize, // sm | md | lg | xl
	responsiveStyle, // DEV OPTION: For fixed thead
	border, // bordered
	// borderless, 
	hover, 
	strip, // striped
	// size, // "sm" | 
	sm, 
	kind, // "dark" | 
	layout, // "auto" | "fix" 

	thead,
	tbody, 
	tfoot, // OPTION
	caption, 

	className, 
	theadClass, // "thead-dark" | "thead-light" | "bg-light"
	tbodyClass, 
	tfootClass, // OPTION
	captionClass, 

	fixThead, // DEV OPTION: For fixed thead
	customScroll = true, 
}){
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Table','color:yellow;');
	// }, []);

	const Tb = () => (
		<table 
			className={
				Q.Cx("table", {
					// "table-bordered": border, 
					// "table-borderless": borderless,
					["table-border" + (border === true ? "ed" : "less")]: border, 
					"table-striped": strip, 
					"table-hover": hover,
					["table-sm"]: sm, 
					["table-" + kind]: kind, 
					["tb-" + layout]: layout
				}, className)
			}
		>
			{caption && <caption className={captionClass}>{caption}</caption>}

			{thead && <thead className={theadClass}>{thead}</thead>}

			{tbody && <tbody className={tbodyClass}>{tbody}</tbody>}

			{/* OPTION */}
			{tfoot && <tfoot className={tfootClass}>{tfoot}</tfoot>}
		</table>
	);

	// const Tbl = () => 
	// 	fixThead ? 
	// 		<ResizeSensor 
	// 			// className="" 
	// 			// wait={1000} 
	// 			onResizeEnd={(s) => {
	// 				console.log('onResizeEnd size: ', s);
	// 			}}
	// 		>
	// 			{Tb()}
	// 		</ResizeSensor>
	// 	: Tb();

	if(responsive || fixThead){
		return (
			<div 
				className={
					Q.Cx({
						["table-responsive" + (responsiveSize ? "-" + responsiveSize : "")]: responsive, 
						"ovauto thead-fix": fixThead,
						"q-scroll ovscroll-auto": customScroll
					}, responsiveClass)
				}
				style={responsiveStyle} // DEV OPTION: For fixed thead
			>
				{Tb()}
			</div>
		);
	}

	return Tb();
}

