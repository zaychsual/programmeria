import React from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import { useLocation, useHistory } from 'react-router-dom';

import Btn from './Btn';

export default function BtnRoute({ 
	to, 
	onClick = Q.noop, 
	...etc 
}){
	let location = useLocation();
	const history = useHistory();

	return (
		<Btn 
			{...etc} 
			// className={Q.Cx({"active": location.pathname === to}, className)} 
			active={location.pathname === to} 
			onClick={e => {
				history.push(to);
				onClick(e);
			}} 
		/>
	)
}
