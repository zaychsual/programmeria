import React, { forwardRef } from 'react';// , useRef, useEffect

// let disableds;

const Fieldset = forwardRef(
  ({
    disabled, 
    children, 
    onClick = () => {}, 
    onKeyDown = () => {}, 
    onTouchStart = () => {}, 
    ...etc
  }, 
  ref
) => {
  // const myRef = ref || useRef(null);

  // useEffect(() => {
  //   const EVENTS = ["click","mouseenter","keydown","contextmenu","auxclick"];
  //   const prevents = e => Q.preventQ(e);
  //   if(disabled){
  //     // disableds = 1;
  //     Q.domQall(Q.FOCUSABLE, myRef.current).forEach(n => {
  //       console.log('n: ', n);
  //       EVENTS.forEach(ev => n.addEventListener(ev, prevents));
  //     })
  //   }
  //   else{
  //     console.log('Fieldset else');
  //     Q.domQall(Q.FOCUSABLE, myRef.current).forEach(n => {
  //       EVENTS.forEach(ev => n.removeEventListener(ev, prevents));
  //     })
  //   }
  //   console.log('%cuseEffect in Fieldset: ','color:yellow');
  //   console.log('disabled: ', disabled);
  //   // console.log('disableds: ', disableds);

  //   // return () => {
  //   //   disableds = null;
  //   // }
  // }, [disabled]);

	const isDisabled = e => {
		if(disabled){
      e.preventDefault();
      return;
    }
	}

  const Click = e => {
    isDisabled(e);
    onClick(e);
  }

  const KeyDown = e => {
    isDisabled(e);
    onKeyDown(e);
  }

  const TouchStart = e => {
    isDisabled(e);
    onTouchStart(e);
  }

  return (
    <fieldset 
      {...etc} 
      ref={ref} // ref | myRef
      disabled={disabled} 
      onClick={Click} 
      onKeyDown={KeyDown} 
      onTouchStart={TouchStart}
    >
      {children}
    </fieldset>
  )
});

export default Fieldset;