import React from 'react';

/** Loops React Component with render props & auto key 
@usage: 
	<Loops 
		data={chartSetting.xAxis}
	>
		{(v, i) => (
			<li>{v.xaxisname} <Btn kind="danger" size="xs" onClick={() => this.onChartRemove('xAxis', i, v)}>X</Btn></li>
		)}
	</Loops> */

const Loops = ({data = [], children}) => React.Children.toArray(data.map((v, i) => children && children(v, i)));
export default Loops;

// function uniq(l = 3){
// 	let a = new Uint32Array(l), //  Int8Array | Uint8Array | Int16Array | Uint16Array | Int32Array | Uint32Array
// 			b = window.crypto.getRandomValues(a);
// 	return b.join("-");
// }

// const Loops = ({data = [], children}) => {
// 	return React.Children.toArray(
// 		data.map((v, i) => children && children(v, i))
// 	);
// }

// export default Loops;
