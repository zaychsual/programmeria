import React, { forwardRef, useState } from 'react';

import Input from '../Input';
import UniqueComponentId from '../../../utils/UniqueComponentId';

const InputDataList = forwardRef(
  ({
    list, 
    onFocus = () => {}, 
    options = [], 
    // notFound, 
    ...etc
  }, 
  ref
) => {
  const myList = list && options ? list : UniqueComponentId();// "list_"
  const [once, setOnce] = useState(false);

  const Focus = e => {
    if(!once && options) setOnce(true);
    onFocus(e);
  }

  return (
    <>
      <Input 
        {...etc} 
        ref={ref} 
        list={myList} 
        onFocus={Focus} 
      />

      {(once) && 
        <datalist id={myList}>
          {options.map((v, i) => 
            <option 
              key={i} 
              disabled={v.disabled} 
              label={v.label} 
              value={v.value || v} 
            />
          )}

          {/* {notFound && <option>{notFound}</option>} */}
        </datalist>
      }
    </>
  )
});

export default InputDataList;