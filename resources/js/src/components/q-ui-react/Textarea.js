import React, { forwardRef, useRef, useState, useEffect } from 'react';

// import { Cx } from '../../utils/Q';

const Textarea = forwardRef(
  ({
    h, //  = "auto"
    value, 
    defaultValue, 
    className, 
    style, 
		disabled, 
		readOnly, 
		onKeyDown = Q.noop, 
		onChange = Q.noop, 
		onCut = Q.noop, 
		onPaste = Q.noop, 
    // onKeyUp, 
		isize, 
    prevfixClass = "form-control", // bs
    ...etc
  }, 
  ref
) => {
	const elRef = ref || useRef(null);
	const [txt, setTxt] = useState("");// "" | value || defaultValue || 
	const [height, setHeight] = useState(h);
  // const [parentHeight, setParentHeight] = useState("auto");
  
  // useEffect(() => {
  //   let el = elRef.current;

  //   if(value || defaultValue){
  //     setHeight(el.scrollHeight + 2);
  //   }
  // }, [value, defaultValue]);

	useEffect(() => {
    // setParentHeight(`${elRef.current.scrollHeight}px`);
    let el = elRef.current;
    // console.log('clientHeight: ', el.clientHeight);
    // console.log('scrollHeight: ', el.scrollHeight);
		if((value || defaultValue) && el && el.clientHeight !== el.scrollHeight){ // text.length > 0 && 
      setHeight(el.scrollHeight + 2);// (el.scrollHeight + 2) + "px"
    }

    // if(txt || (value || defaultValue)){
    //   setHeight(el.scrollHeight + 2);
    // }

    // console.log('txt: ', txt);
    // console.log('value: ', value);
    // console.log('defaultValue: ', defaultValue);

	}, [txt, value, defaultValue]);
	
	const cantEdit = e => {
		if(disabled || readOnly){
			e.preventDefault();
			return;
		}
	}

	const Change = (e) => {
		cantEdit(e);
		
		setHeight(h);// null | "auto"
		// setParentHeight(`${elRef.current.scrollHeight}px`);
    setTxt(e.target.value);
    
    // setHeight(elRef.current.scrollHeight + "px");
		if(onChange) onChange(e);
  };
  
  // const Keyup = (e) => {
  //   let et = e.target;

  //   // setTimeout(() => {
  //     // et.style.height = 'auto';// ;padding:0
  //     // for box-sizing other than "content-box" use:
  //     // et.style.cssText = '-moz-box-sizing:content-box';
  //     let scrl = et.scrollHeight;

  //     // et.value.length === 0
  //     if(et.clientHeight !== scrl){//  && e.keyCode === 8
  //       // et.style.height = h; // 'height:' + h + 'px';
  //       setHeight((scrl + 2) + "px");// setHeight(h);
  //     }
  //     // else{
  //     //   // et.style.height = scrl + 'px';
  //     //   setHeight((scrl + 2) + "px");
  //     // }

  //     // et.classList.remove("ovhide");
  //   // }, 1);

  //   if(onKeyUp) onKeyUp(e);
  // }

	return (
    <textarea
      {...etc} 
      ref={elRef} 
      value={value} 
      defaultValue={defaultValue} 
			// className={Q.Cx(prevfixClass, className)} 
			className={
				Q.Cx(prevfixClass, { 
					[(prevfixClass ?? "i") + "-" + isize] : isize 
				}, className)
			} 
      style={{ ...style, height }} 
      // onKeyUp={Keyup} 
			
			disabled={disabled} 
			readOnly={readOnly} 
			onKeyDown={e => {
				const { key, ctrlKey } = e;				
				if(!ctrlKey && (key !== "c" || key !== "C")){
					cantEdit(e);
				}
				
				onKeyDown(e);
			}} 
			
      onChange={Change} 
			
			onCut={e => {
				cantEdit(e);
				onCut(e);
			}} 
			onPaste={e => {
				cantEdit(e);
				onPaste(e);
			}}
    />
  );
});

export default Textarea;


