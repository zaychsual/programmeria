import React, { useRef, useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import Observer from '@researchgate/react-intersection-observer';

// import Btn from '../../components/q-ui-react/Btn';

// function isSticky(el){
//   return getComputedStyle(el).position.match('sticky') !== null;
// }

// function fire(stuck, target){
//   const evt = new CustomEvent('sticky-change', {detail: { stuck, target } });
//   document.dispatchEvent(evt);
// }

// function observeEl(root, stickyTarget, {
// 	threshold = 1.0, 
// 	cb
// }) {
//   const observer = new IntersectionObserver((records, observer) => {
//     for (let record of records) {
//       const targetInfo = record.boundingClientRect;
//       // const stickyTarget = record.target.parentElement.querySelector('.sticky');
//       const rootBoundsInfo = record.rootBounds;

//       if (targetInfo.bottom < rootBoundsInfo.top) {
// 				// fire(true, stickyTarget);// stickyTarget | container
// 				cb(true, stickyTarget);
//       }

//       if (targetInfo.bottom >= rootBoundsInfo.top && targetInfo.bottom < rootBoundsInfo.bottom) {
// 				// fire(false, stickyTarget);// stickyTarget | container
// 				cb(false, stickyTarget);
//       }
//     }
//   }, {
// 		root,
//     rootMargin: "48px", // '-16px',
//     threshold, // [0],
//   });

//   // Add the bottom sentinels to each section and attach an observer.
//   // const sentinels = addSentinels(container, 'sticky_sentinel--top');
// 	// sentinels.forEach(el => observer.observe(el));
// 	observer.observe(stickyTarget);
// }

// function observer(el ,{
// 	root = null, 
// 	rootMargin = "48px",
// 	threshold = [0],
// 	onSticky = () => {}
// } = {}){
//   let obs;

//   // let options = {
//   //   root: null,
//   //   rootMargin: "0px",
//   //   threshold: buildThresholdList()
// 	// };
	
// 	function onIntersect(entries, observer) {
// 		entries.forEach((entry) => {
//       const targetInfo = entry.boundingClientRect;
//       // const stickyTarget = record.target.parentElement.querySelector('.sticky');
// 			const rootBoundsInfo = entry.rootBounds;
			
// 			// if (entry.intersectionRatio > prevRatio) {
// 			// 	entry.target.style.backgroundColor = increasingColor.replace("ratio", entry.intersectionRatio);
// 			// } 

//       if (targetInfo.bottom < rootBoundsInfo.top) {
// 				fire(true, el);// stickyTarget | container
// 				// onSticky(true, el);
//       }

//       if (targetInfo.bottom >= rootBoundsInfo.top && targetInfo.bottom < rootBoundsInfo.bottom) {
// 				fire(false, el);// stickyTarget | container
// 				// onSticky(false, el);
//       }
	
// 			// prevRatio = entry.intersectionRatio;
// 		});
// 	}

//   obs = new IntersectionObserver(onIntersect, { root, rootMargin, threshold });
// 	obs.observe(el);
	
// 	onSticky();
// }

// const el = document.querySelector(".myElement");
function observer(
	el, 
	onSticky = Q.noop, 
	{
		// root = null, // null | document.documentElement | document.body
		// rootMargin ="48px 0px 0px 0px", 
		options, 
		threshold = [1]
	} = {}
){
	const observ = new IntersectionObserver( 
		([e]) => onSticky(e), // e.target.classList.toggle("is-pinned", e.intersectionRatio < 1),
		{ ...options, threshold } // root, rootMargin
	);
	
	observ.observe(el);
}

export default function Sticky({
	style, 
	wrapClass, 
	className, 
	children, 
	El = "div",
	position = -1, // Must minus
	onSticky = Q.noop,
	...etc
}){
	const elRef = useRef(null);
	const [pos, setPos] = useState(null);// false
	
	useEffect(() => {
		console.log('%cuseEffect in Sticky','color:yellow;');
		const el = elRef.current;
		console.log('el: ', el);
		// if(el){
			observer(
				el, 
				(e) => {
					console.log('onSticky e: ', e);
					const data = e.intersectionRatio < 1 ? {
						...style, 
						top: position,
						// '--t': pos && position + "px", 
						'--h': Math.abs(position) + "px"
					} : null;

					setPos(data);
					onSticky({ ...data, parent: el, elm: el.firstElementChild });
				}, 
				// {
				// 	threshold: 1.0
				// }
			);
		// }

		// unmount
    return () => observer.unobserve(el);
	}, []);
	
	// useEffect(() => {
	// 	if(el){
	// 		// document.documentElement | document
	// 		observer(el, {
	// 			// root: "",
	// 			// threshold: 1.0, 
	// 			onSticky: () => {
	// 				console.log('onSticky call');
	// 				// console.log('a: ', a);
	// 				// console.log('b: ', b);

	// 				setTimeout(() => {
	// 					document.addEventListener('sticky-change', e => {
	// 						// Update sticking header title.
	// 						const [header, stuck] = [e.detail.target, e.detail.stuck];
	// 						header.classList.toggle('shadow-sm', stuck);
	// 						// const str = stuck ? header.textContent : '--';
	// 						// whoIsSticky.textContent = str;
						
	// 						// Update TOC selected item.
	// 						// allTocsItems.map(el =>  {
	// 						// 	const match = (el.firstElementChild.getAttribute('href').slice(1) ==
	// 						// 			header.firstElementChild.id);
	// 						// 	el.classList.toggle('active', match);
	// 						// });
	// 					});
	// 				}, 9);
	// 			}
	// 		});
	// 	}
	// }, []);

	// const Change = (e) => {
	// 	console.log(e);
	// 	setPos(!e?.isIntersecting);
	// }

	// <div className={Q.Cx("navbar position-sticky bg-light t48 zi-2", {"shadow-sm": see})}>
	return (
		<div 
			ref={elRef} 
			// sticky | position-sticky
			className={Q.Cx("sticky-q", {"isSticky": pos}, wrapClass)} // , {"shadow-sm": pos} 
			// className="position-relative" 
			// className={
			// 	Q.Cx(
			// 		{"position-sticky": pos}, 
			// 		className
			// 	)
			// }
			// style={{ minHeight:1 }} 
		>
			<El 
				{...etc} 
				className={className} 
				style={{
					...style, 
					...pos
					// top: position,
					// // '--t': pos && position + "px", 
					// '--h': pos && Math.abs(position) + "px"
				}}
			>
				{children(pos)}
			</El>
		</div>
	);
}

/*
{children(pos)} 

		<Observer 
			onChange={Change} 
			// root={document.documentElement} // body | documentElement
			rootMargin="48px 0px 0px 0px" 
		>
			<El 
				{...etc} 
				// ref={elRef} 
				className={Q.Cx({"position-sticky shadow-sm": pos}, className)} 
				// className="position-relative" 
				// className={
				// 	Q.Cx(
				// 		{"position-sticky": pos}, 
				// 		className
				// 	)
				// }
				// style={{ minHeight:1 }} 
			>
				{children(pos)} 
			</El>
		</Observer>

<React.Fragment></React.Fragment>
*/
