// https://github.com/primefaces/primereact/blob/master/src/components/toast/ToastMessage.js
import React, { Component, forwardRef } from 'react';
// import PropTypes from 'prop-types';

// import { classNames } from '../utils/ClassNames';
import DomHandler from '../../../utils/DomHandler';
import ObjectUtils from '../../../utils/ObjectUtils';
// import { Ripple } from '../ripple/Ripple';

class ToastMessageComponent extends Component {
  static defaultProps = {
    message: null,
    onClose: null,
    onClick: null
  }

  // static propTypes = {
  //   message: PropTypes.object,
  //   onClose: PropTypes.func,
  //   onClick: PropTypes.func
  // };

  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
    this.onClose = this.onClose.bind(this);
  }

  componentWillUnmount() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  }

  componentDidMount() {
    if (!this.props.message.sticky) {
      this.timeout = setTimeout(() => {
        this.onClose(null);
      }, this.props.message.life || 3000);
    }
  }

  onClose() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    if (this.props.onClose) {
      this.props.onClose(this.props.message);
    }
  }

  onClick(event) {
    if (this.props.onClick && !(DomHandler.hasClass(event.target, 'p-toast-icon-close') || DomHandler.hasClass(event.target, 'p-toast-icon-close-icon'))) {
      this.props.onClick(this.props.message);
    }
  }

  renderCloseIcon() {
    if (this.props.message.closable !== false) {
      return (
        <button onClick={this.onClose} className="p-toast-icon-close p-link" type="button">
          <i className="p-toast-icon-close-icon qi qi-close"></i>{/* pi pi-times */}

          {/* <Ripple /> */}
        </button>
      );
    }

    return null;
  }

  renderMessage() {
    if (this.props.message) {
      const { kind, content, summary, detail } = this.props.message;// severity
      const contentEl = ObjectUtils.getJSXElement(content, { ...this.props, onClose: this.onClose });
      let iconClassName = Q.Cx('p-toast-message-icon pi', {
        'pi-info-circle': kind === 'info',
        'pi-exclamation-triangle': kind === 'warn',
        'pi-times': kind === 'error',
        'pi-check': kind === 'success'
      });

      return contentEl || (
        <>
          <span className={iconClassName}></span>
          <div className="p-toast-message-text">
            <span className="p-toast-summary">{summary}</span>

            {detail && <div className="p-toast-detail">{detail}</div>}
          </div>
        </>
      )
    }

    return null;
  }

  render() {
    const kind = this.props.message.kind;// severity
    const className = Q.Cx('p-toast-message', {
      'p-toast-message-info': kind === 'info',
      'p-toast-message-warn': kind === 'warn',
      'p-toast-message-error': kind === 'error',
      'p-toast-message-success': kind === 'success'
    });
    const message = this.renderMessage();
    const closeIcon = this.renderCloseIcon();

    return (
      <div 
        ref={this.props.forwardRef} 
        className={className} 
        role="alert" 
        aria-live="assertive" 
        aria-atomic="true" 
        onClick={this.onClick}
      >
        <div className="p-toast-message-content">
          {message}

          {closeIcon}
        </div>
      </div>
    );
  }
}

export const ToastMessage = forwardRef((props, ref) => <ToastMessageComponent forwardRef={ref} {...props} />);
