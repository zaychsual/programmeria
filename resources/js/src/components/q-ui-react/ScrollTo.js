import React, { forwardRef, useRef, useState, useEffect } from 'react';// useEffect, useLayoutEffect
import throttle from 'lodash/throttle';

import Btn from './Btn';
import DomHandler from '../../utils/DomHandler';

export const ScrollTo = forwardRef(
  ({ 
    As = "div", 
    ChildAs = "div", 
    target = "parent", // parent | window
    className, 
    children, 

    threshold = 300, // 400
    wait = 300, 
    behavior = "smooth", // smooth | auto
    scroll = "top", // top | down
    btnProps, 
    id, 
    childWrapClass, 
    ...etc
  }, 
  ref
) => {
  const ID = id || "scroll-" + Q.Qid(3);
  const scrollRef = useRef();
  const [once, setOnce] = useState(false);
  const [show, setShow] = useState(false);

  useEffect(() => {
    // console.log('%cuseEffect in ScrollTo', 'color:yellow');

    const scrollEL = target === "parent" ? Q.domQ("#" + ID) : window;
    scrollRef.current = scrollEL;

    if(scroll === "down"){
      scrollEL.scroll({
        top: target === "parent" ? scrollEL.scrollHeight : document.body.offsetHeight
      });
    }

    const fnScroll = () => {
      // console.log('fnScroll');

      const st = target === "parent" ? scrollEL.scrollTop : DomHandler.getWindowScrollTop();
      const isShow = scroll === "top" ? st > threshold : st < threshold;
      if(!once && isShow){
        setOnce(true);
      }

      setShow(isShow);
    }

    scrollEL.addEventListener('scroll', throttle(fnScroll, wait));

    return () => {
      scrollEL.removeEventListener('scroll', throttle(fnScroll, wait));
    }
  }, [target, threshold, scroll]);// once, 

  const onClickBtn = () => {
    const pos = target === "parent" ? scrollRef.current.scrollHeight : document.body.offsetHeight;
    // console.log('pos: ', pos);

    scrollRef.current.scroll({
      top: scroll === "top" ? 0 : pos, 
      behavior
    });
  }

  const renderBtn = () => once && (
    <Btn 
      {...btnProps} 
      hidden={!show} 
      onClick={onClickBtn} 
    />
  );

  if(target === "window"){
    return renderBtn();
  }

  return (
    <As 
      {...etc} 
      ref={ref} 
      id={ID} 
      className={Q.Cx("q-scrollto", className)} 
    >
      <div className={childWrapClass}>
        {children}
      </div>

      {renderBtn()}
    </As>
  )
});
