import React, { useMemo } from 'react';
import useResizeObserver from 'use-resize-observer';
import debounce from 'lodash/debounce';// lodash-es/debounce

export default function ResizeSensor({
	className, 
  children, 
  wait = 500, 
  onResizeEnd = Q.noop // () => {} // onResizeEnd
}){
  const onResize = useMemo(() => debounce(({ width, height }) => {
    // console.log('RowWrap onResize width: ', width);
    onResizeEnd({ width, height });
  }, wait, { leading: true, trailing: false }), [wait, onResizeEnd]);

  // const onResize = useMemo(() => ({ width, height }) => {
  // 	console.log('RowWrap onResize width: ', width);
  //   onResizeEnd({ width, height });
  // }, [onResizeEnd]);

  const { ref } = useResizeObserver({ onResize });

  return <div ref={ref} className={className}>{children}</div>;
}

