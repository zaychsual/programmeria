import React from 'react';

// import BgImage from './BgImage';
// import { Cx } from '../../utils/Q';

export default function SpinLoader({
	bg, // = "/icon/android-icon-36x36.png"
	stroke, 
	fill, 
	className, 
	id, 
	style, 
	label, 
	// tab, 
	circleClass, 
	appendChild, 
	children, 
	w = 86, 
	h = 86, 
	sw = 4, 
	tip = "Loading...", 
}){
	// const bgIco = bg ? {backgroundImage:`url(${bg})`} : null;

	return (
			<div
				className={				
					Q.Cx("load-spin",{
						// "fix" : fix,
						"withFill" : fill,
						"withStroke" : stroke,
						//"full-wh" : full
					}, className)
				}
				id={id}
				style={{
					...style, 
					backgroundImage: bg ? `url(${bg})` : null
				}} // ...bgIco
				role="status" // progressbar
				// aria-hidden 
				aria-label={label || tip} // label ? label : tip
				title={tip} 
				// tabIndex={tab} 
			>
				{appendChild}
			
				<svg className="svg-spin"
					width={w}
					height={h}
					viewBox="0 0 44 44"
				>
					<circle 
						className={circleClass}
						fill={fill} 
						cx="22" 
						cy="22" 
						r="20" 
						stroke={stroke}
						strokeWidth={sw} 
					/>
				</svg>
				
				{children}
			</div>
	);
}

/* SpinLoader.defaultProps = {
	w: 86,
	h: 86,
	sw: 4,
	tip: "Loading...",
	// tab: "-1"
}; */

/*
<React.Fragment>
*/
