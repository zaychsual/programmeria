import React, { useState } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

// import { Drop } from './Drop';
// import { Drag } from './Drag';
import { Dnd } from './Dnd';
import { ItemTypes } from './ItemTypes';

// import Btn from '../Btn';
import { arrMove, addInto } from '../../../utils/collection-q';

const LIST_1 = [
	{ label:"Home", href:"/home" }, 
	{ label:"About Us", href:"/about-us" },
	{ label:"Contact Us", href:"/contact-us" },
	{ label:"Gallery", href:"/gallery" },
	{ label:"Projects", href:"/projects" },
	{ label:"Help", href:"/help" },
];

// const LIST_2 = [
// 	{ label:"Settings", href:"/settings" }, 
// 	{ label:"Login", href:"/login" },
// 	{ label:"Register", href:"/register" },
// 	{ label:"Forgot password", href:"/forgot-password" },
// 	{ label:"Development", href:"/development" },
// 	{ label:"Admin", href:"/admin" },
// ];

const swap = (arr, i1, i2) => {
	let aux = "";
	arr = [...arr];

	aux = arr[i1];
	arr[i1] = arr[i2];
  arr[i2] = aux;
  return arr;
}

export default function Sorter({
	type = ItemTypes.BOX, 
	mode = "copy", // copy | move
}){
  const [list1, setList1] = useState(LIST_1);
	// const [list2, setList2] = useState(LIST_2);
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Sorter','color:yellow;');
	// }, []);

	return (
		<div>
			<div className="list-group ovyauto border p-2 mb-3" style={{ height: 250 }}>
				{list1.map((v, i) => 
					<Dnd key={i} // v.href
						type={type} 
						data={v} 
						// index={i} 
						dragProps={{
							className: "btn btn-light flexno mr-2 qi qi-link", 
							title: "Drag" 
						}}
						cursor={mode} // copy | move
						onDropEnd={(data) => { // item, dropData
							// console.log('onDropEnd list1 data: ', data);
							// console.log('onDropEnd list1 dropData: ', dropData);
							// const val = mode === "copy" ? addInto(list1, i, data) : arrMove(list1, );
							const dataIndex = list1.findIndex((f, i2) => f.href === data.href);// i2 === i
							const swapRes = swap(list1, dataIndex, i);
							console.log('onDropEnd list1: ', list1);
							console.log('onDropEnd list1 dataIndex: ', dataIndex);
							console.log('onDropEnd list1 swapRes: ', swapRes);
							// let newList = [...list];

							setList1(arrMove(list1, i, dataIndex));// swapRes
						}}
						// onDragEnd={(item, dropData) => {
						// 	console.log('onDragEnd list1 item: ', item);
						// 	console.log('onDragEnd list1 dropData: ', dropData);
						// 	// setList1(list1.filter((f, i2) => i2 !== i));
						// }}
						className="list-group-item list-group-item-action d-flex align-items-center" // justify-content-between 
					>
						{v.label}
					</Dnd>
				)}
			</div>

			{/* <div className="list-group ovyauto border p-2" style={{ height: 200 }}>
				{list2.map((v, i) => 
					<Dnd key={i} // v.href
						type={type} 
						data={v} 
						// index={i} 
						dragProps={{
							className: "btn btn-light flexno mr-2 qi qi-cog", 
							title: "Drag" 
						}}
						cursor={mode} // copy | move
						onDropEnd={(data) => { // item, dropData
							console.log('onDropEnd list2 data: ', data);
							// console.log('onDropEnd list2 dropData: ', dropData);
							setList2(addInto(list2, i, data));
						}}
						onDragEnd={(item, dropData) => {
							console.log('onDragEnd list2 item: ', item);
							console.log('onDragEnd list2 dropData: ', dropData);
							setList2(list2.filter((f, i2) => i2 !== i));
						}}
						className="list-group-item list-group-item-action d-flex align-items-center"
					>
						{v.label}
					</Dnd>
				)}
			</div> */}

			{/* <div className="mt-3">
				<Drag 
					// As={Btn} 
					className="btn btn-primary" 
					data={{
						value: "OKEH"
					}}
				>
					Drag Me
				</Drag>
			</div> */}
		</div>
	);
}

/*
				{Array.from({length:15}).map((v, i) => 
					<Dnd key={i} 
						className="list-group-item list-group-item-action" 
					>
						Drop : {i + 1}
					</Dnd>
				)}

				<Drop greedy={true}>
					<Drop greedy={true}>
						<Drop greedy={true}/>
					</Drop>
				</Drop>

				<Drop>
					<Drop>
						<Drop />
					</Drop>
				</Drop>
*/
