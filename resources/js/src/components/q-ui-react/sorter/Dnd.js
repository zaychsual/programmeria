import React from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import { useDrop, useDrag } from 'react-dnd';

// import Btn from '../../components/q-ui-react/Btn';
// import { ItemTypes } from './ItemTypes';

// default 
export function Dnd({
	className, 
	// prefixClass = "btn-group", 
	type = "", // ItemTypes.BOX, 
	options = {}, 
	data = {}, 
	dragProps = {}, 
	cursor = "copy", // copy | move
	onDropEnd = () => {}, 
	onDragEnd = () => {}, 
	children, 
	...etc
}){
	// const [dropTarget, setDropTarget] = useState();

  // collected, drag, dragPreview
  const [{ isDragging }, drag] = useDrag(() => ({ 
    type, 
    item: data, 
    end(item, monitor){
			const dropResult = monitor.getDropResult();
			// console.log('end item: ', item);
			// console.log('end dropResult: ', dropResult);
			onDragEnd(item, dropResult);
			// setDropTarget({ item, dropResult });
    }, 
		options: {
			...options, 
			effectAllowed: cursor, 
			dropEffect: cursor
		}, 
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
      // isOverCurrent: monitor.isOver({ shallow: true }),
    }),
    // isDragging: (monitor) =>{
    //   console.log('isDragging monitor: ', monitor);
    //   return {
    //     isDragging: monitor.isDragging()
    //   }
    // }
	}));
  // }), [setDropTarget]); // setHasDrop, setHasDroppedOnChild, greedy, 

  // { isOver, isOverCurrent }
  const [props, drop] = useDrop(() => ({
    accept: type, // ItemTypes.BOX,
    drop(item, monitor) {
      const didDrop = monitor.didDrop();
      if(didDrop){ //  && !greedy
        return;
      }
      // setHasDrop(true);
      // setHasDroppedOnChild(didDrop);
      // console.log('drop monitor: ', monitor.get);
			// const dropResult = monitor.getDropResult();
			onDropEnd(item);// { item, dropResult }
    },
		// options: etc, // data, 
		// drop: (data) => (data), // { data }
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      // isOverCurrent: monitor.isOver({ shallow: true }),
			// connectDropTarget: connect.dropTarget(),
			canDrop: monitor.canDrop()
    }),
		// collect: monitor => monitor
	}));
  // }), [dropTarget]); // setHasDrop, setHasDroppedOnChild, greedy, 

	// console.log('props: ', props);

	return (
		<div 
			{...etc}
			ref={drop} 
			className={Q.Cx("q-dnd", className, { "active isOver": props.isOver })} // prefixClass
			// data-index={etc?.index || undefined} 
		>
			<div 
				{...dragProps} 
				ref={drag}
				className={Q.Cx(dragProps.className, { "isDrag": isDragging })} 
			/>

			{children}
		</div>
	);
}

/*
	const text = greedy ? 'greedy' : 'not greedy';

	let backgroundColor = 'rgba(0, 0, 0, .5)';

	if (isOverCurrent || (isOver && greedy)) {
		backgroundColor = 'darkgreen';
	}

	// useEffect(() => {
	// 	console.log('%cuseEffect in Dnd','color:yellow;');
	// }, []);
*/
