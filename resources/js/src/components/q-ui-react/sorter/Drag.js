import React from 'react';
import { useDrag } from 'react-dnd';

import { ItemTypes } from './ItemTypes';

// const style = {
//   display: 'inline-block',
//   border: '1px dashed gray',
//   padding: '0.5rem 1rem',
//   backgroundColor: '#fff',
//   cursor: 'move'
// };

export const Drag = ({
  // item = {}, 
  data = {}, 
  type = ItemTypes.BOX, 
  As = "div", 
  className, 
  ...etc
}) => {
  // collected, drag, dragPreview
  const [{ isDragging }, drag] = useDrag(() => ({ 
    type, 
    item: data, 
    end(item, monitor){
      console.log('end item: ', item);
      console.log('end monitor: ', monitor);
    }, 
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
      // isOverCurrent: monitor.isOver({ shallow: true }),
    }),
    // isDragging: (monitor) =>{
    //   console.log('isDragging monitor: ', monitor);
    //   return {
    //     isDragging: monitor.isDragging()
    //   }
    // }
  }));

  // console.log('collected: ', collected);

  return (
    <As 
      {...etc} 
      // {...collected} 

      ref={drag} 
      className={Q.Cx(className, { "isDrag": isDragging })} 
    />
  );

  // return collected.isDragging ? 
  //   <As 
  //     {...etc} 
  //     ref={dragPreview} 
  //     className={Q.Cx("isDrag", className)} 
  //   />
  //   : 
  //   <As 
  //     {...etc} 
  //     ref={drag} 
  //     {...collected} 

  //     className={className} 
  //   />
};

/*
return (
    <As 
      {...etc} 
      ref={drag} 
      // style={style}
    />
  );
*/
