import React from 'react';// , { useState }
import { useDrop } from 'react-dnd';

import { ItemTypes } from './ItemTypes';

export const Drop = ({ 
  type = ItemTypes.BOX, 
  // greedy, 
  As = "div", 
  className, 
  children 
}) => {
  // const [hasDrop, setHasDrop] = useState(false);
  // const [hasDroppedOnChild, setHasDroppedOnChild] = useState(false);

  // , isOverCurrent
  const [{ isOver }, drop] = useDrop(() => ({
    accept: type, // ItemTypes.BOX,
    drop(item, monitor) {
      const didDrop = monitor.didDrop();
      if(didDrop){ //  && !greedy
        return;
      }
      // setHasDrop(true);
      // setHasDroppedOnChild(didDrop);
      console.log('drop item: ', item);
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      // isOverCurrent: monitor.isOver({ shallow: true }),
    }),
  }), []); // setHasDrop, setHasDroppedOnChild, greedy, 
  
  // let backgroundColor = 'rgba(0, 0, 0, .5)';
  // if (isOverCurrent || (isOver && greedy)) {
  //   backgroundColor = 'darkgreen';
  // }

  return (
    <As 
      ref={drop} 
      // style={getStyle(backgroundColor)}
      className={Q.Cx(className, { "isOver": isOver })}
    >
      {/* {greedy ? 'greedy' : 'not greedy'} */}
      {/* {hasDropped && <span>dropped {hasDroppedOnChild && ' on child'}</span>} */}

      {children}
    </As>
  );
};

// function getStyle(backgroundColor){
//   return {
//     backgroundColor,
//     border: '1px solid rgba(0,0,0,0.2)',
//     minHeight: '8rem',
//     minWidth: '8rem',
//     color: '#fff',
//     padding: '2rem',
//     paddingTop: '1rem',
//     margin: '1rem',
//     textAlign: 'center',
//     float: 'left',
//     fontSize: '1rem',
//   };
// }
