import React from "react";// * as React
// import classNames from "classnames";
// import { polyfill } from "react-lifecycles-compat";

import { AbstractPureComponent2, Boundary, Classes, Position, removeNonHTMLProps } from "../../common";// Props
import { Menu } from "../menu/menu";
import { MenuItem } from "../menu/menuItem";
import { OverflowList } from "../overflow-list/overflowList";// OverflowListProps, 
import { Popover } from "../popover/popover";// IPopoverProps, 
import { Breadcrumb } from "./breadcrumb";// , BreadcrumbProps

export class Breadcrumbs extends AbstractPureComponent2 {
    constructor() {
        super(...arguments);
        this.renderOverflow = (items) => {
            const { collapseFrom } = this.props;
            const position = collapseFrom === Boundary.END ? Position.BOTTOM_RIGHT : Position.BOTTOM_LEFT;
            let orderedItems = items;
            if (collapseFrom === Boundary.START) {
                // If we're collapsing from the start, the menu should be read from the bottom to the
                // top, continuing with the breadcrumbs to the right. Since this means the first
                // breadcrumb in the props must be the last in the menu, we need to reverse the overlow
                // order.
                orderedItems = items.slice().reverse();
            }
            /* eslint-disable deprecation/deprecation */
            return (React.createElement("li", null,
                React.createElement(Popover, Object.assign({ position: position, disabled: orderedItems.length === 0, content: React.createElement(Menu, null, orderedItems.map(this.renderOverflowBreadcrumb)) }, this.props.popoverProps),
                    React.createElement("span", { className: Classes.BREADCRUMBS_COLLAPSED }))));
            /* eslint-enable deprecation/deprecation */
        };
        this.renderOverflowBreadcrumb = (props, index) => {
            const isClickable = props.href != null || props.onClick != null;
            const htmlProps = removeNonHTMLProps(props);
            return React.createElement(MenuItem, Object.assign({ disabled: !isClickable }, htmlProps, { text: props.text, key: index }));
        };
        this.renderBreadcrumbWrapper = (props, index) => {
            const isCurrent = this.props.items[this.props.items.length - 1] === props;
            return React.createElement("li", { key: index }, this.renderBreadcrumb(props, isCurrent));
        };
    }

    renderBreadcrumb(props, isCurrent) {
        if (isCurrent && this.props.currentBreadcrumbRenderer != null) {
            return this.props.currentBreadcrumbRenderer(props);
        }
        else if (this.props.breadcrumbRenderer != null) {
            return this.props.breadcrumbRenderer(props);
        }
        else {
            // allow user to override 'current' prop
            return React.createElement(Breadcrumb, Object.assign({ current: isCurrent }, props));
        }
    }

    render() {
        const { className, collapseFrom, items, minVisibleItems, overflowListProps = {} } = this.props;
        // return (React.createElement(OverflowList, Object.assign({ collapseFrom: collapseFrom, minVisibleItems: minVisibleItems, tagName: "ul" }, overflowListProps, { className: classNames(Classes.BREADCRUMBS, overflowListProps.className, className), items: items, overflowRenderer: this.renderOverflow, visibleItemRenderer: this.renderBreadcrumbWrapper })));
    
        return (
            <OverflowList
                collapseFrom={collapseFrom}
                minVisibleItems={minVisibleItems}
                tagName="ul"
                {...overflowListProps}
                // className={classNames(Classes.BREADCRUMBS, overflowListProps.className, className)}
                className={Q.Cx(Classes.BREADCRUMBS, overflowListProps.className, className)} 
                items={items} 
                overflowRenderer={this.renderOverflow}
                visibleItemRenderer={this.renderBreadcrumbWrapper}
            />
        );
    }
}

Breadcrumbs.defaultProps = {
    collapseFrom: Boundary.START,
};


