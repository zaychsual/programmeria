import React from "react";// * as React
// import classNames from "classnames";

import * as Classes from "../../common/classes";
// import { ActionProps, LinkProps } from "../../common/props";
import { Icon } from "../icon/icon";

export const Breadcrumb = breadcrumbProps => {
    const classes = Q.Cx(
        Classes.BREADCRUMB,
        {
            [Classes.BREADCRUMB_CURRENT]: breadcrumbProps.current,
            [Classes.DISABLED]: breadcrumbProps.disabled,
        },
        breadcrumbProps.className,
    );

    const icon = breadcrumbProps.icon != null ? <Icon icon={breadcrumbProps.icon} /> : undefined;

    if (breadcrumbProps.href == null && breadcrumbProps.onClick == null) {
        return (
            <span className={classes}>
                {icon}
                {breadcrumbProps.text}
                {breadcrumbProps.children}
            </span>
        );
    }
    return (
        <a
            className={classes}
            href={breadcrumbProps.href}
            onClick={breadcrumbProps.disabled ? undefined : breadcrumbProps.onClick}
            tabIndex={breadcrumbProps.disabled ? undefined : 0}
            target={breadcrumbProps.target}
        >
            {icon}
            {breadcrumbProps.text}
            {breadcrumbProps.children}
        </a>
    );
};
