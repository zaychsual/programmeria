import React from "react";// * as React
import { findDOMNode } from "react-dom";
// import { polyfill } from "react-lifecycles-compat";
// import ResizeObserver from "resize-observer-polyfill";

import { AbstractPureComponent2 } from "../../utils/blueprint/abstractPureComponent2";
// import { DISPLAYNAME_PREFIX } from "../../common/props";

const DISPLAYNAME_PREFIX = "Programmeria";

// eslint-disable-next-line deprecation/deprecation
/** `ResizeSensor` requires a single DOM element child and will error otherwise. */
// @polyfill
export default class ResizeSensor extends AbstractPureComponent2 {
    static displayName = `${DISPLAYNAME_PREFIX}.ResizeSensor`;

    constructor(props){
        super(props); // ...arguments
        this.element = null;
        // this.observer = new ResizeObserver(entries => { var _a, _b; return (_b = (_a = this.props).onResize) === null || _b === void 0 ? void 0 : _b.call(_a, entries); });
        this.observer = new ResizeObserver(entries => props.onResize?.(entries));
    }

    componentDidMount() {
        this.observeElement();
    }

    componentDidUpdate(prevProps) {
        this.observeElement(this.props.observeParents !== prevProps.observeParents);
    }

    componentWillUnmount() {
        this.observer.disconnect();
    }

    /** Observe the DOM element, if defined and different from the currently
     * observed element. Pass `force` argument to skip element checks and always
     * re-observe */
     observeElement(force = false) {
        const element = this.getElement();
        if (!(element instanceof Element)) {
            // stop everything if not defined
            this.observer.disconnect();
            return;
        }

        if (element === this.element && !force) {
            return; // quit if given same element -- nothing to update (unless forced)
        } else {
            this.observer.disconnect(); // clear observer list if new element
            this.element = element; // remember element reference for next time
        }

        // observer callback is invoked immediately when observing new elements
        this.observer.observe(element);

        if (this.props.observeParents) {
            let parent = element.parentElement;
            while (parent != null) {
                this.observer.observe(parent);
                parent = parent.parentElement;
            }
        }
    }

    getElement() {
        try {
            // using findDOMNode for two reasons:
            // 1. cloning to insert a ref is unwieldy and not performant.
            // 2. ensure that we resolve to an actual DOM node (instead of any JSX ref instance).
            // HACKHACK: see https://github.com/palantir/blueprint/issues/3979
            /* eslint-disable-next-line react/no-find-dom-node */
            return findDOMNode(this);
        } catch {
            // swallow error if findDOMNode is run on unmounted component.
            return null;
        }
    }

    render() {
        // pass-through render of single child
        return React.Children.only(this.props.children);
    }
}
