import React, { Component } from 'react';
// import PropTypes from 'prop-types';

// import { classNames } from '../utils/ClassNames';
import UniqueComponentId from '../../utils/UniqueComponentId';
import DomHandler from '../../utils/DomHandler';

export class ResizePanel extends Component {
  static defaultProps = {
    As: "div", 
    size: null,
    minSize: null,
    style: null,
    className: null
  }

  // static propTypes = {
  //   header: PropTypes.number,
  //   minSize: PropTypes.number,
  //   style: PropTypes.object,
  //   className: PropTypes.string
  // }
}

export class Resizer extends Component {
  static defaultProps = {
    id: null,
    className: null,
    style: null,
    layout: 'horizontal',
    gutterSize: 4,
    stateKey: null,
    stateStorage: 'session',
    onResizeEnd: null
  }

  // static propTypes = {
  //   id: PropTypes.string,
  //   className: PropTypes.string,
  //   style: PropTypes.object,
  //   layout: PropTypes.string,
  //   gutterSize: PropTypes.number,
  //   stateKey: PropTypes.string,
  //   stateStorage: PropTypes.string,
  //   onResizeEnd: PropTypes.func
  // };

  constructor(props){
    super(props);
    this.id = props.id || UniqueComponentId();// this.props.id || UniqueComponentId()
  }

  bindMouseListeners() {
    if (!this.mouseMoveListener) {
      this.mouseMoveListener = event => this.onResize(event)
      document.addEventListener('mousemove', this.mouseMoveListener);
    }

    if (!this.mouseUpListener) {
      this.mouseUpListener = event => {
        this.onResizeEnd(event);
        this.unbindMouseListeners();
      }
      document.addEventListener('mouseup', this.mouseUpListener);
    }
  }

  validateResize(newPrevPanelSize, newNextPanelSize) {
    if (this.props.children[0].props && this.props.children[0].props.minSize && this.props.children[0].props.minSize > newPrevPanelSize) {
      return false;
    }

    if (this.props.children[1].props && this.props.children[1].props.minSize && this.props.children[1].props.minSize > newNextPanelSize) {
      return false;
    }

    return true;
  }

  unbindMouseListeners() {
    if (this.mouseMoveListener) {
      document.removeEventListener('mousemove', this.mouseMoveListener);
      this.mouseMoveListener = null;
    }

    if (this.mouseUpListener) {
      document.removeEventListener('mouseup', this.mouseUpListener);
      this.mouseUpListener = null;
    }
  }

  clear() {
    this.dragging = false;
    this.size = null;
    this.startPos = null;
    this.prevPanelElement = null;
    this.nextPanelElement = null;
    this.prevPanelSize = null;
    this.nextPanelSize = null;
    this.gutterElement = null;
    this.prevPanelIndex = null;
  }

  isStateful() {
    return this.props.stateKey != null;
  }

  getStorage() {
    const { stateStorage } = this.props;
    switch (stateStorage) {
      case 'local':
        return window.localStorage;
      case 'session':
        return window.sessionStorage;
      default:
        throw new Error(stateStorage + ' is not a valid value for the state storage, supported values are "local" and "session".');
    }
  }

  saveState() {
    this.getStorage().setItem(this.props.stateKey, JSON.stringify(this.panelSizes));
  }

  restoreState() {
    const storage = this.getStorage();
    const stateString = storage.getItem(this.props.stateKey);

    if (stateString) {
      this.panelSizes = JSON.parse(stateString);
      let children = [...this.container.children].filter(child => DomHandler.hasClass(child, 'p-splitter-panel'));
      children.forEach((child, i) => {
        child.style.flexBasis = 'calc(' + this.panelSizes[i] + '% - ' + ((this.props.children.length - 1) * this.props.gutterSize) + 'px)';
      });

      return true;
    }

    return false;
  }

  onResizeStart(e, index) {
    this.gutterElement = e.currentTarget;
    this.size = this.horizontal ? DomHandler.getWidth(this.container) : DomHandler.getHeight(this.container);
    this.dragging = true;
    this.startPos = this.props.layout === 'horizontal' ? e.pageX : e.pageY;
    this.prevPanelElement = this.gutterElement.previousElementSibling;
    this.nextPanelElement = this.gutterElement.nextElementSibling;
    this.prevPanelSize = 100 * (this.props.layout === 'horizontal' ? DomHandler.getOuterWidth(this.prevPanelElement, true) : DomHandler.getOuterHeight(this.prevPanelElement, true)) / this.size;
    this.nextPanelSize = 100 * (this.props.layout === 'horizontal' ? DomHandler.getOuterWidth(this.nextPanelElement, true) : DomHandler.getOuterHeight(this.nextPanelElement, true)) / this.size;
    this.prevPanelIndex = index;
    DomHandler.addClass(this.gutterElement, 'p-splitter-gutter-resizing');
    DomHandler.addClass(this.container, 'p-splitter-resizing');
  }

  onResize(event) {
    let newPos;
    if (this.props.layout === 'horizontal'){
      newPos = (event.pageX * 100 / this.size) - (this.startPos * 100 / this.size);
    }else{
      newPos = (event.pageY * 100 / this.size) - (this.startPos * 100 / this.size);
    }

    let newPrevPanelSize = this.prevPanelSize + newPos;
    let newNextPanelSize = this.nextPanelSize - newPos;

    if (this.validateResize(newPrevPanelSize, newNextPanelSize)) {
      this.prevPanelElement.style.flexBasis = 'calc(' + newPrevPanelSize + '% - ' + ((this.props.children.length - 1) * this.props.gutterSize) + 'px)';
      this.nextPanelElement.style.flexBasis = 'calc(' + newNextPanelSize + '% - ' + ((this.props.children.length - 1) * this.props.gutterSize) + 'px)';
      this.panelSizes[this.prevPanelIndex] = newPrevPanelSize;
      this.panelSizes[this.prevPanelIndex + 1] = newNextPanelSize;
    }
  }

  onResizeEnd(e) {
    if (this.isStateful()) {
      this.saveState();
    }

    if (this.props.onResizeEnd) {
      this.props.onResizeEnd({
        originalEvent: e,
        sizes: this.panelSizes
      })
    }

    DomHandler.removeClass(this.gutterElement, 'p-splitter-gutter-resizing');
    DomHandler.removeClass(this.container, 'p-splitter-resizing');
    this.clear();
  }

  onGutterMouseDown(e, index) {
    this.onResizeStart(e, index);
    this.bindMouseListeners();
  }

  onGutterTouchStart(e, index) {
    this.onResizeStart(e, index);

    this.windowTouchMoveListener = this.onGutterTouchMove.bind(this);
    this.windowTouchEndListener = this.onGutterTouchEnd.bind(this);
    window.addEventListener('touchmove', this.windowTouchMoveListener, { passive: false, cancelable: false });
    window.addEventListener('touchend', this.windowTouchEndListener);
  }

  onGutterTouchMove(e) {
    this.onResize(e);
  }

  onGutterTouchEnd(e) {
    this.onResizeEnd(e);

    window.removeEventListener('touchmove', this.windowTouchMoveListener);
    window.removeEventListener('touchend', this.windowTouchEndListener);
    this.windowTouchMoveListener = null;
    this.windowTouchEndListener = null;
  }

  componentDidMount() {
    if (this.panelElement) {
      if (this.panelElement.childNodes && DomHandler.find(this.panelElement, '.p-splitter')) {
        DomHandler.addClass(this.panelElement, 'p-splitter-panel-nested');
      }
    }

    if (this.props.children && this.props.children.length) {
      let initialized = false;
      if (this.isStateful()) {
        initialized = this.restoreState();
      }

      if (!initialized) {
        let children = [...this.container.children].filter(child => DomHandler.hasClass(child, 'p-splitter-panel'));
        let _panelSizes = [];

        this.props.children.map((panel, i) => {
          let panelInitialSize = panel.props && panel.props.size ? panel.props.size : null;
          let panelSize = panelInitialSize || (100 / this.props.children.length);
          _panelSizes[i] = panelSize;
          children[i].style.flexBasis = 'calc(' + panelSize + '% - ' + ((this.props.children.length - 1) * this.props.gutterSize) + 'px)';
          return _panelSizes;
        });

        this.panelSizes = _panelSizes;
      }
    }
  }

  renderPanel(panel, index) {
    const { As, className, style, children } = panel.props;
    const { layout, gutterSize } = this.props;
    const gutterStyle = layout === 'horizontal' ? { width: gutterSize + 'px' } : { height: gutterSize + 'px' }
    const gutter = (index !== this.props.children.length - 1) && (
      <div 
        ref={(el) => this.gutterElement = el} 
        className={'p-splitter-gutter'} 
        style={gutterStyle} 
        onMouseDown={e => this.onGutterMouseDown(e, index)}
        onTouchStart={e => this.onGutterTouchStart(e, index)} 
        onTouchMove={e => this.onGutterTouchMove(e)} 
        onTouchEnd={e => this.onGutterTouchEnd(e)}
      >
        <div className="p-splitter-gutter-handle"></div>
      </div>
    );
    
    return (
      <React.Fragment>
        <As // div 
          ref={(el) => this.panelElement = el} 
          key={index} 
          className={Q.Cx('p-splitter-panel', className)} 
          style={style}
        >
          {children}
        </As>

        {gutter}
      </React.Fragment>
    );
  }

  // renderPanels() {
  //   return (
  //     React.Children.map(this.props.children, (panel, index) => {
  //       return this.renderPanel(panel, index);
  //     })
  //   )
  // }

  render() {
    const { layout, className, style, children } = this.props;
    // const className = Q.Cx("p-splitter p-component p-splitter-" + layout, className);
    // const panels = this.renderPanels();

    return (
      <div 
        ref={(el) => this.container = el} 
        id={this.id} 
        className={Q.Cx("p-splitter p-component p-splitter-" + layout, className)} // className
        style={style}
      >
        {/* panels */}

        {React.Children.map(children, (panel, index) => {
          return this.renderPanel(panel, index);
        })}
      </div>
    );
  }
}

