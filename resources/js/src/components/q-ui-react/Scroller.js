import React, { useRef, useState, useLayoutEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import { useInViewport } from 'ahooks';

import Flex from './Flex';
import Btn from './Btn';

// const scrollOptions = {
// 	top: 0,
// 	left: 0, 
// 	behavior: "smooth"
// };

export default function Scroller({
	dir = "row", 
	customScroll = true, 
	nowrap = true, 
	ovx = true, 
	scrollStep = 100, // Same scroll native shift key 
	behavior = "smooth", 
	onWheel = () => {}, 
	onScrollPrev = () => {}, 
	onScrollNext = () => {}, 
	ovy, 
	setScrollTo, 
	className, 
	children, 
	
	...etc
}){
	const scrollRef = useRef(null);
	// const prevRef = useRef(null);
	// const nextRef = useRef(null);
	// const isViewPrev = useInViewport(prevRef);
	// const isViewNext = useInViewport(nextRef);

	const [endPrev , setEndPrev] = useState(false);
	const [endNext , setEndNext] = useState(false);
	
	useLayoutEffect(() => {// useEffect | useLayoutEffect
		// console.log('%cuseEffect / useLayoutEffect in Scroller','color:yellow;');
		const scroller = scrollRef.current;
		if(!setScrollTo && scroller.scrollLeft <= 0){
			setEndPrev(true);
		}

		if(setScrollTo){
			scroller.scrollBy(setScrollTo, 0);
		}
	}, [setScrollTo]);

	const Wheel = e => {
		// Not Run if native scroll with shift key
		if(!e.shiftKey){
			// console.log(e.shiftKey); // ctrlKey
			// let et = e.target;
			
			// let scroller = scrollRef.current; // Q.hasClass(et, "scroller");
			// let pos = e.deltaY > 0 ? scrollStep : -scrollStep;
			// scroller.scrollBy(pos, 0);

			// let left = Math.ceil(scroller.scrollLeft);
			// let width = scroller.clientWidth;
			// let scrollWidth = scroller.scrollWidth;

			// if((left + width + 1) >= scrollWidth){
			// 	console.log('Wheel END NEXT');
			// 	setEndNext(true);
			// }else{
			// 	if(endNext) setEndNext(false);
			// }

			// if(left <= 0){
			// 	console.log('Wheel END PREV');
			// 	setEndPrev(true);
			// }else{
			// 	if(endPrev) setEndPrev(false);
			// }

			// if(scroller){
			// 	scroller.scrollBy(pos, 0);
			// }else{
			// 	et.closest(".scroller").scrollBy(pos, 0);
			// }

			if(e.deltaY > 0){
				onScrollPrevNext(e, "next", "auto");
			}else{
				onScrollPrevNext(e, "prev", "auto");
			}
		}

		onWheel(e);
	}

	const Scroll = (e) => {
		e.stopPropagation();

		let et = e.target;
		// const scroller = scrollRef.current;
		// console.log('Scroll e.target: ', et);

		let left = Math.ceil(et.scrollLeft);
		let width = et.clientWidth;
		let scrollWidth = et.scrollWidth;

		if((left + width + 1) >= scrollWidth){
			// console.log('Scroll END NEXT');
			setEndNext(true);
		}else{
			if(endNext) setEndNext(false);
		}

		if(left <= 0){
			// console.log('Scroll END PREV');
			setEndPrev(true);
		}else{
			if(endPrev) setEndPrev(false);
		}
	}

	const onScrollPrevNext = (e, pos = "next", b = behavior) => {
		if(e) e.stopPropagation();

		// -scrollStep, 0
		const scroller = scrollRef.current;
		scroller.scrollBy({
			// ...scrollOptions, 
			top: 0, 
			left: pos === "next" ? scrollStep : -scrollStep, 
			behavior: b
		});

		let left = Math.ceil(scroller.scrollLeft);
		let width = scroller.clientWidth;
		let scrollWidth = scroller.scrollWidth;

		// // console.log('scrollLeft: ', left);// 
		// // console.log('clientWidth: ', width);
		// // console.log('scrollWidth: ', scrollWidth);
		// // console.log('rect: ', scroller.getBoundingClientRect());

		// // (scroller.scrollWidth - Math.ceil(scroller.scrollLeft)) >= scroller.clientWidth
		// // (Math.ceil(scroller.scrollLeft) + scroller.clientWidth) >= scroller.scrollWidth

		if(pos === "next"){
			if((left + width + 1) >= scrollWidth){
				// console.log('onScrollPrevNext END NEXT');
				setEndNext(true);

				if(press) clearInterval(press);
			}

			if(endPrev) setEndPrev(false);

			onScrollNext(e);
		}else{
			if(left <= 0){
				// console.log('onScrollPrevNext END PREV');
				setEndPrev(true);

				if(press) clearInterval(press);
			}

			if(endNext) setEndNext(false);

			onScrollPrev(e);
		}
	}

	let press = null;

	const onPressedPrev = () => {
		press = setInterval(onPressPrev, 100);
	}

	const onPressPrev = (e) => {
		// console.log('onMouseDown to onPress e: ', e);
		onScrollPrevNext(e, "prev");
	}

	const onPressedNext = () => {
		press = setInterval(onPressNext, 100);
	}

	const onPressNext = (e) => {
		// console.log('onMouseDown to onPress e: ', e);
		onScrollPrevNext(e, "next");// 
	}

	const stopPress = () => {
		clearInterval(press);
		// console.log('onMouseUp to stopPress press: ', press);
	}

	return (
		<Flex
			className="wrap-scroller" 
		>
			<Btn 
				className="qi qi-chevron-left btnScrollPrev" 
				// className={Q.Cx("qi qi-chevron-left btnScrollPrev", { "d-none":endPrev })} 
				disabled={endPrev} 
				onClick={e => onScrollPrevNext(e, "prev")} 
				onMouseDown={onPressedPrev} // onPress 
				onMouseUp={stopPress} 
				onMouseLeave={stopPress} 
			/>

			<Flex 
				{...etc} 
				inRef={scrollRef} // OPTION
				dir={dir} 
				nowrap={nowrap} 
				// justify="between" 
				className={
					Q.Cx("scroller", {
						"ovxauto": ovx, 
						"ovyauto": ovy, 
						"q-scroll": customScroll
					}, className)
				} // scroll-x
				onWheel={Wheel} 
				onScroll={Scroll} 
			>
				{/* <div ref={prevRef} className="ref-s" /> */}

				{children}

				{/* <div ref={nextRef} className="ref-s" /> */}
			</Flex>

				<Btn 
					className="qi qi-chevron-right btnScrollNext" 
					// className={Q.Cx("qi qi-chevron-right btnScrollNext", { "d-none":endNext })} 
					disabled={endNext}
					onClick={e => onScrollPrevNext(e)} 
					onMouseDown={onPressedNext}  
					onMouseUp={stopPress} 
					onMouseLeave={stopPress} 
				/>
		</Flex>
	);
}

/*
<div 
	// {...etc} 
	// nav-scroller py-1 mb-2 w-100
	className="wrap-scroller" // 
>
<React.Fragment></React.Fragment>
*/
