import React from "react";
// import classNames from "classnames";

// import { Classes as CoreClasses, DISPLAYNAME_PREFIX, HTMLSelect, Icon, Intent, Keys } from "@blueprintjs/core";

import * as Classes from "./utils/blueprint/classes";// "./common/classes"
import * as DateUtils from "./utils/blueprint/dateUtils";// "./common/dateUtils"
import {
    getDefaultMaxTime, 
    getDefaultMinTime, 
    getTimeUnit, 
    getTimeUnitClassName, 
    isTimeUnitValid, 
    setTimeUnit, 
    TimeUnit, 
    wrapTimeAtUnit,
} from "./utils/blueprint/timeUnit";// "./common/timeUnit"
import * as Utils from "./utils/blueprint/utils";// "./common/utils"

import * as Keys from "./utils/blueprint/keys";

export const TimePrecision = {
    MILLISECOND: "millisecond", // "millisecond" as "millisecond"
    MINUTE: "minute", // "minute" as "minute"
    SECOND: "second", // "second" as "second"
};

// export const Keys = {
//     BACKSPACE: 8, 
//     TAB: 9, 
//     ENTER: 13, 
//     SHIFT: 16, 
//     ESCAPE: 27, 
//     SPACE: 32, 
//     ARROW_LEFT: 37, 
//     ARROW_UP: 38, 
//     ARROW_RIGHT: 39, 
//     ARROW_DOWN: 40, 
//     DELETE: 46
// };

// eslint-disable-next-line @typescript-eslint/no-redeclare
// export type TimePrecision = typeof TimePrecision[keyof typeof TimePrecision];

const DISABLED = "bp3-disabled";// `${NS}-disabled`;
const DISPLAYNAME_PREFIX = "Blueprint3";

const Intent = {
    NONE: "none", // "none" as "none"
    PRIMARY: "primary", // "primary" as "primary"
    SUCCESS: "success", // "success" as "success"
    WARNING: "warning", // "warning" as "warning"
    DANGER: "danger", // "danger" as "danger"
};

function intentClass(intent) {
    if (intent == null || intent === Intent.NONE) {
        return undefined;
    }
    return "bp3-intent-" + intent.toLowerCase();// `${NS}-intent-${intent.toLowerCase()}`
}

function formatTime(time, unit) {
    switch (unit) {
        case TimeUnit.HOUR_24:
            return time.toString();
        case TimeUnit.HOUR_12:
            return DateUtils.get12HourFrom24Hour(time).toString();
        case TimeUnit.MINUTE:
        case TimeUnit.SECOND:
            return Utils.padWithZeroes(time.toString(), 2);
        case TimeUnit.MS:
            return Utils.padWithZeroes(time.toString(), 3);
        default:
            throw Error("Invalid TimeUnit");
    }
}

function getStringValueFromInputEvent(e){
    return e.target.value;
}

function handleKeyEvent(e, actions, preventDefault = true) {
    for (const k of Object.keys(actions)) {
        const key = Number(k);
        // HACKHACK: https://github.com/palantir/blueprint/issues/4165
        // eslint-disable-next-line deprecation/deprecation
        if (e.which === key) {
            if (preventDefault) {
                e.preventDefault();
            }
            actions[key]();
        }
    }
}

export class InputTime extends React.Component{ // TimePicker
    static defaultProps = {
        autoFocus: false,
        disabled: false,
        maxTime: getDefaultMaxTime(),
        minTime: getDefaultMinTime(),
        precision: TimePrecision.MINUTE,
        selectAllOnFocus: false,
        showArrowButtons: false,
        useAmPm: false,
    };

    static displayName = `${DISPLAYNAME_PREFIX}.TimePicker`;

    constructor(props, context) {
        super(props, context);

        let value = props.minTime;
        if (props.value != null) {
            value = props.value;
        } else if (props.defaultValue != null) {
            value = props.defaultValue;
        }

        this.state = this.getFullStateFromValue(value, props.useAmPm);
    }

    componentDidUpdate(prevProps) {
        const didMinTimeChange = prevProps.minTime !== this.props.minTime;
        const didMaxTimeChange = prevProps.maxTime !== this.props.maxTime;
        const didBoundsChange = didMinTimeChange || didMaxTimeChange;
        const didPropValueChange = prevProps.value !== this.props.value;
        const shouldStateUpdate = didMinTimeChange || didMaxTimeChange || didBoundsChange || didPropValueChange;

        let value = this.state.value;
        if (didBoundsChange) {
            value = DateUtils.getTimeInRange(this.state.value, this.props.minTime, this.props.maxTime);
        }
        if (this.props.value != null && !DateUtils.areSameTime(this.props.value, prevProps.value)) {
            value = this.props.value;
        }

        if (shouldStateUpdate) {
            this.setState(this.getFullStateFromValue(value, this.props.useAmPm));
        }
    }

    // begin method definitions: rendering

    maybeRenderArrowButton(isDirectionUp, timeUnit) {
        if (!this.props.showArrowButtons) {
            return null;
        }
        const classes = Q.Cx(Classes.TIMEPICKER_ARROW_BUTTON, getTimeUnitClassName(timeUnit));
        const onClick = () => (isDirectionUp ? this.incrementTime : this.decrementTime)(timeUnit);
        // set tabIndex=-1 to ensure a valid FocusEvent relatedTarget when focused
        
        // <span tabIndex={-1} className={classes} onClick={onClick}>
        return (
            <div tabIndex={-1} onClick={onClick}
                className={Q.Cx(classes, "btn btn-light qi qi-chevron-" + (isDirectionUp ? "up" : "down"))}
            >
                {/* <Icon icon={isDirectionUp ? "chevron-up" : "chevron-down"} /> */}
                {/* <i className={"qi qi-chevron-" + (isDirectionUp ? "up" : "down")} /> */}
            </div>
        );
    }

    renderDivider(text = ":") {
        // return <span className={Classes.TIMEPICKER_DIVIDER_TEXT}>{text}</span>;
        return <div className={Q.Cx(Classes.TIMEPICKER_DIVIDER_TEXT, "form-control w-auto flexno px-1 bw-y1")}>{text}</div>;
    }

    renderInput(className, unit, value) {
        const isValid = isTimeUnitValid(unit, parseInt(value, 10));
        const isHour = unit === TimeUnit.HOUR_12 || unit === TimeUnit.HOUR_24;

        return (
            <input 
                type="number" 
                // min="0" 
                // max={unit} // "12" 
                className={Q.Cx(
                    "form-control w-auto flexno no-arrow", 
                    Classes.TIMEPICKER_INPUT,
                    { [intentClass(Intent.DANGER)]: !isValid }, // { [CoreClasses.intentClass(Intent.DANGER)]: !isValid }
                    className,
                )}
                onBlur={this.getInputBlurHandler(unit)}
                onChange={this.getInputChangeHandler(unit)}
                onFocus={this.getInputFocusHandler(unit)}
                onKeyDown={this.getInputKeyDownHandler(unit)}
                onKeyUp={this.getInputKeyUpHandler(unit)}
                value={value} 
                disabled={this.props.disabled}
                autoFocus={isHour && this.props.autoFocus}
            />
        );
    }

    maybeRenderAmPm() {
        if (!this.props.useAmPm) {
            return null;
        }
        return (
            <select // HTMLSelect
                className={Q.Cx(Classes.TIMEPICKER_AMPM_SELECT, "w-auto flexno custom-select")}
                disabled={this.props.disabled} 
                value={this.state.isPm ? "pm" : "am"} 
                onChange={this.handleAmPmChange} 
            >
                <option value="am">AM</option>
                <option value="pm">PM</option>
            </select>
        );
    }

    // begin method definitions: event handlers

    getInputChangeHandler = (unit) => (e) => {
        const text = getStringValueFromInputEvent(e);
        switch (unit) {
            case TimeUnit.HOUR_12:
            case TimeUnit.HOUR_24:
                this.setState({ hourText: text });
                break;
            case TimeUnit.MINUTE:
                this.setState({ minuteText: text });
                break;
            case TimeUnit.SECOND:
                this.setState({ secondText: text });
                break;
            case TimeUnit.MS:
                this.setState({ millisecondText: text });
                break;
        }
    };

    getInputBlurHandler = (unit) => (e) => {
        const text = getStringValueFromInputEvent(e);
        this.updateTime(parseInt(text, 10), unit);
        this.props.onBlur?.(e, unit);
    };

    getInputFocusHandler = (unit) => (e) => {
        if (this.props.selectAllOnFocus) {
            e.currentTarget.select();
        }
        this.props.onFocus?.(e, unit);
    };

    getInputKeyDownHandler = (unit) => (e) => {
        // console.log(e.key);
        if(e.key === "."){ // Q-CUSTOM conditional
            e.preventDefault();
        }else{
            handleKeyEvent(e, {
                [Keys.ARROW_UP]: () => this.incrementTime(unit),
                [Keys.ARROW_DOWN]: () => this.decrementTime(unit),
                [Keys.ENTER]: () => {
                    e.currentTarget.blur();
                },
            });
        }

        this.props.onKeyDown?.(e, unit);
    };

    getInputKeyUpHandler = (unit) => (e) => {
        this.props.onKeyUp?.(e, unit);
    };

    handleAmPmChange = (e) => {
        const isNextPm = e.currentTarget.value === "pm";
        if (isNextPm !== this.state.isPm) {
            const hour = DateUtils.convert24HourMeridiem(this.state.value.getHours(), isNextPm);
            this.setState({ isPm: isNextPm }, () => this.updateTime(hour, TimeUnit.HOUR_24));
        }
    };

    // begin method definitions: state modification

    /* Generates a full ITimePickerState object with all text fields set to formatted strings based on value */
    getFullStateFromValue(value, useAmPm) {
        const timeInRange = DateUtils.getTimeInRange(value, this.props.minTime, this.props.maxTime);
        const hourUnit = useAmPm ? TimeUnit.HOUR_12 : TimeUnit.HOUR_24;
        /* tslint:disable:object-literal-sort-keys */
        return {
            hourText: formatTime(timeInRange.getHours(), hourUnit),
            minuteText: formatTime(timeInRange.getMinutes(), TimeUnit.MINUTE),
            secondText: formatTime(timeInRange.getSeconds(), TimeUnit.SECOND),
            millisecondText: formatTime(timeInRange.getMilliseconds(), TimeUnit.MS),
            value: timeInRange,
            isPm: DateUtils.getIsPmFrom24Hour(timeInRange.getHours()),
        };
        /* tslint:enable:object-literal-sort-keys */
    }

    incrementTime = (unit) => this.shiftTime(unit, 1);

    decrementTime = (unit) => this.shiftTime(unit, -1);

    shiftTime(unit, amount) {
        if (this.props.disabled) {
            return;
        }
        const newTime = getTimeUnit(unit, this.state.value) + amount;
        this.updateTime(wrapTimeAtUnit(unit, newTime), unit);
    }

    updateTime(time, unit) {
        const newValue = DateUtils.clone(this.state.value);

        if (isTimeUnitValid(unit, time)) {
            setTimeUnit(unit, time, newValue, this.state.isPm);
            if (DateUtils.isTimeInRange(newValue, this.props.minTime, this.props.maxTime)) {
                this.updateState({ value: newValue });
            } else {
                this.updateState(this.getFullStateFromValue(this.state.value, this.props.useAmPm));
            }
        } else {
            this.updateState(this.getFullStateFromValue(this.state.value, this.props.useAmPm));
        }
    }

    updateState(state) {
        let newState = state;
        const hasNewValue = newState.value != null && !DateUtils.areSameTime(newState.value, this.state.value);
        const { useAmPm, precision } = this.props;

        if (this.props.value == null) {
            // component is uncontrolled
            if (hasNewValue) {
                newState = this.getFullStateFromValue(newState.value, useAmPm);// this.props.
            }
            this.setState(newState);
        } else {
            // component is controlled, and there's a new value
            // so set inputs' text based off of _old_ value and later fire onChange with new value
            if (hasNewValue) {
                this.setState(this.getFullStateFromValue(this.state.value, useAmPm));
            } else {
                // no new value, this means only text has changed (from user typing)
                // we want inputs to change, so update state with new text for the inputs
                // but don't change actual value
                this.setState({ ...newState, value: DateUtils.clone(this.state.value) });
            }
        }

        if (hasNewValue) {
            let parseVal = { ...newState, hour: parseInt(newState.hourText) };

            switch(precision){
                case "minute":
                    parseVal.minute = parseInt(newState.minuteText);
                    delete parseVal.secondText;
                    delete parseVal.millisecondText;
                    break;
                case "second":
                    parseVal.second = parseInt(newState.secondText);
                    delete parseVal.millisecondText;
                    break;
                case "millisecond":
                    parseVal.millisecond = parseInt(newState.millisecondText);
                    break;
                default:
                    break;
            }

            if(useAmPm){
                parseVal.ampm = parseVal.isPm ? "pm" : "am";
            }else{
                delete parseVal.isPm;
            }

            // console.log('newState: ', newState);
            // console.log('parseVal: ', parseVal);

            this.props.onChange?.(parseVal);// newState.value
        }
    }

    render() {
        const shouldRenderMilliseconds = this.props.precision === TimePrecision.MILLISECOND;
        const shouldRenderSeconds = shouldRenderMilliseconds || this.props.precision === TimePrecision.SECOND;
        const hourUnit = this.props.useAmPm ? TimeUnit.HOUR_12 : TimeUnit.HOUR_24;
        const classes = Q.Cx(Classes.TIMEPICKER, this.props.className, {
            [DISABLED]: this.props.disabled, // [CoreClasses.DISABLED]: this.props.disabled,
        });

        return (
            <div className={Q.Cx(classes, "d-inline-flex flex-column input-time")}>
                <div className={Classes.TIMEPICKER_ARROW_ROW}>
                    {this.maybeRenderArrowButton(true, hourUnit)}
                    {this.maybeRenderArrowButton(true, TimeUnit.MINUTE)}
                    {shouldRenderSeconds && this.maybeRenderArrowButton(true, TimeUnit.SECOND)}
                    {shouldRenderMilliseconds && this.maybeRenderArrowButton(true, TimeUnit.MS)}
                </div>

                <div className={Q.Cx(Classes.TIMEPICKER_INPUT_ROW, "input-group d-inline-flex w-auto")}>
                    {this.renderInput(Classes.TIMEPICKER_HOUR, hourUnit, this.state.hourText)}

                    {this.renderDivider()}
                    
                    {this.renderInput(Classes.TIMEPICKER_MINUTE, TimeUnit.MINUTE, this.state.minuteText)}

                    {shouldRenderSeconds && this.renderDivider()}
                    
                    {shouldRenderSeconds &&
                        this.renderInput(Classes.TIMEPICKER_SECOND, TimeUnit.SECOND, this.state.secondText)
                    }

                    {shouldRenderMilliseconds && this.renderDivider(".")}

                    {shouldRenderMilliseconds &&
                        this.renderInput(Classes.TIMEPICKER_MILLISECOND, TimeUnit.MS, this.state.millisecondText)
                    }

                    {this.maybeRenderAmPm()}
                </div>

                {/* {this.maybeRenderAmPm()} */}

                <div className={Classes.TIMEPICKER_ARROW_ROW}>
                    {this.maybeRenderArrowButton(false, hourUnit)}
                    {this.maybeRenderArrowButton(false, TimeUnit.MINUTE)}
                    {shouldRenderSeconds && this.maybeRenderArrowButton(false, TimeUnit.SECOND)}
                    {shouldRenderMilliseconds && this.maybeRenderArrowButton(false, TimeUnit.MS)}
                </div>
            </div>
        );
    }
}


