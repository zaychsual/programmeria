import React, { forwardRef, useRef } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

// import Btn from '../../components/q-ui-react/Btn';

const InputFile = forwardRef(
	({
		multiple, 
		onChange = Q.noop, 
		...etc
	}, 
	ref
) => {
	const myRef = ref || useRef(null);
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in InputFile','color:yellow;');
	// }, []);

	const Change = e => {
		// const et = e.target;// nativeEvent
		const files = e.target.files;
		// const native = e.nativeEvent;
		console.log('files: ', files);
		// console.log('e.nativeEvent: ', native);

		if(files.length > 0){
			// for(let file of files){
			// 	console.log('file: ', file);
			// }
			// let events = {};
			// for(let evt in native){
			// 	events[evt] = native[evt];
			// }
			// console.log('events: ', events);
			onChange(multiple ? files : files[0], e);
		}
	}

	return (
		<input 
			{...etc}
			ref={myRef} 
			type="file" 
			multiple={multiple} 
			onChange={Change}
		/>
	);
});

export default InputFile;
