import React, { forwardRef } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

const Input = forwardRef(
	({ 
		readOnly, 
		disabled, 
		
		className, 
		type = "text", 
		onKeyDown = Q.noop, 
		onChange = Q.noop, 
		// onCopy = Q.noop, 
		onCut = Q.noop, 
		onPaste = Q.noop, 
		
		prevfixClass = "form-control", 
		
		isize, 
		...etc
	}, 
	ref
) => {
	const cantEdit = e => {
		if(disabled || readOnly){
			e.preventDefault();
			return;
		}
	}

	return (
		<input 
			{...etc} 
			ref={ref} 
			type={type} 
			className={
				Q.Cx(prevfixClass, { 
					[(prevfixClass ?? "i") + "-" + isize] : isize 
				}, className)
			} 
			disabled={disabled} 
			readOnly={readOnly} 
			onKeyDown={e => {
				const { key, ctrlKey } = e;				
				if(!ctrlKey && (key !== "c" || key !== "C")){
					cantEdit(e);
				}
				
				onKeyDown(e);
			}} 
			onChange={e => {
				cantEdit(e);
				onChange(e);
			}} 
			// onCopy={e => {
				// cantEdit(e);
				// onCopy(e);
			// }} 
			onCut={e => {
				cantEdit(e);
				onCut(e);
			}} 
			onPaste={e => {
				cantEdit(e);
				onPaste(e);
			}}
		/>
	);
});

export default Input;
