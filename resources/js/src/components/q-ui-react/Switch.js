import React from 'react';

// import {Cx} from '../utils/Q';

export default function Switch({
	As = 'label', 
	className, 
	id, 
	label, 
	parentProps, 
	...etc
}){
	return (
		<As 
			{...parentProps} 
			className={Q.Cx("custom-control custom-switch", className)}
		>
			<input {...etc} type="checkbox" className="custom-control-input" id={id} />
			
			{
				React.createElement(As === 'label' && !id ? 'div' : 'label', {
					className: "custom-control-label",
					htmlFor: As !== 'label' && id ? id : undefined
				}, label)
			}
		</As>
	);
}
// <label className="custom-control-label" htmlFor={id}>{label}</label>

// Switch.defaultProps = {
	// As: 'label'
// };


