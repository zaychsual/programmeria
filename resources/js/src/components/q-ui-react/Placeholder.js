import React from 'react';// { Fragment, useState, useEffect }

export default function Placeholder({
	className,
	w,
	h, 
	style, 
	label, 
	length = 1,
	type = "p", // h1
	...etc
}){
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Placeholder','color:yellow;');
	// }, []);
	
	// const As = "thumb form".includes(type) ? "div" : type; // type === "thumb" || type === "form"

	return React.Children.toArray(
		Array.from({ length: parseInt(length) }).map(() => 
			<div 
				{...etc}
				
				// `holder ${type}${className ? ` ${className}` : ""}`
				className={Q.Cx("holder", type, className)} 
				style={{
					...style, 
					width: w,
					height: h
				}} 
				aria-label={label}
			/>
		)
	);
}

/*
<Fragment>
	{Array.from({ length: parseInt(length) }).map((v, i) => 
		<div key={i} 
			{...etc}
			
			className={`holder ${type}${className ? ` ${className}` : ""}`}
			style={{
				...style, 
				width: w,
				height: h
			}} 
			
		/>
	)}
</Fragment>
*/
