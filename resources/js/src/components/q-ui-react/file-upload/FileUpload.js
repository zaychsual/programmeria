import React from 'react';
import Tab from 'react-bootstrap/Tab';
import Nav from 'react-bootstrap/Nav';

import Flex from '../Flex';
import Btn from '../Btn';
import uid from '../../../utils/UniqueComponentId';

export default function FileUpload({
  dir = "column", 
  className, 
  id, 
}){
  const ID = id || "fileUpload" + uid();

  return (
    <Tab.Container 
      id={ID} 
      mountOnEnter 
      defaultActiveKey="local"
    >
      <Flex 
        dir={dir} 
        className={Q.Cx("q-file-upload", className)}
      >
        <Nav variant="pills" className="flex-column flexno w-200px q-scroll">
          <Nav.Link eventKey="local" {...Q.DD_BTN} className="btn w-100 text-left qi qi-folder-open">Local Files</Nav.Link>
          <Nav.Link eventKey="gd" {...Q.DD_BTN} className="btn w-100 text-left qi qi-google">Google Drive</Nav.Link>
          <Nav.Link eventKey="ig" {...Q.DD_BTN} className="btn w-100 text-left qi qi-instagram">Instagram</Nav.Link>
        </Nav>

        <Tab.Content as="section" className="flex-grow-1 border-left">
          <Tab.Pane eventKey="local">
            <Flex dir="column" justify="center" align="center" className="mnh-50vh qi-3x qi qi-folder-open">
              <h5>Drag and drop files to upload</h5>
              <b className="hr-h mt-2 mb-3">or</b>
              <Btn>Browse Files</Btn>
            </Flex>
          </Tab.Pane>

          <Tab.Pane eventKey="gd">
            <Flex dir="column" justify="center" align="center" className="mnh-50vh qi-3x qi qi-google">
              <h5>Please authenticate with Google Drive to select files</h5>
              <Btn>Connect to Google Drive</Btn>
            </Flex>
          </Tab.Pane>

          <Tab.Pane eventKey="ig">
            <Flex dir="column" justify="center" align="center" className="mnh-50vh qi-3x qi qi-instagram">
              <h5>Please authenticate with Instagram to select files</h5>
              <Btn>Connect to Instagram</Btn>
            </Flex>
          </Tab.Pane>
        </Tab.Content>
      </Flex>
    </Tab.Container>
  )
}

/* <div className="btn-group-vertical btn-group-lg">
<Btn kind="light" className="qi qi-folder-open">Local Files</Btn>
<Btn kind="light" className="qi qi-google">Google Drive</Btn>
</div> */