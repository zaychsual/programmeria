import React, { forwardRef, useRef, useEffect } from 'react';// { useState, useEffect, useRef }

// import ResizeSensor from './ResizeSensor';

const createResizableTable = (table, height) => { // 
	const cols = Q.domQall("thead th", table);// table.querySelectorAll('thead th');
	[].forEach.call(cols, (col) => {
		// Add a resizer element to the column
		const resizer = Q.makeEl("div");
		// const wrap = Q.makeEl("div");

		// wrap.className = "text-truncate";

		resizer.className = "tb-resizer";
		resizer.tabIndex = "-1";
		// Q.setClass(resizer, "tb-resizer");// resizer.classList.add("tb-resizer");
		// Set the height
		// resizer.style.height = height ? height + "px" : table.clientHeight + "px";// div.offsetHeight + "px"
		resizer.style.height = height ? height : table.clientHeight + "px";
		// resizer.style.height = table.clientHeight + "px";
		// console.log('table.clientHeight: ', table.clientHeight);

		// wrap.appendChild(col.firstChild);
		// col.appendChild(wrap);
		col.appendChild(resizer);
		Q.setClass(col, "text-nowrap");

		createResizableCol(col, resizer);
	});
};

const createResizableCol = (col, resizer) => {
	let x = 0;
	let w = 0;
	
	const mouseDown = (e) => {
		// console.log('mouseDown e: ', e);
		if(e.button === 0 || e.type === "touchstart"){
			x = e.clientX || e.touches[0].clientX;
			// console.log('mouseDown x: ', x);

			const styles = window.getComputedStyle(col);
			w = parseInt(styles.width, 10);

			const doc = document;
			doc.addEventListener("mousemove", mouseMove);
			doc.addEventListener("mouseup", mouseUp);

			doc.addEventListener("touchmove", mouseMove);
			doc.addEventListener("touchend", mouseUp);

			Q.setClass(resizer, "tb-resizing");// resizer.classList.add('tb-resizing');
		}
	};

	const mouseMove = (e) => {
		// console.log('mouseMove e: ', e);
		const dx = (e.clientX || e.touches[0].clientX) - x;
		col.style.width = (w + dx) + "px"; // `${w + dx}px`
	};

	const mouseUp = () => {
		Q.setClass(resizer, "tb-resizing", "remove");// resizer.classList.remove('tb-resizing');
		const doc = document;
		doc.removeEventListener("mousemove", mouseMove);
		doc.removeEventListener("mouseup", mouseUp);

		doc.removeEventListener("touchmove", mouseMove);
		doc.removeEventListener("touchend", mouseUp);

		resizer.blur();
	};

	resizer.addEventListener("mousedown", mouseDown);
	resizer.addEventListener("touchstart", mouseDown);
};

// export default function Table({
const Table = forwardRef(
	({
		responsive = true, 
		responsiveClass, 
		responsiveSize, // sm | md | lg | xl
		wrapHeight, // String / Number in px
		responsiveStyle, // DEV OPTION: For fixed thead
		border, // bordered
		// borderless, 
		hover, 
		strip, // striped
		// size, // "sm" | 
		sm, 
		kind, // "dark" | 
		layout, // "auto" | "fix" 

		thead,
		tbody, 
		tfoot, // OPTION
		caption, 

		className, 
		theadClass, // "thead-dark" | "thead-light" | "bg-light"
		tbodyClass, 
		tfootClass, // OPTION
		captionClass, 
		
		wrapProps, 

		fixThead, // DEV OPTION: For fixed thead
		resize = true, 
		customScroll = true, 
	}, 
	ref
) => {
	// const [tbRef, setTbRef] = useState();
	const divRef = useRef(null);
	
	useEffect(() => {
		// console.log('%cuseEffect in Table','color:yellow;');
		// console.log('tbRef: ', tbRef);
		// if(!ref){
		// 	setTbRef();
		// }

		if(resize && responsive){
			const tb = divRef.current?.firstElementChild;
			// console.log(tb);
			if(tb) createResizableTable(tb, wrapHeight);// , divRef.current
		}
	}, [resize, responsive]);// ref

	const Tb = () => (
		<table 
			ref={ref} 
			className={
				Q.Cx("table", {
					// "table-bordered": border, 
					// "table-borderless": borderless,
					["table-border" + (border === true ? "ed" : "less")]: border, 
					"table-striped": strip, 
					"table-hover": hover,
					["table-sm"]: sm, 
					["table-" + kind]: kind, 
					["tb-" + layout]: layout
				}, className)
			}
		>
			{caption && <caption className={captionClass}>{caption}</caption>}

			{thead && <thead className={theadClass}>{thead}</thead>}

			{tbody && <tbody className={tbodyClass}>{tbody}</tbody>}

			{/* OPTION */}
			{tfoot && <tfoot className={tfootClass}>{tfoot}</tfoot>}
		</table>
	);

	// const Tbl = () => 
	// 	fixThead ? 
	// 		<ResizeSensor 
	// 			// className="" 
	// 			// wait={1000} 
	// 			onResizeEnd={(s) => {
	// 				console.log('onResizeEnd size: ', s);
	// 			}}
	// 		>
	// 			{Tb()}
	// 		</ResizeSensor>
	// 	: Tb();

	if(responsive || fixThead){
		return (
			<div ref={divRef} 
				{...wrapProps} 
				className={
					Q.Cx({
						["table-responsive" + (responsiveSize ? "-" + responsiveSize : "")]: responsive, 
						"ovauto thead-fix": fixThead, 
						"tb-resize": resize, 
						"q-scroll ovscroll-auto": customScroll
					}, responsiveClass)
				}
				style={{ 
					height: wrapHeight, 
					...responsiveStyle
				}} // DEV OPTION: For fixed thead
			>
				{Tb()}
			</div>
		);
	}

	return Tb();
});

export default Table;



