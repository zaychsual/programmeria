import React, { useRef, useState, useEffect } from 'react';
// import axios from 'axios';
// import SVG from 'react-inlinesvg';
import Dropdown from 'react-bootstrap/Dropdown';
// import { useInViewport } from 'ahooks';
import List from '@researchgate/react-intersection-list';

import Btn from '../Btn';
import Flex from '../Flex';
import Img from '../Img';
import { ScrollTo } from '../ScrollTo';
import { num2Hex } from '../../../utils/number';
import { clipboardCopy } from '../../../utils/clipboard';
// import { APP_DOMAIN } from '../../../data/appData';
// import { domQall, hasClass, setClass, toggleClass, noop, Qid } from '../../../utils/Q';

// function InViewPort({ inRef, children }){
//   // const ref = useRef();
//   const inView = useInViewport(inRef);

//   return (
//     <Fragment>
//       {children && children(inView)}
//     </Fragment>
//   );
// };

export default function IconPicker({
	icons, // Object data 
	url, // String url fetch
	// id, 
	show = false, // Can use with Dropdown / Modal
	label = "Icons", 
	storageKey = "icons", 
	cache = true, // DEV OPTION: Caching used
	w = 290, // 277 | 329 | 323 
	h  = 340, // 380 | 364,
	autoFocus = false, 
	autoClose = true, 
	preview = {
		name: "home", 
		code: "e933"
	}, 
	copy = true, 
	reload = false, 
	onClickIcon = Q.noop, // () => {}
	onHoverIcon = Q.noop // () => {},
}){
	// const iconBoxRef = useRef(null);
	const findRef = useRef(null);
	
	const [load, setLoad] = useState(false);
	const [data, setData] = useState(icons);// false
	const [meta, setMeta] = useState(null);
	const [dd, setDd] = useState(show);// false
  const [view, setView] = useState(preview);
	const [findVal, setFindVal] = useState("");
	const [findArr, setFindArr] = useState([]); // null
	// const [icoCache, setIcoCache] = useState(null);// DEV OPTION: Caching used

	const onGetSetData = () => {
		if(cache){
			const cacheIcon = localStorage.getItem(storageKey);// "icons"
			if(cacheIcon){
				const parseData = JSON.parse(cacheIcon);

				if(parseData.type === "svg"){
					const ico = parseData.icons;
					setView({ path: ico[0], name: ico[0].split("/").pop() });
					setData(ico);
				}else{
					setData(parseData.data);
					setMeta(parseData.meta);
				}

				setLoad(true);

				// let dataIcon = parseData.data.filter((_, i) => i < 42);
				// console.log('parseData.data: ', parseData.data);
				// console.log('dataIcon: ', dataIcon);
			}else{
				fetchIcon();
			}
		}else{
			fetchIcon();
		}
	}

	useEffect(() => {
		// console.log('%cuseEffect in IconPicker','color:yellow');
		if(show){ // cacheIcon || 
			onGetSetData();// null, cacheIcon
		}else{
			setLoad(true);
		}
	}, [show]);// , onGetSetData

	const fetchIcon = () => { // cb
		if(typeof url === "string"){
			// axios.get(url).then(r => {
			// 	console.log('r: ', r);
			// 	if(r.status === 200){ // CHECK TO...???
			// 		let data = r.data;
			// 		let dataIcon, metas;
	
			// 		if(data.type === "svg"){
			// 			dataIcon = data.icons;
			// 			const ico = dataIcon[0];
			// 			setView({ path: ico, name: ico.split("/").pop() });
			// 		}else{
			// 			metas = {
			// 				name: data.metadata.name, 
			// 				prefix: data.preferences.fontPref.prefix
			// 			};
	
			// 			dataIcon = data.icons.map(v => ({
			// 				name: v.properties.name,
			// 				code: num2Hex(v.properties.code)
			// 			}));
	
			// 			setMeta(metas);
			// 		}

			// 		if(cache){
			// 			const objStorage = metas ? { meta: metas, data: dataIcon } : data;
			// 			localStorage.setItem(storageKey, JSON.stringify(objStorage));
			// 		}

			// 		setLoad(true);
			// 		setData(dataIcon);
			// 	}
			// })
			// .catch(e => console.warn(e))
			// // .then(() => setLoad(true));

			setLoad(false);

			import(/* webpackIgnore:true */url)
			.then(m => {
				// console.log('m: ', m);
				if(m && m.default){
					let data = m.default;// JSON.stringify(m.default);
					let dataIcon, metas;
	
					if(data.type === "svg"){
						dataIcon = data.icons;
						const ico = dataIcon[0];
						setView({ path: ico, name: ico.split("/").pop() });
					}else{
						metas = {
							name: data.metadata.name, 
							prefix: data.preferences.fontPref.prefix
						};
	
						dataIcon = data.icons.map(v => ({
							name: v.properties.name,
							code: num2Hex(v.properties.code)
						}));
	
						setMeta(metas);
					}

					if(cache){
						const objStorage = metas ? { meta: metas, data: dataIcon } : data;
						localStorage.setItem(storageKey, JSON.stringify(objStorage));
					}

					setLoad(true);
					setData(dataIcon);
				}
				// console.log('stringify: ', JSON.stringify(m.default));
			}).catch(e => console.warn('e: ', e));
		}
	}

	// const parseObj = (d) => {
	// 	const arr = d.icons.map(v => ({
	// 		name: v.properties.name,
	// 		code: num2Hex(v.properties.code)
	// 	})).filter((_, i) => i < 42);

	// 	// console.log('arr: ', arr);

	// 	return arr;
	// }

	const onHover = (v, e) => {
		const toView = meta ? v : { path: v, name: v.split("/").pop() };
		if(preview && view.name !== v.name){
			setView(toView);
		}
		
		onHoverIcon(toView, e);
	}

  const onClick = (v, e) => {
		onClickIcon(v, e);// OPTION: i, e

		if(autoClose){ // OPTION: Close Dropdown.Menu
			setDd(false);

			if(findVal.length > 0){ // Reset find
				setFindVal("");
				setFindArr([]);
			}
		}
		
		/* if(isFunc(onClick)){
			console.log(v);
			// const ico = {name: v.name, code: v.code};// {name: v[0], code: v[1]}
			onClick(v, i, e);// OPTION: i, e
		} */
		
		// DEV OPTION: Caching used
		// const ico = {name: v.name, code: v.code};
		/* if(cache){
			// Frequently Used | Current Used
			// const qicoUsed = localStorage.getItem(storageKey);// 'icons'
			// console.log(icoCache);
			if(icoCache !== null){
				const obj = [ico, ...icoCache];
				setIcoCache(obj);
				localStorage.setItem(storageKey, JSON.stringify(obj));// emoji-mart.frequently
			}else{
				setIcoCache([ico]);
				localStorage.setItem(storageKey, JSON.stringify([ico]));
			}
		} */
  }

	const onFind = (e) => {
		// const val = e.target ? ev.target.value : e;
		// const iconBox = iconBoxRef.current;
		
		// setFindVal(val);

		// setTimeout(() => {
		// 	let btnIcons = Q.domQall("button.icon-item", iconBox);
		// 	let NO = "d-none";
		// 	// 
		// 	btnIcons.forEach(el => {
		// 		if(el.dataset.name.includes(val)){// el.title.includes(val) | el.className.includes(val)
		// 			if(Q.hasClass(el, NO)) Q.setClass(el, NO, "remove");
		// 		}else{
		// 			Q.setClass(el, NO);
		// 		}
		// 	});
			
		// 	const getIcon = data.filter(v => v.name.includes(val)).length;
		// 	iconBox.classList.toggle("icoNotFound", getIcon < 1);
		// },900);

		const val = e.target.value;

		setFindVal(val);

		setTimeout(() => {
			const lowCase = val.toLowerCase();
			setFindArr(data.filter(v => meta ? v.name.toLowerCase().includes(lowCase) : v.toLowerCase().includes(lowCase)));
		}, 950);
	}

	const onToggle = (isOpen) => { // , e, meta
		if(!data && isOpen){
			onGetSetData();
		}

		if(dd && meta && preview && view.name !== preview.name){
			// console.log('if 1: ',dd);
			setView(preview);
		}

		if(dd && !meta && preview){
			setView({ path: data[0], name: data[0].split("/").pop() });
		}

		if(dd && findVal !== ""){ // Reset find
			setFindVal("");
			setFindArr([]);
		}

		setDd(isOpen);

		// Set focus 
		if(!dd && autoFocus && findRef.current){
			setTimeout(() => {
				findRef.current.focus();
			},1);
		}
	}

  const itemsRenderer = (items, ref) => {
		const notFound = findVal.length > 0 && findLength() === 0;
		return (
			<ScrollTo As={Flex} wrap // content="start" 
				ref={ref} // iconBoxRef 
				className={Q.Cx("position-absolute position-full p-1 border bw-y1 ovyauto icons-box", { "icoNotFound": notFound })} 
				data-notfound="NOT FOUND" 
				threshold={70} 
				childWrapClass="d-flex flex-wrap align-content-start" 
        btnProps={{ 
					size: "sm", 
          className: "ml-auto shadow-sm position-sticky b0 r0 zi-2 tip tipL qi qi-arrow-up", 
					"aria-label": "Back to top" 
        }} 
			>
				{notFound ? [] : items}
			</ScrollTo>	
		);
	}

  const renderItem = (index, key) => {
		const v = findLength() > 0 ? findArr[index] : data[index];
		// const v = data[index];
		// console.log('v: ', v);
		if(meta){
			// const v = findLength() > 0 ? findArr[index] : data[index];
			return (
				<Dropdown.Item key={key} {...Q.DD_BTN} 
					onMouseEnter={e => onHover(v, e)} 
					onClick={e => onClick({ icon: { ...v, className: pref() + meta.prefix + v.name }, key }, e)} 
					className={"icon-item p-2 flex0 m-1 q-fw " + pref() + meta.prefix + v.name} 
					// data-name={v.name} 
					// JSON.stringify({name: v[0], code: v[1]}, null, 2)
					title={"name: " + v.name + "\ncode: " + v.code} 
				/>
			);
		}else{
			// return (
			// 	<div key={key} 
			// 		className="dropdown-item icon-item p-1 flex0 m-1" // img-frame 
			// 		// style={{ width: 36, height: 36 }} 
			// 	>
			// 	<SVG title=" " 
			// 		src={Q.baseURL + "/public" + v} 
			// 	/>
			// 	</div>
			// )

			let fname = v.split("/").pop();
			return (
				<Img key={key} 
					frame 
					frameClass="dropdown-item icon-item p-1 flex0 m-1" 
					wrapProps={{
						title: fname, 
						onMouseEnter: e => onHover(v, e), 
						onClick: e => onClick(v, e) 
					}}
					// className="d-block" 
					// w={36} 
					// h={36} 
					alt={fname} 
					src={Q.baseURL + "/public" + v} 
				/>
			);
		}
	}

	const onCopy = (e, t) => {
		let et = e.target;
    clipboardCopy(t).then(() => {
      Q.setAttr(et, {"aria-label": "Copied!"});
      setTimeout(() => Q.setAttr(et, "aria-label"), 999);
    }).catch(e => console.log(e));
	}

	const pref = () => meta && meta.prefix.replace("-"," ");

	const findLength = () => findArr.length;

	return (
		<Dropdown bsPrefix="btn-group" 
			show={dd} 
			onToggle={onToggle} 
		>
			<Dropdown.Toggle>{label}</Dropdown.Toggle>{/*  id={id ? id : Q.Qid()} */}
			<Dropdown.Menu 
				// renderOnMount 
				className={Q.Cx("p-0 ovhide icon-picker", { "w-200px": !data })}
			>
				{(data && load) ? 
					<Flex dir="column" align="stretch" 
						style={{
							width: w, 
							height: h
						}} 
					>
						<div className="d-flex p-1 flexno bg-strip">
							{reload && <Btn onClick={fetchIcon} size="sm" kind="light" className="flexno qi qi-redo" title="Reload" />}

							<label className="d-flex m-0 flex1">
								<input ref={findRef}
									autoFocus={autoFocus} 
									value={findVal} 
									onChange={onFind} 
									className="form-control form-control-sm" 
									type="search" // text
									placeholder="Search"
								/>

								{/* Q_appData.UA.browser.name === "Firefox" */}
								{(navigator.userAgent.includes("Firefox") && findVal.length > 0) && 
									<Btn As="b" size="sm" kind="light" // flat
										className="qi qi-close xx"  
										onClick={() => {
											// if(findVal.length > 0) onFind("");// setFindVal | onFind
											// if(findVal.length > 0){
												setFindVal("");
												setFindArr([]);
											// }
										}}
									/>
								}

								<Btn As="b" size="sm" kind="light" 
									className="qi qi-search" 
									// className={"q-fw qi qi-" + (findVal.length > 0 ? "close xx":"search")} 
									// onClick={() => {
									// 	// if(findVal.length > 0) onFind("");// setFindVal | onFind
									// 	if(findVal.length > 0){
									// 		setFindVal("");
									// 		setFindArr([]);
									// 	}
									// }}
								/>
							</label>
						</div>

						{/* <Flex className="flexno">
							<Btn size="xs" kind="light" className="rounded-0 bw-x1 qi qi-redo" title="Reload" />
						</Flex> */}

						<Flex dir="column" align="stretch" className="flex1 h-100 position-relative wrap-icons">
							<List 
								awaitMore={findLength() > 0 ? findLength() > 0 : data.length > 0} 
								pageSize={70} 
								itemCount={findLength() > 0 ? findLength() : data.length} //  
								itemsRenderer={itemsRenderer} 
								renderItem={renderItem} 
								// onIntersection={(size, pageSize) => {
								// 	console.log('onIntersection size: ', size);
								// 	console.log('onIntersection pageSize: ', pageSize);
								// }}
							/>
						</Flex>

						{preview && // JSON.stringify(view, null, 2) //  style={{ width: w }}
							<Flex wrap align="stretch" className="flexno icon-view">
								{copy && 
									<div className="btn-group btn-group-xs w-100 ip-btn-copy">
										<Btn blur kind="light" 
											onClick={e => onCopy(e, meta ? meta.prefix + view.name : view.path)} 
											className="border-top-0 border-left-0 rounded-0 tip tipTL qi qi-copy"
										>
											Copy {meta ? "name" : "path"}
										</Btn>
										<Btn blur kind="light" 
											onClick={e => onCopy(e, meta ? view.code : view.name)} 
											className="border-top-0 rounded-0 tip tipTL qi qi-copy"
										>
											Copy {meta ? "code" : "file name"}
										</Btn>
										
										{/* OPTION: Check if user usage library */}
										{/* {APP_DOMAIN.includes(window.location.origin) ? 
											<Aroute to="/tools/icon-maker" btn="light" className="rounded-0 border-top-0 border-right-0">
												Create icons
											</Aroute>
											: 
											<Btn href={APP_DOMAIN[0] + "/tools/icon-maker"} kind="light" className="rounded-0 border-top-0 border-right-0" target="_blank">Create icons</Btn>
										} */}

										{/* <Btn href="/tools/icon-maker" kind="light" className="rounded-0 border-top-0 border-right-0">Create icons</Btn> */}
									</div>
								}

								<Flex justify="center" align="center" 
									// className={"flexno w-25 p-2 q-s22 " + (pref() || "") + (meta ? meta.prefix : "") + (meta ? view.name : "")} 
									className={
										Q.Cx("flexno w-25 p-2", {
											["q-s22 " + pref() + (meta ? meta.prefix : "") + view.name]: meta, 
											// []: meta
										})
									}
								>
									{!meta && 
										<Img 
											alt="Preview" 
											src={Q.baseURL + "/public" + view.path} 
											w={40} 
											h={40} 
											className="d-block flexno" 
										/>
									}
								</Flex>
								
								<Flex As="pre" align="center" 
									className="mb-0 p-2 small flex1 bg-light border-left q-scroll"
								>
									{meta ? 
										"name: " + (meta ? meta.prefix : "") + (view.name || "") + "\ncode: " + (view.code || "")
										: 
										"name: " + (view.name || "") + "\npath: " + (view.path || "") // view || ""
									}
								</Flex>
							</Flex>
						}
					</Flex>
					:
					<Flex dir="column" align="center" 
						className={"text-center p-5 " + (load ? "i-load cwait" : "qi qi-ban ired fa-2x")} 
						style={load ? { '--bg-i-load': '95px' } : undefined} 
					>
						{load ? "LOADING" : "ICONS NOT LOAD"}
					</Flex>
				}
			</Dropdown.Menu>
		</Dropdown>
	);
}

// export default React.memo(IconPicker, (prevProps, nextProps) => prevProps.icons === nextProps.icons);

