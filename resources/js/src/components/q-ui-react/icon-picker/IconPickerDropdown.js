import React, { useState } from 'react';// useRef, useEffect
import Dropdown from 'react-bootstrap/Dropdown';

export default function IconPickerDropdown({
  show = false, 
  findRef, 
  // data, 
  // meta, 
  // preview, 
}){
  const [dd, setDd] = useState(show);// false

	// useEffect(() => {
	// 	// console.log('%cuseEffect in IconPickerDropdown','color:yellow');
	// 	if(show){ // cacheIcon || 
	// 		onGetSetData();// null, cacheIcon
	// 	}else{
	// 		setLoad(true);
	// 	}
	// }, [show]);// , onGetSetData

	const onToggle = (isOpen) => { // , e, meta
		if(!data && isOpen){
			onGetSetData();
		}

		if(dd && meta && preview && view.name !== preview.name){
			// console.log('if 1: ',dd);
			setView(preview);
		}

		if(dd && !meta && preview){
			setView({ path: data[0], name: data[0].split("/").pop() });
		}

		if(dd && findVal !== ""){ // Reset find
			setFindVal("");
			setFindArr([]);
		}

		setDd(isOpen);

		// Set focus 
		if(!dd && autoFocus && findRef.current){
			setTimeout(() => {
				findRef.current.focus();
			},1);
		}
	}

  return (
		<Dropdown bsPrefix="btn-group" 
			show={dd} 
			onToggle={onToggle} 
		>
			<Dropdown.Toggle>{label}</Dropdown.Toggle>{/*  id={id ? id : Q.Qid()} */}
			<Dropdown.Menu 
				// renderOnMount 
				className={Q.Cx("p-0 ovhide icon-picker", { "w-200px": !data })}
			>

      </Dropdown.Menu>
    </Dropdown>
  );
}
