import React from 'react';
import { format, cancel, render } from 'timeago.js';

/**
 * Convert input to a valid datetime string of <time> tag
 * https://developer.mozilla.org/en-US/docs/Web/HTML/Element/time
 * @param input
 * @returns datetime string
 */
const toDateTime = (input) => {
  // let date: Date = new Date();
  // if (input instanceof Date) {
  //   date = input;
  //   //@ts-ignore
  // } else if (!isNaN(input) || /^\d+$/.test(input)) {
  //   //@ts-ignore
  //   date = new Date(parseInt(input));
  // } else {
  //   date = new Date(input);
  // }

  // try {
  //   return date.toISOString();
  // } catch (e) {
  //   console.error('invalid datetime');
  //   return '';
  // }

  return '' + (input instanceof Date ? input.getTime() : input);
};

export default class Time extends React.Component{
  constructor(props){
    super(props);
    // this.state = {

    // };
		this.el = null;
  }

  componentDidMount(){
    // console.log('%ccomponentDidMount in Time','color:yellow;');
    // fixed #6 https://github.com/hustcc/timeago-react/issues/6
    // to reduce the file size.
    // const { locale } = this.props;
    // if (locale !== 'en' && locale !== 'zh_CN') {
    //   timeago.register(locale, require('timeago.js/locales/' + locale));
    // }
    // render it.
    this.renderTimeAgo();
  }

  componentDidUpdate(){
    this.renderTimeAgo();
  }

  renderTimeAgo(){
    const { live, datetime, locale, opts } = this.props;
    // cancel all the interval
    cancel(this.el);
    // if is live
    if (live !== false) {
      // live render
      // this.el.setAttribute('datetime', toDateTime(datetime));
      this.el.dateTime = toDateTime(datetime);

      render(this.el, locale, opts);
    }
  }

  // remove
  componentWillUnmount(){
    cancel(this.el);
  }

  render(){
    const { As, datetime, live, locale, opts, ...etc } = this.props;
    const time = format(datetime, locale, opts);

    return (
      <As 
        {...etc} 

        ref={(c) => this.el = c} // 
        // title={time} 
      >
        {time}
      </As>
    );
  }
}

Time.defaultProps = {
  As: "time",
  live: true,
  // className: '',
};
