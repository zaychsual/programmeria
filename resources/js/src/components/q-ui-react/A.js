import React, { forwardRef } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

// import Btn from '../../components/q-ui-react/Btn';

const A = forwardRef(
	({
		href, 
		target, 
		rel, 

		kind, 
		nav, 
		btn, 
		size, 
		outline, 
		dropdown, 
		listGroup, 
		noLine, 
		stretched, 
		className, 
		disabled, 
		role, 
		tabIndex, 
		// noNewTab, 
		// children, 
		// onMouseOver = Q.noop, 
		onMouseEnter = Q.noop, 
		onClick = Q.noop, 
		onContextMenu = Q.noop,
		...etc
	}, 
	ref
) => {
	// const [data, setData] = useState();
	const origin = window.location.origin;// Q.baseURL
	const isBlank = href && (!href.startsWith(origin) && !href.startsWith("/")) && target === "_blank";

	// console.log('isBlank: ', isBlank);

	// const onDisabled = e => {
	// 	if(disabled){
  //     e.preventDefault();
  //     return;
	// 	}
	// }

	// const MouseOver = e => {
	// 	// onDisabled(e);
	// 	if(disabled){
	// 		e.preventDefault();
	// 		e.stopPropagation();
  //     return;
	// 	}

	// 	onMouseOver(e);
	// }
	
	const MouseEnter = e => {
		if(disabled){
      e.preventDefault();
      return;
		}

		if(isBlank){ //  && href
			// const et = e.target;
			let link = href; // href || et.href

			e.target.href = decodeURIComponent(link.replace(origin + "/blank/?u=", ""));
			
			// if(!link?.startsWith(origin)){ // link?.startsWith(origin)
			// 	// decodeURI | decodeURIComponent
			// 	et.href = decodeURIComponent(link.replace(origin + "/blank/?u=", ""));
			// }
		}

		// if(noNewTab){
		// 	Q.setAttr(et, "href");
		// }

		onMouseEnter(e);
	}
	
	const Click = e => {
		const et = e.target;

		if(disabled){
      e.preventDefault();
      return;
		}

		if(isBlank){ //  && href
			e.preventDefault();
			window.open(origin + "/blank/?u=" + encodeURIComponent(href), "_blank");
		}

		// Close Dropdown menu if component in <Dropdown.Menu />
    if(Q.hasClass(et, "dropdown-item")){
			document.body.click();
		}
		
		onClick(e);
	}

	const CtxMenu = e => {
		if(disabled){
      e.preventDefault();
      return;
		}

		if(isBlank){ //  && href
			// const et = e.target;
			// const link = href;//  || et.href

			e.target.href = origin + "/blank/?u=" + encodeURIComponent(href);

			// if(!href?.startsWith(origin)){ // Q.baseURL
			// 	et.href = origin + "/blank/?u=" + encodeURIComponent(href);// link
			// }
		}

		onContextMenu(e);
	}

	return (
		<a
			{...etc} 
			ref={ref} 
			href={href} 
			target={target} 
			// isBlank && href && !rel ? "noreferrer noopener" : rel
			rel={isBlank && !rel ? "noreferrer noopener" : rel} 

			// onMouseOver={MouseOver} 
			onMouseEnter={MouseEnter} 
			onClick={Click} 
			onContextMenu={CtxMenu} 

			className={
				Q.Cx(`${btn ? "btn btn" : ""}${btn && outline ? "-outline" : ""}${btn ? "-" + btn : ""}`, {
					"nav-link" : nav, 
					["btn-" + size] : btn && size, // [`btn-${size}`]
					["text-" + kind] : kind, // [`text-${kind}`]
					"text-decoration-none": noLine, 
					"stretched-link": stretched, 
					"dropdown-item" : dropdown, 
					"list-group-item list-group-item-action": listGroup,
					"disabled" : disabled, 
					// "route": !btn && !nav && !noLine && !dropdown && noNewTab
				}, className)
			}
			role={btn && !role ? "button" : role} 
			// title={qtip ? null : tip} 
			// aria-label={(!children || typeof children === 'undefined' || qtip) && tip ? tip : null} 
			tabIndex={disabled && !tabIndex ? -1 : tabIndex} // {disabled ? "-1" : null}
			aria-disabled={disabled || null} // disabled ? true : null
		/>
	);
});

export default A;

/*

*/
