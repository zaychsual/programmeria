import React from 'react';
import { useNetwork } from 'ahooks';

export default function NetWork({ children }){
	const network = useNetwork();
	
	// React.useEffect(() => {
	// 	console.log('%cuseEffect in NetWork','color:yellow;');
	// }, []);

	return (
    <React.Fragment>
      {children && children(network)}
    </React.Fragment>
	);
}

