import React, { useState } from 'react';// useRef, 
import Dropdown from 'react-bootstrap/Dropdown';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import { HexColorInput, HexColorPicker, RgbaStringColorPicker } from 'react-colorful';// RgbaStringColorPicker, RgbStringColorPicker, RgbColorPicker, RgbaColorPicker

import Input from '../Input';
import Btn from '../Btn';
import darkOrLight from '../../../utils/darkOrLight';
import { hex2Rgb, rgb2hex } from '../../../utils/colorConverter';

let CSS_COLORS = [];

export function ColorPicker({
  open = false, 
  rgba = true, 
  colorName = true, 
  value = "#ffffff", 
  inputProps = {}, 
  id, 
  className, 
  toggleClass, 
  dropdownMenuProps, 
  onChange = () => {}, 
}){
  const ID = id || Q.Qid();
  const INIT_CN = { c: "", name: "" };
  // const findRef = useRef();
  const [see, setSee] = useState(open);
  // const [ddColorName, setDdColorName] = useState(false);
  // const [color, setColor] = useState(value);
  const [findColorName, setFindColorName] = useState("");
  const [findColorNames, setFindColorNames] = useState([]);
  const [useColorName, setUseColorName] = useState(INIT_CN);
  const [cssColors, setCssColor] = useState(CSS_COLORS);

  // const [valType, setValType] = useState("");
  const [tab, setTab] = useState("Hexa");

  const Change = (e, obj) => {
    // console.log('Change e: ', e);
    // setColor(e);
    
    // cssColors.find(f => (f.hex || f.hex.toLowerCase()) === e);
    const isName = cssColors.map(v => v.colors).flat().find(f => (f.hex || f.hex.toLowerCase()) === e); // .find(f => f)
    // console.log('isName: ', isName);
    setUseColorName(isName ? { c: e, name: isName.name } : INIT_CN);

    onChange(e, obj);
  }

  const Toggle = (isOpen, key) => {
		const docActive = document.activeElement;
		// console.log('onToggleTheme docActive: ', docActive);
		// console.log('onToggleTheme docActive.dataset.color: ', docActive.dataset.color);
		// console.log('onToggleTheme key: ', key);
		// console.log('onToggleTheme see: ', see);
		// console.log('onToggleTheme themePick: ', themePick);
		if(see && docActive.dataset.color === key){ //  && see && docActive 
			setSee(true);
		}else{
			setSee(isOpen);
		}

    setFindColorName("");
    setFindColorNames([]);
	}

  const ToggleColorName = () => { // isOpen, e
    // setDdColorName(isOpen);
    setFindColorName("");
    setFindColorNames([]);

    if(!CSS_COLORS.length){
      import(/* webpackIgnore:true */Q.baseURL + "/js/esm/const/CSS_COLORS.js")
      .then(m => {
        console.log('m: ', m);
        if(m && m.default){
          CSS_COLORS = m.default;
          setCssColor(m.default);
        }
      }).catch(e => console.warn('e: ', e));
    }

    // setTimeout(() => findRef.current.focus(), 1);

    // if(isOpen && findRef.current){
		// 	setTimeout(() => findRef.current.focus(), 1);
    // }

    // if(isOpen && e){
    //   // console.log(e);
    //   const dd = e.target.nextElementSibling;
    //   if(dd){
    //     setTimeout(() => {
    //       const list = dd.lastElementChild;
    //       if(list){
    //         const rect = Q.domQ(".active", list)?.getBoundingClientRect();
    //         console.log('rect: ', rect);
    //         rect && list.scroll(0, rect.y - rect.height);
    //       }
    //     }, 9);
    //   }
    // }
  }

  const onClickColorName = (val) => {
    console.log('onClickColorName val: ', val);
    // setDdColorName(false);
    setFindColorName("");
    setFindColorNames([]);
    Change(val.hex, val);// .replace("#","")
  }

  const onTab = (k) => {
    setTab(k);
    // console.log('====== k ======: ', k);
    switch(k){
      case "Hexa":
        // console.log('rgb2hex: ', rgb2hex(value, 1));
        if(value.startsWith("rgba")) Change(rgb2hex(value, 1));
        break;
      case "Rgba":
        // console.log('hex2Rgb: ', hex2Rgb(value));
        if(!value.startsWith("rgba")) Change("rgba(" + (hex2Rgb(value).str) + ",1)");
        break;
      default:
        ToggleColorName();
        break;
    }
  }

  const renderColorName = (v, i) => {
    return (
      <button key={i} type="button" 
        // (v.hex || v.hex.toLowerCase()) === color | useColorName.name === color
        className={Q.Cx("dropdown-item d-flex align-items-center", { "active": (v.hex || v.hex.toLowerCase()) === value })} // color
        onClick={() => onClickColorName(v)}
      >
        <div style={{ backgroundColor: v.name }} className="p-2 mr-2 border" />
        {v.name}
      </button>
    )
  }

  // console.log('color: ', color);
  // console.log('hex === color: ', "#FA8072".toLowerCase() === color);
  return (
    <Dropdown tabIndex="0" 
      show={see} 
      onToggle={(isOpen) => Toggle(isOpen, ID)} 
      // onBlur={() => setSee(false)} // DEVS
      className={className} 
    >
      <Dropdown.Toggle as="div" 
        bsPrefix={Q.Cx("wrap-input-color input-group", toggleClass)} 
        tabIndex="0" 
        data-color={ID} 
      >
        <label htmlFor={ID} className="input-group-prepend mb-0">
          <div className="input-group-text text-monospace">{tab !== "Rgba" ? "#" : "@"}</div>
        </label>

        {tab === "Hexa" ? 
          <HexColorInput 
            {...inputProps} 
            color={value} // color
            onChange={Change} 
            type="text" 
            data-color={ID} 
            id={ID} 
            className={
              Q.Cx("form-control", { 
                ["text-" + (darkOrLight(value.replace("#","")) === "dark" ? "white":"dark")] : value !== ""
              }, inputProps.className)
            } 
            style={{ ...inputProps.style, backgroundColor: value }}
          />
          : 
          <Input spellCheck={false} 
            data-color={ID} 
            id={ID} 
            className={
              Q.Cx("text-monospace", { 
                ["text-" + (darkOrLight(rgb2hex(value, 1).replace("#","")) === "dark" ? "white":"dark")] : value !== ""
              }, inputProps.className)
            } 
            style={{ ...inputProps.style, backgroundColor: value }}
            value={value} 
            onChange={e => Change(e.target.value)} 
          />
        }
      </Dropdown.Toggle>
      
      <Dropdown.Menu 
        {...dropdownMenuProps} 
        className={Q.Cx("p-1", dropdownMenuProps?.className)} // w-100
        // popperConfig={{
        //   strategy: "fixed"
        // }} 
        // onContextMenu={Q.preventQ} 
        // onClick={e => e.stopPropagation()} 
      >
        <Tabs mountOnEnter 
          id={"tcp" + ID} 
          className="link-sm" 
          variant="pills" 
          activeKey={tab}
          onSelect={onTab} 
        >
          <Tab eventKey="Hexa" title="Hexa">
            <div>
              <HexColorPicker className="w-100" 
                color={value} // color
                onChange={Change} // setColor
              />
            </div>
          </Tab>

          {/* <Tab eventKey="Rgb" title="Rgb">
            <div>
              <RgbStringColorPicker className="w-100" 
                color={"rgb(255, 255, 255)"} // { r: 255, g: 255, b: 255 }
                onChange={(val) => {
                  console.log('onChange RgbStringColorPicker val: ', val);
                }}
              />
            </div>
          </Tab> */}

          {rgba && 
            <Tab eventKey="Rgba" title="Rgba">
              <div>
                <RgbaStringColorPicker className="w-100" 
                  color={value} // "rgba(255, 255, 255, 1)" | { r: 255, g: 255, b: 255, a: 1 }
                  onChange={Change} 
                />
              </div>
            </Tab>
          }

          {colorName && 
            <Tab eventKey="Color" title="Name">
              <div className="mx-n1">
                <label className="input-group input-group-sm p-1">
                  <Input type="search" placeholder="Search" // isize="sm" // id={"fc-" + ID}
                    // autoFocus // ={ddColorName} 
                    // ref={findRef} 
                    value={findColorName} 
                    onChange={e => {
                      const val = e.target.value;
                      setFindColorName(val);

                      setTimeout(() => {
                        const vlow = val.trim().toLowerCase();
                        // f.categories.toLowerCase().includes(vlow) || 
                        const finds = cssColors.map(f => f.colors.filter(f2 => f2.name.toLowerCase().includes(vlow))).flat();
                        // console.log('finds: ', finds);
                        setFindColorNames(finds);
                      }, 150);
                    }}
                  />
                  <div className="input-group-append">
                    <div className="input-group-text qi qi-search" />
                  </div>
                </label>
                
                <div className="ovyauto mt-2-next"
                  style={{ maxHeight: 160 }} // 250
                >
                  {
                    findColorNames.length > 0 ? 
                      findColorNames.map((v, i) => renderColorName(v, i))
                      : 
                      cssColors.map((v, i) => (
                        <div key={i}>
                          <h6 className="dropdown-header border bw-y1 bg-light position-sticky t0 zi-1">{v.categories}</h6>
                          {v.colors.map((v2, i2) => renderColorName(v2, i2))}
                        </div>
                      )
                  )}
                </div>
              </div>
            </Tab>
          }
        </Tabs>

        <Btn kind="light" className="w-100 text-left shadow-none mt-1">{useColorName.name || "Choose Color"}</Btn>

        {/* <Dropdown 
          show={ddColorName} 
          // drop="right" 
          onToggle={ToggleColorName} // (isOpen) => Toggle(isOpen, ID)
          // onClick={e => e.stopPropagation()} 
        >
          <Dropdown.Toggle 
            variant="light" 
            className="w-100 text-left shadow-none mt-1" 
          >
            {/ {(useColorName.c || useColorName.c.toLowerCase()) === color ? useColorName.name : "Choose Color"} /}
            {useColorName.name || "Choose Color"}
          </Dropdown.Toggle>
          
          <Dropdown.Menu 
            // flip={false} 
            // align="right" 
            className="py-0" //  ovyauto bg-clip-inherit
            // style={{
            //   maxHeight: 250
            // }}
          >
            
          </Dropdown.Menu>
        </Dropdown> */}
      </Dropdown.Menu>
    </Dropdown>
  );
}