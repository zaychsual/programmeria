import React from 'react';

// className:
const FC = "form-control";
const CS = "custom-select";

export default function InputQ({
	WrapAs = "label", 
	As = "input", 
	type = "text", 
	wrap, 
	inRef, 
	children, 
	disabled, 
	onChange, 
	classNames, 
	wrapProps, 
	label, 
	// htmlFor, 
	qSize, 
	// isValid, 

	...etc
}){
  // const [data, setData] = React.useState();
	
	// React.useEffect(() => {
	// 	console.log('%cuseEffect in Input','color:yellow;');
	// }, []);

	const isInput = As === "input";

	const Change = e => {
		if(disabled){
			e.preventDefault();
			return;
		}

		if(onChange && !disabled) onChange(e);
	}

	const Input = () => (
		<As 
			{...etc} 
			ref={inRef} 
			className={
				Q.Cx(
					isInput ? FC : CS, 
					qSize && (isInput ? FC + "-" + qSize : CS + "-" + qSize), 
					// isValid ? "is-valid" : "is-invalid"
					// [isValid ? "is-valid" : "is-invalid"] 
				)
			} 
			type={isInput ? type : undefined} 
			disabled={disabled} 
			onChange={Change} 
		/>
	);

	if(wrap){ // children
		return (
			<WrapAs 
				{...wrapProps} 
				className={Q.Cx("q-input", classNames)} 
				aria-label={label} 
				// htmlFor={WrapAs === "label" ? htmlFor : undefined} 
			>
				{Input()} 

				{children} 
			</WrapAs>
		);
	}

	return Input();
}

/*
<React.Fragment></React.Fragment>
*/
