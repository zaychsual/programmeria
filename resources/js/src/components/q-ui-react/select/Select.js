import React, { useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import { useSelect } from 'downshift';
import usePopper from 'react-overlays/usePopper';// react-popper

import Btn from '../Btn';

// const makeVirtualEl = (x = 0, y = 0) => ({
//   getBoundingClientRect: () => ({
//     width: 0,
//     height: 0,
//     top: y,
//     right: x,
//     bottom: y,
//     left: x
//   })
// });

export default function Select({
  prefixClass = "q-select btn-group", 
  className, 
  menuProps = {}, 
  kind = "light", 
  size, 
  placeholder, // OPTION: label
  renderFirst = false, // NOT FIX name
  Arrow = {
    As: Btn, 
    kind, 
    className: "dropdown-toggle px-2" // dropdown-toggle-split
  }, 
  Clear = {
    As: Btn, 
    kind, 
    className: "px-2 qi qi-close"
  }, 
  options = [], 
  defaultValue, 
  value, 
  // onToggle = () => {}, 
  onChange, //  = () => {}
  // onClear = () => {}, 
  portal, 
  popper, 
  popperConfig, 
}){
  const items = Array.isArray(options) ? options : [];
  // defaultValue && !value && !Q.isFunc(onChange) ? defaultValue : null
  const initialSelectedItem = defaultValue || value;
  // const initialHighlightedIndex = items.findIndex(f => f === defaultSelectedItem);

  const toggleRef = useRef(null);
  const menuRef = useRef(null);
  const [popperRef, setPopperRef] = useState(null);
  const [popperEl, setPopperEl] = useState(null);
  const { styles, attributes } = usePopper(popperRef, popperEl, popperConfig);

  const {
    isOpen, 
    selectedItem, 
    getToggleButtonProps, 
    // getLabelProps, 
    getMenuProps, 
    highlightedIndex, 
    highlightedItem, 
    getItemProps, 
    reset, 
  } = useSelect({ 
    items, 
    // selectedItem: value, // Controlled
    initialSelectedItem, // Uncontrolled = defaultValue || value
    // defaultSelectedItem: value, 
    // initialHighlightedIndex, 
    // defaultHighlightedIndex: initialHighlightedIndex, 
    onIsOpenChange: (data) => {
      console.log('onIsOpenChange data: ', data);
      // if(data.isOpen){
      //   menuRef
      // }
    }
  });

  const [once, setOnce] = useState(renderFirst);

  const onClick = () => { // e
    // const et = e.target;
    // onToggle(isOpen, e);
    if(!once) setOnce(true);

    if(popper && !popperEl){
      setPopperRef(toggleRef.current);
      setPopperEl(menuRef.current); // { ref: toggleRef.current, el: menuRef.current }
    }
  }

  const Reset = e => {
    e.stopPropagation();
    reset();
    // onClear("", e); // NOT WORK: selectedItem always previous value
  }

  const onSelectItem = (item, index, e) => {
    if(Q.isFunc(onChange)) onChange(item, index, e);
  }

  const renderMenu = () => {
    const popperAttr = popper ? attributes.popper : {};

    return (
      <div {
        ...getMenuProps({
          ...menuProps, 
          ref: menuRef, 
          style: popper ? { ...styles.popper, ...menuProps.style } : menuProps.style, 
          ...popperAttr, 
          className: Q.Cx("q-select-menu dropdown-menu ovyauto", { "show": isOpen }, menuProps.menuClass), 
          onFocus: e => {
            e.nativeEvent.preventDownshiftDefault = true;// NOT WORK
            e.preventDefault();
            e.stopPropagation();
            // return;
          },
        })
      }>
        {(isOpen || once) && items.map((item, index) => (
          <button type="button" 
            {
              ...getItemProps({
                item, 
                index, 
                key: index, 
                className: Q.Cx("q-option dropdown-item", { 
                  highlighted: highlightedIndex === index && highlightedItem === item, 
                  active: selectedItem === item
                }), 
                // onFocus: e => {
                //   e.preventDefault();
                //   e.stopPropagation();
                // }, 
                onClick: e => onSelectItem(item, index, e), 

              })
            } 
          >
            {item}
          </button>
        ))}
      </div>
    )
  }

// <label {...getLabelProps()}>Choose an element:</label>
  return (
    <div 
      role="listbox" // Accessibility
      className={Q.Cx(prefixClass, { "show": isOpen }, className)} 
    >
      <div 
        {
          ...getToggleButtonProps({ 
            onClick, 
            ref: toggleRef, 
            className: Q.Cx("btn-group", { ["btn-group-" + size]: size }), 
            tabIndex: 0, 
          })
        }
      >
        <Btn 
          kind={kind} 
        >
          {selectedItem || placeholder} 
        </Btn>

        {(Clear && selectedItem) && //  && initialSelectedItem !== selectedItem
          <Clear.As 
            kind={Clear.kind} 
            className={Clear.className} 
            onClick={Reset}
          />
        }
        
        {Arrow && 
          <Arrow.As 
            kind={Arrow.kind} 
            className={Arrow.className} 
          />
        }
      </div>

      {portal ? 
        ReactDOM.createPortal(renderMenu(), portal) 
        : 
        renderMenu()
      }

      {/* if you Tab from menu, focus goes on button, and it shouldn't. only happens here. */}
      {/* <div tabIndex="0" /> */}
    </div>
  )
}

