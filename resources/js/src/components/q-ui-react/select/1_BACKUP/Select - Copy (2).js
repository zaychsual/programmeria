import React from 'react';// , { useState }
import { useSelect } from 'downshift'; // Downshift
// import Dropdown from 'react-bootstrap/Dropdown';
import { Popper, Manager } from 'react-popper';// , Target

import Btn from '../Btn';

export default function Select({
	options, // Option component 

	open, 
	downshiftProps, 
	// downshiftPropsEtc, 
	downshiftMenuProps, 
	dropdownProps, 
	dropdownToggleProps, 
	// dropdownMenuProps, 
	// inputProps, 
	
	// search, 
	// keyValue, 
	// multiple, 
	// value, // selected, 
	placeholder = "Choose", 
	// kind = "light", 
	// size, // input & tag size
	// notFound = "Not Found", // not Found text / component
	// onRemoveItem = Q.noop, // onRemoveItem | onChange
}){
	// if(!options) return null;
	// const [openMenu, setOpenMenu] = useState(open);
	
	const {
		isOpen, 
		// inputValue, 
		selectedItem, 
		// getRootProps, 
		// getInputProps, 
    getToggleButtonProps, 
    // getLabelProps, 
    getMenuProps, 
    highlightedIndex, 
    getItemProps, 
	} = useSelect({ ...downshiftProps, items: options });
	
	return (
		<div className="dropdown">
			<Manager>
				<>
					<Btn
						{...getToggleButtonProps()} 
						{...dropdownToggleProps} 
					>
						{selectedItem || placeholder}
					</Btn>

					<div {...getMenuProps({ ...downshiftMenuProps })}>
						{open || isOpen && (
							<Popper
								placement={selectedItem || "bottom"}
								style={{ backgroundColor: "skyblue" }}
							>
								<div className="dropdown-menu show">
									{options.map((item, index) => 										
										<button type="button" 
											{...getItemProps({
												key: index, // item.pathname,
												index,
												item, 
												active: selectedItem === item, 
												className: highlightedIndex === index && "highlighted"
											})}
										>
											{item}
										</button>
									)}
								</div>
							</Popper>
						)}
					</div>
				</>
			</Manager>
		</div>
	);
}

/*
		<Dropdown 
			{...dropdownProps} 
			show={open || isOpen} 
		>

			<div {...getMenuProps({ ...downshiftMenuProps, suppressRefError : true })}>
				<Dropdown.Menu {...dropdownMenuProps}>

				</Dropdown.Menu>
			</div>
		</Dropdown>
*/

