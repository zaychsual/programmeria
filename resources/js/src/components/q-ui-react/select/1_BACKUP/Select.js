import React, { useState } from 'react';// 
import { useCombobox } from 'downshift'; // , useSelect || Downshift
import Dropdown from 'react-bootstrap/Dropdown';

import Btn from '../Btn';

export default function Select({
	options, // Option component 

	open = false, 
	downshiftProps, 
	// downshiftPropsEtc, 
	downshiftMenuProps, 
	dropdownProps, 
	dropdownToggleProps, 
	dropdownMenuProps, 
	inputProps, 
	
	search, 
	kind = "light", 
	size, // input & tag size
	comboboxClass, 
	clear = true, 
	// keyValue, 
	// multiple, 
	value, // = "", 
	// selected, 
	placeholder = "Choose", 
	notFound = "Not Found", // not Found text / component
	onKeyDown = Q.noop, 
	onChange = Q.noop, 
}){
	// if(!options) return null;
	const [openMenu, setOpenMenu] = useState(open);
	const [items, setItems] = useState(options);
	const [select, setSelect] = useState(value);
	const [inputVal, setInputVal] = useState(value || "");
	
	const {
		isOpen, 
		inputValue, 
		selectedItem, 
		// getRootProps, 
		getInputProps, 
		getComboboxProps, 
    getToggleButtonProps, 
    // getLabelProps, 
    getMenuProps, 
    // highlightedIndex, 
		getItemProps, 
	} = useCombobox({ 
		...downshiftProps, 
		items, 
    onSelectedItemChange: (val) => { // changes
      // console.log('val: ', val);
			setOpenMenu(false);
			setInputVal(val.selectedItem);
			setSelect(val.selectedItem);
			onChange(val);
    },
    onInputValueChange: ({ inputValue }) => {
      onFilter(inputValue);
    },
	});

	const onFilter = (val) => {
		setItems(options.filter((item) => item.toLowerCase().startsWith(val.toLowerCase())))
	}

	const keyDown = e => {
		e.stopPropagation();
		onKeyDown(e);
	}

	const onClear = e => {
		e.stopPropagation();
		// console.log('onClear');
		setSelect(null);
		setItems(options);
		onChange(null);
	}

	const isSearch = search && getToggleButtonProps();
	// console.log('clearSelection: ', clearSelection);
	
	return (
		<Dropdown 
			{...dropdownProps} 
			// drop={open || isOpen ? "up" : "down"} 
			className={Q.Cx("q-select", dropdownProps?.className)} 
			show={openMenu || isOpen} 
			onToggle={(show) => {
				setOpenMenu(show);
				// console.log('inputValue: ', inputValue);
				// console.log('selectedItem: ', selectedItem);

				// if(show && inputValue === selectedItem){
				// 	console.log('if');
				// 	setItems(options);
				// }
			}}
		>
			<div {...getComboboxProps({ className: Q.Cx("q-select-toggle", comboboxClass) })}>
				<Dropdown.Toggle 
					as={search ? "div" : Btn} 
					bsPrefix={search ? "q-select-find" : undefined} 
					size={search && size ? size : null} 
					kind={search && kind ? kind : null} 
					{...isSearch} 
					// {...getToggleButtonProps({
					// 	// onKeyDown: e => {
					// 	// 	const et = e.target;
					// 	// 	console.log('et: ', et);
					// 	// 	console.log('e.key: ', e.key);
					// 	// 	if(e.key === "Tab"){
					// 	// 		e.preventDefault();
					// 	// 		e.stopPropagation();
					// 	// 		et.blur();
					// 	// 	}
					// 	// }
					// 	// onKeyDown: e => keyDown(e)
					// })} 
					{...dropdownToggleProps} 
					onSelect={() => {
						// console.log('inputValue: ', inputValue);
						// console.log('selectedItem: ', selectedItem);
						if(inputValue === selectedItem) setItems(options);
					}}
				>
					{search ? 
						<div className="input-group">
							<input 
								{ ...getInputProps(
										{ 
											...inputProps, 
											// type: clear ? "search" : "text", 
											type: "text", 
											value: inputVal, 
											onChange: e => setInputVal(e.target.value), 
											onKeyDown: keyDown, 
											placeholder, 
											// className: Q.Cx("custom-select", { 
											// 	["custom-select-" + size]: size 
											// }, inputProps?.className) 
										}
									)
								} 
							/>

							{(clear && select && inputVal.length > 0) && 
								<div className="input-group-append">
									<Btn size={size} kind="light" className="qi qi-close xx" 
										onClick={e => {
											setInputVal("");
											onClear(e);
										}}
									/>
								</div>
							}
						</div>
						:
						<span 
							{...getToggleButtonProps({
								onKeyDown: keyDown
							})} 
							{...getInputProps({
								onKeyDown: keyDown // e => keyDown(e)
							})} 
						>
							{/* {(!search && clear ? (select || placeholder) : (selectedItem || placeholder))}  */}
							{select || placeholder} 

							{(clear && select) && 
								<b onClick={onClear} className="close float-none">&times;</b>
							}
						</span>
					}
				</Dropdown.Toggle>
			</div>

			<div {...getMenuProps({ ...downshiftMenuProps })} tabIndex="-1">
				<Dropdown.Menu 
					{...dropdownMenuProps} 
					className={Q.Cx("bg-clip-inherit w-100", dropdownMenuProps?.className)} 
					tabIndex="-1" 
				>
					{items.length > 0 ? 
						items.map((item, index) => 										
							<Dropdown.Item 
								{...Q.DD_BTN} 

								{...getItemProps({
									key: index, // item.pathname,
									index,
									item, 
									// active: (!search && clear ? select : selectedItem) === item && inputValue === item, 
									active: select === item && inputValue === item, 
									// className: highlightedIndex === index && "highlighted"
									// onMouseOver: e => {
									// 	// console.log(e.nativeEvent);
									// 	e.nativeEvent.preventDownshiftDefault = true;
									// 	e.preventDefault();
									// 	e.stopPropagation();
									// }
								})}
							>
								{item}
							</Dropdown.Item>
						)
						: 
						Q.isStr(notFound) ? <h6 className="dropdown-header">{notFound}</h6> : notFound
					}
				</Dropdown.Menu>

				{/* if you Tab from menu, focus goes on button, and it shouldn't. only happens here. */}
				{/* <div tabIndex="0" /> */}
			</div>
		</Dropdown>
	);
}

