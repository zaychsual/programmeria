import React, { useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import { useSelect } from 'downshift';
import Dropdown from 'react-bootstrap/Dropdown';
// import usePopper from 'react-overlays/usePopper';// react-popper

import Btn from '../Btn';

// const makeVirtualEl = (x = 0, y = 0) => ({
//   getBoundingClientRect: () => ({
//     width: 0,
//     height: 0,
//     top: y,
//     right: x,
//     bottom: y,
//     left: x
//   })
// });

export default function Select({
  // prefixClass = "q-select btn-group", 
  className, 
  menuProps = {}, 
  kind = "primary", 
  size, 
  placeholder, // OPTION: label
  renderFirst = false, // NOT FIX name
  Arrow = {
    As: Btn, 
    kind, 
    className: "dropdown-toggle px-2" // dropdown-toggle-split
  }, 
  Clear = {
    As: Btn, 
    kind, 
    className: "px-2 qi qi-close"
  }, 
  options = [], 
  defaultValue, 
  value, 
  // onToggle = () => {}, 
  onChange, //  = () => {}
  // onClear = () => {}, 
  portal, 
  // popperConfig, 
}){
  const items = Array.isArray(options) ? options : [];
  const defaultSelectedItem = defaultValue && !value && !Q.isFunc(onChange) ? defaultValue : null;
  // const initialHighlightedIndex = items.findIndex(f => f === defaultSelectedItem);

  // const toggleRef = useRef(null);
  // const menuRef = useRef(null);
  // const [popperRef, setPopperRef] = useState(null);
  // const [popperEl, setPopperEl] = useState(null);
  // const { styles, attributes } = usePopper(popperRef, popperEl, popperConfig);

  const {
    isOpen, 
    selectedItem, 
    getToggleButtonProps, 
    // getLabelProps, 
    getMenuProps, 
    highlightedIndex, 
    highlightedItem, 
    getItemProps, 
    reset, 
  } = useSelect({ 
    items, 
    // selectedItem: value, // Controlled
    initialSelectedItem: defaultSelectedItem, // Uncontrolled = defaultValue || value
    defaultSelectedItem, 
    // initialHighlightedIndex, 
    // defaultHighlightedIndex: initialHighlightedIndex, 
  });

  const [once, setOnce] = useState(renderFirst);

  // const onClick = () => { // e
  //   // const et = e.target;
  //   // onToggle(isOpen, e);
  //   if(!once) setOnce(true);

  //   // if(!popperEl){
  //   //   setPopperRef(toggleRef.current);
  //   //   setPopperEl(menuRef.current); // { ref: toggleRef.current, el: menuRef.current }
  //   // }
  // }

  const Toggle = () => {
    if(!once) setOnce(true);
  }

  const Reset = e => {
    e.stopPropagation();
    reset();
    // onClear("", e); // NOT WORK: selectedItem always previous value
  }

  const onSelectItem = (item, index, e) => {
    if(Q.isFunc(onChange)) onChange(item, index, e);
  }

  const renderMenu = () => {
    return (
      <Dropdown.Menu {
        ...getMenuProps({
          ...menuProps, 
          // ref: menuRef, 
          // style: styles.popper, 
          // ...attributes.popper, 
          className: Q.Cx("q-select-menu ovyauto", menuProps.menuClass), // , { "show": isOpen }
          // onFocus: e => {
          //   e.preventDefault();
          //   e.stopPropagation();
          // },
        })
      }>
        {(isOpen || once) && items.map((item, index) => (
          <button type="button" 
            {
              ...getItemProps({
                item, 
                index, 
                key: index, 
                className: Q.Cx("q-option dropdown-item", { 
                  highlighted: highlightedIndex === index && highlightedItem === item, 
                  active: selectedItem === item
                }), 
                // onFocus: e => {
                //   e.preventDefault();
                //   e.stopPropagation();
                // }, 
                onClick: e => onSelectItem(item, index, e), 

              })
            } 
          >
            {item}
          </button>
        ))}
      </Dropdown.Menu>
    )
  }

// <label {...getLabelProps()}>Choose an element:</label>
  return (
    <Dropdown 
      role="listbox" // Accessibility
      // bsPrefix={Q.Cx("q-select btn-group", className)} // , { "show": isOpen } | className
      onToggle={() => Toggle()} 
    >
      <Dropdown.Toggle 
        {
          ...getToggleButtonProps() // { ref: toggleRef } onClick, 
        }
        as="div" 
        tabIndex="0" 
        className={Q.Cx("btn-group", { ["btn-group-" + size]: size })} // className | bsPrefix
      >
        <Btn kind={kind}>
          {selectedItem || placeholder} 
        </Btn>

        {(Clear && selectedItem && defaultSelectedItem !== selectedItem) && //  && !defaultSelectedItem
          <Clear.As 
            kind={Clear.kind} 
            className={Clear.className} 
            onClick={Reset}
          />
        }
        
        {Arrow && 
          <Arrow.As 
            kind={Arrow.kind} 
            className={Arrow.className} 
          />
        }
      </Dropdown.Toggle>

      {portal ? 
        ReactDOM.createPortal(renderMenu(), portal) 
        : 
        renderMenu()
      }

      {/* if you Tab from menu, focus goes on button, and it shouldn't. only happens here. */}
      <div tabIndex="0" />
    </Dropdown>
  )
}

