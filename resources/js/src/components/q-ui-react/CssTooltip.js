import React, { forwardRef } from 'react';

const CssTooltip = forwardRef(
  ({
    As = "div", 
    className, 
    pos = "TL", 
    animate = true, 
    color, 
    onMouseEnter = () => {}, 
    onMouseLeave = () => {}, 
    ...etc
  }, 
  ref
) => {
  // const TIP_POS = {
  //   T: "top", 
  //   B: "bottom", 
  //   TL: "top-left", 
  //   BL: "bottom-left", 
  //   TR: "top-right", 
  //   BR: "bottom-right"
  // };
  const posAuto = pos === "BR" ? "TR" : pos;// "T"
  // const posAuto = TIP_POS[pos] === "";

  const setPos = e => {
    let et = e.target;
    let calc = (e.screenY + et.offsetHeight) > document.documentElement.scrollTop;
    Q.setClass(et, "tip" + (calc ? posAuto : pos));
  }

  const hover = e => {
    if(animate){
      setTimeout(() => setPos(e), 9);
    }else{
      setPos(e);
    }
    
    onMouseEnter(e);
  }

  const leave = e => {
    let et = e.target;
    let cls = "tip" + pos + " tip" + posAuto;
    if(animate){
      setTimeout(() => {
        Q.setClass(et, cls, "remove");
      }, 400);
    }else{
      Q.setClass(et, cls, "remove");
    }
    
    onMouseLeave(e);
  }

  return (
    <As 
      {...etc} 
      ref={ref} 
      className={
        Q.Cx("tip", { 
          tipNoAnimate: !animate, 
          ["tip-" + color]: color, 
        }, className)
      } 
      onMouseEnter={hover} 
      onMouseLeave={leave}
    />
	);
});

export default CssTooltip;