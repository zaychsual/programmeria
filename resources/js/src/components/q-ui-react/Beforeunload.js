import { useRef, useEffect } from 'react';// React, 
import { useHistory } from 'react-router-dom';

// window.confirm("Do you really want to leave?")

// Are you sure you want to quit without saving your changes?
const usePrompt = (when, msg = "") => {
  const history = useHistory();
  const me = useRef(null);

  useEffect(() => {
    const onWindowOrTabClose = e => {
      if (!when) {
        return;
      }
      if (typeof e === "undefined") {
        e = window.event;
      }
      if (e) {
        e.returnValue = msg;
      }
      return msg;
    };
    
    if (when) {
      me.current = history.block(msg);
    } else {
      me.current = null;
    }

    window.addEventListener("beforeunload", onWindowOrTabClose);

    return () => {
      if (me.current) {
        me.current();
        me.current = null;
      }

      window.removeEventListener("beforeunload", onWindowOrTabClose);
    }
  }, [msg, when, history]);
};

function Beforeunload({
	when, 
	msg = "", 
	children = null
}){
	usePrompt(when, msg);

	return children;//  || null
}

export { usePrompt, Beforeunload };
