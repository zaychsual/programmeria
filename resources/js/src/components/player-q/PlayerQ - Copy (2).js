import React, { useState, Component, createRef, Fragment } from 'react';// 
import { useFullscreen } from 'ahooks';
import Dropdown from 'react-bootstrap/Dropdown';
import Tab from 'react-bootstrap/Tab';
// import Nav from 'react-bootstrap/Nav';
import ReactPlayer from 'react-player/lazy';
import IdleTimer from 'react-idle-timer';

import Flex from '../q-ui-react/Flex';
import Btn from '../q-ui-react/Btn';
// import Range from '../q-ui-react/Range';
import ContextMenu from '../q-ui-react/ContextMenu';
import { clipboardCopy } from '../../utils/clipboard';

function pad(string){
  return ('0' + string).slice(-2);
}
function formatDuration(seconds, separator = ":"){
  const date = new Date(seconds * 1000);
  const hh = date.getUTCHours();
  const mm = date.getUTCMinutes();
  const ss = pad(date.getUTCSeconds());
  if(hh){
    return `${hh}${separator}${pad(mm)}${separator}${ss}`;
  }
  return mm + separator + ss;// `${mm}:${ss}`
}

const BEZEL_KEYS = ["ArrowRight","ArrowLeft","ArrowUp","ArrowDown","k","m"];// ,"f"
const btnFullScreenID = Q.Qid();

const Controls = ({
  // inRef, 
  className, 
  targetFull, 
  url, 
  playing, 
  played, 
  duration, 
  loaded, 
  volume, 
  muted, 
  playbackRate, 
  sizes, 
  size, 
  // ccActive, 
  cc, 
  ccShow, 
  tmode, 
  enablePip, 
  onPlayPause, 
  onMouseDownRange, 
  onChangeRange, 
  onMouseUpRange, 
  onVolumeChange, 
  onMute, 
  onSetPlaybackRate, 
  onSetSize, 
  onActiveCC, 
  onSetCc, 
  onTheaterMode, //  = () => {}
  onToggleSet, 
  onPip, 
  ddSet, 
  playbackRates = [0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2]
}) => {
  const [isFullscreen, { toggleFull }] = useFullscreen(targetFull);// setFull, exitFull, 
  const [tab, setTab] = useState("menus");

  const ToggleSet = (open) => {
    onToggleSet(open);

    if(!open && tab !== "menus") setTab("menus");
  }

  const setPlaybackRate = v => {
    if(playbackRate !== v) onSetPlaybackRate(v);
    setTab("menus");
  }

  const setSize = v => {
    if(size !== v) onSetSize(v);
    setTab("menus");
  }

  const setCc = v => {
    if(ccShow !== v) onSetCc(v);
    setTab("menus");
  }

  return (
    <div className={Q.Cx("text-white zi-2 v-ctrl", className)}>
      <div className="v-range">
        {/* <Range 
          grab={false} 
          className="rounded-pill" 
          min={0} 
          max={0.999999} // {100} 
          step="any" 
          // defaultValue={0} 
          value={played} 
          onMouseDown={onMouseDownRange} 
          onMouseUp={onMouseUpRange} 
          onChange={onChangeRange} 
        /> */}

        <input type="range" 
          min={0} 
          max={0.999999} // {100} 
          step="any" 
          className="q-range rounded-pill"
          value={played} 
          style={{ '--slider-val': (played * 100) + '%' }} 
          onMouseDown={onMouseDownRange} 
          onMouseUp={onMouseUpRange} 
          onChange={onChangeRange} 
        />

        <div role="progressbar" 
          className="q-progress rounded-pill" 
          aria-valuemin="0" 
          aria-valuemax="100" // 100 | 1
          aria-valuenow={loaded * 100} 
          style={{ height:5, "--val": (loaded * 100) + "%" }}
        />
      </div>

      <Flex align="center" className="mt-2 v-btn-ctrl">
        <Btn size="sm" // kind="flat" 
          aria-label={playing ? "Pause" : "Play"} 
          className={"mr-1 tip tipTL far fa-" + (playing ? "pause" : "play")} 
          onClick={onPlayPause} 
        />

        <Flex align="center" className="mr-2 v-volume">
          <Btn size="sm" // kind="flat" 
            className={
              Q.Cx("px-1 tip tipT flexno q-s13 fa-fw far", {
                "fa-volume": (volume * 10) >= 5, // volume > 0, // 
                "fa-volume-down": (volume * 10) < 5, // volume.toFixed(1) < 0.5
                "fa-volume-mute": muted || volume === 0, // slash
              })
            } 
            aria-label={muted || volume === 0 ? "Unmute" : "Mute"} 
            onClick={onMute}
          />

          <input type="range" 
            min={0} 
            max={1} // {100} 
            step="any" 
            className="q-range rounded-pill ml-1"
            value={volume} 
            style={{ '--slider-val': (volume * 100) + '%' }} 
            onChange={onVolumeChange} 
          />
        </Flex>

        <small className="mr-auto v-time">
          {formatDuration(duration * played)} / {formatDuration(duration)}
        </small>

        {cc?.length > 0 && 
          <Btn size="sm" 
            active={ccShow !== "Off"} 
            className="tip tipT q-s12 far fa-closed-captioning" 
            aria-label="Subtitles/closed captions" 
            onClick={onActiveCC}
          />
        }

        <Dropdown alignRight drop="up" 
          show={ddSet} 
          onToggle={ToggleSet} 
        >
          <Dropdown.Toggle size="sm" bsPrefix="tip tipT q-s11 far fa-cog" aria-label="Settings" />
          <Dropdown.Menu flip={false} className="v-dd-sets">
            <Tab.Container id="playerQ-tabSets" 
              activeKey={tab} 
              onSelect={k => setTab(k)} 
            >
              <Tab.Content>
                <Tab.Pane eventKey="menus">
                  <Flex As="label" justify="between" align="center" className="dropdown-item">
                    Autoplay 
                    <div className="custom-control custom-switch d-block ml-5">
                      <input type="checkbox" className="custom-control-input" />
                      <div className="custom-control-label" />
                    </div>
                  </Flex>

                  <Flex As="label" justify="between" align="center" className="dropdown-item">
                    Annotations 
                    <div className="custom-control custom-switch d-block ml-5">
                      <input type="checkbox" className="custom-control-input" />
                      <div className="custom-control-label" />
                    </div>
                  </Flex>
                  
                  <Flex justify="between" align="center" className="dropdown-item"
                    onClick={() => setTab("playbackSpeed")} 
                  >
                    Playback speed 
                    <div className="ml-5">
                      {playbackRate === 1.0 ? "Normal" : playbackRate} <i className="far fa-chevron-right ml-1" />
                    </div>
                  </Flex>
                  
                  {cc?.length > 0 && 
                    <Flex justify="between" align="center" className="dropdown-item"
                      onClick={() => setTab("cc")} 
                    >
                      Subtitles/CC 
                      <div className="ml-5">
                        {ccShow} <i className="far fa-chevron-right ml-1" />
                      </div>
                    </Flex>
                  }

                  <Flex justify="between" align="center" className="dropdown-item"
                    onClick={() => setTab("quality")} 
                  >
                    Quality 
                    <div className="ml-5">
                      {size}p <i className="far fa-chevron-right ml-1" />
                    </div>
                  </Flex>
                </Tab.Pane>

                <Tab.Pane eventKey="playbackSpeed">
                  <div onClick={() => setTab("menus")} className="dropdown-item far fa-chevron-left q-mr">
                    Playback speed
                  </div>
                  <hr />

                  {playbackRates.map((v, i) => 
                    <button key={i} type="button" 
                      className={"dropdown-item " + (playbackRate === v ? "fal fa-check q-mr" : "pl-36px")} 
                      onClick={() => setPlaybackRate(v)} 
                    >
                      {v === 1.0 ? "Normal" : v}
                    </button>
                  )}
                </Tab.Pane>

                {cc?.length > 0 && 
                  <Tab.Pane eventKey="cc">
                    <div onClick={() => setTab("menus")} className="dropdown-item far fa-chevron-left q-mr">
                      Subtitles/CC 
                    </div>
                    <hr />
                    
                    {cc.map((v, i) => 
                      <button key={i} type="button" 
                        className={"dropdown-item " + (ccShow === v ? "fal fa-check q-mr" : "pl-36px")} 
                        onClick={() => setCc(v)} 
                      >
                        {v}
                      </button>
                    )}
                  </Tab.Pane>
                }

                <Tab.Pane eventKey="quality">
                  <div onClick={() => setTab("menus")} className="dropdown-item far fa-chevron-left q-mr">
                    Quality
                  </div>
                  <hr />
                  
                  {sizes?.length > 0 && sizes.map((v, i) => 
                    <button key={i} type="button" 
                      className={"dropdown-item " + (size === v ? "fal fa-check q-mr" : "pl-36px")} 
                      onClick={() => setSize(v)} 
                    >
                      {v + "p"}
                    </button>
                  )}
                </Tab.Pane>
              </Tab.Content>
            </Tab.Container>
          </Dropdown.Menu>
        </Dropdown>

        {(enablePip && ReactPlayer.canEnablePIP(url) && onPip) && 
          <Btn size="sm" className="tip tipT far fa-tv-alt" 
            aria-label="Mini player" 
            onClick={onPip} 
          />
        }

        <Btn size="sm" className="tip tipT q-s11 far fa-film" 
          aria-label={tmode ? "Default view" : "Theater mode"} 
          onClick={onTheaterMode} 
        />

        <Btn size="sm" // className="tip tipTR far fa-expand" 
          id={btnFullScreenID} 
          className={"tip tipTR q-s11 far fa-" + (isFullscreen ? "compress" : "expand")} 
          aria-label={(isFullscreen ? "Exit " : "") + "Full screen"}
          onClick={toggleFull} 
        />
      </Flex>
    </div>
  );
}

export default class PlayerQ extends Component{
  constructor(props){
    super(props);
    this.state = {
      ready: false, 
      playing: false, 
      played: 0, 
      // seeking: false, 
      duration: 0,
      loaded: 0, 
      loop: false, 
      volume: 0.5, // 0.8
      muted: false, 
      pip: false, 
      playbackRate: 1.0, 

      lastVol: 0, 
      size: 360, 
      // ccActive: false, 
      ccShow: "Off", // Selected track cc: null | "Off"
      active: false, 
      ddSet: false, // Dropdown setting
      keyVol: false, 
      bezelPlay: null, 
      bezelCopy: false, 
      // vPoster: props.poster ? true : false
    };

    this.wrap = createRef();
    this.idleTimer = null;
    // this.defaultCC = null;
  }

  // componentDidMount(){
	// 	console.log('%ccomponentDidMount in PlayerQ','color:yellow;', this.wrap);
  // }

  componentWillUnmount(){
    if(this.track){
      this.track.removeEventListener("cuechange", this.onCueChange);
    }
    // this.idleTimer = null;// OPTION
  }

  onPlay = () => {
    // console.log('onPlay: ');
    // if(this.state.vPoster){ //  && this.props.poster
    //   // Q.setClass(this.getPlayerDOM(), "of-cov", "remove");
    //   this.setState({ vPoster: false });
    // }
    this.setState({ playing: true });
  }

  onPause = () => {
    // console.log('onPause');
    this.setState({ playing: false });
  }

  onPlayPause = () => {
    this.setState(s => ({ playing: !s.playing }));
  }

  onSeekMouseDown = () => {
    console.log('onSeekMouseDown');
    this.setState({ seeking: true })
  }

  onSeekMouseUp = e => {
    let val = e.target.value;
    this.setState({ seeking: false });
    this.player.seekTo(parseFloat(val));

    console.log('onSeekMouseUp');
  }

  onSeekChange = e => {
    this.setState({ played: parseFloat(e.target.value) });
  }

  onProgress = state => {
    // console.log('onProgress state: ', state);
    // console.log('onProgress seeking: ', this.state.seeking);
    // We only want to update time slider if we are not currently seeking
    if(!this.state.seeking){
      this.setState({ ...this.state, ...state });
    }
  }

  onDuration = duration => {
    this.setState({ duration });
  }

  onEnded = () => {
    // console.log('onEnded');
    this.setState(s => ({ playing: s.loop, ccTxt: null }));
  }

// NOT FIX:
  onVolumeChange = e => {
    let v = e.target.value;
    this.setState(s => ({ 
      // lastVol: s.volume, 
      volume: parseFloat(v) 
    }), () => {
      this.setState(s => ({ muted: s.volume === 0 }))
    });
  }

// NOT FIX:
  onMute = () => {
    // const { muted, volume, lastVol } = this.state;
    this.setState(s => ({ 
      // muted: !s.muted,
      lastVol: s.volume !== 0 ? s.volume : 0.5, 
      // volume: s.muted && s.volume === 0 ? s.lastVol : 0
    }), () => {
      this.setState(s => ({ 
        muted: !s.muted, 
        volume: s.muted && s.volume === 0 ? s.lastVol : 0 
      }))
    });
  }

  onBezelPlayPause = () => {
    this.onPlayPause();
    this.setState(s => ({ bezelPlay: "fa fa-" + (s.playing ? "pause" : "play") + "-circle " }));

    setTimeout(() => this.setState({ bezelPlay: null }), 500);
  }

  onBezelKeyDown = e => {
    // console.log(e.key);
    if(BEZEL_KEYS.includes(e.key)){
      Q.preventQ(e);

      const { volume, keyVol } = this.state;
      let v;
      // ["ArrowRight","ArrowLeft","ArrowUp","ArrowDown"]
      switch(e.key){
        case "k":
          this.onBezelPlayPause();
          break;
        case "m":
          this.onMute();
          break;
        // case "f": // Fullscreen
        //   this.onMute();
        //   break;  
        case "ArrowRight":
          console.log("ArrowRight NEXT");
          break;
        case "ArrowLeft":
          console.log("ArrowLeft PREV");
          break;
        case "ArrowUp":
          v = volume + 0.05;
          if(v > 1) v = 1;
          this.setState({ volume: v });

          // Show range volume
          if(!keyVol) this.setState({ keyVol: true });
          break;
        case "ArrowDown":
          v = volume - 0.05;
          if(v < 0) v = 0;
          this.setState({ volume: v });

          // Show range volume
          if(!keyVol) this.setState({ keyVol: true });
          break;
        default: 
          return;
      }
    }
  }

  onSetPlaybackRate = v => {
    this.setState({ playbackRate: v });
  }

  onSetSize = size => {
    this.setState({ size });
  }

  onActiveCC = () => {
    // const { ccShow } = this.state;// ccActive, 
    // this.setState({ ccActive: !ccActive });
    // this.onSetCc(!ccActive ? ccShow : "Off");
    this.onSetCc(this.state.ccShow === "Off" ? this.defaultCC : "Off");
  }

  onSetCc = ccShow => {
    if(this.track){
      // let isOff = ccShow === "Off";
      this.setState({ ccShow });// , ccActive: isOff

      if(ccShow === "Off"){
        this.setState({ ccTxt: null });
        this.track.removeEventListener("cuechange", this.onCueChange);
      }else{
        let ply = this.getPlayerDOM();
        let trackActive = Q.domQ("track[default]", ply);
        Q.setAttr(trackActive, "default");

        let trackActiveNow = Q.domQ(`track[label="${ccShow}"]`, ply);
        // Q.setAttr(trackActiveNow, {default:""});
        trackActiveNow.default = true;

        console.log('trackActive: ', trackActive);

        this.track = trackActiveNow;

        setTimeout(() => {
          this.track.addEventListener("cuechange", this.onCueChange);
        }, 1);
      }
    }
  }

  setUrl = (url) => {
    return Array.isArray(url) ? 
      url.map(v => ({ 
          ...v, 
          src: Q.newURL(v.src).href, 
          sizes: v.sizes ?  v.sizes + "px" : undefined
        })) 
      : 
      Q.newURL(url).href;
  }

  onCueChange = (e) => {
    let cues = e.target.track.activeCues;
    let ccTxt = cues[0]?.text;
    // console.log('e: ', e);
    console.log('cues: ', cues);
    // console.log('ccTxt: ', ccTxt);

    this.setState({ ccTxt });
  }

  onReady = (p) => {
    this.setState({ ready: true });

    if(this.props.fileConfig?.tracks){
      let ply = p.getInternalPlayer();
      let trackActive = Q.domQ("track[default]", ply);
      // console.log('onReady p: ', p);
      // console.log('onReady trackActive: ', trackActive.label);
  
      if(trackActive){
        // const ccShow = trackActive.label;
        this.track = trackActive;
        this.defaultCC = trackActive.label;// ccShow;
        // this.track.addEventListener("cuechange", this.onCueChange);
        // this.setState({ ccShow: "Off" });// , ccActive: true
      }
    }
  }

  // onAction = e => {
  //   let et = e?.target;
  //   console.log('onAction - user did something e: ', e);
  //   console.log('onAction target: ', et);
  // }
  onActive = () => {
    // let et = e?.target;
    // console.log('onActive - user is active e: ', e);
    // console.log('onActive - time remaining: ', this.idleTimer.getRemainingTime());
    // console.log('onActive target: ', et?.tagName);

    this.setState({ active: true  });
  }

  onIdle = () => {
    const { ddSet, keyVol } = this.state;
    // console.log('onIdle: ');
    if(!ddSet) this.setState({ active: false });
    if(keyVol) this.setState({ keyVol: false });
  }

  onToggleSet = ddSet => {
    this.setState({ ddSet });
  }

  onPip = () => {
    this.setState(s => ({ pip: !s.pip }));
  }

  ref = p => {
    this.player = p
  }

  getPlayerDOM = () => {
    return this.player?.getInternalPlayer();
  }

  onCtxPlayPause = (hide) => {
    hide();
    this.onBezelPlayPause();// onPlayPause
  }

  onCtxCopy = (hide, txt) => {
    hide();
    // let id = window.location.href.split("/");
    // id[id.length - 1]
    clipboardCopy(txt).then(() => {
      this.setState({ bezelCopy: true });
      setTimeout(() => this.setState({ bezelCopy: false }), 500);
    }).catch(e => console.log(e));
  }

  parseFileConfig(fileConfig){
    return fileConfig?.tracks ? { ...fileConfig, tracks: fileConfig.tracks.map(v => ({ ...v, src: Q.newURL(v.src).href })) } : fileConfig;
  }

  parseCC(fileConfig){
    return fileConfig?.tracks ? ["Off", ...fileConfig.tracks.map(v => v.label)] : null;
  }

  render(){
    const { 
      reactPlayerClass, url, config, fileConfig, poster, 
      className, wrapStyle, tmode, enablePip, videoID, 
      onTheaterMode, 
    } = this.props;

    const { 
      ready, playing, played, duration, loaded, loop, volume, muted, playbackRate, pip, 
      size, active, ddSet, keyVol, ccShow, ccTxt, bezelPlay, bezelCopy, 
    } = this.state;// ccActive, vPoster, 

    // console.log(': ', );
    return (
      <ContextMenu 
        className="zi-5" // upModal
        appendTo={this.wrap.current || document.body} 
        popperConfig={{
          strategy: "fixed",
          // modifiers: [
          //   {
          //     name: 'preventOverflow',
          //     options: {
          //       // padding: 100, 
          //       // boundary: document.body, // this.wrap.current
          //       // altBoundary: true, // false by default
          //       // rootBoundary: 'document', // 'viewport' by default
          //       // mainAxis: false, // true by default
          //     }
          //   },
          // ]
        }} 
        // , ctxMenu
        component={(hide) => (
          <div className="dropdown-menu show v-dd-sets"
            onContextMenu={(e) => {Q.preventQ(e);hide()}} // ctxMenu | Q.preventQ | (e) => {Q.preventQ(e);hide()}
          >
            <button type="button" 
              className={"dropdown-item q-mr far fa-" + (playing ? "pause":"play")} 
              onClick={() => this.onCtxPlayPause(hide)} 
            >
              {playing ? "Pause":"Play"}
            </button>

            <label role="button" htmlFor={btnFullScreenID} 
              className="dropdown-item q-mr far fa-expand" 
              onClick={() => {
                // let et = e.target;
                // let btn = Q.domQ("#" + btnFullScreenID);
                // if(btn){
                //   btn.click();
                // }
                setTimeout(() => hide(), 9);
              }}
            >
              Full screen
            </label>

            {videoID && 
              <Fragment>
                <button type="button" className="dropdown-item q-mr far fa-copy" 
                  onClick={() => this.onCtxCopy(hide, videoID)} // "Copy video URL: dummy-video-id"
                >
                  Copy video URL
                </button>

                <button type="button" className="dropdown-item q-mr far fa-copy" 
                  onClick={() => this.onCtxCopy(hide, "https://programmeria.com/" + videoID + "?t=" + formatDuration(duration * played, "."))} 
                >
                  Copy video URL at current time
                </button>

                <button type="button" className="dropdown-item q-mr far fa-code" 
                  onClick={() => this.onCtxCopy(hide, `<iframe width="716" height="403" src="https://programmeria.com/embed/${videoID}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`)} 
                >
                  Copy embed code
                </button>
              </Fragment>
            }

            <hr/>

            <button type="button" className="dropdown-item q-mr far fa-info-circle" 
              onClick={hide} 
            >
              Info
            </button>
          </div>
        )}
      >
        <div 
          ref={this.wrap} 
          className={
            Q.Cx("embed-responsive embed-responsive-16by9 playerQ", {
              "ready": ready, 
              "playing": playing,
              "active": active,
              "ddSetOpen": ddSet,
            }, className)
          } 
          style={wrapStyle} 
          // role="application" // OPTION
        >
          {ready && 
            <IdleTimer
              ref={r => { this.idleTimer = r }} 
              element={this.wrap.current} 
              timeout={3000} // 1000 * 60 * 15
              debounce={250} 
              startOnMount={false} 
              onActive={this.onActive} 
              onIdle={this.onIdle} 
              // onAction={this.onAction} 
            />
          }

          <ReactPlayer 
            ref={this.ref} 
            // controls 
            className={reactPlayerClass} 
            width="100%" // auto
            height="100%" 
            config={{
              ...config, 
              file:{
                // ...fileConfig, 
                ...this.parseFileConfig(fileConfig), 
                attributes:{
                  preload: poster ? "metadata" : "auto", 
                  crossOrigin: "anonymous", 
                  tabIndex: "-1",
                  controlsList: "nodownload nofullscreen noremoteplayback",
                  // className: poster ? "of-cov" : undefined,
                  poster
                }
              }
            }}
            // 
            url={this.setUrl(url)} 
            playing={playing} 
            loop={loop} 
            playbackRate={playbackRate} 
            volume={volume} 
            muted={muted} 
            pip={pip} 
            onReady={this.onReady} 
            // onStart={() => console.log('onStart')} 
            onError={e => console.log('onError: ', e)} 
            onPlay={this.onPlay} 
            onPause={this.onPause} 
            onProgress={this.onProgress} 
            onDuration={this.onDuration} 
            onEnded={this.onEnded} 
            // onBuffer={() => console.log('onBuffer')} 
            // onBufferEnd={() => console.log('onBufferEnd')} 
            onEnablePIP={(e) => console.log('onEnablePIP: ', e)} // this.handleEnablePIP
            onDisablePIP={(e) => console.log('onDisablePIP: ', e)} // this.handleDisablePIP

            onSeek={(e) => {
              console.log('onSeek e: ', e);
            }}
          />

          <button type="button" 
            className={
              Q.Cx("btn rounded-0 shadow-none cauto w-100 text-primary fa-2x position-absolute position-full zi-1 v-bezel", {
                "i-load": !ready,
                "playerQ-bezel-animate": bezelPlay || bezelCopy, 
                "far fa-copy": bezelCopy,
                ["" + bezelPlay]: bezelPlay
              })
            } 
            style={ready ? undefined : { '--bg-i-load': '90px' }} 
            onClick={this.onBezelPlayPause} 
            onKeyDown={this.onBezelKeyDown}
          />

          <Controls 
            targetFull={() => this.wrap.current} 
            className={Q.Cx({ "keyVol": keyVol })} 
            url={this.setUrl(url)} 
            playing={playing} 
            played={played} 
            duration={duration} 
            loaded={loaded} 
            volume={volume} 
            muted={muted} 
            playbackRate={playbackRate} 
            sizes={Array.isArray(url) ? url.map(v => v.sizes && Number(v.sizes)) : undefined} 
            size={size} // "360" 
            cc={this.parseCC(fileConfig)} 
            // ccActive={ccActive} 
            ccShow={ccShow} 
            tmode={tmode} 
            ddSet={ddSet} 
            enablePip={enablePip} 
            onPlayPause={this.onPlayPause} 
            onMouseDownRange={this.onSeekMouseDown} 
            onMouseUpRange={this.onSeekMouseUp} 
            onChangeRange={this.onSeekChange} 
            onVolumeChange={this.onVolumeChange} 
            onMute={this.onMute} 
            onToggleSet={this.onToggleSet} 
            onSetPlaybackRate={this.onSetPlaybackRate} 
            onSetSize={this.onSetSize} 
            onActiveCC={this.onActiveCC} 
            onSetCc={this.onSetCc} 
            onPip={this.onPip} 
            onTheaterMode={onTheaterMode} 
          />

          {/* DEV OPTION: Custom poster for youtube API etc */}
          {/* {(poster && Q.isAbsoluteUrl(url)) && 

          } */}

          {ccTxt && 
            <div className="position-absolute l0 r0 mx-auto text-center v-cc">
              <span>{ccTxt}</span>
            </div>
          }
        </div>
      </ContextMenu>
    );
  }
}

PlayerQ.defaultProps = {
  onTheaterMode: () => {},

}; 

/*
<video 
  // controls 
  className="embed-responsive-item" 
  src="/media/video_360.mp4" 
/>

<React.Fragment> </React.Fragment>
*/
