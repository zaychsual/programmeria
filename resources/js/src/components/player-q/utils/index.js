// 

function pad(string){
  return ('0' + string).slice(-2);
}
function formatTime(seconds, separator = ":"){
  const date = new Date(seconds * 1000);
  const hh = date.getUTCHours();
  const mm = date.getUTCMinutes();
  const ss = pad(date.getUTCSeconds());

  if(hh) return hh + separator + pad(mm) + separator + ss; // `${hh}${separator}${pad(mm)}${separator}${ss}`;
  return mm + separator + ss;// `${mm}:${ss}`
}

// FROM: video-react
function findElPosition(el){
  let box;

  if(el.getBoundingClientRect && el.parentNode){
    box = el.getBoundingClientRect();
  }

  if(!box){
    return {
      left: 0,
      top: 0
    };
  }

  const docEl = document.documentElement;
  const body = document.body;

  const clientLeft = docEl.clientLeft || body.clientLeft || 0;
  const scrollLeft = window.pageXOffset || body.scrollLeft;
  const left = (box.left + scrollLeft) - clientLeft;

  const clientTop = docEl.clientTop || body.clientTop || 0;
  const scrollTop = window.pageYOffset || body.scrollTop;
  const top = (box.top + scrollTop) - clientTop;

  // Android sometimes returns slightly off decimal values, so need to round
  return {
    left: Math.round(left),
    top: Math.round(top),
		width: box.width // Q-CUSTOM
  };
}

function getPointerPosition(el, e){
  let pos = {};
  const box = findElPosition(el);
  const boxW = el.offsetWidth;
  const boxH = el.offsetHeight;

  const boxY = box.top;
  const boxX = box.left;
  let pY = e.pageY;
  let pX = e.pageX;

  if(e.changedTouches){
    pX = e.changedTouches[0].pageX;
    pY = e.changedTouches[0].pageY;
  }

  pos.y = Math.max(0, Math.min(1, ((boxY - pY) + boxH) / boxH));
  pos.x = Math.max(0, Math.min(1, (pX - boxX) / boxW));

  return pos;
}

// Keyboard Shortcut:
const BEZEL_KEYS = ["ArrowRight","ArrowLeft","ArrowUp","ArrowDown","f","i","k","m"];

const CC_FONT_COLORS = [
  {name:"White", hex:"#fff"},
  {name:"Yellow", hex:"#ffff00"},
  {name:"Green", hex:"#00ff00"},
  {name:"Cyan", hex:"#00ffff"},
  {name:"Blue", hex:"#0000ff"},
  {name:"Magenta", hex:"#ff00ff"},
  {name:"Red", hex:"#ff0000"} 
];

const CC_FONT_SIZES = [50, 75, 100, 125, 150, 175, 200];// 300, 400

// URL for copy:
// const 

export { formatTime, findElPosition, getPointerPosition, BEZEL_KEYS, CC_FONT_COLORS, CC_FONT_SIZES };



