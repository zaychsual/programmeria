import React, { useState, Component, createRef } from 'react';
import { useFullscreen } from 'ahooks';
import Dropdown from 'react-bootstrap/Dropdown';
import Tab from 'react-bootstrap/Tab';
// import Nav from 'react-bootstrap/Nav';
import ReactPlayer from 'react-player/lazy';
import IdleTimer from 'react-idle-timer';

import Flex from '../q-ui-react/Flex';
import Btn from '../q-ui-react/Btn';
// import Range from '../q-ui-react/Range';

function format(seconds){
  const date = new Date(seconds * 1000);
  const hh = date.getUTCHours();
  const mm = date.getUTCMinutes();
  const ss = pad(date.getUTCSeconds());
  if(hh){
    return `${hh}:${pad(mm)}:${ss}`;
  }
  return `${mm}:${ss}`;
}

function pad(string){
  return ('0' + string).slice(-2);
}

const BEZEL_KEYS = ["ArrowRight","ArrowLeft","ArrowUp","ArrowDown"];

const Controls = ({
  targetFull, 
  playing, 
  played, 
  duration, 
  loaded, 
  volume, 
  muted, 
  playbackRate, 
  sizes, 
  size, 
  cc, 
  ccShow, 
  onPlayPause, //  = () => {}
  onMouseDownRange, 
  onChangeRange, 
  onMouseUpRange, 
  onVolumeChange, 
  onMute, 
  onSetPlaybackRate, 
  onSetSize, 
  onSetCc, 
  onTheaterMode, //  = () => {}
  playbackRates = [0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2]
}) => {
  const [isFullscreen, { toggleFull }] = useFullscreen(targetFull);// setFull, exitFull, 
  const [tab, setTab] = useState("menus");

  const onToggleSet = (open) => {
    if(!open && tab !== "menus") setTab("menus");
  }

  const setPlaybackRate = v => {
    if(playbackRate !== v) onSetPlaybackRate(v);
    setTab("menus");
  }

  const setSize = v => {
    if(size !== v) onSetSize(v);
    setTab("menus");
  }

  const setCc = v => {
    if(ccShow !== v) onSetCc(v);
    setTab("menus");
  }

  return (
    <div className="text-white zi-2 v-ctrl">
      <div className="v-range">
        {/* <Range 
          grab={false} 
          className="rounded-pill" 
          min={0} 
          max={0.999999} // {100} 
          step="any" 
          // defaultValue={0} 
          value={played} 
          onMouseDown={onMouseDownRange} 
          onMouseUp={onMouseUpRange} 
          onChange={onChangeRange} 
        /> */}

        <input type="range" 
          min={0} 
          max={0.999999} // {100} 
          step="any" 
          className="q-range rounded-pill"
          value={played} 
          style={{ '--slider-val': (played * 100) + '%' }} 
          onMouseDown={onMouseDownRange} 
          onMouseUp={onMouseUpRange} 
          onChange={onChangeRange} 
        />

        <div role="progressbar" 
          className="q-progress rounded-pill" 
          aria-valuemin="0" 
          aria-valuemax="100" // 100 | 1
          aria-valuenow={loaded * 100} 
          style={{ height:5, "--val": (loaded * 100) + "%" }}
        />
      </div>

      <Flex align="center" className="mt-1">
        <Btn size="sm" kind="flat" 
          aria-label={playing ? "Pause" : "Play"} 
          className={"mr-1 tip tipTL far fa-" + (playing ? "pause" : "play")} 
          onClick={onPlayPause} 
        />

        <Flex align="center" className="mr-2 v-volume">
          <Btn size="sm" kind="flat" 
            className={
              Q.Cx("px-1 tip tipT flexno fa-fw far", {
                "fa-volume": (volume * 10) >= 5, // volume > 0, // 
                "fa-volume-down": (volume * 10) < 5, // volume.toFixed(1) < 0.5
                "fa-volume-mute": muted || volume === 0, // slash
              })
            } 
            aria-label={muted || volume === 0 ? "Unmute" : "Mute"} 
            onClick={onMute}
          />
          {/* <Range 
            grab={false} 
            className="rounded-pill" 
            min={0} 
            max={1} // 100
            value={volume} 
            onChange={onVolumeChange}
          /> */}
          <input type="range" 
            min={0} 
            max={1} // {100} 
            step="any" 
            className="q-range rounded-pill"
            value={volume} 
            style={{ '--slider-val': (volume * 100) + '%' }} 
            onChange={onVolumeChange} 
          />
        </Flex>

        <small className="v-time">
          {format(duration * played)} / {format(duration)}
        </small>

        <Btn size="sm" kind="flat" 
          className="ml-auto tip tipT far fa-closed-captioning" 
          aria-label="Subtitles/closed captions" 
        />

        <Dropdown alignRight drop="up" onToggle={onToggleSet}>
          <Dropdown.Toggle size="sm" variant="flat" bsPrefix="tip tipT far fa-cog" aria-label="Settings" />

          <Dropdown.Menu 
            flip={false} 
            // align="right" 
            className="v-dd-sets" 
          >
            <Tab.Container id="playerQ-tabSets" 
              activeKey={tab} // defaultActiveKey
              onSelect={k => setTab(k)} 
            >
              <Tab.Content>
                <Tab.Pane eventKey="menus">
                  <Flex As="label" justify="between" align="center" className="dropdown-item">
                    Autoplay 
                    <div className="custom-control custom-switch d-block ml-5">
                      <input type="checkbox" className="custom-control-input" />
                      <div className="custom-control-label" />
                    </div>
                  </Flex>

                  <Flex As="label" justify="between" align="center" className="dropdown-item">
                    Annotations 
                    <div className="custom-control custom-switch d-block ml-5">
                      <input type="checkbox" className="custom-control-input" />
                      <div className="custom-control-label" />
                    </div>
                  </Flex>
                  
                  <Flex justify="between" align="center" className="dropdown-item"
                    onClick={() => setTab("playbackSpeed")} 
                  >
                    Playback speed 
                    <div className="ml-5">
                      {playbackRate === 1.0 ? "Normal" : playbackRate} <i className="far fa-chevron-right ml-1" />
                    </div>
                  </Flex>
                  
                  <Flex justify="between" align="center" className="dropdown-item"
                    onClick={() => setTab("cc")} 
                  >
                    Subtitles/CC 
                    <div className="ml-5">
                      {ccShow} <i className="far fa-chevron-right ml-1" />
                    </div>
                  </Flex>

                  <Flex justify="between" align="center" className="dropdown-item"
                    onClick={() => setTab("quality")} 
                  >
                    Quality 
                    <div className="ml-5">
                      {size} <i className="far fa-chevron-right ml-1" />
                    </div>
                  </Flex>
                </Tab.Pane>

                <Tab.Pane eventKey="playbackSpeed">
                  <div onClick={() => setTab("menus")} className="dropdown-item far fa-chevron-left q-mr">
                    Playback speed
                  </div>
                  <hr />

                  {playbackRates.map((v, i) => 
                    <button key={i} type="button" 
                      className={"dropdown-item " + (playbackRate === v ? "fal fa-check q-mr" : "pl-36px")} 
                      onClick={() => setPlaybackRate(v)} 
                    >
                      {v === 1.0 ? "Normal" : v}
                    </button>
                  )}
                </Tab.Pane>

                <Tab.Pane eventKey="cc">
                  <div onClick={() => setTab("menus")} className="dropdown-item far fa-chevron-left q-mr">
                    Subtitles/CC 
                  </div>
                  <hr />
                  
                  {cc?.length > 0 && cc.map((v, i) => 
                    <button key={i} type="button" 
                      className={"dropdown-item " + (ccShow === v ? "fal fa-check q-mr" : "pl-36px")} 
                      onClick={() => setCc(v)} 
                    >
                      {v}
                    </button>
                  )}
                </Tab.Pane>

                <Tab.Pane eventKey="quality">
                  <div onClick={() => setTab("menus")} className="dropdown-item far fa-chevron-left q-mr">
                    Quality
                  </div>
                  <hr />
                  
                  {sizes?.length > 0 && sizes.map((v, i) => 
                    <button key={i} type="button" 
                      className={"dropdown-item " + (size === v ? "fal fa-check q-mr" : "pl-36px")} 
                      onClick={() => setSize(v)} 
                    >
                      {v + "p"}
                    </button>
                  )}
                </Tab.Pane>
              </Tab.Content>
            </Tab.Container>
          </Dropdown.Menu>
        </Dropdown>

        <Btn size="sm" kind="flat" 
          className="tip tipT far fa-tv-alt" 
          aria-label="Mini player" 
        />

        <Btn size="sm" kind="flat" 
          className="tip tipT far fa-film" 
          aria-label="Theater mode" 
          onClick={onTheaterMode} 
        />

        <Btn size="sm" kind="flat" // className="tip tipTR far fa-expand" 
          className={"tip tipTR far fa-" + (isFullscreen ? "compress" : "expand")} 
          aria-label={(isFullscreen ? "Exit " : "") + "Full screen"}
          onClick={toggleFull} 
        />
      </Flex>
    </div>
  );
}

export default class PlayerQ extends Component{
  constructor(props){
    super(props);
    this.state = {
      playing: false, 
      played: 0, 
      // seeking: false, 
      duration: 0,
      loaded: 0, 
      loop: false, 
      volume: 0.5, // 0.8
      muted: false, 
      pip: false, 
      playbackRate: 1.0, 

      lastVol: 0, 
      size: 360, 
      ccShow: null, // Selected track cc
      active: false, 
      // vPoster: props.poster ? true : false
    };

    this.wrap = createRef();
    this.idleTimer = null;
  }

  // componentDidMount(){
	// 	console.log('%ccomponentDidMount in PlayerQ','color:yellow;', this.wrap);
  // }

  componentWillUnmount(){
    if(this.track){
      this.track.removeEventListener("cuechange", this.onCueChange);
    }
  }

  onPlay = () => {
    // console.log('onPlay: ');
    // if(this.state.vPoster){ //  && this.props.poster
    //   // Q.setClass(this.getPlayerDOM(), "of-cov", "remove");
    //   this.setState({ vPoster: false });
    // }

    this.setState({ playing: true });
  }

  onPause = () => {
    // console.log('onPause');
    this.setState({ playing: false });
  }

  onPlayPause = () => {
    this.setState(s => ({ playing: !s.playing }));
  }

  onSeekMouseDown = () => {
    console.log('onSeekMouseDown');
    this.setState({ seeking: true })
  }

  onSeekMouseUp = e => {
    let val = e.target.value;
    this.setState({ seeking: false });
    this.player.seekTo(parseFloat(val));

    console.log('onSeekMouseUp');
  }

  onSeekChange = e => {
    this.setState({ played: parseFloat(e.target.value) });
  }

  onProgress = state => {
    // console.log('onProgress state: ', state);
    // console.log('onProgress seeking: ', this.state.seeking);
    // We only want to update time slider if we are not currently seeking
    if (!this.state.seeking) {
      this.setState({ ...this.state, ...state });
    }
  }

  onDuration = (duration) => {
    this.setState({ duration });
  }

  onEnded = () => {
    // console.log('onEnded');
    this.setState(s => ({ playing: s.loop, ccTxt: null }));
  }

// NOT FIX:
  onVolumeChange = e => {
    let v = e.target.value;
    this.setState(s => ({ 
      // lastVol: s.volume, 
      volume: parseFloat(v) 
    }), () => {
      this.setState(s => ({ muted: s.volume === 0 }))
    });
  }

// NOT FIX:
  onMute = () => {
    // const { muted, volume, lastVol } = this.state;
    this.setState(s => ({ 
      // muted: !s.muted,
      lastVol: s.volume !== 0 ? s.volume : 0.5, 
      // volume: s.muted && s.volume === 0 ? s.lastVol : 0
    }), () => {
      this.setState(s => ({ 
        muted: !s.muted, 
        volume: s.muted && s.volume === 0 ? s.lastVol : 0 
      }))
    });
  }

  onBezelPlayPause = e => {
    let et = e.target;
    let c = "fa-" + (this.state.playing ? "pause" : "play") + "-circle ";

    this.onPlayPause();

    Q.setClass(et, c + "playerQ-bezel-animate");

    setTimeout(() => {
      Q.setClass(et, c + "playerQ-bezel-animate", "remove");
    }, 500);
  }

  onBezelKeyDown = e => {
    if(BEZEL_KEYS.includes(e.key)){
      e.preventDefault();
      e.stopPropagation();

      const { volume } = this.state;
      // ["ArrowRight","ArrowLeft","ArrowUp","ArrowDown"]
      switch(e.key){
        case "ArrowRight":
          console.log("ArrowRight NEXT");
          break;
        case "ArrowLeft":
          console.log("ArrowLeft PREV");
          break;
        case "ArrowUp":
          if(volume < 0.9){
            this.setState({ volume: volume + 0.1 });
          }
          break;
        case "ArrowDown":
          if(volume >= 0.1){
            this.setState({ volume: volume - 0.1 });
          }
          else if(volume <= 0.1){
            this.setState({ volume: 0 });
          }
          break;
        default: 
          return;
      }
    }
  }

  onSetPlaybackRate = v => {
    this.setState({ playbackRate: v });
  }

  onSetSize = size => {
    this.setState({ size });
  }

  onSetCc = ccShow => {
    this.setState({ ccShow });

    if(this.track){
      if(ccShow === "Off"){
        this.setState({ ccTxt: null });
        this.track.removeEventListener("cuechange", this.onCueChange);
      }else{
        this.track.addEventListener("cuechange", this.onCueChange);
      }
    }
  }

  parseCC = fileConfig => {
    return fileConfig?.tracks ? ["Off", ...fileConfig.tracks.map(v => v.label)] : null;
  }

  setUrl = (url) => {
    return Array.isArray(url) ? 
      url.map(v => ({ 
          ...v, 
          src: Q.newURL(v.src).href, 
          sizes: v.sizes ?  v.sizes + "px" : undefined
        })) 
      : 
      Q.newURL(url).href;
  }

  onCueChange = (e) => {
    let cues = e.target.track.activeCues;
    let txt = cues[0]?.text;
    // console.log('e: ', e);
    // console.log('cues: ', cues);
    // console.log('txt: ', txt);

    this.setState({ ccTxt: txt });
  }

  onReady = (p) => {
    if(this.props.fileConfig?.tracks){
      let ply = p.getInternalPlayer();
      let trackActive = Q.domQ("track[default]", ply);
      // console.log('onReady p: ', p);
      // console.log('onReady trackActive: ', trackActive.label);
  
      if(trackActive){
        this.track = trackActive;
        this.track.addEventListener("cuechange", this.onCueChange);
        this.setState({ ccShow: trackActive.label });
      }
    }
  }

  // onAction = e => {
  //   let et = e?.target;
  //   console.log('onAction - user did something e: ', e);
  //   console.log('onAction target: ', et);
  // }

  onActive = () => {
    // let et = e?.target;
    // console.log('onActive - user is active e: ', e);
    // console.log('onActive - time remaining: ', this.idleTimer.getRemainingTime());
    // console.log('onActive target: ', et);
    this.setState({ active: true });
  }

  onIdle = () => {
    this.setState({ active: false });
  }

  ref = p => {
    this.player = p
  }

  getPlayerDOM = () => {
    return this.player?.getInternalPlayer();
  }

  render(){
    const { 
      reactPlayerClass, url, config, fileConfig, poster, 
      className, wrapStyle, onTheaterMode, // cc 
    } = this.props;

    const { 
      playing, played, duration, loaded, loop, volume, muted, playbackRate, 
      size, active, ccShow, ccTxt 
    } = this.state;// vPoster

    // console.log('this.getPlayerDOM: ', this.getPlayerDOM());
    // console.log('url: ', Q.isAbsoluteUrl(url));// 
    // console.log('url: ', Array.isArray(url));
    return (
      <div 
        ref={this.wrap} 
        className={
          Q.Cx("embed-responsive embed-responsive-16by9 playerQ", {
            "playing": playing,
            "active": active
          }, className)
        } 
        style={wrapStyle}
      >
        {
          this.wrap.current && 
            <IdleTimer
              ref={r => { this.idleTimer = r }} 
              element={this.wrap.current} 
              timeout={3000} // 1000 * 60 * 15
              debounce={250} 
              startOnMount={false} 
              onActive={this.onActive} 
              onIdle={this.onIdle} 
              // onAction={this.onAction} 
            />
        }

        <ReactPlayer 
          ref={this.ref} 
          // controls 
          className={reactPlayerClass} 
          width="100%" // auto
          height="100%" 
          config={{
            ...config, 
            file:{
              ...fileConfig, 
              attributes:{
                preload: poster ? "metadata" : "auto", 
                crossOrigin: "anonymous", 
                tabIndex: "-1",
                controlsList: "nodownload nofullscreen noremoteplayback",
                // className: poster ? "of-cov" : undefined,
                poster
              }
            }
          }}
          // 
          url={this.setUrl(url)} 
          playing={playing} 
          loop={loop} 
          playbackRate={playbackRate} 
          volume={volume} 
          muted={muted} 
          onReady={this.onReady} 
          // onStart={() => console.log('onStart')} 
          // onError={e => console.log('onError', e)} 
          onPlay={this.onPlay} 
          onPause={this.onPause} 
          onProgress={this.onProgress} 
          onDuration={this.onDuration} 
          onEnded={this.onEnded} 
        />

        <button type="button" 
          className="btn rounded-0 cauto shadow-none w-100 text-primary fa fa-2x position-absolute position-full zi-1 v-bezel" 
          onClick={this.onBezelPlayPause} 
          onKeyDown={this.onBezelKeyDown}
        />

        <Controls 
          targetFull={() => this.wrap.current} 
          playing={playing} 
          played={played} 
          duration={duration} 
          loaded={loaded} 
          volume={volume} 
          muted={muted} 
          playbackRate={playbackRate} 
          sizes={Array.isArray(url) ? url.map(v => v.sizes && Number(v.sizes)) : undefined} 
          size={size} // "360" 
          cc={this.parseCC(fileConfig)} 
          ccShow={ccShow} 
          onPlayPause={this.onPlayPause} 
          onMouseDownRange={this.onSeekMouseDown} 
          onMouseUpRange={this.onSeekMouseUp} 
          onChangeRange={this.onSeekChange} 
          onVolumeChange={this.onVolumeChange} 
          onMute={this.onMute} 
          onSetPlaybackRate={this.onSetPlaybackRate} 
          onSetSize={this.onSetSize} 
          onSetCc={this.onSetCc} 
          onTheaterMode={onTheaterMode} 
        />

        {/* DEV OPTION: Custom poster for youtube API etc */}
        {/* {(poster && Q.isAbsoluteUrl(url)) && 

        } */}

        {ccTxt && 
          <div className="v-cc">
            <span>{ccTxt}</span>
          </div>
        }
      </div>
    );
  }
}

PlayerQ.defaultProps = {
  onTheaterMode: () => {},

}; 

/*
<video 
  // controls 
  className="embed-responsive-item" 
  src="/media/video_360.mp4" 
/>

<React.Fragment> </React.Fragment>
*/
