// import { cloneDeep } from '../../utils/collection-q';

// RcTree utils
const recursiveAdd = (data, target, { 
	ext, 
	title, //  = "New_Folder"
	type = "directory", 
	key = "children" 
} = {}) => { // newItem
	if(Array.isArray(data)){
		let tree = [];
		let newItem = { 
			type, title, 
			key: "ti_" + Q.Qid()
		};
		
		data.forEach((item) => {
			if(Array.isArray(item[key])){ // item[key]				
				// setTimeout(() => $(node.element).children("ul").find('.tree-edit[data-id="'+ id +'"]').trigger("click"), 9);
				if(item.key === target.key){
					const isFile = type === "file" && ext;
					const fnames = item[key].filter((f) => isFile ? (f.title.startsWith(title) && f.title.endsWith("." + ext)) : f.title.startsWith(title));
					const name = title + (fnames.length > 0 ? "_" + (fnames.length + 1) : "");

					if(isFile){
						newItem.title = name + "." + ext;
						newItem.content = "";
					}else{
						newItem.title = name;
						newItem.children = [];
					}

					tree.push({
						...item, 
						[key]: [ ...item[key], newItem ] // newItem | { ...newItem, title: hasName }
					});
				}else{
					tree.push({
						...item, 
						[key]: recursiveAdd(item[key], target, { ext, title, type, key })
					});
				}
			}else{
				tree.push(item);
			}
		});

		return tree;
	}
}

// https://www.scraggo.com/recursive-filter-function/
// const noMe = item => item.key !== props.data.key;

/* const recursiveDel = (arr, keyCheck) => {
	const res = cloneDeep(arr);// [ ...arr ];
	return res.reduce((acc, item) => {
		// acc -> short for "accumulator" (array)
		// item -> the current array item
		let newItem = item;// so that we don't overwrite the item parameter

		if(item.children){
			newItem.children = recursiveDel(item.children);// here is the recursive call
		}
		if(item.key !== keyCheck){ // noMe(newItem) | cond(newItem)
			acc.push(newItem);// here's where acc takes the new item
		}
		return acc;// we always have to return acc
	}, []); // initialize accumulator (empty array)
}*/

export {
	recursiveAdd, 
	// recursiveDel, 
	
};