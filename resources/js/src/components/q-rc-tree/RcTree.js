import React, { useState, useEffect } from 'react';// , { useState, useEffect, useRef, } 
import Tree from 'rc-tree';// , { TreeNode }

import Btn from '../q-ui-react/Btn';
import { cloneDeep } from '../../utils/collection-q';// sortDirBy, recursiveEdit
import { getExt, setExt } from '../../utils/file/getSetExt';// 
import { recursiveAdd } from './utils';// , recursiveDel

function allowDrop({ dropNode, dropPosition }) {
  if (!dropNode.children) {
    if (dropPosition === 0) return false;
  }
  return true;
}

export default function RcTree({
	className, 
	addFolderName = "New_Folder", // For add new folder name
	addFileName = "Untitled", // For add new file name
	defaultExt = "js", // For add new file, set extension
	acceptExt = ["txt"], 
  textFiles = ["txt", "html", "css", "js"], // OPTION to readAs FileReader API 
	regExNodeName = /[\s/[/\]<>+=()&^%$#@!~`:"',{}/\\|?*\u0000-\u001F]/g, 
	tools = true, 
	inRef, 
	// rc-tree:
	treeData, 
	expandKeys = [], 
	...etc
}){
	const [dataTree, setDataTree] = useState(treeData);
	const [expandedKeys, setExpandedKeys] = useState(expandKeys);// ['0-0-key', '0-0-0-key', '0-0-0-0-key']
	const [autoExpandParent, setAutoExpandParent] = useState(true);
	const [selectKeys, setSelectKeys] = useState([]);
	const [editVal, setEditVal] = useState("");
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in RcTreePage','color:yellow;');
	// }, []);

  const onDragStart = info => {
    console.log('start: ', info);
  }

  // const onDragEnter = () => {
  //   console.log('enter');
  // }

  const onDrop = info => {
    console.log('onDrop info: ', info);
    const dropKey = info.node.key;
    const dragKey = info.dragNode.key;
    const dropPos = info.node.pos.split('-');
    const dropPosition = info.dropPosition - Number(dropPos[dropPos.length - 1]);
    const loop = (arr, key, callback) => {
      arr.forEach((item, index, arr) => {
        if (item.key === key) {
          callback(item, index, arr);
          return;
        }
        if (item.children) {
          loop(item.children, key, callback);
        }
      });
    };

    const datas = [ ...dataTree ];

    // Find dragObject
    let dragObj;
    loop(datas, dragKey, (item, index, arr) => {
      arr.splice(index, 1);
      dragObj = item;
    });
    if (dropPosition === 0) {
      // Drop on the content
      loop(datas, dropKey, item => {
        // eslint-disable-next-line no-param-reassign
        item.children = item.children || [];
        // where to insert 示例添加到尾部，可以是随意位置
        item.children.unshift(dragObj);
      });
    } else {
      // Drop on the gap (insert before or insert after)
      let ar;
      let i;
      loop(datas, dropKey, (item, index, arr) => {
        ar = arr;
        i = index;
      });
      if (dropPosition === -1) {
        ar.splice(i, 0, dragObj);
      } else {
        ar.splice(i + 1, 0, dragObj);
      }
    }

		setDataTree(datas);// this.setState({ gData: data });
  }

  const onExpand = exKeys => {
    // console.log('onExpand exKeys: ', exKeys);
		setExpandedKeys(exKeys);
		setAutoExpandParent(false);
    // this.setState({
    //   expandedKeys,
    //   autoExpandParent: false,
    // });
  }

	const onAddFolder = (e, props) => {
		// console.log('onAddFolder', expandedKeys);
		// e.stopPropagation();
		// expandedKeys.length > 0 && expandedKeys.find(f => f === props.data.key)
		if(expandedKeys.includes(props.data.key)){
			e.stopPropagation();
			console.log('is expand');
		}
		// recursiveAdd(data, target, { 
		// 	ext, 
		// 	title = "New_Folder", 
		// 	type = "directory", 
		// 	key = "children" 
		// })
		const datas = recursiveAdd(dataTree, props.data, { title: addFolderName });
		// console.log('onAddFolder props: ', props);
		// console.log('datas: ', datas);

		if(datas){
			setDataTree(datas);
		}
	}

	const onAddFile = (e, props) => {
		if(expandedKeys.includes(props.data.key)){
			e.stopPropagation();
		}

		const datas = recursiveAdd(dataTree, props.data, { ext: defaultExt, title: addFileName, type: "file" });
		setDataTree(datas);
	}

	const onAddRootFolderOrFile = (type = "directory") => {
		const isFile = type === "file";
		const fnames = dataTree.filter((f) => isFile ? f.title.startsWith(addFileName) && f.title.endsWith("." + defaultExt) : f.title.startsWith(addFolderName));
		const key = "ti_" + Q.Qid();
		const name = fnames.length > 0 ? "_" + (fnames.length + 1) : "";
		let newData = { key, type };

		if(isFile){
			newData.title = addFileName + name + "." + defaultExt;
			newData.content = "";
		}
		else{
			newData.title = addFolderName + name;
			newData.children = [];
		}

		setDataTree([ ...dataTree, newData ]);
	}

	const onDeleteItem = (props) => {
		// console.log('onDeleteItem props: ', props);
		const { title, type } = props;
		
		Swal.fire({
			icon: "warning", 
			title: "Are you sure to delete " + type + " " + title + "?", 
			// text: "Change name or cancel", 
			showCancelButton: true, 
			allowEnterKey: false, 
			confirmButtonText: "Yes", 
			cancelButtonText: "No" 
		}).then((v) => {
			if(v.isConfirmed){
				// const noMe = item => item.key !== props.data.key;
				const recursiveDel = arr => { // Can separate file / side function.???
					// const res = cloneDeep(arr);// [ ...arr ];
					return arr.reduce((acc, item) => {
						// acc -> short for "accumulator" (array)
						// item -> the current array item
						let newItem = item;// so that we don't overwrite the item parameter
			
						if(item.children){
							newItem.children = recursiveDel(item.children);// here is the recursive call
						}
						if(newItem.key !== props.data.key){ // noMe(newItem)
							acc.push(newItem);// here's where acc takes the new item
						}
						return acc;// we always have to return acc
					}, []); // init accumulator (empty array)
				}
				// const newData = recursiveDel(dataTree);// , props.data.key
				// console.log('newData: ', newData);
				setDataTree(recursiveDel(cloneDeep(dataTree)));
			}
		});
	}

	const onEditItem = (e, props) => {// 
		e.stopPropagation();
		// console.log('onEditItem props: ', props);

		const { key, title } = props.data;
		const recursiveEdit = (data, keyChild = "children") => { 		
			let tree = [];
			const edit = true;
			data.forEach((item) => {
				if(item.key === key){ // props.data.key
					tree.push({
						...item, 
						edit
					});
				}else{
					if(item.key === key){
						tree.push({
							...item, 
							edit
						});
					}else{
						if(Array.isArray(item[keyChild])){
							tree.push({
								...item, 
								[keyChild]: recursiveEdit(item[keyChild], keyChild)
							});
						}else{
							if(item.key === key){
								tree.push({
									...item, 
									edit
								});
							}else{
								tree.push(item);
							}
						}
					}
				}
			});
			return tree;
		}

		const newData = recursiveEdit(dataTree);
		// console.log('newData: ', newData);
		setDataTree(newData);
		setEditVal(title);
		setSelectKeys([key]);
	}

	const renderEdit = (obj) => {
		const recursiveEditTitle = (data, title = "") => { // key = "children" 		
			let tree = [];
			const dataEdit = {
				edit: false, 
				title
			};
			data.forEach((item) => {
				if(item.key === obj.data.key){ // // props.data.key
					tree.push({ ...item, ...dataEdit });
				}else{
					if(item.key === obj.data.key){// props.data.key
						tree.push({ ...item, ...dataEdit });
					}else{
						if(Array.isArray(item.children)){
							tree.push({
								...item, 
								children: recursiveEditTitle(item.children, title)
							});
						}else{
							if(item.key === obj.data.key){ // props.data.key
								tree.push({ ...item, ...dataEdit });
							}else{
								tree.push(item);
							}
						}
					}
				}
			});
			return tree;
		}

		const removeInput = (input, title, run) => {
			console.warn('run removeInput in : ', run);
			Q.setClass(input.parentElement, "tree-edit", "remove");
			const editData = recursiveEditTitle(dataTree, title);
			setDataTree(editData);
		}

		const saveEdit = (e) => {
			const input = e.target;
			const val = input.value.trim();// editVal.trim();
			let txtArr = val.split(".");
			const { type, title, data } = obj;
			const isFile = type === "file";
			// const isRegExValid = regExNodeName && regExNodeName.test(val);
			const swalOps = {
				icon: "warning", 
				allowEnterKey: false
			};
			// const fnames = data.children && data.children.length > 0 ? data.children.filter((f) => f.name === val) : [];
			const context = obj.context.keyEntities[data.key];
			const fnames = context.parent && context.parent.children.length > 0 ? context.parent.children.filter((f) => f.node.title === val) : dataTree.filter(f => f.title === val);
			
			// console.log('obj: ', obj);
			console.log('obj context: ', context);
			console.log('fnames: ', fnames);
			
			if(val.length < 1){ // Empty string / only contain space
				removeInput(input, title, "if(val.length < 1){");
				Swal.fire({
					...swalOps, 
					title: "Name should not be empty.", 
					willClose(){}
				});
				return;
			}
			else if(val.length > 0 && val !== title && regExNodeName && regExNodeName.test(val)){ //  && isNotValid.length > 0
				console.log('title: ', title);
				// console.log('title.length: ', title.length);
				removeInput(input, title, "else if(val.length > 0 && regExNodeName && regExNodeName.test(val)){");
				Swal.fire({
					...swalOps, 
					// "Name can't contain any of the following characters: \\ / : * ? \" |"
					title: "Name only alphanumeric, dash & underscore.",
					// didRender(){}
					willClose(){
						// input.focus();
					}
				});
				return;
			}
			else if(isFile && txtArr.length < 2){ // Item is file & dont have extension
				removeInput(input, title, "else if(isFile && txtArr.length < 2){");
				Swal.fire({
					...swalOps, 
					title: "File must have extension.",
					willClose(){}
				});
				return;
			}
			else if(isFile && !acceptExt.includes([...txtArr].pop())){ // Item is file & extension not valid
				// removeInput(input, title);
				Swal.fire({
					...swalOps, 
					title: "File extension not valid.", 
					text: "Only support extensions :\n" + acceptExt.join(", "), 
					// confirmButtonText: ""
					willClose(){
						input.focus();
					}
				});
				return;
			}
			else if(!(regExNodeName && regExNodeName.test(val)) && val !== title){
				// console.warn('else if(val !== title)');
				// console.warn('else if(val !== title) regExNodeName && regExNodeName.test(val): ', regExNodeName && regExNodeName.test(val));
				// if(!isRegExValid){
					if(fnames.length > 0){
						console.warn('if(fnames.length > 0)');
						Swal.fire({
							...swalOps, 
							title: type + " name is available.", 
							text: "Please change", 
							willClose(){
								input.focus();
							}
						});
						return;
					}
					
					if(!isFile){
						removeInput(input, val, "if(!isFile){");
						return;
					}else{
						const extOld = getExt(title);// name.split(".").pop();
						const extNew = [...txtArr].pop();
						// console.warn('extOld: ', extOld);
						// console.warn('extNew: ', extNew);
						
						if(extOld !== extNew){
							Swal.fire({
								...swalOps, 
								title: "Are you sure to change file " + title + "?", 
								text: "File extension " + extOld + " to " + extNew, 
								showCancelButton: true, 
								confirmButtonText: "Yes", 
								cancelButtonText: "No"
							}).then((v) => {
								if(v.isConfirmed){
									removeInput(input, val, "if(v.isConfirmed){");
								}else{
									removeInput(input, title, "else v.isConfirmed");
								}
							});
							return;
						}else{
							removeInput(input, val, "else extOld !== extNew");
							return;
						}
					}
				// }
			}
			else if(val === title && fnames.length > 1){ // 1
				console.warn('else if(val === title && fnames.length > 0)');
				Swal.fire({
					...swalOps, 
					title: type + " name is available.", 
					text: "Please change", 
					willClose(){
						input.focus();
					}
				});
				return;
			}
			else{
				removeInput(input, title, "else");
			}
			// else if(val === title){ // else
			// 	removeInput(input, title, "else");
			// }
		}

		return (
			<input className="form-control form-control-xs" type="text" autoFocus 
				value={editVal} 
				onChange={e => setEditVal(e.target.value)} 
				onClick={e => e.stopPropagation()}
				onFocus={e => {
					e.stopPropagation();
					Q.setClass(e.target.parentElement, "tree-edit");
				}}
				onBlur={e => {
					// Q.setClass(e.target.parentElement, "tree-edit", "remove");
					// const edit = recursiveEditTitle(dataTree);
					// setDataTree(edit);
					saveEdit(e);
				}}
				onKeyDown={e => {
					// e.stopPropagation();
					const { key, ctrlKey } = e;// ctrlKey
					if(key === "Enter"){
						saveEdit(e);
					}
					if(ctrlKey && (key === "s" || key === "S")){
						e.preventDefault();
						saveEdit(e);
					}
				}}
			/>
		);
	}

	const switcherIcon = obj => {
		// console.log('obj: ', obj);
		const ico = (cx) => (
			<>
				<i className={"q-fw qi qi-" + cx} />

				{obj.edit && renderEdit(obj)}
			</>
		);

		if(obj.isLeaf && obj.data.type === "file"){
			// obj.title.split(".").pop()
			return ico((getExt(obj.title, 1) ? setExt(obj.title) : "txt") + " i-color");// <i className={"q-fw i-color qi qi-" + obj.title.split(".").pop()} />;
		}
		return ico("folder" + (obj.expanded ? "-open" : ""));// <i className={"q-fw qi qi-folder" + (obj.expanded ? "-open" : "")} />;
	}

	return (		
		<div className={Q.Cx("q-rc-tree", className)}>
			{tools && 
				<div className="btn-group w-100" role="group">
					{[
						{ cx:"btn-add-root-folder", icon:"qi qi-folder", title:"Add Folder", fn: () => onAddRootFolderOrFile() }, 
						{ cx:"btnFile btn-upload-root-folder", icon:"qi qi-upload", title:"Upload Folder", fn: Q.noop }, // , fn: () => onUploadDir()
						{ cx:"btn-add-root-file", icon:"qi qi-file", title:"Add File", fn: () => onAddRootFolderOrFile("file") }, 
						{ cx:"btnFile btn-upload-root-file", icon:"qi qi-upload", title:"Upload File", fn: Q.noop } // 
					].map((v) => 
						v.cx.startsWith("btnFile") ? 
						<Btn key={v.cx} As="label" outline size="sm" kind="secondary" 
							className={Q.Cx(v.cx, v.icon, v.className)}
							title={v.title} 
							// onClick={() => v.fn()} 
						>
							{v.cx.endsWith("btn-upload-root-file") ? 
								<input onChange={() => v.fn()} type="file" accept={"." + acceptExt.join(",.")} multiple hidden />
								: 
								"" // <input type="file" webkitdirectory="" directory="" hidden />
							}
						</Btn>
						:
						<Btn key={v.cx} blur outline size="sm" kind="secondary" 
							className={Q.Cx(v.cx, v.icon, v.className)} title={v.title} 
							onClick={() => v.fn()} 
						/>
					)}

					{/* <Btn blur outline size="sm" kind="secondary" className="qi qi-file " title="Add File" />
					<Btn As="label" outline size="sm" kind="secondary" 
						className="btnFile qi qi-upload btn-upload-root-file" 
						title=""
					>
					</Btn> */}
				</div>
			}

			<Tree 
				{...etc} 
				ref={inRef} 
				className="q-rc-tree q-tree-tools" 
				draggable 
				// multiple 
				// selectable={false} 
				treeData={dataTree} // data | getTreeData() 
				// treeData={renderTree()}
				expandedKeys={expandedKeys} 
				autoExpandParent={autoExpandParent} 
				selectedKeys={selectKeys} 
				allowDrop={allowDrop} 
				onExpand={onExpand} 
				onDragStart={onDragStart}
				onDrop={onDrop} 
				onSelect={(keys, e) => { // selectedKeys
					const { node, nativeEvent } = e;
					// console.log('onSelect keys: ', keys);
					// console.log('onSelect node: ', node);
					// if(e.type !== "file"){
					// 	if(e.expanded) setExpandedKeys(expandedKeys.filter(f => f !== selectedKeys[0]));
					// 	else setExpandedKeys(selectedKeys);
					// }
					
					// OPTION
					// if(node.type !== "file" && node.expanded){
					// 	setSelectKeys([]);
					// }
					// else{
					// 	setSelectKeys([node.key]);// keys
					// }

					setSelectKeys([node.key]);// keys

					if(node.type !== "file"){
						const p = nativeEvent.target.closest(".rc-tree-treenode");// Q.domQ(".rc-tree-treenode", e.nativeEvent.target);
						// console.log('onSelect p: ', p);
						Q.domQ(".rc-tree-switcher", p).click();
					}
				}}
				// onRightClick={(val) => {
				// 	console.log('onRightClick val: ', val);
				// }} 
				switcherIcon={switcherIcon} 
				// showIcon={false} 
				icon={(props) => {
					// console.log('props: ',props);
					return (
						<div className="btn-group btn-group-xs">
							{props.type !== "file" && 
								<>
									<Btn onClick={e => onAddFolder(e, props)} kind="flat" className="qi qi-folder" title="Add Folder" />
									<Btn onClick={e => onAddFile(e, props)} kind="flat" className="qi qi-file" title="Add File" />
								</>
							}

							<Btn onClick={(e) => onEditItem(e, props)} kind="flat" className="qi qi-edit" title="Edit" />
							<Btn onClick={() => onDeleteItem(props)} kind="flat" className="qi qi-trash" title="Remove" />
						</div>
					)
				}} 
			/>
		</div>
	);
}



