import React, { createRef } from 'react';
// import loadable from '@loadable/component';
// import { useInView } from '../react-intersection-observer';// InView

import SplitPane from '../react-split-pane';
import Flex from '../q-ui-react/Flex';
import Btn from '../q-ui-react/Btn';
import Switch from '../q-ui-react/Switch';
// import UctrlAlert from '../../components/q-react-bootstrap/UctrlAlert';// OPTION
import Browser from '../browser/Browser';

import Compiler from './Compiler';
import { ControlledEditor } from '../monaco-react';// {ControlledEditor} | Editor | IdeCtrl
import onDidMount from '../monaco-react/ide/onDidMount';
// import { ResizeSensor } from '../blueprintjs/components/resize-sensor/resizeSensor';
// import ResizeSensor from '../q-ui-react/ResizeSensor';
import { Copy } from '../../utils/clipboard/clipboardApi';
// import copy from '../../utils/clipboard/copy-to-clipboard';
// import {xhrScript} from '../../utils/xhrScript';

/* const Bs = loadable.lib(() => import('@babel/standalone'));
// import BabelStandalone from '@babel/standalone';
console.log('Bs: ', Bs);

function BabelStandalone({ date }) {
	// ({ default: b })
  return (
    <div>
			<Bs fallback={<div>Loading Babel...</div>}>
				{m => {
					console.log('render BabelStandalone: ', b);
				}}
			</Bs>
    </div>
  )
} */

// import { LazyLoadComponent } from '../../components/react-lazy-load-image-component';// trackWindowScroll

/** NOT FIX ISSUE: can't render component twice if Babel load with getScript */
export default class LiveCode extends React.Component{
  constructor(props){
		super(props);
		this.state = {
			// themes: props.theme,
			pos: "vertical",
			ideOpen: props.openIde,// false |  props.openIde
			autoRun: props.auto,
			codeAuto: undefined, // undefined | props.code
			codeNoAuto: undefined, // undefined | props.code
			err: null,
			editorReady: false, 
			maxSize1: document.body.clientWidth - 320, // 800
			// maxSize2: 290
		};
		
		this.ideRef = createRef();
		this.liveRef = createRef();
	}	
	
	componentDidMount(){// async 
		// console.log('%ccomponentDidMount in LiveCode:', 'color:yellow');
    this.onGetBabel();
		
		/* const {code, onRender} = this.props;
		if(window.Babel){
			this.setState({
				codeAuto: code,
				codeNoAuto: code
			}, () => onRender());
		}else{
			Qrequirejs(['/storage/app_modules/@babel-standalone/babel.min.js'],(Babel) => {
				console.log('Babel set', Babel);
				this.setState({
					codeAuto: code,
					codeNoAuto: code
				}, () => onRender());
			});
		} */
		
		/* if(window.Babel){//  && renderCount < 1
			this.setState({
				codeAuto: code,
				codeNoAuto: code
			}, () => onRender());
		}else{
			xhrScript("/storage/app_modules/@babel-standalone/babel.min.js", "Babel", 
				(fn, v) => {
					// console.log('xhrScript: ', v);
					// console.log('xhrScript: ', fn);
					this.setState({
						codeAuto: code,
						codeNoAuto: code
					}, () => onRender());
				},
				(e) => console.warn(e)
			);
		} */
	}
	
	onGetBabel = () => {// async 
		// const { code, onRender } = this.props;
		if(window.Babel){
			// console.log('%cBabel available: ', 'color:orange', window.Babel);
			this.setBabel();
		}
		else{
			let windowDefine = window.define;
			if(window.monaco && windowDefine){
				window.define = null;
				// delete window.define;
				// window.AMDLoader = null;
			}
			
			// await 
			Q.getScript({ src:"/storage/app_modules/@babel-standalone/babel.min.js", async:false, "data-js":"Babel" })
			.then(() => {
				if(window.Babel){
					// console.log('%cif Babel: ', 'color:orange', window.Babel);
					this.setBabel();
					window.define = windowDefine;
				}
				else{
					console.log('%celse Babel: ', 'color:orange', window.Babel);
					// @babel/standalone | @babel/core
					importShim("https://jspm.dev/@babel/standalone").then(m => {
						console.log('importShim: ', m);
						window.Babel = m.default;// store Babel to window
						window.define = windowDefine;
						
						this.setBabel();
					}).catch(e => console.log('importShim catch: ', e));
				}
			}).catch(e => console.warn(e));// NOT RUN...!!!
		}
	}
	
	setBabel = () => {
		const { code, onRender } = this.props;
		
		this.setState({
			codeAuto: code,
			codeNoAuto: code
		}, () => onRender());
	}
	
/** CREATE component / method */
  onDidChangeEditor = (v, editor) => {
    if(!this.state.editorReady){
      this.setState({editorReady: true});
      this.ideRef.current = editor;
      
			onDidMount(v, editor);// , {required:true, onMount: noop}
    }
  }
  
  /* onSetPosition = () => {
		const liveRef = this.liveRef.current;
    this.setState(s => ({
			pos: s.pos === 'vertical' ? 'horizontal':'vertical',
			maxSize1: s.pos === 'vertical' ? liveRef.clientHeight / 1.3 : liveRef.clientWidth / 1.3
		}), () => {
      let compilerCode = Q.domQ('.compilerCode', liveRef);
      if(compilerCode){
        let s = compilerCode.style;
        if(s.width !== '50%' && s.height !== '50%'){
          this.state.pos === 'vertical' ? s.width = '50%' : s.height = '50%';
        }
      }
    })
  } */

  /* onResizeEnd = (size) => {
    // `${e.contentRect.width} x ${e.contentRect.height}`
    // const ents = size.map(e => [e.contentRect.width, e.contentRect.height]);
    // const setMaxSize1 = this.state.pos === 'vertical' ? ents[0][0] / 1.3 : ents[0][1] / 1.3;
    // this.setState({maxSize1: setMaxSize1});
		
		console.log('onResizeEnd size: ', size);
		console.log('onResizeEnd this.liveRef: ', this.liveRef);
		// window.scrollTo(0, window.scrollY + document.body.offsetHeight);// document.body.offsetHeight
		this.setState({ maxSize2: size.height / 1.2 });
  } */
	
	onCopy = (to, e) => {
		let et = e.target;
		Copy(to, {
			onOk: t => {
				if(et){
					let pTitle = Q.getAttr(et, 'aria-label');// et.title;
					Q.setAttr(et, {'aria-label':'Copied'});// et.title = 'Copied';
					setTimeout(() => Q.setAttr(et, {'aria-label':pTitle}), 1000);// et.title = pTitle
				}
			},
			onErr: e => console.warn('Error copy: ',e)
		})
	}
	
	renderLiveCode = () => {
		const { codeAuto, codeNoAuto, autoRun } =  this.state;
		
		if(codeAuto){
			return (
				<Compiler 
					babel={window.Babel} 
					code={autoRun ? codeAuto : codeNoAuto} 
					scope={this.props.scope} 
					setError={e => {
						// if(e){
							// this.setState({ err: e.replace('/file.tsx: ', '') });
						// }else{
							// this.setState({ err: null });
						// }
						
						this.setState({ err: e ? e.replace('/file.tsx: ', '') : null });
					}}
					// preview={err !== null ? ErrBabel : null} // ???
				/>
			)
		}
	}
	
	render(){
		// auto, code, inRef
    const { scope, h, minH, maxH, className, viewClass, liveBoxClass, browser, onChange } = this.props;
    // themes, maxSize2
		const { editorReady, ideOpen, pos, autoRun, codeAuto, codeNoAuto, err, maxSize1 } = this.state;
		
		// <LazyLoadComponent>
		return (
			
				<Flex // inRef={inRef} 
					dir="column" className={Q.Cx("liveCode", className)}
				>
					{/* <BabelStandalone /> */}
				
					<Flex className="p-1 bg-light border border-bottom-0 ml-1-next">			
						<Btn outline 
							onClick={() => this.setState({ideOpen: !ideOpen})} 
							size="sm" 
							className={"tip tipBL qi qi-eye" + (ideOpen ? "-slash":"")} 
							aria-label="Show/Hide Code"
						/>

						{(editorReady && ideOpen) && 
							<React.Fragment>
								<Btn outline 
									onClick={e => this.onCopy(codeAuto, e)} 
									size="sm" 
									className="tip tipBL qi qi-copy" 
									aria-label="Copy Code" 
								/>
								{/* <Btn outline 
									onClick={this.onSetPosition} 
									size="sm" 
									className="tip tipBL qi qi-columns" // {"tip tipBL q-col-resize q-s12" + (pos === "vertical" ? "-r90":"")} 
									aria-label="Change Orientation" 
								/> */}

								{/* <select onChange={e => {
									this.setState({themes: e.target.value}, () => console.log('themes: ', this.state.themes));
								}} value={themes} className="custom-select custom-select-sm w-auto">
									<option value="light">Light</option>
									<option value="dark">Dark</option>
								</select> */}
								
								<Switch // checked={!autoRun} // disabled={!ideOpen} 
									className="btn btn-flat btn-sm border-secondary pl-5 pr-3 tip tipBL" 
									parentProps={{
										"aria-label":"Auto Run"
									}} 
									label="Auto" // Auto Run
									defaultChecked={autoRun}  
									onChange={e => this.setState({autoRun: e.target.checked})}
								/>
							
								{!autoRun && 
									<Btn 
										onClick={() => this.setState({codeNoAuto: codeAuto})} 
										size="sm" 
										className="tip tipBL qi qi-play"  
										aria-label="Run" 
									/>
								}
							</React.Fragment>
						}
					</Flex>
					
					{/* <ResizeSensor 
						wait={20} // 90 
						onResizeEnd={this.onResizeEnd} 
					> */}
						<div ref={this.liveRef} 
							tabIndex="-1" 
							style={{
								height: h,
								minHeight: minH,
								maxHeight: maxH
							}} 
							className={Q.Cx("position-relative liveBox", liveBoxClass)}   
							title="Resize" 
						>
							<SplitPane
								split={pos} // "horizontal" // vertical
								minSize={50}
								maxSize={maxSize1} // 1000 | 300
								// defaultSize={100}
								// defaultSize="100%" // 50%
								size={ideOpen ? "50%" : "100%"} 
								allowResize={ideOpen} 
								className="border position-absolute pb-4" 
								// className={Q.Cx("border position-absolute", themes)} 
								pane1Class="compilerCode" 
								pane2Class="ovhide bg-dark" 
								// pane2Class={`ovhide bg-${themes}`} 
								parentProps={{
									title: "" 
								}}
							>
								<div className={Q.Cx("bg-white h-100 ovyscroll q-scroll ovscroll-auto liveView", viewClass)}>
									{browser ? 
										<Browser type={null}>
											{this.renderLiveCode()}
										</Browser>
										: 
										this.renderLiveCode()
									}
								</div>

								{ideOpen && 
									<SplitPane 
										split="horizontal" 
										allowResize={err !== null} 
										size={err !== null ? "50%" : "100%"} 
										// maxSize={maxSize2} // 290
										pane1Class={err !== null ? "mxh-80" : undefined} 
										// resizerClass={err === null ? 'd-none':''} 
										pane2Class="bg-white ovhide"
									>
										<div className="w-100 live-app">										
											<ControlledEditor
												// height="calc(100% - 31px)" // calc(100vh - 75px) 
												// className={Cx({'d-none': tools !== 'Compiler'})} 
												editorDidMount={this.onDidChangeEditor} 
												value={codeAuto} 
												onChange={(e, codeAuto) => {
													this.setState({codeAuto});
													onChange(codeAuto);// OPTION
												}} 
												// language={compilerLang} // ideLang | 'javascript'
												// theme={themes} 
												options={{
													fontSize: 14, // fontSize
													tabSize: 2, // tabSize
													minimap: {
														enabled: false
													}
												}}
											/>
										</div>
										
										{err && <pre className="alert alert-danger mb-0 border-0 rounded-0 h-100 q-scroll ovscroll-auto">{err}</pre>} 
									</SplitPane>
								}
							</SplitPane>
						</div>
					{/* </ResizeSensor> */}
				</Flex>
			
		);
		// </LazyLoadComponent>
	}
}

LiveCode.defaultProps = {
  // theme: 'dark', // light | dark
	scope: {}, 
  h: 150, // 300
	minH: 150, // 300
	// maxH: 300,
	// code: '',
	openIde: false,
	auto: false,
	onRender: Q.noop,
	onChange: Q.noop // OPTION
};

// export default trackWindowScroll(LiveCode);
// export default React.memo(LiveCode, (prevProps, nextProps) => prevProps.code === nextProps.code);

/*
</React.Fragment>
*/