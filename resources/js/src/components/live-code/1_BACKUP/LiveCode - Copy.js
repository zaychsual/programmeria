import React from 'react';
import SplitPane from '../../component-etc/react-split-pane';
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-react-bootstrap/Btn';

const errorBoundary = (El, errorCb) => {
	class ErrorBoundary extends React.Component{
		componentDidCatch(error){
			console.log('ErrorBoundary error: ',error);
			errorCb(error);
		}
		render(){
			if(typeof El === 'undefined') return null;
			return typeof El === 'function' ? React.createElement(El, null) : El;
		}
	}
	return ErrorBoundary;
};

const evalCode = (code, scope = {}) => {
	if(window.Babel){
		try{// transform | transformFromAst
			const parseCode = window.Babel.transform(code, {
				// ast: true,
				// code: false,
				// retainLines: true,
				// compact: true, // OPTION ISSUE: CAN'T REPLACE function _extends()
				// minified: true, // OK
				// inputSourceMap: false,
				// sourceMaps: false,
				filename: 'file.tsx',
				comments:false,
				presets: ['es2017','react','typescript','flow'], // ,'stage-2' | 'env',
				plugins: [
					['proposal-class-properties'], // , { "loose": true }
					// 'transform-modules-amd',
					// 'transform-modules-commonjs',
					// 'transform-modules-umd',
				],
				// modules:['amd','umd','cjs']
			});

			const resultCode = parseCode ? parseCode.code : '';
			const scopeKeys = Object.keys(scope);
			const scopeValues = Object.values(scope);
			const res = new Function('React', ...scopeKeys, `return ${resultCode}`);
			const ResCall = res(React, ...scopeValues);
			
			// const strExtendsFn = 'function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }';
			// const strExtendsFn = 'function _extends(){_extends=Object.assign||function(target){for(var i=1;i<arguments.length;i++){var source=arguments[i];for(var key in source){if(Object.prototype.hasOwnProperty.call(source,key)){target[key]=source[key];}}}return target;};return _extends.apply(this,arguments);}';
			
			// const replaceExtends = resultCode.replace(strExtendsFn, '');
			// const ext2ObjectAssign = replaceExtends.replace(/_extends/gm, 'Object.assign').trim();//
			
			console.log('%cresultCode: ', LOG_DEV, resultCode);
			// console.log('%creplaceExtends: ', LOG_DEV, replaceExtends);
			// console.log('%cext2ObjectAssign: ', LOG_DEV, ext2ObjectAssign);
			
			// return ext2ObjectAssign;
			// return res(React, ...scopeValues);
			console.log('ResCall: ', ResCall);
		}
		catch(e){
			console.warn(e);
			// return null;
		}
	}else{
		console.log('%cwindow.Babel NOT AVAILABLE: ', LOG_DEV);
	}
};

const generateEl = (code, scope, errorCb) => {
  return errorBoundary(evalCode(code, scope), errorCb);
};

export default function LiveCode({theme, code}){
	const [themes, setThemes] = React.useState(theme);
  const [pos, setPos] = React.useState('vertical');
	const [ideOpen, setIdeOpen] = React.useState(false);
	const [codes, setCodes] = React.useState(code);
	const [Out, setOut] = React.useState(null);
	const [Err, setErr] = React.useState(null);
	
	React.useEffect(() => {
		// console.log('%cuseEffect in LiveCode', LOG_DEV);
		getScripts(
			{src:"/storage/app_modules/@babel-standalone/babel.min.js", "data-js":"Babel"}, 
			// {src:"/storage/app_modules/rollup/dist/rollup.browser-1.29.1.js", attrs:{"data-js":"rollup"}}  // rollup.browser-1.29.1
		)
		.then(v => {
			// console.log(v);
			if(window.Babel){
				console.log('%cBabel: ', LOG_DEV, window.Babel);
			}
		}).catch(e => console.log(e));// NOT RUN...!!!
	}, []);

	const onRun = (code) => {
		try{
			// , Modal, ModalHeader
			const component = generateEl(code, {Btn}, (err) => {
				console.log('try err: ',err);
				setErr(err.toString());// setError(error.toString());OutCompile: null, 
			});
			console.log('component: ',component);
			setOut(component);
			setErr(null);
			// this.setState({
				// Out: component, // component
				// Err: null
			// });
		}
		catch(err){
			console.log('catch err: ',err);
			setErr(err.toString());
		}
	}
	
	return (
		<Flex dir="column">
			<Flex className="ml-1-next">
				<Btn onClick={() => setIdeOpen(!ideOpen)} size="sm">Open ide</Btn>
				<Btn onClick={() => setPos(pos === 'vertical' ? 'horizontal':'vertical')} size="sm">Position {pos}</Btn>
				<select onChange={e => setThemes(e.target.value)} value={themes} className="custom-select custom-select-sm w-auto">
					<option value="light">Light</option>
					<option value="dark">Dark</option>
				</select>
				
				<Btn size="sm" kind="primary" className="ml-auto" 
					onClick={() => {
						onRun(codes, {Btn});
						// const compile = babelTransform(codes);
						// setOut(compile);
					}}
				>RUN</Btn>
			</Flex>
			
			<div style={{height:300}} 
				className={Cx("position-relative", `bg-${themes}`)} 
			>
				<SplitPane
					split={pos} // "horizontal" // vertical
					minSize={50}
					maxSize={1000} // 300
					// defaultSize={100}
					// defaultSize="100%" // 50%
					size={ideOpen ? "50%" : "100%"} 
					allowResize={ideOpen}
					className={Cx("border position-absolute", `text-${themes === 'dark' ? 'white':'dark'}`, themes)} // vh-100
					// style={{height:300}}
				>
					<div className="h-100 bg-white">
						{Out && <Out />}
					</div>

					{ideOpen && 
						<SplitPane split="horizontal">
							<div className="w-100 live-app">
								<textarea onChange={e => setCodes(e.target.value)} value={codes} className="form-control form-control-sm" rows="9" />
							</div>

							<pre className="alert alert-danger mb-0 border-0 rounded-0 h-100">
								error compile
							</pre>
						</SplitPane>
					}
				</SplitPane>
			</div>
		</Flex>
	);
}

LiveCode.defaultProps = {
	theme: 'light', // dark
	code: ''
};

/*
</React.Fragment>
*/
