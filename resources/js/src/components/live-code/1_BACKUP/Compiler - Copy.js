import React from 'react';
// import presetReact from '@babel/preset-react';// OPTION

const errorBoundary = (El, errorCb) => {
	class ErrorBoundary extends React.Component{
		componentDidCatch(error){
			console.log('ErrorBoundary error: ',error);
			errorCb(error);
		}
		render(){
			if(typeof El === 'undefined') return null;
			return typeof El === 'function' ? React.createElement(El, null) : El;
		}
	}
	return ErrorBoundary;
};

// const RenderCode = ({code, scope = {}}) => {
const evalCode = (babel, code, scope = {}, presets = []) => {
	// if(window.Babel){ // window.buble && code && code.length > 0
		// const parseCode = buble.transform(code.trim());
		// const parseCode = window.Babel.transform(code, {
        const parseCode = babel.transform(code, {
			// ast: true,
			// code: false,
			// retainLines: true,
			// compact: true, // OPTION ISSUE: CAN'T REPLACE function _extends()
			minified: true, // OK
			inputSourceMap: false,
			sourceMaps: false,
			// TS preset needs this and it doesn't seem to matter when TS preset is not used, so let's keep it here?
			filename: 'file.tsx',
			comments:false,
			presets: ['es2017','react','typescript','flow', ...presets], // ,'stage-2' | 'env',
			// presets: presets ? [presetReact, ...presets] : [presetReact],
			plugins: [
				['proposal-class-properties'], // , { "loose": true }
				// 'transform-modules-amd',
				// 'transform-modules-commonjs',
				// 'transform-modules-umd',
			],
			// modules:['amd','umd','cjs']
		});

		const resCode = parseCode ? parseCode.code : '';
		const scopeKeys = Object.keys(scope);
		const scopeVals = Object.values(scope);
		// eslint-disable-next-line
		const res = new Function('React', ...scopeKeys, `return ${resCode}`);
		// eslint-disable-next-line
		return res(React, ...scopeVals);
	// }
	// return null;// OPTION
};

const generateEl = (babel, code, scope, presets, errorCb) => {
  return errorBoundary(evalCode(babel, code, scope, presets), errorCb);
};

const parseCode = (babel, code, scope, presets, setOutput, setError) => {
	try{
		const component = generateEl(babel, code, scope, presets, (err) => {
      // console.log('%ctry component err: ', LOG_DEV, err);
			setError(err.toString());
		});
		setOutput({ component });
		setError(null);
	}
	catch(err){
		// console.log('%ccatch err: ', LOG_DEV, err);
		// setOutput({ component: code });// Q_CUSTOM (DEV)
		setError(err.toString());
	}
};

// Output, Error
// code, Placeholder, minHeight,
const Compiler = ({babel, code, scope, presets, setError, preview, previewClass}) => {
  const [output, setOutput] = React.useState({component: null});
	// const [error, setError] = React.useState(null);// { where: '', msg: null }

  React.useEffect(() => {
    if(babel) parseCode(babel, code, scope, presets, setOutput, setError);
  }, [babel, code, scope, presets, setError]);

  const El = output.component;
  const Preview = preview;

  return El ? <El /> : Preview ? <Preview className={previewClass} /> : null;
};

export default React.memo(Compiler, (prevProps, nextProps) => prevProps.code === nextProps.code);
// export default Compiler;

// <React.Fragment></React.Fragment>

