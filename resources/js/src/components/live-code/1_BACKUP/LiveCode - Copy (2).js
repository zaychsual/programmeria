import React from 'react';
import SplitPane from '../../component-etc/react-split-pane';
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-react-bootstrap/Btn';
// import UctrlAlert from '../../components/q-react-bootstrap/UctrlAlert';// OPTION
import Compiler from './Compiler';
// 
// const ERR_BABEL = 'React.createElement("div",{className:"alert alert-danger rouded-0 h-100",role:"alert"}, React.createElement("h6",null,"Babel not loaded!"),React.createElement("p",null,"Please try again."));';
// const ErrBabel = () => (
	// <div className="alert alert-danger rouded-0 h-100" role="alert">
		// <h6>Babel not loaded!</h6>
		// <p>Please try again.</p>
	// </div>
// );

// NOT RUN
// window.countRender = [];

// 
export default function LiveCode({theme, code, auto}){
	const [themes, setThemes] = React.useState(theme);
  const [pos, setPos] = React.useState('vertical');
	const [ideOpen, setIdeOpen] = React.useState(false);
	const [autoRun, setAutoRun] = React.useState(auto);
	const [codeAuto, setCodeAuto] = React.useState('');// code
	const [codeNoAuto, setCodeNoAuto] = React.useState('');// code | auto ? code : null
	const [err, setErr] = React.useState(null);
	
	React.useEffect(() => {
		// console.log('%cuseEffect in LiveCode', LOG_DEV);
		// {src:"/storage/app_modules/rollup/dist/rollup.browser-1.29.1.js", attrs:{"data-js":"rollup"}}
		if(window.Babel){
			setTimeout(() => {
				// window.countRender.push('load');
				setCodeAuto(code);
				setCodeNoAuto(code);
				console.log('%cBabel available', 'color:yellow');
				// console.log(window.countRender);
			},9);
		}else{
			getScripts({src:"/storage/app_modules/@babel-standalone/babel.min.js", "data-js":"Babel"})
			.then(v => {
				if(window.Babel){
					// window.countRender.push('load');
					
					setCodeAuto(code);
					setCodeNoAuto(code);
					console.log('%cBabel: ', LOG_DEV, window.Babel);
					// console.log(window.countRender);
				}
				/* else{
					setErr(<div className="alert alert-danger rouded-0 h-100" role="alert">
						<h6>Babel not loaded!</h6>
						<p>Please try again.</p>
					</div>);// setCodeAuto(ERR_BABEL);
					// setCodeNoAuto(ERR_BABEL);
				} */
			}).catch(e => console.log(e));// NOT RUN...!!!
		}
	}, []);// code
	
	return (
		<Flex dir="column" className="liveCode">
			<Flex className="ml-1-next">
				<Btn onClick={() => setAutoRun(!autoRun)} size="sm" kind={autoRun ? "primary" : "secondary"}>Auto Run: {autoRun + ''}</Btn>
				<Btn onClick={() => setIdeOpen(!ideOpen)} size="sm">Open ide</Btn>
				<Btn onClick={() => setPos(pos === 'vertical' ? 'horizontal':'vertical')} size="sm">Position {pos}</Btn>
				<select onChange={e => setThemes(e.target.value)} value={themes} className="custom-select custom-select-sm w-auto">
					<option value="light">Light</option>
					<option value="dark">Dark</option>
				</select>
				<Btn onClick={() => setCodeNoAuto(codeAuto)} disabled={autoRun} size="sm" kind="primary">RUN</Btn>
			</Flex>
			
			<div style={{height:300}} 
				className={Cx("position-relative", `bg-${themes}`)} 
			>
				<SplitPane
					split={pos} // "horizontal" // vertical
					minSize={50}
					maxSize={1000} // 300
					// defaultSize={100}
					// defaultSize="100%" // 50%
					size={ideOpen ? "50%" : "100%"} 
					allowResize={ideOpen}
					className={Cx("border position-absolute", `text-${themes === 'dark' ? 'white':'dark'}`, themes)} // vh-100
					// style={{height:300}}
				>
					<div className="h-100 bg-white">
						{(codeAuto.length > 0 && codeNoAuto.length > 0) && 
							<Compiler 
								code={autoRun ? codeAuto : codeNoAuto} 
								scope={{Btn}} 
								setError={(e) => {
									// window.Babel
									if(e){
										let rep = e.replace('/file.tsx: ', '');
										setErr(rep);
									}
								}}
								// preview={err !== null ? ErrBabel : null} 
								className="liveCoding"  
							/>
						}
					</div>

					{ideOpen && 
						<SplitPane 
							split="horizontal" 
							// allowResize={err !== null} 
							size={err !== null ? "50%" : "100%"} 
							pane2Class="bg-white ovauto q-scroll"
						>
							<div className="w-100 live-app">
								<textarea onChange={e => setCodeAuto(e.target.value)} value={codeAuto} className="form-control form-control-sm" rows="9" />
							</div>
							
							{err && <pre className="alert alert-danger mb-0 border-0 rounded-0 h-100">{err}</pre>} 
						</SplitPane>
					}
				</SplitPane>
			</div>
		</Flex>
	);
}

LiveCode.defaultProps = {
	theme: 'light', // dark
	code: '',
	auto: false
};

// export default React.memo(LiveCode, (prevProps, nextProps) => prevProps.code === nextProps.code);

/*
</React.Fragment>
*/
