import React from 'react';
// import { useInView } from '../react-intersection-observer';// InView
import SplitPane from '../../component-etc/react-split-pane';
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-react-bootstrap/Btn';
import Switch from '../../components/q-react-bootstrap/Switch';
// import UctrlAlert from '../../components/q-react-bootstrap/UctrlAlert';// OPTION
import Compiler from './Compiler';
import {ControlledEditor} from '../../components/monaco-react';
// 
// const ERR_BABEL = 'React.createElement("div",{className:"alert alert-danger rouded-0 h-100",role:"alert"}, React.createElement("h6",null,"Babel not loaded!"),React.createElement("p",null,"Please try again."));';
// const ErrBabel = () => (
	// <div className="alert alert-danger rouded-0 h-100" role="alert">
		// <h6>Babel not loaded!</h6>
		// <p>Please try again.</p>
	// </div>
// );

// NOT RUN
// window.countRender = [];

/** NOT FIX ISSUE: can't render component twice */
// export default 
function LiveCode({inRef, theme, code, auto, onRender}){
  // const [ref, inView, entry] = useInView({
		// triggerOnce: true,
    // threshold: 0 /* Optional options */
  // });
	const [themes, setThemes] = React.useState(theme);
  const [pos, setPos] = React.useState('vertical');
	const [ideOpen, setIdeOpen] = React.useState(false);
	const [autoRun, setAutoRun] = React.useState(auto);
	const [codeAuto, setCodeAuto] = React.useState(undefined);// code | '' | undefined
	const [codeNoAuto, setCodeNoAuto] = React.useState(undefined);// code | '' | auto ? code : null
	const [err, setErr] = React.useState(null);
	const [editorReady, setEditorReady] = React.useState(false);
	
	const ideRef = React.useRef(null);
	
	React.useEffect(() => {
		// console.log('%cuseEffect in LiveCode', 'color:yellow');
			/* getScripts({src:"/storage/app_modules/@babel-standalone/babel.min.js", "data-js":"Babel"})
			.then(v => {
				if(window.Babel){
					// window.countRender.push('load');
					// setCodeAuto(code);
					// setCodeNoAuto(code);
					console.log('%cBabel: ', 'color:orange', window.Babel);
					// console.log(window.countRender);
					onRender();
				}
			}).catch(e => console.log(e)); */
			
		if(window.Babel){
			console.log('%cBabel available: ', 'color:orange', window.Babel);
			
			setCodeAuto(code);
			setCodeNoAuto(code);
			onRender();
		}
		else{
			getScripts({src:"/storage/app_modules/@babel-standalone/babel.min.js", "data-js":"Babel"})
			.then(v => {
				if(window.Babel){
					setTimeout(() => {
						setCodeAuto(code);
						setCodeNoAuto(code);
						console.log('%cBabel: ', 'color:orange', window.Babel);
						// console.log(window.countRender);
						onRender();
					},9);
				}
			}).catch(e => console.log(e));// NOT RUN...!!!
		}
	}, []);// code
	
  const onDidChangeEditor = (e, editor) => {
    if(!editorReady){
      setEditorReady(true);
      ideRef.current = editor;
      
      if(editor){ // this.ideMain.current
				// Get bind editor contextmenu FROM https://github.com/Microsoft/monaco-editor/issues/484
				editor.onContextMenu(e => {
					let ctxMenu = domQ(".monaco-menu-container", editor.getDomNode());// this.ideMain.current
					if(ctxMenu){
						// window.outerHeight
						let ee = e.event;
						const posY = (ee.posy + ctxMenu.clientHeight) > window.innerHeight ? ee.posy - ctxMenu.clientHeight : ee.posy;
						// window.outerWidth
						const posX = (ee.posx + ctxMenu.clientWidth) > document.body.clientWidth ? ee.posx - ctxMenu.clientWidth : ee.posx;
				
						ctxMenu.style.position = "fixed";// OPTIONS: set in css internal
						ctxMenu.style.top = Math.max(0, Math.floor(posY)) + "px";
						ctxMenu.style.left = Math.max(0, Math.floor(posX)) + "px";
					}
				});
      }
    }
	}	

// <h2>{`LiveCode viewport ${inView}.`}</h2>
//  style={{'--lc':themes === 'light' ? '#000':'#fff'}}
	return (
		<Flex inRef={inRef} dir="column" className="liveCode">
			<Flex className="ml-1-next">				
				<Btn onClick={() => setIdeOpen(!ideOpen)} size="sm" className="q q-json q-s12" tip="Show code" />
				
				{(editorReady && ideOpen) && 
					<React.Fragment>
						<Btn onClick={() => setPos(pos === 'vertical' ? 'horizontal':'vertical')} size="sm">{pos}</Btn>
						<select onChange={e => setThemes(e.target.value)} value={themes} className="custom-select custom-select-sm w-auto">
							<option value="light">Light</option>
							<option value="dark">Dark</option>
						</select>
						
						{/* <Btn onClick={() => setAutoRun(!autoRun)} size="sm" kind={autoRun ? "primary" : "secondary"} disabled={!ideOpen}>Auto Run</Btn> */}
						<Switch 
							label="Auto Run" 
							// checked={!autoRun} 
							defaultChecked={autoRun} 
							// disabled={!ideOpen} 
							onChange={e => setAutoRun(e.target.checked)}
						/>
					
						{/* disabled={autoRun}  */}
						{!autoRun && <Btn onClick={() => setCodeNoAuto(codeAuto)} size="sm" kind="primary">RUN</Btn>}
					</React.Fragment>
				}
			</Flex>
			
			<div style={{height:300}} className="position-relative" 
				// className={Cx("position-relative", `bg-${themes}`)} 
			>
				<SplitPane
					split={pos} // "horizontal" // vertical
					minSize={50}
					maxSize={1000} // 300
					// defaultSize={100}
					// defaultSize="100%" // 50%
					size={ideOpen ? "50%" : "100%"} 
					allowResize={ideOpen}
					className={Cx("border position-absolute", themes)} // , `text-${themes === 'dark' ? 'white':'dark'}` | vh-100
					// style={{height:300}} | (codeAuto.length > 0 && codeNoAuto.length > 0)
					pane2Class={`ovhide bg-${themes}`} // Cx("position-relative", `bg-${themes}`)
				>
					<div className="h-100 bg-white">
						{(codeAuto && codeNoAuto) && 
							<Compiler 
								code={autoRun ? codeAuto : codeNoAuto} 
								scope={{Btn}} 
								setError={(e) => {
									// window.Babel
									// setErr(e);
									if(e){
										let rep = e.replace('/file.tsx: ', '');
										setErr(rep);
									}else{
										setErr(null);
									}
								}}
								// preview={err !== null ? ErrBabel : null} 
								className="liveCoding"  
							/>
						}
					</div>

					{ideOpen && 
						<SplitPane 
							split="horizontal" 
							// allowResize={err !== null} 
							size={err !== null ? "50%" : "100%"} 
							// pane1Class="ovhide" 
							pane2Class="bg-white ovhide"
						>
							<div className="w-100 live-app">
								{/* <textarea onChange={e => setCodeAuto(e.target.value)} value={codeAuto} className="form-control form-control-sm" rows="9" /> */}
								<ControlledEditor
									// height="calc(100% - 31px)" // calc(100vh - 75px) 
									// className={Cx({'d-none': tools !== 'Compiler'})} 
									editorDidMount={onDidChangeEditor} 
									value={codeAuto} 
									onChange={(e, v) => {
										setCodeAuto(v);
										// console.log('%cControlledEditor value: ', LOG_DEV, v);
									}} 
									// language={compilerLang} // ideLang | 'javascript'
									theme={themes} 
									options={{
										fontSize: 14, // fontSize
										tabSize: 2, // tabSize
										// dragAndDrop: false,
										minimap: {
											enabled: false, // minimapEnabled, // Defaults = true
											// side: minimapSide, // Default = right | left
										},
										// mouseWheelZoom: zoomIde
									}}
								/>
							</div>
							
							{err && <pre className="alert alert-danger mb-0 border-0 rounded-0 h-100 q-scroll">{err}</pre>} 
						</SplitPane>
					}
				</SplitPane>
			</div>
		</Flex>
	);
}

LiveCode.defaultProps = {
	theme: 'light', // dark
	code: '',
	auto: false,
	onRender: noop
};

export default React.memo(LiveCode, (prevProps, nextProps) => prevProps.code === nextProps.code);

/*
</React.Fragment>
*/
