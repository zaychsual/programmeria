import React, { useState } from 'react';
import { ErrorBoundary } from 'react-error-boundary';
import { Route } from 'react-router-dom';

import Btn from './q-ui-react/Btn';

function ErrorFallback({ error, resetErrorBoundary }){
  return (
    <div role="alert" 
      className="alert alert-danger mb-0 rounded-0 pre-wrap ovauto" // ff-inherit fs-inherit
    >&#9888; Something went wrong. 
      <p>{navigator.onLine ? error.message : "Your internet connection is offline."}</p>
      <Btn onClick={resetErrorBoundary}>Try again</Btn>
    </div>
  )
}

export default function RouteLazy({
  path, 
  children, 
  ...etc
}){
  const [explode, setExplode] = useState(false);

  // return path ? <Route {...etc} path={path} /> : children;
  // console.log('explode: ', explode);
  return (
    <ErrorBoundary
      FallbackComponent={ErrorFallback} 
      onError={(err, info) => {
        // setExplode(err);// e => !e
        console.log('onError Component err: ', err);
        console.log('onError Component info: ', info);
      }} 
      onReset={() => setExplode(false)} 
      resetKeys={[explode]}
    >
      {!explode && (path ? <Route {...etc} path={path} /> : children)}
    </ErrorBoundary>
  )
}

// export default class RouteLazy extends React.Component{
//   constructor(props){
//     super(props);
//     this.state = {
//       isError: false
//     };
//   }

//   static getDerivedStateFromError(){// err
//     // Update state so the next render will show the fallback UI.
//     // console.log('getDerivedStateFromError in RouteLazy err: ',err);
//     return { isError: true };
//   }

//   componentDidCatch(err, errInfo){
//     // You can also log the error to an error reporting service
//     // logErrorToMyService(error, errorInfo);
//     console.log('componentDidCatch in RouteLazy err: ', err);
//     console.log('componentDidCatch in RouteLazy errInfo: ', errInfo);

// 		// fetch('https://jsonplaceholder.typicode.com/posts', {
// 		// 	method: 'POST',
// 		// 	body: JSON.stringify({
// 		// 		title: 'foo',
// 		// 		body: 'bar',
// 		// 		userId: 1
// 		// 	}),
// 		// 	headers: {
// 		// 		"Content-type": "application/json; charset=UTF-8"
// 		// 	}
// 		// })
// 		// .then(response => response.json())
// 		// .then(json => console.log(json));

//     //  let sb = navigator.sendBeacon(
//     //    'https://reqres.in/api/users',
//     //    JSON.stringify({
// 		// 		name: 'Husein',
// 		// 		job: 'Programmer'
// 		// 	})
//     //  );
//     //  console.log('sb: ', sb);
//   }

//   render(){
//     const { errorMsg, path, children, ...etc } = this.props;

//     if(this.state.isError){
//       // You can render any custom fallback UI
//       // return errorMsg; // <h1>Something went wrong.</h1>
//       if(errorMsg) return errorMsg;

//       return (
//         <div role="alert" 
//           className="alert alert-danger mb-0 rounded-0 pre-wrap ovauto" // ff-inherit fs-inherit
//         >&#9888;{` Something went wrong. 

// ${navigator.onLine ? 'Server error.' : 'Your internet connection is offline.'}`}</div>
//       );
//     }

//     // return this.props.children;
//     return path ? <Route {...etc} path={path} /> : children;
//   }
// }

