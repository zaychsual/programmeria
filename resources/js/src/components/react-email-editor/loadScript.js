const scriptUrl = "//editor.unlayer.com/embed.js?2"; // 
const callbacks = [];
let loaded = false;

const isScriptInjected = () => {
  const scripts = Q.domQall('script');// document.querySelectorAll
  let injected = false;

  scripts.forEach((script) => {
    if(script.src.includes(scriptUrl)){
      injected = true;
    }
  });

  return injected;
};

const addCallback = (callback) => {
  callbacks.push(callback);
};

const runCallbacks = () => {
  if (loaded) {
    let callback;

    while ((callback = callbacks.shift())){
      callback();
    }
  }
};

export const loadScript = (callback) => {
  addCallback(callback);

  if (!isScriptInjected()) {
    const script = Q.makeEl('script');
    script.async = 1;
    // script.crossOrigin = "anonymous";
    script.src = scriptUrl;
    // script.setAttribute('src', scriptUrl);
    script.onload = () => {
      loaded = true;
      runCallbacks();
    };
    document.head.appendChild(script);
  } else {
    runCallbacks();
  }
};
