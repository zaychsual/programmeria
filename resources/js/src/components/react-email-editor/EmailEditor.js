import React, { Component } from 'react';

import { loadScript } from './loadScript';
// import pkg from './package.json';
import isEqual from '../../utils/react-fast-compare';

let lastEditorId = 0;

export default class EmailEditor extends Component {
  constructor(props){
    super(props);
    this.state = {
      isUpdate: false
    };
    this.editorId = "qEmailEditor-" + (++lastEditorId); 
  }

  componentDidMount(){
    loadScript(this.loadEditor);
  }

  // shouldComponentUpdate(nextProps){ // , nextState
  //   return !isEqual(this.props, nextProps);
  // }

  componentDidUpdate(prevProps){ // prevState, snapshot
    if(!isEqual(this.props, prevProps)){ // this.props.userID !== prevProps.userID 
      this.setState({ isUpdate: true }, () => {
        this.setState({ isUpdate: false }, () => this.loadEditor());
      });
    }
  }

  loadEditor = () => {
    // const options = this.props.options || {};
    const { projectId, tools, appearance, locale, onLoad } = this.props;

    // if (this.props.projectId) {
    //   options.projectId = this.props.projectId;
    // }

    // if (this.props.tools) {
    //   options.tools = this.props.tools;
    // }

    // if (this.props.appearance) {
    //   options.appearance = this.props.appearance;
    // }

    // if (this.props.locale) {
    //   options.locale = this.props.locale;
    // }
    
    this.editor = unlayer.createEditor({
      // ...options, 
      projectId, 
      tools, 
      appearance, 
      locale, 
      id: this.editorId,
      displayMode: "email",
      source: {
        name: "react-email-editor", // pkg.name,
        version: "1.2.0" // pkg.version,
      },
    });

    // All properties starting with on[Name] are registered as event listeners.
    for(const [key, value] of Object.entries(this.props)){
      if(/^on/.test(key) && key !== 'onLoad'){
        this.addEventListener(key, value);
      }
    }

    // const { onLoad } = this.props;
    if(onLoad) onLoad();
  }

  registerCallback = (type, callback) => {
    this.editor.registerCallback(type, callback);
  }

  addEventListener = (type, callback) => {
    this.editor.addEventListener(type, callback);
  }

  loadDesign = (design) => {
    this.editor.loadDesign(design);
  }

  saveDesign = (callback) => {
    this.editor.saveDesign(callback);
  }

  exportHtml = (callback) => {
    this.editor.exportHtml(callback);
  }

  setMergeTags = (mergeTags) => {
    this.editor.setMergeTags(mergeTags);
  }

  render() {
    // let {
    //   props: { 
    //     minHeight = 500, 
    //     style = {} 
    //   },
    // } = this;

    const { minHeight, style, className } = this.props;
    const { isUpdate } = this.state;

    if(isUpdate){
      return null;
    }

    return (
      <div 
        className={Q.Cx("q-email-editor", className)}
        style={{
          // flex: 1, 
          // display: 'flex', 
          minHeight: minHeight
        }}
      >
        <div 
          id={this.editorId} 
          style={style} 
          className="flex1"
          // style={{ ...style, flex: 1 }} 
        />
      </div>
    );
  }
}

EmailEditor.defaultProps = {
  options: {}, 
  minHeight: 500, 
  style: {}, 
};
