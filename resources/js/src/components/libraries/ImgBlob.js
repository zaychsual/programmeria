import React, { Suspense } from 'react'; // useState, useEffect, 
import { lazy } from '@loadable/component';
// import ImageBlobReduce from 'image-blob-reduce';

import SpinLoader from '../q-ui-react/SpinLoader';

const Lib = lazy.lib(() => import(/* webpackChunkName: "ImageBlobReduce" */ "image-blob-reduce"));

export default function ImgBlob({ children, load }){
  if(load){
    return (
      <Suspense fallback={<SpinLoader bg="/icon/android-icon-36x36.png" />}>
        <Lib>
          {(fn) => {
            if(fn && children){
              return children(fn);
            }
            return null;
          }}
        </Lib>
      </Suspense>
    )
  }

  return null;
}

