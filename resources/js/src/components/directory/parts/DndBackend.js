import React, { Suspense } from 'react';
import { lazy } from '@loadable/component';// loadable

// import Flex from '../../q-ui-react/Flex';
import SpinLoader from '../../q-ui-react/SpinLoader';
// import { isMobile } from '../../../utils/Q';

// import { HTML5 } from './HTML5';
// import { Touch } from './Touch';

// Q.UA.platform.type === "mobile" || screen.width <= 480

const HTML5 = lazy.lib(() => import(/* webpackChunkName: "HTML5Backend" */'react-dnd-html5-backend'));// loadable.lib
const Touch = lazy.lib(() => import(/* webpackChunkName: "TouchBackend" */'react-dnd-touch-backend'));

export default function DndBackend({ children }){
  const IS_TOUCH = 'ontouchstart' in window || (navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0);

  return (
    <Suspense 
      fallback={
        <SpinLoader bg="/icon/android-icon-36x36.png" />
      }
    >
      {["mobile","tablet"].includes(Q_appData.UA.platform.type) && IS_TOUCH ? 
        <Touch>{({ TouchBackend }) => children(TouchBackend)}</Touch> 
        : 
        <HTML5>
          {({ HTML5Backend }) => children(HTML5Backend)}
        </HTML5>
      }
    </Suspense>
  )
}

/* 
<Flex justify="center" align="center" className="h-100 cwait">
  <div className="spinner-border text-primary" style={{ width: '3rem', height: '3rem' }} role="status" />
</Flex>

fallback={date.toLocaleDateString()} 
*/