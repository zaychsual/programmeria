import React, { useState } from 'react';// { useMemo, useEffect, useRef, useContext, useLayoutEffect, useCallback }
import { useDrop } from 'react-dnd';// useDrag, 
import Collapse from 'react-bootstrap/Collapse';
import Dropdown from 'react-bootstrap/Dropdown';

import DirItem from './DirItem';
import { treeType } from './utils';

export default function Directory({
  // inputFileId, 
  data, 
  className, 
	type = treeType.DIR, 
	title, 
	id, 
	children, 
	open = false, 
	timeout = 50, // 90 | 150 
	mountOnEnter = true, 

  labelClass, 
  copy, 
  cut, //  = false
  rename, 
	onDrop = Q.noop, 
	// onToggle = Q.noop, 
	onCut = Q.noop, 
	onCopy = Q.noop, 
  onPaste = Q.noop, 
  // onCtrlPaste = Q.noop, 
  onClickRename = Q.noop, 
  onRename = Q.noop, 
  onBlurRename = Q.noop, 
  // onKeyDownRename = Q.noop, 
  onSaveRename = Q.noop, 
  onDelete = Q.noop, 
  onNewFile = Q.noop, 
  onNewDirectory = Q.noop, 
  onClickUpload = Q.noop, 
	...etc
}){
  const [see, setSee] = useState(open);

  const MENUS = [
    {t:"New file", i:"file", fn: onNewFile},
    {t:"New directory", i:"folder", fn: onNewDirectory},
    {t:"Upload files", i:"upload", fn: onClickUpload} // clickCtxMenuUploadFiles
  ];

	const [{ isOver, isOverCurrent }, drop] = useDrop({
		accept: [treeType.DIR, treeType.FILE],
		drop(item){ // , monitor
			// const didDrop = monitor.didDrop();
			// console.log('drop didDrop: ', didDrop);

			// handleDrop({
			// 	...item, 
			// 	...data
			// });

			// if(didDrop){
			// 	return;
			// }
			if(!see) setSee(true);

			onDrop(item, data);

			return undefined;
		},
		// hover(){
		// 	setSee(true);
		// },
		collect: (monitor) => ({
			isOver: monitor.isOver(), 
			isOverCurrent: monitor.isOver({ shallow: true }),
		}),
	});

	// const handleDrop = useCallback((dropData) => {
	// 	console.log('handleDrop dropData: ', dropData);
	// 	onDrop(dropData, data);
	// }, []);

	const toggle = (e) => {
		setSee(!see);
		// onToggle(!see, e);
  }

  // const onPasteData = (e) => {
  //   // if(!see) setSee(true);

  //   const label = Q.domQ(`[aria-controls="${id}"]`);
  //   console.log("onPasteData label:", label);

  //   if(label){
  //     console.log('onPasteData label: ', label);
  //     label.click();
  //     label.blur();
  //   }
    
  //   setTimeout(() => {
  //     onPaste(e);
  //   }, 300);
  // }
  
  // const clickCtxMenuUploadFiles = () => {
	// 	console.log("clickCtxMenuUploadFiles");
	// 	let fileInput = Q.domQ("#" + inputFileId);
	// 	if(fileInput){
	// 		fileInput.click();
	// 	}
	// }

	return (
    <div 
      ref={drop} 
      role="tree" 
			className={
        // Q.Cx("detail-q tree-q tree-folder w-100", { "bg-light": (isOver && isOverCurrent) && type !== "root" }, className)
				Q.Cx("detail-q tree-q tree-folder w-100", { 
          "bg-light": isOver && isOverCurrent
        }, className)
			} 
    >
      <DirItem 
				type={type} 
				icon="folder" 
				title={title} 
        ariaControls={id} 
        expanded={see} // type !== "root" ? see : null
        // isDrop={(isOver && isOverCurrent) && type !== "root" ? "bg-strip" : undefined}
				isDrop={(isOver && isOverCurrent) ? "bg-strip" : undefined} 
				data={data} 
				// appendCtxMenu={(hide, unFocus) => 
				// 	<>
				// 		{MENUS.map(v => 
        //       <button key={v.t} 
        //         onClick={() => {
        //           hide();
        //           unFocus();
        //           v.fn();
        //         }} 
        //         className={"dropdown-item qi q-fw qi-" + v.i} type="button">{v.t}</button>	
        //       )
				// 		}
				// 	</>
				// } 
        labelClass={labelClass} 
        copy={copy} 
        cut={cut} 
        rename={rename} 
        onClick={e => toggle(e)} 
        // onDoubleClick={e => toggle(e)} // OPTION
				onCut={onCut} 
				onCopy={onCopy} 
        onPaste={onPaste} // onPasteData 
        // onCtrlPaste={onCtrlPaste} // DEVS
        onClickRename={onClickRename} 
        onRename={onRename} 
        onBlurRename={onBlurRename} 
        // onKeyDownRename={onKeyDownRename} 
        onSaveRename={onSaveRename} 
				onDelete={onDelete} // (data, e) => onDelete(data, e)
			>
				{/* <Dropdown.Item as="label" className="btnFile qi qi-upload" tabIndex="-1">
					Upload files
					<input type="file" tabIndex="-1" hidden id={inputFileId} />
				</Dropdown.Item> */}

        {MENUS.map((v, i) => 
          <Dropdown.Item key={i} as="button" type="button" onClick={v.fn} className={"qi qi-" + v.i} tabIndex="-1">{v.t}</Dropdown.Item>
        )}
			</DirItem>

			<Collapse 
				{...etc} 
				mountOnEnter={mountOnEnter} 
				timeout={timeout} 
				in={see} 
			>
			  {children && 
          <div className="tree-content" id={id} aria-hidden={!see}>
            {children}
          </div>
        }
			</Collapse>
		</div>
  );
}



