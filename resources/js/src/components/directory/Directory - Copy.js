import React from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

// import DndBackend from './parts/DndBackend';
import Details from '../q-ui-react/Details';
import Btn from '../q-ui-react/Btn';

function TreeItem({
	className, 
	children, 
	txt, 
	title, 
	icon, 
	onRename = Q.noop, 
	onDelete = Q.noop,  
	As = "div", // summary
}){
	return (
		<As className={Q.Cx("input-group input-group-xs tree-item", className)}>
			<div 
				className={
					Q.Cx("form-control btn btn-flat text-left", { ["i-color fa-" + icon]: icon }) // fab
				}  
				title={title ? title : txt}
			>
				{txt}
			</div>
			
			<div className="input-group-append tree-tool">
				{children}
				<Btn onClick={onRename} blur kind="flat" className="rounded far fa-pen" title="Rename" tabIndex="-1" />
				<Btn onClick={onDelete} blur kind="flat" className="rounded far fa-times xx" title="Delete" tabIndex="-1" />
			</div>
		</As>
	)
}

export default function Directory({
	open = false, // 
	renderOpen = false, // 
}){
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Directory','color:yellow;');
	// }, []);

	return (
		<Details 
			open={open} 
			renderOpen={renderOpen} 
			className="tree-q tree-folder" 
			onToggle={(open, e) => {
				console.log('onToggle open: ', open);
				console.log('onToggle e: ', e);
			}}
		>
			<TreeItem className="far fa-fw fa-folder" 
				As="summary" 
				txt="public folder long title text"
			>
				<Btn As="label" kind="flat" className="rounded btnFile far fa-upload" title="Upload files" tabIndex="-1">
					<input type="file" hidden />
				</Btn>
				<Btn blur kind="flat" className="rounded far fa-file" title="New file" tabIndex="-1" />
				<Btn blur kind="flat" className="rounded far fa-folder" title="New directory" tabIndex="-1" />
			</TreeItem>

			<div className="detail-content">
				<TreeItem className="tree-file" 
					icon="html5 fab" 
					txt="index.html"
				/>

				<TreeItem className="tree-file" 
					icon="js fab" 
					txt="App.js"
				/>
			</div>

			{/* <div className="input-group input-group-xs tree-item">
				<div className="form-control btn btn-flat text-left fab i-color fa-js" title="App.js">App.js</div>
				<div className="input-group-append tree-tool">
					<Btn kind="flat" className="rounded far fa-pen" title="Rename" tabIndex="-1" />
					<Btn kind="flat" className="rounded far fa-times xx" title="Delete" tabIndex="-1" />
				</div>
			</div> */}
		</Details>
	);
}

/*
		<DndBackend>
			{(backend) => {
				// console.log('backend: ',backend);
				return (

				)
			}}
		</DndBackend>
*/
