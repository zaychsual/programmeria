import React from 'react';
// import P from 'prop-types';
// import stylePropType from 'react-style-proptype';
// import {Cx} from '../../utils/Q';

export default class Pane extends React.PureComponent{
  render(){
    const {children, className, split, style: styleProps, size, eleRef} = this.props;
    // const classes = ['Pane', split, className];

    let style = {
      flex: 1
      // position: 'relative',
      // outline: 'none'
    };

    if(size !== undefined){
      if(split === 'vertical'){
        style.width = size;
      }else{
        style.height = size;
        style.display = 'flex';
      }
      style.flex = 'none';
    }

    style = Object.assign({}, style, styleProps || {});

		// classes.join(' ')
    return (
      <div ref={eleRef} className={Q.Cx('Pane', split, className)} style={style}>
        {children}
      </div>
    );
  }
}

/* Pane.propTypes = {
  className: P.string.isRequired,
  children: P.node.isRequired,
  size: P.oneOfType([P.string, P.number]),
  split: P.oneOf(['vertical', 'horizontal']),
  // style: stylePropType,
  eleRef: P.func
}; */

// Pane.defaultProps = {};

// export default Pane;
