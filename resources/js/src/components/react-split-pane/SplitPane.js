import React from 'react';
// import P from 'prop-types';
// import stylePropType from 'react-style-proptype';
// import { polyfill } from 'react-lifecycles-compat';
// import {bindFuncs, Cx, toggleClass, isFunc} from '../../utils/Q';

import Pane from './Pane';
import Resizer, { RESIZER_DEFAULT_CLASSNAME } from './Resizer';

function unFocus(document, window){
  if(document.selection){
    document.selection.empty();
  }else{
    try{
      window.getSelection().removeAllRanges();
      // eslint-disable-next-line no-empty
    }catch(e){}
  }
}

function getDefaultSize(defaultSize, minSize, maxSize, draggedSize){
  if(typeof draggedSize === 'number'){
    const min = typeof minSize === 'number' ? minSize : 0;
    const max = typeof maxSize === 'number' && maxSize >= 0 ? maxSize : Infinity;
    return Math.max(min, Math.min(max, draggedSize));
  }
  if(defaultSize !== undefined){
    return defaultSize;
  }
  return minSize;
}

function removeNullChildren(children){
  return React.Children.toArray(children).filter(c => c);
}

export default class SplitPane extends React.Component{
  constructor(props){
    super(props);
    // this.onMouseDown = this.onMouseDown.bind(this);
    // this.onTouchStart = this.onTouchStart.bind(this);
    // this.onMouseMove = this.onMouseMove.bind(this);
    // this.onTouchMove = this.onTouchMove.bind(this);
    // this.onMouseUp = this.onMouseUp.bind(this);
		Q.bindFuncs.call(this,['onMouseDown','onTouchStart','onMouseMove','onTouchMove','onMouseUp']);

    // order of setting panel sizes.
    // 1. size
    // 2. getDefaultSize(defaultSize, minsize, maxSize)
    const { size, defaultSize, minSize, maxSize, primary } = props;
    const initialSize = size !== undefined ? size : getDefaultSize(defaultSize, minSize, maxSize, null);

    this.state = {
      active: false,
      resized: false,
      pane1Size: primary === 'first' ? initialSize : undefined,
      pane2Size: primary === 'second' ? initialSize : undefined,
      // these are props that are needed in static functions. ie: gDSFP
      instanceProps: {
        size
      }
    };
  }

  componentDidMount(){
    // document.addEventListener('mouseup', this.onMouseUp);
    // document.addEventListener('mousemove', this.onMouseMove);
    // document.addEventListener('touchmove', this.onTouchMove);
		this.events().forEach(v => document.addEventListener(v.e, v.f));
    this.setState(SplitPane.getSizeUpdate(this.props, this.state));
  }

  static getDerivedStateFromProps(nextProps, prevState){
    return SplitPane.getSizeUpdate(nextProps, prevState);
  }

  componentWillUnmount(){
    // document.removeEventListener('mouseup', this.onMouseUp);
    // document.removeEventListener('mousemove', this.onMouseMove);
    // document.removeEventListener('touchmove', this.onTouchMove);
		this.events().forEach(v => document.removeEventListener(v.e, v.f));
  }

	events = () => {
		return [
			{e:'mouseup', f:this.onMouseUp},
			{e:'mousemove', f:this.onMouseMove},
			{e:'touchmove', f:this.onTouchMove}
		];
	}	
	
// Q-CUSTOM - toggle className for backdrop (iframe issue)
	setCursor(cursor, s){
		if(cursor) document.documentElement.classList.toggle((s === 'vertical' ? 'col':'row') + '-resize'); // toggleClass(document.documentElement, (s === 'vertical' ? 'col':'row') + '-resize');
	}	
	
  onMouseDown(e){
		if(e.button !== 2){ // Q-CUSTOM: FOR prevent if right mouse (klik kanan, iframe issue)
			const eventWithTouches = Object.assign({}, e, {
				touches: [{ clientX: e.clientX, clientY: e.clientY }]
			});
			this.onTouchStart(eventWithTouches);
		}
  }

  onTouchStart(e){
    const { allowResize, onDragStarted, split, setCursor } = this.props;
    if(allowResize){
      unFocus(document, window);
      const position = split === 'vertical' ? e.touches[0].clientX : e.touches[0].clientY;

      if(Q.isFunc(onDragStarted)){ // typeof onDragStarted === 'function'
        onDragStarted();
      }
			
			this.setCursor(setCursor, split);// Q-CUSTOM (iframe issue)
			
      this.setState({
        active: true,
        position
      });
    }
  }

  onMouseMove(e){
    const eventWithTouches = Object.assign({}, e, {
      touches: [{ clientX: e.clientX, clientY: e.clientY }]
    });
    this.onTouchMove(eventWithTouches);
  }

  onTouchMove(e){
    const { allowResize, maxSize, minSize, onChange, split, step } = this.props;
    const { active, position } = this.state;

    if(allowResize && active){
      unFocus(document, window);
      const isPrimaryFirst = this.props.primary === 'first';
      const ref = isPrimaryFirst ? this.pane1 : this.pane2;
      const ref2 = isPrimaryFirst ? this.pane2 : this.pane1;
			
      if(ref){
        const node = ref;
        const node2 = ref2;

        if(node.getBoundingClientRect){
          const width = node.getBoundingClientRect().width;
          const height = node.getBoundingClientRect().height;
          const current = split === 'vertical' ? e.touches[0].clientX : e.touches[0].clientY;
          const size = split === 'vertical' ? width : height;
          let positionDelta = position - current;
					
          if(step){
            if(Math.abs(positionDelta) < step){
              return;
            }
            // Integer division
            // eslint-disable-next-line no-bitwise
            positionDelta = ~~(positionDelta / step) * step;
          }
          let sizeDelta = isPrimaryFirst ? positionDelta : -positionDelta;

          const pane1Order = parseInt(window.getComputedStyle(node).order);
          const pane2Order = parseInt(window.getComputedStyle(node2).order);
					
          if(pane1Order > pane2Order){
            sizeDelta = -sizeDelta;
          }

          let newMaxSize = maxSize;
          if(maxSize !== undefined && maxSize <= 0){
            const splitPane = this.splitPane;
            if(split === 'vertical'){
              newMaxSize = splitPane.getBoundingClientRect().width + maxSize;
            }else{
              newMaxSize = splitPane.getBoundingClientRect().height + maxSize;
            }
          }

          let newSize = size - sizeDelta;
          const newPosition = position - positionDelta;

          if(newSize < minSize){
            newSize = minSize;
          }else if(maxSize !== undefined && newSize > newMaxSize){
            newSize = newMaxSize;
          }else{
            this.setState({
              position: newPosition,
              resized: true
            });
          }

          if(onChange) onChange(newSize);

          this.setState({
            draggedSize: newSize,
            [isPrimaryFirst ? 'pane1Size' : 'pane2Size']: newSize
          });
        }
      }
    }
  }

  onMouseUp(){
		// Q-CUSTOM = setCursor
    const { allowResize, onDragFinished, split, setCursor } = this.props;
    const { active, draggedSize } = this.state;
    if(allowResize && active){
      if(Q.isFunc(onDragFinished)){// typeof onDragFinished === 'function'
        onDragFinished(draggedSize);
      }
      this.setState({ active: false });
			this.setCursor(setCursor, split);// Q-CUSTOM (iframe issue)
    }
  }

// we have to check values since gDSFP is called on every render and more in StrictMode
  static getSizeUpdate(props, state){
    const newState = {};
    const { instanceProps } = state;

    if(instanceProps.size === props.size && props.size !== undefined){
      return {};
    }

    const newSize = props.size !== undefined ? 
			props.size 
			: 
			getDefaultSize(
				props.defaultSize,
				props.minSize,
				props.maxSize,
				state.draggedSize
			);

    if(props.size !== undefined){
      newState.draggedSize = newSize;
    }

    const isPanel1Primary = props.primary === 'first';

    newState[isPanel1Primary ? 'pane1Size' : 'pane2Size'] = newSize;
    newState[isPanel1Primary ? 'pane2Size' : 'pane1Size'] = undefined;

    newState.instanceProps = { size: props.size };

    return newState;
  }

  render(){
    const {
      allowResize,
      children,
      className,
      onResizerClick,
      onResizerDoubleClick,
      paneClass,
      pane1Class,
      pane2Class,
      paneStyle,
      pane1Style: pane1StyleProps,
      pane2Style: pane2StyleProps,
      resizerClass, // resizerClassName = ORI
      resizerStyle,
			resizerProps, // Q-CUSTOM
      split,
      style: styleProps, 
			parentProps
    } = this.props;

    const { pane1Size, pane2Size } = this.state;

    const disabledClass = allowResize ? '' : 'disabled';
		// resizerClassNamesIncludingDefault = ORI
    const resizerClassWithDefault = resizerClass ? resizerClass +' '+ RESIZER_DEFAULT_CLASSNAME : resizerClass;
    const notNullChildren = removeNullChildren(children);

    const style = {
      // display: 'flex',
      // flex: 1,
      // height: '100%',
      // position: 'absolute',
      // outline: 'none',
      // overflow: 'hidden',
      // MozUserSelect: 'text',
      // WebkitUserSelect: 'text',
      // msUserSelect: 'text',
      // userSelect: 'text',
      ...styleProps
    };

    // if(split === 'vertical'){
      // Object.assign(style, {
        // flexDirection: 'row',
        // left: 0,
        // right: 0
      // });
    // }else{
      // Object.assign(style, {
        // bottom: 0,
        // flexDirection: 'column',
        // minHeight: '100%',
        // top: 0,
        // width: '100%'
      // });
    // }

    // const classes = ['SplitPane', className, split, disabledClass];

    const pane1Style = { ...paneStyle, ...pane1StyleProps };
    const pane2Style = { ...paneStyle, ...pane2StyleProps };

    // const pane1Classes = ['Pane1', paneClass, pane1Class].join(' ');
    // const pane2Classes = ['Pane2', paneClass, pane2Class].join(' ');

    return (
      <div 
				{...parentProps} 
				ref={n => this.splitPane = n} 
				// className={Q.Cx('SplitPane', split, disabledClass, className)}
        className={Q.Cx('SplitPane', {'isDisabled': !allowResize}, split, className)} // classes.join(' ')
        style={style} 
      >
        <Pane 
					key="pane1" 
          className={Q.Cx('Pane1', paneClass, pane1Class)} // pane1Classes
          eleRef={n => this.pane1 = n}
          size={pane1Size}
          split={split}
          style={pane1Style}
        >
          {notNullChildren[0]}
        </Pane>
				
        <Resizer {...resizerProps}// Q-CUSTOM
					key="resizer" 
					tabIndex={allowResize ? "-1":null} // "-1" 
          className={disabledClass} 
          onClick={onResizerClick} 
          onDoubleClick={onResizerDoubleClick} 
          onMouseDown={this.onMouseDown} 
          onTouchStart={this.onTouchStart} 
          onTouchEnd={this.onMouseUp} 
          resizerClass={resizerClassWithDefault} 
          split={split} 
          style={resizerStyle || {}} 
        />
				
        <Pane 
					key="pane2" 
          className={Q.Cx('Pane2', paneClass, pane2Class)} // pane2Classes
          eleRef={n => this.pane2 = n} 
          size={pane2Size} 
          split={split} 
          style={pane2Style}
        >
          {notNullChildren[1]}
        </Pane>
      </div>
    );
  }
}

SplitPane.defaultProps = {
  allowResize: true,
  minSize: 50,
  primary: 'first',
  split: 'vertical',
  // paneClass: '',
  // pane1Class: '',
  // pane2Class: '',
	setCursor: true // Q-CUSTOM For set drop cursor (iframe issue)
};

/* SplitPane.propTypes = {
  allowResize: P.bool,
  children: P.arrayOf(P.node).isRequired,
  className: P.string,
  primary: P.oneOf(['first', 'second']),
  minSize: P.oneOfType([P.string, P.number]),
  maxSize: P.oneOfType([P.string, P.number]),
  // eslint-disable-next-line react/no-unused-prop-types
  defaultSize: P.oneOfType([P.string, P.number]),
  size: P.oneOfType([P.string, P.number]),
  split: P.oneOf(['vertical', 'horizontal']),
  onDragStarted: P.func,
  onDragFinished: P.func,
  onChange: P.func,
  onResizerClick: P.func,
  onResizerDoubleClick: P.func,
  // style: stylePropType,
  // resizerStyle: stylePropType,
  paneClass: P.string,
  pane1Class: P.string,
  pane2Class: P.string,
  // paneStyle: stylePropType,
  // pane1Style: stylePropType,
  // pane2Style: stylePropType,
  resizerClass: P.string,
  step: P.number
}; */

// polyfill(SplitPane);

// export default SplitPane;
