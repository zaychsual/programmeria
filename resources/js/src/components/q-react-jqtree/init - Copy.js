import { confirm } from '../react-confirm/util/confirm';// , confirmComplex
import { fileRead } from '../../utils/file/fileRead';

// bootstrap-4.css
// sweetalert2.min.js

const CONFIRM_OPTION = {
  type: "toast",
  // title: "Title",
  // toastInfoText: "11 min ago",
  // icon: <i className="fab fa-react mr-2" />, 
  size: "sm",  
  bodyClass: "text-center", 
  btnsWrapClass: "d-flex w-50 mt-3 mx-auto flex1-all ml-1-next", 
  // backdrop: false, 
  cancelLabel: false, 
  modalProps: {
    centered: true, 
    returnFocusAfterClose: false
  }
};

// const onConfirm = async () => {
//   if(await confirm(<h6>Are your sure?</h6>, CONFIRM_OPTION)){
//     console.log('YES');
//   }else{
//     console.log('NO');
//   }
// }

const jqtreeInit = (treeEl, wrapEl, data, acceptExt) => {
  const tree = $(treeEl);
  // const wrap = $(wrapEl);
  let isMove;
  
  function editInput(node){
    let input = $(`<input value="${node.name}" class="form-control form-control-sm d-inline-block flex-grow-1" type="text" spellcheck="false">`);// w-auto 
    input.on("click",function(e){
      e.stopPropagation();
    });
    return input;
  }
  
  let dir = tree.tree({
    data: data, 
    slide: false, 
    openedIcon: $('<i class="qi qi-folder-open" />'), 
    closedIcon: $('<i class="qi qi-folder" />'), 
    showEmptyFolder: true,  
    autoOpen: 0, // bool = true / index num position = 0 is open folder in root
    useContextMenu: false, 
    dragAndDrop: true, 
    onCreateLi(node, li){
      // console.log('onCreateLi node:', node);
      // li.attr("data-path", node.path);
      let { id, name, type, content } = node;
      let isFile = type === "file";
      
      li.attr({
        title: `Name: ${name}
Type: ${type}
${isFile ? "Size: " + Q.bytes2Size(Q.strByte(content)) + "" : ""}`,
        "data-id": id
      });
      
      let el = li.find('.jqtree-element');
      let jqtitle = li.find(".jqtree-title");
      let treeTools = $('<div class="tree-tools" />');
      let btnCx = "btn btn-light btn-sm qi qi-";
      
      jqtitle.addClass("d-inline-flex align-items-center");
      
      if(isFile){
        jqtitle.addClass("qi qi-" + name.split(".").pop());
      }else{
        [
          '<button class="'+ btnCx +'folder tree-add-folder" data-id="'+ id +'" title="Add Folder" type="button" />', 
          '<button class="'+ btnCx +'file tree-add-file" data-id="'+ id +'" title="Add File" type="button" />'
        ].forEach(v => treeTools.append(v));
      }

      [
        '<button class="'+ btnCx +'edit tree-edit" data-id="'+ id +'" title="Edit" type="button" />', 
        '<button class="'+ btnCx +'close tree-del" data-id="'+ id +'" title="Remove" type="button" />'
      ].forEach(v => treeTools.append(v));
      
      el.append(treeTools);
    }, 
    onDragStop(node, e){
      console.log('onDragStop node: ', node);
      // console.log('onDragStop e: ', e);
      if(isMove){
        // let nodeMove = dir.tree('getNodeById', node.id);
        // console.log('onDragStop nodeMove: ', nodeMove);
        let { name, id } = node;// parent
        dir.tree("updateNode", node, {
          name,
          id,
          // path: parent.path + "/" + name
        });
        isMove = false;
      }
    }, 
    /*onCanMove(node){
      console.log('onCanMove node: ', node);
      if(!node.parent.parent){
        // Example: Cannot move root node
        return false;
      }
      else {
        return true;
      }
    }*/
    /* onCanMoveTo(moved_node, target_node, position){
      console.log('moved_node:', moved_node);
      console.log('target_node:', target_node);
      console.log('position:', position);
    }*/
  })
  /*.on('tree.init',function(e){
    console.log('init:', e);
  })
  .on('tree.refresh',function(e){
    console.log('refresh:', e);
  })*/
  .on('tree.click',function(e){
    // The clicked node is 'event.node'
    let node = e.node;
    // let nodeById = dir.tree('getNodeById', node.id);
    
    // console.log('dir:', dir);
    // console.log('node: ', node);
    // console.log('e.click_event.ctrlKey:', e.click_event.ctrlKey);
    // console.log('nodeById:', nodeById);
    
    let isSelected = dir.tree('isNodeSelected', node);
    // $(node.element).children(".jqtree-element").find(".jqtree-title").trigger("blur");
    
    if(e.click_event.ctrlKey){
      e.preventDefault();// Disable single selection
      if(node.id === undefined){
        console.warn('The multiple selection functions require that nodes have an id');
        return;
      }
      if(isSelected){
        dir.tree('removeFromSelection', node);
      }else{
        dir.tree('addToSelection', node);
      }
    }
    else{
      // e.preventDefault();// Disable single selection
      // dir.tree('selectNode', node);
      // dir.tree('addToSelection', node);
      if(isSelected){
        // e.preventDefault();
        $(node.element).children(".jqtree-element").find(".tree-edit").trigger("click");
      }
      else{
        console.log('click node: ', node);
      }
      
      /*let level = node.getLevel();// .parents();
      let p = Array.from({ length: level }).map((_, i) => i);
      p.forEach((f, i) => {
        console.log($(node.element).parents('li.jqtree-folder'));// [data-id]
      });*/
      
      // let p = $(node.element).parents('li.jqtree-folder');
      // let itemPath = node.name;
      // let paths = "";
      // if(p.length > 0){
        // /*p.each(function(i){
          // console.log('i: ', i)
          // console.log($(this).data("name"));
          // paths +=  "/" + $(this).data("name");
        // });*/
        
        // let count = 0;
        // p.sort();
      // }
      
      // console.log("p: ", p);
      // console.log("itemPath: ", itemPath);
      // console.log("paths: ", paths);
      
      /*let p = $(node.element).parentsUntil('[data-id]');// ="presentation"
      console.log("p: ", p);*/
    }
    
    /*if(e.click_event.ctrlKey){
      let nodeSelect = dir.tree('getNodeById', node.id);
      dir.tree('addToSelection', nodeSelect);
    }*/
    
    // let del = node.element.find('.jqtree-element');
  })
  .on('tree.move',function(e){
    let type = e.move_info.target_node.type;
    // console.log('moved_node', e.move_info.moved_node);
    // console.log('target_node', e.move_info.target_node);
    // console.log('position', e.move_info.position);
    // console.log('previous_parent', e.move_info.previous_parent);
    
    isMove = true;// e.move_info.target_node
    
    if(type === "file"){
      e.preventDefault();
      isMove = false;
    }
  });
  
  // tree.on('click','.dropdown-toggle',function(e){
    // e.stopPropagation();
  // });
  
  tree.on('click','.tree-tools',function(e){
    e.stopPropagation();
  });
  
  tree.on('click','.tree-add-folder',function(e){
    e.stopPropagation();
    let id = $(e.target).data('id');// Get the id from the 'node-id' data property
    let node = dir.tree('getNodeById', id);// Get the node from the tree
    console.log('node: ', node);
    
    if(node){
      // MUST Validate duplicate name
      let fnames = node.children.filter((f) => f.name.startsWith("New_Folder"));
      let id = Q.Qid();
      // console.log('fnames: ', fnames);
      
      dir.tree("appendNode", {
        id, 
        type: "directory", 
        name: "New_Folder" + (fnames.length > 0 ? "_" + (fnames.length + 1) : ""), 
        children: [], 
        isEmptyFolder: true
      }, node);
      
      setTimeout(() => $(node.element).children("ul").find('.tree-edit[data-id="'+ id +'"]').trigger("click"), 9);
    }
  });
  
  tree.on('click','.tree-add-file',function(e){
    e.stopPropagation();
    let id = $(e.target).data('id');// Get the id from the 'node-id' data property
    let node = dir.tree('getNodeById', id);// Get the node from the tree
    // console.log('node: ', node);
    
    if(node){
      // MUST Validate duplicate name
      let fnames = node.children.filter((f) => f.name.startsWith("Untitled") && f.name.endsWith(".txt"));
      let id = Q.Qid();
      
      dir.tree("appendNode", {
        id, 
        type: "file", 
        name: "Untitled" + (fnames.length > 0 ? "_" + (fnames.length + 1) : "") + ".txt", 
        content: "" 
      }, node);
      
      setTimeout(() => $(node.element).children("ul").find('.tree-edit[data-id="'+ id +'"]').trigger("click"), 9);
    }
  });
  
  tree.on('click','.tree-edit',function(e){
    e.stopPropagation();
    let id = $(e.target).data('id');// Get the id from the 'node-id' data property
    let node = dir.tree('getNodeById', id);// Get the node from the tree
    
    if(node){
      let { element, name, type } = node;// parent, id
      // console.log("node: ", node);
      
      let nodeTitle = $(element).find(".jqtree-title").eq(0);// .first()
      let input = nodeTitle.find("input");
      //let isKey = false;
      
      // console.log("edit parent: ", parent);
      function removeInput(){
        input.remove();
        nodeTitle.text(name);
        nodeTitle.removeClass("editing");
      }
      function editFn(e){ // async 
        //console.log('editFn e: ', e);
        e.stopPropagation();
        let val = e.target.value.trim();
        let txtArr = val.split(".");
        let isFolder = type === "directory";
        // let swalOps = {
        //   icon: "warning", 
        //   allowEnterKey: false
        // };
        // console.log('blur input val: ', val);
        
        if(val.length < 1){ // Empty string / only contain space
          removeInput();
          // Swal.fire({
          //   title: "", 
          //   text: "Name should not be empty.", 
          //   icon: "warning", 
          //   allowEnterKey: false, 
          // });
          confirm(<h6>Name should not be empty.</h6>, CONFIRM_OPTION);
          return;
        }
        else if(!isFolder && txtArr.length < 2){ // Item is file & dont have extension
          removeInput();
          confirm(<h6>File must have extension.</h6>, CONFIRM_OPTION);
          return;
        }
        else if(!isFolder && !acceptExt.includes([...txtArr].pop())){ // Item is file & extension not valid
          removeInput();
          confirm(<><h6>File extension not valid.</h6>Only support extensions :<br/>{acceptExt.join(", ")}</>, CONFIRM_OPTION);
          return;
        }
        else if(val !== name){
          // console.warn('blur input update node: ', val);
          // NOTE: Recursive...!!!
          // type !== "file"
          // let children = node.children?.length > 0 ? node.children.map(v => ({ id: v.id, name: v.name, type: v.type, path: parent.path + "/" + val + "/" + v.name })) : null;
          // recursiveEdit(data, parent, valName = "", key = "children")
          // let children = node.children?.length > 0 ? recursiveEdit(node.children, parent, val) : [];
          let fnames = node.parent.children.filter((f) => f.name === val);
          // console.log('fnames: ', fnames);
          if(fnames.length > 0){
            confirm(<h6>{type} name is available, Please change.</h6>, CONFIRM_OPTION);
            return;
          }
          
          if(isFolder){
            dir.tree("updateNode", node, {
              id, 
              name: val, 
              // path: parent.path ? parent.path + "/" + val : "/" + val, 
              // children: node.children?.length > 0 ? recursiveEdit(node.children, parent, val) : []
            });
            removeInput();
          }else{
            // let fnames = node.children.filter((f) => f.name.startsWith("Untitled") && f.name.endsWith(".txt"));
            let extOld = name.split(".").pop();
            let extNew = [...txtArr].pop();
            
            if(extOld !== extNew){
              const more = { ...CONFIRM_OPTION, cancelLabel: "No", okLabel: "Yes" };
              const isOk = confirm(<h6>Are you sure to change extension? <div>File {name} to {extNew}</div></h6>, more);
              if(isOk){
                dir.tree("updateNode", node, {
                  id, 
                  name: val
                });
                removeInput();
              }else{
                removeInput();
              }
              return;

              // Swal.fire({
              //   ...swalOps, 
              //   title: "Are you sure to change extension?", 
              //   text: "File " + name + " to " + extNew, 
              //   showCancelButton: true, 
              //   confirmButtonText: "Yes", 
              //   cancelButtonText: "No"
              // }).then((v) => {
              //   if(v.isConfirmed){
              //     dir.tree("updateNode", node, {
              //       id, 
              //       name: val
              //     });
              //     removeInput();
              //   }else{
              //     removeInput();
              //   }
              // });
            }else{
              dir.tree("updateNode", node, {
                id, 
                name: val
              });
              removeInput();
            }
          }
        }
        else{
          removeInput();
        }
      }
      
      let isSelected = dir.tree('isNodeSelected', node);
      if(!isSelected){
        dir.tree('selectNode', node);
      }
      
      if(input.length){
        input.select();
        // dir[0].scrollTo(100, 0);
      }else{
        input = editInput(node).on("focus",function(ee){
          $(this).parent().addClass("editing");
        }).on("blur",function(ee){
          editFn(ee);
          // $(this).parent().removeClass("editing");
        }).on("keydown",function(ee){
          if(ee.key === "Enter"){
            editFn(ee);
          }
        });
        
        nodeTitle.html(input);
        setTimeout(() => {
          input.select();
          // dir[0].scrollTo(100, 0);
        }, 9);// ;// .trigger("focus");
      }
    }
  });
  
  tree.on('click','.tree-del', async function(e){
    e.stopPropagation();
    let ID = $(e.target).data('id');// Get the id from the 'node-id' data property
    let node = dir.tree('getNodeById', ID);// Get the node from the tree

    if(node){
      let isSelected = dir.tree('isNodeSelected', node);
      if(!isSelected){
        dir.tree('selectNode', node);
      }
      // console.log("Delete: ", node);
      let { type, name } = node;

      if(await confirm(<><h6>Are you sure to delete {type} {name}?</h6><br/>You won't be able to revert this!</>, CONFIRM_OPTION)){
        dir.tree("removeNode", node);
      }
    }
  });
  
  /* tree.on('tree.dblclick',function(e){
    // event.node is the clicked node
    console.log(e.node);
  }); */
  
  // tree.on('tree.contextmenu',function(e){
  //   //e.preventDefault();
  //   e.stopPropagation();
  //   let { node } = e;
  //   console.log('contextmenu e: ', e);
  //   /* if(dir.tree('isNodeSelected', node)){
  //     dir.tree('removeFromSelection', node);
  //   }else{
  //     dir.tree('addToSelection', node);
  //   } */
  
  //   dir.tree('selectNode', node);
  // });
  
  /*tree.on('focusout',function(e){
    let nodes = dir.tree('getSelectedNodes');
    console.log("blur nodes: ", nodes);
    nodes.forEach(n => dir.tree('removeFromSelection', n));// n.element.classList.contains("jqtree-selected")
  });*/
  
  $(wrapEl + " .btn-add-root-folder").on("click",function(){
    let node = dir.tree('getTree');// dir.tree('getNodeById', id);// Get the node from the tree
    // console.log('node: ', node);
    
    if(dir && node){
      // MUST Validate duplicate name
      let fnames = node.children.filter((f) => f.name.startsWith("New_Folder"));
      let id = Q.Qid();
      
      dir.tree("appendNode", {
        id, 
        type: "directory", 
        name: "New_Folder" + (fnames.length > 0 ? "_" + (fnames.length + 1) : ""), 
        children: [], 
        isEmptyFolder: true
      }, node);
      
      setTimeout(() => $(node.element).children("ul").find('.tree-edit[data-id="'+ id +'"]').trigger("click"), 9);
    }
  });
  
  $(wrapEl + " .btn-add-root-file").on("click",function(){
    let node = dir.tree('getTree');// dir.tree('getNodeById', id);// Get the node from the tree
    console.log('node: ', node);
    
    if(dir && node){
      // MUST Validate duplicate name
      let fnames = node.children.filter((f) => f.name.startsWith("Untitled") && f.name.endsWith(".txt"));
      let id = Q.Qid();
      
      dir.tree("appendNode", {
        id, 
        type: "file", 
        name: "Untitled" + (fnames.length > 0 ? "_" + (fnames.length + 1) : "") + ".txt", 
        content: "" 
      }, node);
      
      setTimeout(() => $(node.element).children("ul").find('.tree-edit[data-id="'+ id +'"]').trigger("click"), 9);
    }
  });
  
  $(wrapEl + " .btn-upload-root-file").on("change",function(e){
    let files = e.target.files;
    // console.log('files: ', files);
    
    if(files && files.length > 0){
      let node = dir.tree('getTree');
      
      for(let i = 0; i < files.length; i++){
        let file = files[i];
        fileRead(file, { readAs: file.type.startsWith("text/") || file.type.includes("/svg") ? "Text" : "DataURL" }).then(data => {
          console.log('data: ', data);
          
          if(dir && node){
            let fnames = node.children.filter((f) => f.name === file.name);
            let id = Q.Qid();
            let fname = file.name.split(".").shift();
            let ext = file.name.split(".").pop();
            
            dir.tree("appendNode", {
              id, 
              type: "file", 
              name: (fnames.length > 0 ? fname + "_" + (fnames.length + 1) : fname) + "." + ext,  
              content: data.result 
            }, node);
          }
        }).catch(e => console.log(e));
      }
    }
  });
}

export { jqtreeInit };