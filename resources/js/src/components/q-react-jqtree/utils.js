// 
function regExFromStr(rs, b){
	return new RegExp(rs, b);// if rs undefined = /(?:)/
}

function noExt(s = ""){
	let a = s.split(".");
	a.pop();
	return a.join(".");
}

export { 
	regExFromStr, 
	noExt
};