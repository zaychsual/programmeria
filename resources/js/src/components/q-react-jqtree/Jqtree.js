import React, { useRef, useState, useEffect } from 'react';// , { useState, useEffect, useRef, } 

import Btn from '../q-ui-react/Btn';
// import { Beforeunload } from '../q-ui-react/Beforeunload';
// import { confirm } from '../react-confirm/util/confirm';// , confirmComplex
import ContextMenu from '../q-ui-react/ContextMenu';
import { jqtreeInit } from './init';
import { getExt } from '../../utils/file/getSetExt';
import { noExt } from './utils';// regExFromStr
import { recursiveSortDir } from '../../utils/collection-q';// sortDirBy
// import { fileReaderApi } from '../../utils/file/fileReaderApi';
// import { fileRead } from '../../utils/file/fileRead';

// const sortByDir = (datas) => { // sortByFile
//   if(Array.isArray(datas)){
//     return datas.sort((a, b) => {
//       //let nameA = a.name.toUpperCase(); // ignore upper and lowercase
//       //let nameB = b.name.toUpperCase(); // ignore upper and lowercase
//       if(a.type === "directory"){ // nameA < nameB
//         return -1;
//       }
//       if(b.type === "file"){
//         return 1;
//       }
//       return 0;// names must be equal
//     });
//     // console.log('res: ', res);
//     // return res;
//   }
// }

const TREE_ID = "tree_" + Q.Qid(3);
let loadLibs;//  = []
let browserNativefs;

export default function Jqtree({
  className, 
  treeClass, 
  treeStyle, 
  treeHeight, 
  tools = true, 
  acceptExt = ["txt"], 
  textFiles = ["js", "json", "php", "scss", "sass"], // OPTION to readAs FileReader API 
  defaultExt = "txt", 
  regExNodeName = /[\s/[/\]<>+=()&^%$#@!~`:"',{}/\\|?*\u0000-\u001F]/g, 
  onInit = Q.noop, 
  onClickItem = Q.noop, 
  // onAddItem, 
  // jqtree options:
  data, 
  selectable = true, 
  slide = false, 
  useContextMenu = false, 
  dragAndDrop = true, 
  openedIcon = "q-fw qi qi-folder-open", 
  closedIcon = "q-fw qi qi-folder", 
  autoOpen = false, 
  onRefresh = Q.noop, 
}){
  const CTXMENU_ADD = [ 
    "Add Folder", 
    "Add File" // { label:"Add File", node }, // fn: (cb) => { cb(node) }
  ];

  const refTree = useRef(null);
  const refJqtree = useRef(null);
	const [load, setLoad] = useState(false);
  const [ctxMenuItems, setCtxMenuItems] = useState();// []
  const [dataCtxMenu, setDataCtxMenu] = useState();
  const [cutData, setCutData] = useState();
  const [copyData, setCopyData] = useState();

  const onClickEditItem = (node) => { // node, e
    if(node){
      const el = node.element;
      const isCut = Q.hasClass(el, "o-05");
      const isCopy = Q.hasClass(el, "copyMe");

      if(isCut){
        Q.setClass(el, "o-05", "remove");
        setCutData(null);
      }
      if(isCopy){
        Q.setClass(el, "copyMe", "remove");
        setCopyData(null);
      }

      if(isCut || isCopy) setDataCtxMenu(null);
    }
  }

  const getTreeData = () => {
    if(refJqtree.current){
      return refJqtree.current.tree("getTree").getData(false);
    }
  }

	useEffect(() => {
		// console.log('%cuseEffect in Jqtree','color:yellow;');
    const runTree = () => {
			setLoad(true);
      setTimeout(() => {
        refJqtree.current = jqtreeInit("#" + TREE_ID, "#wrap_" + TREE_ID, {
          acceptExt, 
          textFiles, 
          defaultExt, 
          regExNodeName, 
          data: recursiveSortDir(data), // 
          // data, 
          selectable, 
          slide, 
          useContextMenu, 
          dragAndDrop, 
          openedIcon, 
          closedIcon, 
          autoOpen, 
          onRefresh: (e) => {
            // console.log('onRefresh e: ', e);
            if(Q.isFunc(onRefresh)) onRefresh(getTreeData(), refJqtree.current, e);
          }, 
          onContextMenu: (node) => { // , e
            if(node.type !== "file"){
              setCtxMenuItems(CTXMENU_ADD);
            }else{
              setCtxMenuItems(null);
            }
            setDataCtxMenu(node);
          }, 
          onClickItem: (node, e) => {
            if(Q.isFunc(onClickItem)) onClickItem(node, e);
          }, 
          onClickEditItem: onClickEditItem, 
          // onAddItem: (data, node) => onAddItem(data, node)
        });

        // NOT FIX
        if(Q.isFunc(onInit) && refJqtree.current){
          onInit(getTreeData(), refJqtree.current);
        }
      }, 9);
    }

    if(!browserNativefs){
      importShim("https://cdn.jsdelivr.net/npm/browser-nativefs@0.12.0/dist/index.min.js")
      .then(m => {
        console.log(m);
        browserNativefs = m;
      }).catch(e => console.log(e));
    }

		if(loadLibs){
      runTree();
		}else{
			// const LIBS = [
			// 	{ tag:"link", rel:"stylesheet", href:"/css/libs/jqtree.css" }, // /js/libs/jqtree/jqtree.css
      //   { tag:"link", rel:"stylesheet", href:"/js/libs/sweetalert/bootstrap-4.css" }, // "/css/libs/swal2-bootstrap-4.css"
			// 	// { src:"/js/libs/jquery-3.5.1.min.js", "data-js": "jQuery" }, 
			// 	// { src:"/js/libs/sweetalert/sweetalert2.min.js", "data-js": "Swal" }
			// ];
	
			// LIBS.forEach((v, i) => {
			// 	Q.getScript(v, "head").then(() => {
      //     // console.log('OK: ', LIBS.length === i + 1);
      //     // console.log('window.jQuery: ', window.jQuery);
			// 		if(LIBS.length === (i + 1)){ // i === 3
      //       if(window.$ || window.jQuery){
      //         Q.getScript({ src:"/js/libs/jqtree/tree.jquery.js", "data-js": "jqtree" }).then(() => {
      //           // if(window.$ || window.jQuery){
      //             loadLibs = 1;
      //             runTree();
      //           // }
      //         }).catch(e => {
      //           console.log(e);
      //         });
      //       }
			// 		}
			// 	}).catch(e => {
			// 		console.log(e);
			// 	});
			// });

      Q.getScript({ tag:"link", rel:"stylesheet", href:"/css/libs/jqtree.css" }, "head").then(() => {
        // console.log('window.jQuery: ', window.jQuery);
        if(window.$ || window.jQuery){
          Q.getScript({ src:"/js/libs/jqtree/tree.jquery.js", "data-js": "jqtree" }).then(() => {
            loadLibs = 1;
            runTree();
          }).catch(e => {
            console.log(e);
          });
        }
      }).catch(e => {
        console.log(e);
      });
		}

    // OPTION to destroy jqtree
    return () => {
      if(refJqtree.current) refJqtree.current.tree("destroy");
    }
	}, [data, acceptExt]);

  const onAddFolderOrFile = (label, hide) => {
    if(dataCtxMenu){
      const btn = Q.domQ(".jqtree-element .tree-add-" + (label === "Add File" ? "file" : "folder"), dataCtxMenu.element);
      // console.log('btn: ', btn);
      if(btn){
        btn.click();
        hide();
        setDataCtxMenu(null);
      }
    }
  }

  const onEditOrRemove = (label, hide) => {
    if(dataCtxMenu){
      const btn = Q.domQ(".jqtree-element .tree-" + label, dataCtxMenu.element);
      if(btn){
        btn.click();
        hide();
        setDataCtxMenu(null);
      }
    }
  }

  const onCut = (hide) => {
    // console.log('onCut dataCtxMenu: ', dataCtxMenu);
    if(dataCtxMenu){
      const el = dataCtxMenu.element;
      Q.setClass(el, "o-05");// point-no
      if(copyData){
        Q.setClass(el, "copyMe", "remove");
        setCopyData(null);
      }
      
      setCutData(dataCtxMenu);
      // setDataCtxMenu(null);// OPTION
    }
    hide();
  }

  const onCopy = (hide) => {
    // console.log('onCopy dataCtxMenu: ', dataCtxMenu);
    // console.log('onCopy cutData: ', cutData);
    if(dataCtxMenu){
      Q.setClass(dataCtxMenu.element, "copyMe");
      if(cutData){
        const elCut = cutData.element;
        Q.setClass(elCut, "o-05", "remove");
        setCutData(null);
      }

      setCopyData(dataCtxMenu);
      // setDataCtxMenu(null);// OPTION
    }
    hide();
  }

  const onPaste = (hide) => {
    if((copyData || cutData) && dataCtxMenu){
      // console.log('onPaste cutData: ', cutData);
      // console.log('onPaste dataCtxMenu: ', dataCtxMenu);

      const dir = refJqtree.current;
      const id = "ti_" + Q.Qid(3);
      const pasteData = cutData || copyData;
      const { type, name, content, children } = pasteData;
      const targetChild = dataCtxMenu.children;

      const addItem = () => {
        dir.tree("appendNode", {
          id, 
          type, 
          name, 
          content, 
          children, 
        }, dataCtxMenu);
      }

      if(targetChild.length > 0){
        const ext = "." + getExt(name);
        const fname = noExt(name);// name.replace(regExFromStr(ext, "i"), "");
        const hasName = targetChild.filter(f => f.name.startsWith(fname) && f.name.endsWith(ext));// f.name === name
        // console.log('onPaste hasName: ', hasName);
        
        if(hasName && hasName.length > 0){
          Swal.fire({
            icon: "warning", 
            title: type + " name is available.", 
            // text: "Please change", 
            text: "Change name or cancel", // type + " name will auto changed."
            showCancelButton: true, 
            allowEnterKey: false, 
            confirmButtonText: "Yes", 
            cancelButtonText: "No"
          }).then((v) => {
            if(v.isConfirmed){
              addItem();
              const newNode = dir.tree("getNodeById", id);
              setTimeout(() => {
                if(cutData){
                  dir.tree("removeNode", pasteData);
                }
                // $(newNode.element).children(".jqtree-element").find(".tree-edit").trigger("click");
                dir.tree("updateNode", newNode, fname + "_" + (hasName.length + 1) + ext);
                dir.tree("selectNode", newNode);
              }, 9);
            }
            else{
              if(cutData){
                Q.setClass(cutData.element, "o-05", "remove");
                setCutData(null);
              }
              if(copyData){
                Q.setClass(copyData.element, "copyMe", "remove");
                setCopyData(null);
              }
            }
          });

          hide();
          return;
        }else{
          addItem();
        }
      }
      else{
        addItem();
      }

      if(cutData){
        dir.tree("removeNode", cutData);
        setCutData(null);
      }
      if(copyData){
        setCopyData(null);
      }

      if(!dataCtxMenu.is_open){
        dir.tree("openNode", dataCtxMenu, false);
      }

      setTimeout(() => {
        const newNode = dir.tree("getNodeById", id);
        dir.tree("selectNode", newNode);
        setDataCtxMenu(null);// OPTION
      }, 9);
    }
    hide();
  }

  const onCtxMenu = (active, e) => {
    if(dataCtxMenu){
      if(active && e){
        const et = e.target;
        if(et && !Q.hasClass(et, "jqtree-element")){
          setDataCtxMenu(null);
        }
      }
    }
  }

  const onUploadDir = async () => { // hide
    const { directoryOpen } = browserNativefs;

    // hide();
    try {
      // const blobs = await directoryOpen({recursive: true});
      // const blobs = await directoryOpen();
      const dir = refJqtree.current;
      const node = dir.tree("getTree");// dir.tree('getNodeById', id);// Get the node from the tree

      // let datas = [];
      if(dir && node){ // MUST Validate duplicate name
        const blobs = await directoryOpen();// { recursive: true }
        blobs.sort((a, b) => {
          a = a.name;// a.webkitRelativePath + a.name
          b = b.name;
          if (a < b) {
            return -1;
          } else if (a > b) {
            return 1;
          }
          return 0;
        }).forEach((blob) => {
          // The Native File System API currently reports the `webkitRelativePath`
          // as empty string `''`.
          // fileStructure += `${blob.webkitRelativePath}${
          //     blob.webkitRelativePath.endsWith(blob.name) ?
          //     '' : blob.name}\n`;

          // datas.push({ 
          //   content: blob, 
          //   type:"file", 
          //   name: blob.name, // blob.webkitRelativePath.endsWith(blob.name) ? "" : blob.name, 
          //   // path: "/", // blob.webkitRelativePath, //  + "/", 
          //   size: Q.bytes2Size(blob.size), // , " "
          //   // created_at: blob.lastModifiedDate, 
          //   // ext: blob.name.split(".").pop() 
          // });
          
          // console.log('blob: ', blob);
          const { name, size } = blob;
          const ext = "." + getExt(name);
          const fname = noExt(name);// name.replace(regExFromStr(ext, "i"), ""); // name.replace(new RegExp(ext, "g"), "");
          const fnames = node.children.filter((f) => f.name.startsWith(fname) && f.name.endsWith(ext));
          const id = "ti_" + Q.Qid();
          
          dir.tree("appendNode", {
            id, 
            type: "file", 
            size: Q.bytes2Size(size), 
            name: fname + (fnames.length > 0 ? "_" + (fnames.length + 1) : "") + ext, 
            content: blob
          }, node);
          
          setTimeout(() => {
            const newNode = dir.tree("getNodeById", id);
            dir.tree("addToSelection", newNode, false);
          }, 9);
          
          // fileReaderApi(blob, { 
          //   readAs: blob.type.startsWith("text/") || blob.type.includes("/svg") || (textFiles && textFiles.includes(ext)) ? "Text" : "DataURL" 
          // })
          // .then(data => {
          //   // console.log('data: ', data);
          //   // const fnames = node.children.filter((f) => f.name === file.name);
          //   // const fname = file.name.split(".").shift();

          //   const fname = name.replace(new RegExp("." + ext, "g"), "");
          //   const fnames = node.children.filter((f) => f.name.startsWith(fname) && f.name.endsWith("." + ext));
          //   const id = "ti_" + Q.Qid();
            
          //   dir.tree("appendNode", {
          //     id, 
          //     type: "file", 
          //     size: Q.bytes2Size(size), 
          //     name: fname + (fnames.length > 0 ? "_" + (fnames.length + 1) : "") + "." + ext, 
          //     content: data.result 
          //   }, node);
            
          //   setTimeout(() => {
          //     const newNode = dir.tree("getNodeById", id);
          //     dir.tree("addToSelection", newNode, false);
          //   }, 9);
          // }).catch(e => console.log(e));
        });
      }

      // console.log('datas: ', datas);
      // setAppState({ data: [ ...dataDirs, ...datas ] });
    } catch (err) {
      if (err.name !== 'AbortError') {
        console.error(err);
      }
    }
  }

	if(!load) return null;// Before load libs

	return (
    <div 
      className={className} 
      id={"wrap_" + TREE_ID} 
    >
      {tools && 
        <div className="btn-group w-100" role="group">
          {[
            { cx:"btn-add-root-folder", icon:"qi qi-folder", title:"Add Folder" }, 
            { cx:"btnFile btn-upload-root-folder", icon:"qi qi-upload", title:"Upload Folder", fn: () => onUploadDir() }, // hide
            { cx:"btn-add-root-file", icon:"qi qi-file", title:"Add File" }, 
            { cx:"btnFile btn-upload-root-file", icon:"qi qi-upload", title:"Upload File", fn: Q.noop }
          ].map((v) => 
            v.cx.startsWith("btnFile") ? 
            <Btn key={v.cx} As="label" outline size="sm" kind="secondary" 
              className={Q.Cx(v.cx, v.icon, v.className)}
              title={v.title} 
              onClick={() => v.fn()} 
            >
              {v.cx.endsWith("btn-upload-root-file") ? 
                <input type="file" accept={"." + acceptExt.join(",.")} multiple hidden />
                : 
                "" // <input type="file" webkitdirectory="" directory="" hidden />
              }
            </Btn>
            :
            <Btn key={v.cx} blur outline size="sm" kind="secondary" 
              className={Q.Cx(v.cx, v.icon, v.className)} title={v.title} />
          )}

          {/* <Btn blur outline size="sm" kind="secondary" className="qi qi-file " title="Add File" />
          <Btn As="label" outline size="sm" kind="secondary" 
            className="btnFile qi qi-upload btn-upload-root-file" 
            title=""
          >
          </Btn> */}
        </div>
      }

      <ContextMenu 
        className="zi-1021" 
        appendTo={refTree.current || document.body} // isFullscreen ? this.wrap.current : document.body
        esc={false} // Default = true
        // hideOnScroll={false} // Default = true
        popperConfig={{
          strategy: "fixed"
        }} 
        // , ctxMenu | _, active
        component={(hide) => {
          // if(!view && active){
          //   hide();
          //   // return null;
          // }
          if(!dataCtxMenu ){
            hide();
            return null;
          }

          return (
            <div className="dropdown-menu show v-dd-sets w-auto mnw-auto" 
              onContextMenu={(e) => {
                Q.preventQ(e);
                setDataCtxMenu(null);
                hide();
              }} 
            >
              {ctxMenuItems && 
                ctxMenuItems.map((v, i) => 
                  <button key={i} className="dropdown-item" type="button"
                    onClick={() => onAddFolderOrFile(v, hide)} // v.node, v.label, hide
                  >{v}</button>
                )
              }

              <button onClick={() => onCut(hide)} className="dropdown-item" type="button">Cut</button>
              <button onClick={() => onCopy(hide)} className="dropdown-item" type="button">Copy</button>

              {(copyData || cutData) && (dataCtxMenu && dataCtxMenu.type === "directory") && 
                <button className="dropdown-item" type="button" 
                  onClick={() => onPaste(hide)}
                >
                  Paste
                </button>
              }
              
              <button className="dropdown-item" type="button"
                onClick={() => onEditOrRemove("edit", hide)} 
              >Edit</button>
              <button className="dropdown-item" type="button"
                onClick={() => onEditOrRemove("del", hide)} 
              >Remove</button>
            </div>
          )
        }}
        onContextMenu={onCtxMenu}
      >
        <div ref={refTree}>
          <div 
            // ref={refTree} 
            id={TREE_ID} 
            tabIndex={0} 
            className={Q.Cx("py-1 q-jqtree", treeClass)} 
            style={{ ...treeStyle, height: treeHeight }}
          />
        </div>
      </ContextMenu>
    </div>
	);
}

/*

*/
