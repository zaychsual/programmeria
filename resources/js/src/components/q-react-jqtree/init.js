// import { confirm } from '../react-confirm/util/confirm';// , confirmComplex
// import { fileRead } from '../../utils/file/fileRead';
import { fileReaderApi } from '../../utils/file/fileReaderApi';
import { getExt } from '../../utils/file/getSetExt';
import { noExt } from './utils';// regExFromStr
// import { MONACO_LANGS } from '../../data/monaco';
// import { semverRegex } from '../../utils/string';// validFilename

// const CONFIRM_OPTION = {
//   type: "toast",
//   // title: "Title",
//   // toastInfoText: "11 min ago",
//   // icon: <i className="fab fa-react mr-2" />, 
//   size: "sm",  
//   bodyClass: "text-center", 
//   btnsWrapClass: "d-flex w-50 mt-3 mx-auto flex1-all ml-1-next", 
//   // backdrop: false, 
//   cancelLabel: false, 
//   modalProps: {
//     centered: true, 
//     returnFocusAfterClose: false
//   }
// };

const jqtreeInit = (treeEl, wrapEl, { 
  acceptExt, 
  textFiles, // OPTION to readAs FileReader API
  defaultExt, // For add new file, set extension
  regExNodeName, 
  onClickEditItem, 
  onClickItem, // = Q.noop, 
  onContextMenu = Q.noop, 
  // onAddItem, 
  // jqtree options:
  data, 
  onRefresh, // = Q.noop, 
  selectable, 
  autoOpen, 
  slide, 
  useContextMenu, 
  dragAndDrop, 
  openedIcon, 
  closedIcon
} = {}) => {
  const tree = $(treeEl);
  let isMove;
  
  function editInput(name){// w-auto 
    return $('<input value="'+ name +'" class="form-control form-control-sm d-inline-block border-0 rounded-0 shadow-blue flex1" type="text" spellcheck="false">').on("click",function(e){
      e.stopPropagation();
    });
  }
  function addBtn(c, id, t){
    return '<button class="btn btn-flat btn-xs qi qi-'+ c +'" data-id="'+ id +'" title="'+ t +'" type="button" tabindex="-1"/>'
  }
  
  const dir = tree.tree({
    data, 
    slide, 
    openedIcon: $('<i class="'+ openedIcon +'"/>'), 
    closedIcon: $('<i class="'+ closedIcon +'"/>'), 
    showEmptyFolder: true, 
    selectable, 
    autoOpen, // bool = true / index num position = 0 is open folder in root
    useContextMenu, 
    dragAndDrop, 
    onCreateLi(node, li){
      // li.attr("data-path", node.path);
      let { id, name, type, content, size } = node;
      let isFile = type === "file";
      // console.log('onCreateLi size:', size);
      
      li.attr({
        title: `Name: ${name}
Type: ${type}
${isFile && content ? "Size: " + (size ? size : Q.bytes2Size(Q.strByte(content)) + "") : ""}`, 
        "data-id": id
      });
      
      let el = li.find(".jqtree-element");
      let jqtitle = li.find(".jqtree-title");
      // let treeTools = $('<div class="tree-tools" />');
      // jqtitle.addClass("d-inline-flex align-items-center");
      
      if(isFile){
        // jqtitle.addClass("qi qi-" + name.split(".").pop());
        const x = getExt(name);
        const ext = x && acceptExt.includes(x) ? x : "txt";// getExt(name, true)
        // console.log('ext: ', ext);
        jqtitle.addClass("i-color q-mr q-fw qi qi-" + ext);
      }else{
        if(node.children.length < 1){
          el.addClass("empty-dir");
          jqtitle.addClass("q-mr q-fw qi qi-folder");// empty-folder 
        }

        [ 
          addBtn("folder tree-add-folder", id, "Add Folder"), 
          addBtn("file tree-add-file", id, "Add File") 
        ].forEach(v => el.append(v));// treeTools
      }

      [ 
        addBtn("edit tree-edit", id, "Edit"), 
        addBtn("close xx tree-del", id, "Remove") 
      ].forEach(v => el.append(v));// treeTools
      // el.append(treeTools);
    }, 
    onDragStop(node){ // , e
      // console.log('onDragStop node: ', node);
      // console.log('onDragStop isMove: ', isMove);
      if(isMove){
        const { name, type, id, children, content } = node;
        const ext = "." + getExt(name);
        const fname = noExt(name);// name.replace(regExFromStr(ext, "i"), "");// , "g"
        // f.name === name | f.name.startsWith(fname) && f.name.endsWith(ext)
        const fnames = node.parent.children.filter((f) => f.name.startsWith(fname) && f.name.endsWith(ext));// 
        // console.log('onDragStop fnames: ', fnames);
        
        if(fnames && fnames.length > 1){
          Swal.fire({
            icon: "warning", 
            title: type + " name is available.", 
            text: "Change name or cancel", // type + " name will auto changed."
            showCancelButton: true, 
            allowEnterKey: false, 
            confirmButtonText: "Change", // Yes
            cancelButtonText: "Cancel" // No
          }).then((v) => {
            if(v.isConfirmed){
              // $(node.element).children(".jqtree-element").find(".tree-edit").trigger("click");
              dir.tree("updateNode", node, fname + "_" + (fnames.length) + ext);
            }
            else{
              dir.tree("removeNode", node);
              dir.tree("appendNode", { id, type, name, children, content }, isMove);
            }
            isMove = false;
          });
        }else{
          isMove = false;
        }
        // dir.tree("updateNode", node, {
        //   name,
        //   id,
        //   // path: parent.path + "/" + name
        // });
        // isMove = false;
      }
    }, 
    /*onCanMove(node){
      console.log('onCanMove node: ', node);
      if(!node.parent.parent){
        // Example: Cannot move root node
        return false;
      }
      else {
        return true;
      }
    }*/
    /* onCanMoveTo(moved_node, target_node, position){
      console.log('moved_node:', moved_node);
      console.log('target_node:', target_node);
      console.log('position:', position);
    }*/
  })
  // .on('tree.init',function(e){
  //   console.log('tree.init: ', e);
  // })
  .on("tree.refresh", function(e){
    onRefresh(e);
  })
  .on("tree.move", function(e){
    // console.log('tree.move', e);
    // console.log('moved_node', e.move_info.moved_node);
    // console.log('position', e.move_info.position);
    // console.log('previous_parent', e.move_info.previous_parent);
    const { target_node, previous_parent } = e.move_info;
    if(target_node.type === "file"){
      e.preventDefault();
      isMove = false;
    }
    else{
      isMove = previous_parent;// true | e.move_info.target_node
    }
  })
  // .on("tree.select", function(e){
  //   console.log('tree.select: ', e);
  // })
  .on("tree.click", function(e){
    e.preventDefault();// Disable single selection
    const node = e.node;
    const { type, id, name, content } = node;
    const isSelected = dir.tree("isNodeSelected", node);
    const isFile = type === "file";
    const ctrl = e.click_event.ctrlKey;
    // console.log('e: ', e);
    
    if(!ctrl && !isFile){
      dir.tree("toggle", node, false);
    }

    if(ctrl){
      // e.preventDefault();// Disable single selection
      if(id === undefined){
        console.warn("Multiple selection require node have an id");
        return;
      }
      
      if(isSelected) dir.tree("removeFromSelection", node);
      else dir.tree("addToSelection", node);
    }
    else{
      if(isSelected){
        const selected = dir.tree("getSelectedNodes");
        if(selected && selected.length > 1){
          selected.forEach(v => dir.tree("removeFromSelection", v));
          dir.tree("selectNode", node, { mustToggle: false });// mustSetFocus: false, 
        }
      }else{
        dir.tree("selectNode", node, { mustToggle: false });
      }
    }

    if(isFile && content){ // && textFiles.includes(ext) | (Q.isStr(content) || TEXT_EXT.includes(ext))
      const ext = getExt(name);
      // const lg = MONACO_LANGS.find(f => f.ex === setExt(ext));
      // const langCode = lg ? lg.name : "text/plain";

      if(Q.isStr(content)){
        // console.log('No FileReader');
        onClickItem(node, e);
      }
      if(Q.isStr(content.name)){
        const isBinary = "image/ audio/ video/";
        // console.log('FileReader: ', isBinary.includes(content.type));
        fileReaderApi(content, { 
          // type.startsWith("text/") || type.includes("/svg") || (textFiles && textFiles.includes(ext)) ? "Text" : "DataURL" 
          readAs: !isBinary.includes(content.type) || content.type === "" || (textFiles && textFiles.includes(ext)) ? "Text" : "DataURL" 
        })
        .then(data => {
          // console.log('data: ', data);
          dir.tree("updateNode", node, { 
            content: data.result
          });

          onClickItem(node, e);
        }).catch(e => console.log(e));
      }
    }else{
      onClickItem(node, e);
    }

    // onClickItem(node, e);

    // else{ // OPTION second click to edit
    //   // e.preventDefault();// Disable single selection
    //   // dir.tree("selectNode", node);
    //   // dir.tree("addToSelection", node);
    //   if(isFile){
    //     if(isSelected){
    //       // e.preventDefault();
    //       $(node.element).children(".jqtree-element").find(".tree-edit").trigger("click");
    //     }
    //   }
    // }
  });
  
  // tree.on('click','.dropdown-toggle',function(e){
    // e.stopPropagation();
  // });
  // tree.on('click','.tree-tools',function(e){
  //   e.stopPropagation();
  // });
  
  tree.on("click", ".tree-add-folder",function(e){
    e.stopPropagation();
    const node = dir.tree("getNodeById", e.target.dataset.id);// Get the node from the tree
    // console.log('node: ', node);
    
    if(node){ // MUST Validate duplicate name
      const n = "New_Folder";
      const fnames = node.children.filter((f) => f.name.startsWith(n));
      const id = "ti_" + Q.Qid();
      
      dir.tree("appendNode", {
        id, 
        type: "directory", 
        name: n + (fnames.length > 0 ? "_" + (fnames.length + 1) : ""), 
        children: [], 
        isEmptyFolder: false // true
      }, node);
      
      setTimeout(() => $(node.element).children("ul").find('.tree-edit[data-id="'+ id +'"]').trigger("click"), 9);
    }
  });
  
  tree.on("click", ".tree-add-file",function(e){
    e.stopPropagation();
    const node = dir.tree("getNodeById", e.target.dataset.id);// Get the node from the tree
    
    if(node){ // MUST Validate duplicate name
      const n = "Untitled";
      const fnames = node.children.filter((f) => f.name.startsWith(n) && f.name.endsWith("." + defaultExt));// ".txt"
      const id = "ti_" + Q.Qid();
      
      dir.tree("appendNode", {
        id, 
        type: "file", 
        name: n + (fnames.length > 0 ? "_" + (fnames.length + 1) : "") + "." + defaultExt, // ".txt"
        content: "" 
      }, node);
      
      setTimeout(() => $(node.element).children("ul").find('.tree-edit[data-id="'+ id +'"]').trigger("click"), 9);
    }
  });
  
  
  tree.on("click", ".tree-edit",function(e){
    e.stopPropagation();
    const id = e.target.dataset.id;// $(e.target).data('id');// Get the id from the 'node-id' data property
    const node = dir.tree("getNodeById", id);// Get the node from the tree

    if(node){
      let { element, name, type } = node;// parent, id
      let nodeTitle = $(element).find(".jqtree-title").eq(0);// .first()
      let input = nodeTitle.find("input");
      
      // console.log("edit parent: ", parent);
      function removeInput(){
        input.remove();
        nodeTitle.text(name);
        nodeTitle.removeClass("editing");
      }
      function editFn(e){
        e.stopPropagation();
        const val = e.target.value.trim();
        // let inValid;
        // if(regExNodeName){
        //   inValid = regExNodeName.test(val);
        // }
        // /[<>:"/\\|?*\u0000-\u001F]/g | /^\.\.?$/gm | 
        // const inValid = /[\s/[/\]<>+=()&^%$#@!~`:"',{}/\\|?*\u0000-\u001F]/g.test(val);// validFilename(val);
        // console.log("edit inValid: ", inValid);

        let txtArr = val.split(".");
        const isFile = type === "file";// const isFolder = type === "directory";
        const swalOps = {
          icon: "warning", 
          allowEnterKey: false
        };
        const fnames = node.parent.children.filter((f) => f.name === val);
        
        if(val.length < 1){ // Empty string / only contain space
          removeInput();
          Swal.fire({
            ...swalOps, 
            title: "Name should not be empty."
          });
          return;
        }
        else if(val.length > 0 && regExNodeName && regExNodeName.test(val)){ //  && isNotValid.length > 0
          removeInput();
          Swal.fire({
            ...swalOps, 
            // "Name can't contain any of the following characters: \\ / : * ? \" |"
            title: "Name only alphanumeric, dash & underscore."
          });
          return;
        }
        else if(isFile && txtArr.length < 2){ // Item is file & dont have extension
          removeInput();
          Swal.fire({
            ...swalOps, 
            title: "File must have extension."
          });
          return;
        }
        else if(isFile && !acceptExt.includes([...txtArr].pop())){ // Item is file & extension not valid
          // removeInput();
          Swal.fire({
            ...swalOps, 
            title: "File extension not valid.", 
            text: "Only support extensions :\n" + acceptExt.join(", "), 
            // confirmButtonText: ""
            willClose(){
              input.trigger("focus");
            }
          });
          return;
        }
        else if(val !== name){ //  || val === name
          // console.warn('blur input update node: ', val);
          // NOTE: Recursive...!!!
          // let children = node.children?.length > 0 ? node.children.map(v => ({ id: v.id, name: v.name, type: v.type, path: parent.path + "/" + val + "/" + v.name })) : null;
          // recursiveEdit(data, parent, valName = "", key = "children")
          // let children = node.children?.length > 0 ? recursiveEdit(node.children, parent, val) : [];
          // const fnames = node.parent.children.filter((f) => f.name === val);
          // console.log('fnames: ', fnames);
          if(fnames.length > 0){
            Swal.fire({
              ...swalOps, 
              title: type + " name is available.", 
              text: "Please change", 
              willClose(){
                input.trigger("focus");
              }
            });
            return;
          }
          
          if(!isFile){
            dir.tree("updateNode", node, {
              id, 
              name: val, 
              // path: parent.path ? parent.path + "/" + val : "/" + val, 
              // children: node.children?.length > 0 ? recursiveEdit(node.children, parent, val) : []
            });
            removeInput();
          }else{
            const extOld = getExt(name);// name.split(".").pop();
            const extNew = [...txtArr].pop();
            
            if(extOld !== extNew){
              Swal.fire({
                ...swalOps, 
                title: "Are you sure to change file " + name + "?", 
                text: "Extension " + extOld + " to " + extNew, 
                showCancelButton: true, 
                confirmButtonText: "Yes", 
                cancelButtonText: "No"
              }).then((v) => {
                if(v.isConfirmed){
                  dir.tree("updateNode", node, {
                    id, 
                    name: val
                  });
                  removeInput();
                }else{
                  removeInput();
                }
              });
            }else{
              dir.tree("updateNode", node, {
                id, 
                name: val
              });
              removeInput();
            }
          }
        }
        else if(val === name && fnames.length > 1){
          Swal.fire({
            ...swalOps, 
            title: type + " name is available.", 
            text: "Please change", 
            willClose(){
              input.trigger("focus");
            }
          });
        }
        else{
          removeInput();
        }
      }
      
      const isSelected = dir.tree("isNodeSelected", node);
      if(!isSelected){
        dir.tree("selectNode", node);
      }
      
      if(input.length){
        input.trigger("select");// input.select();
      }else{
        input = editInput(name).on("focus",function(){
          $(this).parent().addClass("editing");
        }).on("blur",function(e){ // ee
          editFn(e);
        }).on("keydown",function(e){
          if(e.key === "Enter"){
            editFn(e);
          }
        });
        
        nodeTitle.html(input);
        setTimeout(() => {
          input.trigger("select");// input.select();// .trigger("focus");
        }, 9);
      }

      onClickEditItem(node, e);
    }
  });
  
  tree.on("click", ".tree-del", function(e){
    e.stopPropagation();
    const node = dir.tree("getNodeById", e.target.dataset.id);// Get the node from the tree

    if(node){
      const isSelected = dir.tree("isNodeSelected", node);
      if(!isSelected){
        dir.tree("selectNode", node);
      }
      // console.log("Delete: ", node);
      const { type, name } = node;
      Swal.fire({
        title: "Are you sure?", 
        text: "To delete " + type + " " + name + ",\nYou won't be able to revert this!",
        icon: "warning",
        showCancelButton: true, 
        allowEnterKey: false, 
        confirmButtonText: "Yes", 
        cancelButtonText: "No"
      }).then((v) => {
        if(v.isConfirmed){
          dir.tree("removeNode", node);

          Swal.fire({
            icon: "success", 
            position: "top", 
            text: type + " " + name + " has been deleted.", 
            showConfirmButton: false, 
            toast: true, 
            timer: 2500
          });

          // const dataTree = dir.tree("getTree").getData(false);
          // console.log('dataTree: ', dataTree);
          // if(dataTree && dataTree.length < 1){
          //   const ul = $(wrapEl).find(treeEl + " ul");
          //   console.log('ul: ', ul);
          //   ul.addClass("empty-dirs").text("This folder is empty.");
          // }
        }
      });
    }
  });
  
  // tree.on("tree.dblclick", function(e){
  //   console.log("tree.dblclick", e.node);// event.node is the clicked node
  // });
  
  // tree.on('tree.contextmenu',function(e){
  //   //e.preventDefault();
  //   e.click_event.stopPropagation();
  //   // let { node } = e;
  //   console.log('contextmenu e: ', e);
  //   console.log('contextmenu click_event: ', e.click_event);
  //   /* if(dir.tree('isNodeSelected', node)){
  //     dir.tree('removeFromSelection', node);
  //   }else{
  //     dir.tree('addToSelection', node);
  //   } */
  
  //   // dir.tree('selectNode', node);
  // });

  tree.on("contextmenu", '.jqtree-element[role="presentation"]',function(e){ // .jqtree_common
    // e.stopPropagation();
    const node = dir.tree("getNodeById", $(this).parent().data("id"));
    // console.log('contextmenu e: ', e);
    if(node && onContextMenu){
      // if(dir.tree('isNodeSelected', node)){
      //   dir.tree('removeFromSelection', node);
      // }else{
      //   dir.tree('addToSelection', node);
      // }
      dir.tree("selectNode", node, { mustToggle: false });
      onContextMenu(node, e);
    }
  });
  
  // tree.on('focusout',function(e){ // tree | blur
  //   e.stopPropagation();
  //   let nodes = dir.tree('getSelectedNodes');
  //   console.log("blur nodes: ", nodes);
  //   // nodes.forEach(n => dir.tree('removeFromSelection', n));// n.element.classList.contains("jqtree-selected")
  //   // $(this).removeClass("wrapTreeFocus");
  // });
  
  $(wrapEl + " .btn-add-root-folder").on("click",function(){
    const node = dir.tree("getTree");// dir.tree('getNodeById', id);// Get the node from the tree
    // console.log('node: ', node);
    
    if(dir && node){ // MUST Validate duplicate name
      const n = "New_Folder";
      const fnames = node.children.filter((f) => f.name.startsWith(n));
      const id = "ti_" + Q.Qid(3);
      
      dir.tree("appendNode", {
        id, 
        type: "directory", 
        name: n + (fnames.length > 0 ? "_" + (fnames.length + 1) : ""), 
        children: [], 
        isEmptyFolder: true
      }, node);
      
      setTimeout(() => {
        $(treeEl).children("ul").find('.tree-edit[data-id="'+ id +'"]').trigger("click");
      }, 9);
    }
  });
  
  $(wrapEl + " .btn-add-root-file").on("click",function(){
    const node = dir.tree("getTree");// dir.tree('getNodeById', id);// Get the node from the tree
    
    if(dir && node){ // MUST Validate duplicate name
      const n = "Untitled";
      const fnames = node.children.filter((f) => f.name.startsWith(n) && f.name.endsWith("." + defaultExt));// ".txt"
      const id = "ti_" + Q.Qid();
      
      dir.tree("appendNode", {
        id, 
        type: "file", 
        name: n + (fnames.length > 0 ? "_" + (fnames.length + 1) : "") + "." + defaultExt, // ".txt"
        content: "" 
      }, node);

      // onAddItem(node.getData(false), node);
      
      setTimeout(() => $(treeEl).children("ul").find('.tree-edit[data-id="'+ id +'"]').trigger("click"), 9);
    }
  });
  
  $(wrapEl + " .btn-upload-root-file input").on("change",function(e){
    const files = e.target.files;
    const fl = files.length;
    // console.log('files: ', files);
    
    if(files && fl > 0){
      const node = dir.tree("getTree");
      // console.log('node: ', node);
      // if(node){
        // const nodeState = dir.tree("getState");// getSelectedNodes
        // console.log('nodeState: ', nodeState);
        // if(nodeState.selected_node.length > 0){ // nodeState && 
        //   dir.tree("setState", { ...nodeState, selected_node: [] });
        // }

        for(let i = 0; i < fl; i++){
          const file = files[i];
          const ext = getExt(file.name);
          // fileRead
          // fileReaderApi(file, { 
          //   readAs: file.type.startsWith("text/") || file.type.includes("/svg") || (textFiles && textFiles.includes(ext)) || file.type === "" ? "Text" : "DataURL" 
          // })
          // .then(data => {
          //   console.log('data: ', data);
          //   const fnames = node.children.filter((f) => f.name === file.name);
          //   const id = "ti_" + Q.Qid();
          //   const fname = noExt(file.name);// file.name.replace(regExFromStr("." + ext, "i"), ""); // file.name.split(".").shift();
            
          //   dir.tree("appendNode", {
          //     id, 
          //     type: "file", 
          //     name: (fnames.length > 0 ? fname + "_" + (fnames.length + 1) : fname) + "." + ext,  
          //     content: data.result 
          //   }, node);
            
          //   setTimeout(() => {
          //     const newNode = dir.tree("getNodeById", id);
          //     dir.tree("addToSelection", newNode, false);
          //   }, 9);
          // }).catch(e => console.log(e));
          
          console.log('file: ', file);
          console.log('node: ', node);
          const fnames = node?.children.filter((f) => f.name === file.name) || [];
          const id = "ti_" + Q.Qid();
          const fname = noExt(file.name);// file.name.replace(regExFromStr("." + ext, "i"), ""); // file.name.split(".").shift();
          
          dir.tree("appendNode", {
            id, 
            type: "file", 
            name: (fnames.length > 0 ? fname + "_" + (fnames.length + 1) : fname) + "." + ext,  
            content: file 
          });// , node

          // onAddItem(node.getData(false), node);
          
          // setTimeout(() => {
          //   const newNode = dir.tree("getNodeById", id);
          //   console.log('newNode: ', newNode);

          //   // dir.tree("addToSelection", newNode, false);
          // }, 9);
        }

        // onAddItem(node.getData(false), node);
      // }
    }
  });

  return dir;/// OPTION to destroy
}

export { jqtreeInit };