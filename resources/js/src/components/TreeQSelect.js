import React, { useState } from 'react';
import Collapse from 'react-bootstrap/Collapse';

import Btn from './q-ui-react/Btn';

export function TreeQSelect({
	open = false, // true
	mountOnEnter = true, 
	timeout = 150, 
	onToggle = Q.noop, 
	label,  
	labelProps, 
	children, 
	...etc
}){
	const [see, setSee] = useState(open);

	return (
		<div>
			<Btn {...labelProps} 
				active={see} 
				onClick={(e) => {
					setSee(!see);
					onToggle(!see, e);
				}}
			>
				{label}
			</Btn>

      <Collapse 
				{...etc} 
				in={see}
				mountOnEnter={mountOnEnter} 
				timeout={timeout} 
			>
        <div>{children}</div>
      </Collapse>
		</div>
	)
}

