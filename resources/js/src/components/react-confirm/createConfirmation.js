// import React from 'react';
// import ReactDOM from 'react-dom';

const createConfirmation = (Component, unmountDelay = 950, mountingNode) => { // 1000
  return (props) => {
		const body = document.body;
    const wrap = (mountingNode || body).appendChild(Q.makeEl("div"));// document.createElement('div')

    const promise = new Promise((resolve, reject) => {
      try {
        ReactDOM.render(
          <Component
            reject={reject}
            resolve={resolve}
            dispose={dispose}
            {...props}
          />,
          wrap
        );
      } catch (e) {
        console.error(e);
        throw e;
      }
    })

    function dispose() {
      setTimeout(() => {
        ReactDOM.unmountComponentAtNode(wrap);
        setTimeout(() => {
					if(body.contains(wrap)) {
						body.removeChild(wrap)
					}
        });
      }, unmountDelay);
    }

    return promise.then((result) => {
      dispose();
      return result;
    }, (result) => {
      dispose();
      return Promise.reject(result);
    });
  }
}

export default createConfirmation;
