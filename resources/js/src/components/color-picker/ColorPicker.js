import React, { useState } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import { HexColorPicker, RgbaColorPicker, RgbColorPicker, HexColorInput } from "react-colorful";//  

// import Btn from '../../components/q-ui-react/Btn';
import { rgb2hex, hex2Rgb } from '../../utils/colorConverter';

const rgbaObj2str = (obj) => {
	const { r, g, b, a } = obj;
	// Object.entries(val)
	const rgb = r + "," + g + "," + b;

	if(a) return "rgba(" + rgb + "," + a + ")";
	return "rgb(" + rgb + ")";
}

export default function ColorPicker({
	color = { r: 255, g: 255, b: 255, a: 1 }, 
	w = 265, 
	theme = "light", 
	// all, hex, rgb, rgba, hsl, hsla, hsv, hsva, (OPTION: RgbString, RgbaString, HslString, HslaString, HsvString, HsvaString)
	// type = "all", 

	// For show input type:
	hex = true, 
	rgb, 
	rgba = true, 

	className, 
	style, 
	onChange = Q.noop, 
}){
	const [val, setVal] = useState(color);
	const rgbVal = (Q.isStr(color) && hex2Rgb(color.replace(/#/g,''))) || null;
	// const [rgbVal, setRgbVal] = useState(Q.isStr(color) && hex2Rgb(color.replace(/#/g,'')) || null);// ?.obj
	
	const Change = (v, e, i) => {
		const vl = e.target.value;
		// const rgbObj = { ...val, [v]: Number(vl) };
		// const vals = Q.isObj(color) ? { ...val, [v]: Number(vl) } : getRgb2hex(hex2Rgb( val.replace(/#/g,'') )?.arr);

		// const h2r = hex2Rgb( val.replace(/#/g,'') )?.arr;
		// const r2h = rgb2hex(rgbaObj2str({ r: h2r[0], g: vl, b: h2r[2] }), 1);
		// console.log('Change h2r: ', h2r);
		// console.log('Change r2h: ', r2h);

		const vals = rgbVal ? getRgb2hex({ ...rgbVal.obj, [v]: Number(vl) }, 1) : { ...val, [v]: Number(vl) };

		setVal(vals);

		onChange(vals, e);
	}

	const getRgb2hex = (value, hx) => {
		return rgb2hex(rgbaObj2str(value), hx);
	}
	
	// console.log('rgb2hex: ', rgb2hex(rgbaObj2str(val), 1));
	// console.log('hex2Rgb: ', hex2Rgb(getRgb2hex()));
	// console.log('rgbVal: ', rgbVal);
	
  return (
    <div 
			className={Q.Cx("rounded color-pick bg-" + theme, className)} // 
			style={{ ...style, width: w }}
		>
			{(Q.isStr(color) && color.startsWith("#")) && 
				<HexColorPicker 
					className="w-100 mx-auto hex"
					color={val} 
					onChange={(v) => {
						setVal(v);
						onChange(v);
					}} 
				/>
			}

			{/* ((type === "all" || type === "rgba") && rgba) */}
			{(Q.isObj(color) && (rgba || color?.a) && !rgb) && 
				<RgbaColorPicker 
					className="w-100 mx-auto rgba"
					color={color} 
					onChange={(v) => {
						setVal(v);
						
					}} 
				/>
			}

			{(Q.isObj(color) && !color?.a && rgb) && 
				<RgbColorPicker 
					className="w-100 mx-auto rgb" 
					color={color} 
					onChange={setVal} 
				/>
			}
			
			<div className="p-2 border-top mt-2-next">
				<div className="input-group input-group-sm">
					{
						Object.entries(rgbVal?.obj || val).map((v, i) => // .filter(f => f[0] !== "a")
							<input key={i} className="w-auto flex-basis-0 form-control rgba-input" 
								type="number" // text 
								min={0} 
								// 999
								max={v[0] !== "a" ? 999 : 1} // 
								step={v[0] === "a" ? "0.01" : null} 
								// maxLength={v[0] !== "a" ? 3 : null} // OPTION
								defaultValue={v[1]} // value
								onChange={e => Change(v[0], e)} 
								// onChange={e => {
									// const c = e.target.value;
									// setVal({ ...val, [v[0]]: c });
								// }} 
							/>
						)
						
					}
				</div>

				{/* ((type === "all" || type === "hex") && hex) */}
				{(hex || color?.length >= 4) && 
					<label className="input-group input-group-sm">
						<div className="input-group-prepend">
							<div className="input-group-text">#</div>
						</div>
						<HexColorInput 
							className="form-control" 
							type="text" 
							// color={Q.isStr(color) && color.startsWith("#") ? color : getRgb2hex(1)} 
							color={Q.isStr(color) && color.startsWith("#") ? val : getRgb2hex(val, 1)} 
							onChange={(v) => {
								if(Q.isObj(color)){
									const rgbData = hex2Rgb(v.replace(/#/g,''));
									console.log('rgbData: ', rgbData);

									const [ r, g, b ] = rgbData.arr;
									setVal({ 
										r, g, b, 
										a: val.a 
									});
								}
								// Hexa
								else{
									setVal(v);
								}
							}} 
						/>
					</label>
				}
			</div>
    </div>
  );
};

