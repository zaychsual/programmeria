import React, { useState, useEffect } from 'react';
import { MemoryRouter, Route } from 'react-router-dom';

import Flex from '../q-ui-react/Flex';
import Btn from '../q-ui-react/Btn';
import { copyStyles } from '../../utils/css/copyStyles';
import { APP_NAME } from '../../data/appData';// APP_LANG

const setSrcDoc = ({ title, stylesheet, script, body }) => `<!DOCTYPE html><html lang="en"><head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
<meta name="theme-color" content="#e3f2fd">
<meta name="mobile-web-app-capable" content="yes">
<link rel="icon" href="/favicon.ico">
<title>${title}</title>${stylesheet ? stylesheet.map(v => `<link rel="stylesheet" href="${v}" crossorigin="anonymous">`) : ""}${script ? script.map(v => `<script src="${v}" crossorigin="anonymous"></script>`) : ""}
</head><body class="body-iframe">${body ? body : ""}</body></html>`;

// DEVS: Option for input url id
const INPUT_ID = "browserQ-url-" + Q.Qid();// 3

export default function Browser({
	// MemoryRouter props:
	initialEntries, 
	initialIndex, 
	keyLength, 
	
	className, 
	append, // nav, 
	style, 
	onLoad, 
	// onMount, onUpdate, 
	children, 
	frameProps, 
	frameRefNative, 
	html, 
	htmlBody, 
	stylesheet, 
	script, 
	display = "browser", // fullscreen, standalone, minimal-ui, browser (Options like manifest.json)
	theme = "light", // dark | light
	type = "native", // native | component
	addStyleParent = true, 
	// OPTION: For render datalist history entries
	historyList, // = true, 
	onMakeCss = Q.noop
}){
	// const [size, setSize] = useState(); // [window.innerWidth, window.innerHeight]
	// const [urlEntries, setUrlEntries] = useState([]);
	const [url, setUrl] = useState(null);
	const [load, setLoad] = useState(false);
	const [loadCount, setLoadCount] = useState(0);
	
	useEffect(() => {
		// if(type === "native") setLoad(true);
		setLoad(true);
	}, []);// , [type]
	
// OPTIONS: ???
	const getConfirm = (msg, cb) => {
		cb(window.confirm(msg));
	}

// ???
	// const createPath = (location) => {
		// return location.pathname + location.search;
	// }
	
	const setFrameProps = () => {
		return {
			...frameProps,
			className: Q.Cx("iframe-app type-" + type, frameProps?.className), // frameProps && frameProps.className
			title: frameProps?.title || APP_NAME, // "Programmeria",
			// ambient-light-sensor; camera; hid; vr; 
			allow: "accelerometer; encrypted-media; geolocation; gyroscope; microphone; midi; payment; usb; xr-spatial-tracking",
			// allow-autoplay 
			sandbox: "allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts", 
			onLoad: e => {
				let et = e.target;
				
				/* Add style from parent window */
				if(addStyleParent){
					setTimeout(() => copyStyles(document, et.contentDocument, onMakeCss?.(e)), 9);
				}
				
				if(onLoad) onLoad(e);
			}
		}
	}
	
	/* const renderDataList = (history) => {
		let entries = history.entries;
		
		if(entries.length > 1){
			let urls = entries.filter(f => f.pathname !== "/");
			// OPTION: For Firefox datalist is poor
			if(Q.UA.browser.name === "Firefox"){
				return (
					<div className="dropdown-menu mt-0 d-block" style={{ left: 60 }}>
						{urls.map(v => <button key={v.key} className="dropdown-item" type="button">{v.pathname}</button>)}
					</div>
				);
			}
			
			return (
				<datalist id={INPUT_ID}>
					{urls.map(v => <option key={v.key} value={v.pathname} />)}
				</datalist>
			);
		}
	} */
	
	const renderDataList = (history, nativeHistory) => {
		if(historyList){
			let entries = history.entries;
			// console.log('entries: ',entries);
			// console.log('window.history: ',window.history);
			
			if(entries.length > 1){
				return (
					<datalist id={INPUT_ID}>
						{entries.filter(f => f.pathname !== "/").map(v => <option key={v.key} value={v.pathname} />)}
					</datalist>
				);
			}
		}
	}

	return (
		<MemoryRouter 
			// initialEntries 
			initialEntries={initialEntries} // array | e.g ["/tests/1","/tests/2"], { pathname: "/three" }
			initialIndex={initialIndex} // number | e.g 1 
			keyLength={keyLength} // number | e.g 12
			getUserConfirmation={getConfirm} // OPTIONS: ???
		>
			<Route 
				render={({ history, location }) => (
					<Flex nowrap dir="column" className={Q.Cx("browser-q", className)} style={style}>
						{(display === "browser" || display === "minimal-ui") && 
							<div 
								// className={"input-group input-group-sm flex-nowrap flexno bg-" + theme}
								className="input-group input-group-sm flex-nowrap flexno"
							>
								<div className="input-group-prepend">
									<Btn kind={theme} 
										className="rounded-0 tip tipBL qi qi-arrow-left" 
										disabled={!history.canGo(-1)} 
										aria-label="Back" 
										onClick={history.goBack} 
									/>
									<Btn kind={theme} 
										className="qi qi-arrow-right tip tipBL" 
										disabled={!history.canGo(1)} 
										aria-label="Forward" 
										onClick={history.goForward} 
									/>

									{type === "native" && 
										<Btn kind={theme} 
											className="qi qi-redo tip tipBL browserBtnReload" 
											aria-label="Reload" 
											onClick={() => {
												setLoad(false);
												setLoadCount(loadCount + 1);
												// FOR iframe native
												if(type === "native") setTimeout(() => setLoad(true), 9);
											}} 
										/>
									}
								</div>
								
								<input type="url" spellCheck={false} // text // placeholder="Url" 
									list={INPUT_ID} // DEVS : Option
									className={Q.Cx("browser-input-url form-control rounded-0 shadow-none", { "bg-secondary border-secondary text-white": theme === "dark" })} 
									value={url ? url : location.pathname.replace("/", "") + location.search} // createPath(location) 
									// onFocus={e => e.target.select()} // OPTION FOR select all: onDoubleClick, onMouseUp, 
									onChange={e => setUrl(e.target.value)} 
									onKeyDown={e => {
										if(e.key === "Enter"){
											setUrl(null);
											history.push("/" + e.target.value);// history.push('/' + e.target.value);
										}
									}} 
								/>
								
								{/*<span>{console.log("history: ", history)}</span>
								<span>{console.log("location: ", location)}</span>
								<span>{console.log(history.entries.filter(v => ( v.pathname !== "/" && "OK" ) ))}</span>*/}
								
								{/* DEVS: Option for input url id */}
								
								{renderDataList(history)}
								
								{/*nav && <div className="input-group-append">{nav}</div>*/} 
								
								{/* append && <div className="input-group-append">{append}</div> */}
								
								{append}
							</div>
						}
						
						{/* load && */}
						{(type === "native" && load) ? // eslint-disable-next-line
							<iframe 
								{...setFrameProps()} 
								ref={frameRefNative} // DEV OPTION
								srcDoc={
									html 
									|| 
									setSrcDoc({
										title: frameProps?.title || APP_NAME, // "Programmeria"
										stylesheet, // css
										script, // js
										body: htmlBody
									})
								}
							/>
							: 
							children
						}
					</Flex>
				)}
			/>
		</MemoryRouter>
	);
}

// Browser.defaultProps = {
	// display: "browser", // fullscreen, standalone, minimal-ui, browser (Options like manifest.json)
	// theme: "light",
	// type: "native", // native | component
	// addStyleParent: true,
	// onMakeCss: Q.noop // () => {}
// };

// OPTIONS
// export default React.memo(Browser);

