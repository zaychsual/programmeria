import React from 'react';
import { MemoryRouter, Route } from 'react-router-dom';

export default function RouterMemory({
	// initialEntries, 
	// initialIndex, 
	// keyLength, 
	children, 
	...etc
}){

// OPTIONS: ???
	// const getConfirm = (msg, cb) => {
		// cb(window.confirm(msg));
	// }

// ???
	// const createPath = (location) => {
		// return location.pathname + location.search;
	// }	
	
	return (
		<MemoryRouter 
			{...etc} 
			// initialEntries={initialEntries} // array | e.g ["/tests/1","/tests/2"], { pathname: "/three" }
			// initialIndex={initialIndex} // number | e.g 1 
			// keyLength={keyLength} // number | e.g 12
			// getUserConfirmation={getConfirm} 
		>
			<Route 
				render={({history, location}) => {
					return children({ history, location })
				}}
			/>
		</MemoryRouter>
	);
}