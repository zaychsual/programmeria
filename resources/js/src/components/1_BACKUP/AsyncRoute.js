import React from "react";
import { Route } from "react-router-dom";
import loadable from "@loadable/component";

export default function AsyncRoute({ 
  importPath, // importPath
  ...etc 
}) {
  return <Route {...etc} component={loadable(importPath)} />;
}

