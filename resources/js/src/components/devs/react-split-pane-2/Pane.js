import React from 'react';// , { PureComponent }
// import P from 'prop-types';// PropTypes

// import prefixAll from 'inline-style-prefixer/static';

import { getUnit, convertSizeToCssValue } from './SplitPane';

// import { Cx } from '../../utils/Q';

function PaneStyle({ split, initialSize, size, minSize, maxSize, resizersSize }){
  const value = size || initialSize;
  const vertical = split === 'vertical';
  const styleProp = {
    minSize: vertical ? 'minWidth' : 'minHeight',
    maxSize: vertical ? 'maxWidth' : 'maxHeight',
    size: vertical ? 'width' : 'height'
  };

  let style = {
    // display: 'flex',
    // outline: 'none'
  };

  style[styleProp.minSize] = convertSizeToCssValue(minSize, resizersSize);
  style[styleProp.maxSize] = convertSizeToCssValue(maxSize, resizersSize);

  switch(getUnit(value)){
    case 'ratio':
      style.flex = value;
      break;
    case '%':
    case 'px':
      style.flexGrow = 0;
      style[styleProp.size] = convertSizeToCssValue(value, resizersSize);
      break;
		default:
			break;
  }

  return style;
}


class Pane extends React.PureComponent{
  setRef = n => this.props.innerRef(this.props.index, n);

  render(){
    const { children, theme, className, id } = this.props;
    const prefixedStyle = PaneStyle(this.props);// prefixAll(PaneStyle(this.props));

    return (
      <div ref={this.setRef} 
				id={id} // Q-CUSTOM
        className={Q.Cx("Pane", theme, className)} // 
        style={prefixedStyle}
      >
        {children}
      </div>
    );
  }
}

// Pane.propTypes = {
  // children: P.node,
  // innerRef: P.func,
  // index: P.number,
  // className: P.string,
  // initialSize: P.oneOfType([P.string, P.number]),
  // minSize: P.string,
  // maxSize: P.string
// };

Pane.defaultProps = {
  initialSize: '1',
  split: 'vertical',
  minSize: '0',
  maxSize: '100%'
};

export default Pane;
