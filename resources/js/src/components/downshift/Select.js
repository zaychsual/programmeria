import React from 'react';
import Downshift from 'downshift';
import Dropdown from 'react-bootstrap/Dropdown';

import Btn from '../q-ui-react/Btn';

export default function Select({
	entries, // history, 
	// location, 
	open, 
	downshiftProps, 
	// downshiftPropsEtc, 
	downshiftMenuProps, 
	dropdownProps, 
	dropdownToggleProps, 
	dropdownMenuProps, 
	inputProps, 
	
	keyValue, 
	multiple, 
	value, // selected, 
	size, // input & tag size
	notFound = "Not Found", // not Found text / component
	onRemoveItem = Q.noop, // onRemoveItem | onChange
}){
	if(!entries) return null;
	
	// let entries = history.entries;
	
	const findItems = ({ inputValue, getItemProps, selectedItem, highlightedIndex }) => {
		const filters = entries.filter(item => !inputValue || (keyValue ? item[keyValue].includes(inputValue) : item.includes(inputValue)));
		console.log('filters: ', filters);
		// !inputValue || item.pathname.includes(inputValue)
		return filters?.length > 0 ? 
			filters.map((item, index) => (										
				<Dropdown.Item 
					{...Q.DD_BTN} 

					{...getItemProps({
						key: index, // item.pathname,
						index, 
						item, 
						active: selectedItem === item || (multiple && value.includes(item)), 
						className: highlightedIndex === index && "highlighted"
						// style: {
							// backgroundColor:
								// highlightedIndex === index ? 'lightgray' : 'white',
							// fontWeight: selectedItem === item ? 'bold' : 'normal',
						// },
					})}
				>
					{item}
				</Dropdown.Item>
			))
			: 
			Q.isStr(notFound) ? <h6 className="dropdown-header">{notFound}</h6> : notFound
	}
	
	const renderInput = (getInputProps, inputProps) => {
		return (
			<input 
				{ ...getInputProps(
						{ 
							...inputProps, 
							className: Q.Cx("form-control ml-0", { 
								["form-control-" + size]: size 
							}, inputProps?.className) 
						}
					)
				} 
			/>
		)
	};
	
	// console.log(multiple && selected?.length > 0);

	return (
		<Downshift 
			{...downshiftProps} 
			// onChange={selection => {
				// console.log(selection ? `You selected ${selection.value}` : 'Selection Cleared')
			// }}
			// onChange={downshiftProps.onChange}
			// itemToString={item => (item && key ? item[key] : "")}
		>
			{({
				// downshiftPropsEtc, // Error???
				getRootProps, 
				getInputProps,
				getItemProps,
				// getLabelProps,
				getMenuProps, 
				isOpen,
				inputValue,
				highlightedIndex,
				selectedItem, 
			}) => (
				<Dropdown 
					{...getRootProps({ ...dropdownProps, className: Q.Cx("d-flex flex1", dropdownProps?.className) }, { 
							suppressRefError: true 
						}
					)} 

					show={open || isOpen} 
				>
					<Dropdown.Toggle {...dropdownToggleProps}>
						{(multiple && Array.isArray(value)) ? 
							<label className="d-flex flex-row flex-wrap mb-0 ml-1-next">
								{value?.length > 0 && 
									value.map((v, i) => 
										<div key={i} className={Q.Cx("btn-group flexno", { ["btn-group-" + size]: size })}>
											<Btn As="div" kind="light">{v.label || v}</Btn>
											<Btn onClick={() => onRemoveItem(v, i)} As="div" kind="light" className="qi qi-close" />
										</div>
									)
								}
								
								{renderInput(getInputProps, inputProps)}
							</label>
							: 
							renderInput(getInputProps, inputProps)
						}
					</Dropdown.Toggle>

					<div {...getMenuProps({ ...downshiftMenuProps })}>
						<Dropdown.Menu {...dropdownMenuProps}>
							{inputValue && inputValue.length > 0 ? 
								findItems({ inputValue, getItemProps, selectedItem, highlightedIndex })
								: 
								entries.map((item, index) => (										
									<Dropdown.Item 
										{...Q.DD_BTN} 

										{...getItemProps({
											key: index, // item.pathname,
											index,
											item, 
											// || highlightedIndex === index
											active: selectedItem === item || (multiple && value.includes(item)), 
											className: highlightedIndex === index && "highlighted"
											// style: {
												// backgroundColor:
													// highlightedIndex === index ? 'lightgray' : 'white',
												// fontWeight: selectedItem === item ? 'bold' : 'normal',
											// },
										})}
									>
										{item}
									</Dropdown.Item>
								))
							}
						</Dropdown.Menu>
					</div>
				</Dropdown>
			)}
		</Downshift>
	);
}

/*
<Btn key={i} kind="light" className="flexno qia qia-close">{v} </Btn>
*/

	// const HistoryList = ({ history, onClick = Q.noop }) => {
	// 	let entries = history.entries;
	// 	if(entries){
	// 		console.log('entries: ',entries);
	// 		// console.log('window.history: ', window.history);
			
	// 		if(entries.length > 1){
	// 			return (
	// 				history.entries.map((v, i) => 
	// 					<Dropdown.Item key={i} onClick={() => onClick(v)} as="button" type="button">{v.pathname}</Dropdown.Item>
	// 				)
	// 			);
	// 		}
	// 		return null;
	// 	}
	// }

	// const renderDataList = (history) => {
	// 	const entries = history.entries;
	// 	if(entries && entries.length > 1){
	// 		const noDup = [...(new Set(entries.map(v => v.pathname)))];
	// 		console.log('entries: ', entries);
	// 		console.log('noDup: ', noDup);
	// 		// console.log('window.history: ', window.history);
	// 		// [...(new Set(MONACO_LANGS.map(v => v.type)))];
	// 		// [...(new Set(entries.map(v => <option key={v.key} value={v.pathname} />)))];
	// 		return (
	// 			<datalist id={INPUT_ID}>
	// 				{noDup.map((v, i) => <option key={i} value={v} />)}
	// 			</datalist>
	// 		);
	// 	}
	// }

/* <Dropdown // className="position-absolute inset-0" 
	alignRight 
	show={ddPrevLocations} 
	onToggle={(open) => {
		setDdPrevLocations(open);
		if(open) onSetEditPath();
	}}
>
	<Dropdown.Toggle size="sm" variant="light" title="Previous locations">
		
	</Dropdown.Toggle>
	<Dropdown.Menu className="py-1 dd-sm dd-prevlocs"
		popperConfig={{
			modifiers: [{
				name: "offset",
				options: {
					offset: [-30, 0]
				}
			}]
		}}
	>
		<HistoryList history={history} onClick={(path) => setPathType(path.pathname)} />
	</Dropdown.Menu>
</Dropdown> */	