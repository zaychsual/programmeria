import React from 'react';
import ReactDOM from 'react-dom';
// import P from 'prop-types';

import { copyStyles } from '../../utils/css/copyStyles';

/** Convert features props to window features format (name=value,other=value).
 * @param {Object} obj
 * @return {String} */

function toWindowFeatures(obj){
  if(Q.isObj(obj)){
    return Object.keys(obj).reduce((features, name) => {
      const value = obj[name];
      if(Q.isBool(value)){ // typeof value === 'boolean'
        features.push(name + "=" + (value ? "yes" : "no")); // `${name}=${value ? "yes" : "no"}`
      }else{
        features.push(name + "=" + value);// `${name}=${value}`
      }
      return features
    }, [])
    .join(",");
  }

  return null;
}

export default class NewWindow extends React.PureComponent{
  constructor(props){
    super(props);

    this.div = Q.makeEl("div"); // document.createElement('div')
    this.win = null;
    this.windowCheckerInterval = null;
    this.released = false;

    this.state = {
      mounted: false
    };
  }

  componentDidMount(){
    this.openChild();
    this.setState({ mounted: true });
  }

/** Close the opened window (if any) when NewWindow will unmount */
  componentWillUnmount(){
    if(this.win) this.win.close();
  }

/** Create the new window when NewWindow component mount. */
  openChild(){
    const { url, title, name, features, onBlock, onOpen, center } = this.props;

    if(features){
      // Prepare position of the new window to be centered against the 'parent' window or 'screen'.
      // typeof center === 'string' && (features.width === undefined || features.height === undefined)
      if(Q.isStr(center) && (features.width === undefined || features.height === undefined)){
        console.warn("width and height window features must be present when a center prop is provided");
      }else if(center === "parent"){
        features.left = window.top.outerWidth / 2 + window.top.screenX - features.width / 2;
        features.top = window.top.outerHeight / 2 + window.top.screenY - features.height / 2;
      }else if(center === 'screen'){
        const screenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screen.left;
        const screenTop = window.screenTop !== undefined ? window.screenTop : window.screen.top;

        const width = window.innerWidth
          ? window.innerWidth
          : document.documentElement.clientWidth
          ? document.documentElement.clientWidth
          : window.screen.width;
        const height = window.innerHeight
          ? window.innerHeight
          : document.documentElement.clientHeight
          ? document.documentElement.clientHeight
          : window.screen.height;

        features.left = width / 2 - features.width / 2 + screenLeft;
        features.top = height / 2 - features.height / 2 + screenTop;
      }
    }

    // Open a new window.
    this.win = window.open(url, name, toWindowFeatures(features));

    // When a new window use content from a cross-origin there's no way we can attach event
    // to it. Therefore, we need to detect in a interval when the new window was destroyed
    // or was closed.
    this.windowCheckerInterval = setInterval(() => {
      if(!this.win || this.win.closed){
        this.release();
      }
    }, 50);

    // Check if the new window was succesfully opened.
    if(this.win){
      this.win.document.title = title;
      this.win.document.body.appendChild(this.div);

      // If specified, copy styles from parent window's document.
      if(this.props.copyCss){
        setTimeout(() => copyStyles(document, this.win.document), 1);// 0
      }

      if(Q.isFunc(onOpen)){// typeof onOpen === 'function'
        onOpen(this.win);
      }
      // Release anything bound to this component before the new window unload.
      this.win.addEventListener('beforeunload', () => this.release());
    }else{
      // Handle error on opening of new window.
      if(Q.isFunc(onBlock)){// typeof onBlock === 'function'
        onBlock(null);
      }else{
        console.warn('A new window could not be opened. Maybe it was blocked.');
      }
    }
  }

/** Release the new window and anything that was bound to it */
  release(){
    // This method can be called once.
    if(this.released) return;
    this.released = true;

    // Remove checker interval.
    clearInterval(this.windowCheckerInterval);

    // Call any function bound to the `onUnload` prop.
    const { onUnload } = this.props;

    if(Q.isFunc(onUnload)) onUnload(null);// typeof onUnload === 'function'
  }	
	
  render(){
    if(!this.state.mounted) return null;
    return ReactDOM.createPortal(this.props.children, this.div);
  }
}

NewWindow.defaultProps = {
	url: "",
	name: "",
	title: "",
	features: {
    width: "600px", 
    height: "640px"
  },
	// onBlock: null, 
	// onOpen: null, 
	// onUnload: null, 
	center: "parent", // parent | screen
	copyCss: true
};

// NewWindow.propTypes = {
//   children: P.node,
//   url: P.string,
//   // name: P.string,
//   title: P.string,
//   // features: P.object,
//   onUnload: P.func,
//   onBlock: P.func,
//   onOpen: P.func,
//   center: P.oneOf(['parent','screen']),
//   copyCss: P.bool // copyStyles
// };


