import React, { useState, useContext } from 'react';// { , useContext, useEffect }
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik | Formik
// import axios from 'axios';
import { Redirect } from 'react-router-dom';// useHistory,

// import { AuthContext } from '../../context/AuthContext';
import Head from '../../../components/q-ui-react/Head';
import Placeholder from '../../../components/q-ui-react/Placeholder';
import Form from '../../../components/q-ui-react/Form';
import Flex from '../../../components/q-ui-react/Flex';
import Aroute from '../../../components/q-ui-react/Aroute';
import Btn from '../../../components/q-ui-react/Btn';
import Input from '../../../components/q-ui-react/Input';
import Password from '../../../components/q-ui-react/Password';
import { AppConfigContext } from '../../../context/AppContext';
import { useFetch } from '../../../utils/hooks/useFetch';

export default function Register(){
	const CARD_CLASS = "card w-350px w-t-p-50 w-t-l-35 w-p-p-100 shadow-sm";//  w-p-lp-50

	const initialValues = {
		email: "", // DUMMY: eve.holt@reqres.in
		// fullname: null, // null
		// username: null, 
		password: "", // DUMMY: pistol
		c_password: "", // confirmPassword
		// term: false
	};

	// let history = useHistory();
	// const auth = useContext(AuthContext);
	// const isLog = auth.localAuth();

	const { config, setAppConfig } = useContext(AppConfigContext);
	const [formValues, setFormValues] = useState(initialValues);
	// const [appConfig, setAppConfig] = useState(config);// null
	const [isLog, setLog] = useState(false);// OPTION: Check Auth in App.js // auth.localAuth()
	const [err, setErr] = useState(false);
	const [ok, setOk] = useState(false);//  | false
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Register','color:yellow;');
	// }, []);

/** @DEV : Set with useContext */	
	const parseVal = (data) => {
		const { fullname, username } = data;
		let val = {};
		if(fullname?.register) val.name = "";
		if(username?.register) val.username = "";
		return val;
	}
	const isLoad = useFetch(
		async (cancelToken) => {
			// let exp = sessionStorage.getItem("exp");
			// if(exp){
			// 	let now = Date.now();
			// 	let calc = now - Number.parseInt(exp);
			// 	console.log('calc exp: ', calc);
			// 	if(calc >= 5000){
			// 		sessionStorage.removeItem("exp");
			// 		alert('SESSION EXPIRED...!!!');
			// 	}else{
			// 		sessionStorage.setItem("exp", now);
			// 	}
			// }

			if(config.identity){
				setFormValues({ // await 
					...initialValues, 
					...parseVal(config)
				});
        return;
      }
			// APP_CONFIG
			const req = await axios.get(Q.baseURL + "/DUMMY/json/AUTH_CONFIG.json", { cancelToken });
			console.log('req: ', req);
			const data = req.data;
			if(data?.error){// r.status === 200 && 
				console.log('handle error server e: ', e);
			}else{
				setAppConfig({ ...config, ...data });
				setFormValues({
					...initialValues, 
					...parseVal(data)
				});
			}
		}, 
		(err) => {
			console.log('err: ', err);
		}
	);

	let { passwordMinLength, fullname, username, confirm } = config;
	
  const formik = useFormik({
		enableReinitialize: true, 
    initialValues: formValues, 
    validationSchema: Yup.object().shape({
			email: Yup.string()
				// .label('Email')
				.email("Wrong email format")
				// .max(50, "Maximum 50 symbols")
				.required("Email is required"), 
			name: Yup.string().nullable()
				.max(50, "Maximum 50 symbols")
				.when("f", { // ["isComputedField", "fieldType"]
					is: () => fullname?.register && fullname?.required, // true
					then: Yup.string().required("Full name is required")
				}),
			username: Yup.string().nullable()
				.max(50, "Maximum 50 symbols")
				.when("u", {
					is: () => username?.register && username?.required, // true
					then: Yup.string().required("Username is required")
				}),
			password: Yup.string()
				.min(passwordMinLength, "Minimum " + passwordMinLength + " symbols")
				.max(50, "Maximum 50 symbols")
				.required("Password is required"),
			c_password: Yup.string() 
				.required('Must same with password')
				// .test('passwords-match', 'Password must match', v => v === Yup.ref("password")), 
				.oneOf([Yup.ref('password')], 'Must same with password')
				// term: Yup.bool().oneOf([true], 'Terms & Conditions must be accepted')
		}), 
    onSubmit: (values, fn) => { // setStatus, 
			// https://reqres.in/api/register
			// DEVS ONLY:
			const val = {
				// username: values.username, 
				// name: values.fullname, 
				// email: values.email, 
				// password: values.password, 
				// c_password: values.c_password, 
				...values, 
				role_id: "2"
			};
			console.log('values: ',values);
			console.log('val: ',val);
			// { withCredentials: false }
			axios.post("/register", val).then(r => {
				/** @NOTES : Encrypt password / all data */
				console.log('r: ', r);
				if(r?.data?.token){
					// sessionStorage.setItem('auth', JSON.stringify(r.data));
					// history.replace("/");// Redirect to Home
					setErr(false);
					// DEVS: Use confirm register with email / sms
					setOk(confirm ? "Please check your " + confirm : true);// true
					// document.title = "Success " + document.title;// OPTION
				}
				else{
					setErr("Error Register");
				}
			})
			.catch(e => {
				// console.log('e.response: ', e.response);
        let { status, data } = e.response;
        let { errors, message } = data || {};
        if(status === 422 && data){
          console.log('errors', errors);
          for(let key in errors){
            fn.setFieldError(key, errors?.[key].join("\n"));
          }
        }
        message && setErr(message);
			}).then(() => fn.setSubmitting(false));
    },
	});

	let { values, errors, touched, isSubmitting, handleSubmit, handleChange } = formik; 

	if(isLog) return <Redirect to="/" />;// OPTION: Check Auth in App.js

	if(ok){
		return (
			<Flex dir="column" justify="center" align="center" className="h-full-navmain">
				<h1>Register Success</h1>
				{Q.isStr(ok) && <p className="lead">{ok}</p>}
				<Aroute to="/login" btn="primary">Login</Aroute>
			</Flex>
		)
	}

// console.log('config: ', config);
// d-grid place-center
	return (
		<Flex dir="column" justify="center" align="center" className="container-fluid py-3 mh-full-navmain">
			<Head title="Register" />

			{isLoad ? 
				<>
					{err && 
						<div className={"alert alert-danger " + CARD_CLASS}>{err}</div>
					}

					<div className={CARD_CLASS + " p-3 bg-light"}>{/* bg-strip */}
						<h1 className="h4 hr-h mb-3">Register</h1>

						<Form noValidate 
							// className={formik.isValid ? undefined : "was-validated"} 
							className={isSubmitting && "i-load cwait"} // Q.Cx(formik.isSubmitting && "i-load cwait") | formik.isSubmitting ? " i-load cwait" : ""
							style={isSubmitting ? { '--bg-i-load': '95px' } : null} 
							disabled={isSubmitting} // isProgress
							fieldsetClass="mt-3-next" 
							onSubmit={handleSubmit} 
						>
							<div>{/* form-group | mb-3 */}
								<label htmlFor="email" className="small mb-1">Email</label>
								<Input 
									required 
									type="email" 
									className={Q.formikValidClass(formik, "email")} 
									id="email" 
									value={values.email} 
									onChange={handleChange} 
									// onBlur={formik.handleBlur} 
								/>

								{(touched.email && errors.email) && 
									<div className="invalid-feedback">{errors.email}</div>
								}
							</div>

							{fullname?.register && 
								<div>
									<label htmlFor="name" className="small mb-1">Full name</label>
									<Input 
										className={Q.formikValidClass(formik, "name")} 
										required={fullname?.required} 
										autoComplete="off" 
										autoCapitalize="off" 
										id="name" 
										value={values.name ?? ""} 
										onChange={handleChange} 
									/>

									{(touched.name && errors.name) && 
										<div className="invalid-feedback">{errors.name}</div>
									}
								</div>
							}

							{username?.register && 
								<div>
									<label htmlFor="username" className="small mb-1">Username</label>
									<Input 
										className={Q.formikValidClass(formik, "username")} 
										required={username?.required} 
										autoComplete="off" 
										autoCapitalize="off" 
										id="username" 
										value={values.username ?? ""} 
										onChange={handleChange} 
									/>

									{(touched.username && errors.username) && 
										<div className="invalid-feedback">{errors.username}</div>
									}
								</div>
							}

							<div>
								<label htmlFor="password" className="small mb-1">Password</label>
								<Password 
									required 
									className={Q.formikValidClass(formik, "password")} 
									// pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 
									id="password" 
									value={values.password} 
									onChange={handleChange}
								/>

								{(touched.password && errors.password) && 
									<div className="invalid-feedback d-block">{errors.password}</div>
								}
							</div>

							<div>
								<label htmlFor="c_password" className="small mb-1">Confirm Password</label>
								<Password 
									required 
									className={Q.formikValidClass(formik, "c_password")} 
									// pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 
									id="c_password" 
									value={values.c_password} 
									onChange={handleChange}
								/>

								{(touched.c_password && errors.c_password) && 
									<div className="invalid-feedback d-block">{errors.c_password}</div>
								}
							</div>

							<Flex align="start">
								<div className="small mr-2">
									By register, you agree to our Terms, Data Policy and Cookies Policy. 
									<Aroute 
										to="/terms-and-conditions" 
										// kind="dark" // secondary
										className="d-inline-block mt-2 qi qi-info" // 
										disabled={isSubmitting} 
										tabIndex="-1" 
									> Read terms & conditions
									</Aroute>
								</div>
								
								<Btn type="submit">Register</Btn>
							</Flex>
						</Form>
					</div>

					<div className={CARD_CLASS + " p-3 bg-light d-block mt-2 text-center text-sm"}>
						Have an account? <Aroute to="/login">Login</Aroute>
					</div>
				</>
				: 
				<Placeholder type="thumb" className={CARD_CLASS} />
			}
		</Flex>
	);
}

/*

*/
