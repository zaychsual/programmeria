import React, { useState, useContext } from 'react';// { , useContext, useEffect }
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik | Formik
// import axios from 'axios';
// import { Redirect } from 'react-router-dom';// useHistory, 

// import { AuthContext } from '../../context/AuthContext';
import Head from '../../../components/q-ui-react/Head';
import Placeholder from '../../../components/q-ui-react/Placeholder';
import Form from '../../../components/q-ui-react/Form';
import Flex from '../../../components/q-ui-react/Flex';
import Aroute from '../../../components/q-ui-react/Aroute';
import Btn from '../../../components/q-ui-react/Btn';
import Input from '../../../components/q-ui-react/Input';
import { AppConfigContext } from '../../../context/AppContext';
import { useFetch } from '../../../utils/hooks/useFetch';

export default function ForgotPassword(){
	const CARD_CLASS = "card w-350px w-t-p-50 w-t-l-35 w-p-p-100 shadow-sm";//  w-p-lp-50

	// let history = useHistory();
	// const auth = useContext(AuthContext);
	// const isLog = auth.localAuth();

	const { config, setAppConfig } = useContext(AppConfigContext);
	// const [appConfig, setAppConfig] = useState({ passwordMinLength: 5 });// null
	// const [isLog, setLog] = useState(false);// auth.localAuth()
	const [err, setErr] = useState(false);
	const [ok, setOk] = useState(false);//  | false
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in ForgotPassword','color:yellow;');
	// }, []);

/** @DEV : Set with useContext */	
	const isLoad = useFetch(
		async (cancelToken) => {
			// let exp = sessionStorage.getItem("exp");
			// if(exp){
			// 	let now = Date.now();
			// 	let calc = now - Number.parseInt(exp);
			// 	console.log('calc exp: ', calc);
			// 	if(calc >= 5000){
			// 		sessionStorage.removeItem("exp");
			// 		alert('SESSION EXPIRED...!!!');
			// 	}else{
			// 		sessionStorage.setItem("exp", now);
			// 	}
			// }

			if(config.identity){
        return;
      }
			// APP_CONFIG
			const req = await axios.get(Q.baseURL + "/DUMMY/json/AUTH_CONFIG.json", { cancelToken });
			console.log('req: ', req);
			const data = req.data;
			if(data && !data.error){// r.status === 200 && 
				setAppConfig({ ...config, ...data });
			}else{
				console.log('handle error server e: ', e);
			}
		}, 
		(err) => {
			console.log('err: ', err);
		}
	);
	
  const formik = useFormik({
		enableReinitialize: true, 
    initialValues: {
			identity: "", // DUMMY: eve.holt@reqres.in
			// username: "", 
		}, 
    validationSchema: Yup.object().shape({
			identity: Yup.string()
				.min(3, "Minimum 3 symbols")
				// .email("Wrong email format")
				.required("Is required"), // Email is required
			// username: Yup.string()
			// 	.max(50, "Maximum 50 symbols"), 
			// 	// .required("Username is required"),
		}), 
    onSubmit: (values, fn) => { // setStatus, 
			console.log('values: ',values);
			// https://reqres.in/api/register | /api/forgot-password
			axios.post(
				"https://reqres.in/api/register", 
				{ email: values.identity, password: "pistol" }, 
				{ withCredentials: false }
			).then(r => {
				console.log('r: ', r);
				if(r.data?.token){ // token | access_token
					// sessionStorage.setItem('auth', JSON.stringify(r.data));
					// history.replace("/");// Redirect to Home
					setErr(false);
					setOk(true);
					// document.title = "Success " + document.title;// OPTION
				}
				else{
					setErr("Error Forgot Password");
				}
			})
			.catch(e => {
				console.log('e: ', e);
				setErr(e.message);
			}).then(() => fn.setSubmitting(false));
    },
	});

  const parseLabelIdentity = () => {
    const { identity } = config;
    return identity?.length > 1 ? identity.map((v, i) => (i + 1) === identity.length ? "or " + v : v).join(", ") : identity[0];
  }

	// if(isLog) return <Redirect to="/" />;// OPTION

	if(ok){
		return (
			<Flex dir="column" justify="center" align="center" className="h-full-navmain">
				<h1>Forgot Password Success</h1>
				<p className="lead">Open your email</p>
				<Aroute to="/login" btn="primary">Login</Aroute>
			</Flex>
		)
	}

	return (
		<Flex dir="column" justify="center" align="center" className="container-fluid py-3 mh-full-navmain">
			<Head title="Forgot Password" />

			{isLoad ? 
				<>
					{err && 
						<div className={"alert alert-danger " + CARD_CLASS}>
							{err}
						</div>
					}

					<div className={CARD_CLASS + " p-3 bg-light"}>{/* bg-strip */}
						<h1 className="h4 hr-h mb-3">Forgot Password</h1>

						<Form noValidate 
							className={formik.isSubmitting && "i-load cwait"} 
							style={formik.isSubmitting ? { '--bg-i-load': '95px' } : null} 
							disabled={formik.isSubmitting} // isProgress
							onSubmit={formik.handleSubmit} 
						>
							<div className="mb-3">{/* form-group */}
								{/* <div className="small mb-1">
									Enter your {parseLabelIdentity()} and we'll send you a link to get back into your account.
								</div> text-capitalize */}
                <label htmlFor="identity" className="small mb-1">
									Enter your <b>{parseLabelIdentity()}</b> and we'll send you a link to get back into your account.
                </label>
								<Input 
									required 
									// type="email" 
									// placeholder="Email, Phone, or Username" 
									className={Q.formikValidClass(formik, "identity")} 
									id="identity" 
									value={formik.values.identity} 
									onChange={formik.handleChange} 
									// onBlur={formik.handleBlur} 
								/>

								{(formik.touched.identity && formik.errors.identity) && 
									<div className="invalid-feedback">{formik.errors.identity}</div>
								}
							</div>

							<Btn block type="submit">Submit</Btn>

							{/** @OPTION: Enter your email, phone number, or username
							
							<Flex align="start" className="mt-3">
								<div className="small mr-2">
									By register, you agree to our Terms, Data Policy and Cookies Policy. 
									<Aroute 
										to="/terms-and-conditions" 
										kind="dark" // secondary
										className="d-inline-block mt-2 qi qi-info" // 
										disabled={formik.isSubmitting} 
										tabIndex="-1" 
									> Read terms & conditions
									</Aroute>
								</div>
								
								<Btn type="submit">Register</Btn>
							</Flex> */}
						</Form>
					</div>

					<div className={CARD_CLASS + " p-3 bg-light d-block mt-2 text-center text-sm"}>
						{/* Have an account? <Aroute to="/login">Login</Aroute> */}
						Don't have account? <Aroute to="/register">Register</Aroute>
					</div>
				</>
				: 
				<Placeholder type="thumb" className={CARD_CLASS} />
			}
		</Flex>
	);
}

/*
<React.Fragment></React.Fragment>
*/
