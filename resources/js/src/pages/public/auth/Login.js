import React, { useState, useContext } from 'react';// useEffect
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik | Formik
import { useHistory } from 'react-router-dom';// withRouter, Redirect
import { BroadcastChannel }from 'broadcast-channel';
// import axios from 'axios';

import Head from '../../../components/q-ui-react/Head';
import Placeholder from '../../../components/q-ui-react/Placeholder';
import Flex from '../../../components/q-ui-react/Flex';
import Form from '../../../components/q-ui-react/Form';
import Input from '../../../components/q-ui-react/Input';
import Btn from '../../../components/q-ui-react/Btn';
import Aroute from '../../../components/q-ui-react/Aroute';
import Password from '../../../components/q-ui-react/Password';
import { AppConfigContext } from '../../../context/AppContext';
import { useFetch } from '../../../utils/hooks/useFetch';

export default function Login(){
  const CARD_CLASS = "card w-350px w-t-p-50 w-t-l-35 w-p-p-100 shadow-sm";// w-p-lp-50 

  let history = useHistory();
  const { config, setAppConfig } = useContext(AppConfigContext);
  // const [ok, setOk] = useState(false);//  | false
  const [err, setErr] = useState(false);

  // useEffect(() => {
  //   console.log('%cuseEffect in Login','color:yellow');
  // }, []);

/** @DEV : Set with useContext */ 
	const isLoad = useFetch(
		async (cancelToken) => {
      // let exp = sessionStorage.getItem("exp");
      // if(exp){
      //   let now = Date.now();
      //   let calc = now - Number.parseInt(exp);
      //   console.log('calc exp: ', calc);
      //   if(calc >= 5000){
      //     sessionStorage.removeItem("exp");
      //     alert('%cSESSION EXPIRED...!!!','color:yellow');
      //   }else{
      //     sessionStorage.setItem("exp", now);
      //   }
      // }

      if(config.identity){
        return;
      }

      // APP_CONFIG
			const req = await axios.get(Q.baseURL + "/DUMMY/json/AUTH_CONFIG.json", { cancelToken });
			console.log('req: ', req);
			if(req.data && !req.data.error){// r.status === 200 && 
				setAppConfig({ ...config, ...req.data });
			}else{
				console.log('handle error server e: ', e);
			}
		}, 
		(err) => {
			console.log('err: ', err);
		}
	);

  const formik = useFormik({
    enableReinitialize: true, 
    initialValues: {
      // email | identity
      identity: "", // DUMMY: eve.holt@reqres.in
      password: "", // DUMMY: cityslicka
      remember: false
    }, 
    validationSchema: Yup.object().shape({
      identity: Yup.string()
        .min(3, "Minimum 3 symbols")
        // .max(50, "Maximum 50 symbols")
        .required("Is required")
        // .required(
        //   intl.formatMessage({
        //     id: "AUTH.VALIDATION.REQUIRED_FIELD",
        //   })
        // ),
        // OPTION: 
        .when("n", { // ["isComputedField", "fieldType"]
					is: () => config?.identity.length === 1 && config.identity[0] === "email", // true
					then: Yup.string().email("Wrong email format")
				}),
      password: Yup.string()
        .min(config.passwordMinLength, "Minimum " + config.passwordMinLength + " symbols")
        .max(50, "Maximum 50 symbols")
        .required("Password is required")
    }), 
    onSubmit: (values, fn) => {
      /** @NOTES : Encrypt password / all data */
      console.log('values: ', values);
      // https://reqres.in/api/login
      axios.post("/login", { 
        password: values.password, 
        email: values.identity, 
        remember: values.remember,
        passwordMinLength: config.passwordMinLength
      }).then(r => {
        console.log('r: ', r);
        let { token, user, error } = r.data;
        if(token && !error){ // token | access_token
          const bcAuth = new BroadcastChannel('AUTH');
          bcAuth.postMessage({ 
            action: "LOGIN", 
            // location: window.location.pathname
          });
          // sessionStorage.setItem("auth", JSON.stringify(r.data));
          // sessionStorage.setItem("exp", Date.now());// 10000
          // sessionStorage.setItem("user", JSON.stringify(user));
          window.axios.defaults.headers.common.Authorization = "Bearer " + token;
          localStorage.setItem("t", token);
          history.replace("/");// Redirect to Home
          setTimeout(() => setAppConfig({ ...config, user, token }), 1);
        }
        else{
          setErr(error || "Error Login");
        }
      })
      .catch(e => {
        // console.log('e.response: ', e.response);
        let { status, data } = e.response;
        let { errors, message } = data || {};
        if(status === 422 && data){
          console.log('errors', errors);
          // Object.entries(errors).forEach(([key, val]) => {
          //   fn.setFieldError(key === "email" ? "identity" : key, val?.join("\n"));
          // });
          for(let key in errors){
            fn.setFieldError(key === "email" ? "identity" : key, errors?.[key].join("\n"));
          }
        }
        message && setErr(message);
      }).then(() => fn.setSubmitting(false));
    }
  });

	// if(isLog) return <Redirect to="/" />;// OPTION: Check Auth in App.js

  // if(!isLoad){
  //   return (
  //     <Flex dir="column" justify="center" align="center" className="container-fluid py-3 h-full-navmain">
  //       <Placeholder type="thumb" className={CARD_CLASS} />
  //     </Flex>
  //   )
  // }

  const parseLabelIdentity = () => {
    const { identity } = config;
    return identity?.length > 1 ? identity.map((v, i) => (i + 1) === identity.length ? "or " + v : v).join(", ") : identity[0];
  }

// console.log('config: ', config);
  return (
    <Flex dir="column" justify="center" align="center" 
			className="container-fluid py-3 mh-full-navmain"
		>
      <Head title="Login" 
        // (optional) set to false to not use requestAnimationFrame and instead update the DOM as soon as possible. 
        // Useful if you want to update the title when the tab is out of focus 
        // defer={false} 
      >
        {/* <meta name="theme-color" content="#ffffff" /> */}
      </Head>

      {isLoad ? 
        <>
          {err && 
            <div className={"alert alert-danger " + CARD_CLASS}>{err}</div>
          }

          <div className={CARD_CLASS + " p-3 bg-light"}>{/* bg-strip */}
            <h1 className="h4 hr-h mb-3">Login</h1>

            <Form noValidate 
              disabled={formik.isSubmitting} // isProgress
              // Q.Cx(!formik.isValid && "was-validated", formik.isSubmitting && "i-load cwait")
              className={formik.isSubmitting && "i-load cwait"} // formik.isValid ? "" : "was-validated"
              style={formik.isSubmitting ? { '--bg-i-load': '95px' } : null} 
              fieldsetClass="mt-3-next" 
              onSubmit={formik.handleSubmit} 
            >
              <div>{/* form-group | mb-3 */}
                <label htmlFor="identity" className="small mb-1 text-capitalize">
                  {parseLabelIdentity()}
                </label>
                <Input required 
                  className={Q.formikValidClass(formik, "identity")} 
                  // type="text" // email
                  id="identity" // email
                  value={formik.values.identity} 
                  onChange={formik.handleChange} 
                />

                {(formik.touched.identity && formik.errors.identity) && 
                  <div className="invalid-feedback pre-wrap">{formik.errors.identity}</div>
                }
              </div>

              <div>
                <label htmlFor="password" className="small mb-1">Password</label>
                <Password required 
                  className={Q.formikValidClass(formik, "password")} 
                  id="password" 
                  value={formik.values.password} 
                  onChange={formik.handleChange}
                />

                {(formik.touched.password && formik.errors.password) && 
                  <div className="invalid-feedback d-block pre-wrap">{formik.errors.password}</div>
                }
              </div>

              <Flex justify="between" align="center" className="mb-3">
                <div className="custom-control custom-checkbox">
                  <input onChange={formik.handleChange} type="checkbox" className="custom-control-input" id="remember" />
                  <label className="custom-control-label text-sm" htmlFor="remember">
                    Remember me
                  </label>
                </div>

                <Btn type="submit">Login</Btn>
              </Flex>

              <Aroute 
                to="/forgot-password" 
                // kind="secondary" 
                className="text-sm" //  fa fa-question-circle
                // disabled={formik.isSubmitting} 
              > Forgot password?
              </Aroute>
            </Form>
          </div>

          <div className={CARD_CLASS + " p-3 bg-light d-block mt-2 text-center text-sm"}>
            Don't have account? <Aroute to="/register">Register</Aroute>
          </div>
        </>
        : 
        <Placeholder type="thumb" className={CARD_CLASS} />
      }
    </Flex>
  )
}

// export default withRouter(Login);