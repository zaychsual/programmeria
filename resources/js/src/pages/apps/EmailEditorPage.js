import React, { useRef, useState, useEffect } from 'react';// , { useState, useEffect, useRef, } 
import Dropdown from 'react-bootstrap/Dropdown';

import Head from '../../components/q-ui-react/Head';
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
import Img from '../../components/q-ui-react/Img';
import EmailEditor from '../../components/react-email-editor/EmailEditor';
import { fileRead } from '../../utils/file/fileRead';
import LANGS from './lang.json';

// console.log('LANGS: ', LANGS);

export default function EmailEditorPage(){
	const editorRef = useRef(null);
	const [load, setLoad] = useState(false);
	const [data, setData] = useState();
	const [theme, setTheme] = useState("light");// dark
	const [dock, setDock] = useState("right");
	const [locale, setLocale] = useState("en-US");
	
	useEffect(() => {
		console.log('%cuseEffect in EmailEditorPage','color:yellow;');
		// if(!load) setLoad(true);
		setLoad(true);
	}, []);

  // const saveDesign = () => {
  //   editorRef.current.editor.saveDesign((design) => {
  //     console.log('saveDesign', design);
  //     console.log('Design JSON has been logged in your developer console.');
  //   });
	// }
	
	const onZip = zip => {
		const zipFolder = "email-template";
		// zip.file(zipFolder + "/index.html", indexHtml);

    editorRef.current.editor.exportHtml((data) => {
			const { design, html } = data;
			const json = {
				name: zipFolder + ".json",
				txt: JSON.stringify(design, null, 2)
			};

			const jsonMin = {
				name: zipFolder + ".min" + ".json",
				txt: JSON.stringify(design)
			};

			const htmlTmp = {
				name: zipFolder + ".html",
				txt: html // .replace(/\n|\n/gm, "")
			};

			// console.log('exportHtml design: ', design);
			// console.log('exportHtml design: ', JSON.stringify(design, null, 2));
			
			[json, jsonMin, htmlTmp].forEach((v) => {
				zip.file(zipFolder + "/" + v.name, v.txt);
			});
			
			zip.generateAsync({ type: "blob" }).then((blob) => {
				// console.log("zip: ", blob);
				let a = Q.makeEl('a');
				a.href = URL.createObjectURL(blob);
				a.rel = "noopener";
				a.download = zipFolder;
	
				setTimeout(() => a.click(), 1);
				setTimeout(() => {
					URL.revokeObjectURL(a.href);
					a = null;// OPTION
				}, 4E4); // 40s
			});
    });
	}

	const onSave = () => { // exportHtml
		import('jszip').then(JSZip => {
			// console.log('JSZip: ', JSZip);
			if(JSZip && JSZip.default){
				onZip(new JSZip.default());
			}
		});
	}
	
  const onDesignLoad = (data) => {
		console.log('onDesignLoad data: ', data);
		if(!load) setLoad(true);
  }

  const onLoad = () => {
		// console.log('onLoad');
		// if(!load) setLoad(true);

    // you can load your template here;
    // const templateJson = {};
	  // editorRef.current.editor.loadDesign(templateJson);
		
		const editor = editorRef.current?.editor;
		if(editor){
			editor.addEventListener('onDesignLoad', onDesignLoad);
			if(data){
				editor.loadDesign(JSON.parse(data.result));
			}
		}
	}
	
	const onUpload = e => {
		const files = e.target.files;
		if(files.length){
			setLoad(false);

			fileRead(files[0], { readAs: "Text" }).then(res => {
				console.log(res);
				setLoad(true);
				setData(res);
			})
			.catch(e => console.log(e));
		}
	}

	return (
		<Flex dir="column" className={"w-100 h-100" + (theme === "dark" ? " bg-dark":"")}>
			<Head title="Email Editor" />

			<nav className={Q.Cx("navbar py-1 shadow-sm", { [`navbar-${theme} bg-${theme}`]: theme })}>
				<a className="navbar-brand py-1" href="/">
					<Img w="28" h="28" 
						src="/logo/logo-36x36.png" 
						alt="Programmeria" 
						className="d-inline-block mr-2"
					/>
					<strong>Email Editor</strong>
				</a>

				<fieldset className="ml-1-next" disabled={!load}>{/*  className="form-inline" */}
					{data && 
						<div className="btn-group btn-group-sm">
							<Btn kind="light" As="div" className="cauto">{data?.file.name || data.name}</Btn>
							<Btn kind="light" className="qi qi-close xx" />
						</div>
					}
					
					<Dropdown className="d-inline-block">
						<Dropdown.Toggle size="sm" variant={"outline-" + (theme === "dark" ? "light":"dark")} bsPrefix="qi qi-menu tip tipBR" aria-label="Options" />
						<Dropdown.Menu className={theme === "dark" ? "dropdown-menu-dark" : undefined}>
							<Dropdown.Item as="label">
								Upload template json
								<input type="file" accept=".json" hidden 
									onChange={onUpload} 
								/>
							</Dropdown.Item>
							
							<hr />

							<h6 className="dropdown-header">Language</h6>
							<div className="dropdown-item">
								<select className="custom-select custom-select-sm"
									value={locale} 
									onChange={e => {
										setLocale(e.target.value);
										document.body.click();
									}}
								>
									{LANGS.map((v) => <option key={v.code} value={v.code}>{v.name}</option>)}
								</select>
							</div>

							<hr />

							<h6 className="dropdown-header">Theme</h6>
							{["dark", "light"].map((v, i) => 
								<Dropdown.Item key={i} as="div">
									<label className="custom-control custom-radio">
										<input type="radio" className="custom-control-input" 
											name="theme" 
											checked={v === theme} 
											onChange={e => setTheme(e.target.checked ? v : theme)}
										/>
										<div className="custom-control-label">{v}</div>
									</label>
								</Dropdown.Item>
							)}

							<hr />

							<h6 className="dropdown-header">Tools Position</h6>
							{["right", "left"].map((v, i) => 
								<Dropdown.Item key={i} as="div">
									<label className="custom-control custom-radio">
										<input type="radio" className="custom-control-input" 
											name="dock" 
											checked={v === dock} 
											onChange={e => setDock(e.target.checked ? v : dock)}
										/>
										<div className="custom-control-label">{v}</div>
									</label>
								</Dropdown.Item>
							)}
						</Dropdown.Menu>
					</Dropdown>

					<Btn onClick={onSave} size="sm">Save</Btn>{/* Export HTML */}
				</fieldset>
			</nav>

			{load && 
				<EmailEditor
					ref={editorRef} 
					appearance={{
						theme, // light
						panels: {
							tools: {
								dock: dock // "right" // left
							}
						}
					}} 
					locale={locale} // "id-ID" 
					// tools={} 
					onLoad={onLoad} 
					className="d-flex flex1" 
				/>
			}
		</Flex>
	);
}

/*

*/
