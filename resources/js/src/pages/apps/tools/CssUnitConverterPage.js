import React from 'react';// , { useEffect }

import Head from '../../../components/q-ui-react/Head';
import CssUnitConverter from '../../../apps/css-unit-converter/CssUnitConverter';

export default function CssUnitConverterPage(){
	// useEffect(() => {
	// 	console.log('%cuseEffect in CssUnitConverterPage','color:yellow;');
	// 	// if(!load) setLoad(true);
	// 	// setLoad(true);
	// }, []);

  return (
    <div className="py-3">
      <Head title="Css Unit Converter" />

      <CssUnitConverter 

      />
    </div>
  );
}