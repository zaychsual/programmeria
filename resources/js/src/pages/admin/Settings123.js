import React, { useState, useEffect } from 'react';// { useState, useEffect, Fragment } 
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
import Tab from 'react-bootstrap/Tab';
import Nav from 'react-bootstrap/Nav';
import FormBs from 'react-bootstrap/Form';
import { MultiSelect } from 'primereact/multiselect';

import Head from '../../components/q-ui-react/Head';

import Form from '../../components/q-ui-react/Form';
import Btn from '../../components/q-ui-react/Btn';
import Input from '../../components/q-ui-react/Input';
import InputGroup from '../../components/q-ui-react/InputGroup';

export default function Settings(){
	const formikAuth = useFormik({
		enableReinitialize: true, 
		initialValues: {
			identity: ["email"], // [{ name: "email", value: "email" }], 
			passwordMinLength: 6, 
			sessExpired: 1, 
			confirm: "email", 
			fullname: false, 
			username: false, 
		}, 
		validationSchema: Yup.object().shape({
			identity: Yup.array(Yup.string()).min(1, "Select minimum 1").required("is required"),
			passwordMinLength: Yup.number()
				.min(4, "Minimum value 4")
				.integer("Value must integer")
				.required("is required"),
			sessExpired: Yup.number()
				.min(1, "Minimum value 1")
				.integer("Value must integer")
				.required("is required"), 
			confirm: Yup.string().required("is required"), 
			// app_name: Yup.string()
			// 	.min(2, "Minimum 2 symbols")
			// 	.max(50, "Maximum 50 symbols")
			// 	.required("App Name is required"),
			// company_name: Yup.string().max(50, "Maximum 100 symbols"),
			// email: Yup.string()
			// 	.email("Wrong email format")
			// 	.required("Email is required"), 
		}), 
		onSubmit: (values, fn) => {
			console.log('values formikAuth: ', values);
			fn.setSubmitting(false);
		}
	});

	const TABS = ["Info","Authentication","General"];

	return (
		<div className="container-fluid py-3">
			<Head title="Settings App" />
			<h5 className="hr-h hr-left mb-4">Settings App</h5>

			<Tab.Container 
				mountOnEnter 
				id="tabSettingApp" 
				defaultActiveKey="Info" 
				onSelect={key => {
					console.log('onSelect key: ', key);
				}}
			>
				<div className="row">
					<div className="col-md-3">
						<Nav variant="pills" className="flex-column">
							{TABS.map((v) => 
								<Nav.Item key={v}>
									<Nav.Link eventKey={v} className="btn w-100 text-left" {...Q.DD_BTN}>{v}</Nav.Link>
								</Nav.Item>
							)}
						</Nav>
					</div>

					<div className="col-md-9">
						<Tab.Content>
							<Tab.Pane eventKey="Info">
								
							</Tab.Pane>

							<Tab.Pane eventKey="Authentication">
								<div className="row flex-row-reverse">
									<div className="col-md-5 mb-3">
										<div className="card shadow-sm position-sticky t48 zi-1">
											<div className="card-header">Setting your app authentication</div>
											<div className="card-body">

											</div>
										</div>
									</div>

									<div className="col-md-7">
										<Form noValidate 
											className={"card shadow-sm" + (formikAuth.isSubmitting ? " i-load cwait" : "")} 
											disabled={formikAuth.isSubmitting} 
											onSubmit={formikAuth.handleSubmit} 
											// onReset={formikAuth.handleReset} // Q-OPTION
										>
											<div className="card-header bg-light py-2 text-right position-sticky t48 zi-3">
												<Btn type="reset" size="sm" kind="dark">Reset</Btn>{" "}
												<Btn type="submit" size="sm">Save</Btn>
											</div>

											<div className="card-body mt-3-next">
												<div>
													<label>Identity</label>
													<MultiSelect 
														className={"d-flex" + (formikAuth.touched.identity && formikAuth.errors.identity ? " border-danger" : "")} 
														// placeholder="Select a City" 
														display="chip" 
														optionLabel="name" 
														options={[
															{ name: "email", value: "email" }, 
															{ name: "username", value: "username" }, 
															{ name: "phone", value: "phone" }, 
														]} 
														value={formikAuth.values.identity} 
														onChange={(e) => {
															formikAuth.setFieldValue("identity", e.value);
														}} 
													/>
													{(formikAuth.touched.identity && formikAuth.errors.identity) && 
														<div className="invalid-feedback d-block">{formikAuth.errors.identity}</div>
													}
												</div>

												<div>
													<label>Password Minimum</label>
													<Input required 
														className={Q.formikValidClass(formikAuth, "passwordMinLength")} 
														type="number" 
														min={4} 
														inputMode="decimal" 
														id="passwordMinLength" 
														value={formikAuth.values.passwordMinLength} 
														onChange={formikAuth.handleChange} 
													/>
													{(formikAuth.touched.passwordMinLength && formikAuth.errors.passwordMinLength) && 
														<div className="invalid-feedback">{formikAuth.errors.passwordMinLength}</div>
													}
												</div>

												<div>
													<label>Expired</label>
													<InputGroup
														append={
															<select 
																className="custom-select" 
																defaultValue={"Days"} 
																onChange={e => {
																	let val = e.target.value;
																	console.log('val: ', val);
																}}
															>
																{["Hours","Days","Months","Years"].map(v => 
																	<option key={v} value={v}>{v}</option>	
																)}
															</select>
														}
													>
														<Input required 
															className={Q.formikValidClass(formikAuth, "sessExpired")} 
															type="number" 
															min={1} 
															inputMode="decimal" 
															id="sessExpired" 
															value={formikAuth.values.sessExpired} 
															onChange={formikAuth.handleChange} 
														/>
													</InputGroup>
													
													{(formikAuth.touched.sessExpired && formikAuth.errors.sessExpired) && 
														<div className="invalid-feedback d-block">{formikAuth.errors.sessExpired}</div>
													}
												</div>

												<div>
													<label>Register Confirmation</label>
													<select 
														className="custom-select bg-white text-dark o-1" 
														defaultValue={formikAuth.values.confirm} // "email"
														// readOnly 
														disabled 
													>
														{["email","sms","whatsapp"].map(v => 
															<option key={v} value={v}>{v}</option>	
														)}
													</select>
												</div>

												<div>
													<label className="small">Required Full name in Register Process</label>
													<FormBs.Check 
														custom
														type="checkbox" 
														id="fullname"
														label="Full name" 
														checked={Boolean(formikAuth.values.fullname)} 
														onChange={e => formikAuth.setFieldValue("fullname", e.target.checked)}
													/>
												</div>

												<div>
													<label className="small">Required Username in Register Process</label>
													<FormBs.Check 
														custom
														type="checkbox" 
														id="username" 
														label="Username" 
														checked={Boolean(formikAuth.values.username)} 
														onChange={e => formikAuth.setFieldValue("username", e.target.checked)}
													/>
												</div>
											</div>
										</Form>
									</div>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey="General">
								
							</Tab.Pane>
						</Tab.Content>
					</div>
				</div>
			</Tab.Container>

			
		</div>
	);
}

/*
<ListGroup variant="flush">
	<ListGroup.Item action onClick={onModal} type="button">
		Add Page
	</ListGroup.Item>
	<ListGroup.Item action onClick={backUpDb} disabled={loading} type="button">
		Backup database
	</ListGroup.Item>
</ListGroup>
*/
