import React, { useState, useEffect } from 'react';// { useState, useEffect, Fragment } 
import * as Yup from 'yup';
import { useFormik } from 'formik';

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
import Input from '../../components/q-ui-react/Input';
import ModalQ from '../../components/q-ui-react/ModalQ';

const formikMakeTable = ({ 
	initialValues = {
		default: "", // null
		max_length: 9, 
		name: "", // id
		null: false, 
		primary_key: 0, 
		type: "int"
	}, 
	onSubmit
} = {}) => useFormik({
	enableReinitialize: true, 
	initialValues, 
	validationSchema: Yup.object().shape({
		name: Yup.string()
			.min(3, "Minimum 2 symbols")
			.max(20, "Maximum 20 symbols")
			.required("Name is required"), 
			// .matches(/[a-zA-Z]/g, "Only alpha & underscore character")
	}), 
	onSubmit: (values, fn) => {
		console.log('formikMakeTable values: ', values);
		onSubmit(values, fn);
	}
});

export default function SettingPage(){
	const initialValues = { name: "", title: "" };// , suffix: "page"
	const [load, setLoad] = useState({ list: false, form: false });
	const [data, setData] = useState([]);
	const [fields, setFields] = useState(initialValues);
  const [action, setAction] = useState("Add");
	const [modal, setModal] = useState(false);
	const [schema, setSchema] = useState([]);

	useEffect(() => {
		console.log('%cuseEffect in SettingPage','color:yellow;');

  }, []);

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues: fields, 
		validationSchema: Yup.object().shape({
			name: Yup.string()
				.min(2, "Minimum 2 symbols")
				.max(30, "Maximum 30 symbols")
				.required("Name is required"), 
				// .matches(/[a-zA-Z]/g, "Only alpha & underscore character")
			// suffix: Yup.string().max(10, "Maximum 10 symbols"),
			title: Yup.string()
				.min(2, "Minimum 2 symbols")
				.max(100, "Maximum 100 symbols")
				.required("Title is required"), 
		}), 
		onSubmit: (values, fn) => {
			console.log('formik values: ', values);
			// add-page 
			let fd = { 
				...values, 
				name: values.name.trim() + SUFFIX_FIELD_PAGE,// + "_" + values.suffix.trim(), 
				title: values.title.trim()
			};
			delete fd.date;
			delete fd.created_at;

			axios.post("/add-edit-page", Q.obj2formData(fd))
			.then(r => {
				console.log('r: ', r);
				const err = r.data.error;
				if(err){
					if(Q.isObj(err)){
						for(let key in err){
							fn.setFieldError(key, err[key]);
						}
					}else{
						fn.setFieldError("name", err);
					}
				}else{
					setFields(initialValues);// 
					fn.resetForm(initialValues);
					if(action === "Add"){
						setData([ ...data, { ...r.data, date: Q.dateIntl(new Date(), LANG_CODE, {year:'numeric',month:'long',day:'numeric',hour:'numeric',minute:'numeric'}) } ]);
						// setData([ ...data, { ...r.data, created_at: Q.dateIntl(new Date(), LANG_CODE, {year:'numeric',month:'long',day:'numeric',hour:'numeric',minute:'numeric'}) } ]);
					}else{
						setAction("Add");
						setData(data.map(v => (v.id === fd.id ? { ...v, ...fd } : v)));
					}

					swalToast({ icon:"success", text:"Success save page " + fd.name });
				}
			})
			.catch(e => {
				console.log('e: ', e);
				swalToast({ icon:"error", text: "Failed save page", timer: 4000 });
			})
			.then(() => fn.setSubmitting(false));
		}
	});

	const onLoad = () => {
		if(!load.list) setLoad({ ...load, list: true });

		axios.get('/get_all/pages').then(r => {
			const datas = r.data;
			console.log('/get_all/pages datas: ', datas);
			if(datas && !datas.error){
				if(datas.length > 0){
					setData(datas.map(v => ({ ...v, date: Q.dateIntl(v.created_at, LANG_CODE, {year:'numeric',month:'long',day:'numeric',hour:'numeric',minute:'numeric'}) })));
					// setData(datas);
				}else{
					swalToast({ icon:"info", text: "No Data", timer: 5000 });
				}
			}
		})
		.catch(e => console.log('e: ', e))
		.then(() => setLoad({ ...load, list: false }));
	}

	const onClickItem = (val) => {
		setAction("Edit");
		const obj = Q.omit(val, ['date']);
		// console.log('obj: ', obj);
		setFields({ ...obj, name: obj.name.replace(SUFFIX_FIELD_PAGE, "") });//  obj
	}

	const onDelete = (v) => {
		// console.log('onDelete v: ', v);
		Swal.fire({
			icon: 'warning',
			title: 'Are you sure to delete page ' + v.name + '?',
			text: 'Data can not be restored',
			showCloseButton: true,
			allowEnterKey: false,
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then(r => {
			if(r.isConfirmed){
				setLoad({ ...load, list: true });
				axios.delete("/delete_by/pages/id/" + v.id).then(r => {
					if(r.data && !r.data.error){
						setData(data.filter(f => f.id !== v.id));
						swalToast({ icon:"success", text:"Success delete page " + v.name });
					}else{
						swalToast({ icon:"error", text: r.data.error + " page " + v.name });
					}
				})
				.catch(e => {
					console.log('e: ', e);
					swalToast({ icon:"error", text: "Failed delete page " + v.name });
				})
				.then(() => setLoad({ ...load, list: false }));
			}
		});
	}

	const onCancelEdit = () => {
		setAction("Add");
		setFields(initialValues);// formik.resetForm({});
	}

	const onGetDbSchema = () => {
		// /get-db-schema/
		axios.get("/get_where_by/page_schema/name/" + fields.name + SUFFIX_FIELD_PAGE + "/row").then(r => {
			const data = r.data;
			console.log('/get_where_by/page_schema/name/ data: ', data);
			if(data && !data.error && data.value){
				setSchema(JSON.parse(data.value));
				setModal(true);
			}
		}).catch(e => console.log('e: ', e));
	}

	const renderField = (ftype, label, i) => {
		let input;
		switch(ftype){
			case "type":
				input = (
					<select name="type" 
						id={"type" + i} 
						className="custom-select text-uppercase" 
					>
						{[ // 
							{ type:"int", title:"A 4-byte integer, signed range is -2,147,483,648 to 2,147,483,647, unsigned range is 0 to 4,294,967,295" }, 
							{ type:"varchar", title:"A variable-length (0-65,535) string, the effective maximum length is subject to the maximum row size" }, 
							{ type:"text", title:"A TEXT column with a maximum length of 65,535 (2^16 - 1) characters, stored with a two-byte prefix indicating the length of the value in bytes" }, 
							{ type:"date", title:"A date, supported range is 1000-01-01 to 9999-12-31" }, 
							{ label:"Numeric", 
								options: [
									{ type:"tinyint", title:"A 1-byte integer, signed range is -128 to 127, unsigned range is 0 to 255" }, 
									{ type:"smallint", title:"A 2-byte integer, signed range is -32,768 to 32,767, unsigned range is 0 to 65,535" }
								]
							}
						].map(((v, i) => 
							v.options ? // Q.isObj(v)
								<optgroup key={i} label={v.label}>
									{v.options.map((v2, i2) => 
										<option key={i2} value={v2.type} title={v2.title}>{v2.type}</option>
									)}
								</optgroup>
								: 
								<option key={i} value={v.type} title={v.title}>{v.type}</option>
						))}
					</select>
				);
				break;
			default:
				input = (
					<Input  
						
					/>
				);
				break;
		}
// v[0]
		return (
			<div key={i} className="col-2 px-1">
				<label htmlFor={label + i}>{label}</label>

				{input}
			</div>
		);
	}

  return (
		<div className="container py-3">
			<Head title="Setting Page" />
			<h5 className="hr-h hr-left mb-4">Setting Page</h5>

			<div className="row flex-row-reverse">
				<div className="col-md-5 mb-3">
					<div className="card shadow-sm position-sticky t48 zi-1">
						<div className="card-header">
							<Btn onClick={onLoad} size="sm" kind="info" loading={load.list}>
								{data.length > 0 ? "Reload":"Show"} Pages </Btn>
						</div>

						{data.length > 0 && 
							<fieldset disabled={load.list}>
								<div className="list-group list-group-flush">
									{data.map(v => 
										<Flex key={v.id} align="center" className="list-group-item py-1 pr-1" //  list-group-item-action
											title={"Name: " + v.name + "\nTitle: " + v.title + "\nCreated at: " + v.date} 
											// title={v.title} 
											// loading={loading} 
											// onClick={() => onClickItem(v)} // c
										>
											{v.title}
											<Btn onClick={() => onClickItem(v)} size="sm" kind="flat" className="ml-auto qi qi-edit" title="Edit" />
											<Btn onClick={() => onDelete(v)} size="sm" kind="flat" className="qi qi-trash xx" title="Delete" />
										</Flex>
									)}
									
									{/* <div className="card-body">
									</div>*/}
								</div>
							</fieldset>
						}
					</div>
				</div>

				<div className="col-md-7">
					<div className="alert alert-warning">DEVS: Option created_at field, suffix field name</div>
          <Form noValidate // id="formAddTable" 
						className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
						style={formik.isSubmitting ? { '--bg-i-load': '40px' } : null} 
						disabled={formik.isSubmitting} 
						onReset={formik.handleReset} 
						onSubmit={formik.handleSubmit} 
					>
						<Flex align="center" className="card-header bg-light py-2 ml-1-next position-sticky t48 zi-2">
							<h6 className="mb-0 mr-auto">{action} Page</h6>
							{action === "Edit" && 
								<Btn onClick={onCancelEdit} size="sm" kind="warning">Cancel</Btn>
							}
							<Btn type="reset" size="sm" kind="dark">Reset</Btn>
							<Btn type="submit" size="sm">Save</Btn>
						</Flex>

            <div className="card-body">
							<div className="mb-3">
								<label htmlFor="name">Name <small>(Field)</small></label>
								<Input required spellCheck="false" autoComplete="off" 
									id="name" 
									className={Q.formikValidClass(formik, "name")} 
									value={formik.values.name} 
									onChange={formik.handleChange}
								/>

								{(formik.touched.name && formik.errors.name) && 
									<div className="invalid-feedback">{formik.errors.name}</div>
								}
							</div>

							<div className="mb-3">
								<label htmlFor="title">Title <small>(Title page / menu label)</small></label>
								<Input required spellCheck="false" autoComplete="off" 
									id="title" 
									className={Q.formikValidClass(formik, "title")} 
									value={formik.values.title} 
									onChange={formik.handleChange}
								/>

								{(formik.touched.title && formik.errors.title) && 
									<div className="invalid-feedback">{formik.errors.title}</div>
								}
							</div>

							{action === "Edit" && 
								<Btn onClick={onGetDbSchema}>Edit fields</Btn>
							}
            </div>
					</Form>
				</div>
			</div>

			<ModalQ 
				open={modal} 
				position="center" 
				size="xl" 
				// title="" 
				body={
					<div className="table-responsive">
						{schema.map((v1, i1) => 
							<Form key={i1} noValidate 
								// fieldsetClass="mt-3-next" 
								// className={"" + (formikMakeTable.isSubmitting ? " i-load cwait" : "")} 
								// onReset={formikMakeTable.handleReset} 
								// onSubmit={formikMakeTable.handleSubmit} 
							>
								<Flex dir="row" nowrap className="ml-1-next">
									{Object.entries(v1).map((v, i) => 
										renderField(v[0], v[1], i)
									)}
								</Flex>
							</Form>
						)}
					</div>
				}
			/>
		</div>
  );
}

/*
<div className="form-row">
                <div className="col-12 col-lg-8 mb-3">
                  <label htmlFor="name">Name <small>(Field)</small></label>
                  <Input required spellCheck="false" autoComplete="off" 
                    id="name" 
                    className={Q.formikValidClass(formik, "name")} 
                    value={formik.values.name} 
                    onChange={formik.handleChange}
                  />

                  {(formik.touched.name && formik.errors.name) && 
                    <div className="invalid-feedback">{formik.errors.name}</div>
                  }
                </div>
                
                <div className="col-12 col-lg-4 mb-3">
                  <label htmlFor="suffix">Suffix <small>(Suffix field)</small></label>
                  <Input required spellCheck="false" 
                    id="suffix" 
                    className={Q.formikValidClass(formik, "suffix")} 
                    value={formik.values.suffix} 
                    onChange={formik.handleChange}
                  />

                  {(formik.touched.suffix && formik.errors.suffix) && 
                    <div className="invalid-feedback">{formik.errors.suffix}</div>
                  }
                </div>
              </div>

		axios.get("/get-db-schema/" + val.name).then(r => {
			const data = r.data;
			console.log('get-db-schema data: ', data);
			if(data && !data.error){
				const schemas = data.filter(f => (f.name !== "id" && f.type !== "int" && f.primary_key !== 1))
														.map(v => {

														});
				console.log('schemas: ', schemas);
				setValidationSchema(schemas);
			}
		}).catch(e => console.log('e: ', e));

		// axios.get('/get-tables').then(r => {
		// 	const data = r.data;
		// 	console.log('get-tables data: ', data);
		// 	// if(data && !data.error){
				
		// 	// }
		// }).catch(e => console.log('e: ', e));
*/