import React, { useState, useEffect } from 'react';// { useState, useRef, useEffect }

import Head from '../../components/q-ui-react/Head';
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
// import A from '../../components/q-ui-react/A';
import Table from '../../components/q-ui-react/Table';

export default function Visitor(){
	const [load, setLoad] = useState(false);
	const [data, setData] = useState([]);
	const [sortData, setSortData] = useState("Latest");// Oldest 

	useEffect(() => {
		console.log('%cuseEffect in Visitor','color:yellow');
		axios.get('/get_all/visitor').then(r => {
			// console.log('r: ', r);
			const datas = r.data;
			if(datas && !datas.error){// r.status === 200 && 
				let d = datas.map(f => ( { ...f, ua: bowser.parse(f.useragent) } ));
				if(data.length > 1){
					d.reverse();
				}

				// d.sort((a, b) => {
				// 	let idA = Number(a.id);
				// 	let idB = Number(b.id);
				// 	if(idB < idA){ // idA < idB
				// 		return -1;
				// 	}
				// 	if(idB > idA){ // idA > idB
				// 		return 1;
				// 	}
				// 	return 0;
				// });
				// console.log('d: ', d);
				setData(d);
			}
		})
		.catch(e => console.log('e: ',e))
		.then(() => setLoad(true));
	}, []);

	const onDelete = (v) => {
		Swal.fire({
			icon: "question", // warning
			title: "Are you sure to delete this data?",
			// text: "", 
			showCloseButton: true, 
			allowEnterKey: false, 
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes'
		}).then(r => {
			if(r.isConfirmed){
				axios.delete("/delete_by/visitor/id/" + v.id).then(r => {
					if(r.data && !r.data.error){// r.status === 200 && 
            swalToast({ icon:"success", text:"Success delete data visitor" });
            setData(data.filter(f => f.id !== v.id));
          }
				}).catch(e => {
					console.log('e: ', e);
				});
			}
		});
	}

	return (
		<div className="container-fluid py-3">
			<Head title="Visitor" />
			<h5 className="hr-h hr-left mb-3">Visitor</h5>

			<Flex wrap justify="between" align="center" className="py-2 mb-3 bg-white border-bottom position-sticky t48 zi-4">
				<h6 className="m-0">Total Visitor : {data.length}</h6>
				{data.length > 1 && 
					<div className="d-print-none">
						<Btn size="sm" kind="info" 
							onClick={() => {
								setSortData(sortData === "Oldest" ? "Latest":"Oldest");
								const newData = [ ...data ];
								setData(newData.reverse());
							}} 
						>Sort By {sortData}</Btn>

						{/* <div className="input-group input-group-sm">
							<div className="input-group-prepend">
								<label className="input-group-text" htmlFor="sortData">Sort</label>
							</div>
							<select id="sortData">
								{[""]}
							</select>
						</div> */}
						
						{/* <Btn blur onClick={onPrint} size="sm">Print</Btn> */}
					</div>
				}
			</Flex>
			
			{load ? 
				data.length > 0 ? 
					<Table 
						wrapHeight="508px" // {508} 
						fixThead 
						strip 
						hover 
						border 
						sm 
						thead={
							<tr>
								{["No.", "Page", "Status", "Timezone", "User Agent", "IP Address", "Access time", "Action"].map((v, i) => // , "Status", "Levels"
									<th key={i + v} scope="col" className={v === "Action" ? "d-print-none" : ""}>{v}</th>
								)}
							</tr>
						}
						tbody={
							data?.map((v, i) => 
								<tr key={i}>
									<th scope="row">{i + 1}</th>
									<td className="px-2">
                    <a href={Q.baseURL + v.page} target="_blank">{v.page}</a>
                  </td>
									<td>{v.status}</td>
									<td>{v.timezone}</td>
									<td>
										<ul className="list-unstyled mb-0 text-capitalize">
											{Object.entries(v.ua).map((v2, i2) => 
												<li key={i2}>
													{v2[0]} :
													<ul className="small">
														{Object.entries(v2[1]).map((v3, i3) => 
															<li key={i3}>{v3[0]} : {v3[1]}</li>
														)}
													</ul>
												</li>
											)}
										</ul>
									</td>
									<td>{v.ip_address}</td>
									<td>{Q.dateIntl(new Date(v.created_at), "en", {year:'numeric',month:'long',day:'numeric',hour:'numeric',minute:'numeric',second:'numeric'})}</td>
									<td className="d-print-none" style={{ width: 100 }}>
										<Btn onClick={() => onDelete(v)} kind="danger" size="xs">Delete</Btn>
									</td>
								</tr>
							)
						}
					/>
					: 
					<div className="alert alert-info">No Data</div>
				: 
				<div>LOADING</div>
			}
		</div>
	);
}
