import React, { useState, lazy, Suspense } from 'react';// { useState, useEffect, Fragment } 
import { Switch, Route, Redirect } from 'react-router-dom';
import Dropdown from 'react-bootstrap/Dropdown';

import Head from '../../components/q-ui-react/Head';
import PageLoader from '../../components/PageLoader';
import RouteLazy from '../../components/RouteLazy';
// import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute';
// import Textarea from '../../components/q-ui-react/Textarea';
import { HomeSet } from './set-pages/HomeSet';
import NotFound from '../public/NotFound';

const AboutUsSet = lazy(() => import(/* webpackChunkName: "AboutUsSet" */'./set-pages/AboutUsSet'));
const ContactUsSet = lazy(() => import(/* webpackChunkName: "ContactUsSet" */'./set-pages/ContactUsSet'));

const ROOT_PATH = "/settings-pages";
const PAGES = [
	{ title:"Home", to: ROOT_PATH + "/home" }, 
	{ title:"Tentang Kami", to: ROOT_PATH + "/tentang-kami" }, // , exact:true, strict:true
	{ title:"Hubungi Kami", to: ROOT_PATH + "/hubungi-kami" }
];

export default function SettingsPages(){
	const [page, setPage] = useState(PAGES[0]);
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in SettingsPages','color:yellow;');
	// }, []);

	return (
		<div className="container py-3">
			<Head title="Settings Pages" />
			<h5 className="hr-h hr-left mb-4">Settings Pages</h5>

			<Dropdown>
				<Dropdown.Toggle size="sm" variant="light">{page.title}</Dropdown.Toggle>
				<Dropdown.Menu>
					{PAGES.map((v, i) => 
						<Aroute key={i} dropdown to={v.to}>{v.title}</Aroute>
					)}
				</Dropdown.Menu>
			</Dropdown>

			<div className="row">
				<div className="col-md-3">
					<div className="nav flex-column nav-pills link-sm position-sticky t48 zi-3">
						{PAGES.map((v, i) => 
							<Aroute key={i} nav to={v.to}>{v.title}</Aroute>
						)}
					</div>
				</div>

				<div className="col-md-9">
					<div className="tab-content">
						<Suspense 
							fallback={<PageLoader top right bottom left />}
						>
							<Switch>
								<Route path={ROOT_PATH + "/home"} component={HomeSet} />
								<RouteLazy path={ROOT_PATH + "/tentang-kami"} component={AboutUsSet} />
								<RouteLazy path={ROOT_PATH + "/hubungi-kami"} component={ContactUsSet} />

								<Redirect exact from={ROOT_PATH} to={ROOT_PATH + "/home"} />

								<Route path="*" component={NotFound} />
							</Switch>
						</Suspense>
					</div>
				</div>
			</div>
		</div>
	);
}
