import React, { useState } from 'react';// { useState, useEffect, Fragment } 
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
// import Dropdown from 'react-bootstrap/Dropdown';

import Head from '../../components/q-ui-react/Head';
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
import Textarea from '../../components/q-ui-react/Textarea';
// import { confirm } from '../../components/react-confirm/util/confirm';// , confirmComplex

const scroll2Down = () => {
	window.scrollTo({
		top: document.documentElement.scrollHeight,
		left: 0,
		behavior: 'smooth'
	});
}

const initValues = {
	app_name: "",
	company_name: "",
	email: "", 
	address: "", 
	phone: "", 
	fax: "", 
	// social_media: "", 
	// theme: "#e3f2fd", 
	description: ""
};

const SOCIAL_MEDIA = ["facebook", "instagram", "twitter", "youtube", "whatsapp", "telegram"];
const INIT_SOCIAL = { name: "", url: "", icon: "share-alt", id: Q.Qid() };//  | instagram

export default function Settings(){
	const [vals, setVals] = useState(initValues);
	const [socials, setSocials] = useState([INIT_SOCIAL]);
	const [errorSocial, setErrorSocial] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Settings','color:yellow;');
	// }, []);

	const validationSchema = Yup.object().shape({
		app_name: Yup.string()
			.min(2, "Minimum 2 symbols")
			.max(50, "Maximum 50 symbols")
			.required("App Name is required"),
			// .required(
			//   intl.formatMessage({
			//     id: "AUTH.VALIDATION.REQUIRED_FIELD",
			//   })
			// ),
		email: Yup.string()
			.email("Wrong email format")
			.required("Email is required"), 
		// theme: Yup.string()
		// 	.required("Theme is required")
	});

	const formik = useFormik({
		initialValues: vals, 
		validationSchema,  
		onSubmit: (values, fn) => {
			const invalidSocial = socials.find(f => f.name.length < 1 || f.url.length < 1);// filter
			console.log('invalidSocial: ', invalidSocial);

			if(invalidSocial){ // .length > 0
				setErrorSocial("Please insert socials name & url.");
				// fn.setFieldError("social_media", "Please insert social name & url.");
				fn.setSubmitting(false);
				scroll2Down();
			}else{
				// fn.setSubmitting(true);
				// let vals = {
				// 	...values, 
				// 	// social_media: SocialVal.length > 0 ? JSON.stringify(SocialVal) : null
				// };

				// if(socials.length > 0){
				// 	vals.social_media = JSON.stringify(socials.map(f => ({ name: f.name, url: f.url, icon: f.name.toLowerCase() })));
				// }

				console.log('values: ', values);
				// console.log('vals: ', vals);
				// console.log('socials: ', JSON.stringify(socials));
				let sosMed = [];
				if(socials.length > 0){
					// JSON.stringify(socials.map(f => ({ name: f.name, url: f.url, icon: f.name.toLowerCase() })));
					socials.forEach(f => {
						sosMed.push({ name: f.name, url: f.url, icon: f.icon });
						// axios.post(Q.baseURL + "/api/setting_social", Q.obj2formData(sos)).then(r => {
						// 	console.log('r: ', r);
						// }).catch(e => {
						// 	console.log('e: ', e);
						// });
					});
				}
				console.log('sosMed: ', sosMed);

				// axios.post(Q.baseURL + "/api/setting_app", Q.obj2formData(values)).then(r => {
				// 	console.log('r: ', r);
				// 	if(r.status === 200){
				// 		if(r.data.error){
				// 			console.log('error');
				// 			Swal.fire({
				// 				title: "Error", 
				// 				text: r.data.error, 
				// 				icon: "danger",  
				// 				confirmButtonText: "Try again"
				// 			});
				// 		}else{
				// 			if(socials.length > 0){
				// 				// JSON.stringify(socials.map(f => ({ name: f.name, url: f.url, icon: f.name.toLowerCase() })));
				// 				socials.forEach(f => {
				// 					const sosName = f.name.toLowerCase();
				// 					const sos = { name: f.name, url: f.url, icon: SOCIAL_MEDIA.includes(sosName) ? sosName : "share-alt" };
				// 					axios.post(Q.baseURL + "/api/setting_social", Q.obj2formData(sos)).then(r => {
				// 						console.log('r: ', r);
				// 					}).catch(e => {
				// 						console.log('e: ', e);
				// 					});
				// 				});
				// 			}else{
				// 				setVals(values);
				// 				Swal.fire({
				// 					icon: "success", 
				// 					position: "top", 
				// 					text: "Success save settings.", 
				// 					showConfirmButton: false, 
				// 					toast: true, 
				// 					timer: 2500
				// 				});
				// 			}
				// 		}
				// 	}
				// }).catch(e => {
				// 	console.log('e: ', e);
				// 	Swal.fire({
				// 		title: "Error", 
				// 		text: "Failed save setting.", 
				// 		icon: "danger",  
				// 		confirmButtonText: "Try again"
				// 	});
				// }).then(() => fn.setSubmitting(false));

				// setTimeout(() => {
				// 	console.log("values: ", values); // JSON.stringify(values, null, 2)
				// 	// setVals(values);
				// 	fn.setSubmitting(false);
				// }, 3000);
			}
		},
	});

	const onAddSocial = () => {
		const id = Q.Qid();
		setSocials([ ...socials, { ...INIT_SOCIAL, id } ]);
		setTimeout(() => {
			scroll2Down();
			setTimeout(() => Q.domQ("#" + id)?.focus(), 200);
		}, 9);
	}

	const onRemoveSocial = (item) => {
		setSocials(socials.filter(f => f.id !== item.id));
		if(errorSocial){ // formik.errors.social_media
			// formik.setFieldValue("social_media", "");
			setErrorSocial(null);
		}
	}

	const onChangeSocialName = (e, item) => {
		const name = e.target.value;
		const newVal = socials.map(f => {
			if(item.id === f.id){
				// const ico = SOCIAL_MEDIA.includes(val.toLowerCase()) ? { icon: val.toLowerCase() } : { icon: "share-alt" };
				const ico = name.toLowerCase();
				return { 
					...f, 
					name, 
					icon: SOCIAL_MEDIA.includes(ico) ? ico : "share-alt"
				}
			}
			return f;
		});

		setSocials(newVal);
	}

	const onChangeSocialUrl = (e, item) => {
		// const val = e.target.value;
		// const newVal = socials.map(f => {
		// 	if(item.id === f.id){
		// 		return { ...f, url: e.target.value, ...ico }
		// 	}
		// 	return f;
		// });

		setSocials(socials.map(f => ( item.id === f.id ? { ...f, url: e.target.value } : f )));
	}

	return (
		<div className="container py-3">
			<Head title="Settings App" />
			<h5 className="hr-h hr-left mb-4">Settings App</h5>

			<div className="row flex-row-reverse">
				<div className="col-md-5">
					<div className="card bg-light shadow-sm">
						<div className="card-header">Header</div>
  					<div className="card-body">
							Info
						</div>
					</div>
				</div>

				<div className="col-md-7">
					<form noValidate 
						className={"card shadow-sm" + (formik.isSubmitting ? " i-load" : "")} 
						onSubmit={formik.handleSubmit} 
						onReset={formik.handleReset} 
					>
						<fieldset disabled={formik.isSubmitting}>
							<div className="card-header bg-light py-2 position-sticky t48 zi-1">
								<Btn type="reset" size="sm" kind="dark">Reset</Btn>{" "}
								<Btn type="submit" size="sm">Save</Btn>
							</div>

							<div className="card-body">
								<div className="form-group">
									<label htmlFor="app_name">App Name</label>
									<input type="text" 
										className={"form-control" + Q.formikValidClass(formik, "app_name")} 
										id="app_name" 
										required 
										// value={formik.values.app_name} 
										// onChange={formik.handleChange} 
										{...formik.getFieldProps("app_name")} 
									/>
									{/* <small className="form-text text-muted">We'll never share your email with anyone else.</small> */}
									{(formik.touched.app_name && formik.errors.app_name) && 
										<div className="invalid-feedback">{formik.errors.app_name}</div>
									}
								</div>

								<div className="form-group">
									<label htmlFor="company_name">Company Name</label>
									<input type="text" 
										className={"form-control" + Q.formikValidClass(formik, "company_name")} 
										id="company_name" 
										{...formik.getFieldProps("company_name")} 
									/>
								</div>

								<div className="form-group">
									<label htmlFor="email">Email</label>
									<input type="email" 
										className={"form-control" + Q.formikValidClass(formik, "email")} 
										id="email" 
										required 
										{...formik.getFieldProps("email")} 
									/>

									{(formik.touched.email && formik.errors.email) && 
										<div className="invalid-feedback">{formik.errors.email}</div>
									}
								</div>

								<div className="form-group">
									<label htmlFor="address">Address</label>
									<Textarea 
										id="address" 
										className={Q.formikValidClass(formik, "address")} 
										{...formik.getFieldProps("address")} 
									/>
								</div>

								<div className="form-group">
									<label htmlFor="phone">Phone</label>
									<input type="tel" 
										className={"form-control" + Q.formikValidClass(formik, "phone")} 
										id="phone" 
										{...formik.getFieldProps("phone")} 
									/>
								</div>

								<div className="form-group">
									<label htmlFor="fax">Fax</label>
									<input type="tel" 
										className={"form-control" + Q.formikValidClass(formik, "fax")} 
										id="fax" 
										{...formik.getFieldProps("fax")} 
									/>
								</div>

								<div className="form-group">
									<label htmlFor="description">Description</label>
									<Textarea 
										id="description" 
										className={Q.formikValidClass(formik, "description")} 
										{...formik.getFieldProps("description")} 
									/>
								</div>

								<div className="form-group">
									<Flex className="mb-2">
										Social Media
										<Btn size="xs" className="qi qi-plus ml-auto"
											onClick={onAddSocial}
										>Add</Btn>
									</Flex>

									{socials.map((v, i) => 
										<div key={v.id} className="input-group mb-3">
											<div className="input-group-prepend">
												{/* (v.icon ? v.icon : v.name.length > 0 ? v.name.toLowerCase() : "share-alt") */}
												<label htmlFor={v.id} className={"input-group-text w-40px i-color qi qi-" + v.icon} />
												{/* <Dropdown>
													<Dropdown.Toggle variant="light" />
													<Dropdown.Menu>
														<Dropdown.Item {...Q.DD_BTN}>Action</Dropdown.Item>
													</Dropdown.Menu>
												</Dropdown> */}
											</div>

											<input type="text" className="form-control" 
												id={v.id} 
												list={v.id + i}
												value={v.name} 
												onChange={e => onChangeSocialName(e, v)} // onChangeSocial(e, v, "name")
												// onFocus={e => {
												// 	console.log(e.target.nextElementSibling);
												// }}
											/>

											<datalist id={v.id + i}>
												{SOCIAL_MEDIA.map((v, i) => <option key={i} value={v} />)}
											</datalist>

											<input type="url" className="form-control" 
												value={v.url} 
												onChange={e => onChangeSocialUrl(e, v)} // onChangeSocial(e, v, "url")
											/>

											<div className="input-group-append">
												<Btn onClick={() => onRemoveSocial(v)} kind="light" className="qi qi-close xx" title="Remove" />
											</div>
										</div>
									)}

									{errorSocial && 
										<div className="invalid-feedback d-block">{errorSocial}</div>
									}

									{/* {(formik.touched.social_media && formik.errors.social_media) && 
										<div className="invalid-feedback d-block">{formik.errors.social_media}</div>
									} */}
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	);
}
