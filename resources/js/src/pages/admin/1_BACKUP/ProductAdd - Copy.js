import React, { useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import { useHistory } from 'react-router-dom';
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
import Dropdown from 'react-bootstrap/Dropdown';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
// import htmlToDraft from 'html-to-draftjs';
// import DOMPurify from 'dompurify';

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Btn from '../../components/q-ui-react/Btn';
import Input from '../../components/q-ui-react/Input';
// import Textarea from '../../components/q-ui-react/Textarea';
import Flex from '../../components/q-ui-react/Flex';
import Img from '../../components/q-ui-react/Img';
import NumberFormat from '../../components/react-number-format/NumberFormat';
import { TreeQSelect } from '../../components/TreeQSelect';
import axios from 'axios';

export default function ProductAdd(){
	const history = useHistory();
  const [menus, setMenus] = useState([]);
	const [categoryVal, setCategoryVal] = useState("");
	const [publish, setPublish] = useState(true);
	const [img, setImg] = useState();
	const [images, setImages] = useState([]);
	const [merchants, setMerchants] = useState([]);
	const [myMerchants, setMyMerchants] = useState([]);
	const [editorState, setEditorState] = useState(EditorState.createEmpty());
	
	useEffect(() => {
		console.log('%cuseEffect in ProductAdd','color:yellow;');
		axios.post("/get_all/category", Q.obj2formData({ select: "id,name,slug,child" })).then(r => {
			console.log('r: ', r);
			const data1 = r.data;
			if(r.status === 200 && data1 && !data1.error){
				setMenus(data1);
			}
		}).catch(e => {
			console.log('e: ', e);
		});
	}, []);

	const initialValues = {
		name: "", 
		category_id: "", 
		// product_type: "", 
		// code: "", 
		// views: "", 
		// images: "", 
		// price: "", 
		price_currency: "", 
		// description: "", 
	};

	const validationSchema = Yup.object().shape({
		name: Yup.string()
			.min(2, "Minimum 2 symbols")
			.max(255, "Maximum 255 symbols")
			.required("Name is required"),
		category_id: Yup.string().required("Kategori is required"), 
		price_currency: Yup.string().required("Harga is required"), 
	});

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues, 
		validationSchema, 
		onSubmit: (values, fn) => {
			// const { category_id } = categoryVal;
			let newVal = {
				...values, 
				...categoryVal, 
				slug: Q.str2slug(values.name), 
				publish: publish ? 1 : 0, 
				price: parseFloat(values.price_currency.replace(/Rp |\./g, '')), 
				created_by: USER_DATA[0].id
			};

			if(img){
				newVal.file = img;
			}

			const draftVal = editorState.getCurrentContent();
			if(draftVal.hasText()){// DOMPurify.sanitize(html)
				newVal.description = draftToHtml(convertToRaw(draftVal)).trim();
			}

			let fd = Q.obj2formData(newVal);

			// let fdImages = new FormData();
			if(images.length > 0){
				images.forEach(f => {
					fd.append('files[]', f);
				});
				// for(let pair of fdImages.entries()){
				// 	console.log(pair[0]+ ': ', pair[1]);
				// }
			}

			console.log('values: ', values);
			console.log('newVal: ', newVal);
			console.log('img: ', img);
			console.log('images: ', images);

			axios.post("/add-product", fd, {
				headers: {'Content-Type': 'multipart/form-data'}
			}).then(r => {
				console.log('add-product r: ', r);
				// const data = r.data;
				// if(r.status === 200 && data && !data.error){
				// 	swalToast({ icon:"success", text:"Success save product " + newVal.name + "." });

				// 	fn.resetForm(initialValues);
				// 	// fn.setSubmitting(false);
				// }else{
				// 	const fieldErr = data.error.toLowerCase().split(" ").shift();
				// 	// console.log('fieldErr: ', fieldErr);
				// 	if(fieldErr && Object.keys(newVal).includes(fieldErr)){
				// 		fn.setFieldError(fieldErr, "Produk " + data.error);
				// 	}
				// }

				history.push("/product/data");
			}).catch(e => {
				console.log('e: ', e);
			}).then(() => fn.setSubmitting(false));
		}
	});

	const onToggleTree = (open, v) => {
		if(open && !v.children){
			axios.post("/get_where_by/product_type/parent/" + v.id, Q.obj2formData({ select: "id,name,slug,parent" })).then(r => {
				// console.log('r: ', r);
				const datas = r.data;
				if(r.status === 200 && datas && !datas.error){
					setMenus(menus.map(f => ( f.id === v.id ? { ...f, children: datas } : f )));
				}
			}).catch(e => {
				console.log('e: ', e);
			});
		}
	}

	const onSetCategory = (v) => {
		console.log('onSetCategory v: ', v);
		const val = v.parent ? v.parent_name + ", " + v.product_type_name : v.name;
		formik.setFieldValue("category_id", val);
		// formik.setFieldValue("product_type", v.parent ? v.product_type_id : v.id);
		setCategoryVal(v.parent ? { category_id: v.parent_id, category_slug: v.parent_slug, category_name: v.parent_name, product_type_id: v.product_type_id, product_type_slug: v.product_type_slug, product_type_name: v.product_type_name } : { category_id: v.id, category_slug: v.slug, category_name: v.name });
	}

	const onChangeImgFile = (e) => {
		const file = e.target.files[0];
		// const file = et.files[0];
		if(file){
			setImg(file);
			// setModalData({ ...modalData, file });
			// if(errorImg) setErrorImg(null);
		}
	}

	const onChangeImgesFile = (e) => {
		const files = e.target.files;
		if(files && files.length > 0){
			let dataImg = [];
			for(let i = 0; i < files.length; i++) {
				// const file = files[i];
				// if (!file.type.startsWith('image/')){ continue }
				dataImg.push(files[i]);
			}
			// console.log('dataImg: ', dataImg);
			setImages([ ...images, ...dataImg ]);
		}
	}

	const onGetMerchants = open => {
		if(open && merchants.length < 1){
			axios.get("/get_all/merchants").then(r => {
				console.log('r: ', r);
				if(r.status === 200 && r.data && !r.data.error){
					setMerchants(r.data);
				}
			}).catch(e => {
				console.log('e: ', e);
			});
		}
	}

	return (
		<div className="container-fluid py-3">
			<Head title="Add Product" />
			<h5 className="hr-h hr-left mb-3">Add Product</h5>

			<div className="row">
				<div className="col-md-4">
					<Form	noValidate 
						className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
						style={formik.isSubmitting ? { '--bg-i-load': '99px' } : null} 
						disabled={formik.isSubmitting} 
						onSubmit={formik.handleSubmit} 
						onReset={formik.handleReset}
					>
						{img && 
							<div className="position-absolute r0 m-1">
								<Btn As="label" htmlFor="imgFile" size="sm" kind="light">Change</Btn>{" "}
								<Btn size="sm" kind="danger" className="qi qi-close" title="Remove" />
							</div>
						}
						<label className="d-block mb-0 cpoin">						
							<input type="file" accept="image/*" hidden // required 
								id="imgFile" 
								onChange={onChangeImgFile} 
							/>
							<Img 
								className="card-img-top" 
								alt={img ? img.name : "Add Product Image"} 
								src={img ? window.URL.createObjectURL(img) : ""} 
								onLoad={e => {
									if(img){
										window.URL.revokeObjectURL(e.target.src);
									}
								}}
							/>
						</label>

						<div className="card-body py-2 mt-3-next">
							<div>
								<Flex className="mb-2">
									Images 
									<Btn As="label" size="sm" className="btnFile ml-auto qi qi-plus" title="Add Images">
										<input type="file" accept="image/*" hidden multiple 
											id="images" 
											onChange={onChangeImgesFile} 
										/>
									</Btn>
								</Flex>
								{images.length > 0 && 
									<div className="wrap-scroller">
										<Flex nowrap className="ovxauto scroller">
											{images.map((v, i) => 
												<Img key={i} 
													thumb 
													w={75} 
													// h={50} 
													className="flexno czoomin" 
													alt={v.name || "Images " + i} 
													src={v.name ? window.URL.createObjectURL(v) : ""} 
													onLoad={e => {
														if(v){
															window.URL.revokeObjectURL(e.target.src);
														}
													}}
													onClick={(e) => {
														const et = e.target;
														Swal.fire({
															title: et.alt, 
															allowEnterKey: false, 
															showCloseButton: true, 
															showConfirmButton: false, 
															imageUrl: window.URL.createObjectURL(v), // v.name ? window.URL.createObjectURL(v) : et.src, 
															imageHeight: 400, 
															imageAlt: et.alt,
															didOpen(){
																const img = Swal.getImage();
																if(img) window.URL.revokeObjectURL(img.src);
															}
														});
													}}
												/>
											)}
										</Flex>
									</div>
								}
							</div>

							<div>
								<label htmlFor="name">Name</label>
								<Input isize="sm" required 
									id="name" 
									className={Q.formikValidClass(formik, "name")} 
									// {...formik.getFieldProps("name")} 
									value={formik.values.name} 
									onChange={formik.handleChange} 
									// onBlur={formik.handleBlur} 
								/>

								{(formik.touched.name && formik.errors.name) && 
									<div className="invalid-feedback">{formik.errors.name}</div>
								}
							</div>

							<div>
								<label htmlFor="category_id">Category</label>
								<Dropdown>
									<Dropdown.Toggle variant="fff" // tabIndex="0" 
										className={"form-control form-control-sm nbsp d-flex justify-content-between align-items-center cpoin" + Q.formikValidClass(formik, "category_id")} 
									>
										{formik.values.category_id} 
									</Dropdown.Toggle>
									
									<Dropdown.Menu className="bg-clip-inherit p-2 w-100 mxh-50vh ovxhide mt-1-next">
										{menus.length > 0 && 
											menus.map((v, i) => 
												v.child === "1" ? 
													<TreeQSelect key={i} 
														className="pl-3" 
														label={v.name} 
														labelProps={{
															kind: "light", 
															size: "sm", 
															className: "d-flex justify-content-between align-items-center w-100 text-left dropdown-toggle", 
															title: v.name
														}} 
														onToggle={(open) => onToggleTree(open, v)} 
													>
														{v.children && 
															<div className="btn-group-sm btn-group-vertical w-100 pt-1">
																{v.children.map((v2, i2) => 
																	<Dropdown.Item key={i2} {...Q.DD_BTN} bsPrefix="btn btn-sm btn-light w-100 text-left" title={v2.name}
																		onClick={() => {
																			const omit = Q.omit({ 
																				...v, 
																				parent_id: v.id, 
																				parent_name: v.name, 
																				parent_slug: v.slug, 
																				...v2, 
																				product_type_id: v2.id, 
																				product_type_name: v2.name,
																				product_type_slug: v2.slug
																			}, ['id','slug','name']);

																			onSetCategory(omit);
																		}}
																	>{v2.name}</Dropdown.Item>
																)}
															</div>
														}
													</TreeQSelect>
													: 
													<Dropdown.Item key={i} onClick={() => onSetCategory(v)} {...Q.DD_BTN} bsPrefix="btn btn-sm btn-light w-100 text-left" title={v.name}>{v.name}</Dropdown.Item>
											)
										}
									</Dropdown.Menu>
								</Dropdown>

								{(formik.touched.category_id && formik.errors.category_id) && 
									<div className="invalid-feedback d-block">{formik.errors.category_id}</div>
								}
							</div>

							<div>
								<label htmlFor="price_currency">Price</label>
								<NumberFormat 
									thousandSeparator="." 
									decimalSeparator="," 
									prefix="Rp " 

									id="price_currency" 
									inputMode="numeric" // decimal
									autoComplete="off" 
									className={"form-control form-control-sm" + Q.formikValidClass(formik, "price_currency")} // text-right
									// {...formik.getFieldProps("price_currency")} 
									value={formik.values.price_currency} 
									onChange={formik.handleChange} 
									// onBlur={formik.handleBlur} 
								/>

								{(formik.touched.price_currency && formik.errors.price_currency) && 
									<div className="invalid-feedback">{formik.errors.price_currency}</div>
								}
							</div>

							<div>
								<label>Merchants</label>
								<Dropdown 
									drop="right" 
									onToggle={onGetMerchants}
								>
									<Dropdown.Toggle variant="fff" 
										className={"form-control form-control-sm nbsp d-flex justify-content-between align-items-center cpoin"} 
									>
										{myMerchants.length > 0 && myMerchants.map(f => f.name).join(", ")}
									</Dropdown.Toggle>
									<Dropdown.Menu className="w-100">
										{merchants.map((v, i) => 
											<label key={i} className="dropdown-item">
												<div className="custom-control custom-checkbox">
													<input type="checkbox" className="custom-control-input" 
														id={v.name + i} 
														// value="1" 
														// checked={publish} 
														onChange={e => {
															const mer = e.target.checked ? [ ...myMerchants, v ] : myMerchants.filter(f => f.id !== v.id);
															setMyMerchants(mer);
														}} 
													/>
													<div className="custom-control-label">{v.name}</div>
												</div>
											</label>
										)}
									</Dropdown.Menu>
								</Dropdown>

								{myMerchants.map((v, i) => 
									<div key={i} className="mt-2 small">
										{v.name} 
										<Input isize="sm" type="url" className="mt-1" autoFocus 
											value={v.domain} 
											onChange={e => setMyMerchants(myMerchants.map(f => (f.id === v.id ? { ...f, domain: e.target.value } : f ) ))}
										/>
									</div>
								)}
							</div>

							<div className="custom-control custom-checkbox mb-3">
								<input type="checkbox" className="custom-control-input" 
									id="publish" 
									value="1" 
									checked={publish} 
									onChange={e => setPublish(e.target.checked)} 
								/>
								<label className="custom-control-label" htmlFor="publish">Publish</label>
							</div>

							<div className="text-right">
								<Btn type="reset" size="sm" kind="dark">Reset</Btn>{" "}
								<Btn type="submit" size="sm">Save</Btn>
							</div>
						</div>
					</Form>
				</div>

				<div className="col-md-8">
					<div className="form-group">
						<label>Description</label>
						<Editor 
							editorState={editorState} 
							// toolbarHidden 
							toolbar={{
								options: [
									'inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 
									'link', 'remove', 'history', // , 'emoji', 'embedded', 'colorPicker', 'image'
								],
								inline: {
									inDropdown: false, 
									// className: undefined, 
									// component: undefined,
									// dropdownClassName: undefined, 
									// , 'subscript'
									options: ['bold', 'italic', 'underline', 'strikethrough', 'monospace', 'superscript'], 
									// bold: { className: "tip tipT", "aria-label": "Bold" }, // icon: bold, 
									// italic: { icon: italic, className: undefined },
									// underline: { icon: underline, className: undefined },
									// strikethrough: { icon: strikethrough, className: undefined },
									// monospace: { icon: monospace, className: undefined },
									// superscript: { icon: superscript, className: undefined },
									// subscript: { icon: subscript, className: undefined },
								}, 
								// blockType: { className: "text-decoration-none" }, 
								fontSize: { dropdownClassName:"q-scroll" }, 
							}}
							toolbarClassName="position-relative bg-light" 
							wrapperClassName="shadow-sm" 
							editorClassName="border mt-n1px px-3 rounded-bottom ctext" 
							editorStyle={{ minHeight: 'calc(100vh - 226px)' }} 
							// localization={{
							// 	locale: "id", 
							// }}
							onEditorStateChange={(state) => {
								setEditorState(state);
							}}
						/>

						{/* <Textarea 
							readOnly 
							value={draftToHtml(convertToRaw(editorState.getCurrentContent()))} 
						/> */}
					</div>
				</div>
			</div>
		</div>
	);
}

/* <select required 
	id="category" 
	className={"custom-select" + Q.formikValidClass(formik, "category")} 
	{...formik.getFieldProps("category")} 
>
	<option value="">Choose Kategori</option>
	<option value="hell 1">Hell 1</option>
	<option value="hell 2">Hell 2</option>
</select> */
