import React, { useState, lazy, Suspense } from 'react';// { useState, useEffect, Fragment } 
import { Switch, Route, Redirect } from 'react-router-dom';// useHistory
import Dropdown from 'react-bootstrap/Dropdown';

import Head from '../../components/q-ui-react/Head';
import PageLoader from '../../components/PageLoader';
import RouteLazy from '../../components/RouteLazy';
import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute';
// import Textarea from '../../components/q-ui-react/Textarea';
import { HomeSet } from './set-pages/HomeSet';
import NotFound from '../public/NotFound';

const AboutUsSet = lazy(() => import(/* webpackChunkName: "AboutUsSet" */'./set-pages/AboutUsSet'));
const ContactUsSet = lazy(() => import(/* webpackChunkName: "ContactUsSet" */'./set-pages/ContactUsSet'));

const ROOT_PATH = "/settings-pages";
const PAGES = [
	{ title:"Home", to: ROOT_PATH + "/home" }, 
	{ title:"Tentang Kami", to: ROOT_PATH + "/tentang-kami" }, // , exact:true, strict:true
	{ title:"Hubungi Kami", to: ROOT_PATH + "/hubungi-kami" }
];

export default function SettingsPages(){
	// const history = useHistory();
	const [page, setPage] = useState(PAGES[0].title);
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in SettingsPages','color:yellow;');
	// }, []);

	return (
		<div className="container py-3">
			<Head title="Settings Pages" />
			<h5 className="hr-h hr-left">Settings Pages</h5>

			<Flex className="col col-md-5 px-0 my-4">
				Page : 
				<Dropdown className="flex1 ml-2">
					<Dropdown.Toggle size="sm" variant="light" className="w-100 text-left">{page}</Dropdown.Toggle>
					<Dropdown.Menu className="w-100">
						{PAGES.map((v, i) => 
							<Aroute key={i} dropdown to={v.to}>{v.title}</Aroute>
						)}
					</Dropdown.Menu>
				</Dropdown>
			</Flex>

			<Suspense fallback={<PageLoader top right bottom left />}>
				<Switch>
					<Route path={ROOT_PATH + "/home"} component={HomeSet} />
					<RouteLazy path={ROOT_PATH + "/tentang-kami"} component={AboutUsSet} />
					<RouteLazy path={ROOT_PATH + "/hubungi-kami"} component={ContactUsSet} />
					
					<Redirect exact from={ROOT_PATH} to={ROOT_PATH + "/home"} />

					<Route path="*" component={NotFound} />
				</Switch>
			</Suspense>
		</div>
	);
}

/*
				<select className="custom-select custom-select-sm"
					value={page} 
					onChange={e => {
						setPage(e.target.value);
						history.push(ROOT_PATH + )
					}}
				>
					{PAGES.map((v, i) => 
						<option key={i} value={v.title}>{v.title}</option>
					)}
				</select>
*/