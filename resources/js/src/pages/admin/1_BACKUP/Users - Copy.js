import React, { useRef, useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import { Redirect } from 'react-router-dom';
// import pdfMake from "pdfmake/build/pdfmake";
// import pdfFonts from "pdfmake/build/vfs_fonts";
// import htmlToPdfmake from "html-to-pdfmake";

import Head from '../../components/q-ui-react/Head';
// import Placeholder from '../../components/q-ui-react/Placeholder';
// import Img from '../../components/q-ui-react/Img';// Img | Ava
import Flex from '../../components/q-ui-react/Flex';
import Table from '../../components/q-ui-react/Table';
import Aroute from '../../components/q-ui-react/Aroute';
import Btn from '../../components/q-ui-react/Btn';
import Form from '../../components/q-ui-react/Form';
import ModalQ from '../../components/q-ui-react/ModalQ';
// import NewWindow from '../../components/react-new-window/NewWindow';
import { clipboardCopy } from '../../utils/clipboard';
// import setSrcDoc from '../../utils/setSrcDoc';
// import { APP_NAME } from '../../data/appData';

// pdfMake.vfs = pdfFonts.pdfMake.vfs;

export default function Users(){
	const div = useRef(null);
	const [data, setData] = useState([]);
	const [sortData, setSortData] = useState("Latest");// Oldest 
	const [modal, setModal] = useState({ open: false, load: false });
	const [confirm, setConfirm] = useState("Yes");
	// const [loadActivated, setLoadActivated] = useState(false);
	// const [winOpen, setWinOpen] = useState(null);
	// const [divPrint, setDivPrint] = useState(null);

	useEffect(() => {
		// if(userData && userData.isAdmin){
			// console.log('%cuseEffect in Users','color:yellow;');
			// axios.get("/employee").then(r => {
			// 	console.log('r: ', r);
			// 	if(Array.isArray(r.data) && r.data.length > 0 && !r.data.error){//  && Q.isObj(r.data)
			// 		let d = r.data.filter(v => v.id !== "1");
			// 		if(data.length > 1){
			// 			d.reverse();
			// 		}
			// 		setData(d); // No store user Admin

			// 		// setTimeout(() => {
			// 		// 	setDivPrint(div.current.outerHTML);
			// 		// }, 99);
			// 	}
			// }).catch(e => console.warn(e));
		// }

		axios.get("/users").then(r => {
      console.log('/users r: ', r);
			console.log('/users !r.data.error: ', !r.data.error);
			let data = r.data;
			if(data && !data.error && Array.isArray(data)){ //  && r.data.length > 0 && !r.data.error
				setData(data);
			}
    })
    .catch(e => console.log('e: ', e));
	}, []);

	const onDelete = (v) => {
		console.log('onDelete v: ', v);
		const username = v.username || v.first_name;
		Swal.fire({
			icon: "question", 
			title: "Are you sure to delete user " + username + "?", 
			showCloseButton: true, 
			allowEnterKey: false, 
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes'
		}).then(r => {
			if(r.isConfirmed){
				axios.delete("/delete_user/" + v.id).then(r => {
					// console.log(r);
					if(r.data && !r.data.error){// r.status === 200
						swalToast({ icon:"success", text:"Success delete user"+ username });
						setData(data.filter(f => f.id !== v.id));
						// setTimeout(() => {
						// 	setDivPrint(div.current.outerHTML);
						// }, 99);
					}
				}).catch(e => console.warn(e));
			}
		});
	}

	const onCopy = (txt, e) => {
		let et = e.target;
		clipboardCopy(txt).then(() => {
			Q.setAttr(et, { "aria-label": "Copied!" });
			setTimeout(() => Q.setAttr(et, "aria-label"), 1000);
		}).catch(e => console.warn(e));
	}

	// const onPrint = () => {
	// 	// if(divPrint){
	// 	// 	// const divDom = divPrint.current.outerHTML;
	// 	// 	const htmoDom = setSrcDoc({
	// 	// 		title: APP_NAME + " users",
	// 	// 		body: divPrint, 
	// 	// 		// bcApi: bcApi
	// 	// 	});

	// 	// 	const blob = new Blob([htmoDom], { type: 'text/html' }); // 'text/html'
	// 	// 	const objUrl = window.URL.createObjectURL(blob);
	// 	// 	// console.log('objUrl: ', objUrl);

	// 	// 	setWinOpen(objUrl);
	// 	// }
	// }

	const thead = () => {
		let th = ["No.", "Name", "Username", "Email"]; // , "Forgot Password"
		if(USER_DATA.isAdmin){
			th.push("Active", "Action");
		}
		return th;
	}

	return (			
		<div ref={div} className="container py-3">
			{/* {winOpen && 
				<NewWindow 
					copyCss={false} 
					url={winOpen} 
					features={{ left: 0, top: 0, width: screen.width, height: screen.height }} 
					title="Co-Space users" 
					onUnload={() => setWinOpen(null)}
					onBlock={() => setWinOpen(null)}
				>
				</NewWindow>
			} */}

			<Head title="Users" />
			<h5 className="hr-h hr-left mb-4">Users</h5>

			{/* <div className="d-none d-print-block">
				<img width="40" className="img-fluid" src={ORIGIN + "assets/icon/android-icon-48x48.png"} />
				<h4>{APP_NAME}</h4>
				<hr/>
			</div> */}

			<Flex wrap justify="between" align="center" className="py-2 mb-3 bg-white border-bottom position-sticky t48 zi-4">
				<h6 className="m-0">Total Users : {data.length}</h6>
				<div className="d-print-none">
					{data.length > 1 && 
						<Btn size="sm" kind="info" 
							onClick={() => {
								setSortData(sortData === "Oldest" ? "Latest":"Oldest");
								const newData = [ ...data ];
								setData(newData.reverse());
							}} 
						>Sort By {sortData}</Btn>
					}
					{" "}
					{USER_DATA.isAdmin && 
						<Aroute to="/user/add" btn="info" size="sm" className="qi qi-plus">Add user</Aroute>
					}
					
					{/* /auth/create_group/ */}
					{/* <Aroute to="/user/create-level" btn="info" size="sm" className="mdi mdi-plus">Add level</Aroute>{" "} */}
					{/* <Btn blur onClick={onPrint} size="sm">Print</Btn> */}
				</div>
			</Flex>

			{data.length > 0 ? 
				<Table 
					// responsiveRef={div} 
					// responsiveStyle={{ maxHeight: 'calc(100vh - 170px)' }} 
					fixThead 
					customScroll 
					strip 
					hover 
					border 
					sm 
					thead={
						<tr>
							{thead().map((v, i) => // , "Status", "Levels", "Forgot Password",
								<th key={i + v} className={["Action"].includes(v) ? "d-print-none" : ""}>{v}</th>
							)}
						</tr>
					}
					tbody={
						data.map((v, i) => 
							<tr key={i}>
								<th scope="row">{i + 1}</th>
								<td>{v.name}</td>
								{/* <td>{v.last_name}</td> */}
								<td>
									<Aroute to={"/user/detail/" + v.id}>{v.username}</Aroute>
								</td>
								<td><a href={"mailto:" + v.email}>{v.email}</a></td>
								{/* <td className="ml-1-next">
									{v.groups.map(g => 
										<Aroute key={g.id} to={"/auth/edit_group/" + g.id} btn="primary" size="xs" className="btn-block">{g.name}</Aroute>
									)}
								</td> */}

								{/* <td className="d-print-none">
									{v.forgotten_password_code && 
										<Btn block outline size="xs" className="tip tipTL"
											onClick={e => onCopy(Q.baseURL + "/reset_password/" + v.forgotten_password_code, e)} 
										>Copy</Btn>
									}
								</td> */}

								{USER_DATA.isAdmin && 
									<>
										<td>
											<Btn  
												onClick={() => {
													// setModal({ ...modal, open: true, action: (v.active === "1"  ? "de" : "") + "activate", id: v.id })
													setModal({ ...modal, open: true, action: (v.email_verified_at  ? "de" : "") + "activate", id: v.id })
												}} 
												kind={v.email_verified_at ? "primary":"secondary"} // v.active === "1" ? "primary":"secondary"
												size="xs" 
												className="w-100"
											>
												{/* {v.active === "1" ? "Ina" : "A"}ctive */}
												{v.email_verified_at ? "Ina" : "A"}ctive
											</Btn>
										</td>
										<td className="d-print-none" style={{ width: 100 }}>
											<Btn onClick={() => onDelete(v)} kind="danger" size="xs">Delete</Btn>{" "}
											<Aroute to={"/user/edit/" + v.id} btn="info" size="xs">Edit</Aroute>
										</td>
									</>
								}
							</tr>
						)
					}
				/>
				: 
				<div className="alert alert-info">
					No Data
				</div>
			}

			<ModalQ 
				// returnFocusAfterClose={false} 
				open={modal.open} 
				onHide={() => setModal(!modal)} // toggle
				size="sm" 
				title={modal.action} 
				headProps={{
					className: "text-capitalize"
				}}
				bodyClass="text-center" 
				body={
					<Form 
						className={modal.load ? "cwait" : undefined}
						disabled={modal.load} 
						onReset={() => {
							setModal({ ...modal, open: false });
							setConfirm("Yes");
						}} 
						onSubmit={e => {
							Q.preventQ(e);
							if(confirm === "No"){
								setModal({ load: false, open: false });
							}else{
								setModal({ ...modal, load: true });
							
								axios.post("/" + modal.action + "/" + modal.id, Q.obj2formData({ confirm, id: modal.id }))
								.then(r => {
									console.log('r: ', r);
									if(r.data && !r.data.error){
										setModal({ load: false, open: false });
										setData(data.map(v => v.id === modal.id ? { ...v, active: modal.action === "activate" ? "1" : "0" } : v));
									}else{
										setModal({ ...modal, load: false });
										swalToast({ icon:"error", text:"Failed change active" });
									}
								}).catch(e => {
									console.warn('e: ', e);
									setModal({ ...modal, load: false });
									swalToast({ icon:"error", text:"Failed change active" });
								});
							}
						}}
					>
						{['Yes','No'].map(v => 
							<div key={v} className="custom-control custom-radio custom-control-inline">
								<input id={v} name="confirm" type="radio" className="custom-control-input" 
									checked={v === confirm} 
									onChange={e => setConfirm(e.target.checked ? v : "")} 
								/>
								<label htmlFor={v} className="custom-control-label">{v}</label>
							</div>
						)}

						<div className="mt-3">
							<Btn type="reset" kind="dark">Cancel</Btn>{" "}
							<Btn type="submit">Save</Btn>
						</div>
					</Form>
				}
			/>
		</div>
	);
}

/*
{v.active && v.active === "1" ? // 
	<Aroute to={"/user/deactivate/" + v.id} btn="primary" size="xs">Deactivate</Aroute>
	: 
	<Aroute to={"/user/activate/" + v.id} btn="dark" size="xs">Activate</Aroute>
}
*/
