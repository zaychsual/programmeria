import React, { useRef, useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo, Fragment }
import { useHistory } from 'react-router-dom';
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
// import Dropdown from 'react-bootstrap/Dropdown';

import Head from '../../components/q-ui-react/Head';
import Btn from '../../components/q-ui-react/Btn';
import Flex from '../../components/q-ui-react/Flex';
import Form from '../../components/q-ui-react/Form';
import Input from '../../components/q-ui-react/Input';
import { arrMove } from '../../utils/collection-q';

const scroll2Down = () => {
	window.scrollTo({
		top: document.documentElement.scrollHeight,
		left: 0,
		behavior: 'smooth'
	});
}

export default function Menus(){
	const initValues = {
		name: "", 
		img: "",
	};
	const history = useHistory();
	const P_MENU = Q.urlSearch().get("p");
	const inputRef = useRef();

	const [load, setLoad] = useState(true);// false
	const [err, setErr] = useState(false);
  const [vals, setVals] = useState(initValues);
	const [menus, setMenus] = useState([]);
	const [child, setChild] = useState([]);
	const [childDel, setChildDel] = useState([]);
	const [file, setFile] = useState({});

	const loadData = (cb) => { // 
		axios.get("/get_all/category").then(r => {
			// console.log('r: ', r);
			if(r.data && !r.data.error){// r.status === 200 && 
				setMenus(r.data);
				
				if(cb) cb(r.data);
			}else{
				setErr(true);
			}
		}).catch(e => {
			console.log('e: ', e);
			setErr(true);
		}).then(() => {
			if(!load) setLoad(true);
		});
	}

	useEffect(() => {
		console.log('%cuseEffect in Menus','color:yellow;');
		// loadData();
		// loadData((data) => {
		// 	if(P_MENU){
		// 		setTimeout(() => {
		// 			const getMenu = data.find(f => f.slug === P_MENU);
		// 			if(getMenu) onClickListMenu(getMenu);
		// 			// console.log('history: ',history);
		// 			history.replace(history.location.pathname);
		// 		}, 99);
		// 	}
		// });
	}, []);

	const validationSchema = Yup.object().shape({
		name: Yup.string()
			.min(2, "Minimum 2 symbols")
			.max(50, "Maximum 255 symbols")
			.required("Menu Name is required"),
	});

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues: vals, 
		validationSchema, 
		onSubmit: async (values, fn) => {
			const childLength = child.length;
			const childDelLength = childDel.length;
			let newVal = {
				...values, 
				slug: Q.str2slug(values.name),
				child: childLength > 0 ? 1 : 0
			};

			if(file.name){
				newVal.file = file;
				delete newVal.img;
			}
			if(!newVal.img){
				delete newVal.img;
			}

			// console.log('vals: ', vals);// state to formik values
			console.log('values: ', values);
			console.log('newVal: ', newVal);

			if(newVal.id){// EDIT
				if(JSON.stringify(values) !== JSON.stringify(newVal)){// Compare data to edit
					try {
						const res1 = await axios.post("/set-menu/category", Q.obj2formData(newVal));
						// console.log('%cAdd / Edit parent Menu res1: ', 'color:yellow', res1);
						if(res1.data && !res1.data.error){// res1.status === 200 && 
							// && childDelLength < 1
							if(childLength < 1){// Info here
								swalToast({ icon:"success", text:"Success save menu " + newVal.name });
								setFile({});
								loadData();
								setVals(initValues);
								fn.resetForm(initValues);
								fn.setSubmitting(false);
							}
						}
					}catch(e){
						console.log('e: ', e);
					}
				}
				
				if(childLength > 0){// Add / Edit Child
					child.forEach(async (f, i) => {
						try {
							const r2 = await axios.post("/set-menu/product_type", Q.obj2formData({ ...f, slug: Q.str2slug(f.name), parent: newVal.id }));
							console.log('%cAdd / Edit Child r2: ', 'color:yellow', r2);
							if(i + 1 === childLength && r2.status === 200 && !r2.data.error){
								console.log('%cSuccess Add / Edit Child r2: ', 'color:yellow', r2);
							}
						}catch(e){
							console.log('e: ', e);
						}
					});
				}

				if(childDelLength > 0){// Remove Child
					childDel.forEach(async (f, i) => {
						try {
							const r3 = axios.delete("/delete_by/product_type/id/" + f.id);
							console.log('%cRemove Child r3: ', 'color:yellow', r3);
							if(i + 1 === childDelLength && r3.status === 200 && !r3.data.error){
								console.log('%cSuccess Remove Child r3: ', 'color:yellow', r3);
							}
						}catch(e){
							console.log('e: ', e);
						}
					});
				}
			}
			else{// Add
				axios.post("/set-menu/category", Q.obj2formData(newVal)).then(r => {
					console.log('%cr: ', 'color:yellow', r);
					const data1 = r.data;
					if(data1 && !data1.error){// r.status === 200 && 
						if(childLength > 0){// Add With Child
							axios.get("/get_where_by/category/slug/" + newVal.slug + "/row").then(r2 => {
								const data2 = r2.data;
								console.log('%cr2: ', 'color:yellow', r2);
								if(data2 && !data2.error){// r2.status === 200 && 
									console.log('%cdata2: ', 'color:yellow', data2);
									child.forEach((f, i) => {
										axios.post("/set-menu/product_type", Q.obj2formData({ ...f, slug: Q.str2slug(f.name), parent: data2.id })).then(r3 => {
											console.log('%cr3: ', 'color:yellow', r3);
											if(i + 1 === childLength){
												swalToast({ icon:"success", text:"Success save menu " + newVal.name });
												setFile({});
												loadData();
												setChild([]);
												formik.resetForm(initValues);
												fn.setSubmitting(false);
											}
										}).catch(e => {
											console.log('e: ', e);
											// fn.setSubmitting(false);
										});
									});
								}
							}).catch(e2 => {
								console.log('e2: ', e2);
							});
						}
						else{// Add Without Child
							swalToast({ icon:"success", text:"Success save menu " + newVal.name });
							setFile({});
							loadData();
							formik.resetForm(initValues);
							fn.setSubmitting(false);
						}
					}
				}).catch(e => {
					console.log('e: ', e);
					fn.setSubmitting(false);
				});// .then(() => fn.setSubmitting(false));
			}
		},
	});

	const onAddChild = (parent) => {
		const id = "child-" + Q.Qid();
		setChild([ ...child, { id, name: "", parent } ]);
		setTimeout(() => {
			scroll2Down();
			setTimeout(() => Q.domQ("#" + id)?.focus(), 90);
		}, 9);
	}

	const onChangeChild = (e, item) => {
		setChild(child.map(f => ( item.id === f.id ? { ...f, name: e.target.value } : f )));
	}

	const onClickListMenu = (item) => {
		if(item.id !== vals.id){
			setVals(item);
			if(child.length > 0) setChild([]);
			if(childDel.length > 0) setChildDel([]);

			if(item.child === "1"){
				axios.get("/get_where_by/product_type/parent/" + item.id + "/result").then(r => {
					console.log('%cGet Childs r: ', 'color:yellow', r);
					const datas = r.data;
					if(datas && !datas.error){// r.status === 200 && 
						setChild(datas);
					}
				}).catch(e => {
					console.log('e: ', e);
				});
			}
		}
		inputRef.current.focus();
	}

	const onDelChild = (v, i) => {
		if(vals.id && vals.child === "1"){
			Swal.fire({
				icon: 'warning',
				title: 'Are you sure to delete menu child ' + v.name + '?',
				text: 'Data can not be restored',
				showCloseButton: true,
				allowEnterKey: false,
				showCancelButton: true,
				confirmButtonText: 'Yes',
				cancelButtonText: 'No'
			}).then(r => {
				if(r.isConfirmed){
					setChildDel([ ...childDel, v ]);// For remove child
					setChild(child.filter((f, i2) => i !== i2));
					swalToast({ icon:"success", text:"Success delete menu child " + v.name });
				}
			});
		}else{
			setChild(child.filter((f, i2) => i !== i2));
		}
	}

	const onRemoveParent = (v) => {
		Swal.fire({
			icon: 'warning',
			title: 'Are you sure to delete menu ' + v.name + '?',
			text: 'Data can not be restored',
			showCloseButton: true,
			allowEnterKey: false,
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then(r => {
			if(r.isConfirmed){
				axios.delete("/delete_by/category/id/" + v.id).then(r => {
					// console.log('%cDelete menu r: ', 'color:yellow', r);
					if(r.data && !r.data.error){// r.status === 200 && 
						if(v.child === "1"){// With Child
							axios.get("/get_where_by/product_type/parent/" + v.id + "/result").then(r => {
								// console.log('%cGet Childs r: ', 'color:yellow', r);
								const datas = r.data;
								if(datas && !datas.error){// r.status === 200 && 
									// setChild(datas);
									if(datas.length > 0){
										datas.forEach((f, i) => {
											axios.delete("/delete_by/product_type/id/" + f.id).then(r2 => {
												if(i + 1 === datas.length){
													// console.log('r2: ', r2);
													swalToast({ icon:"success", text:"Success delete menu " + v.name });
												}
											});
										});
									}
								}
							}).catch(e => {
								console.log('e: ', e);
							});
						}
						else{// No Child
							swalToast({ icon:"success", text:"Success delete menu " + v.name });
						}
						
						if(vals.name.length > 0){
							formik.resetForm(initValues);
							setVals(initValues);
							setChild([]);
							setChildDel([]);
							// fn.setSubmitting(false);
						}
						setMenus(menus.filter(f => f.id !== v.id));
					}
				}).catch(e => {
					console.log('e: ', e);
				});
			}
		});
	}

	const onChangeFile = e => {
		const f = e.target.files[0];
		if(f){
			setFile(f);
			formik.setFieldValue("img", f.name);
		}
	}

	const onDetailSwal = () => {
		const fname = file.name || formik.values.img;
		if(fname){
			Swal.fire({
				title: fname, 
				allowEnterKey: false, 
				showCloseButton: true, 
				showConfirmButton: false, 
				imageUrl: file.name ? window.URL.createObjectURL(file) : "public/media/menus/" + formik.values.img, 
				imageHeight: 400, 
				imageAlt: fname, 
				didOpen(){
					if(file.name){
						const img = Swal.getImage();
						if(img) window.URL.revokeObjectURL(img.src);
					}
				}
			});
		}
	}

	return (
		<div className="container py-3">
			<Head title="Settings Menus / Category" />
			<h5 className="hr-h hr-left mb-4">Settings Menus / Category</h5>

			{load ? 
				<div className="row">
					<div className="col-md-4">
						{menus.length > 0 ? 
							<>
								<h6>Choose Menu / Category to edit :</h6>
								<div className="list-group shadow-sm">
									{menus.map((v, i) => 
										<Flex key={i} justify="between" align="center" 
											className={Q.Cx("list-group-item list-group-item-action py-0 pr-3 pl-0 cpoin", { "active": v.id === vals.id })} 
										>
											<div onClick={() => onClickListMenu(v)} className="flex1 py-1 px-3 select-no" title={v.name}>{v.name}</div>
											<Btn onClick={() => onRemoveParent(v)} blur size="xs" kind="light" className="tip tipL qi qi-close xx" aria-label="Remove"  />
										</Flex>
									)}
								</div>
							</>
							: 
							<Flex justify="between" align="center" className={"alert alert-" + (err ? "danger" : "info")}>
								{err ? <>Error load data. <Btn onClick={loadData}>Reload</Btn></> : "No Menu Data"}
							</Flex>
						}
					</div>

					<div className="col-md-8">
						<div className="card bg-light shadow-sm">
							<div className="card-body">
								<Form noValidate 
									className={Q.Cx({ "i-load cwait": formik.isSubmitting })} 
									style={formik.isSubmitting ? { '--bg-i-load': '95px' } : null} 
									disabled={formik.isSubmitting} 
									fieldsetClass="mt-3-next" 
									onSubmit={formik.handleSubmit} 
									onReset={e => {
										formik.handleReset(e);
										setVals(initValues);
										if(child.length > 0) setChild([]);
										if(childDel.length > 0) setChildDel([]);
									}} // Q-OPTION
								>
									<h5 className="hr-h hr-left mb-4">{menus.find(f => f.id === vals.id) ? "Edit" : "Add"} Menu / Category</h5>

									<div>
										<label htmlFor="img">Menu image</label>
										<div className="input-group">
											<Btn As="label" kind="fff" tabIndex="0" className="form-control text-left">
												{file.name || formik.values.img || "Choose image"} 
												<input onChange={onChangeFile} id="img" type="file" accept="image/*" hidden />
											</Btn>
											{(file.name || formik.values.img) && 
												<div className="input-group-append">
													<Btn kind="light" title="Remove" className="qi qi-close xx" 
														onClick={() => {
															setFile({});
															formik.setFieldValue("img","");
														}}
													/>
													<Btn onClick={onDetailSwal} kind="light" title="View" className="qi qi-img" />
												</div>
											}
										</div>
									</div>

									<div>
										<label htmlFor="name">Menu Name</label>
										<div className="input-group">
											<Input required 
												ref={inputRef} 
												id="name"
												className={Q.formikValidClass(formik, "name")} 
												value={formik.values.name} 
												onChange={formik.handleChange} 
											/>
											<div className="input-group-append">
												<Btn blur kind="light" className="tip tipTR qi qi-plus" 
													aria-label="Add Children" 
													onClick={() => onAddChild(vals.id || vals.slug)} // WHY vals.slug
												>Children</Btn>
											</div>
										</div>

										{(formik.touched.name && formik.errors.name) && 
											<div className="invalid-feedback d-block">{formik.errors.name}</div>
										}
									</div>

									{child.length > 0 && 
										<>
											<h6>Children :</h6>
											<ol>
												{child.map((v, i) => 
													<li key={i}>
														<div className="input-group input-group-sm mb-2">
															<div className="input-group-prepend">
																<label htmlFor={v.id} className="input-group-text">Name</label>
															</div>
															<Input 
																id={v.id} 
																value={v.name} 
																onChange={e => onChangeChild(e, v)} 
															/>
															<div className="input-group-append">
																{i > 0 && 
																	<Btn As="b" kind="light" className="tip tipTR qi qi-chevron-up" aria-label="Move Up" 
																		onClick={() => setChild(arrMove(child, i, i - 1))}
																	/>
																}
																
																{(i + 1 !== child.length) && 
																	<Btn As="b" kind="light" className="tip tipTR qi qi-chevron-down" aria-label="Move Down" 
																		onClick={() => setChild(arrMove(child, i, i + 1))}
																	/>
																}

																<Btn As="b" kind="light" className="tip tipTR qi qi-close xx"
																	aria-label="Remove" 
																	onClick={() => onDelChild(v, i)}
																/>
															</div>
														</div>
													</li>
												)}
											</ol>
										</>
									}

									<div className="text-right">
										<Btn type="reset" kind="dark">Reset</Btn>{" "}
										<Btn type="submit">Save</Btn>
									</div>
								</Form>
							</div>
						</div>
					</div>
				</div>
				: 
				<div>LOADING</div>
			}
		</div>
	);
}

