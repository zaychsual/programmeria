import React, { useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
import Collapse from 'react-bootstrap/Collapse';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
// import htmlToDraft from 'html-to-draftjs';
// import axios from 'axios';

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Btn from '../../components/q-ui-react/Btn';
import NumberFormat from '../../components/react-number-format/NumberFormat';
import Textarea from '../../components/q-ui-react/Textarea';

function Tree({
	open = false, // true
	mountOnEnter = true, 
	timeout = 150, 

	onToggle = Q.noop, 
	// kind = "flat", 
	// size = "sm", 
	label,  
	labelProps, 
	children, 
	...etc
}){
	const [see, setSee] = useState(open);

	return (
		<div>
			<Btn {...labelProps} 
				active={see} 
				// kind={kind} 
				// size={size} 
				onClick={(e) => {
					setSee(!see);
					onToggle(!see, e);
				}}
			>
				{label}
			</Btn>

      <Collapse 
				{...etc} 
				in={see}
				mountOnEnter={mountOnEnter} 
				timeout={timeout} 
			>
        <div>{children}</div>
      </Collapse>
		</div>
	)
}

export default function Products(){
  const [menus, setMenus] = useState([]);
	const [editorState, setEditorState] = useState(EditorState.createEmpty());
	
	useEffect(() => {
		console.log('%cuseEffect in Products','color:yellow;');
		// Q.baseURL + "/api
		axios.post("/get_all/category", Q.obj2formData({ select: "id,name,slug,child" })).then(r => {
			console.log('r: ', r);
			const data1 = r.data;
			if(r.status === 200 && data1 && !data1.error){
				// console.log('data1: ', data1);
				setMenus(data1);
			}
		}).catch(e => {
			console.log('e: ', e);
		});
	}, []);

	const initialValues = {
		name: "", 
		category: "", 
		product_type: "", 
		// code: "", 
		// views: "", 
		// images: "", 
		// price: "", 
		priceCurrency: "", 
		// description: "", 
	};

	const validationSchema = Yup.object().shape({
		name: Yup.string()
			.min(2, "Minimum 2 symbols")
			.max(255, "Maximum 255 symbols")
			.required("Name is required"),
			// .required(
			//   intl.formatMessage({
			//     id: "AUTH.VALIDATION.REQUIRED_FIELD",
			//   })
			// ),
		category: Yup.string().required("Kategori is required"), 
		priceCurrency: Yup.string().required("Harga is required"), 
	});

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues, 
		validationSchema, 
		onSubmit: (values, fn) => {
			let newVal = {
				...values, 
				price: parseFloat(values.priceCurrency.replace(/Rp |\./g, '')), 
				description: draftToHtml(convertToRaw(editorState.getCurrentContent()))
			};
			console.log('values: ', values);
			console.log('newVal: ', newVal);
		}
	});

	const onToggleTree = (open, v) => {
		if(open && !v.children){
			// Q.baseURL + "/api
			axios.post("/get_where_by/product_type/parent/" + v.id, Q.obj2formData({ select: "id,name,slug,parent" })).then(r => {
				console.log('r: ', r);
				const datas = r.data;
				if(r.status === 200 && datas && !datas.error){
					setMenus(menus.map(f => ( f.id === v.id ? { ...f, children: datas } : f )));
				}
			}).catch(e => {
				console.log('e: ', e);
			});
		}
	}

	const onSetCategory = (v) => {
		console.log('onSetCategory v: ', v);
		const val = v.parent ? v.parent_name + ", " + v.product_type_name : v.name;
		formik.setFieldValue("category", val);
	}

	return (
		<div className="container-fluid py-3">
			<Head title="Manage Product" />
			<h5 className="hr-h hr-left mb-3">Manage Product</h5>

			<div className="row">
				<div className="col-md-4 mt-1-next">
					{menus.length > 0 && 
						menus.map((v, i) => 
							v.child === "1" ? 
								<Tree key={i} 
									className="pl-3" 
									label={v.name} 
									labelProps={{
										kind: "light", 
										size: "sm", 
										className: "d-flex justify-content-between align-items-center w-100 text-left dropdown-toggle", 
										title: v.name
									}} 
									onToggle={(open) => onToggleTree(open, v)} 
								>
									{v.children && 
										<div className="btn-group-sm btn-group-vertical w-100 pt-1">
											{v.children.map((v2, i2) => 
												<Btn key={i2} kind="light" className="text-left" title={v2.name}
													onClick={() => {
														const omit = Q.omit({ 
															...v, 
															parent_id: v.id, 
															parent_name: v.name, 
															parent_slug: v.slug, 
															...v2, 
															product_type_id: v2.id, 
															product_type_name: v2.name,
															product_type_slug: v2.slug
														}, ['id','slug','name']);

														onSetCategory(omit);
													}}
												>{v2.name}</Btn>
											)}
										</div>
									}
								</Tree>
								: 
								<Btn key={i} onClick={() => onSetCategory(v)} kind="light" size="sm" className="w-100 text-left" title={v.name}>{v.name}</Btn>
						)
					}
				</div>

				<div className="col-md-8">
					<Form	noValidate 
						className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
						style={formik.isSubmitting ? { '--bg-i-load': '99px' } : null} 
						disabled={formik.isSubmitting} 
						onSubmit={formik.handleSubmit} 
						onReset={formik.handleReset}
					>
						<div className="card-body">
							<div className="form-group">
								<label htmlFor="name">Name</label>
								<input type="text" required 
									id="name" 
									className={"form-control" + Q.formikValidClass(formik, "name")} 
									{...formik.getFieldProps("name")} 
								/>

								{(formik.touched.name && formik.errors.name) && 
									<div className="invalid-feedback">{formik.errors.name}</div>
								}
							</div>

							<div className="form-group">
								<label htmlFor="category">Category</label>
								<input type="text" required 
									id="category" 
									autoComplete="off" 
									className={"form-control" + Q.formikValidClass(formik, "category")} 
									{...formik.getFieldProps("category")} 
								/>

								{(formik.touched.category && formik.errors.category) && 
									<div className="invalid-feedback">{formik.errors.category}</div>
								}
							</div>

							<div className="form-group">
								<label htmlFor="price">Price</label>
								{/* <input type="text" required 
									id="price" 
									className={"form-control" + Q.formikValidClass(formik, "price")} 
									{...formik.getFieldProps("price")} 
								/> */}

								<NumberFormat 
									thousandSeparator="." 
									decimalSeparator="," 
									prefix="Rp " 

									id="priceCurrency" 
									inputMode="numeric" // decimal
									// className="form-control" //  text-right
									className={"form-control" + Q.formikValidClass(formik, "priceCurrency")} 
									// defaultValue="" 
									// {...formik.getFieldProps("priceCurrency")} 
									value={formik.values.priceCurrency} 
									onChange={formik.handleChange} 
									onBlur={formik.handleBlur} 
								/>

								{(formik.touched.priceCurrency && formik.errors.priceCurrency) && 
									<div className="invalid-feedback">{formik.errors.priceCurrency}</div>
								}
							</div>

							<div className="form-group">
								<Editor 
									editorState={editorState} 
									// toolbarHidden 
									toolbar={{
										options: [
											'inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 
											'link', 'embedded', 'emoji', 'remove', 'history', // 'colorPicker', 'image'
										],
										inline: {
											inDropdown: false,
											className: undefined,
											component: undefined,
											dropdownClassName: undefined, 
											// , 'subscript'
											options: ['bold', 'italic', 'underline', 'strikethrough', 'monospace', 'superscript'], 
											blockType: { className: "text-decoration-none" }, 
											// bold: { className: "tip tipT", "aria-label": "Bold" }, // icon: bold, 
											// italic: { icon: italic, className: undefined },
											// underline: { icon: underline, className: undefined },
											// strikethrough: { icon: strikethrough, className: undefined },
											// monospace: { icon: monospace, className: undefined },
											// superscript: { icon: superscript, className: undefined },
											// subscript: { icon: subscript, className: undefined },
										}
									}}
									toolbarClassName="position-relative bg-light" 
									// wrapperClassName="wrapperClassName" 
									editorClassName="border mt-n1px px-3 rounded-bottom" 
									// localization={{
									// 	locale: "id", 
									// }}
									onEditorStateChange={(state) => {
										setEditorState(state);
									}}
								/>

								{/* <Textarea 
									readOnly 
									value={draftToHtml(convertToRaw(editorState.getCurrentContent()))} 
								/> */}
							</div>

							<div className="text-right">
								<Btn type="reset" kind="dark">Reset</Btn>{" "}
								<Btn type="submit">Save</Btn>
							</div>
						</div>
					</Form>
				</div>
			</div>
		</div>
	);
}

/* <select required 
	id="category" 
	className={"custom-select" + Q.formikValidClass(formik, "category")} 
	{...formik.getFieldProps("category")} 
>
	<option value="">Choose Kategori</option>
	<option value="hell 1">Hell 1</option>
	<option value="hell 2">Hell 2</option>
</select> */
