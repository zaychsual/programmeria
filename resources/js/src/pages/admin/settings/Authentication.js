import React, { useState, useEffect } from 'react';// { useState, useEffect, Fragment } 
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { MultiSelect } from 'primereact/multiselect';
import FormBs from 'react-bootstrap/Form';

import Form from '../../../components/q-ui-react/Form';
import Btn from '../../../components/q-ui-react/Btn';
import Input from '../../../components/q-ui-react/Input';
import InputGroup from '../../../components/q-ui-react/InputGroup';

export default function Authentication(){
  const [initialValues, setInitialValues] = useState({
    identity: ["email"], // [{ name: "email", value: "email" }], 
    passwordMinLength: 6, 
    sessExpired: 1, 
    confirm: "email", 
    fullname: false, 
    username: false, 
  });

  useEffect(() => {

  }, []);

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues, 
		validationSchema: Yup.object().shape({
			identity: Yup.array(Yup.string()).min(1, "Select minimum 1").required("is required"),
			passwordMinLength: Yup.number()
				.min(4, "Minimum value 4")
				.integer("Value must integer")
				.required("is required"),
			sessExpired: Yup.number()
				.min(1, "Minimum value 1")
				.integer("Value must integer")
				.required("is required"), 
			confirm: Yup.string().required("is required"), 
			// app_name: Yup.string()
			// 	.min(2, "Minimum 2 symbols")
			// 	.max(50, "Maximum 50 symbols")
			// 	.required("App Name is required"),
			// company_name: Yup.string().max(50, "Maximum 100 symbols"),
			// email: Yup.string()
			// 	.email("Wrong email format")
			// 	.required("Email is required"), 
		}), 
		onSubmit: (values, fn) => {
			console.log('values formik: ', values);
			fn.setSubmitting(false);
		}
	});

	// const TABS = ["Info","Authentication","General"];
	// const renderSup = <sup className="text-danger bold">*</sup>;

	return (
		<div className="row flex-row-reverse">
      <div className="col-md-5 mb-3">
        <div className="card shadow-sm position-sticky t48 zi-1">
          <div className="card-header">Setting your app authentication</div>
          <div className="card-body">

          </div>
        </div>
      </div>

      <div className="col-md-7">
        <Form noValidate 
          className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
          disabled={formik.isSubmitting} 
          onSubmit={formik.handleSubmit} 
          // onReset={formik.handleReset} // Q-OPTION
        >
          <div className="card-header bg-light py-2 text-right position-sticky t48 zi-3">
            <Btn type="reset" size="sm" kind="dark">Reset</Btn>{" "}
            <Btn type="submit" size="sm">Save</Btn>
          </div>

          <div className="card-body mt-3-next">
            <div>
              <label>Identity</label>
              <MultiSelect 
                className={"d-flex text-capitalize" + (formik.touched.identity && formik.errors.identity ? " border-danger" : "")} 
                panelClassName="text-capitalize" 
                // placeholder="Select a City" 
                display="chip" 
                optionLabel="name" 
                options={[
                  { name: "email", value: "email" }, 
                  { name: "username", value: "username" }, 
                  { name: "phone", value: "phone" }, 
                ]} 
                value={formik.values.identity} 
                onChange={(e) => {
                  formik.setFieldValue("identity", e.value);
                }} 
              />
              {(formik.touched.identity && formik.errors.identity) && 
                <div className="invalid-feedback d-block">{formik.errors.identity}</div>
              }
            </div>

            <div>
              <label>Password Minimum</label>
              <Input required 
                className={Q.formikValidClass(formik, "passwordMinLength")} 
                type="number" 
                min={4} 
                inputMode="decimal" 
                id="passwordMinLength" 
                value={formik.values.passwordMinLength} 
                onChange={formik.handleChange} 
              />
              {(formik.touched.passwordMinLength && formik.errors.passwordMinLength) && 
                <div className="invalid-feedback">{formik.errors.passwordMinLength}</div>
              }
            </div>

            <div>
              <label>Expired</label>
              <InputGroup
                append={
                  <select 
                    className="custom-select" 
                    defaultValue={"Days"} 
                    onChange={e => {
                      let val = e.target.value;
                      console.log('val: ', val);
                    }}
                  >
                    {["Minutes","Hours","Days","Months","Years"].map(v => 
                      <option key={v} value={v}>{v}</option>	
                    )}
                  </select>
                }
              >
                <Input required 
                  className={Q.formikValidClass(formik, "sessExpired")} 
                  type="number" 
                  min={1} 
                  inputMode="decimal" 
                  id="sessExpired" 
                  value={formik.values.sessExpired} 
                  onChange={formik.handleChange} 
                />
              </InputGroup>
              
              {(formik.touched.sessExpired && formik.errors.sessExpired) && 
                <div className="invalid-feedback d-block">{formik.errors.sessExpired}</div>
              }
            </div>

            <div>
              <label>Register Confirmation</label>
              <select 
                className="custom-select bg-white text-dark text-capitalize o-1" 
                defaultValue={formik.values.confirm} // "email"
                // readOnly 
                disabled 
              >
                {["email","sms","whatsapp"].map(v => 
                  <option key={v} value={v}>{v}</option>	
                )}
              </select>
            </div>

            <div>
              <label className="small">Required Full name in Register Process</label>
              <FormBs.Check 
                custom
                type="checkbox" 
                id="fullname"
                label="Full name" 
                checked={Boolean(formik.values.fullname)} 
                onChange={e => formik.setFieldValue("fullname", e.target.checked)}
              />
            </div>

            <div>
              <label className="small">Required Username in Register Process</label>
              <FormBs.Check 
                custom
                type="checkbox" 
                id="username" 
                label="Username" 
                checked={Boolean(formik.values.username)} 
                onChange={e => formik.setFieldValue("username", e.target.checked)}
              />
            </div>
          </div>
        </Form>
      </div>
    </div>
	);
}

