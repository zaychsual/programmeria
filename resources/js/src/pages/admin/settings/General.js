import React, { useState, useEffect } from "react";
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { Dropdown as DdPrime } from 'primereact/dropdown';

import Flex from '../../../components/q-ui-react/Flex';
import Form from '../../../components/q-ui-react/Form';
import Btn from '../../../components/q-ui-react/Btn';
import Img from '../../../components/q-ui-react/Img';
import { ColorPicker } from '../../../components/q-ui-react/color-picker/ColorPicker';
import CURRENCY_CODES from '../../../data/CURRENCY_CODES';

// console.log(JSON.stringify([...new Map(CURRENCY_CODES.map(item => [item.AlphabeticCode, item])).values()]));

export default function General(){
  const INIT_VAL = {
    lang: "id", // { name:"English", code: "en", icon:"united-kingdom" }, 
    currency: "USD", 
    theme: {
      bg: "main", 
      hex: "#e3f2fd", 
      text: "#212529", 
    }
  };
  const [initialValues, setInitialValues] = useState(INIT_VAL);
  const [loadValue, setLoadValue] = useState(true);

  useEffect(() => {
    setTimeout(() => { // DUMMY Xhr
      setInitialValues({ ...INIT_VAL, lang: "en" });
      setLoadValue(false);
    }, 2000);
  }, []);

  const formik = useFormik({
    enableReinitialize: true, 
		initialValues, 
		validationSchema: Yup.object().shape({
			lang: Yup.string().required("Is required"),
			// app_name: Yup.string()
			// 	.min(2, "Minimum 2 symbols")
			// 	.max(50, "Maximum 50 symbols")
			// 	.required("Is required"),
				// .required(
				//   intl.formatMessage({
				//     id: "AUTH.VALIDATION.REQUIRED_FIELD",
				//   })
				// ),
			// company_name: Yup.string().max(50, "Maximum 100 symbols"),
			// email: Yup.string()
			// 	.email("Wrong email format")
			// 	.required("Is required"), 
		}), 
		onSubmit: (values, fn) => {
      console.log('values: ', values);
      fn.setSubmitting(false);
    }
  });

  const renderLang = (option, size = 18) => (
    <Flex align="center">
      <Img w={size} 
        alt={option.name} 
        src={"/media/svg/flags/" + option.icon + ".svg"} 
        className="flexno d-block border mr-2"
      />
      {option.name}
    </Flex>
  );

  let { isSubmitting, values, handleSubmit, setFieldValue } = formik;// handleReset
  let disabledForm = loadValue || isSubmitting;

  return (
    <div className="row flex-row-reverse">
      <div className="col-md-5 mb-3">
        <div className="card shadow-sm position-sticky t48 zi-1">
          <div className="card-header">Setting General</div>
          <div className="card-body">
            
          </div>
        </div>
      </div>

      <div className="col-md-7">
        <Form noValidate 
          className={"card shadow-sm" + (disabledForm ? " i-load cwait" : "")} 
          // fieldsetClass="mt-3-next" 
          disabled={disabledForm} 
          onSubmit={handleSubmit} 
          // onReset={handleReset} // Q-OPTION
        >
          <div className="card-header bg-light py-2 text-right position-sticky t48 zi-3">
            <Btn type="reset" size="sm" kind="dark">Reset</Btn>{" "}
            <Btn type="submit" size="sm">Save</Btn>
          </div>

          <div className="card-body mt-3-next">
            <div>
              <label>Language</label>
              <DdPrime 
                className="d-flex" 
                disabled={disabledForm} 
                value={values.lang} 
                options={[
                  { name:"English", code: "en", icon:"united-kingdom" }, 
                  { name:"Indonesia", code: "id", icon:"indonesia" }
                ]} 
                onChange={(e) => setFieldValue("lang", e.value)} 
                optionLabel="name" 
                optionValue="code" 
                filter 
                // showClear 
                showFilterClear 
                resetFilterOnHide 
                filterBy="name" 
                filterPlaceholder="Search" // placeholder 
                // emptyFilterMessage="Not Found" 
                panelClassName="zi-1021 w-auto" 
                // panelStyle={{
                //   width: 240 
                // }}
                valueTemplate={(option) => { // , props
                  if (option) {
                    return renderLang(option, 15);
                  }
                  return null;
                }} 
                itemTemplate={renderLang} 
              />
            </div>

            <div>
              <label>Currency</label>
              <DdPrime 
                className="d-flex" 
                disabled={disabledForm} 
                value={values.currency} 
                options={CURRENCY_CODES} 
                onChange={(e) => {
                  console.log("onChange e: ", e);
                  setFieldValue("currency", e.value);
                }} 
                optionLabel="Currency" 
                optionValue="AlphabeticCode" 
                filter 
                // showClear 
                showFilterClear 
                resetFilterOnHide 
                filterBy="AlphabeticCode" 
                filterPlaceholder="Search" 
                panelClassName="zi-1021 w-auto" 
                valueTemplate={(option) => option && option.AlphabeticCode + " - " + option.Currency} 
                itemTemplate={option => option.AlphabeticCode + " - " + option.Currency} 
              />
            </div>
          </div>

          <div className="card-header bg-light py-2 border-top">
            Themes
          </div>

          <div className="card-body mt-3-next">
            <div>
              <label>Background Color</label>
              <ColorPicker 
                rgba={false} 
                inputProps={{
                  className: "text-monospace"
                }}
                dropdownMenuProps={{
                  className: "w-100"
                }}
                value={values.theme.hex} 
                onChange={(val, obj) => {
                  console.log('onChange val: ', val);
                  console.log('onChange obj: ', obj);
                  setFieldValue("theme.bg", obj?.name || val);
                  setFieldValue("theme.hex", val);
                }} 
              />
            </div>

            <div>
              <label>Text Color</label>
              <ColorPicker 
                // colorName={false} 
                inputProps={{
                  className: "text-monospace"
                }}
                dropdownMenuProps={{
                  className: "w-100"
                }}
                value={values.theme.text} 
                onChange={(val, obj) => {
                  console.log('onChange val: ', val);
                  console.log('onChange obj: ', obj);
                  setFieldValue("theme.text", obj?.name || val);
                }} 
              />
            </div>
          </div>
        </Form>
      </div>
    </div>
  );
}
