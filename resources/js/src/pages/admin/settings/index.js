import React from 'react';// , { useState, useEffect }
import Tab from 'react-bootstrap/Tab';
import Nav from 'react-bootstrap/Nav';

import Head from '../../../components/q-ui-react/Head';
import Info from './Info';
import Authentication from './Authentication';
import General from "./General";

export default function Settings(){
  const MENUS = ["General","Info","Authentication"];
  const TABS = [General, Info, Authentication];

  return (
		<div className="container-fluid py-3">
			<Head title="Settings App" />
			<h5 className="hr-h hr-left mb-4">Settings App</h5>

			<Tab.Container 
				mountOnEnter 
				id="tabSettingApp" 
				defaultActiveKey="General" 
				onSelect={key => {
					console.log('onSelect key: ', key);
				}}
			>
				<div className="row">
					<div className="col-md-3">
						<Nav variant="pills" className="flex-column position-sticky t48 zi-1">
							{MENUS.map((v) => 
								<Nav.Item key={v}>
									<Nav.Link eventKey={v} className="btn w-100 text-left" {...Q.DD_BTN}>{v}</Nav.Link>
								</Nav.Item>
							)}
						</Nav>
					</div>

					<div className="col-md-9">
						<Tab.Content>
              {TABS.map((Content, i) => 
                <Tab.Pane key={i} eventKey={MENUS[i]}>
                  <Content />
                </Tab.Pane>
              )}
            </Tab.Content>
          </div>
        </div>
      </Tab.Container>
    </div>
  );
}
