import React, { useState, useEffect } from "react";
import * as Yup from 'yup';
import { useFormik } from 'formik';

import Flex from '../../../components/q-ui-react/Flex';
import Form from '../../../components/q-ui-react/Form';
import Btn from '../../../components/q-ui-react/Btn';
import Input from '../../../components/q-ui-react/Input';
import Textarea from '../../../components/q-ui-react/Textarea';
import InputDataList from '../../../components/q-ui-react/experiment/InputDatalist';
import Placeholder from '../../../components/q-ui-react/Placeholder';
import ModalQ from '../../../components/q-ui-react/ModalQ';
import ManifestGenerator from '../../../apps/ManifestGenerator';

const SOCIAL_MEDIA = ["facebook", "instagram", "twitter", "youtube", "whatsapp", "telegram"];
const INIT_SOCIAL = { name: "", url: "", icon: "link", id: "sos-" + Q.Qid() };

export default function Info(){
  const [load, setLoad] = useState(false);
	const [loading, setLoading] = useState(false);
	const [vals, setVals] = useState({
		logo: "logo-144x144.png", 
		app_name: "",
		company_name: "",
		email: "", 
		address: "", 
		phone: "", 
		fax: "", 
		// theme: "", 
		description: ""
	});
	const [socials, setSocials] = useState([INIT_SOCIAL]);
	const [appTitle, setAppTitle] = useState();// "appName"
	const [errorSocial, setErrorSocial] = useState();
	// const [file, setFile] = useState({});
	const [fileZip, setFileZip] = useState();
	const [modal, setModal] = useState(false);
	const [manifest, setManifest] = useState({});

	const scroll2Down = () => {
		window.scrollTo({
			top: document.documentElement.scrollHeight,
			left: 0,
			behavior: 'smooth'
		});
	}
	
	useEffect(() => {
		console.log('%cuseEffect in Settings','color:yellow;');
		axios.get("/get_row/app").then(r => {
			console.log('/api/get_row/app r: ', r);
			const datas = r.data;
			if(Array.isArray(datas) && !datas.error){ // r.status === 200 && 
				axios.get("/get_all/social_media").then(r2 => {
					// console.log('/api/get_all/social_media r2: ', r2);
					const data2 = r2.data;
					if(data2 && !data2.error){ // r2.status === 200 && 
						setSocials(data2);
					}
				}).catch(e => {
					console.log('/api/get_all/social_media e: ', e);
				});

				const { app_title, app_name } = datas;// , company_name
				// setAppTitle({ 
				// 	type: app_title === app_name ? app_name : company_name, 
				// 	value: app_title
				// });
				setAppTitle(app_title === app_name ? "appName" : "companyName");
				setVals(datas);
			}else{
				setAppTitle("appName");
			}
		}).catch(e => {
			console.log('/api/get_row/app e: ', e);
		});

		axios.get(Q.baseURL + "/logo/manifest.json") // Q.baseURL + "/public/media/logo/manifest.json"
		.then(r => {
			console.log('manifest.json r: ', r);
			const icons = r.data.icons.map(v => ({ src: v.src.replace("logo/","") }));
			setManifest({ ...r.data, icons });// , sizes: v.sizes

		})
		.catch(e => console.log('e: ', e))
		.then(() => setLoad(true));
	}, []);

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues: vals, 
		validationSchema: Yup.object().shape({
			logo: Yup.string()
				.min(5, "Minimum 5 symbols")
				.required("Logo is required"),
			app_name: Yup.string()
				.min(2, "Minimum 2 symbols")
				.max(50, "Maximum 50 symbols")
				.required("App Name is required"),
				// .required(
				//   intl.formatMessage({
				//     id: "AUTH.VALIDATION.REQUIRED_FIELD",
				//   })
				// ),
			company_name: Yup.string().max(50, "Maximum 100 symbols"),
			email: Yup.string()
				.email("Wrong email format")
				.required("Email is required"), 
		}), 
		onSubmit: (values, fn) => {
			const invalidSocial = socials.find(f => f.name.length < 1 || !Q.isAbsoluteUrl(f.url));// f.url.length < 1
			console.log('invalidSocial: ', invalidSocial);

			if(invalidSocial){
				setErrorSocial("Please insert socials name & valid url.");
				fn.setSubmitting(false);
				scroll2Down();
			}else{
				const newVal = {
					...values, 
					app_title: appTitle === "appName" ? values.app_name : values.company_name
				};
				console.log('fileZip: ', fileZip);
				console.log('values: ', values);
				console.log('newVal: ', newVal);
				// console.log('socials: ', socials);

				let saveLogo;
				axios.post("/setting-app", Q.obj2formData(newVal))
				.then(async r => {
					console.log('/setting-app r: ', r);
					if(r.data && r.data.error){
						Swal.fire({
							icon: "error", 
							text: r.data.error, 
							confirmButtonText: "Try again"
						});
					}else{
						if(socials.length > 0){
							socials.forEach((f, i) => {
								axios.post("/setting-social", Q.obj2formData(f))
								.then(async r => {
									console.log('/setting-social r: ', r);
									if(i + 1 === socials.length){
										// fn.setSubmitting(false);
										setErrorSocial(null);
										// swalToast({ icon:"success", text:"Success save settings app" });
										if(fileZip){
											saveLogo = await saveExtractLogo();
											if(saveLogo.data && !saveLogo.data.error){
												// setErrorSocial(null);
												swalToast({ icon:"success", text:"Success save setting app" });
											}else{
												swalToast({ icon:"error", text:"Failed save setting app" });
											}
										}else{
											swalToast({ icon:"success", text:"Success save setting app" });
										}
									}
								}).catch(e => {
									console.log('e: ', e);
									swalToast({ icon:"error", text:"Failed save setting app" });
								});// .then(() => fn.setSubmitting(false));
							});
						}
						else{
							if(fileZip){
								saveLogo = await saveExtractLogo();
								if(saveLogo.data && !saveLogo.data.error){
									setErrorSocial(null);
									swalToast({ icon:"success", text:"Success save setting app" });
								}else{
									swalToast({ icon:"error", text:"Failed save setting app" });
								}
							}else{
								swalToast({ icon:"success", text:"Success save setting app" });
							}
							// fn.setSubmitting(false);
						}
					}
				}).catch(e => {
					console.log('e: ', e);
					// fn.setSubmitting(false);
					swalToast({ icon:"error", text:"Failed save setting app" });
				}).then(() => fn.setSubmitting(false));
			}
		},
	});

  const saveExtractLogo = async () => {
		try {
			const req = await axios.post("/save-extract-zip", Q.obj2formData({
				path: "/logo/", // /public/media/
				file: fileZip
			}), {
				headers: {'Content-Type': 'multipart/form-data'}
			});
			console.log('req: ', req);
			return req;
		}catch(e){
			console.log('e: ', e);
			return e;
		}
	}

	const onAddSocial = () => {
		const id = "sos-" + Q.Qid();
		setSocials([ ...socials, { ...INIT_SOCIAL, id } ]);
		setTimeout(() => {
			scroll2Down();
			setTimeout(() => Q.domQ("#" + id)?.focus(), 200);
		}, 9);
	}

	const onRemoveSocial = (item) => {
		setSocials(socials.filter(f => f.id !== item.id));
		if(errorSocial){
			setErrorSocial(null);
		}
	}

	const onChangeSocialName = (e, item) => {
		const name = e.target.value;
		setSocials(socials.map(f => {
			if(item.id === f.id){
				const ico = name.toLowerCase();
				return { 
					...f, 
					name, 
					icon: SOCIAL_MEDIA.includes(ico) ? ico : "link"
				}
			}
			return f;
		}));
	}

	// const onChangeSocialUrl = (e, item) => {
	// 	setSocials(socials.map(f => ( item.id === f.id ? { ...f, url: e.target.value } : f )));
	// }

	// const onChangeFile = e => {
	// 	const f = e.target.files[0];
	// 	if(f){
	// 		setFile(f);
	// 		formik.setFieldValue("logo", f.name);// , true
	// 	}
	// }

	const onDetailSwal = () => {
		const fname = formik.values.logo;// file.name || formik.values.logo
		if(fname){
			Swal.fire({
				title: fname, 
				allowEnterKey: false, 
				showCloseButton: true, 
				showConfirmButton: false, 
				// imageUrl: file.name ? window.URL.createObjectURL(file) : "public/media/logo/" + formik.values.logo, 
				imageUrl: "/logo/" + fname, // "public/media/logo/" + fname
				imageHeight: 400, 
				imageAlt: fname, 
				// didOpen(){
				// 	if(file.name){
				// 		const img = Swal.getImage();
				// 		if(img) window.URL.revokeObjectURL(img.src);
				// 	}
				// }
			});
		}
	}

	const backUpDb = () => { // e
		// const et = e.target;
		// if(et) et.hidden = true;
		setLoading(true);

		axios.get('/backup-db').then(r => {
			const data = r.data;
			console.log('backup-db r: ', r);
			if(data && !data.error){
				swalToast({ icon:"success", text:"Success backup database" });
			}else{
				swalToast({ icon:"error", text:"Error backup database" });
			}
		}).catch(e => {
			console.log('e: ', e);
			swalToast({ icon:"error", text:"Error backup database" });
		}).then(() => {
			// if(et) et.hidden = false;
			setLoading(false);
		});
	}

	// const onRemoveFile = () => {
	// 	setFile({});
	// 	formik.setFieldValue("logo","");
	// }

  const renderSup = <sup className="text-danger bold">*</sup>;

  return (
    <div className="row flex-row-reverse">
      {load ? 
        <>
          <div className="col-md-5 mb-3">
            <div className="card shadow-sm position-sticky t48 zi-1">
              <div className="card-header">Setting your app information</div>
              <div className="card-body">
                <div className="btn-group-vertical w-100">
                  {/* <Btn onClick={onModal} kind="light" className="text-left">Add Page</Btn> */}
                  <Btn onClick={backUpDb} kind="light" className="text-left"
                    loading={loading}
                  >
                    Backup database {loading && <b className="spinner-border spinner-border-sm" />}
                  </Btn>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-7">
            <Form noValidate 
              className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
              disabled={formik.isSubmitting} 
              onSubmit={formik.handleSubmit} 
              onReset={formik.handleReset} // Q-OPTION
            >
              <div className="card-header bg-light py-2 text-right position-sticky t48 zi-3">
                <Btn type="reset" size="sm" kind="dark">Reset</Btn>{" "}
                <Btn type="submit" size="sm">Save</Btn>
              </div>

              <div className="card-body mt-3-next">
                {/* <div>
                  <label htmlFor="logo">Logo {renderSup}</label>
                  <div className="input-group">
                    <Btn As="label" kind="fff" tabIndex="0" className="form-control text-left">
                      {file.name || formik.values.logo || "Choose image"} 
                      <input onChange={onChangeFile} id="logo" type="file" accept="image/*" hidden />
                    </Btn>
                    {(file.name || formik.values.logo) && 
                      <div className="input-group-append">
                        <Btn kind="light" title="Remove" className="qi qi-close xx" 
                          onClick={() => {
                            setFile({});
                            formik.setFieldValue("logo","");
                          }}
                        />
                        <Btn onClick={onDetailSwal} kind="light" title="View" className="qi qi-img" />
                      </div>
                    }
                  </div>

                  {(formik.touched.logo && formik.errors.logo) && 
                    <div className="invalid-feedback d-block">{formik.errors.logo}</div>
                  }
                </div> */}

                <div>
                  <label>Logo {renderSup}</label>
                  <div className="input-group">
                    {/* <Btn onClick={() => setModal(true)} kind="fff" 
                      className={"form-control text-left" + Q.formikValidClass(formik, "logo")} 
                    >
                      {file.name || formik.values.logo || "Choose image"} 
                    </Btn> */}

                    <select 
                      className={"custom-select" + Q.formikValidClass(formik, "logo")} 
                      id="logo" 
                      value={formik.values.logo} // file.name || formik.values.logo
                      onChange={formik.handleChange} 
                    >
                      {/* <option hidden>Choose image</option> */}
                      {manifest?.icons.map((v, i) => 
                        <option key={i} value={v.src} title={v.src}>{v.src}</option>
                      )}
                    </select>

                    {/* {(file.name || formik.values.logo) && 
                      <div className="input-group-append">
                        <Btn onClick={onRemoveFile} kind="light" title="Remove" className="qi qi-close xx" />
                        <Btn onClick={() => setModal(true)} kind="light" title="Edit" className="qi qi-edit" />
                        <Btn onClick={onDetailSwal} kind="light" title="View" className="qi qi-img" />
                      </div>
                    } */}

                    <div className="input-group-append">
                      <Btn onClick={() => setModal(true)} kind="light" title="Edit" className="qi qi-edit" />
                      <Btn onClick={onDetailSwal} kind="light" title="View" className="qi qi-img" />
                    </div>
                  </div>

                  {(formik.touched.logo && formik.errors.logo) && // ⚠ 
                    <div className="invalid-feedback d-block">{formik.errors.logo}</div>
                  }
                </div>

                <div>
                  <label htmlFor="app_name">App Name {renderSup}</label>
                  <div className="input-group">
                    <Input required 
                      className={Q.formikValidClass(formik, "app_name")} 
                      id="app_name" 
                      value={formik.values.app_name} 
                      onChange={formik.handleChange} 
                    />

                    <label className="input-group-append mb-0 select-no tip tipTR" aria-label="Use for app title">
                      <div className="input-group-text pr-1">
                        <div className="custom-control custom-checkbox">
                          <input id="cTitleAppNAme" name="appTitle" type="radio" className="custom-control-input" 
                            checked={appTitle === "appName"} 
                            onChange={e => {
                              if(e.target.checked){
                                setAppTitle("appName");
                              }
                            }} 
                          />
                          <div className="custom-control-label" />
                        </div>
                      </div>
                    </label>
                  </div>

                  {/* <small className="form-text text-muted">We'll never share your email with anyone else.</small> */}
                  {(formik.touched.app_name && formik.errors.app_name) && // ⚠ 
                    <div className="invalid-feedback d-block">{formik.errors.app_name}</div>
                  }
                </div>

                <div>
                  <label htmlFor="company_name">Company Name</label>
                  <div className="input-group">
                    <Input required 
                      className={Q.formikValidClass(formik, "company_name")} 
                      id="company_name" 
                      value={formik.values.company_name} 
                      onChange={formik.handleChange}
                    />

                    <label className="input-group-append mb-0 select-no tip tipTR" aria-label="Use for app title">
                      <div className="input-group-text pr-1">
                        <div className="custom-control custom-checkbox">
                          <input id="cTitleCompany" name="appTitle" type="radio" className="custom-control-input" 
                            checked={appTitle === "companyName"} 
                            onChange={e => {
                              if(e.target.checked){
                                setAppTitle("companyName")
                              }
                            }} 
                          />
                          <div className="custom-control-label" />
                        </div>
                      </div>
                    </label>

                    {(formik.touched.company_name && formik.errors.company_name) && // ⚠ 
                      <div className="invalid-feedback d-block">{formik.errors.company_name}</div>
                    }
                  </div>
                </div>

                <div>
                  <label htmlFor="email">Email {renderSup}</label>
                  <Input type="email" required 
                    className={Q.formikValidClass(formik, "email")} 
                    id="email" 
                    value={formik.values.email} 
                    onChange={formik.handleChange} 
                  />

                  {(formik.touched.email && formik.errors.email) && // ⚠ 
                    <div className="invalid-feedback">{formik.errors.email}</div>
                  }
                </div>

                <div>
                  <label htmlFor="address">Address</label>
                  <Textarea spellCheck={false} 
                    className={Q.formikValidClass(formik, "address")} 
                    id="address" 
                    value={formik.values.address} 
                    onChange={formik.handleChange} 
                    // {...formik.getFieldProps("address")} 
                  />
                </div>

                <div>
                  <label htmlFor="phone">Phone</label>
                  <Input type="tel" 
                    className={Q.formikValidClass(formik, "phone")} 
                    id="phone" 
                    value={formik.values.phone} 
                    onChange={formik.handleChange} 
                  />
                </div>

                <div>
                  <label htmlFor="fax">Fax</label>
                  <Input type="tel" 
                    className={Q.formikValidClass(formik, "fax")} 
                    id="fax" 
                    value={formik.values.fax} 
                    onChange={formik.handleChange}  
                  />
                </div>

                <div>
                  <label htmlFor="description">Description</label>
                  <Textarea 
                    className={Q.formikValidClass(formik, "description")} 
                    id="description" 
                    value={formik.values.description} 
                    onChange={formik.handleChange} 
                  />
                </div>

                <div>
                  <Flex className="mb-2">
                    Social Media
                    <Btn onClick={onAddSocial} size="xs" className="qi qi-plus ml-auto">Add</Btn>
                  </Flex>

                  {socials.map((v, i) => 
                    <div key={v.id} className="input-group mb-3">
                      <div className="input-group-prepend">
                        <label htmlFor={v.id} className={"input-group-text w-40px i-color qi qi-" + v.icon} />
                      </div>

                      <InputDataList placeholder="Social Name" 
                        required 
                        id={v.id} 
                        options={SOCIAL_MEDIA} 
                        value={v.name} 
                        onChange={e => onChangeSocialName(e, v)} // onChangeSocial(e, v, "name")
                      />

                      {/* <Input placeholder="Social Name" 
                        required 
                        id={v.id} 
                        list={v.id + i}
                        value={v.name} 
                        onChange={e => onChangeSocialName(e, v)} // onChangeSocial(e, v, "name")
                        // onFocus={e => {
                        // 	console.log(e.target.nextElementSibling);
                        // }}
                      />

                      <datalist id={v.id + i}>
                        {SOCIAL_MEDIA.map((v, i) => <option key={i} value={v} />)}
                      </datalist> */}

                      <Input placeholder="Social Url" // http / https
                        // type="url" 
                        required 
                        value={v.url} 
                        // onChangeSocialUrl(e, v) | onChangeSocial(e, v, "url")
                        onChange={e => setSocials(socials.map(f => ( v.id === f.id ? { ...f, url: e.target.value } : f )))} 
                      />

                      <div className="input-group-append">
                        <Btn onClick={() => onRemoveSocial(v)} kind="light" className="qi qi-close xx" title="Remove" />
                      </div>
                    </div>
                  )}

                  {errorSocial && 
                    <div className="invalid-feedback d-block">{errorSocial}</div>
                  }
                </div>
              </div>
            </Form>
          </div>

          <ModalQ 
            open={modal} 
            toggle={() => setModal(!modal)} 
            // autoFocus={false} 
            scrollable 
            position="center" 
            size="xl" 
            title="Create Logos & Manifest" 
            body={
              <div>
                <ManifestGenerator 
                  manifestData={{ 
                    name: vals.app_name, 
                    short_name: manifest.short_name, 
                    description: vals.description
                  }}
                  edtitableFilename={false} 
                  zipNameInputProps={{
                    value: "logo",
                    readOnly: true
                  }}
                  onSave={val => {
                    console.log('ManifestGenerator onSave val: ', val);
                    const { name, description } = val.manifest;

                    setFileZip(val.zip);
                    setVals({ ...formik.values, app_name: name, description });
                    setModal(false);
                  }}
                  // className="p-3" 
                  asideClass="position-sticky t0 zi-1" 
                  // btnSaveLabel="Save" 
                  // prepend={
                  // 	<h4>Manifest Generator</h4>
                  // }
                />
              </div>
            } 
            foot={
              <Btn onClick={() => setModal(false)} kind="dark">Cancel</Btn>
            }
          />
        </>
        : 
        [5, 7].map(v => 
          <div key={v} className={"mb-3 col-md-" + v}>
            <Placeholder type="thumb" className="card" />
          </div>
        )
      }
    </div>
  );
}
