import React, { useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import { useParams } from 'react-router-dom';
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
import Dropdown from 'react-bootstrap/Dropdown';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

import Head from '../../components/q-ui-react/Head';
import Placeholder from '../../components/q-ui-react/Placeholder';
import Form from '../../components/q-ui-react/Form';
import Btn from '../../components/q-ui-react/Btn';
import Input from '../../components/q-ui-react/Input';
import Flex from '../../components/q-ui-react/Flex';
import Img from '../../components/q-ui-react/Img';
import NumberFormat from '../../components/react-number-format/NumberFormat';
import { TreeQSelect } from '../../components/TreeQSelect';

export default function ProductEdit(){
	let { id } = useParams();
	const initialValues = {
		name: "", 
		category_id: "", 
		price_currency: "", 
	};

	const [load, setLoad] = useState(false);
	const [formVals, setFormVals] = useState(initialValues);
  const [menus, setMenus] = useState([]);
	const [categoryVal, setCategoryVal] = useState("");
	const [publish, setPublish] = useState(true);
	const [display, setDisplay] = useState(false);
	const [img, setImg] = useState();
	const [images, setImages] = useState([]);
	const [merchants, setMerchants] = useState([]);
	const [editorState, setEditorState] = useState(EditorState.createEmpty());
	const [customVal, setCustomVal] = useState(false);
	
	useEffect(() => {
		// console.log('%cuseEffect in ProductEdit','color:yellow;');
		if(id){
			// Q.obj2formData({ select: "id,name,slug,child" })
			axios.get("/get_where_by/products/id/" + id + "/row/").then(r => {
				console.log('get_where_by r: ', r);
				const rdata = r.data;
				if(rdata && !rdata.error){// r.status === 200 && 
					setFormVals({ ...formVals, ...rdata, img: rdata.img || "" });
					setTimeout(() => {
						const omit = Q.omit({ 
							...rdata, 
							parent: rdata.category_id, 
							parent_id: rdata.category_id, 
							parent_name: rdata.category_name, 
							parent_slug: rdata.category_slug
						}, ['id','slug','name']);
						onSetCategory(omit);// rdata

						if(!rdata.price){
							setCustomVal(true);
						}
					}, 9);

					if(rdata.images){
						setImages(JSON.parse(rdata.images));
					}

					if(rdata.display && rdata.display === "1"){
						setDisplay(true);
					}

					if(rdata.description){
						const blocksFromHtml = htmlToDraft(rdata.description);
						const { contentBlocks, entityMap } = blocksFromHtml;
						const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
						setEditorState(EditorState.createWithContent(contentState));
					}
				}
			}).catch(e => {
				console.log('e: ', e);
			}).then(() => setLoad(true));
		}

		axios.post("/get_all/category", Q.obj2formData({ select: "id,name,slug,child" })).then(r => {
			// console.log('get_all r: ', r);
			const data1 = r.data;
			if(data1 && !data1.error){// r.status === 200 && 
				setMenus(data1);
			}
		}).catch(e => {
			console.log('e: ', e);
		});

		axios.post("/get_all/merchants", Q.obj2formData({ select: "id,name,domain,img" })).then(r => {
			// console.log('merchants r: ', r);
			const dataMerchants = r.data;
			if(dataMerchants && !dataMerchants.error){// r.status === 200 && 
				axios.get("/get_where_by/product_merchant/product_id/" + id).then(r2 => {
					console.log('product_merchant r2: ', r2);
					if(r2.data && !r2.data.error){// r2.status === 200 && 
						let fixMer = dataMerchants;
						r2.data.forEach((v) => {
							const used = fixMer.findIndex(f => f.name === v.name);
							fixMer[used] = { ...fixMer[used], use: true, domain: v.url, id: v.id, product_id: v.product_id };
						});
						// console.log('fixMer: ',fixMer);
						setMerchants(fixMer);
					}else{
						setMerchants(r.data);
					}
				}).catch(e => {
					console.log('e: ', e);
				});
			}
		}).catch(e => {
			console.log('e: ', e);
		});
	}, []);// id

	const validationSchema = Yup.object().shape({
		name: Yup.string()
			.min(2, "Minimum 2 symbols")
			.max(255, "Maximum 255 symbols")
			.required("Name is required"),
		category_id: Yup.string().required("Kategori is required"), 
		price_currency: Yup.string()
			.max(100, "Maximum 100 symbols")
			// .required("Harga is required"), 
	});

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues: formVals, 
		validationSchema, 
		onSubmit: (values, fn) => {
			let newVal = {
				...values, 
				...categoryVal, 
				slug: Q.str2slug(values.name), 
				publish: publish ? 1 : 0, 
				display: display ? 1 : 0, 
				// price: parseFloat(values.price_currency.replace(/Rp |\./g, '')), 
				created_by: USER_DATA[0].id, 
			};
			const uses = merchants.length > 0 && merchants.filter(f => f.use);

			if(newVal.price_currency && !customVal){
				newVal.price = parseFloat(values.price_currency.replace(/Rp |\./g, ''));
			}

			if(uses.length > 0){
				newVal.merchant = uses.length;
			}else{
				delete newVal.merchant;
			}

			const delMerchants = merchants.filter(f => !f.use);
			if(delMerchants.length > 0){
				newVal.del_merchants = JSON.stringify(delMerchants);
			}

			if(img){
				newVal.file = img;
			}
			if(!newVal.img){
				delete newVal.img;
			}
			if(uses.length < 1){
				newVal.merchant = 0;// delete newVal.merchant;
			}
			if(!newVal.product_type_id){
				delete newVal.product_type_id;
			}
			if(!newVal.product_type_name){
				delete newVal.product_type_name;
			}
			if(!newVal.product_type_slug){
				delete newVal.product_type_slug;
			}

			const draftVal = editorState.getCurrentContent();
			if(draftVal.hasText()){// DOMPurify.sanitize(html)
				newVal.description = draftToHtml(convertToRaw(draftVal)).trim();
			}else{
				delete newVal.description;
			}

			let fd = Q.obj2formData(newVal);

			if(images.length > 0){
				images.forEach(f => fd.append('files[]', f));
			}

			if(uses.length > 0){
				// fd.append('merchant', uses.length);
				uses.forEach(f => {
					const haveData = f.product_id ? { id: f.id, product_id: f.product_id } : undefined;
					fd.append('merchants[]', JSON.stringify({ ...haveData, name: f.name, url: f.domain, img: f.img }));
				});
			}

			console.log('values: ', values);
			console.log('newVal: ', newVal);
			// console.log('img: ', img);
			// console.log('images: ', images);
			// console.log('merchants: ', merchants);
			// console.log('delMerchants: ', delMerchants);
			// for(let pair of fdImages.entries()){
			// 	console.log(pair[0]+ ': ', pair[1]);
			// }

			axios.post("/edit-product", fd, { // add
				headers: {'Content-Type': 'multipart/form-data'}
			}).then(r => {
				console.log('edit-product r: ', r);
				const data = r.data;
				if(data && !data.error){// r.status === 200 && 
					swalToast({ icon:"success", text:"Success edit product " + newVal.name + "." });
				}
			}).catch(e => {
				console.log('e: ', e);
			}).then(() => fn.setSubmitting(false));
		}
	});

	const onToggleTree = (open, v) => {
		if(open && !v.children){
			axios.post("/get_where_by/product_type/parent/" + v.id, Q.obj2formData({ select: "id,name,slug,parent" })).then(r => {
				// console.log('r: ', r);
				const datas = r.data;
				if(datas && !datas.error){// r.status === 200 && 
					setMenus(menus.map(f => ( f.id === v.id ? { ...f, children: datas } : f )));
				}
			}).catch(e => {
				console.log('e: ', e);
			});
		}
	}

	const onSetCategory = (v) => {
		// console.log('onSetCategory v: ', v);
		const val = v.parent ? v.parent_name + (v.product_type_name ? ", " + v.product_type_name : "") : v.name;
		formik.setFieldValue("category_id", val);
		setCategoryVal(v.parent ? { category_id: v.parent_id, category_slug: v.parent_slug, category_name: v.parent_name, product_type_id: v.product_type_id, product_type_slug: v.product_type_slug, product_type_name: v.product_type_name } : { category_id: v.id, category_slug: v.slug, category_name: v.name });
	}

	const onChangeImgFile = (e) => {
		const file = e.target.files[0];
		// const file = et.files[0];
		if(file){
			setImg(file);
			// setModalData({ ...modalData, file });
			// if(errorImg) setErrorImg(null);
		}
	}

	const onChangeImgesFile = (e) => {
		const files = e.target.files;
		if(files && files.length > 0){
			let dataImg = [];
			for(let i = 0; i < files.length; i++) {
				// const file = files[i];
				// if (!file.type.startsWith('image/')){ continue }
				dataImg.push(files[i]);
			}
			// console.log('dataImg: ', dataImg);
			setImages([ ...images, ...dataImg ]);
			// Q.domQ('.scroller').scrollLeft += 50;
		}
	}

	const onDeleteImages = (v, i) => {
		Swal.fire({
			icon: 'warning',
			title: 'Are you sure to delete image ' + v.name + '?', 
			showCloseButton: true,
			allowEnterKey: false,
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then(r => {
			if(r.isConfirmed){
				setImages(images.filter((f, i2) => i !== i2));
			}
		});
	}

// console.log('formik.values: ', formik.values);
	return (
		<div className="container-fluid py-3">
			<Head title="Edit Product" />
			<h5 className="hr-h hr-left mb-3">Edit Product</h5>

			<div className="row">
				<div className="col-md-4 mb-3">
					{load ? 
						<Form	noValidate 
							className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
							style={formik.isSubmitting ? { '--bg-i-load': '99px' } : null} 
							disabled={formik.isSubmitting} 
							onSubmit={formik.handleSubmit} 
							onReset={formik.handleReset}
						>
							{(img || formik.values.img) && 
								<div className="position-absolute r0 m-1">
									<Btn As="label" htmlFor="imgFile" size="sm" kind="light">Change</Btn>{" "}
									<Btn size="sm" kind="danger" className="qi qi-close" title="Remove" 
										onClick={() => {
											formik.setFieldValue("img", "");
											if(img) setImg(null);
										}}
									/>
								</div>
							}
							<label className="d-block mb-0 cpoin">
								<input type="file" accept="image/*" hidden // required 
									id="imgFile" 
									onChange={onChangeImgFile} 
								/>
								<Img 
									className="card-img-top" 
									alt={img ? img.name : "Add Product Image"} 
									src={img ? window.URL.createObjectURL(img) : "public/media/products/" + formik.values.img} 
									onLoad={e => {
										if(img){
											window.URL.revokeObjectURL(e.target.src);
										}
									}}
								/>
							</label>

							<div className="card-body py-2 mt-3-next">
								<div>
									<Flex className="mb-2">
										More Images 
										<Btn As="label" size="sm" className="btnFile ml-auto qi qi-plus" title="Add Images">
											<input type="file" accept="image/*" hidden multiple 
												id="images" 
												onChange={onChangeImgesFile} 
											/>
										</Btn>
									</Flex>

									{images.length > 0 && 
										<div className="wrap-scroller">
											<Flex nowrap className="ml-1-next ovxauto scroller">
												{images.map((v, i) => 
													<div key={i} className="position-relative">
														<Btn As="div" size="xs" kind="light" className="position-absolute mt-1 ml-1 zi-1 qi qi-trash" title="Remove" 
															onClick={() => onDeleteImages(v, i)}
														/>

														<Img // thumb 
															w={95} 
															h={65} 
															className="flexno ratio-4-3 of-cov czoomin" 
															alt={v.name ? v.name : v} 
															src={v.name ? window.URL.createObjectURL(v) : "public/media/products/" + v} 
															onLoad={e => {
																if(v.name){ 
																	window.URL.revokeObjectURL(e.target.src);
																}
															}}
															onClick={(e) => {
																const et = e.target;
																Swal.fire({
																	title: et.alt, 
																	allowEnterKey: false, 
																	showCloseButton: true, 
																	showConfirmButton: false, 
																	imageUrl: v.name ? window.URL.createObjectURL(v) : et.src, 
																	imageHeight: 400, 
																	imageAlt: et.alt, 
																	didOpen(){
																		if(v.name){
																			const img = Swal.getImage();
																			if(img) window.URL.revokeObjectURL(img.src);
																		}
																	}
																});
															}}
														/>
													</div>
												)}
											</Flex>
										</div>
									}
								</div>

								<div>
									<label htmlFor="name">Name</label>
									<Input required 
										id="name" 
										className={Q.formikValidClass(formik, "name")} 
										value={formik.values.name} 
										onChange={formik.handleChange} 
									/>

									{(formik.touched.name && formik.errors.name) && 
										<div className="invalid-feedback">{formik.errors.name}</div>
									}
								</div>

								<div>
									<label htmlFor="category_id">Category</label>
									<Dropdown>
										<Dropdown.Toggle variant="fff" tabIndex="0" 
											className={"form-control nbsp d-flex justify-content-between align-items-center cpoin" + Q.formikValidClass(formik, "category_id")} 
										>
											{formik.values.category_id} 
										</Dropdown.Toggle>
										
										<Dropdown.Menu className="bg-clip-inherit p-2 w-100 mxh-50vh ovxhide mt-1-next">
											{menus.length > 0 && 
												menus.map((v, i) => 
													v.child === "1" ? 
														<TreeQSelect key={i} 
															className="pl-3" 
															label={v.name} 
															labelProps={{
																kind: "light", 
																size: "sm", 
																className: "d-flex justify-content-between align-items-center w-100 text-left dropdown-toggle", 
																title: v.name
															}} 
															onToggle={(open) => onToggleTree(open, v)} 
														>
															{v.children && 
																<div className="btn-group-sm btn-group-vertical w-100 pt-1">
																	{v.children.map((v2, i2) => 
																		<Dropdown.Item key={i2} {...Q.DD_BTN} bsPrefix="btn btn-sm btn-light w-100 text-left" title={v2.name}
																			onClick={() => {
																				const omit = Q.omit({ 
																					...v, 
																					parent_id: v.id, 
																					parent_name: v.name, 
																					parent_slug: v.slug, 
																					...v2, 
																					product_type_id: v2.id, 
																					product_type_name: v2.name,
																					product_type_slug: v2.slug
																				}, ['id','slug','name']);

																				onSetCategory(omit);
																			}}
																		>{v2.name}</Dropdown.Item>
																	)}
																</div>
															}
														</TreeQSelect>
														: 
														<Dropdown.Item key={i} onClick={() => onSetCategory(v)} {...Q.DD_BTN} bsPrefix="btn btn-sm btn-light w-100 text-left" title={v.name}>{v.name}</Dropdown.Item>
												)
											}
										</Dropdown.Menu>
									</Dropdown>

									{(formik.touched.category_id && formik.errors.category_id) && 
										<div className="invalid-feedback d-block">{formik.errors.category_id}</div>
									}
								</div>

								<div>
									<Flex className="mb-2">
										Price
										{/* <Btn size="xs" className="ml-auto">Custom Value</Btn> */}
										<div className="custom-control custom-checkbox ml-auto">
											<input type="checkbox" className="custom-control-input" id="customVal" 
												checked={customVal} 
												onChange={e => {
													setCustomVal(e.target.checked);
													setTimeout(() => Q.domQ("#price_currency")?.focus(), 9);
												}}
											/>
											<label className="custom-control-label" htmlFor="customVal">Custom Value</label>
										</div>
									</Flex>

									<div className="input-group">
										{customVal ? 
											<Input 
												id="price_currency" 
												className={"form-control" + Q.formikValidClass(formik, "price_currency")} 
												value={formik.values.price_currency} 
												onChange={formik.handleChange} 
											/>
											: 
											<>
												<div className="input-group-prepend">
													<label htmlFor="price_currency" className="input-group-text">Rp</label>
												</div>
												<NumberFormat 
													thousandSeparator="." 
													decimalSeparator="," 
													// prefix="Rp " 

													id="price_currency" 
													inputMode="numeric" // decimal
													autoComplete="off" 
													spellCheck={false} 
													className={"form-control" + Q.formikValidClass(formik, "price_currency")} // text-right
													value={formik.values.price_currency} 
													onChange={formik.handleChange} 
												/>
											</>
										}
									</div>

									{/* {(formik.touched.price_currency && formik.errors.price_currency) && 
										<div className="invalid-feedback d-block">{formik.errors.price_currency}</div>
									} */}
								</div>

								<div>
									{/* <Btn onClick={onGetMerchants} kind="info" size="sm">Add Merchant</Btn> */}
									<label>Merchant</label>
									<div className="alert alert-light px-2 border mt-2-next">
										{merchants.map((v, i) => 
											<div key={i} className="small">
												{v.name} 
												<div className="input-group input-group-sm mt-1">
													<div className="input-group-prepend">
														<label className="input-group-text">
															<input type="checkbox" 
																defaultChecked={v.use} 
																onChange={e => {
																	const et = e.target;
																	setMerchants(merchants.map(f => (f.id === v.id ? { ...f, use: et.checked } : f ) ));
																	if(et.checked){
																		setTimeout(() => {
																			Q.domQ(".form-control", et.closest(".input-group")).focus();
																		}, 9);
																	}
																}}
															/>
														</label>
													</div>
													<Input // type="url" 
														disabled={!v.use} 
														value={v.domain} 
														onChange={e => setMerchants(merchants.map(f => (f.id === v.id ? { ...f, domain: e.target.value } : f ) ))}
													/>
												</div>
											</div>
										)}
									</div>
								</div>

								<div className="custom-control custom-checkbox mb-3">
									<input type="checkbox" className="custom-control-input" 
										id="display" 
										value="1" 
										checked={display} 
										onChange={e => setDisplay(e.target.checked)} 
									/>
									<label htmlFor="display" className="custom-control-label">Display in home</label>
								</div>

								<div className="custom-control custom-checkbox mb-3">
									<input type="checkbox" className="custom-control-input" 
										id="publish" 
										value="1" 
										checked={publish} 
										onChange={e => setPublish(e.target.checked)} 
									/>
									<label className="custom-control-label" htmlFor="publish">Publish</label>
								</div>

								<div className="text-right">
									<Btn type="reset" size="sm" kind="dark">Reset</Btn>{" "}
									<Btn type="submit" size="sm">Save</Btn>
								</div>
							</div>
						</Form>
						: 
						<Placeholder
							type="thumb" 
						/>
					}
				</div>

				<div className="col-md-8 mb-3">
					{load ? 
						<>
							<label>Description</label>
							<Editor 
								editorState={editorState} 
								toolbar={{
									options: [
										'inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 
										'link', 'remove', 'history', // , 'emoji', 'embedded', 'colorPicker', 'image'
									],
									inline: {
										inDropdown: false, 
										options: ['bold', 'italic', 'underline', 'strikethrough', 'monospace', 'superscript'], 
									}, 
									fontSize: { dropdownClassName:"q-scroll" }, 
								}}
								toolbarClassName="position-sticky t48 zi-2 bg-light" // relative
								wrapperClassName="shadow-sm" 
								editorClassName="border mt-n1px px-3 rounded-bottom ctext" 
								editorStyle={{ minHeight: 'calc(100vh - 226px)' }} 
								// localization={{
								// 	locale: "id", 
								// }}
								onEditorStateChange={(state) => {
									setEditorState(state);
								}}
							/>
						</>
						: 
						<Placeholder
							type="thumb" 
						/>
					}
				</div>
			</div>
		</div>
	);
}

/*
				<div className="row">
					<div className="col-md-4 mb-3">
						<Placeholder
							type="thumb" 
						/>
					</div>
					
					<div className="col-md-8 mb-3">
						<Placeholder
							type="thumb" 
						/>
					</div>
				</div>
*/

