import React, { useState, useEffect } from 'react';// useState, useEffect, useRef
import { useParams } from 'react-router-dom';

import Head from '../../components/q-ui-react/Head';
import Flex from '../../components/q-ui-react/Flex';
import Ava from '../../components/q-ui-react/Ava';// Img | Ava
import Aroute from '../../components/q-ui-react/Aroute';

export default function UserDetail(){
	let { id } = useParams();
	const [data, setData] = useState();

	useEffect(() => {
		// console.log('%cuseEffect in UserDetail','color:yellow;');
		// let ID = window.location.pathname.split("/");

		if(id){
			axios.get("/user-detail/" + id).then(r => { // ID[ID.length - 1]
				// console.log(r);
				if(r.data && !r.data.error){ // r.status === 200 && Q.isObj(r.data)
					const datas = Q.omit(r.data, [
						"password","forgotten_password_code","forgotten_password_time",
						"activation_code","remember_code","user_id","ip_address","salt" 
					]);
					// console.log('datas: ', datas);
					setData(datas);
				}
			}).catch(e => console.log(e));
		}
	}, []);

	return (
		<>
			{data && <Head title={"User Detail " + (data?.username?.length > 0 ? data.username : data.first_name + " " + data.last_name)} />}
			
			<div className="container py-3">
				<h5 className="hr-h hr-left mb-4">User Detail</h5>

				<div className="card shadow-sm mx-auto col col-md-8">
					<div className="card-body">
						{data ? 
							<>
								<Flex align="center" className="pb-3 mb-3 border-bottom">
									<Ava thumb w={70} h={70} wrapClass="mr-3" 
										alt={data.first_name + " " + data.last_name} 
										src={Q.baseURL + "/public/media/users/" + data.avatar} 
									/> 
									<h5 className="m-0">{data.first_name} {data.last_name}</h5>
								</Flex>

								<dl className="form-row dl-align">
									<dt className="col-4">ID</dt>
									<dd className="col-8">{data.id}</dd>

									<dt className="col-4">First Name</dt>
									<dd className="col-8">{data.first_name}</dd>

									<dt className="col-4">Last Name</dt>
									<dd className="col-8">{data.last_name}</dd>

									<dt className="col-4">Username</dt>
									<dd className="col-8">{data.username}</dd>

									<dt className="col-4">Position</dt>
									<dd className="col-8">{data.position}</dd>

									<dt className="col-4">Email</dt>
									<dd className="col-8">
										<a href={"mailto:" + data.email}>{data.email}</a>
									</dd>

									<dt className="col-4">Phone</dt>
									<dd className="col-8">{data.phone}</dd>
								</dl>

								{((USER_DATA && USER_DATA.isAdmin) || USER_DATA.id === data.id) && 
									<>
										<hr/>

										<Aroute to="/user/change-password" btn="primary">Change Password</Aroute>
									</>
								}
							</>
							:
							<div className="text-center">
								<svg className="spinner-container" width="86" height="86" viewBox="0 0 44 44">
									<circle className="spinner-path" cx="22" cy="22" r="20" fill="none" strokeWidth="4" />
								</svg>
							</div>
						}
					</div>
				</div>
			</div>
		</>
	);
}


