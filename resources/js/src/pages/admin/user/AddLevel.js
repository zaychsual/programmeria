import React, { useState, useEffect } from 'react';// useState, useEffect, useRef
import { useHistory } from 'react-router-dom';// Redirect, 
import * as Yup from 'yup';
import { Formik } from 'formik'; // useFormik, Form

import Head from '../../../components/q-ui-react/Head';
import Btn from '../../../components/q-ui-react/Btn';
// import Flex from '../../../components/q-ui-react/Flex';
import Aroute from '../../../components/q-ui-react/Aroute';
import Input from '../../../components/q-ui-react/Input';
import Textarea from '../../../components/q-ui-react/Textarea';
import Form from '../../../components/q-ui-react/Form';

export default function AddLevel(){
	const initialValues = {
		group_name: "", // level_name
		description: ""
	};
	const [data, setData] = useState(initialValues);
	const [err, setErr] = useState(null);
	const [ok, setOk] = useState(null);

	let history = useHistory();
	let ID = Q.urlSearch().get('id');

	useEffect(() => {
		if(USER_DATA && USER_DATA.isAdmin){
			console.log('%cuseEffect in AddLevel','color:yellow;');
			if(ID){
				axios.post("/get_level/" + ID).then(r => {
					console.log('r: ', r);

					let res = r.data;
					if(r.status === 200 && Q.isObj(res)){
						if(res.error){
							setErr(res.error);
						}else{
							let datas = { ...res, group_name: res.name };
							setData(datas);
						}
					}
				}).catch(e => {
					console.log(e);
					setErr(e.message);
				});
			}
		}
	}, [ID]);

	// if(userData && !userData.isAdmin) return <Redirect to="/home" />;

	return (
		<div className="container py-3">
			<Head title={(ID ? "Edit":"Add") + " level" + (ID ? " : " + data.group_name : "")} />
			<h5 className="hr-h hr-left mb-4">{ID ? "Edit":"Add"} level</h5>

			<div className="card shadow-sm mx-auto col col-md-6">
				<Formik 
					enableReinitialize 
					initialValues={data} 
					validationSchema={Yup.object().shape({
						group_name: Yup.string() // 
							.min(2, "Minimum 2 symbols")
							.max(20, "Maximum 20 symbols")
							.required("Level name is required"), 
						description: Yup.string()
							.min(2, "Minimum 2 symbols")
							.max(100, "Maximum 100 symbols")
							.required("Description is required")
					})} 
					onSubmit={(values, { setSubmitting }) => { // resetForm, setStatus, 
						let vals = ID ? 
						{
							...values, 
							group_description: values.description
						} : values;

						let action = ID ? "edit" : "create";

						axios.post("/" + action +  "_group" + (ID ? "/" + data.id : ""), Q.obj2formData(vals)).then(r => {
							console.log('r: ', r);
							let data = r.data;
							if(data && data.error){
								setErr(data.error);
								setSubmitting(false);
							}
							else{
								setOk("Success " + action + " level: " + values.group_name);
								setTimeout(() => history.push('/user/levels'), 2000);
							}
						}).catch(e => {
							console.log(e);
							setErr(e.message);
							setSubmitting(false);
						});
					}}
				>
					{formik => 
						<Form noValidate 
							className={"card-body" + (formik.isSubmitting ? " i-load cwait" : "")} 
							style={formik.isSubmitting ? { '--bg-i-load': '95px' } : null} 
							fieldsetClass="mt-3-next" 
							disabled={formik.isSubmitting} 
							onSubmit={formik.handleSubmit} 
							onReset={formik.handleReset} 
						>
							{(ok || err) && <div className={"pre-wrap alert alert-" + (ok ? "info":"danger")}>{ok || err}</div>}

							<div>
								<label htmlFor="group_name" className="small">Level name</label>
								<Input required 
									readOnly={data.id === "1" || data.id === "2"} 
									id="group_name" 
									className={Q.formikValidClass(formik, "group_name")} 
									value={formik.values.group_name} 
									onChange={formik.handleChange}
								/>

								{(formik.touched.group_name && formik.errors.group_name) && 
									<div className="invalid-feedback">{formik.errors.group_name}</div>
								}
							</div>

							<div>
								<label htmlFor="description" className="small">Description</label>
								<Textarea rows="7" required  
									id="description" 
									className={Q.formikValidClass(formik, "description")} 
									value={formik.values.description} 
									onChange={formik.handleChange}
								/>

								{(formik.touched.description && formik.errors.description) && 
									<div className="invalid-feedback">{formik.errors.description}</div>
								}
							</div>

							<div className="d-flex ml-1-next">
								<Aroute to="/user/levels" outline btn="primary" className="mr-auto">Levels</Aroute>
								<Btn type="reset" kind="dark">Reset</Btn>
								<Btn type="submit">Save</Btn>
							</div>
						</Form>
					}
				</Formik>
			</div>
		</div>
	);
}

/*

*/
