import React, { useState } from 'react';// useEffect, useRef
import * as Yup from 'yup';
import { Formik } from 'formik'; // useFormik, Form
// import { useHistory } from 'react-router-dom';

import Head from '../../../components/q-ui-react/Head';
import Btn from '../../../components/q-ui-react/Btn';
// import Input from '../../../components/q-ui-react/Input';
import Password from '../../../components/q-ui-react/Password';
// import Aroute from '../../../components/q-ui-react/Aroute';

export default function ChangePassword(){
	const initialValues = {
		old: "", 
		new: "",
		new_confirm: ""
	};
	const [err, setErr] = useState(null);
	const [ok, setOk] = useState(null);
	// const [data, setData] = useState(initialValues);

	// let history = useHistory();
	// let ID = Q.urlSearch().get('id');

	// useEffect(() => {
	// 	console.log('%cuseEffect in ChangePassword','color:yellow;');
	// 	// if(ID){
	// 		axios.post(ORIGIN + "api/change_password").then(r => {
	// 			console.log('r: ', r);

	// 			let res = r.data;
	// 			if(r.status === 200){ //  && Q.isObj(res)
	// 				if(res.error){
	// 					// setErr(res.error);

	// 				}else{
	// 					// let datas = { ...res, group_name: res.name };
	// 					// setData(datas);
	// 				}
	// 			}
	// 		}).catch(e => {
	// 			console.log(e);
	// 			setErr(e.message);
	// 		});
	// 	// }
	// }, []);

	return (
		<div className="container py-3">
			<Head title="Change Password" />
			<h5 className="hr-h hr-left mb-4">Change Password</h5>

			<div className="card shadow-sm mx-auto col-md-6">
				<div className="card-body">
					<Formik 
						enableReinitialize 
						initialValues={initialValues} // data | initialValues
						validationSchema={Yup.object().shape({
							old: Yup.string() // 
								.min(6, "Minimum 6 symbols")
								.max(20, "Maximum 20 symbols")
								.required("Old Password is required"), 
							new: Yup.string()
								.min(6, "Minimum 6 symbols")
								.max(100, "Maximum 100 symbols")
								.required("New Password is required"),
							new_confirm: Yup.string()
								.required("Confirm New Password must same")
								.test('passwords-match', 'Passwords must match', function(value){
									return this.parent.new === value;
								})
						})} 
						onSubmit={(values, { setSubmitting, resetForm }) => { // , setStatus, 
							console.log('values: ', values);
							axios.post("/change_password", Q.obj2formData(values)).then(r => {
								console.log('r: ', r);
								let res = r.data;
								if(res && !res.error){
									setOk(res.message);
									setTimeout(() => window.location.replace("auth/logout"), 2000);
								}else{
									setErr(res.error);
									setSubmitting(false);
									resetForm(initialValues);
								}
							}).catch(e => {
								console.log(e);
								setErr(e.message);
							});
						}}
					>
						{formik => 
							<form noValidate 
								onSubmit={formik.handleSubmit} 
								onReset={formik.handleReset} 
							>
								{(ok || err) && <div className={"pre-wrap alert alert-" + (ok ? "info":"danger")}>{ok || err}</div>}

								<fieldset disabled={formik.isSubmitting}>
									<div className="form-group">
										<label htmlFor="old" className="small">Old Password</label>
										{/* <Input type="password" required 
											name="old" 
											id="old" 
											className={Q.formikValidClass(formik, "old")} 
											value={formik.values.old} 
											onChange={formik.handleChange}
										/> */}
										<Password 
											id="old" 
											className={Q.formikValidClass(formik, "old")} 
											value={formik.values.old} 
											onChange={formik.handleChange} 
										/>

										{(formik.touched.old && formik.errors.old) && 
											<div className="invalid-feedback">{formik.errors.old}</div>
										}
									</div>

									<div className="form-group">
										<label htmlFor="new" className="small">New Password</label>
										{/* <Input type="password" required 
											name="new" 
											id="new" 
											className={Q.formikValidClass(formik, "new")} 
											value={formik.values.new} 
											onChange={formik.handleChange}
										/> */}
										<Password 
											id="new" 
											className={Q.formikValidClass(formik, "new")} 
											value={formik.values.new} 
											onChange={formik.handleChange} 
										/>

										{(formik.touched.new && formik.errors.new) && 
											<div className="invalid-feedback">{formik.errors.new}</div>
										}
									</div>

									<div className="form-group">
										<label htmlFor="new_confirm" className="small">Confirm New Password</label>
										{/* <Input type="password" required 
											name="new_confirm" 
											id="new_confirm" 
											className={Q.formikValidClass(formik, "new_confirm")} 
											value={formik.values.new_confirm} 
											onChange={formik.handleChange}
										/> */}
										<Password 
											id="new_confirm" 
											className={Q.formikValidClass(formik, "new_confirm")} 
											value={formik.values.new_confirm} 
											onChange={formik.handleChange} 
										/>

										{(formik.touched.new_confirm && formik.errors.new_confirm) && 
											<div className="invalid-feedback">{formik.errors.new_confirm}</div>
										}
									</div>

									<div className="text-center">
										<Btn type="reset" kind="dark">Reset</Btn>{" "}
										<Btn type="submit">Change</Btn>
									</div>
								</fieldset>
							</form>
						}
					</Formik>
				</div>
			</div>
		</div>
	);
}

/*
<React.Fragment></React.Fragment>
*/
