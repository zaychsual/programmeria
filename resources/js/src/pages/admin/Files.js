import React, { useState, useCallback, useMemo } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import { useDropzone } from 'react-dropzone';
// import * as Yup from 'yup';
// import { useFormik } from 'formik';
// import Dropdown from 'react-bootstrap/Dropdown';

import Head from '../../components/q-ui-react/Head';
import Flex from '../../components/q-ui-react/Flex';
import Input from '../../components/q-ui-react/Input';
import Img from '../../components/q-ui-react/Img';
import Btn from '../../components/q-ui-react/Btn';
import Table from '../../components/q-ui-react/Table';
import ModalQ from '../../components/q-ui-react/ModalQ';
import Form from '../../components/q-ui-react/Form';
import { getExt, noExt } from '../../utils/file/getSetExt';

import { useFetch } from '../../utils/hooks/useFetch';

const IMG_EXT = ['jpg','jpeg','png','PNG','svg','gif'];

export default function Files(){
	// const [load, setLoad] = useState(false);
  const [data, setData] = useState([]);
	const [view, setView] = useState("Grid");
	const [modal, setModal] = useState(false);
	const [dataModal, setDataModal] = useState({
		id: "", 
		name: "", 
		use_catalog: false, 
	});

	const isLoad = useFetch(
		async (cancelToken) => {
			const req = await axios.get("/get_all/files", { cancelToken });
			console.log('req: ', req);
			if(req.data && !req.data.error){// r.status === 200 && 
				setData(req.data.map(v => ({ ...v, file_size: parseFloat(v.file_size) })));
			}else{
				console.log('handle error server e: ', e);
			}
		}, 
		(err) => {
			console.log('err: ', err);
		}
	);
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Files','color:yellow;');
	// 	// Q.obj2formData({ select: "id,name,slug,category_id,category_name,product_type_id,product_type_name,img_thumb,publish" })
	// 	// axios.get("/get_all/files").then(r => {
	// 	// 	// console.log('r: ', r);
	// 	// 	const data1 = r.data;
	// 	// 	if(data1 && !data1.error){// r.status === 200 && 
	// 	// 		setData(data1.map(v => ({ ...v, file_size: parseFloat(v.file_size) })));
	// 	// 	}
	// 	// }).catch(e => {
	// 	// 	console.log('e: ', e);
	// 	// }).then(() => setLoad(true));
	// }, []);

	// const formik = useFormik({
	// 	enableReinitialize: true, 
	// 	initialValues: dataModal, 
	// 	validationSchema: Yup.object().shape({
	// 		name: Yup.string()
	// 			.min(2, "Minimum 2 symbols")
	// 			.max(255, "Maximum 255 symbols")
	// 			.required("Name is required")
	// 	}), 
	// 	onSubmit: (values, fn) => {
	// 		console.log('values: ', values);
	// 	}
	// });

  const onDrop = useCallback((acceptedFiles) => {
		let files = [];
		acceptedFiles.forEach((file) => {
			const raw_name = Q.str2slug(noExt(file.name), false);
			const file_ext = "." + getExt(file.name);
			files.push({ 
				file, 
				id: "file-" + Q.Qid(), 
				name: raw_name + file_ext, // file.name, 
				type: file.type, 
				file_size: file.size, 
				file_ext,
				raw_name
			});
		});

		// console.log('acceptedFiles: ', acceptedFiles);
		// console.log('files: ', files);
		setData([ ...data, ...files ]);
  }, [data]);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });// , noDragEventsBubbling: true

	const activeDrop = useMemo(() => {
		return isDragActive ? "active" : false
	}, [ isDragActive ]);// isDragReject, isDragAccept

	const onDel = (v) => {
		console.log('onDel v: ', v);
		Swal.fire({
			icon: "question", // warning
			title: "Are you sure to delete file " + v.name + "?",
			// text: "", 
			showCloseButton: true, 
			allowEnterKey: false, 
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes'
		}).then(r => {
			if(r.isConfirmed){
				const del = data.filter(f => f.id !== v.id);
				if(v.file){
					setData(del);
				}else{
					axios.post("/delete_by/files/id/" + v.id, Q.obj2formData({ path: v.path + v.name }))
					.then(r => {
						// console.log('r: ', r);
						if(r.data && !r.data.error){// r.status === 200
							// axios.post("/delete-file", Q.obj2formData({ path: v.path + v.name })).then(r2 => {
							// 	console.log('r2: ', r2);
							// 	if(r2.status === 200 && !r2.data.error){
							// 		console.log('r2: ', r2);
							// 	}
							// }).catch(e => {
							// 	console.log('e: ', e);
							// });

							swalToast({ icon:"success", text:"Success delete file " + v.name });
							setData(del);// Remove item
						}
					}).catch(e => {
						console.log('e: ', e);
					});
				}

				if(modal) setModal(false);
			}
		});
	}

	const onDetail = (v) => {
		// console.log('onDetail v: ',v);
		const isFileImg = v.file && IMG_EXT.includes(getExt(v.file.name));
		const isImg = v.type.startsWith("image/") || isFileImg;// (v.meta && v.meta.is_image) || isFileImg
		let imgSwal = isImg && { imageUrl: isFileImg ? window.URL.createObjectURL(v.file) : v.path + v.name, imageHeight: 400, imageAlt: v.name };
		const isPdf = v.file_ext.endsWith("pdf");// v.file_ext.includes("pdf");
		// const pdfSrc = v.file ? window.URL.createObjectURL(v.file) : v.path + v.name;
		// console.log('imgSwal: ',imgSwal);

		// axios.get(Q.baseURL + "/" + v.path + v.name, {
		// 	responseType: 'blob'
		// }).then(r => {
			
		// }).catch(e => console.log('e: ', e));

		// const fImg = async () => {
		// 	try{
		// 		const xhrImg = await axios.get(Q.baseURL + "/" + v.path + v.name, { responseType: 'blob' });
		// 		if(xhrImg.data){
		// 			console.log('xhrImg: ', xhrImg);
		// 			const reader = new FileReader();
		// 			reader.onload = function(){
		// 				const img = reader.result;
		// 				console.log('img: ', img);
		// 				imgSwal.imageUrl = img;
		// 			}
		// 			reader.readAsDataURL(xhrImg.data);
		// 		}
		// 	}catch(e){
		// 		console.log('e: ', e);
		// 	}
		// }

		// fImg();

		// function imgPreloader(){
		// 	this.images = new Array();
		// 	this.addImages = function(images){
		// 		let self = this;
		// 		if(!images) return;
		
		// 		if (Array.isArray(images)){
		// 			images.forEach(function(ele) {
		// 				let _image = new Image();
		// 				_image.src = ele;
		// 				self.images.push(_image);
		// 			});
		// 		}
		// 	};
		// 	return this;
		// }

		Swal.fire({
			title: v.name, 
			html: `${isPdf ? `<div class="embed-responsive embed-responsive-1by1"><embed class="embed-responsive-item" src="${v.file ? window.URL.createObjectURL(v.file) : v.path + v.name}"></div>`:''}<div class="alert alert-info text-left mb-2 small"><div class="row"><div class="col-md-2">Size </div><div class="col-md-10">${Q.bytes2Size(v.file_size)}</div></div><div class="row"><div class="col-md-2">Type </div><div class="col-md-10">${v.type}</div></div></div>`, 
			allowEnterKey: false, 
			showCloseButton: true, 
			showConfirmButton: false, 
			...imgSwal, 
			didOpen(){
				if(isFileImg){
					const img = Swal.getImage();
					if(img){
						window.URL.revokeObjectURL(img.src);
					}
				}
				// else{
				// 	let newImage = new imgPreloader();
				// 	newImage.addImages([v.path + v.name]);// "http://joeylea.com/img/piano.jpg"
				// 	console.log('newImage: ',newImage);
				// }

				if(isPdf && v.file){
					const swalContent = Swal.getContent();
					if(swalContent){
						window.URL.revokeObjectURL(Q.domQ("embed", swalContent).src);
					}
				}
			}
		});
	}

	const onEdit = (v) => {
		setDataModal({ ...dataModal, ...v });
		setModal(true);
	}

	const onUpload = (data, cb) => {
		let fdData = new FormData();
		fdData.append('file', data);
		fdData.append('file_size', data.size);
		axios.post('/do-upload', fdData, {
			headers: {'Content-Type': 'multipart/form-data'}
		})
		.then(r => {
			// console.log('r: ',r);
			const data = r.data;
			if(data && !data.error){// r.status === 200 && 
				cb(data);
				setTimeout(() => {
					swalToast({ icon:"success", text:"Success upload file " + data.name });
				}, 99);
			}
		})
		.catch(e => {
			console.log('e: ',e);
		});
	}

	const editFile = (val) => {
		setData(data.map(f => ( f.id === val.id ? val : { ...f, use_catalog: false } ) ));
		setModal(false);
	}

	const onClickUpload = (v) => {
		onUpload(v.file, (res) => {
			// console.log('res: ',res);
			setData(data.map(f => ( f.id === v.id ? res : f ) ));// { ...v, upload: true }
		});
	}

	return (
		<div className="container-fluid py-3">
			<Head title="Files" />
			<h5 className="hr-h hr-left mb-3">Files</h5>

			<Flex wrap justify="between" align="center" className="py-2 mb-3 bg-white border-bottom position-sticky t48 zi-4">
				<h6 className="m-0">Total Files : {data.length}</h6>
				<div className="btn-group btn-group-sm ml-auto">
					{[
						{txt:"Grid", icon:'grid'}, 
						{txt:"Table", icon:'table'}
					].map((v) => 
						<Btn key={v.txt} onClick={() => setView(v.txt)} active={v.txt === view} kind="light" 
							className={"tip tipBR qi qi-" + v.icon} 
							aria-label={"View as " + v.txt} 
						/>
					)}
				</div>
			</Flex>
			
			{isLoad && 
				<div {...getRootProps()} 
					className={Q.Cx("position-relative dropzone", { "empty": data.length < 1 }, activeDrop)} 
					style={{ minHeight: 'calc(100vh - 175px)' }}
				>
					<input {...getInputProps()} />

					<Btn As="div" block outline size="lg" className="alert alert-info">
						Drop file here or Click here to insert file
					</Btn>

					{data.length > 0 && // No Data
						<>							
							{view === "Grid" ? 
								<div className="row" onClick={e => e.stopPropagation()}>
									{data.map((v, i) => 
										<div key={i} className="col-md-3 mb-3">
											<div className="card h-100 shadow-sm">
												{v.use_catalog && 
													<div className="badge badge-primary qi qi-check position-absolute mt-1 ml-1 zi-1 shadow"> Katalog & Price List</div>
												}

												<div className="cpoin" onClick={() => onDetail(v)}>
													{IMG_EXT.includes(getExt(v.name)) ? 
														<Img 
															frame 
															// wrapAs="div" 
															frameClassPrefix="" 
															frameClass="embed-responsive embed-responsive-4by3 block" 
															alt={v.name} 
															className="embed-responsive-item card-img-top of-cov ratio-4-3 bg-light" 
															// className="card-img-top of-cov ratio-4-3 bg-light" // 16-9
															// "public/media/products/" + v.img_thumb
															src={v.file ? window.URL.createObjectURL(v.file) : v.path + v.name} 
															onLoad={e => {
																if(v.file){
																	window.URL.revokeObjectURL(e.target.src);
																}
															}}
														/>
														: 
														<div className="embed-responsive embed-responsive-4by3 block">
															<Flex justify="center" align="center" 
																className={"embed-responsive-item card-img-top of-cov ratio-4-3 py-3 bg-light i-color qi-7x qi qi-" + (getExt(v.name))} 
															/>
														</div>
													}

													<h6 className="mb-0 pt-2 px-3">{v.raw_name}{v.file_ext}</h6>
												</div>

												<Flex justify="end" className="card-body p-3">
													<div className="mt-auto">
														{v.file && 
															<Btn onClick={() => onClickUpload(v)} size="xs">Upload</Btn>
														}{" "}
														<Btn onClick={() => onDetail(v)} kind="info" size="xs">Detail</Btn>{" "}
														<Btn onClick={() => onEdit(v)} kind="warning" size="xs">Edit</Btn>{" "}
														<Btn onClick={() => onDel(v)} kind="danger" size="xs">Delete</Btn>
													</div>
												</Flex>
											</div>
										</div>
									)}
								</div>
								: 
								<Table 
									// fixThead 
									resize={false} 
									customScroll={false} 
									strip hover border sm 
									// wrapHeight="508px" // {508} 
									wrapProps={{
										onClick: e => e.stopPropagation()
									}}
									thead={
										<tr>
											{["No.", "Name", "Type", "Size", "Created at", "Action"].map((v, i) => // , "ID"
												<th key={i + v} scope="col">{v}</th>
											)}
										</tr>
									}
									tbody={
										data.map((v, i) => 
											<tr key={i}>
												<th scope="row">{i + 1}</th>
												<td>
													<a onClick={() => onDetail(v)} className="text-dark">{v.name}</a>
												</td>
												<td>{v.type}</td>
												<td>{Q.bytes2Size(v.file_size)}</td>
												<td>
													{Q.dateIntl(new Date(v.created_at || v.file?.lastModified || Date.now()), document.documentElement.lang, { dateStyle:"full", timeStyle:"short" })}	
												</td>
												<td style={{ width: 145 }}>
													<Btn onClick={() => onDetail(v)} size="xs">Detail</Btn>{" "}

													{USER_DATA.isAdmin && 
														<>
															<Btn onClick={() => onEdit(v)} size="xs" kind="warning">Edit</Btn>{" "}
															<Btn onClick={() => onDel(v)} size="xs" kind="danger">Delete</Btn>
														</>
													}
												</td>
											</tr>
										)
									}
								/>
							}
						</>
					}
				</div>
			}

			<ModalQ 
				open={modal} 
				position="center"  
				scrollable 
				// unmountOnClose={false} 
				size="lg" // lg | xl
				title={dataModal.name} // dataModal && 
				toggle={() => setModal(!modal)} 
				// bodyClass="p-0 ovhide rounded-bottom" 
				body={
					<Form noValidate 
						fieldsetClass="mt-3-next" 
						// onSubmit={formik.handleChange} 
						// onReset={formik.handleReset} 
						onSubmit={e => {
							Q.preventQ(e);
							console.log('dataModal: ', dataModal);
							const et = e.target;
							if(et.checkValidity()){
								if(dataModal.file){
									onUpload(dataModal.file, (res) => {
										console.log('res: ',res);
										editFile(dataModal);
									});
								}else{
									let newData = { ...dataModal, oldname: dataModal.name, name: dataModal.raw_name + dataModal.file_ext };
									if(newData.use_catalog){
										newData.use_catalog = 1;
									}else{
										delete newData.use_catalog;
									}

									axios.post('/edit-file', Q.obj2formData(newData)).then(r => {
										console.log('r: ',r);
									}).catch(e => {
										console.log('e: ',e);
									});
									editFile(dataModal);
								}
							}
							Q.setClass(et, "was-validated");
						}}
					>
						<div>
							<div className="input-group">
								<Input required 
									// id="name" 
									// defaultValue={noExt(dataModal.name)} 
									// value={noExt(formik.values.name)} 
									// value={formik.values.name} 
									// onChange={formik.handleChange}
									value={dataModal.raw_name} 
									onChange={e => setDataModal({ ...dataModal, raw_name: e.target.value })}
								/>
								<div className="input-group-append">
									<div className="input-group-text rounded-right">{dataModal.file_ext}</div>
								</div>
								<div className="invalid-tooltip">Please insert file name</div>
							</div>
							{/* {(formik.touched.name && formik.errors.name) && // getExt(dataModal.name)
								<div className="invalid-feedback d-block">{formik.errors.name}</div>
							} */}
						</div>

						<div className="custom-control custom-checkbox">
							<input type="checkbox" className="custom-control-input" id="useCatalog" 
								defaultChecked={dataModal.use_catalog} // checked
								onChange={e => setDataModal({ ...dataModal, use_catalog: e.target.checked })}
							/>
							<label className="custom-control-label" htmlFor="useCatalog">Use to Catalog</label>
						</div>

						<div className="text-right">
							<Btn onClick={() => onDel(dataModal)} kind="danger">Delete</Btn>{" "}
							<Btn type="submit">Save</Btn>
						</div>
					</Form>
				}
				// foot={
				// 	<>
				// 		<Btn onClick={() => setModal(false)} kind="dark" size="xs">Close</Btn>
				// 	</>
				// }
			/>
		</div>
	);
}


