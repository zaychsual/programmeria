import React, { Fragment } from 'react';// Component
// import axios from 'axios';

import Head from '../../../components/q-ui-react/Head';
import ThemeBuilder from '../../../apps/theme-builder/ThemeBuilder';

export default function ThemeBuilderPage(){
	return (
		<Fragment>
			<Head title="Theme Builder" />

			<ThemeBuilder 
				className="p-3" 
			/>
		</Fragment>
	)
}