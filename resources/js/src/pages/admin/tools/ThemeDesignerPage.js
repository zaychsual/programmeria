import React from 'react';// , { useState, useEffect }
// import Tabs from 'react-bootstrap/Tabs';

import Head from '../../../components/q-ui-react/Head';
import ThemeDesigner from '../../../apps/theme-designer/ThemeDesigner';

export default function ThemeDesignerPage(){
  // const [load, setLoad] = useState(false);

  // useEffect(() => {
  //   console.log('%cuseEffect in ThemeDesignerPage','color:yellow;');
  // }, []);

  return (
		<div className="container-fluid py-3">
			<Head title="Theme Designer" />
			{/* <h5 className="hr-h hr-left mb-4">Documentation</h5> */}
      <ThemeDesigner 

      />
    </div>
  );
}

/*

*/