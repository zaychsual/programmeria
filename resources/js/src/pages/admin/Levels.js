import React, { useState, useEffect } from 'react';// { useRef, useContext, useLayoutEffect, useMemo }
// import { Redirect } from 'react-router-dom';

import Head from '../../components/q-ui-react/Head';
import Flex from '../../components/q-ui-react/Flex';
import Table from '../../components/q-ui-react/Table';
import Aroute from '../../components/q-ui-react/Aroute';
import Btn from '../../components/q-ui-react/Btn';

export default function Levels(){
	const [data, setData] = useState([]);

	useEffect(() => {
		if(USER_DATA && USER_DATA.isAdmin){
			// console.log('%cuseEffect in Levels','color:yellow;');
			axios.get("/levels").then(r => {
				// console.log(r);
				let datas = r.data;
				if(datas && !datas.error){
					setData(datas);
				}
			}).catch(e => console.log(e));
		}
	}, []);

	const onDelete = (e, v) => {
		const et = e.target;
		et.disabled = true;

		Swal.fire({
			icon: "question", // warning
			title: "Are you sure to delete level " + v.name + "?",
			// text: "", 
			showCloseButton: true, 
			allowEnterKey: false, 
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes'
		}).then(r => {
			if(r.isConfirmed){
				axios.delete("/delete_level/" + v.id).then(r => {
					console.log('r: ', r);
					if(r.status === 200){
						let res = r.data;
						if(res && res.error){
							setErr(res.error);
						}else{
							swalToast({ icon:"success", text:"Success delete level " + v.name });
							setData(data.filter(f => f.id !== v.id));
						}
					}
				}).catch(e => {
					console.log(e);
					swalToast({ icon:"error", text:"Failed delete level " + v.name });
				}).then(() => et.disabled = false);
			}else{
				et.disabled = false;
			}
		});
	}

	// if(userData && !userData.isAdmin) return <Redirect to="/home" />;

	return (			
		<div className="container py-3">
			<Head title="Levels" />
			<h5 className="hr-h hr-left mb-4">Levels</h5>

			<Flex justify="between" align="center" className="mb-3">
				{<h6 className="m-0">Total Levels : {data.length}</h6>}
				<div>
					{/* /auth/create_user/ */}
					<Aroute to="/user/add" btn="info" size="sm" className="qi qi-plus">Add user</Aroute>{" "}
					{/* /auth/create_group/ */}
					<Aroute to="/user/create-level" btn="info" size="sm" className="qi qi-plus">Add level</Aroute>
				</div>
			</Flex>

			<Table 
				responsiveStyle={{ maxHeight: 'calc(100vh - 170px)' }} 
				fixThead 
				customScroll 
				strip 
				hover 
				border 
				sm 
				thead={
					<tr>
						{["No.", "ID", "Level name", "Description", "Action"].map((v, i) => 
							<th key={i + v} scope="col">{v}</th>
						)}
					</tr>
				}
				tbody={
					data.map((v, i) => 
						<tr key={i}>
							<th scope="row">{i + 1}</th>
							<th scope="row">{v.id}</th>
							<td>{v.name}</td>
							<td>{v.description}</td>
							<td style={{ width: 115 }}>
								<Aroute to={"/user/create-level?id=" + v.id} btn="info" size="xs">Edit</Aroute>{" "}
								{(v.name !== "admin" && v.name !== "members") && <Btn onClick={(e) => onDelete(e, v)} size="xs" kind="danger">Delete</Btn>}
							</td>
						</tr>
					)
				}
			/>
		</div>
	);
}

/*

*/
