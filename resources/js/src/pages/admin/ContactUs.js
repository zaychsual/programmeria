import React, { useState } from 'react';// { useState, useRef, useEffect }

import { useFetch } from '../../utils/hooks/useFetch';
import Head from '../../components/q-ui-react/Head';
import Btn from '../../components/q-ui-react/Btn';
import Table from '../../components/q-ui-react/Table';
import Flex from '../../components/q-ui-react/Flex';

export default function ContactUs(){
	// const [load, setLoad] = useState(false);
	const [data, setData] = useState([]);
	const [sortData, setSortData] = useState("Latest");// Oldest 

	const isLoad = useFetch(
		async (cancelToken) => {
			const req = await axios.get("/get_all/contact_us", { cancelToken });
			console.log('req: ', req);
			if(req.data && !req.data.error){// r.status === 200 && 
				let d = req.data.map(f => ( { ...f, ua: bowser.parse(f.useragent) } ));
				if(d.length > 1){
					d.reverse();
				}
				setData(d);
			}else{
				console.log('handle error server e: ', e);
			}
		}, 
		(err) => {
			console.log('err: ', err);
		}
	);

	// useEffect(() => {
	// 	const CancelToken = axios.CancelToken;
	// 	const source = CancelToken.source();
	// 	console.log('%cuseEffect in ContactUs','color:yellow');

	// 	const onXhr = async () => {
	// 		try{
	// 			const req = await axios.get("/get_all/contact_us", { cancelToken: source.token });
	// 			console.log('req: ', req);
	// 			if(req.data && !req.data.error){// r.status === 200 && 
	// 				let d = req.data.map(f => ( { ...f, ua: bowser.parse(f.useragent) } ));
	// 				if(d.length > 1){
	// 					d.reverse();
	// 				}
	// 				setData(d);
	// 			}else{
	// 				console.log('handle error server e: ', e);
	// 			}
	// 			setLoad(true);
	// 		}catch(e){
	// 			if(axios.isCancel(e)){
	// 				console.log('Request canceled e: ', e);
	// 			}else{
	// 				console.log('handle error e: ', e);
	// 			}
	// 		}
	// 	}

	// 	onXhr();

	// 	// axios.get("/get_all/contact_us", {
	// 	// 	cancelToken: source.token
	// 	// }).then(r => {
	// 	// 	console.log('r: ', r);
	// 	// 	const datas = r.data;
	// 	// 	if(datas && !datas.error){// r.status === 200 && 
	// 	// 		let d = datas.map(f => ( { ...f, ua: bowser.parse(f.useragent) } ));
	// 	// 		if(d.length > 1){
	// 	// 			d.reverse();
	// 	// 		}
	// 	// 		// console.log('d: ', d);
	// 	// 		setData(d);
	// 	// 		// isLoad = true;
	// 	// 	}
	// 	// }).catch(e => {
	// 	// 	if(axios.isCancel(e)){
	// 	// 		console.log('Request canceled message: ', e.message);
	// 	// 	}else{
	// 	// 		console.log('handle error e: ', e);
	// 	// 	}
	// 	// }).then(() => setLoad(true));

	// 	return () => {
	// 		source.cancel('Operation canceled by the user.');// cancel the request (the message parameter is optional)
	// 	}
	// }, []);// load

	const onDelete = (v) => {
		// console.log('onDelete v: ', v);
		Swal.fire({
			icon: "question", // warning
			title: "Are you sure to delete this data?",
			// text: "", 
			showCloseButton: true, 
			allowEnterKey: false, 
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes'
		}).then(r => {
			if(r.isConfirmed){
				axios.delete("/delete_by/contact_us/id/" + v.id).then(r => {
					if(r.data && !r.data.error){// r.status === 200 && 
            swalToast({ icon:"success", text:"Success delete data contact us " });
            setData(data.filter(f => f.id !== v.id));
          }
				}).catch(e => {
					console.log('e: ', e);
				});
			}
		});
	}
 
	return (
		<div className="container-fluid py-3">
			<Head title="Contact Us" />
			<h5 className="hr-h hr-left mb-4">Contact Us</h5>
			
			<Flex wrap justify="between" align="center" className="py-2 mb-3 bg-white border-bottom position-sticky t48 zi-4">
				<h6 className="m-0">Total data : {data.length}</h6>
				{data.length > 1 && 
					<div className="d-print-none">
						<Btn size="sm" kind="info" 
							onClick={() => {
								setSortData(sortData === "Oldest" ? "Latest":"Oldest");
								const newData = [ ...data ];
								setData(newData.reverse());
							}} 
						>Sort By {sortData}</Btn>
					</div>
				}
			</Flex>

			{isLoad ? // load
				data.length > 0 ? 
					<Table 
						wrapHeight="700px" // {508} 
						fixThead 
						strip 
						hover 
						border 
						sm 
						thead={
							<tr>
								{["No.", "Email", "Phone", "Message", "User Agent", "IP Address", "Action"].map((v, i) => // , "Status", "Levels"
									<th key={i + v} scope="col" className={v === "Action" ? "d-print-none" : ""}>{v}</th>
								)}
							</tr>
						}
						tbody={
							data?.map((v, i) => 
								<tr key={i}>
									<th scope="row">{i + 1}</th>
									{/* <td>{v.id}</td> */}
									<td><a href={"mailto:" + v.email}>{v.email}</a></td>
									<td>{v.phone}</td>
									<td>{v.message}</td>
									<td>
										<ul className="list-unstyled mb-0 text-capitalize">
											{Object.entries(v.ua).map((v2, i2) => 
												<li key={i2}>
													{v2[0]} :
													<ul className="small">
														{Object.entries(v2[1]).map((v3, i3) => 
															<li key={i3}>{v3[0]} : {v3[1]}</li>
														)}
													</ul>
												</li>
											)}
										</ul>
									</td>
									<td>{v.ip_address}</td>
									<td className="d-print-none" style={{ width: 100 }}>
										<Btn onClick={() => onDelete(v)} kind="danger" size="xs">Delete</Btn>
									</td>
								</tr>
							)
						}
					/>
					: 
					<div className="alert alert-info">No Data</div>
				: 
				<div>LOADING</div>
			}
		</div>
	);
}

/*
<td className="pre-wrap tab-2">{`Device :
	Type	: ${v.ua.platform.type}
	OS			: ${v.ua.os.name} ${v.ua.os.versionName}
Browser :
	Name		: ${v.ua.browser.name} 
	Version	: ${v.ua.browser.version}`}</td>
*/
