import React, { useState, useEffect } from 'react';// useState, useEffect, useRef
import { useParams } from 'react-router-dom';// Redirect, useHistory

// import Head from '../../components/q-ui-react/Head';
import UserAdd from './UserAdd';

export default function UserEdit(){
	let { id } = useParams();// Q.urlSearch().get('id');
	const [load, setLoad] = useState(false);
	const [forms, setForms] = useState({});

	useEffect(() => {
		// if(USER_DATA && USER_DATA.isAdmin){
			// console.log('%cuseEffect in UserEdit','color:yellow;');
			if(id){
				axios.get("/user-detail/" + id).then(r => {
					let res = r.data;
					// console.log('res: ', res);
					if(res && !res.error){// r.status === 200 && Q.isObj(res)
						let datas = Q.omit(res, [
							"password",
							"forgotten_password_code","forgotten_password_time",
							"activation_code","remember_code","user_id","ip_address","salt" // ,"avatar"
						]);

						// datas.current_roles = datas.current_roles.map(v => ({ ...v, checked: true }));
						// console.log('datas: ', datas);
						setForms({ ...datas, password: "", password_confirm: "" });
					}
				}).catch(e => {
					console.warn(e);
				}).then(() => setLoad(true));
			}
		// }
	}, []);

	return load ? 
		<UserAdd 
			title="Edit User" 
			action="edit" 
			dataId={forms.id}
			forms={forms} 
		/>
		: 
		null;
}


