import React, { useState, useEffect } from 'react';// { useState, useEffect, Fragment } 
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { Dropdown } from 'primereact/dropdown';

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
import Input from '../../components/q-ui-react/Input';
import ModalQ from '../../components/q-ui-react/ModalQ';

import { DATA_TYPE, COLLATIONS, DEFAULTS, ATTRIBUTES, INDEXS } from '../../data/const/SQL_DATA';

const INIT_FIELD = {
	name: "", // id
	type: "INT", 
	length: "", // 9| max_length
	default: "", // null
	collation: "", 
	attributes: "", 
	null: false, 
	AI: false, // OPTIONS | extra (AUTO_INCREMENT)
	index: "", // primary_key: 0, 
};

const FormFieldTable = ({ 
	id = "", 
	initialValues = INIT_FIELD, 
	onSubmit, 
	disabled, 
	children
}) => {

	const formikMakeTable = useFormik({
		enableReinitialize: true, 
		initialValues, 
		validationSchema: Yup.object().shape({
			name: Yup.string()
				.min(2, "Minimum 2 symbols")
				.max(20, "Maximum 20 symbols")
				.required("Name is required"), 
				// .matches(/[a-zA-Z]/g, "Only alpha & underscore character")
		}), 
		onSubmit: (values, fn) => {
			// console.log('formikMakeTable values: ', values);
			onSubmit(values, fn);
		}
	});

	// console.log('initialValues: ', initialValues);
	return (
		<Form noValidate 
			id={id} 
			// fieldsetClass="mt-3-next" 
			className={"pb-3 position-relative" + (formikMakeTable.isSubmitting ? " i-load cwait" : "")} 
			disabled={disabled} 
			onReset={formikMakeTable.handleReset} 
			onSubmit={formikMakeTable.handleSubmit} 
		>
			{children(formikMakeTable)}
		</Form>
	);
}

export default function SettingPage(){
	const initialValues = { name: "", title: "" };// , suffix: "page"
	const [load, setLoad] = useState({ list: false, form: false });
	const [data, setData] = useState([]);
	const [fields, setFields] = useState(initialValues);
  const [action, setAction] = useState("Add");
	const [modal, setModal] = useState(false);
	const [schema, setSchema] = useState([]);

	useEffect(() => {
		console.log('%cuseEffect in SettingPage','color:yellow;');
		// axios.get("/get-db-schema/pages").then(r => { //  + val.name
		// 	const data = r.data;
		// 	console.log('get-db-schema/pages data: ', data);
		// 	// if(data && !data.error){
		// 	// 	const schemas = data.filter(f => (f.name !== "id" && f.type !== "int" && f.primary_key !== 1))
		// 	// 											.map(v => {

		// 	// 											});
		// 	// 	console.log('schemas: ', schemas);
		// 	// 	setValidationSchema(schemas);
		// 	// }
		// }).catch(e => console.log('e: ', e));
  }, []);

	const onSaveField = (fd, fn, crudSchema = true) => { // values, 
		console.log('%conSaveField NOT FIX, reset form parent','color:yellow');
		axios.post("/add-edit-page", Q.obj2formData(fd))
		.then(r => {
			console.log('r: ', r);
			const err = r.data.error;
			if(err){
				if(Q.isObj(err)){
					for(let key in err){
						fn.setFieldError(key, err[key]);
					}
				}else{
					fn.setFieldError("name", err);
				}
			}else{
				if(!crudSchema){
					setFields(initialValues);// 
					fn.resetForm(initialValues);
				}

				if(action === "Add"){
					setData([ ...data, { ...r.data, date: Q.dateIntl(new Date(), LANG_CODE, {year:'numeric',month:'long',day:'numeric',hour:'numeric',minute:'numeric'}) } ]);
				}else{
					setAction("Add");
					setData(data.map(v => (v.id === fd.id ? { ...v, ...fd } : v)));
				}

				swalToast({ icon:"success", text:"Success save page " + fd.name });
			}
		})
		.catch(e => {
			console.log('e: ', e);
			swalToast({ icon:"error", text: "Failed save page", timer: 4000 });
		})
		.then(() => {
			if(!crudSchema) fn.setSubmitting(false);
		});
	}

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues: fields, 
		validationSchema: Yup.object().shape({
			name: Yup.string()
				.min(2, "Minimum 2 symbols")
				.max(30, "Maximum 30 symbols")
				.required("Name is required"), 
				// .matches(/[a-zA-Z]/g, "Only alpha & underscore character")
			// suffix: Yup.string().max(10, "Maximum 10 symbols"),
			title: Yup.string()
				.min(2, "Minimum 2 symbols")
				.max(100, "Maximum 100 symbols")
				.required("Title is required"), 
		}), 
		onSubmit: (values, fn) => {
			let fd = { 
				...values, 
				name: values.name.trim().replace(SUFFIX_FIELD_PAGE, "") + SUFFIX_FIELD_PAGE,// + "_" + values.suffix.trim(), 
				title: values.title.trim()
			};
			delete fd.date;
			delete fd.created_at;
	
			if(!fd.id){ // Add
				console.log('ADD');
				fd.schema_data = JSON.stringify([
					{
						name: "id", 
						type: "INT", 
						length: "11", 
						default: "", // "" | NULL
						collation: "", 
						attributes: "", 
						null: false, 
						AI: true, // OPTIONS | extra
						index: "PRIMARY" // primary_key: 1
					}
				]);
			}
			else{
				console.log('EDIT');
			}
			console.log('formik values: ', values);
			console.log('formik fd: ', fd);

			onSaveField(fd, fn, false);
		}
	});

	const onLoad = () => {
		if(!load.list) setLoad({ ...load, list: true });

		axios.get('/get_all/pages').then(r => {
			const datas = r.data;
			console.log('/get_all/pages datas: ', datas);
			if(datas && !datas.error){
				if(datas.length > 0){
					setData(datas.map(v => ({ ...v, date: Q.dateIntl(v.created_at, LANG_CODE, {year:'numeric',month:'long',day:'numeric',hour:'numeric',minute:'numeric'}) })));
					// setData(datas);
				}else{
					swalToast({ icon:"info", text: "No Data", timer: 5000 });
				}
			}
		})
		.catch(e => console.log('e: ', e))
		.then(() => setLoad({ ...load, list: false }));
	}

	const onClickItem = (val) => {
		setAction("Edit");
		const obj = Q.omit(val, ['date']);
		// console.log('obj: ', obj);
		setFields({ ...obj, name: obj.name.replace(SUFFIX_FIELD_PAGE, "") });//  obj
	}

	const onDelete = (v) => {
		// console.log('onDelete v: ', v);
		Swal.fire({
			icon: 'warning',
			title: 'Are you sure to delete page ' + v.name + '?',
			text: 'Data can not be restored',
			showCloseButton: true,
			allowEnterKey: false,
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then(r => {
			if(r.isConfirmed){
				setLoad({ ...load, list: true });
				axios.delete("/delete_by/pages/id/" + v.id).then(r => {
					if(r.data && !r.data.error){
						setData(data.filter(f => f.id !== v.id));
						swalToast({ icon:"success", text:"Success delete page " + v.name });
					}else{
						swalToast({ icon:"error", text: r.data.error + " page " + v.name });
					}
				})
				.catch(e => {
					console.log('e: ', e);
					swalToast({ icon:"error", text: "Failed delete page " + v.name });
				})
				.then(() => setLoad({ ...load, list: false }));
			}
		});
	}

	const onCancelEdit = () => {
		setAction("Add");
		setFields(initialValues);// formik.resetForm({});
	}

	const onGetDbSchema = () => {
		console.log('onGetDbSchema fields: ', fields);
		// /get-db-schema/
		// axios.get("/get_where_by/page_schema/name/" + fields.name + SUFFIX_FIELD_PAGE + "/row").then(r => {
		// 	const data = r.data;
		// 	console.log('/get_where_by/page_schema/name/ data: ', data);
		// 	if(data && !data.error && data.value){
		// 		setSchema(JSON.parse(data.value));
		// 		setModal(true);
		// 	}
		// }).catch(e => console.log('e: ', e));

		if(fields.schema_data){
			setSchema(JSON.parse(fields.schema_data));
		}
		setModal(true);
	}

	const onDeleteField = (idx) => {
		console.log('onDeleteField val: ', idx);
		setSchema(schema.filter((f, i) => i !== idx));
	}

	const renderField = (label, i1, form) => { // , i, i2
		const isRequired = ["name","type","length"].includes(label);// max_length
		let input;

		// <select required={isRequired} 
		// 	id={"type" + i1} 
		// 	name="type" 
		// 	className={"custom-select custom-select-sm text-uppercase" + Q.formikValidClass(form, label)} 
		// 	value={form.values.type} 
		// 	onChange={form.handleChange} 
		// >
		// 	{DATA_TYPE.map((v, i) => 
		// 		v.options ? 
		// 		<optgroup key={i} label={v.l}>
		// 			{v.options.map((v2, i2) => 
		// 				<option key={i2} value={v2.v} title={v2.t}>{v2.v}</option>
		// 			)}
		// 		</optgroup>
		// 		: 
		// 		<option key={i} value={v.v} title={v.t}>{v.v}</option>
		// 	)}
		// </select>

		switch(label){
			case "type":
				input = (
					<Dropdown 
						id={"type" + i1} 
						name="type" 
						value={form.values.type} 
						options={DATA_TYPE} 
						onChange={(v) => {
							// console.log('onChange v: ', v);
							// form.handleChange(v.target);
							form.setFieldValue("type", v.value);
						}} 
						optionLabel="l" // l | label | name
						optionValue="v" // v | value | code
						optionGroupLabel="l" // l | label
						optionGroupChildren="options" 
						filter 
						// showClear 
						showFilterClear 
						resetFilterOnHide 
						filterBy="v" // v | value | name
						filterPlaceholder="Search" // placeholder 
						// emptyFilterMessage="Not Found" 
						scrollHeight="250px" // 300px
						panelClassName="q-p-select-panel input-sm sticky-head zi-1051" // zi-upModal
						panelStyle={{
							width: 240 
						}}
						className="q-p-select input-sm w-100 text-truncate" 
						optionGroupTemplate={(option) => option.l}
						// valueTemplate={(option, props) => {
						// 	if (option) {
						// 		return (
						// 			<div title={option.t}>{option.label}</div>
						// 		);
						// 	}
						// 	return <div>{props.placeholder}</div>;// null
						// }} 
						itemTemplate={(option) => (
							<div title={option.t}>{option.l}</div>
						)} 
					/>
				);
				break;
			case "default":
				// <div className="input-group">
				input = (
					<>
						<select // required={isRequired} 
							id={"default" + i1} 
							name="default" 
							// readOnly={i2 === 0}  
							className={"custom-select custom-select-sm" + Q.formikValidClass(form, label)} 
							value={form.values.default} 
							onChange={form.handleChange} 
						>
							{DEFAULTS.map((v, i) => 
								<option key={i} value={v.v}>{v.l}</option>
							)}
						</select>
						{form.values.default === "AS DEFINED" && 
							<Input isize="sm" autoFocus />
						}
					</>
				);
				break;
			case "collation":
				input = (
					<select 
						id={"collation" + i1} 
						name="collation" 
						className={"custom-select custom-select-sm" + Q.formikValidClass(form, label)} 
						value={form.values.collation} 
						onChange={form.handleChange} 
					>
						<option />
						{COLLATIONS.map((v, i) => 
							v.options ? 
							<optgroup key={i} label={v.l}>
								{v.options.map((v2, i2) => 
									<option key={i2} value={v2.v} title={v2.t}>{v2.v}</option>
								)}
							</optgroup>
							: 
							<option key={i} value={v.v} title={v.t}>{v.v}</option>
						)}
					</select>
				);
				break;
			case "attributes":
				input = (
					<select 
						id={"attributes" + i1} 
						name="attributes" 
						className={"custom-select custom-select-sm" + Q.formikValidClass(form, label)} 
						value={form.values.attributes} 
						onChange={form.handleChange} 
					>
						{ATTRIBUTES.map((v, i) => 
							<option key={i} value={v.v}>{v.l}</option>
						)}
					</select>
				);
				break;
			case "null":
			case "AI":
				let lb = (label === "AI" ? "AI":"null");
				input = (
					<div className="custom-control custom-checkbox">
						<input type="checkbox" className="custom-control-input" 
							id={lb + i1} 
							name={lb} 
							checked={form.values[lb]} 
							onChange={form.handleChange} 
						/>
						<label htmlFor={lb + i1} className="custom-control-label" />
					</div>
				);
				break;
			case "index":
				input = (
					<select 
						id={"index" + i1} 
						name="index" 
						className={"custom-select custom-select-sm" + Q.formikValidClass(form, label)} 
						value={form.values.index} 
						onChange={form.handleChange} 
					>
						{INDEXS.map((v, i) => 
							<option key={i} value={v.v} title={v.l}>{v.l}</option>
						)}
					</select>
				);
				break;
			default:
				input = (
					<Input isize="sm" 
						required={isRequired} 
						id={label + i1} 
						name={label} 
						className={Q.formikValidClass(form, label)} 
						value={form.values[label]} 
						onChange={form.handleChange} 
						// readOnly={i2 === 0} // label === "primary_key" || value === "id"
					/>
				);
				break;
		}

		const isBool = Q.isBool(form.values[label]);
		return (
			<div key={i1}
				className={"px-1 small text-capitalize mb-" + (isBool ? "4 flexno" : "3 col-2")}
			>
				<label htmlFor={label + i1}>{label}</label>
				
				{input}

				{(isRequired && form.touched[label] && form.errors[label]) && 
					<div className="invalid-tooltip ml-1">{form.errors[label]}</div>
				}
			</div>
		);
	}

  return (
		<div className="container py-3">
			<Head title="Setting Page" />
			<h5 className="hr-h hr-left mb-4">Setting Page</h5>

			<div className="row flex-row-reverse">
				<div className="col-md-5 mb-3">
					<div className="card shadow-sm position-sticky t48 zi-1">
						<div className="card-header">
							<Btn onClick={onLoad} size="sm" kind="info" loading={load.list}>
								{data.length > 0 ? "Reload":"Show"} Pages </Btn>
						</div>

						{data.length > 0 && 
							<fieldset disabled={load.list}>
								<div className="list-group list-group-flush">
									{data.map(v => 
										<Flex key={v.id} align="center" className="list-group-item py-1 pr-1" //  list-group-item-action
											title={"Name: " + v.name + "\nTitle: " + v.title + "\nCreated at: " + v.date} 
											// title={v.title} 
											// loading={loading} 
											// onClick={() => onClickItem(v)} // c
										>
											{v.title}
											<Btn onClick={() => onClickItem(v)} size="sm" kind="flat" className="ml-auto qi qi-edit" title="Edit" />
											<Btn onClick={() => onDelete(v)} size="sm" kind="flat" className="qi qi-trash xx" title="Delete" />
										</Flex>
									)}
									
									{/* <div className="card-body">
									</div>*/}
								</div>
							</fieldset>
						}
					</div>
				</div>

				<div className="col-md-7">
					<div className="alert alert-warning">DEVS: Option created_at field, suffix field name</div>
          <Form noValidate // id="formAddTable" 
						className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
						style={formik.isSubmitting ? { '--bg-i-load': '40px' } : null} 
						disabled={formik.isSubmitting} 
						onReset={formik.handleReset} 
						onSubmit={formik.handleSubmit} 
					>
						<Flex align="center" className="card-header bg-light py-2 ml-1-next position-sticky t48 zi-2">
							<h6 className="mb-0 mr-auto">{action} Page</h6>
							{action === "Edit" && 
								<Btn onClick={onCancelEdit} size="sm" kind="warning">Cancel</Btn>
							}
							<Btn type="reset" size="sm" kind="dark">Reset</Btn>
							<Btn type="submit" size="sm">Save</Btn>
						</Flex>

            <div className="card-body">
							<div className="mb-3">
								<label htmlFor="name">Name <small>(Field)</small></label>
								<div className="input-group">
									<Input required spellCheck="false" autoComplete="off" 
										id="name" 
										className={Q.formikValidClass(formik, "name")} 
										value={formik.values.name} 
										onChange={formik.handleChange}
									/>
									<div className="input-group-append" title="Suffix">
										<label htmlFor="name" className="input-group-text">{SUFFIX_FIELD_PAGE}</label>
									</div>
								</div>

								{(formik.touched.name && formik.errors.name) && 
									<div className="invalid-feedback d-block">{formik.errors.name}</div>
								}
							</div>

							<div className="mb-3">
								<label htmlFor="title">Title <small>(Title page / menu label)</small></label>
								<Input required spellCheck="false" autoComplete="off" 
									id="title" 
									className={Q.formikValidClass(formik, "title")} 
									value={formik.values.title} 
									onChange={formik.handleChange}
								/>

								{(formik.touched.title && formik.errors.title) && 
									<div className="invalid-feedback">{formik.errors.title}</div>
								}
							</div>

							{action === "Edit" && 
								<Btn onClick={onGetDbSchema}>Edit fields</Btn>
							}
            </div>
					</Form>
				</div>
			</div>

			<ModalQ 
				open={modal} 
				backdrop="static" 
				scrollable 
				position="center" 
				size="xl" 
				title={"Setting Field - " + fields.title} 
				body={
					<div className="table-responsive">
						{schema.map((v1, i1) => 
							i1 !== 0 && 
								<FormFieldTable key={i1} 
									id={"formField" + i1} 
									initialValues={v1} 
									// id="form" 
									// fieldsetClass="mt-3-next" 
									// className={"" + (formikMakeTable.isSubmitting ? " i-load cwait" : "")} 
									disabled={i1 === 0} 
									// onReset={formikMakeTable.handleReset} 
									onSubmit={(values, fn) => {
										const newSchema = schema.map((v, i) => (i === i1 ? values : v));
										let fd = {
											...fields, 
											tb_name: fields.name.trim() + SUFFIX_FIELD_PAGE, 
											schema_data: JSON.stringify(newSchema)
										};
										// delete fd.title;
										// delete fd.created_at;
										// if(Q.isBool(fd.default)){

										// }

										if(fd.id){
											console.log('EDIT');
											const schema_tb = newSchema.filter(f => f.name !== "id");
											// .map(v => (
											// 	{
											// 		[v.name] : { ...v }
											// 	}
											// ))
											fd.schema_tb = JSON.stringify(schema_tb);
										}

										console.log('formikMakeTable values: ', values);
										console.log('formikMakeTable schema: ', schema);
										console.log('formikMakeTable fields: ', fields);
										console.log('formikMakeTable fd: ', fd);

										onSaveField(fd, fn);
										// setModal(false);
									}} 
								>
									{form => (
										<Flex nowrap dir="row" align="center" className="ml-1-next">
											{Object.entries(v1).map((v, i) => // form.values
												renderField(v[0], (i + "-" + i1), form) // v[0], v[1], (i + "-" + i1), form
											)}

											<div className="btn-group btn-group-sm mt-2 position-sticky r0 zi-2">
												<Btn onClick={() => onDeleteField(i1)} kind="danger">Delete</Btn>
												<Btn type="reset" kind="dark">Reset</Btn>
												<Btn type="submit">Save</Btn>
											</div>
											{/* {i1 !== 0 && <Btn type="submit" size="sm" className="mt-2 position-sticky r0 zi-2">Save</Btn>} */}
										</Flex>
									)}
								</FormFieldTable>
						)}

						{schema.length - 1 < 1 && 
							<div className="alert alert-info m-0">No Field</div>
						}
					</div>
				} 
				foot={
					<>
						<Btn onClick={() => {
							console.log('onAddField schema: ', schema);
							setSchema([
								...schema, 
								INIT_FIELD
							]);
						}}>Add Field</Btn>{" "}
						<Btn onClick={() => setModal(false)} kind="dark">Cancel</Btn>
					</>
				}
			/>
		</div>
  );
}

/*
<div className="form-row">
                <div className="col-12 col-lg-8 mb-3">
                  <label htmlFor="name">Name <small>(Field)</small></label>
                  <Input required spellCheck="false" autoComplete="off" 
                    id="name" 
                    className={Q.formikValidClass(formik, "name")} 
                    value={formik.values.name} 
                    onChange={formik.handleChange}
                  />

                  {(formik.touched.name && formik.errors.name) && 
                    <div className="invalid-feedback">{formik.errors.name}</div>
                  }
                </div>
                
                <div className="col-12 col-lg-4 mb-3">
                  <label htmlFor="suffix">Suffix <small>(Suffix field)</small></label>
                  <Input required spellCheck="false" 
                    id="suffix" 
                    className={Q.formikValidClass(formik, "suffix")} 
                    value={formik.values.suffix} 
                    onChange={formik.handleChange}
                  />

                  {(formik.touched.suffix && formik.errors.suffix) && 
                    <div className="invalid-feedback">{formik.errors.suffix}</div>
                  }
                </div>
              </div>

		// axios.get('/get-tables').then(r => {
		// 	const data = r.data;
		// 	console.log('get-tables data: ', data);
		// 	// if(data && !data.error){
				
		// 	// }
		// }).catch(e => console.log('e: ', e));
*/