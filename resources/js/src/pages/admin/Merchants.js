import React, { useRef, useState, useEffect } from 'react';// { useState, useRef, useEffect }
import * as Yup from 'yup';
import { useFormik } from 'formik';

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Btn from '../../components/q-ui-react/Btn';
import Input from '../../components/q-ui-react/Input';
import Flex from '../../components/q-ui-react/Flex';

export default function Merchants(){
	const initialValues = {
		name: "", 
		domain: "", 
		img: ""
	};
	const inputRef = useRef();

	const [load, setLoad] = useState(false);
	const [err, setErr] = useState(false);
	const [data, setData] = useState([]);
	const [forms, setForms] = useState(initialValues);
	const [file, setFile] = useState({});

	const loadData = (cb) => { // 
		axios.get("/get_all/merchants").then(r => {
			// console.log('r: ', r);
			if(r.data && !r.data.error){// r.status === 200 && 
				setData(r.data);
				if(cb) cb(r.data);
			}else{
				setErr(true);
			}
		}).catch(e => {
			console.log('e: ', e);
			setErr(true);
		}).then(() => {
			if(!load) setLoad(true);
		});
	}

	useEffect(() => {
		// console.log('%cuseEffect in Merchants','color:yellow');
		loadData();
		// axios.get('/get_all/merchants').then(r => {
		// 	console.log('r: ', r);
		// 	const datas = r.data;
		// 	if(datas && datas && !datas.error){// r.status === 200
		// 		setData(datas);
		// 	}
		// })
		// .catch(e => console.log('e: ',e))
		// .then(() => setLoad(true));
	}, []);

	const validationSchema = Yup.object().shape({
		name: Yup.string()
			.min(2, "Minimum 2 symbols")
			.max(100, "Maximum 100 symbols")
			.required("Name is required"),
		domain: Yup.string()
			.min(5, "Minimum 5 symbols")
			.max(100, "Maximum 100 symbols")
			.required("Domain is required"), 
		img: Yup.string()
			.max(255, "Maximum 255 symbols")
			.required("Image is required"), 
	});

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues: forms, 
		validationSchema, 
		onSubmit: (values, fn) => {
			let newVal = {
				...values, 
			};

			if(file.name){
				newVal.file = file;
				delete newVal.img;
			}
			if(!newVal.img){
				delete newVal.img;
			}

			console.log('values: ', values);
			console.log('newVal: ', newVal);

			axios.post('/crud-merchant', Q.obj2formData(newVal)).then(r => {
				console.log('r: ', r);
				if(r.data && !r.data.error){// r.status === 200 && 
					swalToast({ icon:"success", text:"Success save merchant " + newVal.name });
					loadData();
					setForms(initialValues);
					setFile({});
					fn.resetForm(initialValues);
				}
			}).catch(e => {
				console.log('e: ', e);
			}).then(() => fn.setSubmitting(false));
		}
	});

	const onChangeFile = e => {
		const f = e.target.files[0];
		if(f){
			setFile(f);
			formik.setFieldValue("img", f.name);
		}
	}

	const onClickListMenu = (item) => {
		if(item.id !== forms.id){
			setForms(item);
		}
		inputRef.current.focus();
	}

	const onDelete = (v) => {
		console.log('onDelete v: ', v);
		Swal.fire({
			icon: "question", // warning
			title: "Are you sure to delete merchant " + v.name + "?", 
			showCloseButton: true, 
			allowEnterKey: false, 
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes'
		}).then(r => {
			if(r.isConfirmed){
				const del = data.filter(f => f.id !== v.id);
				if(v.file){
					setData(del);
				}else{
					axios.post("/delete_by/merchants/id/" + v.id, Q.obj2formData({ path: "public/media/merchants/" + v.img }))
					.then(r => {
						console.log('r: ', r);
						if(r.data && !r.data.error){// r.status === 200 && 
							swalToast({ icon:"success", text:"Success delete merchant " + v.name });
							setData(del);
						}
					}).catch(e => {
						console.log('e: ', e);
					});
				}
			}
		});
	}

	const onDetailSwal = () => {
		const fname = file.name || formik.values.img;
		if(fname){
			Swal.fire({
				title: fname, 
				allowEnterKey: false, 
				showCloseButton: true, 
				showConfirmButton: false, 
				imageUrl: file.name ? window.URL.createObjectURL(file) : "public/media/merchants/" + formik.values.img, 
				imageHeight: 400, 
				imageAlt: fname, 
				didOpen(){
					if(file.name){
						const img = Swal.getImage();
						if(img) window.URL.revokeObjectURL(img.src);
					}
				}
			});
		}
	}

	return (
		<div className="container-fluid py-3">
			<Head title="Merchants" />
			<h5 className="hr-h hr-left mb-3">Merchants</h5>

			<div className="row">
				<div className="col-12 col-md-5">
					{load ? 
						data.length > 0 ? 
							<>
								<h6>Choose Merchant to edit :</h6>
								<div className="list-group shadow-sm">
									{data.map((v, i) => 
										<Flex key={i} justify="between" align="center" 
											className={Q.Cx("list-group-item list-group-item-action py-0 pr-3 pl-0 cpoin", { "active": v.id === forms.id })} 
										>
											<div onClick={() => onClickListMenu(v)} className="flex1 py-1 px-3 select-no" title={v.name}>{v.name}</div>
											<Btn onClick={() => onDelete(v)} blur size="xs" kind="light" className="tip tipL qi qi-close xx" aria-label="Remove"  />
										</Flex>
									)}
								</div>
							</>
							: 
							<Flex justify="between" align="center" className={"alert alert-" + (err ? "danger" : "info")}>
								{err ? <>Error get data. <Btn onClick={loadData}>Reload</Btn></> : "No Merchants Data"}
							</Flex>
						:
						<div>LOADING</div>
					}
				</div>

				<div className="col-12 col-md-7">
					<div className="card shadow-sm">
						<Form	noValidate 
							className={"card-body bg-light" + (formik.isSubmitting ? " i-load cwait" : "")} 
							style={formik.isSubmitting ? { '--bg-i-load': '99px' } : null} 
							fieldsetClass="mt-3-next" 
							disabled={formik.isSubmitting} 
							onSubmit={formik.handleSubmit} 
							// onReset={formik.handleReset}
							onReset={e => {
								formik.handleReset(e);
								setForms(initialValues);
								setFile({});
							}}
						>
							<h5 className="hr-h hr-left mb-4">{forms.id ? "Edit" : "Add"} Merchant</h5>

							<div>
								<label htmlFor="name">Merchant Name</label>
								<Input id="name" 
									ref={inputRef} 
									spellCheck={false} 
									className={Q.formikValidClass(formik, "name")} 
									value={formik.values.name} 
									onChange={formik.handleChange} 
								/>

								{(formik.touched.name && formik.errors.name) && 
									<div className="invalid-feedback">{formik.errors.name}</div>
								}
							</div>

							<div>
								<label htmlFor="domain">Merchant Domain</label>
								<Input id="domain" 
									className={Q.formikValidClass(formik, "domain")} 
									value={formik.values.domain} 
									onChange={formik.handleChange} 
								/>

								{(formik.touched.domain && formik.errors.domain) && 
									<div className="invalid-feedback">{formik.errors.domain}</div>
								}
							</div>

							<div>
								<label htmlFor="img">Merchant image</label>
								<div className="input-group">
									<Btn As="label" kind="fff" tabIndex="0" 
										className={"form-control text-left" + Q.formikValidClass(formik, "img")}
									>
										{file.name || formik.values.img || "Choose image"} 
										<input onChange={onChangeFile} id="img" type="file" accept="image/*" hidden />
									</Btn>
									{(file.name || formik.values.img) && 
										<div className="input-group-append">
											<Btn kind="light" title="Remove" className="qi qi-close xx" 
												onClick={() => {
													setFile({});
													formik.setFieldValue("img","");
												}}
											/>
											<Btn onClick={onDetailSwal} kind="light" title="View" className="qi qi-img" />
										</div>
									}
								</div>

								{(formik.touched.img && formik.errors.img) && 
									<div className="invalid-feedback d-block">{formik.errors.img}</div>
								}
							</div>
							
							<div className="text-right">
								<Btn type="reset" kind="dark">Reset</Btn>{" "}
								<Btn type="submit">Save</Btn>
							</div>
						</Form>
					</div>
				</div>
			</div>
		</div>
	);
}
