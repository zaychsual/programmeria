import React, { useEffect } from 'react';// { useState, useRef, useEffect }

import Head from '../../../../components/q-ui-react/Head';
// import Btn from '../../../components/q-ui-react/Btn';
// import Aroute from '../../../components/q-ui-react/Aroute';
// import Placeholder from '../../../components/q-ui-react/Placeholder';

import Highcharts from '../../../../components/q-ui-react/Highcharts';

const HIGHTCHART_DATA = {
	credits: {
		// enabled: false,
		text: 'programmeria.com',
		href: 'https://programmeria.com'
	},
	title: {
    text: 'My chart'
  }, 
	series: [{
		type: 'column',
		name: 'Jane',
		data: [3, 2, 1, 3, 4]
	}, {
		type: 'column',
		name: 'John',
		data: [2, 3, 5, 7, 6]
	}, {
		type: 'column',
		name: 'Joe',
		data: [4, 3, 3, 9, 0]
	}, {
		type: 'spline',
		name: 'Average',
		data: [3, 2.67, 3, 6.33, 3.33]
	}]
};

export default function HighchartsPage(){


	useEffect(() => {
		console.log('%cuseEffect in HighchartsPage','color:yellow');
		// [
		// 	'highcharts/modules/exporting.js', 
		// 	'highcharts/modules/export-data.js'
		// ].forEach(v => {
		// 	import("" + v).then((module) => {
		// 		console.log('module: ', module);
		// 	});
		// });

		// import("highcharts/modules/exporting.js").then((module) => {
		// 	console.log('module: ', module);
		// });
		// import("highcharts/modules/export-data.js").then((module) => {
		// 	console.log('module: ', module);
		// });
	}, []);

	return (
		<div className="container py-3">
			<Head title="Highcharts" />

			<h4>{"<Highcharts />"}</h4>

			<div>
				<Highcharts 
					options={HIGHTCHART_DATA}
				/>
			</div>
			
		</div>
	);
}

/*

*/