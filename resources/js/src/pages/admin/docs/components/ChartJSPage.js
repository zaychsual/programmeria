import React, { useEffect } from 'react';// { useState, useRef, useEffect }

import Head from '../../../../components/q-ui-react/Head';
// import Btn from '../../../components/q-ui-react/Btn';
// import Aroute from '../../../components/q-ui-react/Aroute';
// import Placeholder from '../../../components/q-ui-react/Placeholder';

import { ChartJS } from '../../../../components/q-ui-react/ChartJS';

const CHART_DATA = {
	labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
	datasets: [{
		type: 'line',
		label: 'Dataset 1',
		borderColor: '#42A5F5',
		borderWidth: 2,
		fill: false,
		data: [ 50, 25, 12, 48, 56, 76, 42 ]
	}, {
		type: 'bar',
		label: 'Dataset 2',
		backgroundColor: '#66BB6A',
		data: [ 21, 84, 24, 75, 37, 65, 34 ],
		borderColor: 'white',
		borderWidth: 2
	}, {
		type: 'bar',
		label: 'Dataset 3',
		backgroundColor: '#FFA726',
		data: [ 41, 52, 24, 74, 23, 21, 32 ]
	}]
};

export default function ChartJSPage(){
	useEffect(() => {
		console.log('%cuseEffect in ChartJSPage','color:yellow');
	}, []);

	return (
		<div className="container py-3">
			<Head title="ChartJS" />

			<h4>{"<ChartJS />"}</h4>

			<div>
				<ChartJS 
					type="bar" 
					data={CHART_DATA} 
					options={{
						legend: {
							labels: {
								fontColor: '#495057'
							}
						},
						scales: {
							xAxes: [{
								ticks: {
									fontColor: '#495057'
								}
							}],
							yAxes: [{
								ticks: {
									fontColor: '#495057'
								}
							}]
						}
					}} 
				/>
			</div>
			
		</div>
	);
}

/*

*/