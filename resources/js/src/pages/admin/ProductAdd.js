import React, { useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import { useHistory } from 'react-router-dom';
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
import Dropdown from 'react-bootstrap/Dropdown';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
// import htmlToDraft from 'html-to-draftjs';
// import DOMPurify from 'dompurify';

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Btn from '../../components/q-ui-react/Btn';
import Input from '../../components/q-ui-react/Input';
// import Textarea from '../../components/q-ui-react/Textarea';
import Flex from '../../components/q-ui-react/Flex';
import Img from '../../components/q-ui-react/Img';
import NumberFormat from '../../components/react-number-format/NumberFormat';
import { TreeQSelect } from '../../components/TreeQSelect';

export default function ProductAdd(){
	const history = useHistory();
  const [menus, setMenus] = useState([]);
	const [categoryVal, setCategoryVal] = useState("");
	const [publish, setPublish] = useState(true);
	const [display, setDisplay] = useState(false);
	const [img, setImg] = useState();
	const [images, setImages] = useState([]);
	const [merchants, setMerchants] = useState([]);
	// const [myMerchants, setMyMerchants] = useState([]);
	const [editorState, setEditorState] = useState(EditorState.createEmpty());
	const [customVal, setCustomVal] = useState(false);
	
	useEffect(() => {
		console.log('%cuseEffect in ProductAdd','color:yellow;');
		axios.post("/get_all/category", Q.obj2formData({ select: "id,name,slug,child" })).then(r => {
			console.log('category r: ', r);
			const data1 = r.data;
			if(r.status === 200 && data1 && !data1.error){
				setMenus(data1);
			}
		}).catch(e => {
			console.log('e: ', e);
		});

		axios.post("/get_all/merchants", Q.obj2formData({ select: "id,name,domain,img" })).then(r => {
			console.log('merchants r: ', r);
			if(r.status === 200 && r.data && !r.data.error){
				setMerchants(r.data);
			}
		}).catch(e => {
			console.log('e: ', e);
		});
	}, []);

	const initialValues = {
		name: "", 
		category_id: "", 
		price_currency: "", 
	};

	const validationSchema = Yup.object().shape({
		name: Yup.string()
			.min(2, "Minimum 2 symbols")
			.max(255, "Maximum 255 symbols")
			.required("Name is required"),
		category_id: Yup.string().required("Kategori is required"), 
		price_currency: Yup.string()
			.max(100, "Maximum 100 symbols")
			// .required("Harga is required"), 
	});

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues, 
		validationSchema, 
		onSubmit: (values, fn) => {
			let newVal = {
				...values, 
				...categoryVal, 
				slug: Q.str2slug(values.name), 
				publish: publish ? 1 : 0, 
				display: display ? 1 : 0, 
				// price: parseFloat(values.price_currency.replace(/Rp |\./g, '')), 
				created_by: USER_DATA[0].id
			};

			if(newVal.price_currency && !customVal){
				newVal.price = parseFloat(values.price_currency.replace(/Rp |\./g, ''));
			}

			if(img){
				newVal.file = img;
			}

			const draftVal = editorState.getCurrentContent();
			if(draftVal.hasText()){// DOMPurify.sanitize(html)
				newVal.description = draftToHtml(convertToRaw(draftVal)).trim();
			}

			let fd = Q.obj2formData(newVal);

			if(images.length > 0){
				images.forEach(f => {
					fd.append('files[]', f);
				});
			}

			if(merchants.length > 0){
				const uses = merchants.filter(f => f.use);
				if(uses.length > 0){
					fd.append('merchant', uses.length);// "1"
					uses.forEach(f => {
						fd.append('merchants[]', JSON.stringify({ name: f.name, url: f.domain, img: f.img }));
					});
				}
			}

			console.log('values: ', values);
			console.log('newVal: ', newVal);
			console.log('img: ', img);
			console.log('images: ', images);
			console.log('merchants: ', merchants);
			// for(let pair of fd.entries()){
			// 	console.log(pair[0]+ ': ', pair[1]);
			// 	console.log('%cMERCHANTS: ', 'color:yellow', pair[1]);
			// }

			axios.post("/add-product", fd, {
				headers: {'Content-Type': 'multipart/form-data'}
			}).then(r => {
				console.log('add-product r: ', r);
				const data = r.data;
				if(data && !data.error){// r.status === 200 && 
					swalToast({ 
						icon:"success", 
						text:"Success save product " + newVal.name + ".", 
						willClose: () => history.push("/product/all") 
					});
					// 	fn.resetForm(initialValues);
					// 	// fn.setSubmitting(false);
					// }else{
					// 	const fieldErr = data.error.toLowerCase().split(" ").shift();
					// 	// console.log('fieldErr: ', fieldErr);
					// 	if(fieldErr && Object.keys(newVal).includes(fieldErr)){
					// 		fn.setFieldError(fieldErr, "Produk " + data.error);
					// 	}
				}
			}).catch(e => {
				console.log('e: ', e);
			}).then(() => fn.setSubmitting(false));
		}
	});

	const onToggleTree = (open, v) => {
		if(open && !v.children){
			axios.post("/get_where_by/product_type/parent/" + v.id, Q.obj2formData({ select: "id,name,slug,parent" })).then(r => {
				// console.log('r: ', r);
				const datas = r.data;
				if(r.status === 200 && datas && !datas.error){
					setMenus(menus.map(f => ( f.id === v.id ? { ...f, children: datas } : f )));
				}
			}).catch(e => {
				console.log('e: ', e);
			});
		}
	}

	const onSetCategory = (v) => {
		// console.log('onSetCategory v: ', v);
		const val = v.parent ? v.parent_name + ", " + v.product_type_name : v.name;
		formik.setFieldValue("category_id", val);
		// formik.setFieldValue("product_type", v.parent ? v.product_type_id : v.id);
		setCategoryVal(v.parent ? { category_id: v.parent_id, category_slug: v.parent_slug, category_name: v.parent_name, product_type_id: v.product_type_id, product_type_slug: v.product_type_slug, product_type_name: v.product_type_name } : { category_id: v.id, category_slug: v.slug, category_name: v.name });
	}

	const onChangeImgFile = (e) => {
		const file = e.target.files[0];
		// const file = et.files[0];
		if(file){
			setImg(file);
			// setModalData({ ...modalData, file });
			// if(errorImg) setErrorImg(null);
		}
	}

	const onChangeImagesFile = (e) => {
		const files = e.target.files;
		if(files && files.length > 0){
			let dataImg = [];
			for(let i = 0; i < files.length; i++) {
				// const file = files[i];
				// if (!file.type.startsWith('image/')){ continue }
				dataImg.push(files[i]);
			}
			// console.log('dataImg: ', dataImg);
			setImages([ ...images, ...dataImg ]);
		}
	}

	const onDeleteImages = (v, i) => {
		Swal.fire({
			icon: 'warning',
			title: 'Are you sure to delete image ' + v.name + '?', 
			showCloseButton: true,
			allowEnterKey: false,
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then(r => {
			if(r.isConfirmed){
				setImages(images.filter((f, i2) => i !== i2));
			}
		});
	}

	return (
		<div className="container-fluid py-3">
			<Head title="Add Product" />
			<h5 className="hr-h hr-left mb-3">Add Product</h5>

			<div className="row">
				<div className="col-md-4">
					<Form	noValidate 
						className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
						style={formik.isSubmitting ? { '--bg-i-load': '99px' } : null} 
						disabled={formik.isSubmitting} 
						onSubmit={formik.handleSubmit} 
						onReset={formik.handleReset}
					>
						{img && 
							<div className="position-absolute r0 m-1">
								<Btn As="label" htmlFor="imgFile" size="sm" kind="light">Change</Btn>{" "}
								<Btn size="sm" kind="danger" className="qi qi-close" title="Remove" 
									onClick={() => setImg(null)}
								/>
							</div>
						}
						<label className="d-block mb-0 cpoin">						
							<input type="file" accept="image/*" hidden // required 
								id="imgFile" 
								onChange={onChangeImgFile} 
							/>
							<Img 
								className="card-img-top border-bottom" 
								alt={img ? img.name : "Add Product Image"} 
								src={img ? window.URL.createObjectURL(img) : ""} 
								onLoad={e => {
									if(img){
										window.URL.revokeObjectURL(e.target.src);
									}
								}}
							/>
						</label>

						<div className="card-body py-2 mt-3-next">
							<div>
								<Flex className="mb-2">
									More Images 
									<Btn As="label" size="sm" className="btnFile ml-auto qi qi-plus" title="Add Images">
										<input type="file" accept="image/*" hidden multiple 
											id="images" 
											onChange={onChangeImagesFile} 
										/>
									</Btn>
								</Flex>

								{images.length > 0 && 
									<div className="wrap-scroller">
										<Flex nowrap className="ml-1-next ovxauto scroller">
											{images.map((v, i) => 
												<div key={i} className="position-relative">
													<Btn As="div" size="xs" kind="light" className="position-absolute mt-1 ml-1 zi-1 qi qi-trash" title="Remove" 
														onClick={() => onDeleteImages(v, i)}
													/>

													<Img // thumb 
														w={95} 
														h={65} 
														className="flexno ratio-4-3 of-cov czoomin" 
														alt={v.name || "Images " + i} 
														src={v.name ? window.URL.createObjectURL(v) : ""} 
														onLoad={e => {
															if(v){
																window.URL.revokeObjectURL(e.target.src);
															}
														}}
														onClick={(e) => {
															const et = e.target;
															Swal.fire({
																title: et.alt, 
																allowEnterKey: false, 
																showCloseButton: true, 
																showConfirmButton: false, 
																imageUrl: window.URL.createObjectURL(v), // v.name ? window.URL.createObjectURL(v) : et.src, 
																imageHeight: 400, 
																imageAlt: et.alt,
																didOpen(){
																	const img = Swal.getImage();
																	if(img) window.URL.revokeObjectURL(img.src);
																}
															});
														}}
													/>
												</div>
											)}
										</Flex>
									</div>
								}
							</div>

							<div>
								<label htmlFor="name">Name</label>
								<Input isize="sm" required 
									id="name" 
									className={Q.formikValidClass(formik, "name")} 
									// {...formik.getFieldProps("name")} 
									value={formik.values.name} 
									onChange={formik.handleChange} 
									// onBlur={formik.handleBlur} 
								/>

								{(formik.touched.name && formik.errors.name) && 
									<div className="invalid-feedback">{formik.errors.name}</div>
								}
							</div>

							<div>
								<label htmlFor="category_id">Category</label>
								<Dropdown>
									<Dropdown.Toggle variant="fff" // tabIndex="0" 
										className={"form-control form-control-sm nbsp d-flex justify-content-between align-items-center cpoin" + Q.formikValidClass(formik, "category_id")} 
									>
										{formik.values.category_id} 
									</Dropdown.Toggle>
									
									<Dropdown.Menu className="bg-clip-inherit p-2 w-100 mxh-50vh ovxhide mt-1-next">
										{menus.length > 0 && 
											menus.map((v, i) => 
												v.child === "1" ? 
													<TreeQSelect key={i} 
														className="pl-3" 
														label={v.name} 
														labelProps={{
															kind: "light", 
															size: "sm", 
															className: "d-flex justify-content-between align-items-center w-100 text-left dropdown-toggle", 
															title: v.name
														}} 
														onToggle={(open) => onToggleTree(open, v)} 
													>
														{v.children && 
															<div className="btn-group-sm btn-group-vertical w-100 pt-1">
																{v.children.map((v2, i2) => 
																	<Dropdown.Item key={i2} {...Q.DD_BTN} bsPrefix="btn btn-sm btn-light w-100 text-left" title={v2.name}
																		onClick={() => {
																			const omit = Q.omit({ 
																				...v, 
																				parent_id: v.id, 
																				parent_name: v.name, 
																				parent_slug: v.slug, 
																				...v2, 
																				product_type_id: v2.id, 
																				product_type_name: v2.name,
																				product_type_slug: v2.slug
																			}, ['id','slug','name']);

																			onSetCategory(omit);
																		}}
																	>{v2.name}</Dropdown.Item>
																)}
															</div>
														}
													</TreeQSelect>
													: 
													<Dropdown.Item key={i} onClick={() => onSetCategory(v)} {...Q.DD_BTN} bsPrefix="btn btn-sm btn-light w-100 text-left" title={v.name}>{v.name}</Dropdown.Item>
											)
										}
									</Dropdown.Menu>
								</Dropdown>

								{(formik.touched.category_id && formik.errors.category_id) && 
									<div className="invalid-feedback d-block">{formik.errors.category_id}</div>
								}
							</div>

							<div>
								<Flex className="mb-2">
									Price
									{/* <Btn size="xs" className="ml-auto">Custom Value</Btn> */}
									<div className="custom-control custom-checkbox ml-auto">
										<input type="checkbox" className="custom-control-input" id="customVal" 
											onChange={e => {
												setCustomVal(e.target.checked);
												setTimeout(() => Q.domQ("#price_currency")?.focus(), 9);
											}}
										/>
										<label className="custom-control-label" htmlFor="customVal">Custom Value</label>
									</div>
								</Flex>

								<div className="input-group">
									{customVal ? 
										<Input 
											id="price_currency" 
											className={"form-control" + Q.formikValidClass(formik, "price_currency")} 
											value={formik.values.price_currency} 
											onChange={formik.handleChange} 
										/>
										: 
										<>
											<div className="input-group-prepend">
												<label htmlFor="price_currency" className="input-group-text">Rp</label>
											</div>
											<NumberFormat 
												thousandSeparator="." 
												decimalSeparator="," 
												// prefix="Rp " 

												id="price_currency" 
												inputMode="numeric" // decimal
												autoComplete="off" 
												spellCheck={false} 
												className={"form-control" + Q.formikValidClass(formik, "price_currency")} // text-right
												value={formik.values.price_currency} 
												onChange={formik.handleChange} 
											/>
										</>
									}
								</div>

								{/* {(formik.touched.price_currency && formik.errors.price_currency) && 
									<div className="invalid-feedback d-block">{formik.errors.price_currency}</div>
								} */}
							</div>

							<div>
								{/* <Btn onClick={onGetMerchants} kind="info" size="sm">Add Merchant</Btn> */}
								<label>Merchant</label>
								<div className="alert alert-light px-2 border mt-2-next">
									{merchants.map((v, i) => 
										<div key={i} className="small">
											{v.name} 
											<div className="input-group input-group-sm mt-1">
												<div className="input-group-prepend">
													<label className="input-group-text">
														<input type="checkbox" 
															defaultChecked={v.use} 
															onChange={e => {
																const et = e.target;
																setMerchants(merchants.map(f => (f.id === v.id ? { ...f, use: et.checked } : f ) ));
																if(et.checked){
																	setTimeout(() => {
																		Q.domQ(".form-control", et.closest(".input-group")).focus();
																	}, 9);
																}
															}}
														/>
													</label>
												</div>
												<Input // type="url" 
													disabled={!v.use} 
													value={v.domain} 
													onChange={e => setMerchants(merchants.map(f => (f.id === v.id ? { ...f, domain: e.target.value } : f ) ))}
												/>
											</div>
										</div>
									)}
								</div>
							</div>

							<div className="custom-control custom-checkbox mb-3">
								<input type="checkbox" className="custom-control-input" 
									id="display" 
									value="1" 
									checked={display} 
									onChange={e => setDisplay(e.target.checked)} 
								/>
								<label htmlFor="display" className="custom-control-label">Display in home</label>
							</div>

							<div className="custom-control custom-checkbox mb-3">
								<input type="checkbox" className="custom-control-input" 
									id="publish" 
									value="1" 
									checked={publish} 
									onChange={e => setPublish(e.target.checked)} 
								/>
								<label htmlFor="publish" className="custom-control-label">Publish</label>
							</div>

							<div className="text-right">
								<Btn type="reset" size="sm" kind="dark">Reset</Btn>{" "}
								<Btn type="submit" size="sm">Save</Btn>
							</div>
						</div>
					</Form>
				</div>

				<div className="col-md-8">
					<label>Description</label>
					<Editor 
						editorState={editorState} 
						toolbar={{
							options: [
								'inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 
								'link', 'remove', 'history', // , 'emoji', 'embedded', 'colorPicker', 'image'
							],
							inline: {
								inDropdown: false, 
								options: ['bold', 'italic', 'underline', 'strikethrough', 'monospace', 'superscript'], 
							}, 
							fontSize: { dropdownClassName:"q-scroll" }, 
						}}
						toolbarClassName="position-sticky t48 zi-2 bg-light" // relative
						wrapperClassName="shadow-sm" 
						editorClassName="border mt-n1px px-3 rounded-bottom ctext" 
						editorStyle={{ minHeight: 'calc(100vh - 226px)' }} 
						// localization={{
						// 	locale: "id", 
						// }}
						onEditorStateChange={(state) => {
							setEditorState(state);
						}}
					/>

					{/* <Textarea 
						readOnly 
						value={draftToHtml(convertToRaw(editorState.getCurrentContent()))} 
					/> */}
				</div>
			</div>
		</div>
	);
}

/* <select required 
	id="category" 
	className={"custom-select" + Q.formikValidClass(formik, "category")} 
	{...formik.getFieldProps("category")} 
>
	<option value="">Choose Kategori</option>
	<option value="hell 1">Hell 1</option>
	<option value="hell 2">Hell 2</option>
</select> */
