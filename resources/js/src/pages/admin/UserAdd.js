import React, { useRef, useState, useEffect } from 'react';// useState, useEffect, useRef
// import { useHistory } from 'react-router-dom';// Redirect, 
import * as Yup from 'yup';
import { useFormik } from 'formik'; // useFormik, Form
// import { MultiSelect } from 'primereact/multiselect';

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Input from '../../components/q-ui-react/Input';
import Password from '../../components/q-ui-react/Password';
import Btn from '../../components/q-ui-react/Btn';
import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute';
import Ava from '../../components/q-ui-react/Ava';

let currentAva = "";

export default function UserAdd({
	title = "Add User", 
	action = "create", 
	dataId, 
	forms
}){
	const initialValues = {
		first_name: "",
		last_name: "",
		email : "",
		password: "", 
		password_confirm: "", 
		username: "", 
		// company: "",
		// phone: "",
		// levels: ["2"], // members
		// active: false, // ???
		avatar: "", 
	};
	const INIT_LEVELS = [
		{ id: "2", name: "members", description: "General User" }
	];

	// let history = useHistory();
	// let ID = Q.urlSearch().get('id');
	const fileRef = useRef();

	const [err, setErr] = useState(null);
	const [ok, setOk] = useState(null);
	const [levels, setLevels] = useState(INIT_LEVELS);
	const [levelVals, setLevelVals] = useState(["2"]);// null
	const [file, setFile] = useState({});

	useEffect(() => {
		// if(USER_DATA && USER_DATA.isAdmin){
			// console.log('%cuseEffect in UserAdd','color:yellow;');
			axios.get("/levels").then(r => {
				let res = r.data;
				// console.log('res: ', res);
				if(res && !res.error){
					if(dataId && forms && forms.current_roles){
						// res = res.map(v => forms.current_roles.includes(v) ? { ...v, checked: true } : v);
						// res = res.filter(f => !forms.current_roles.includes(f));
						// setData(data.filter(f => !selectedRows.includes(f)));
						setLevelVals(forms.current_roles.map(v => v.id));
					}

					if(forms && forms.avatar !== "User.svg") currentAva = forms.avatar;

					setLevels(res);
				}
			}).catch(e => {
				console.warn(e);
				// setErr(e.message);
			});
		// }

		return () => {
			currentAva = "";
		}
	}, [dataId, forms]);

	const formik = useFormik({
		enableReinitialize: true,
		initialValues: forms || initialValues, 
		validationSchema: Yup.object().shape({
			first_name: Yup.string()
				.min(2, "Minimum 2 symbols")
				.max(50, "Maximum 50 symbols")
				.required("First name is required"), 
			// last_name: Yup.string()
			// 	.min(2, "Minimum 2 symbols")
			// 	.max(50, "Maximum 50 symbols")
			// 	.required("Last name is required"),
			email: Yup.string()
				.email("Wrong email format")
				.required("Email is required"),
			// levels: Yup.string()
			// 	.required("Levels is required"),
			// password: Yup.string()
			// 	.min(MIN_PASS, "Minimum " + MIN_PASS + " symbols") // .max(50, "Maximum 50 symbols")
			// 	.required("Password is required"),
			// password_confirm: Yup.string()
			// 	.required('Confirm Password must same')
			// 	// .test('passwords-match', 'Password must match', v => v === Yup.ref("password")),
			// 	.test('passwords-match', 'Passwords must match', function(value){
			// 		return this.parent.password === value;
			// 	}),
			// term: Yup.bool().oneOf([true], 'Terms & Conditions must be accepted')

			password: Yup.string().nullable()
				.when("mode" , 				{
					is: () => action === "create",
					then: Yup.string().min(MIN_PASS, "Minimum " + MIN_PASS + " symbols").required("Password is required")
				}),
			password_confirm: Yup.string().nullable()
				.when("mode" , 				{
					is: () => action === "create",
					then: Yup.string().nullable()
						.required('Confirm Password must same')
						.test('passwords-match', 'Passwords must match', function(value){
							return this.parent.password === value;
						}),
				}),
		}), 
		onSubmit: (values, fn) => { // setSubmitting, resetForm, setStatus, 
			console.log('values: ', values);
			console.log('levelVals: ', levelVals);
			if(levelVals.length < 1){
				// fn.setFieldError("levels", "Levels must be select min 1");
				fn.setSubmitting(false);
				return;
			}

			let fd = Q.obj2formData(values);
			if(file.name){
				console.log('file: ', file);
				fd.append('file', file, file.name);
			}

			levelVals.forEach(f => {
				fd.append('groups[]', f);
			});

			axios.post("/" + action + "_user" + (action === "edit" && dataId ? "/" + dataId : ""), fd, {
				headers: {'Content-Type': 'multipart/form-data'}
			})
			.then(r => {
				console.log('r: ', r);
				let data = r.data;
				if(data.error){
					setErr(data.error);
					// fn.setSubmitting(false);
				}
				else{
					setOk("Success save user");
					window.scrollTo({
						top: 0,
						left: 0,
						behavior: 'smooth'
					});
					// setTimeout(() => history.push('/user/all'), 2500);
				}
			}).catch(e => {
				console.warn(e);
				setErr(e.message);
				// fn.setSubmitting(false);
			})
			.then(() => fn.setSubmitting(false));
		}
	});

	const onChangeFile = e => {
		const f = e.target.files[0];
		if(f){
			setFile(f);
			// formik.setFieldValue("avatar", f.name);
		}
	}

	const onResetAvatar = () => {
		fileRef.current.value = "";
		setFile({});
		formik.setFieldValue("avatar", currentAva);
	}

	return (
		<div className="container py-3">
			<Head title={title} />
			<h5 className="hr-h hr-left mb-4">{title}</h5>

			<div className="mx-auto col col-md-7">
				{(ok || err) && <div className={"pre-wrap alert alert-" + (ok ? "info":"danger")}>{ok || err}</div>}

				<Form noValidate 
					className={"card shadow-sm pb-2" + (formik.isSubmitting ? " i-load cwait" : "")} 
					style={formik.isSubmitting ? { '--bg-i-load': '95px' } : null} 
					disabled={formik.isSubmitting} 
					onSubmit={formik.handleSubmit} 
					onReset={e => {
						if(currentAva && (!formik.values.avatar || file.name)){
							onResetAvatar();
						}
						
						formik.handleReset(e);
					}} 
				>
					<Flex className="card-header py-2 position-sticky t48 zi-1 bg-light ml-1-next">
						<Aroute to="/user/all" outline btn="primary" size="sm">Users</Aroute>
						<Btn type="reset" kind="dark" size="sm" className="ml-auto">Reset</Btn>{" "}
						<Btn type="submit" size="sm">Save</Btn>
					</Flex>

					<div className="card-body mt-3-next">
						<Flex dir="column" align="center">
							<Ava 
								// round 
								thumb 
								w={99} 
								h={99} 
								wrapClass="flexno mb-2 shadow-sm" // mx-auto
								className="of-cov" //  border border-right-0
								alt={formik.values.first_name + " " + formik.values.last_name} 
								src={file.name ? window.URL.createObjectURL(file) : Q.baseURL + "/public/media/users/" + (formik.values.avatar || "User.svg")} 
								onLoad={e => {
									if(file.name){
										window.URL.revokeObjectURL(e.target.src);
									}
								}}
							/>

							<div className="btn-group btn-group-sm">
								<Btn As="label" kind="light" className="btnFile qi-fw qi qi-edit">
									<input type="file" accept="image/*" hidden 
										ref={fileRef} 
										onChange={onChangeFile}
									/>
								</Btn>

								{(currentAva && (!formik.values.avatar || file.name)) && // 
									<Btn kind="light" className="qi-fw qi qi-undo" title="Undo" 
										onClick={onResetAvatar}
									/>
								}

								{(formik.values.avatar || file.name) && 
									<Btn onClick={() => {
										fileRef.current.value = "";
										setFile({});
										formik.setFieldValue("avatar", "");
									}} 
									kind="light" className="qi-fw qi qi-close xx" />
								}
							</div>
						</Flex>

						<div>
							<label htmlFor="first_name">First name</label>
							<Input required 
								id="first_name" 
								className={Q.formikValidClass(formik, "first_name")} 
								value={formik.values.first_name} 
								onChange={formik.handleChange}
							/>

							{(formik.touched.first_name && formik.errors.first_name) && 
								<div className="invalid-feedback">{formik.errors.first_name}</div>
							}
						</div>

						<div>
							<label htmlFor="last_name">Last name</label>
							<Input // required 
								id="last_name" 
								className={Q.formikValidClass(formik, "last_name")} 
								value={formik.values.last_name} 
								onChange={formik.handleChange}
							/>

							{/* {(formik.touched.last_name && formik.errors.last_name) && 
								<div className="invalid-feedback">{formik.errors.last_name}</div>
							} */}
						</div>

						<div>
							<label htmlFor="username">Username</label>
							<Input 
								id="username" 
								className={Q.formikValidClass(formik, "username")} 
								value={formik.values.username} 
								onChange={formik.handleChange}
							/>

							{/* {(formik.touched.username && formik.errors.username) && 
								<div className="invalid-feedback">{formik.errors.username}</div>
							} */}
						</div>

						<div>
							<label htmlFor="email">Email</label>
							<Input type="email" required 
								id="email" 
								className={Q.formikValidClass(formik, "email")} 
								value={formik.values.email} 
								onChange={formik.handleChange}
							/>

							{(formik.touched.email && formik.errors.email) && 
								<div className="invalid-feedback">{formik.errors.email}</div>
							}
						</div>

						{/* <div>
							<label htmlFor="level">Level</label>
							<MultiSelect 
								required 
								value={levelVals} // formik.values.level
								options={levels} 
								onChange={(e) => {
									setLevelVals(e.value);
									formik.setFieldValue("level", e.value);
									console.log('onChange level: ', e.value);
									console.log('onChange levelVals: ', levelVals);
								}} 
								optionLabel="name" 
								optionValue="id" 
								placeholder="Select a level" 
								display="chip" 
								// q-p-select input-sm 
								className={"w-100 text-truncate" + Q.formikValidClass(formik, "level")} 
							/>
						</div> */}

						<div className="card shadow-sm">
							<div className="card-header py-2 px-3">Levels</div>
							<div className="card-body p-3">
								{levels.map(v => 
									<div key={v.id} className="custom-control custom-checkbox">
										<input id={v.name + v.id} type="checkbox" className="custom-control-input" 
											checked={levelVals.includes(v.id)} 
											onChange={e => setLevelVals(e.target.checked ? [ ...levelVals, v.id ] : levelVals.filter(f => f !== v.id))} 
										/>
										<label htmlFor={v.name + v.id} className="custom-control-label">{v.name}</label>
									</div>
								)}

								{(levelVals.length < 1) && 
									<div className="invalid-feedback d-block">Levels must be select min 1</div>
								}
							</div>
						</div>

						{/* <div>
							<label htmlFor="level">Level</label>
							<select required 
								// id="level" 
								className={"custom-select custom-select-sm" + Q.formikValidClass(formik, "level")} 
								value={formik.values.level} 
								onChange={formik.handleChange}
							>
								{levels.map(v => <option key={v.id} value={v.id}>{v.name} - {v.description}</option>)}
							</select>
						</div> */}

						{/* <div>
							<label htmlFor="position">Position</label>
							<select required 
								name="position" 
								id="position" 
								className={"custom-select custom-select-sm" + Q.formikValidClass(formik, "position")} 
								value={formik.values.position} 
								onChange={formik.handleChange}
							>
								{USER_POSITION.map((v, i) => <option key={i} value={v}>{v}</option>)}
							</select>
						</div> */}

						<div>
							<label htmlFor="password">Password</label>
							{/* <Input type="password" required 
								id="password" 
								className={Q.formikValidClass(formik, "password")} 
								value={formik.values.password} 
								onChange={formik.handleChange}
							/> */}
							<Password 
								id="password" 
								className={Q.formikValidClass(formik, "password")} 
								value={formik.values.password} 
								onChange={formik.handleChange} 
							/>

							{(formik.touched.password && formik.errors.password) && 
								<div className="invalid-feedback d-block">{formik.errors.password}</div>
							}
						</div>
						
						<div>
							<label htmlFor="password_confirm">Confirm Password</label>
							<Password 
								id="password_confirm" 
								className={Q.formikValidClass(formik, "password_confirm")} 
								value={formik.values.password_confirm} 
								onChange={formik.handleChange} 
							/>

							{(formik.touched.password_confirm && formik.errors.password_confirm) && 
								<div className="invalid-feedback d-block">{formik.errors.password_confirm}</div>
							}
						</div>

						{/* <div>
							<div className="custom-control custom-switch">
								<input type="checkbox" className="custom-control-input" id="active" />
								<label className="custom-control-label" htmlFor="active"> Active</label>
							</div>
						</div> */}
					</div>
				</Form>
			</div>
		</div>
	);
}


