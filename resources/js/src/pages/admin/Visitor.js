import React, { useState, useEffect } from 'react';// { useState, useRef, useEffect }
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
// import { useInViewport } from 'ahooks';
import Dropdown from 'react-bootstrap/Dropdown';

import Head from '../../components/q-ui-react/Head';
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
import Input from '../../components/q-ui-react/Input';
// import Table from '../../components/q-ui-react/Table';

export default function Visitor(){
	// const viewRef = useRef();
	// const inView = useInViewport(viewRef);
	// const dtRef = useRef(null);
	const [load, setLoad] = useState(false);
	const [data, setData] = useState([]);
	const [sortData, setSortData] = useState("Latest");// Oldest 
	const [globalFilter, setGlobalFilter] = useState(null);
	const [expandedRows, setExpandedRows] = useState(null);
	const [selectedRows, setSelectedRows] = useState([]);// null

	useEffect(() => {
		// console.log('%cuseEffect in Visitor','color:yellow');
		// , Q.obj2formData({ select:"id,page" })
		axios.get('/get_all/visitor').then(r => { // post
			const datas = r.data;// datas
			if(Array.isArray(datas) && !datas.error){// r.status === 200 && 
				// let d = datas.map(f => ( { ...f, ua: bowser.parse(f.useragent) } )); // new Date(f.created_at)
				let d = datas.map(f => ( { ...f, created_at: Q.dateIntl(f.created_at, LANG_CODE, {year:'numeric',month:'long',day:'numeric',hour:'numeric',minute:'numeric',second:'numeric'}) } ));
				if(data.length > 1){
					d.reverse();
				}
				// console.log('datas: ', d);

				// d.sort((a, b) => {
				// 	let idA = Number(a.id);
				// 	let idB = Number(b.id);
				// 	if(idB < idA){ // idA < idB
				// 		return -1;
				// 	}
				// 	if(idB > idA){ // idA > idB
				// 		return 1;
				// 	}
				// 	return 0;
				// });
				// console.log('d: ', d);
				setData(d);
			}
		})
		.catch(e => console.log('e: ',e))
		// .then(() => setLoad(true));
	}, []);

	const COLS = [
		{field: "page", header: "Page"}, 
		// {field: "status", header: "Status"}, 
		// {field: "timezone", header: "Timezone"}, 
		// {field: "useragent", header: "User Agent"}, 
		{field: "ip_address", header: "IP Address"}, 
		{field: "created_at", header: "Access time"}, 
	];
	const COLS_EXP = [
		{field: "status", header: "Status"}, 
		{field: "timezone", header: "Timezone"}
	];
	const exportColumns = [...COLS, ...COLS_EXP].map(v => ({ title: v.header, dataKey: v.field }));

	// const exportCSV = (selectionOnly) => {
	// 	dtRef.current.exportCSV({ selectionOnly });
	// }

	const exportPdf = () => {
		// console.log('exportPdf');
		import('jspdf').then(jsPDF => {
			import('jspdf-autotable').then(() => {
				const doc = new jsPDF.default(0, 0);
				doc.autoTable(exportColumns, data);
				doc.save('Visitors.pdf');
			});
		});
	}

	const exportExcel = () => {
		console.log('exportExcel');
		import('xlsx').then(xlsx => {
			// console.log('xlsx: ', xlsx);
			const worksheet = xlsx.utils.json_to_sheet(data);
			const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
			const excelBuffer = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
			saveAsExcelFile(excelBuffer, 'Visitors');
		});
	}

	const saveAsExcelFile = (buffer, fileName) => {
		console.log('saveAsExcelFile');
		import('file-saver').then(fileSave => {
			// console.log('fileSave: ', fileSave);
			const FileSaver = fileSave.default;
			let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
			let EXCEL_EXTENSION = '.xlsx';
			const data = new Blob([buffer], { type: EXCEL_TYPE });
			FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
		});
	}

	const onDelete = (e, v) => {
		const et = e.target;
		et.disabled = true;

		Swal.fire({
			icon: "question", // warning
			title: "Are you sure to delete this data?",
			// text: "", 
			showCloseButton: true, 
			allowEnterKey: false, 
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes'
		}).then(r => {
			if(r.isConfirmed){
				axios.delete("/delete_by/visitor/id/" + v.id).then(r => {
					if(r.data && !r.data.error){// r.status === 200 && 
            swalToast({ icon:"success", text:"Success delete data visitor" });
            setData(data.filter(f => f.id !== v.id));
						if(selectedRows.length){
							setSelectedRows(selectedRows.filter(f => f.id !== v.id));
						}
          }else{
						swalToast({ icon:"error", text:"Failed delete data visitor" });
					}
				}).catch(e => {
					console.log('e: ', e);
					swalToast({ icon:"error", text:"Failed delete data visitor" });
				})
				.then(() => et.disabled = false);
			}else{
				et.disabled = false;
			}
		});
	}

	const onDeleteChecked = () => {
		Swal.fire({
			icon: "question", // warning
			title: "Are you sure to delete this selected data?",
			showCloseButton: true, 
			allowEnterKey: false, 
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes'
		}).then(r => {
			if(r.isConfirmed){
				setLoad(true);

				const checked = JSON.stringify(selectedRows.map(v => v.id));
				// console.log('onDeleteChecked selectedRows: ', selectedRows);
				// console.log('onDeleteChecked checked: ', checked);
		
				axios.post('/delete-checked/visitor/id', Q.obj2formData({ checked })).then(r => {
					// console.log('r: ', r);
					if(r.data && !r.data.error){
						setData(data.filter(f => !selectedRows.includes(f)));
						setSelectedRows([]);
						swalToast({ icon:"success", text:"Success delete data visitor" });
					}else{
						swalToast({ icon:"error", text:"Failed delete data visitor" });
					}
				})
				.catch(e => {
					console.warn('e: ', e);
					swalToast({ icon:"error", text:"Failed delete data visitor" });
				})
				.then(() => setLoad(false));
			}
		});
	}

	// const renderNo = (_, tb) => {
	// 	console.log('tb: ', tb);
	// 	return tb.rowIndex + 1;
	// }

	const renderTd = (row, v) => {
		switch(v.field){
			case "page":
				return <a href={Q.baseURL + row.page} target="_blank" className="d-block">{row.page}</a>;
			default:
				return row[v.field]
		}
	}
	
	return (
		<div className="container-fluid pt-3 pb-5">
			<Head title="Visitor" />
			<h5 className="hr-h hr-left mb-2">Visitor</h5>

			<Flex wrap align="center" className="py-2 mb-2 bg-white border-bottom position-sticky t48 zi-4">
				<h6 className="mb-md-0 mb-pp-3 mb-p-lp-0">Total Visitor : {data.length}</h6>
				{data.length > 0 && 
					<Flex wrap className="ml-sm-auto flex-grow-pp-1">{/*  className="ml-tl-1-next" d-print-none */}
						{selectedRows.length > 0 && 
							<Btn size="sm" kind="warning" className="mb-pp-2 mb-p-lp-0 mb-md-0 mr-md-1 w-pp-100 qi qi-check"
								onClick={onDeleteChecked} 
								loading={load}
							>
								Delete <b className="badge badge-light">{selectedRows.length}</b>{" "}
								{load && 
									<b role="status" aria-hidden="true" className="spinner-border spinner-border-sm" />
								}
							</Btn>
						}

						<Flex grow="1">
							<div className="btn-group btn-group-sm mr-1">
								<Btn size="sm" kind="light" 
									onClick={() => {
										setSortData(sortData === "Oldest" ? "Latest":"Oldest");
										const newData = [ ...data ];
										setData(newData.reverse());
									}} 
								>Sort By {sortData}</Btn>

								<Dropdown bsPrefix="btn-group">
									<Dropdown.Toggle size="sm" bsPrefix="rounded-right qi qi-file" title="Export to" />
									<Dropdown.Menu>
										{[
											{ i: "xls", t: "Export to Excel", fn: exportExcel }, 
											{ i: "pdf", t: "Export to PDF", fn: exportPdf }
										].map(v => 
											<Dropdown.Item key={v.i} onClick={v.fn} {...Q.DD_BTN} className={"qi qi-" + v.i}>{v.t}</Dropdown.Item>
										)}
									</Dropdown.Menu>
								</Dropdown>

								{/* <Btn onClick={exportExcel} size="sm" kind="success" className="qi qi-xls" title="Export to Excel" />
								<Btn onClick={exportPdf} size="sm" kind="danger" className="qi qi-pdf" title="Export to PDF" /> */}
							</div>

							{/* <div className="input-group input-group-sm">
								<div className="input-group-prepend">
									<label className="input-group-text" htmlFor="sortData">Sort</label>
								</div>
								<select id="sortData">
									{[""]}
								</select>
							</div> */}
							
							<Input isize="sm" type="search" placeholder="Search" 
								className="w-auto flex-grow-1" // flex1
								onInput={e => setGlobalFilter(e.target.value)}
							/>
						</Flex>
					</Flex>
				}
			</Flex>

			<DataTable // ref={dtRef} 
				className="p-datatable-sm text-sm border table-responsive" //  table-responsive p-datatable-striped 
				tableClassName="tb-auto" 
				rowHover 
				value={data} 
				dataKey="id"  
				globalFilter={globalFilter} 
				// selection={selectedCustomers} 
				// onSelectionChange={e => setSelectedCustomers(e.value)} 
				// paginator 
				// rows={10} 
				emptyMessage="No Data" 
				// currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries" 
				// paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown" 
				// rowsPerPageOptions={[10,25,50]} 

				// resizableColumns 
				// scrollable 
				// scrollHeight="700px" // 350px 
				selection={selectedRows} 
				onSelectionChange={e => {
					setSelectedRows(e.value);
					console.log('onSelectionChange e.value', e.value);
				}} 
				expandedRows={expandedRows} 
				onRowToggle={(e) => setExpandedRows(e.data)} 
				// bowser.parse(f.useragent)
				rowExpansionTemplate={row => (
					<div className="form-row">
						<div className="col-12 col-md-4 mb-2">
							<h6>User Agent</h6>
							{/* <div>{row.useragent}</div> */}
							<ul className="list-unstyled mb-0 text-capitalize">
								{Object.entries(bowser.parse(row.useragent)).map((v2, i2) => 
									<li key={i2}>
										{v2[0]} :
										<ul className="small">
											{Object.entries(v2[1]).map((v3, i3) => 
												<li key={i3}>{v3[0]} : {v3[1]}</li>
											)}
										</ul>
									</li>
								)}
							</ul>
						</div>
						
						<div className="col-12 col-md-4 mb-2">
							<h6>Timezone</h6>
							{row.timezone}
						</div>

						<div className="col-12 col-md-4 mb-2">
							<h6>Status</h6>
							{row.status}
						</div>
					</div>
				)}
			>
				<Column selectionMode="multiple" headerStyle={{ width:40 }} />
				{/* <Column field="id" header="No." body={renderNo()} /> */}
				<Column expander style={{ width: 45 }} />{/*  headerClassName="position-sticky t95 zi-3"  */}

				{COLS.map((v) => 
					<Column key={v.field} sortable 
						field={v.field} 
						header={v.header} 
						body={row => renderTd(row, v)} 
						// headerClassName="position-sticky t95 zi-3" 
					/>
				)}

				<Column 
					body={row => <Btn onClick={(e) => onDelete(e, row)} kind="danger" size="xs">Delete</Btn>} 
					headerStyle={{ width: 70 }} 
					// bodyStyle={{textAlign: 'center', overflow: 'visible'}} 
					headerClassName="text-center" // position-sticky t95 zi-3 
					bodyClassName="text-center" 
				/>

				{/* <Column field="name" header="Name" body={nameBodyTemplate} sortable filter filterPlaceholder="Search by name" />
				<Column sortField="country.name" filterField="country.name" header="Country" body={countryBodyTemplate} sortable filter filterMatchMode="contains" filterPlaceholder="Search by country"/>
				<Column sortField="representative.name" filterField="representative.name" header="Representative" body={representativeBodyTemplate} sortable filter filterElement={representativeFilterElement} />
				<Column field="date" header="Date" body={dateBodyTemplate} sortable filter filterMatchMode="custom" filterFunction={filterDate} filterElement={dateFilterElement} />
				<Column field="status" header="Status" body={statusBodyTemplate} sortable filter filterElement={statusFilterElement} />
				<Column field="activity" header="Activity" body={activityBodyTemplate} sortable filter filterMatchMode="gte" filterPlaceholder="Minimum" />
				<Column body={actionBodyTemplate} headerStyle={{width: '8em', textAlign: 'center'}} bodyStyle={{textAlign: 'center', overflow: 'visible'}} /> */}
			</DataTable>

			{/* <div ref={viewRef} className="pt-1" aria-hidden={!inView} /> */}
		</div>
	);
}

/* {load ? 
	data.length > 0 ? 
		<Table 
			wrapHeight="508px" // {508} 
			fixThead 
			strip 
			hover 
			border 
			sm 
			thead={
				<tr>
					{["No.", "Page", "Status", "Timezone", "User Agent", "IP Address", "Access time", "Action"].map((v, i) => // , "Status", "Levels"
						<th key={i + v} scope="col" className={v === "Action" ? "d-print-none" : ""}>{v}</th>
					)}
				</tr>
			}
			tbody={
				data?.map((v, i) => 
					<tr key={i}>
						<th scope="row">{i + 1}</th>
						<td className="px-2">
							<a href={Q.baseURL + v.page} target="_blank">{v.page}</a>
						</td>
						<td>{v.status}</td>
						<td>{v.timezone}</td>
						<td>
							<ul className="list-unstyled mb-0 text-capitalize">
								{Object.entries(v.ua).map((v2, i2) => 
									<li key={i2}>
										{v2[0]} :
										<ul className="small">
											{Object.entries(v2[1]).map((v3, i3) => 
												<li key={i3}>{v3[0]} : {v3[1]}</li>
											)}
										</ul>
									</li>
								)}
							</ul>
						</td>
						<td>{v.ip_address}</td>
						<td>{Q.dateIntl(new Date(v.created_at), "en", {year:'numeric',month:'long',day:'numeric',hour:'numeric',minute:'numeric',second:'numeric'})}</td>
						<td className="d-print-none" style={{ width: 100 }}>
							<Btn onClick={() => onDelete(v)} kind="danger" size="xs">Delete</Btn>
						</td>
					</tr>
				)
			}
		/>
		: 
		<div className="alert alert-info">No Data</div>
	: 
	<div>LOADING</div>
} */
