import React, { useState, useEffect } from 'react';// { useState, useRef, useEffect }
import Alert from 'react-bootstrap/Alert';

import Head from '../../components/q-ui-react/Head';
// import Btn from '../../components/q-ui-react/Btn';
import Aroute from '../../components/q-ui-react/Aroute';
import Placeholder from '../../components/q-ui-react/Placeholder';

// import LiveCode from '../../components/live-code/LiveCode';

export default function HomeAdmin(){
	const [err, setErr] = useState(false);
	const [load, setLoad] = useState(false);
	const [data, setData] = useState([]);

	useEffect(() => {// async 
		// console.log('%cuseEffect in HomeAdmin','color:yellow');
		// axios.get('/count-all/visitor').then(r => {
		// 	console.log('r: ', r);
		// 	const datas = r.data;
		// 	if(r.status === 200 && datas && !datas.error){
				
		// 	}
		// })
		// .catch(e => console.log('e: ',e))
		// .then(() => setLoad(true));

		let to, text, icon;
		Promise.all([
			axios.get('/count-all/users'), 
			axios.get('/count-all/visitor'), 
			axios.get('/count-all/contact_us')
		]).then(results => {
			// console.log("Promise.all results: ", results);
			let datas = [];
			results.forEach((v, i) => {
				console.log(v);
				const { error } = v.data;
				if(error || !Q.isNum(v.data)){
					return;
				}

				// if(v.data){ // v.status === 200 && 
					switch(i){
						case 0:
							to = "/user/all";
							text = "Users";
							icon = "table";
							break;
						case 1:
							to = "/user/visitor";
							text = "Visitor";
							icon = "table";
							break;
						default:
							to = "/contact-us";
							text = "Contact Us";
							icon = "mail";
							break;
					}

					datas.push({
						to,
						text, 
						// desc: i === 0 ? "Available" : null, 
						icon, 
						value: i === 0 ? v.data - 1 : v.data 
					});
				// }
			});

			setData(datas);
			setLoad(true);
		}).catch(e => {
			console.log('Error e: ', e);
			setErr(true);
			setLoad(true);
		});

		// console.log('jQuery: ', jQuery);
		// Q.getScript({ src:"https://code.jquery.com/jquery-3.6.0.min.js" }).then(m => {
		// 	console.log('m: ', m);
		// }).catch(e => console.log('e: ', e));

		// const jQuery = await Q.getScript({ src:"https://code.jquery.com/jquery-3.6.0.min.js" });
		// console.log('jQuery: ', jQuery);
	}, []);

	return (
		<div className="container py-3">
			<Head title="Home" />
			{/* <h1>Home Admin</h1> */}
			
			{err ? 
				<Alert variant="danger">Error</Alert>
				: 
				<div className="row">
					{load ? 
						data.map((v, i) => 
							<div key={i} className="col-12 col-md-3">
								<Aroute to={v.to} nav className="card py-3 mb-3 shadow-sm">								
									<h5 className={"card-title mb-0 q-mr qi qi-" + v.icon}>
										{v.value} {v.text}
									</h5>
								</Aroute>
							</div>
						)
						: 
						<Placeholder length={2} className="col-12 col-md-3" />
					}
				</div>
			}

			{/* <LiveCode 
				// browser={browser} 
				// openIde={v.openIde} 
				className="shadow-sm" 
				liveBoxClass="ovauto resize-native resize-v qi qi-arrows-v" 
				viewClass="w-100" 
				h={'70vh'} // 
				// scope={{ ...scope, Btn, Form }} 
				code={""} 
			/> */}
		</div>
	);
}

/*
<Aroute to={v.to} noLine kind="dark" className="card mb-3 shadow-sm">
								<div className="row no-gutters">
									<div className="col-4">
										<svg className="placeholder-img h1 w-100 rounded-left" 
											xmlns="http://www.w3.org/2000/svg" 
											preserveAspectRatio="xMidYMid slice" 
											focusable="false" 
											height={100} 
										>
											<rect className="w-100 h-100" fill="#868e96" />
											<text x="50%" y="50%" fill="#dee2e6" dy=".3em">{v.value}</text>
										</svg>
									</div>
									<div className="col-8">
										<div className="card-body">
											<h5 className={"card-title q-mr qi qi-" + v.icon}> {v.text}</h5>
										</div>
									</div>
								</div>
							</Aroute>
*/