import React, { useRef, useState } from 'react';// useEffect
import ColorThief from 'colorthief';
import { PDFDocument, StandardFonts, rgb } from 'pdf-lib';
// import 'emoji-mart/css/emoji-mart.css'
// import { Picker } from 'emoji-mart';
import Editor from '@monaco-editor/react';// , { loader } | useMonaco
import 'rc-tree/assets/index.css';
import Tree from 'rc-tree';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import Modal from 'react-bootstrap/Modal';
// import Dropdown from 'react-bootstrap/Dropdown';
// import { HexColorPicker, HexColorInput } from 'react-colorful';
// import Draggable from 'react-draggable';
// import { useInView } from 'react-hook-inview';// useInViewEffect
// import debounce from 'lodash/throttle';// debounce | throttle
import Frame, { FrameContextConsumer } from 'react-frame-component';
// import Select, { Option } from 'rc-select';
import { ErrorBoundary } from 'react-error-boundary';
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
// import Overlay from 'react-bootstrap/Overlay';
// import htmlMinify from 'html-minifier';// NOT WORK ???
import { crush } from "html-crush";// , defaults, version

import Head from '../../../components/q-ui-react/Head';
import Fieldset from '../../../components/q-ui-react/Fieldset';
import Btn from '../../../components/q-ui-react/Btn';
import Input from '../../../components/q-ui-react/Input';
import CssTooltip from '../../../components/q-ui-react/CssTooltip';
import IconPicker from '../../../components/q-ui-react/icon-picker/IconPicker';
import Signature from '../../../components/q-ui-react/Signature';
import Sorter from '../../../components/q-ui-react/sorter/Sorter';
import FormTableEditor from '../../../components/form-table-editor/FormTableEditor';
import ManifestGenerator from '../../../apps/ManifestGenerator';
import { ColorPicker } from '../../../components/q-ui-react/color-picker/ColorPicker';
import FormDesigner from '../../../components/form-designer/FormDesigner';
import FileUpload from '../../../components/q-ui-react/file-upload/FileUpload';
// import ScrollSpy from '../../../components/q-ui-react/ScrollSpy';
import InputFile from '../../../components/q-ui-react/InputFile';
import MonacoEditor from '../../../components/monaco-editor/MonacoEditor';
// import Select from '../../../components/q-ui-react/select/Select';
import PlayerQ from '../../../components/player-q/PlayerQ';
import Select from '../../../components/q-ui-react/select/Select';
// Blueprint:
// import ResizeSensor from '../../../components/q-ui-react/blueprint/resize-sensor/ResizeSensor';

// APPS:
// import JsCompiler from '../../../apps/js-compiler/JsCompiler';

import { hasAlpha } from '../../../utils/hasAlpha';
import uid from '../../../utils/UniqueComponentId';
import copyStyles from '../../../utils/css/copyStyles';
// import { importEsm } from '../../../utils/load/importEsm';
// import { DATA_TYPE } from '../../../data/const/SQL_DATA';// , COLLATIONS, DEFAULTS, ATTRIBUTES, INDEXS

// function isHex(h){
//   const a = parseInt(h, 16);
//   return (a.toString(16) === h.toLowerCase())
// }

function ErrorFallback({ error, resetErrorBoundary }) {
  return (
    <div className="alert alert-danger" role="alert">
      <p>Something went wrong:</p>
      <pre>{error.message}</pre>
      <button onClick={resetErrorBoundary}>Try again</button>
    </div>
  )
}

function IamError() {
  // throw new Error('💥 CABOOM 💥');
  return (
    <div>{UNDEFINED}</div>
  )
}

const TipAction = React.forwardRef(
  ({
    target, 
    delay = { show: 250, hide: 400 }, 
    popId, // id
    As = "div", 
    onMouseEnter = () => {}, 
    onMouseLeave = () => {}, 
    // onBlur = () => {}, 
    ...etc
  }, 
  ref
) => {
  // delay = delay || { show: 250, hide: 400 };
  // const myRef = ref || useRef(null);
  const ID = popId || uid();
  const [see, setSee] = useState(false);

  // console.log('children: ', children);
  const hover = e => {
    setSee(true); // !see
    onMouseEnter(e);
  }

  const leave = e => {
    setTimeout(() => {
      setSee(false);
    }, delay.hide);
    onMouseLeave(e);
  }

  return (
    <>
      <OverlayTrigger 
        // flip 
        target={target} // myRef.current
        trigger={["hover","focus"]} // hover | click | focus | ['click','focus']
        // trigger="hover" 
        placement="auto" // right | auto-start
        show={see} 
        delay={delay} 
        overlay={
          <Popover 
            id={ID} 
            onMouseEnter={hover} 
            onMouseLeave={leave} 
          >
            <Popover.Title as="h3">Popover right</Popover.Title>
            <Popover.Content>
              And here's some <strong>amazing</strong> content. It's very engaging. right?
            </Popover.Content>
          </Popover>
        }
        onToggle={(open) => {
          console.log('onToggle open: ', open);
          console.log('onToggle see: ', see);
          // if(!see && !open) 
          if(!see) setSee(open);
        }}
        onHide={(val) => {
          console.log('onHide val: ', val);
        }}
      >
        <As 
          {...etc} 
          ref={ref} // ref | myRef
          onMouseLeave={(e) => {
            if(see){
              leave(e);
            }
          }}
          // onMouseEnter={hover} 
          // onMouseLeave={leave} 
          // onClick={() => setSee(!see)} 
          // onBlur={e => {
          //   if(see) setSee(false);
          //   onBlur(e);
          // }}
        />
      </OverlayTrigger>
    </>
  )
});

const DUMMY_TREE = [
  {
    title: 'parent 0',
    key: '0-0',
    children: [
      {
        title: 'leaf 0-0',
        key: '0-0-0',
        isLeaf: true,
      }, {
        title: 'leaf 0-1',
        key: '0-0-1',
        isLeaf: true,
      },
    ],
  }, {
    title: 'parent 1',
    key: '0-1',
    children: [
      {
        title: 'leaf 1-0',
        key: '0-1-0',
        isLeaf: true,
      }, {
        title: 'leaf 1-1',
        key: '0-1-1',
        isLeaf: true,
      },
    ],
  },
];

export default function DevsPage(){
	const [file, setFile] = useState({});
	const [alpha, setAlpha] = useState();

  const divRef = useRef();
  const formRef = useRef();
  // const emotRef = useRef();
  // const taRef = useRef();
  const [pdf, setPdf] = useState();
  const [emoji, setEmoji] = useState([]);// {}

  const [treeData, setTreeData] = useState(DUMMY_TREE);
  const [expandKeys, setExpandKeys] = useState(['0-0-key', '0-0-0-key', '0-0-0-0-key']);
  const [autoExpandParent, setAutoExpandParent] = useState(true);
  const [ytFind, setYtFind] = useState("");
  const [sectionTables, setSectionTables] = useState([
    { fieldType: "text", fieldName: "Name Text", icon: "mail", columnWidth: 500, fid: Q.Qid() }, // , rowIndex: 0
    { fieldType: "textarea", fieldName: "Name Text Area", icon: "wrench", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "number", fieldName: "Name Number", icon: "cut", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "currency", fieldName: "Name Currency", icon: "bars", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "date", fieldName: "Name Date", icon: "play", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "datetime", fieldName: "Name Date & Time", icon: "table", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "email", fieldName: "Name Email", icon: "bootstrap", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "switch", fieldName: "Name Yes/No (switch)", icon: "react", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "image", fieldName: "Name Image", icon: "img", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "rating", fieldName: "Name Rating", icon: "json", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "dropdown", fieldName: "Name Dropdown", icon: "chevron-down", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "checkbox", fieldName: "Name Checkbox / Multi-select", icon: "check", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "slider", fieldName: "Name Slider", icon: "wrench", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "user", fieldName: "Name User", icon: "user", columnWidth: 200, fid: Q.Qid() }, 
    { fieldType: "checklist", fieldName: "Name Checklist", icon: "list", columnWidth: 200, fid: Q.Qid() }, 
  ]);

  const [colorVal, setColorVal] = useState("#777");
  const [signature, setSignature] = useState(null);// "public/media/users/The-Welcome.jpg"
  const [showModal, setShowModal] = useState(false);
  const [explode, setExplode] = useState(false);
  const [fieldsetDisabled, setFieldsetDisabled] = useState(false);

  // const monaco = useMonaco();
  // loader.config({ paths: { vs: "public/js/libs/monaco-editor/min/vs" } });

	// useEffect(() => {// async 
	// 	console.log('%cuseEffect in DevsPage','color:yellow');
  //   // importEsm(Q.baseURL + "/public/js/esm/const/SVG_SRC.js");// .then(m => {})
  //   // import(/* webpackIgnore:true */Q.baseURL + "/public/js/esm/const/SVG_SRC.js")
  //   // .then(m => {
  //   //   console.log('m: ', m);
  //   //   // console.log('stringify: ', JSON.stringify(m.default));
  //   // }).catch(e => console.log('e: ', e));

  //   // public/json/svg_src.json | /public/json/emoji/emoji-mart/all.json | native.json
  //   // axios.get(Q.baseURL + "/public/json/emoji/emoji-mart/all.json").then(r => {
  //   //   console.log('r: ', r);
  //   //   // setEmoji(r.data);
  
  //   //   // dangerouslySetInnerHTML={{ __html: "&#x" + v[1].b + ";" }} 
  //   //   const entries = Object.entries(r.data.emojis);
  //   //   const emojis = entries.map((v) => { // , i
  //   //     let b;
  //   //     if(v[1].b && isHex(v[1].b)){
  //   //       b = v[1].b;
  //   //     }
  //   //     else if(v[1].c && isHex(v[1].c)){
  //   //       b = v[1].c;
  //   //     }
  //   //     else if(v[1].obsoletes && isHex(v[1].obsoletes)){
  //   //       b = v[1].obsoletes;
  //   //     }

  //   //     if(!b) return null;
  //   //     return {
  //   //       a: v[1].a, 
  //   //       b, 
  //   //     }
  //   //   }).filter(f => f);

  //   //   console.log('entries: ', entries);
  //   //   console.log('emojis: ', emojis);
  //   //   setEmoji(emojis);
  //   // }).catch(e => console.log('e: ', e));

  //   // if (monaco) {
  //   //   console.log("here is the monaco isntance:", monaco);
  //   // }

  //   // let emojis = [];
  //   // emotRef.current.querySelectorAll('.group').forEach((f, i) => {
  //   //   if(f.firstElementChild.classList.contains('Igw0E')){
  //   //     let emots = [];
  //   //     f.querySelectorAll('.emots button').forEach((f2, i2) => {
  //   //       emots.push({ e: f2.innerText });
  //   //     });
  //   //     emojis.push({ group: f.firstElementChild.innerText, emoji: emots });
  //   //   }
  //   // });

  //   // taRef.current.value = JSON.stringify(emojis, null, 2);
  // }, []);// monaco

// isHex("2199-FE0F");

  const createPdf = async () => {
    const pdfDoc = await PDFDocument.create();// Create a new PDFDocument
    // Embed the Times Roman font
    // const timesRomanFont = await pdfDoc.embedFont(StandardFonts.TimesRoman);
    // Add a blank page to the document
    const font = await pdfDoc.embedFont(StandardFonts.Helvetica);
    const page = pdfDoc.addPage();
    // Get the width and height of the page
    const { width, height } = page.getSize();

    // Draw a string of text toward the top of the page
    const fontSize = 30
    page.drawText('Creating PDFs in JavaScript is awesome!', {
      x: 50,
      y: height - 4 * fontSize,
      size: fontSize,
      font, // : timesRomanFont
      color: rgb(0, 0.53, 0.71),
    });

    const form = pdfDoc.getForm();
    const checkBox = form.createCheckBox('cool.new.checkBox');
    const textField = form.createTextField('cool.new.textField');

    checkBox.addToPage(page);
    textField.addToPage(page);// ont, page

    // Serialize the PDFDocument to bytes (a Uint8Array)
    const pdfData = await pdfDoc.saveAsBase64({ dataUri: true });// await pdfDoc.save();
    // const blob = new Blob([pdfBytes], { type: 'application/pdf' });
    console.log('pdfData: ', pdfData);
    // console.log('blob: ', blob);

    // Trigger the browser to download the PDF document
		// import('file-saver').then(fileSave => {
		// 	// console.log('fileSave: ', fileSave);
		// 	const FileSaver = fileSave.default;
		// 	FileSaver.saveAs(blob, 'export.pdf');
		// });

    setPdf(pdfData);// URL.createObjectURL(blob)

    // let a = Q.makeEl('a');
    // a.href = URL.createObjectURL(blob);
    // a.rel = "noopener";
    // a.download = zipFolder;

    // setTimeout(() => a.click(), 1);
    // setTimeout(() => {
    //   URL.revokeObjectURL(a.href);
    //   // a = null;// OPTION
    // }, 4E4); // 40s
  }

  const onEditPdf = async () => {
    // ShopeeFood_Merchant_Registration_Form_27112020 | dod_character
    const formPdfBytes = await fetch(Q.baseURL + "/public/DUMMY/dod_character.pdf").then(res => res.arrayBuffer());
    console.log('formPdfBytes: ', formPdfBytes);

    const marioImageBytes = await fetch(Q.baseURL + '/public/media/DUMMY/small_mario.png').then(res => res.arrayBuffer());
    const emblemImageBytes = await fetch(Q.baseURL + '/public/media/DUMMY/mario_emblem.png').then(res => res.arrayBuffer());

    const pdfDoc = await PDFDocument.load(formPdfBytes);
    const marioImage = await pdfDoc.embedPng(marioImageBytes);
    const emblemImage = await pdfDoc.embedPng(emblemImageBytes);
  
    const form = pdfDoc.getForm();
  
    const nameField = form.getTextField('CharacterName 2');
    const ageField = form.getTextField('Age');
    const heightField = form.getTextField('Height');
    const weightField = form.getTextField('Weight');
    const eyesField = form.getTextField('Eyes');
    const skinField = form.getTextField('Skin');
    const hairField = form.getTextField('Hair');
  
    const alliesField = form.getTextField('Allies');
    const factionField = form.getTextField('FactionName');
    const backstoryField = form.getTextField('Backstory');
    const traitsField = form.getTextField('Feat+Traits');
    const treasureField = form.getTextField('Treasure');
  
    const characterImageField = form.getButton('CHARACTER IMAGE');
    const factionImageField = form.getButton('Faction Symbol Image');
  
    nameField.setText('Mario');
    ageField.setText('24 years');
    heightField.setText(`5' 1"`);
    weightField.setText('196 lbs');
    eyesField.setText('blue');
    skinField.setText('white');
    hairField.setText('brown');
  
    characterImageField.setImage(marioImage);
  
    alliesField.setText(
      [
        `Allies:`,
        `  • Princess Daisy`,
        `  • Princess Peach`,
        `  • Rosalina`,
        `  • Geno`,
        `  • Luigi`,
        `  • Donkey Kong`,
        `  • Yoshi`,
        `  • Diddy Kong`,
        ``,
        `Organizations:`,
        `  • Italian Plumbers Association`,
      ].join('\n'),
    );
  
    factionField.setText(`Mario's Emblem`);
    factionImageField.setImage(emblemImage);
  
    backstoryField.setText(
      [
        `Mario is a fictional character in the Mario video game franchise, `,
        `owned by Nintendo and created by Japanese video game designer Shigeru `,
        `Miyamoto. Serving as the company's mascot and the eponymous `,
        `protagonist of the series, Mario has appeared in over 200 video games `,
        `since his creation. Depicted as a short, pudgy, Italian plumber who `,
        `resides in the Mushroom Kingdom, his adventures generally center `,
        `upon rescuing Princess Peach from the Koopa villain Bowser. His `,
        `younger brother and sidekick is Luigi.`,
      ].join('\n'),
    );
  
    traitsField.setText(
      [
        `Mario can use three basic three power-ups:`,
        `  • the Super Mushroom, which causes Mario to grow larger`,
        `  • the Fire Flower, which allows Mario to throw fireballs`,
        `  • the Starman, which gives Mario temporary invincibility`,
      ].join('\n'),
    );
  
    treasureField.setText(['• Gold coins', '• Treasure chests'].join('\n'));

    // Serialize the PDFDocument to bytes (a Uint8Array)
    const pdfData = await pdfDoc.saveAsBase64({ dataUri: true });// await pdfDoc.save();
    console.log('pdfData: ', pdfData);

    setPdf(pdfData);// URL.createObjectURL(blob)
  }

	const form2Pdf = () => {
		console.log('exportPdf formRef.current.outerHTML: ',formRef.current.outerHTML);
		import('jspdf').then(jsPDF => {
      const newPdf = new jsPDF.default('p','mm','a4');// 'p', 'pt', 'letter' | 0, 0
      const html = `<!doctype html><html><head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
      <link rel="stylesheet" href="http://localhost/programmeria-ci-3/public/css/app.css">
</head><body>
      ${formRef.current.outerHTML}</body></html>`;
      newPdf.html(html, {
        callback: (doc) => {
          doc.save("form.pdf");
        },
      });
		});
	}

  // const unescapeHTML = str => str.replace(/&amp;|&#39;|&quot;/g, tag => ({
  //     '&amp;': '&',
  //     '&#39;': "'",
  //     '&quot;': '"'
  //   }[tag] || tag)
  // );
  // unescapeHTML("&#x" + v[1].b + ";")

  const onDragStart = info => {
    console.log('start', info);
  };

  const onDragEnter = () => {
    console.log('enter');
  };

  const onDrop = info => {
    console.log('drop', info);
    const dropKey = info.node.key;
    const dragKey = info.dragNode.key;
    const dropPos = info.node.pos.split('-');
    const dropPosition = info.dropPosition - Number(dropPos[dropPos.length - 1]);

    const loop = (data, key, callback) => {
      data.forEach((item, index, arr) => {
        if (item.key === key) {
          callback(item, index, arr);
          return;
        }
        if (item.children) {
          loop(item.children, key, callback);
        }
      });
    };
    const data = [ ...treeData ];

    // Find dragObject
    let dragObj;
    loop(data, dragKey, (item, index, arr) => {
      arr.splice(index, 1);
      dragObj = item;
    });

    if (dropPosition === 0) {
      // Drop on the content
      loop(data, dropKey, item => {
        // eslint-disable-next-line no-param-reassign
        item.children = item.children || [];
        // where to insert 示例添加到尾部，可以是随意位置
        item.children.unshift(dragObj);
      });
    } else {
      // Drop on the gap (insert before or insert after)
      let ar;
      let i;
      loop(data, dropKey, (item, index, arr) => {
        ar = arr;
        i = index;
      });
      if (dropPosition === -1) {
        ar.splice(i, 0, dragObj);
      } else {
        ar.splice(i + 1, 0, dragObj);
      }
    }

    setTreeData(data);// this.setState({ gData: data });
  };

  const onExpand = expandedKeys => {
    console.log('onExpand', expandedKeys);
    setExpandKeys(expandedKeys);
    setAutoExpandParent(false);
  };

  const allowDrop = ({ dropNode, dropPosition }) => {
    if (!dropNode.children) {
      if (dropPosition === 0) return false;
    }
    return true;
  }

  // const searchByKeyword = () => {
  //   let results = YouTube.Search.list('id,snippet', {q: 'dogs', maxResults: 25});
  
  //   for(var i in results.items) {
  //     var item = results.items[i];
  //     Logger.log('[%s] Title: %s', item.id.videoId, item.snippet.title);
  //   }
  // }

  const DUMMY_FORMS = [
    { size: 4 }, 
    { size: 4 }, 
    { size: 4 }, 
  ];

  // const onResize = (entries) => {
  //   console.log('onResize entries: ', entries);
  // }

  return (
		<div className="container-fluid py-3">
			<Head title="Devs" />
      <h4>Devs</h4>

      <h6>{"<ManifestGenerator />"}</h6>
      <ManifestGenerator 
        
      />
      <hr/>

      <h6>{"<Fieldset />"}</h6>
      <Btn onClick={() => setFieldsetDisabled(!fieldsetDisabled)}>Fieldset disabled: {fieldsetDisabled}</Btn>
      <Fieldset
        disabled={fieldsetDisabled} 
      >
        <Btn onClick={() => alert("HELLO")}>In Fieldset</Btn>
        <input className="form-control" type="text" placeholder="In Fieldset" />
        <a href="/">In Fieldset</a>
      </Fieldset>
      <hr/>

      <div>
        <h6>{"<ErrorBoundary />"}</h6>
        <Btn onClick={() => setExplode(e => !e)} kind="danger">toggle explode</Btn>

        <ErrorBoundary
          FallbackComponent={ErrorFallback}
          onReset={() => setExplode(false)}
          resetKeys={[explode]}
        >
          {explode ? <IamError /> : null}
        </ErrorBoundary>
      </div>
      <hr/>

      {/* <h6>{"<ResizeSensor />"}</h6>
      <ResizeSensor onResize={onResize}>
        <div className="card">
          <div className="card-body">
            Card in ResizeSensor
          </div>
        </div>
      </ResizeSensor>
      <hr/> */}

      {/* <Btn
        onClick={() => {
          let bcrypt = dcodeIO.bcrypt;
          // console.log('bcrypt: ', bcrypt);
          // bcrypt.genSalt(10, (err1, salt) => {
          //   console.log('bcrypt err: ', err1);
          //   bcrypt.hash("password", salt, (err, hash) => {
          //     // Store hash in your password DB.
          //     console.log('bcrypt err: ', err);
          //     console.log('bcrypt hash: ', hash);
          //   });
          // });

          // const husein = "$2y$10$F9iOIEkw0Oc1oWLNjMwwnu3W2AIInue/14zq7ymtcOhhQHJWFpO8C";
          const zay = "$2y$10$HbIMeNRjA/B0dv56QedHzOp/Vq8ADQ5Zn56yL5gl82jcEKyh9A6xe";

          bcrypt.compare("password", zay, (err, res) => {
            console.log('bcrypt compare err: ', err);
            console.log('bcrypt compare res: ', res);
          });
        }}
      >bcrypt.js</Btn>{" "} */}

      <Btn
        onClick={() => {
          const html = `<!DOCTYPE html>
          <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
          <head>
          <link rel="stylesheet" href="{{mix('/css/app.css')}}">
          </head>
          <body>
          <p><strong>Bold</strong></p>
          <p><em>Italic</em></p>
          <p><ins>Underline</ins></p>
          <p><del>Strike</del></p>
          <p><span style="color: rgb(226,80,65)">RED</span></p>
          <p><span style="color: rgb(97,189,109)">GREEN</span></p>
          <p><span style="color: rgb(44,130,201)">BLUE</span></p>
          <p style="text-align:center"><span style="color: rgb(44,130,201)">Align Center</span></p>
          <p style="text-align:right"><span style="color: rgb(0,0,0);background-color: rgb(255,255,255);font-size: 13px;font-family: Poppins, Helvetica, sans-serif">Align Right</span></p>
          <p><a href="https://reactjs.org" target="_blank">React JS</a>&nbsp;</p>
          <?php echo 1 + 1;?>
          <p>OKEH</p></body></html>`;
          // const minify = htmlMinify.minify(html, {
          //   minifyCSS: true, // Default: false
          //   keepClosingSlash: true, // Default: false
          //   decodeEntities: true, // Default: false
          //   quoteCharacter: '"', // OPTION
          //   removeEmptyElements: true, // Default: false
          // });
          const minify = crush(html, {
            lineLengthLimit: 1000, // Default: 500
            removeLineBreaks: true, 
            // reportProgressFunc: false, 
            // reportProgressFunc: function(res){
            //   console.log('reportProgressFunc res: ', res);
            //   console.log('reportProgressFunc arguments: ', arguments);
            // }, 
          });
          console.log('minify: ', minify);
          // console.log('minify.result: ', minify.result);
        }}
      >
        Html Minify
      </Btn><hr/>

      <h6>{"<PlayerQ />"}</h6>
      <PlayerQ 
        url="/media/videos/teleport-platform-presentation-d2.mp4" 
        // url="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" 
      />
      <hr/>

      <h6>{"<Select />"}</h6>
      <Select 
        options={[
          'Neptunium',
          'Plutonium',
          'Americium',
          'Curium',
          'Berkelium',
          'Californium',
          'Einsteinium',
          'Fermium',
          'Mendelevium',
          'Nobelium',
          'Lawrencium',
          'Rutherfordium',
          'Dubnium',
          'Seaborgium',
          'Bohrium',
          'Hassium',
          'Meitnerium',
          'Darmstadtium',
          'Roentgenium',
          'Copernicium',
          'Nihonium',
          'Flerovium',
          'Moscovium',
          'Livermorium',
          'Tennessine',
          'Oganesson',
        ]} 
        Clear={false} 
        placeholder="Choose" 
        value="Neptunium" 
        // defaultValue="Neptunium" 
        // DEVS OPTION: open always false, not work if clieck outside
        // onToggle={(open, e) => {
        //   console.log('onToggle open: ', open);
        //   console.log('onToggle e: ', e);
        // }}
        // onChange={(val, index, e) => {
        //   console.log('onChange val: ', val);
        //   console.log('onChange index: ', index);
        //   console.log('onChange e: ', e);
        // }}
        // onClear={(val, e) => {
        //   console.log('onClear val: ', val);
        //   console.log('onClear e: ', e);
        // }}
        // popper 
        // portal={document.body} 
        
      >

      </Select>
      <hr/>

      <h6>{"<TipAction />"}</h6>
      <TipAction
        As={Btn} 
      >
        Hover Me
      </TipAction>
      <hr/>

      <h6>{"<CssTooltip />"}</h6>
      <CssTooltip 
        As={Btn} 
        kind="danger" 
        // animate={false} 
        pos="BR" 
        // color="white" 
        aria-label="I'm Auto Info Position"
      >
        CssTooltip auto Position
      </CssTooltip>
      <hr/>

      {/* <h6>rc-select</h6>
      <Select>
      </Select>
      <hr/> */}

      <h6>{"<Frame />"} - react-frame-component</h6>
      <div className="embed-responsive embed-responsive-21by9">
        <Frame 
          className="embed-responsive-item" 
          loading="lazy" // <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
          title="Mobile" 
          initialContent='<!DOCTYPE html><html><head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <title>Hello, world!</title>
          </head><body><div id="root"></div></body></html>'
          mountTarget="#root"
        >
          <FrameContextConsumer>
            {frame => {
              // console.log('frame.document: ', frame.document);
              // console.log('frame.window: ', frame.window);
              setTimeout(() => copyStyles(document, frame.document), 1);// 0

              return (
                <div className="container-fluid">
                  <h1>In Iframe</h1>
                  <Btn onClick={() => setShowModal(true)}>Open Modal</Btn>
                  <Modal show={showModal} onHide={() => setShowModal(false)}
                    container={frame.document.body} // divRef.current
                  >
                    <Modal.Header closeButton>
                      <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
                    <Modal.Footer>
                      <Btn onClick={() => setShowModal(false)}>Close</Btn>
                    </Modal.Footer>
                  </Modal>
                </div>
              )
            }}
          </FrameContextConsumer>
        </Frame>
      </div>
      <hr/>

      <h6>{"<MonacoEditor />"}</h6>
      <MonacoEditor 
        // theme="vs-dark" 
        // height="40vh" 
        // disabled // Custom
        required // Custom
        // defaultValue="// some comment" 
        // defaultLanguage="javascript" 
      />
      <hr/>

      {/* <h6>{"<JsCompiler />"}</h6>
      <JsCompiler 

      />
      <hr/> */}

      <h6>@monaco-editor/react</h6>
      <Editor 
        theme="vs-dark" 
        height="60vh" 
        defaultValue="// some comment" 
        defaultLanguage="javascript" 
      />
      <hr/>

      {/* <h6>{"<ScrollSpy />"}</h6>
      <ScrollSpy />
      <hr/> */}

      <h6>{"<InputFile />"}</h6>
      <InputFile 
        // multiple 
        onChange={(val, e) => {
          console.log('onChange InputFile val: ', val);
          console.log('onChange InputFile e: ', e);
        }}
      />
      <hr/>

      <h6>{"<Signature />"}</h6>
      <Signature 
        // strokeWidth={1.5} 
        ext="svg" // png | jpeg | svg
        download 
        name="signature" 
        id="signature" 
        value={signature} 
        onChange={e => {
          console.log('onChange Signature e: ', e);
          setSignature(e.target.value);
        }}
        onBlur={(e) => {
          console.log('onBlur Signature e: ', e);
        }}
      />
      <hr/>

      <h6>{"<FileUpload />"}</h6>
      <FileUpload 
        dir="row" 
        className="border" 
        
      />
      <hr/>

      <h6>{"<FormDesigner />"}</h6>
      <div className="form-row mb-2">
        {[2,2,2,2,2,2].map((v, i) => <div key={i} className={"FormDesignerRulerDev py-2 bg-light border border-dark col-md-" + v}>{v}</div>)}
      </div>

      <FormDesigner 
        data={DUMMY_FORMS}
      />
      <hr/>

      <h6>Table custom (Without {"<table />"})</h6>
      <DndProvider backend={HTML5Backend}>
        <FormTableEditor 
          data={sectionTables} 
          onResize={data => setSectionTables(data)} 
          // onDoubleClick={data => setSectionTables(data)} 
          onDelete={data => setSectionTables(data)} 
          onAdd={data => setSectionTables(data)} 
          onDrop={(dataTarget, dropResult) => {
            if(dataTarget && dropResult && dataTarget.colIndex !== dropResult.colIndex){
              console.log('onDrop dataTarget: ', dataTarget);
              console.log('onDrop dropResult: ', dropResult);
              const newData = sectionTables.map((v, i) => {
                if(i === dropResult.colIndex){
                  return dataTarget.data;
                }
                if(i === dataTarget.colIndex){
                  return dropResult.data;
                }
                return v;
              });
              // console.log('onDrop newData: ', newData);
              setSectionTables(newData);
            }
          }}
        />
      </DndProvider>
      <hr/>

      <h6>Native Emoji</h6>
      <Btn 
        onClick={() => {
          import('../../../data/const/NATIVE_EMOJI').then(m => {
            console.log('m: ', m);
            if(m?.default) setEmoji(m.default);
          });
        }}
      >
        EMOJI
      </Btn>
      {emoji.map((v, i) => 
        <div key={i} className="card">
          <div className="card-header">{v.group}</div>
          <div className="card-body">
            {v.emoji.map((v2, i2) => 
              <Btn key={i2} kind="flat" size="lg">{v2.e}</Btn>
            )}
          </div>
        </div>
      )}
      <hr/>

      <h6>{"<ColorPicker />"}</h6>
      <ColorPicker 
        // open={true} 
        className="w-50" 
        inputProps={{
          className: "text-monospace"
        }}
        dropdownMenuProps={{
          className: "w-100"
        }}
        value={colorVal} 
        onChange={setColorVal} 
      />
      <hr/>

      <h6>{"<Sorter /> - with react-dnd"}</h6>
      <DndProvider backend={HTML5Backend} 
        options={{ enableMouseEvents: true }}
      >
        <Sorter 
          mode="move" // copy | move
        />
      </DndProvider>
      <hr/>

      <h6>Youtube API - Search (NOT IMPLEMENTS)</h6>
      <Input 
        value={ytFind} 
        onChange={e => {
          const val = e.target.value;
          setYtFind(val);

        }}
      />
      <hr/>

      <h6>{"<Tree />"}</h6>
      <div style={{ width:300, height:200 }} className="border ovyauto">
        <Tree 
          className="q-rc-tree" 
          expandedKeys={expandKeys} 
          onExpand={onExpand} 
          autoExpandParent={autoExpandParent} 
          draggable 
          onDragStart={onDragStart}
          onDragEnter={onDragEnter}
          onDrop={onDrop} 
          allowDrop={allowDrop} 
          treeData={treeData} 
        />
      </div>
      <hr/>

      {/* <h6>emoji-mart</h6>
      <Picker native />

      {[ // 'native', 
        'apple', 'google', 'twitter', 'facebook'
      ].map((v) => 

        <Picker key={v} 
          set={v} 
          // emojiSize={20} 
          onSelect={(em) => {
            console.log('onSelect em: ', em);
          }} 
        />
      )} */}

      {/* <Picker title="Pick your emoji" emoji="point_up" /> */}
      {/* <Picker i18n={{ search: "Recherche", categories: { search: 'Résultats de recherche', recent: 'Récents' } }} /> */}
      <hr/>

      {/* <h6>Emoji</h6> */}
      {/* <div className="ml-2-next">
        {(emoji && emoji.emojis) && 
          Object.entries(emoji.emojis).map((v, i) => 
            <div key={i} className="d-inline-block" 
              title={v[1].a} 
              // dangerouslySetInnerHTML={{ __html: "&#x" + v[1].b + ";" }} 

            />
          )
        }
      </div> */}

      {/* <ol>
        {emoji.map((v, i) => 
          <li key={i} title={v.a} dangerouslySetInnerHTML={{ __html: "&#x" + v.b + ";" }}>
            
          </li>
        )}
      </ol>
      <hr/> */}

      <h6>{"<IconPicker />"}</h6>
      <p>Icon fonts</p>
      <IconPicker 
        autoFocus 
        copy={false} 
        reload // DEFAULT = false
        // cache={false} 
        url={Q.baseURL + "/public/js/esm/const/SELECTION.js"} // "/public/json/selection.json"
        // onHoverIcon={(icon) => { // , e
        //   console.log('onHoverIcon icon: ', icon);
        //   // console.log('onHoverIcon e: ', e);
        // }}
        onClickIcon={(icon) => { // , e
          console.log('onClickIcon icon: ', icon);
          // console.log('onClickIcon e: ', e);
        }}
      />
      {" "}
      <IconPicker 
        autoFocus 
        // cache={false} 
        storageKey="svg" 
        url={Q.baseURL + "/public/js/esm/const/SVG_SRC.js"} // "/public/json/svg_src.json"
        label="Svg" 
        // onHoverIcon={(icon) => { // , e
        //   console.log('onHoverIcon icon: ', icon);
        //   // console.log('onHoverIcon e: ', e);
        // }} 
        onClickIcon={(icon) => { // , e
          console.log('onClickIcon icon: ', icon);
          // console.log('onClickIcon e: ', e);
        }}
      />
      <hr/>
      
      <h6>ColorThief</h6>
      <Btn As="label" className="mb-2">
        <input type="file" hidden 
          onChange={async e => {
            const f = e.target.files[0];
            if(f){
              setFile(f);
              const a = await hasAlpha(f);
              setAlpha(a);
              console.log('alpha: ', a);
            }
          }}
        />
        {file.name || "Choose file"}
      </Btn>

      {file.name && 
        <img 
          alt={file.name} 
          src={URL.createObjectURL(file)} 
          onLoad={e => {
            const et = e.target;
            const ct = new ColorThief();
            if(alpha){
              et.style.backgroundColor = `rgb(${ct.getColor(et).join(",")})`;
            }else{
              if(et.style){
                Q.setAttr(et, "style");
              }
            }
            URL.revokeObjectURL(et.src);
          }}
          className="img-thumbnail d-block" 
        />
      }

      <hr/>

      <h6>Pdf</h6>
      <Btn onClick={createPdf}>PDF</Btn>{" "}
      <Btn onClick={onEditPdf}>Fill PDF Form</Btn>{" "}
      <Btn onClick={form2Pdf}>Form to PDF</Btn>{" "}
      <Btn onClick={() => console.log(formRef)}>formRef</Btn>
      <hr/>

      {/* {pdf && 
        <div ref={divRef} className="embed-responsive embed-responsive-4by3">
          <iframe src={pdf} title="PDF" className="embed-responsive-item" loading="lazy" />
        </div>
      } */}

      <form ref={formRef}>
        <fieldset>
          <label>Username</label>
          <input className="form-control" type="text" 
            defaultValue="Husein"
          />
        </fieldset>
      </form>
      <hr/>
      

    </div>
  );
}

/*
  // const gcd = (a, b) => {
  //   return b ? gcd(b, a % b) : a;
  // }

  // const aspectRatio = (width, height)  => {
  //   const divisor = gcd(width, height);
  //   return `${width / divisor}:${height / divisor}`;
  // }

<Btn
  onClick={() => {
    console.log('aspectRatio: ', aspectRatio(1920, 968));
    console.log('gcd: ', gcd(1920, 968));
  }}
>
  Calc Aspect Ratio by Pixel
</Btn>
<hr/>
*/