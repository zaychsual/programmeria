import React, { useEffect } from 'react';// useRef, useState, useEffect, useLayoutEffect

import Head from '../../../components/q-ui-react/Head';
// import Btn from '../../../components/q-ui-react/Btn';
import { ScrollTo } from '../../../components/q-ui-react/ScrollTo';

export default function ScrollToPage(){

  useEffect(() => {
    console.log('%cuseEffect in ScrollToPage','color:yellow');
  }, []);

  return (
    <div className="container-fluid py-3">
      <Head title="ScrollTo" />
      <h4>{"<ScrollTo />"}</h4>

      <ScrollTo 
        threshold={100} 
        scroll="top" // down
        style={{ height: 200 }} 
        className="card shadow-sm ovyauto" 
        btnProps={{ 
          style: { marginTop: -38 }, 
          className: "align-self-end position-sticky b8 r8 zi-2 tip tipL qi qi-arrow-up" // up | down
        }}
      >
        <div className="card-body">
          <ol className="mb-0">
            {Array.from({length:40}).map((v, i) => 
              <li key={i}>DUMMY - {i + 1}</li>
            )}
          </ol>
        </div>
      </ScrollTo>
    </div>
  );
}

