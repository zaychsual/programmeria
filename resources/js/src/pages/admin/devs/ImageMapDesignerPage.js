import React, { useRef, useState, useEffect } from 'react';

import Head from '../../../components/q-ui-react/Head';
import { ImageMapDesigner } from '../../../apps/image-map-designer/ImageMapDesigner';

export default function ImageMapDesignerPage(){
  

  return (
    <div className="container-fluid py-3">
      <Head title="Image Map Designer" />

      <ImageMapDesigner 

      />
    </div>
  );
}