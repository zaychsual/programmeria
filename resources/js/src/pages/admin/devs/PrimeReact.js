import React, { useRef, useState, useEffect } from 'react';// { useState, useRef, useEffect }
// import { InputNumber } from 'primereact/inputnumber';
import { confirmDialog } from 'primereact/confirmdialog'; // To use confirmDialog method
// import { Chart } from 'primereact/chart.js';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
// import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Calendar } from 'primereact/calendar';
import { MultiSelect } from 'primereact/multiselect';
import { ProgressBar } from 'primereact/progressbar';

import Head from '../../../components/q-ui-react/Head';
import Btn from '../../../components/q-ui-react/Btn';
import Input from '../../../components/q-ui-react/Input';
// import Flex from '../../../components/q-ui-react/Flex';
// import Img from '../../../components/q-ui-react/Img';
// import Placeholder from '../../../components/q-ui-react/Placeholder';

import { InputMask } from '../../../components/q-ui-react/InputMask';
import { InputNumber } from '../../../components/q-ui-react/InputNumber';
import { Resizer, ResizePanel } from '../../../components/q-ui-react/Resizer';
import { Toast } from '../../../components/q-ui-react/toast/Toast';

import { DATA_TYPE } from '../../../data/const/SQL_DATA';

// const parseOptions = DATA_TYPE.map((v) => (v.options ? { ...v, options: v.options.map(f => ({ label: f.l, value: f.v, t: f.t })) } : { label: v.l, value: v.v } ));
// console.log('parseOptions: ', parseOptions);

export default function PrimeReact(){
	const toastRef = useRef(null);
	const [toastPos, setToastPos] = useState("top-right");

	const [customers, setCustomers] = useState(null);
	const [selectedCustomers, setSelectedCustomers] = useState(null);
	const [globalFilter, setGlobalFilter] = useState(null);
	const [selectedRepresentatives, setSelectedRepresentatives] = useState(null);
	const [dateFilter, setDateFilter] = useState(null);
	const [selectedStatus, setSelectedStatus] = useState(null);
	// const [lang, setLang] = useState({ name:"Indonesia", code: "ID", icon:"indonesia" });
	// { value: "INT", label: "INT", t: "A 4-byte integer, signed range is -2,147,483,648 t…147,483,647, unsigned range is 0 to 4,294,967,295" }
	const [sqlDataType, setSqlDataType] = useState("INT");
	const dt = useRef(null);
	const representatives = [
		{name: "Amy Elsner", image: 'amyelsner.png'},
		{name: "Anna Fali", image: 'annafali.png'},
		{name: "Asiya Javayant", image: 'asiyajavayant.png'},
		{name: "Bernardo Dominic", image: 'bernardodominic.png'},
		{name: "Elwin Sharvill", image: 'elwinsharvill.png'},
		{name: "Ioni Bowcher", image: 'ionibowcher.png'},
		{name: "Ivan Magalhaes",image: 'ivanmagalhaes.png'},
		{name: "Onyama Limba", image: 'onyamalimba.png'},
		{name: "Stephen Shaw", image: 'stephenshaw.png'},
		{name: "XuXue Feng", image: 'xuxuefeng.png'}
	];
	const statuses = [
		'unqualified', 'qualified', 'new', 'negotiation', 'renewal', 'proposal'
	];
	const cols = [
		{ field: 'code', header: 'Code' },
		{ field: 'name', header: 'Name' },
		{ field: 'category', header: 'Category' },
		{ field: 'quantity', header: 'Quantity' }
	];
	const exportColumns = cols.map(col => ({ title: col.header, dataKey: col.field }));

	useEffect(() => {
		console.log('%cuseEffect in PrimeReact','color:yellow');
		axios.get(Q.baseURL + '/DUMMY/customers-large.json')
		.then(r => {
			console.log('r: ', r);
			setCustomers(r.data);
		});

		console.log('DATA_TYPE: ', DATA_TYPE);
	}, []);// eslint-disable-line react-hooks/exhaustive-deps

	const confirmPosition = (position) => {
		confirmDialog({
			message: 'Do you want to delete this record?',
			header: 'Delete Confirmation',
			icon: 'qi qi-info',
			position, 
			baseZIndex: 1021,
			// accept,
			// reject
		});
	}

	const renderHeader = () => {
		return (
			<div className="table-header d-flex justify-content-between align-items-center">
				<div>
					List of Customers 
					<div className="d-inline-flex export-buttons">
						<Btn onClick={() => exportCSV(false)} className="mr-2 qi qi-file" title="CSV" />
						<Btn kind="success" onClick={exportExcel} className="mr-2 qi qi-xls" title="XLS" />
						<Btn kind="warning" onClick={exportPdf} className="mr-2 qi qi-pdf" title="PDF" />
						{/* <Btn type="button" icon="pi pi-filter" onClick={() => exportCSV(true)} className="p-button-info p-ml-auto" title="Selection Only" /> */}
					</div>
				</div>
				<Input type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" className="w-auto" />
			</div>
		);
	}

	const activityBodyTemplate = (rowData) => {
		return (
			<React.Fragment>
				{/* <span className="p-column-title">Activity</span> */}
				<ProgressBar value={rowData.activity} showValue={false} />
			</React.Fragment>
		);
	}

	const actionBodyTemplate = () => {
		return (
			<Btn className="qi qi-cog" />
		);
	}

	const statusBodyTemplate = (rowData) => {
		return (
		<React.Fragment>
			{/* <span className="p-column-title">Status</span> */}
			<span className={Q.Cx('customer-badge', 'status-' + rowData.status)}>{rowData.status}</span>
		</React.Fragment>
		);
	}

	const nameBodyTemplate = (rowData) => {
		// console.log('rowData: ', rowData);
		// console.log('index: ', index);
		return (
			<React.Fragment>
				{/* <span className="p-column-title">Name</span> */}
				{rowData.name}
			</React.Fragment>
		);
	}

	const countryBodyTemplate = (rowData) => {
		let { name, code } = rowData.country;

		return (
			<React.Fragment>
				{/* <span className="p-column-title">Country</span> */}
				<img 
					src="https://primefaces.org/primereact/showcase/showcase/demo/images/flag_placeholder.png" 
					onError={(e) => e.target.src='https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png'} 
					alt={name} 
					className={Q.Cx('flag', 'flag-' + code)} 
				/>
				<span style={{verticalAlign: 'middle', marginLeft: '.5em'}}>{name}</span>
			</React.Fragment>
		);
	}

	const representativeBodyTemplate = (rowData) => {
		const src = "https://primefaces.org/primereact/showcase/showcase/demo/images/avatar/" + rowData.representative.image;

		return (
			<React.Fragment>
				{/* <span className="p-column-title">Representative</span> */}
				<img 
					alt={rowData.representative.name} 
					src={src} 
					onError={(e) => e.target.src='https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png'} 
					width="32" style={{verticalAlign: 'middle'}} />
				<span style={{verticalAlign: 'middle', marginLeft: '.5em'}}>{rowData.representative.name}</span>
			</React.Fragment>
		);
	}

	const dateBodyTemplate = (rowData) => {
		// return (
		// 	<React.Fragment>
		// 		<span className="p-column-title">Date</span>
		// 		<span>{rowData.date}</span>
		// 	</React.Fragment>
		// );
		return <span>{rowData.date}</span>;
	}

	const renderRepresentativeFilter = () => {
		return (
			<MultiSelect 
				className="p-column-filter" 
				value={selectedRepresentatives} 
				options={representatives}
				onChange={onRepresentativeFilterChange} 
				itemTemplate={representativeItemTemplate} 
				placeholder="All" 
				optionLabel="name" 
				optionValue="name" 
			/>
		);
	}

	const representativeItemTemplate = (option) => {
		const src = "https://primefaces.org/primereact/showcase/showcase/demo/images/avatar/" + option.image;

		return (
			<div className="p-multiselect-representative-option">
				<img alt={option.name} 
					src={src} 
					onError={(e) => e.target.src='https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png'} 
					width="32" style={{verticalAlign: 'middle'}} />
				<span style={{verticalAlign: 'middle', marginLeft: '.5em'}}>{option.name}</span>
			</div>
		);
	}

	const onRepresentativeFilterChange = (event) => {
		dt.current.filter(event.value, 'representative.name', 'in');
		setSelectedRepresentatives(event.value);
	}

	const renderDateFilter = () => {
		return (
			<Calendar 
				value={dateFilter} 
				onChange={onDateFilterChange} 
				placeholder="Registration Date" 
				dateFormat="yy-mm-dd" 
				className="p-column-filter" 
			/>
		);
	}

	const onDateFilterChange = (event) => {
		if (event.value !== null)
			dt.current.filter(formatDate(event.value), 'date', 'equals');
		else
			dt.current.filter(null, 'date', 'equals');

		setDateFilter(event.value);
	}

	const filterDate = (value, filter) => {
		if (filter === undefined || filter === null || (typeof filter === 'string' && filter.trim() === '')) {
			return true;
		}
		if (value === undefined || value === null) {
			return false;
		}
		return value === formatDate(filter);
	}

	const formatDate = (date) => {
		let month = date.getMonth() + 1;
		let day = date.getDate();
		if (month < 10) {
			month = '0' + month;
		}
		if (day < 10) {
			day = '0' + day;
		}
		return date.getFullYear() + '-' + month + '-' + day;
	}

	const renderStatusFilter = () => {
		return (
			<Dropdown 
				value={selectedStatus} 
				options={statuses} 
				onChange={onStatusFilterChange}
				itemTemplate={statusItemTemplate} 
				showClear 
				placeholder="Select a Status" 
				className="p-column-filter"
			/>
		);
	}

	const statusItemTemplate = (option) => {
		return (
			<span className={Q.Cx('customer-badge', 'status-' + option)}>{option}</span>
		);
	}

	const onStatusFilterChange = (event) => {
		dt.current.filter(event.value, 'status', 'equals');
		setSelectedStatus(event.value);
	}

	// const header = renderHeader();
	const representativeFilterElement = renderRepresentativeFilter();
	const dateFilterElement = renderDateFilter();
	const statusFilterElement = renderStatusFilter();

	const exportCSV = (selectionOnly) => {
		dt.current.exportCSV({ selectionOnly });
	}

	const exportPdf = () => {
		console.log('exportPdf');
		import('jspdf').then(jsPDF => {
			import('jspdf-autotable').then(() => {
				const doc = new jsPDF.default(0, 0);
				doc.autoTable(exportColumns, customers);
				doc.save('customers.pdf');
			})
		})
	}

	const exportExcel = () => {
		console.log('exportExcel');
		import('xlsx').then(xlsx => {
			console.log('xlsx: ', xlsx);
			const worksheet = xlsx.utils.json_to_sheet(customers);
			const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
			const excelBuffer = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
			saveAsExcelFile(excelBuffer, 'customers');
		});
	}

	const saveAsExcelFile = (buffer, fileName) => {
		console.log('saveAsExcelFile');
		import('file-saver').then(fileSave => {
			console.log('fileSave: ', fileSave);
			const FileSaver = fileSave.default;
			let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
			let EXCEL_EXTENSION = '.xlsx';
			const data = new Blob([buffer], { type: EXCEL_TYPE });
			FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
		});
	}

	return (
		<div className="container-fluid py-3">
			<Head title="PrimeReact" />
			<h1>PrimeReact</h1>

			<h6>{"<Dropdown />"}</h6>
			<Dropdown 
				value={sqlDataType} // lang
				// options={[
				// 	{ name:"English", code: "EN", icon:"united-kingdom" }, 
				// 	{ name:"Indonesia", code: "ID", icon:"indonesia" }
				// ]} 
				options={DATA_TYPE} // DATA_TYPE | parseOptions
				onChange={(v) => {
					console.log('onChange v: ', v);
					setSqlDataType(v.value);
				}} // setLang
				optionLabel="l" // l | label | name
				optionValue="v" // v | value | code
				optionGroupLabel="l" // l | label
				optionGroupChildren="options" 
				filter 
				showClear 
				showFilterClear 
				resetFilterOnHide 
				filterBy="v" // v | value | name
				filterPlaceholder="Search" // placeholder 
				// emptyFilterMessage="Not Found" 
				scrollHeight="300px" 
				panelClassName="q-p-select-panel input-sm sticky-head" // zi-1021
				panelStyle={{
					width: 240 
				}}
				// className="q-p-select" 
				optionGroupTemplate={(option) => option.l}
				// valueTemplate={(option, props) => {
				// 	if (option) {
				// 		return (
				// 			<div title={option.t}>{option.label}</div>
				// 		);
				// 	}
				// 	return <div>{props.placeholder}</div>;// null
				// }} 
				itemTemplate={(option) => (
					<div title={option.t}>{option.l}</div>
				)} 
			/>
			<hr/>

			<h6>{"<Datatable />"}</h6>
			<DataTable 
				ref={dt} 
				value={customers} 
				header={renderHeader()} // header 
				className="p-datatable-customers p-datatable-sm" 
				dataKey="id" 
				rowHover 
				globalFilter={globalFilter}
				selection={selectedCustomers} 
				onSelectionChange={e => setSelectedCustomers(e.value)} 
				paginator 
				rows={10} 
				emptyMessage="No customers found" 
				currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
				paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown" 
				rowsPerPageOptions={[10,25,50]} 

				resizableColumns 
				scrollable 
				scrollHeight="300px" 
			>
				<Column selectionMode="multiple" style={{width:'3em'}} />
				<Column field="name" header="Name" body={nameBodyTemplate} sortable filter filterPlaceholder="Search by name" />
				<Column sortField="country.name" filterField="country.name" header="Country" body={countryBodyTemplate} sortable filter filterMatchMode="contains" filterPlaceholder="Search by country"/>
				<Column sortField="representative.name" filterField="representative.name" header="Representative" body={representativeBodyTemplate} sortable filter filterElement={representativeFilterElement} />
				<Column field="date" header="Date" body={dateBodyTemplate} sortable filter filterMatchMode="custom" filterFunction={filterDate} filterElement={dateFilterElement} />
				<Column field="status" header="Status" body={statusBodyTemplate} sortable filter filterElement={statusFilterElement} />
				<Column field="activity" header="Activity" body={activityBodyTemplate} sortable filter filterMatchMode="gte" filterPlaceholder="Minimum" />
				<Column body={actionBodyTemplate} headerStyle={{width: '8em', textAlign: 'center'}} bodyStyle={{textAlign: 'center', overflow: 'visible'}} />
			</DataTable>
			<hr/>

			<h5>{"confirmDialog"}</h5>
			<h6>Basic</h6>
			<Btn
				onClick={() => {
					confirmDialog({
            message: 'Are you sure you want to proceed?',
            header: 'Confirmation', 
            icon: 'qi qi-info',
            baseZIndex: 1021, 
            // accept,
            // reject
        	});
				}}
			>
				Basic
			</Btn>
			<hr/>

			<h6>Position</h6>
			<div className="ml-1-next">
				{['left','right','top-left','top','top-right','bottom-left','bottom','bottom-right'].map((v, i) => 
					<Btn key={i}
						onClick={() => confirmPosition(v)} 
					>
						{v}
					</Btn>
				)}
			</div>
			<hr/>

			<h5>{"<InputMask />"}</h5>
			<div className="mt-3-next">
				<div>
					<label htmlFor="inputMaskBasic">Basic</label>
					<InputMask 
						id="inputMaskBasic" 
						placeholder="99-999999" 
						mask="99-999999" 
						// value={val1} 
						// onChange={(e) => setVal1(e.value)}
					/>
				</div>

				<div>
					<label htmlFor="inputMaskSsn">SSN</label>
					<InputMask 
						id="inputMaskSsn" 
						placeholder="999-99-9999" 
						mask="999-99-9999" 
					/>
				</div>

				<div>
					<label htmlFor="inputMaskDate">Date</label>
					<InputMask 
						id="inputMaskDate" 
						placeholder="99/99/9999" 
						mask="99/99/9999" 
					/>
				</div>

				<div>
					<label htmlFor="inputMaskPhone">Phone</label>
					<InputMask 
						id="inputMaskPhone" 
						placeholder="(999) 999-9999" 
						mask="(999) 999-9999" 
					/>
				</div>

				<div>
					<label htmlFor="inputMaskPhoneExt">Phone Ext</label>
					<InputMask 
						id="inputMaskPhoneExt" 
						placeholder="(999) 999-9999? x99999" 
						mask="(999) 999-9999? x99999" 
					/>
				</div>

				<div>
					<label htmlFor="inputMaskSerial">Serial</label>
					<InputMask 
						id="inputMaskSerial" 
						placeholder="a*-999-a999" 
						mask="a*-999-a999" 
					/>
				</div>
			</div>
			<hr/>

			<h5>{"<InputNumber />"}</h5>
			<InputNumber 
				showButtons 
				buttonLayout="horizontal" 
				locale="id-ID" 
				mode="decimal" 
				minFractionDigits={2} 
				id="integeronly" 
				// value={value1} 
				// onValueChange={(e) => setValue1(e.value)} 
			/>
			<hr/>

			<h5>{"<Toast />"}</h5>
			<Toast 
				ref={toastRef} 
				className="text-capitalize" 
				position={toastPos} 
			/>

			<h6>Kind</h6>
			<div className="ml-1-next">
				{[
					{ kind: "success", btn: "success" }, 
					{ kind: "info", btn: "info" }, 
					{ kind: "warn", btn: "warning" }, 
					{ kind: "error", btn: "danger" }, 
				].map((v, i) => 
					<Btn key={i} kind={v.btn} 
						onClick={() => {
							setToastPos("top-right");
							toastRef.current.show({
								kind: v.kind, // severity 
								summary: v.kind + " Message", 
								detail: "Message Content", 
								sticky: true, 
								// life: 3000, 
							});
						}}
					>
						{v.kind}
					</Btn>
				)}
			</div>

			<h6 className="mt-3">Positions</h6>
			<div className="ml-1-next">
				{["top-left", "top-right", "top-center", "bottom-left", "bottom-right", "bottom-center"].map((v, i) => 
					<Btn key={i} // className="" 
						onClick={() => {
							setToastPos(v);
							toastRef.current.clear();

							setTimeout(() => {
								toastRef.current.show({
									kind: "info", // success
									summary: v + " Message", 
									detail: "Message Content", 
									// sticky: true, 
									// life: 3000, 
								});
							}, 9);
						}}
					>
						{v}
					</Btn>
				)}
			</div>

			<h6 className="mt-3">Options</h6>
			<div className="ml-1-next">
				<Btn
					kind="warning" 
					onClick={() => {
						toastRef.current.show([
							{ kind:'info', summary:'Message 1', detail:'Message 1 Content', life: 3000 },
							{ kind:'info', summary:'Message 2', detail:'Message 2 Content', life: 3000 },
							{ kind:'info', summary:'Message 3', detail:'Message 3 Content', life: 3000 }
						]);
					}}
				>Multiple</Btn>

				<Btn 
					onClick={() => {
						toastRef.current.show({ kind: 'info', summary: 'Sticky Message', detail: 'Message Content', sticky: true });
					}}
				>Sticky</Btn>
			</div>
			
			<hr/>

			<h5>{"<Resizer />"}</h5>
			<h6>Horizontal</h6>
			<Resizer 
				style={{height: '300px'}} 
				className="border" 
			>
				<ResizePanel className="d-flex align-items-center justify-content-center">
					Panel 1
				</ResizePanel>

				<ResizePanel className="d-flex align-items-center justify-content-center">
					Panel 2
				</ResizePanel>
			</Resizer>
			<hr/>

			<h6>Vertical</h6>
			<Resizer 
				layout="vertical" 
				style={{height: '300px'}} 
				className="border" 
			>
				<ResizePanel className="d-flex align-items-center justify-content-center">
					Panel 1
				</ResizePanel>

				<ResizePanel className="d-flex align-items-center justify-content-center">
					Panel 2
				</ResizePanel>
			</Resizer>
			<hr/>

			<h6>Nested</h6>
			<Resizer 
				style={{height: '300px'}} 
				className="border"
			>
				<ResizePanel 
					className="d-flex align-items-center justify-content-center" 
					size={20} 
					minSize={10}
				>
					Panel 1
				</ResizePanel>

				<ResizePanel size={80}>
					<Resizer layout="vertical">
						<ResizePanel 
							className="d-flex align-items-center justify-content-center" 
							size={15}
						>
							Panel 2
						</ResizePanel>

						<ResizePanel size={85}>
							<Resizer>
								<ResizePanel 
									className="d-flex align-items-center justify-content-center" 
									size={20}
								>
									Panel 3
								</ResizePanel>

								<ResizePanel 
									className="d-flex align-items-center justify-content-center" 
									size={80}
								>
									Panel 4
								</ResizePanel>
							</Resizer>
						</ResizePanel>
					</Resizer>
				</ResizePanel>
			</Resizer>

			<hr/>


		</div>
	);
}

/*

*/