import React, { useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import Collapse from 'react-bootstrap/Collapse';
import { Editor, Frame, Element } from '@craftjs/core';// useNode

import Head from '../../../components/q-ui-react/Head';
// import Flex from '../../../components/q-ui-react/Flex';
import Btn from '../../../components/q-ui-react/Btn';
import { Resizer, ResizePanel } from '../../../components/q-ui-react/Resizer';

import { Dom } from '../../../apps/app-designer/Dom';// Block
import { Text } from '../../../apps/app-designer/Text';
import { Button } from '../../../apps/app-designer/Button';

// const DUMMY_DATA = {
//   rootNodeId: "node-a",
//   nodes: {
//     "node-a" : {
//       data: {
//         type: "div",
//         nodes: ["node-b", "node-c"]
//       }
//     },
//     "node-b" : {
//       data: {
//         type: "h2",
//         props: { children: "Hello" }
//       }
//     },
//     "node-c" : {
//       data: {
//         type: "h2",
//         props: { children: "World" }
//       }
//     }
//   }
// };

export default function AppDesignerPage(){
  // const [data, setData] = useState(DUMMY_DATA);
	
	useEffect(() => {
		console.log('%cuseEffect in AppDesignerPage','color:yellow;');
	}, []);

	return (
		<div className="container-fluid py-3">
			<Head title="App Designer" />

			{/* <Btn
				onClick={() => {
					let datas = [];
					for(let key in data){
						datas.push({ [key]: data[key] });
					}
					console.log('data: ', data);
					console.log('datas: ', datas);
				}}
			>DATA</Btn> */}

			<Resizer 
				style={{ height: 'calc(100vh - 80px)' }} 
				className="border" 
			>
				<ResizePanel 
					// As={Flex} 
					size={25} 
					minSize={15} 
				>
					<div className="w-100 h-100 ovyauto list-group list-group-flush">
						<Expand	open 
							label="Content" 
						>
							<div className="p-2 border">
								<div className="mt-1-next">
									{[
										{ text:"Typography" }, 
										{ text:"Images" }, 
										{ text:"Table" }, 
										{ text:"Figure" }, 
									].map((v, i) => 
										<Btn key={i} block size="sm" kind="light" className="text-left" draggable>{v.text}</Btn>
									)}
								</div>
							</div>
						</Expand>

						<Expand label="Components">
							<div className="p-2 border mt-1-next">
								{[
									"Alert","Badge","Breadcrumb","Button","Card","Carousel","Collapse","Dropdown",
									"Form","Input Group","Jumbotron","List Group","Media Object","Modal","Navs",
									"Navbar","Pagination","Popover","Progress","Spinner","Toast","Tooltip",
									"Embed","Video"
								].map((v, i) => 
									<Btn key={i} block size="sm" kind="light" className="text-left">{v}</Btn>
								)}
							</div>
						</Expand>
					</div>
				</ResizePanel>

				<ResizePanel 
					// As={Flex} 
					size={75} 
					className="view-app"
				>
					<div className="py-2r px-3 w-100 h-100 ovyauto">
						<Editor 
							resolver={{ 
								Dom, Button, Text
							}}
						> 
							<Frame>
								<Dom>
									<Element id="text" canvas 
										// is="section" 
									>
										<Text 
											draggable
											onDelete={() => {
												console.log('onDelete');
											}}
										>
											Hi world!
										</Text>
									</Element>
									
									<Element id="buttons" canvas>
										<Text 
											draggable
											onDelete={() => {
												console.log('onDelete');
											}}
										>
											Yes I'm Text
										</Text>
										<Button draggable>Primary</Button>{" "}
										<Button kind="danger" draggable>Danger</Button>
									</Element>
								</Dom>
							</Frame>
						</Editor>
					</div>
				</ResizePanel>
			</Resizer>
		</div>
	);
}

function Expand({ 
	open = false, 
	label, 
	children
}){
  const [show, setShow] = useState(open);
  return (
    <>
      <Btn blur 
				active={show} 
				kind="light" 
        onClick={() => setShow(!show)} 
        // aria-controls="example-collapse-text" 
        aria-expanded={show} 
				className={"list-group-item list-group-item-action py-2 position-sticky t0 zi-2 d-flex w-100 text-left rounded-0 qia qia-ml-auto qia-chevron-" + (show ? "down":"right")} 
      >
        {label}
      </Btn>

      <Collapse in={show} mountOnEnter>
        <div>{children}</div>
      </Collapse>
    </>
  );
}

/*
<div className="form-row mt-3">
	{[
		{ text:"Column" }, 
		{ text:"Button" }, 
		{ text:"Divider" }, 
		{ text:"Heading" }, 
		{ text:"Html" }, 
		{ text:"Image" }, 
		{ text:"Menu" }, 
		{ text:"Text" }, 
	].map((v, i) => 
		<div key={i} className="col mb-2">
			<Btn block As="div" kind="light" draggable>{v.text}</Btn>
		</div>
	)}
</div>
*/
