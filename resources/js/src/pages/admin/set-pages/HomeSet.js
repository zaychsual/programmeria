import React, { useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo, Fragment }
// import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
import Dropdown from 'react-bootstrap/Dropdown';
import Carousel from 'react-bootstrap/Carousel';
import ColorThief from 'colorthief';

import Head from '../../../components/q-ui-react/Head';
import Flex from '../../../components/q-ui-react/Flex';
import Form from '../../../components/q-ui-react/Form';
import Btn from '../../../components/q-ui-react/Btn';
import Input from '../../../components/q-ui-react/Input';
// import InputGroup from '../../../components/q-ui-react/InputGroup';
import Textarea from '../../../components/q-ui-react/Textarea';
import Img from '../../../components/q-ui-react/Img';
import ModalQ from '../../../components/q-ui-react/ModalQ';

export default function HomeSet(){
	const initialValues = {
		img: "", 
		url: "", 
		title: "", 
		description: ""
	};
	const INIT_SLIDER = {
		// img: "", 
		// img: "data:image/svg+xml,%3Csvg width='100%25' height='400' xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' style='font-family:sans-serif;font-size:3.5rem;text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23b3d6f4'%3E%3C/rect%3E%3Ctext x='50%25' y='50%25' fill='%23777' dy='.3em'%3EDUMMY IMAGE%3C/text%3E%3C/svg%3E", 
		title: "", 
		description: "", 
		url: "", 
		// caption_bg: "bg-transparent", 
		id: "slider-" + Q.Qid()
	};

	const [load, setLoad] = useState(false);
  // const [vals, setVals] = useState(initialValues);
	const [slider, setSlider] = useState([INIT_SLIDER]);
	const [sliderSee, setSliderSee] = useState(0);
	const [errorSlider, setErrorSlider] = useState();
	const [modal, setModal] = useState(false);
	const [modalData, setModalData] = useState(initialValues);
	const [playInterval, setPlayInterval] = useState(false);
	const [sliderSets, setSliderSets] = useState({
		controls: true, 
		indicators: true, 
		interval: 3000, // 5000
		wrap: true, 
		// Q-CUSTOM
		// caption_bg: "bg-transparent", 
	});
	
	useEffect(() => {
		console.log('%cuseEffect in HomeSet','color:yellow;');
		axios.get("/get_all/sliders").then(r => { // "/get_where_by/pages/name/home/row"
			const datas = r.data;
			console.log('r: ', r);
			if(r.status === 200 && datas && !datas.error && datas.length > 0){
				setSlider(datas);
			}
		}).catch(e => {
			console.log('e: ', e);
		}).then(() => setLoad(true));
	}, []);

	// const validationSchema = Yup.object().shape({
	// 	img: Yup.string().required("Slider image is required"), 
	// });

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues: modalData, // vals
		// validationSchema, 
		onSubmit: (values, fn) => {
			if(!values.img && !values.file){
				setErrorSlider('Slider image is required');
				fn.setSubmitting(false);
				return;
			}

			let newVal = Q.objWithValue(values);
			console.log('values: ', values);
			console.log('newVal: ', newVal);

			if(values.file){
				onXhrSave("/set-slider", Q.obj2formData(Q.omit(newVal, ['id','index'])), fn);
			}else{
				onXhrSave("/edit-slider", Q.obj2formData(Q.omit(newVal, ['index'])), fn);
			}

			setSlider(slider.map(f => ( f.id === values.id ? Q.omit(values, ['index']) : f )));
			setModal(false);
		},
	});

	const onXhrSave = (api, fdData, fn) => {
		axios.post(api, fdData, { // set_home_page
			headers: {'Content-Type': 'multipart/form-data'}
		}).then(r => {
			console.log('r: ', r);
			// const datas = r.data;
			if(r.status === 200 && r.data && !r.data.error){
				swalToast({ icon:"success", text:"Success save page Home" });
			}
		}).catch(e => {
			console.log('e: ', e);
		}).then(() => fn.setSubmitting(false));
	}

	const onAddSlider = () => {
		const id = "slider-" + Q.Qid();
		setSlider([ ...slider, { ...INIT_SLIDER, id } ]);
		setTimeout(() => setSliderSee(slider.length), 200);
	}

	const removeSlideTo = (id) => {
		const idx = slider.findIndex(f => f.id === id);
		const prevNext = idx === 0 ? 0 : idx - 1;
		// console.log('idx: ', idx);
		// console.log('prevNext: ', prevNext);

		setSlider(slider.filter(f => f.id !== id));
		setSliderSee(prevNext);
	}

	const onRemoveSlider = (v) => {
		// console.log('onRemoveSlider v: ', v);
		Swal.fire({
			icon: 'warning',
			title: 'Are you sure to remove this slider?',
			showCloseButton: true,
			allowEnterKey: false,
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then(r => {
			if(r.isConfirmed){
				if(v.created_at){// Remove db & state
					axios.post("/delete_by/sliders/id/" + v.id, Q.obj2formData({ path: "public/media/slider-home/" + v.img }))
					.then(r => {
						console.log('r: ', r);
						removeSlideTo(v.id);
					}).catch(e => console.log('e: ', e));
				}else{// remove state only
					removeSlideTo(v.id);
				}

				if(errorSlider) setErrorSlider(null);
			}
		});
	}

	// const onChangeTextSlider = (e, key) => {
	// 	setModalData({ ...modalData, [key]: e.target.value });
	// }

	// const onDelSliderData = (id, key) => {// val, 
	// 	setSlider(slider.map(f => ( f.id === id ? Q.omit(f, [key]) : f )));
	// }

	const onRemoveFileSlider = (id) => {
		Swal.fire({
			icon: 'warning',
			title: 'Are you sure to remove this image slider?',
			showCloseButton: true,
			allowEnterKey: false,
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then(r => {
			if(r.isConfirmed){
				Q.domQ("#imgSlider").value = null;// OPTION = ""
				const editVal = Q.omit(modalData, ['file']);
				setModalData(editVal);
				setSlider(slider.map(f => ( f.id === id ? editVal : f )));
				if(errorSlider) setErrorSlider(null);
			}
		});
	}

	const onChangeImgSlider = (e) => {
		const et = e.target;
		const file = et.files[0];
		if(file){
			setModalData({ ...modalData, file });
			if(errorSlider) setErrorSlider(null);
		}
	}

	const onLoadImgSlider = (e, v) => {
		const et = e.target;
		if(v.file){
			window.URL.revokeObjectURL(et.src);
		}

		const colorThief = new ColorThief();
		et.style.backgroundColor = 'rgb('+colorThief.getColor(et).join(",")+')';
	}

	return (
		<div className="container py-3">
			<Head title="Home Page Settings" />

			{load ? 
				slider ? 
					<>
						<h5 className="hr-h hr-left mb-4">Home</h5>
						<div className="card shadow-sm">
							<Flex wrap className="card-header align-items-center bg-light py-2 position-sticky t48 zi-4 ml-1-next">
								<Dropdown className="mr-2">
									<Dropdown.Toggle size="sm" variant="outline-primary">Open</Dropdown.Toggle>
									<Dropdown.Menu>
										<Dropdown.Item href={Q.baseURL}>Open Page</Dropdown.Item>
										<Dropdown.Item href={Q.baseURL} target="_blank">Open Page New Tab</Dropdown.Item>
									</Dropdown.Menu>
								</Dropdown>
								{/* <Btn onClick={() => console.log('slider: ',slider)} size="sm" kind="danger">GET SLIDER STATE</Btn> */}

								Slider / Carousel : {slider.length} item 
								<div className="ml-auto">
									<small>Go to Slider : </small>
									<select className="custom-select custom-select-sm w-auto" 
										title="" 
										onChange={e => setSliderSee(Number(e.target.value))}
									>
										{Array.from({length:slider.length}).map((v, i) => 
											<option key={i} value={i}>{i + 1}</option>
										)}
									</select>
								</div>
								<Btn size="sm" kind="info" className={"qi qi-" + (playInterval ? "pause":"play")}
									disabled={sliderSets.interval === "No Autoplay"} 
									title="Play/pause Autoplay" 
									onClick={() => setPlayInterval(!playInterval)}
								/>

								<Btn onClick={() => {
									setModalData({ ...sliderSets, sets:1 });
									setModal(true);
								}} size="sm" kind="light" className="qi qi-cog" title="Settings Slider" />
								<Btn onClick={onAddSlider} size="sm" className="qi qi-plus" title="Add Slider"></Btn>
							</Flex>

							<div className="card-body p-0">
								{slider.length > 0 ? 
									<Carousel 
										controls={sliderSets.controls} 
										indicators={sliderSets.indicators} 
										interval={sliderSets.interval !== "No Autoplay" && playInterval ? sliderSets.interval : null}//  
										wrap={sliderSets.wrap} // loop
										activeIndex={sliderSee} 
										onSelect={i => setSliderSee(i)}
									>
										{slider.map((v, i) => 
											<Carousel.Item key={v.id}>
												<Flex className="position-absolute t0 r0 mt-1 zi-2 mr-1 ml-1-next">
													<Btn onClick={() => {
														setModalData({ ...v, index: i });
														// setModalData(v);
														setModal(true);
													}} size="sm" kind="light">Edit</Btn>
													<Btn onClick={() => onRemoveSlider(v)} kind="danger" size="sm" className="tip tipL qi qi-close" aria-label="Remove this slider" />
												</Flex>

												<Img frame 
													wrapAs={v.url ? "a" : "div"} 
													wrapProps={{
														href: v.url || undefined, 
														onClick: e => e.stopPropagation() // OPTION
													}} 
													frameClass="d-block" 
													className="imgSliderMain d-block w-100 h-400px of-con" // ratio-16-9  bg-strip
													alt={v.title || " "} 
													src={v.img ? "public/media/slider-home/" + v.img : v.file ? window.URL.createObjectURL(v.file) : ""} // "public/media/slider-main/header-1-1.jpg"
													onLoad={e => onLoadImgSlider(e, v)}
												/>
												{/* sliderSets.caption_bg */}
												<Carousel.Caption className={Q.Cx("rounded", v.caption_bg)}>
													{v.title && <h5>{v.title}</h5>}

													{v.description && <p>{v.description}</p>}
												</Carousel.Caption>
											</Carousel.Item>
										)}
									</Carousel>
									: 
									<div className="alert alert-danger">Slider is empty</div>
								}
							</div>
						</div>
					</>
					: 
					<div className="alert alert-danger">
						Page Not Found
					</div>
				: 
				<div>LOADING</div>
			}

			<ModalQ 
				open={modal} 
				toggle={() => setModal(!modal)} 
				title={modalData && ("Slider " + (modalData.sets ? "Settings" : modalData.index + 1))} 
				bodyClass="mt-2-next" // w-350px mxh-350px ovxhide q-scroll px-3 mt-2-next
				body={
					modalData && 
					<>
						{modalData.sets ? 
							<>
								<div className="custom-control custom-checkbox">
									<input type="checkbox" className="custom-control-input" id="controls" 
										checked={modalData.controls} 
										onChange={e => setModalData({ ...modalData, controls: e.target.checked })}
									/>
									<label className="custom-control-label" htmlFor="controls">Slider controls</label>
								</div>

								<div className="custom-control custom-checkbox">
									<input type="checkbox" className="custom-control-input" id="indicators" 
										checked={modalData.indicators} 
										onChange={e => setModalData({ ...modalData, indicators: e.target.checked })}
									/>
									<label className="custom-control-label" htmlFor="indicators">Slider indicators</label>
								</div>

								<div className="custom-control custom-checkbox">
									<input type="checkbox" className="custom-control-input" id="wrap" 
										checked={modalData.wrap} 
										onChange={e => setModalData({ ...modalData, wrap: e.target.checked })}
									/>
									<label className="custom-control-label" htmlFor="wrap">Slider wrap</label>
								</div>

								<div>
									<label htmlFor="interval" className="small">Slider interval</label>
									<select className="custom-select" id="interval" 
										value={modalData.interval} 
										onChange={e => setModalData({ ...modalData, interval: e.target.value })}
									>
										{["No Autoplay", 2000, 3000, 4000, 5000].map((v, i) => <option key={i} value={v}>{v}</option>)}
									</select>
								</div>

								<Btn
									onClick={() => {
										if(modalData && modalData.sets){
											setSliderSets(modalData);
										}
										setModal(false);
									}}
								>Save</Btn>
							</>
							: 
							<Form noValidate 
								className={(formik.isSubmitting ? " i-load cwait" : null)} 
								style={formik.isSubmitting ? { '--bg-i-load': '95px' } : null} 
								fieldsetClass="mt-3-next" 
								disabled={formik.isSubmitting} 
								onSubmit={formik.handleSubmit} 
								onReset={formik.handleReset} // Q-OPTION
							>							
								<div>
									<label htmlFor="imgSlider" className="small">Slider image</label>
									<div className="input-group input-group-sm">
										<label className="form-control cpoin" tabIndex="0">
											{modalData.file ? modalData.file.name : "Upload image"} 
											<input type="file" accept="image/*" hidden required 
												id="imgSlider" 
												onChange={onChangeImgSlider} 
											/>
										</label>

										{(modalData.img && !modalData.file) && 
											<div className={"form-control form-control-sm position-absolute inset-0 w-100 rounded point-no" + Q.formikValidClass(formik, "img")}>{modalData.img}</div>
										}

										{modalData.file && 
											<div className="input-group-append">
												<Btn kind="light" className="tip tipTR qi qi-close xx" aria-label="Remove image" 
													onClick={() => onRemoveFileSlider(modalData.id)} 
												/>
											</div>
										}
									</div>

									{errorSlider && 
										<div className="invalid-feedback d-block">{errorSlider}</div>
									}

									{/* {(formik.touched.img && formik.errors.img) && 
										<div className="invalid-feedback d-block">{formik.errors.img}</div>
									} */}
								</div>

								<div>
									<label htmlFor="url" className="small">Slider Url</label>
									<Input isize="sm" type="url" 
										// id={"url" + modalData.id} 
										className={Q.formikValidClass(formik, "url")} 
										id="url" 
										// value={modalData.url || ""} 
										// onChange={e => onChangeTextSlider(e, "url")} 
										value={formik.values.url || ""} 
										onChange={formik.handleChange} 
									/>
								</div>

								<div>
									<label htmlFor="title" className="small">Slider title</label>
									<Input isize="sm" 
										// id={"title" + modalData.id} 
										className={Q.formikValidClass(formik, "title")} 
										id="title" 
										// value={modalData.title || ""} 
										// onChange={e => onChangeTextSlider(e, "title")} 
										value={formik.values.title || ""} 
										onChange={formik.handleChange} 
									/>
								</div>

								<div>
									<label htmlFor="description" className="small">Slider description</label>
									<Textarea isize="sm" 
										className={Q.formikValidClass(formik, "description")} 
										// id={"desc" + modalData.id} 
										id="description" 
										// value={modalData.description || ""} 
										// onChange={e => onChangeTextSlider(e, "description")} 
										value={formik.values.description || ""} 
										onChange={formik.handleChange} 
									/>
								</div>

								<div className="mb-2">
									<label className="small">Caption Style</label>
									<Flex className="position-relative">
										<Dropdown bsPrefix="btn-group position-static">
											<Dropdown.Toggle size="sm" variant="light">
												{formik.values.caption_bg && <i className={"rounded border color-pick-val " + formik.values.caption_bg} />} Background color
											</Dropdown.Toggle>
											<Dropdown.Menu className="px-2 pr-3 pb-0">
												<div className="form-row">
													{[
														// "primary","secondary","success","danger","warning", 
														// "info","light" // ,"white","dark","transparent","strip"
														{l:"Primary", v:"bg-primary"},
														{l:"Secondary", v:"bg-secondary"},
														{l:"Success", v:"bg-success"},
														{l:"Danger", v:"bg-danger"},
														{l:"Warning", v:"bg-warning"},
														{l:"Info", v:"bg-info"},
														{l:"Light", v:"bg-light", t:"dark"},
														{l:"Gray", v:"bg-eee", t:"dark"},
														{l:"Stripe", v:"bg-strip", t:"dark"},
														{l:"Dark Transparent", v:"bg-rgba-dark-1"},
														{l:"Light Transparent", v:"bg-rgba-light-3", t:"dark"}
													].map((f) => 
														<div key={f.v} className="col-2">
															<Dropdown.Item {...Q.DD_BTN} title={f.l} 
																bsPrefix={"w-100 mb-2 ratio-1-1 border shadow-sm btn btn-sm " + f.v + (f.v === formik.values.caption_bg ? " qi qi-check q-s22 text-" + (f.t || "white") : " nbsp")} 
																// className={"btn mb-2 ratio-1-1 shadow-sm " + v + (v === modalData.caption_bg ? " qi qi-check":"")} 
																onClick={() => {
																	// setModalData({ ...modalData, caption_bg: "bg-" + v });
																	formik.setFieldValue("caption_bg", f.v);
																}} 
															/>
														</div>
													)}
												</div>
											</Dropdown.Menu>
										</Dropdown>
										{formik.values.caption_bg && 
											<Btn kind="light" size="sm" className="ml-1 qi qi-close xx" 
												onClick={() => {
													// setModalData({ ...modalData, caption_bg: null });
													formik.setFieldValue("caption_bg", "");
												}}
											/>
										}
									</Flex>
								</div>

								<div className="text-right">
									<Btn type="reset" kind="dark">Reset</Btn>{" "}
									<Btn type="submit">Save</Btn>
								</div>
							</Form>
						}
					</>
				}
			/>
		</div>
	);
}
