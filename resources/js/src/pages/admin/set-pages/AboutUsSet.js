import React, { useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo, Fragment }
// import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
import Dropdown from 'react-bootstrap/Dropdown';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

import Head from '../../../components/q-ui-react/Head';
import Form from '../../../components/q-ui-react/Form';
import Flex from '../../../components/q-ui-react/Flex';
import Btn from '../../../components/q-ui-react/Btn';
import Input from '../../../components/q-ui-react/Input';
// import Textarea from '../../../components/q-ui-react/Textarea';
import getEmbedUrl from '../../../utils/getEmbedUrl';

function validateExternalUrl(val){
	// const urlReg = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/; // /^((https?|ftp|file):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
	// const isUrl = urlReg.exec(val);
	const isUrl = Q.urlSearch(Q.newURL(val).search).get('v');
	if(isUrl){
		// let sUrl;
		// if(isUrl[0]?.startsWith("https://youtu.be/")){
		// 	sUrl = "https://www.youtube.com/embed" + isUrl[4].replace("/embed","");
		// }else{
		// 	sUrl = isUrl[1] ? val : isUrl[4] ? "https://www.youtube.com/embed" + isUrl[4].replace("/embed","") : 'https://' + isUrl[0];
		// }
		// return sUrl;
		return "https://www.youtube.com/embed/" + isUrl;
	}
	else{
		const getSrc = getEmbedUrl(val);
		console.log('getSrc: ', getSrc);
		if(getSrc){
			return getSrc;
		}
		return val;// false
	}
}

export default function AboutUsSet(){
	const initValues = {
		title: "",
		// description: "", 
		embed: "", 
		// content: ""
	};
	const editorEmpty = EditorState.createEmpty();
	const [vals, setVals] = useState(initValues);
	const [descBanner, setDescBanner] = useState(editorEmpty);
	const [content, setContent] = useState(editorEmpty);
	const [moreContent, setMoreContent] = useState(editorEmpty);
	const [file, setFile] = useState({});
	
	useEffect(() => {
		console.log('%cuseEffect in AboutUsSet','color:yellow;');
		axios.get("/get_row/about_us").then(r => {
			console.log('r: ', r);
			const datas = r.data;
			if(datas && !datas.error){// r.status === 200 && 
				setVals(datas);

				[
					[datas.banner_description, setDescBanner],
					[datas.content, setContent],
					[datas.more_content, setMoreContent]
				].forEach(f => {
					if(f[0]){
						const blocksFromHtml = htmlToDraft(f[0]);
						const { contentBlocks, entityMap } = blocksFromHtml;
						const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
						f[1](EditorState.createWithContent(contentState));
					}
				})
			}
		}).catch(e => {
			console.log('e: ', e);
		});// .then(() => setLoad(false));
	}, []);

	// const validationSchema = Yup.object().shape({
	// 	// embed: Yup.string()
	// 	// 	// .min(2, "Minimum 2 symbols")
	// 	// 	// .max(50, "Maximum 50 symbols")
	// 	// 	.required("Google Map Url / Embed is required"),
	// 	description: Yup.string().required("Description is required"), 
	// });

	const setEditorValue = (state, newVal, key) => {
		const val = state.getCurrentContent();
		if(val.hasText()){// DOMPurify.sanitize(html)
			newVal[key] = draftToHtml(convertToRaw(val)).trim();
		}
		else{
			// delete newVal[key];
			newVal[key] = "";
		}
	}

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues: vals, 
		// validationSchema, 
		onSubmit: (values, fn) => {
			// const url = Q.isAbsoluteUrl(values.embed) ? values.embed : validateExternalUrl(values.embed);
			let newVal = {
				...values, 
				embed: validateExternalUrl(values.embed), 
			};

			[
				[descBanner, "banner_description"],
				[content, "content"],
				[moreContent, "more_content"]
			].forEach(f => {
				setEditorValue(f[0], newVal, f[1]);
			});

			console.log('values: ', values);
			console.log('newVal: ', newVal);

			axios.post("/set-about-us", Q.obj2formData(newVal)).then(r => {
				console.log('r: ', r);
				const datas = r.data;
				if(r.status === 200 && datas && !datas.error){
					swalToast({ icon:"success", text:"Success save page About Us" });
				}
			}).catch(e => {
				console.log('e: ', e);
			}).then(() => fn.setSubmitting(false));
		},
	});

	const onChangeFile = e => {
		const f = e.target.files[0];
		if(f){
			setFile(f);
			formik.setFieldValue("img_banner", f.name);
		}
	}

	const onDetailSwal = () => {
		const fname = file.name || formik.values.img_banner;
		if(fname){
			Swal.fire({
				title: fname, 
				allowEnterKey: false, 
				showCloseButton: true, 
				showConfirmButton: false, 
				imageUrl: file.name ? window.URL.createObjectURL(file) : "public/media/menus/" + formik.values.img_banner, 
				imageHeight: 400, 
				imageAlt: fname, 
				didOpen(){
					if(file.name){
						const img = Swal.getImage();
						if(img) window.URL.revokeObjectURL(img.src);
					}
				}
			});
		}
	}

	const renderEditor = (label, state, fn, h = 98) => {
		return (
			<div>
				<label>{label}</label>
				<Editor 
					editorState={state} 
					toolbar={{
						options: [
							'inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 
							'link', 'remove', 'history', // , 'emoji', 'embedded', 'colorPicker', 'image'
						],
						inline: {
							inDropdown: false, 
							options: ['bold', 'italic', 'underline', 'strikethrough', 'monospace', 'superscript']
						}, 
						fontSize: { dropdownClassName:"q-scroll" }, 
					}}
					toolbarClassName="bg-light" // position-sticky t48 
					wrapperClassName="shadow-sm" 
					editorClassName="border mt-n1px px-3 rounded-bottom ctext" 
					editorStyle={{ minHeight: h }} 
					onEditorStateChange={(state) => {
						fn(state);
						// console.log('onEditorStateChange state: ', state);
					}}
				/>
			</div>
		);
	}

	return (
		<div className="container-fluid py-3">
			<Head title="Tentang Kami" />
			<h5 className="hr-h hr-left mb-4">Tentang Kami</h5>

			<Form	noValidate 
				className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
				style={formik.isSubmitting ? { '--bg-i-load': '99px' } : null} 
				disabled={formik.isSubmitting} 
				onSubmit={formik.handleSubmit} 
				onReset={formik.handleReset} 
			>
				<Flex className="card-header bg-light py-2 position-sticky t48 zi-3 ml-1-next">
					<Dropdown className="mr-auto">
						<Dropdown.Toggle size="sm" variant="outline-primary">Open</Dropdown.Toggle>
						<Dropdown.Menu>
							<Dropdown.Item href={Q.baseURL + "/tentang-kami"}>Open Page</Dropdown.Item>
							<Dropdown.Item href={Q.baseURL + "/tentang-kami"} target="_blank">Open Page New Tab</Dropdown.Item>
						</Dropdown.Menu>
					</Dropdown>
					<Btn type="reset" size="sm" kind="dark">Reset</Btn>
					<Btn type="submit" size="sm">Save</Btn>
				</Flex>

				<div className="card-body p-3 mt-3-next">
					<div>
						<label htmlFor="title">Title</label>
						<Input 
							id="title" 
							className={Q.formikValidClass(formik, "title")} 
							value={formik.values.title} 
							onChange={formik.handleChange} 
						/>
					</div>

					{renderEditor("Banner Description", descBanner, setDescBanner)}

					<div>
						<label htmlFor="img_banner">Banner Image</label>
						<div className="input-group">
							<Btn As="label" kind="light" tabIndex="0" className="form-control text-left">
								{file.name || formik.values.img_banner || "Choose image"} 
								<input onChange={onChangeFile} id="img_banner" type="file" accept="video/*,image/*" hidden />
							</Btn>
							{(file.name || formik.values.img_banner) && 
								<div className="input-group-append">
									<Btn kind="light" title="Remove" className="qi qi-close xx" 
										onClick={() => {
											setFile({});
											formik.setFieldValue("img_banner","");
										}}
									/>
									<Btn onClick={onDetailSwal} kind="light" title="View" className="qi qi-img" />
								</div>
							}
						</div>
					</div>
					
					<div>
						<label htmlFor="embed">Embed</label>
						<Input 
							id="embed" 
							className={Q.formikValidClass(formik, "embed")} 
							value={formik.values.embed} 
							onChange={formik.handleChange} 
						/>
					</div>

					{renderEditor("Content", content, setContent)}

					{renderEditor("More Content", moreContent, setMoreContent, 245)}
				</div>
			</Form>
		</div>
	);
}

