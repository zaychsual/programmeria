import React, { useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo, Fragment }
import { useHistory } from 'react-router-dom';
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
import Dropdown from 'react-bootstrap/Dropdown';

import Head from '../../../components/q-ui-react/Head';
import Flex from '../../../components/q-ui-react/Flex';
import Form from '../../../components/q-ui-react/Form';
import Btn from '../../../components/q-ui-react/Btn';
import Input from '../../../components/q-ui-react/Input';
import Textarea from '../../../components/q-ui-react/Textarea';
import ModalQ from '../../../components/q-ui-react/ModalQ';
import Img from '../../../components/q-ui-react/Img';

export default function GallerySet(){
	const initValues = {
		title: "", 
		description: "", 
		img: ""
	};
	const history = useHistory();
	const uParam = Q.urlSearch().get("a");
	
	const [load, setLoad] = useState(false);
	const [list, setList] = useState([]);
  const [forms, setForms] = useState(initValues);
	const [file, setFile] = useState({});
	const [modal, setModal] = useState(false);

	useEffect(() => {
		console.log('%cuseEffect in GallerySet','color:yellow;');
		axios.post("/get_all/gallery", Q.obj2formData({ select: "id,title,description,img" })).then(r => {
			const datas = r.data;
			console.log('r: ', r);
			if(datas && !datas.error){// r.status === 200 && 
				setList(datas);
			}
		}).catch(e => {
			console.log('e: ', e);
		}).then(() => setLoad(true));

		if(uParam){
			setModal(true);
			history.replace(history.location.pathname);
		}
	}, []);

	const validationSchema = Yup.object().shape({
		img: Yup.string()
			// .min(2, "Minimum 2 symbols")
			.max(255, "Maximum 255 symbols")
			.required("Image is required"),
	});

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues: forms, 
		validationSchema, 
		onSubmit: (values, fn) => {
			let newVal = {
				...values
			};

			if(file.name){
				newVal.file = file;
				delete newVal.img;
			}
			if(!newVal.img){
				delete newVal.img;
			}
			console.log('values: ', values);
			console.log('newVal: ', newVal);
			
			axios.post("/set-gallery", Q.obj2formData(newVal), {
				headers: {'Content-Type': 'multipart/form-data'}
			}).then(r => {
				console.log('r: ', r);
				if(r.data && !r.data.error){
					swalToast({ icon:"success", text:"Success save gallery " + newVal.title });
					setList([ ...list, r.data ]);
					setModal(false);
				}
			}).catch(e => {
				console.log('e: ', e);
			}).then(() => fn.setSubmitting(false));
		},
	});

	const onChangeFile = e => {
		const f = e.target.files[0];
		if(f){
			setFile(f);
			formik.setFieldValue("img", f.name);
		}
	}

	const onDetailSwal = (data) => {
		const fname = (data && data.title) || file.name || formik.values.img;
		if(fname){
			Swal.fire({
				title: fname, 
				text: data && data.description, 
				allowEnterKey: false, 
				showCloseButton: true, 
				showConfirmButton: false, 
				imageUrl: file.name ? window.URL.createObjectURL(file) : "public/media/gallery/" + (data ? data.img : formik.values.img), 
				imageHeight: 400, 
				imageAlt: fname, 
				didOpen(){
					if(file.name){
						const img = Swal.getImage();
						if(img) window.URL.revokeObjectURL(img.src);
					}
				}
			});
		}
	}

	const onClickEdit = (e, v) => {
		e.stopPropagation();
		setForms(v);
		setModal(true);
	}

	const onDelete = (e, v) => {
		e.stopPropagation();
		console.log('onDelete v: ', v);
		Swal.fire({
			icon: "question", 
			title: "Are you sure to delete gallery " + v.title + "?", 
			showCloseButton: true, 
			allowEnterKey: false, 
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes'
		}).then(r => {
			if(r.isConfirmed){
				axios.post("/delete_by/gallery/id/" + v.id, Q.obj2formData({ path: "public/media/gallery/" + v.img }))
				.then(r => {
					// console.log('r: ', r);
					if(r.data && !r.data.error){
						swalToast({ icon:"success", text:"Success delete gallery " + v.title });
						setList(list.filter(f => f.id !== v.id));// Remove item

						if(modal) setModal(false);
					}
				}).catch(e => {
					console.log('e: ', e);
				});
			}
		});
	}

	return (
		<div className="container-fluid py-3">
			<Head title="Gallery" />
			<h5 className="hr-h hr-left mb-4">Gallery</h5>

			{load ? 
				<>
					<Flex wrap justify="between" align="center" className="py-2 mb-3 bg-white border-bottom position-sticky t48 zi-4 ml-1-next">
						<h6 className="m-0">Total Data : {list.length}</h6>
						<Btn onClick={() => setModal(true)} size="sm" className="ml-auto">Add</Btn>
						<Dropdown>
							<Dropdown.Toggle size="sm" variant="outline-primary">Open</Dropdown.Toggle>
							<Dropdown.Menu alignRight>
								<Dropdown.Item href={Q.baseURL + "/gallery-project"}>Open Page</Dropdown.Item>
								<Dropdown.Item href={Q.baseURL + "/gallery-project"} target="_blank">Open Page New Tab</Dropdown.Item>
							</Dropdown.Menu>
						</Dropdown>
					</Flex>

					{list.length > 0 ? 
						<div className="row">
							{list.map((v, i) => 
								<div key={i} className="col-12 col-md-3">
									<Img 
										frame 
										wrapProps={{
											tabIndex: "0", 
											onClick: () => {
												if(v.img) onDetailSwal(v);
											}
										}}
										frameClassPrefix="" 
										frameClass="embed-responsive embed-responsive-4by3 block rounded border shadow-sm czoomin" 
										alt={v.title} 
										className="lozad embed-responsive-item card-img-top of-cov ratio-4-3" //  bg-light
										src={v.img ? "public/media/gallery/" + v.img : ""} 
									>
										<div className="btn-group btn-group-sm position-absolute t0 l0 mt-1 ml-1 zi-1 shadow">
											<Btn onClick={e => onClickEdit(e, v)} kind="light" className="qi qi-edit" title="Edit" />
											<Btn onClick={e => onDelete(e, v)} kind="light" className="qi qi-trash xx" title="Delete" />
										</div>
									</Img>

									{v.description && <div className="figure-caption">{v.description}</div>}
								</div>
							)}
						</div>
						: 
						<div className="alert alert-info">
							No Data
						</div>
					}

					<ModalQ 
						open={modal} 
						position="center"  
						scrollable 
						size="lg" // lg | xl
						title={forms.id ? "Edit":"Add"} 
						toggle={() => setModal(!modal)} 
						onClosed={() => setForms(initValues)} 
						// bodyClass="p-0 ovhide rounded-bottom" 
						body={
							<Form noValidate 
								className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
								style={formik.isSubmitting ? { '--bg-i-load': '95px' } : null} 
								disabled={formik.isSubmitting} 
								onSubmit={formik.handleSubmit} 
								onReset={(e) => {
									formik.handleReset(e);
									setFile({});
								}} // Q-OPTION
							>
								<Flex className="card-header bg-light py-2 ml-1-next">
									{forms.id && <Btn onClick={e => onDelete(e, forms)} kind="danger">Delete</Btn>}
									<Btn type="reset" kind="dark">Reset</Btn>
									<Btn type="submit">Save</Btn>
								</Flex>

								<div className="card-body mt-3-next">
									<div>
										<label htmlFor="img">Image</label>
										<div className="input-group">
											<Btn As="label" kind="fff" tabIndex="0" 
												className={"form-control text-left" + Q.formikValidClass(formik, "img")}
											>
												{file.name || formik.values.img || "Choose image"} 
												<input onChange={onChangeFile} id="img" type="file" accept="image/*" hidden />
											</Btn>
											{(file.name || formik.values.img) && 
												<div className="input-group-append">
													<Btn kind="light" title="Remove" className="qi qi-close xx" 
														onClick={() => {
															setFile({});
															formik.setFieldValue("img","");
														}}
													/>
													<Btn kind="light" title="View" className="qi qi-img" 
														onClick={() => onDetailSwal()} 
													/>
												</div>
											}
										</div>

										{(formik.touched.img && formik.errors.img) && 
											<div className="invalid-feedback d-block">{formik.errors.img}</div>
										}
									</div>

									<div>
										<label htmlFor="title">Title</label>
										<Input required 
											className={Q.formikValidClass(formik, "title")} 
											id="title" 
											value={formik.values.title} 
											onChange={formik.handleChange} 
										/>
									</div>

									<div>
										<label htmlFor="description">Description</label>
										<Textarea rows="7" 
											id="description" 
											className={Q.formikValidClass(formik, "description")} 
											value={formik.values.description} 
											onChange={formik.handleChange} 
										/>
									</div>
								</div>
							</Form>
						}
						// foot={
						// 	<>
						// 		<Btn onClick={() => setModal(false)} kind="dark" size="xs">Close</Btn>
						// 	</>
						// }
					/>
				</>
				: 
				<div>LOADING</div>
			}
		</div>
	);
}

