import React, { useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo, Fragment }
// import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
import Dropdown from 'react-bootstrap/Dropdown';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
// import draftToHtml from 'draftjs-to-html';
// import htmlToDraft from 'html-to-draftjs';

import Head from '../../../components/q-ui-react/Head';
import Form from '../../../components/q-ui-react/Form';
import Flex from '../../../components/q-ui-react/Flex';
import Btn from '../../../components/q-ui-react/Btn';
import Input from '../../../components/q-ui-react/Input';
import Textarea from '../../../components/q-ui-react/Textarea';
import getEmbedUrl from '../../../utils/getEmbedUrl';

export default function AboutUsSet(){
	const initValues = {
		description: "", 
		embed: "", 
		content: ""
	};
	const [vals, setVals] = useState(initValues);
	const [editorState, setEditorState] = useState(EditorState.createEmpty());
	
	useEffect(() => {
		console.log('%cuseEffect in AboutUsSet','color:yellow;');

	}, []);

	// const validationSchema = Yup.object().shape({
	// 	// embed: Yup.string()
	// 	// 	// .min(2, "Minimum 2 symbols")
	// 	// 	// .max(50, "Maximum 50 symbols")
	// 	// 	.required("Google Map Url / Embed is required"),
	// 	description: Yup.string().required("Description is required"), 
	// });

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues: vals, 
		// validationSchema, 
		onSubmit: (values, fn) => {
			const url = Q.isAbsoluteUrl(values.embed) ? values.embed : getEmbedUrl(values.embed);
			console.log('values: ', values);
			console.log('url: ', url);

			// if(url){
			// 	fn.setFieldValue("gmap", url);
			// 	const vals = {
			// 		...page, 
			// 		data: JSON.stringify({ ...values, gmap: url })
			// 	};
			// 	console.log('values: ', values);
			// 	console.log('vals: ', vals);

			// 	axios.post("/set_page", Q.obj2formData(vals)).then(r => {
			// 		console.log('r: ', r);
			// 		const datas = r.data;
			// 		if(r.status === 200 && datas && !datas.error){
			// 			swalToast({ icon:"success", text:"Success save page " + page.title + "." });
			// 		}
			// 	}).catch(e => {
			// 		console.log('e: ', e);
			// 	}).then(() => fn.setSubmitting(false));
			// }else{
			// 	fn.setFieldError("gmap", "Google Map Url / Embed must from embed code from google map.");
			// 	fn.setSubmitting(false);
			// }
		},
	});

	return (
		<div className="container-fluid py-3">
			<Head title="Tentang Kami" />
			<h5 className="hr-h hr-left mb-4">Tentang Kami</h5>

			<div className="row">
				<div className="col-md-4">
					<Form	noValidate 
						className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
						style={formik.isSubmitting ? { '--bg-i-load': '99px' } : null} 
						disabled={formik.isSubmitting} 
						onSubmit={formik.handleSubmit} 
						onReset={formik.handleReset} 
					>
						<Flex className="card-header bg-light py-2 position-sticky t48 zi-3 ml-1-next">
							<Dropdown className="mr-auto">
								<Dropdown.Toggle size="sm" variant="outline-primary">Open</Dropdown.Toggle>
								<Dropdown.Menu>
									<Dropdown.Item href={Q.baseURL + "/tentang-kami"}>Open Page</Dropdown.Item>
									<Dropdown.Item href={Q.baseURL + "/tentang-kami"} target="_blank">Open Page New Tab</Dropdown.Item>
								</Dropdown.Menu>
							</Dropdown>
							<Btn type="reset" size="sm" kind="dark">Reset</Btn>
							<Btn type="submit" size="sm">Save</Btn>
						</Flex>

						<div className="card-body p-3 mt-3-next">
							<div>
								<label htmlFor="description">Description</label>
								<Textarea 
									id="description" 
									className={Q.formikValidClass(formik, "description")} 
									value={formik.values.description} 
									onChange={formik.handleChange} 
								/>
							</div>
							
							<div>
								<label htmlFor="embed">Embed</label>
								<Input 
									id="embed" 
									className={Q.formikValidClass(formik, "embed")} 
									value={formik.values.embed} 
									onChange={formik.handleChange} 
								/>
							</div>
						</div>


					</Form>
				</div>

				<div className="col-md-8">
					<label>Content</label>
					<Editor 
						editorState={editorState} 
						toolbar={{
							options: [
								'inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 
								'link', 'remove', 'history', // , 'emoji', 'embedded', 'colorPicker', 'image'
							],
							inline: {
								inDropdown: false, 
								// className: undefined, 
								// component: undefined,
								// dropdownClassName: undefined, 
								// , 'subscript'
								options: ['bold', 'italic', 'underline', 'strikethrough', 'monospace', 'superscript'], 
								// bold: { className: "tip tipT", "aria-label": "Bold" }, // icon: bold, 
								// italic: { icon: italic, className: undefined },
								// underline: { icon: underline, className: undefined },
								// strikethrough: { icon: strikethrough, className: undefined },
								// monospace: { icon: monospace, className: undefined },
								// superscript: { icon: superscript, className: undefined },
								// subscript: { icon: subscript, className: undefined },
							}, 
							// blockType: { className: "text-decoration-none" }, 
							fontSize: { dropdownClassName:"q-scroll" }, 
						}}
						toolbarClassName="position-sticky t48 bg-light" // relative
						wrapperClassName="shadow-sm" 
						editorClassName="border mt-n1px px-3 rounded-bottom ctext" 
						editorStyle={{ minHeight: 'calc(100vh - 226px)' }} 
						// localization={{
						// 	locale: "id", 
						// }}
						onEditorStateChange={(state) => {
							setEditorState(state);
						}}
					/>
				</div>
			</div>
		</div>
	);
}

