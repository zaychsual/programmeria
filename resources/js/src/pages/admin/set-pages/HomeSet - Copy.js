import React, { useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo, Fragment }
// import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
import Dropdown from 'react-bootstrap/Dropdown';
import Carousel from 'react-bootstrap/Carousel';
// import qs from 'qs';

import Flex from '../../../components/q-ui-react/Flex';
import Form from '../../../components/q-ui-react/Form';
import Btn from '../../../components/q-ui-react/Btn';
import Input from '../../../components/q-ui-react/Input';
import Textarea from '../../../components/q-ui-react/Textarea';

export function HomeSet(){
	const initValues = {
		gmap: "", 
		description: ""
	};
	const INIT_SLIDER = { url: "", label: "", description: "", id: "slider-" + Q.Qid() };// name: "", 

	const [load, setLoad] = useState(false);
  const [vals, setVals] = useState(initValues);
	const [page, setPage] = useState();
	const [slider, setSlider] = useState([INIT_SLIDER]);
	const [errorSlider, setErrorSlider] = useState();
	
	useEffect(() => {
		console.log('%cuseEffect in HomeSet','color:yellow;');
		axios.get("/get_where_by/pages/name/home/row").then(r => {
			const datas = r.data;
			console.log('r: ', r);
			if(r.status === 200 && datas && !datas.error){
				setPage(datas);
				
				// if(datas.data){
				// 	setVals(JSON.parse(datas.data));
				// }
			}
			// else{

			// }
			setLoad(true);
		}).catch(e => {
			console.log('e: ', e);
		});
	}, []);

	// const validationSchema = Yup.object().shape({
	// 	gmap: Yup.string()
	// 		// .min(2, "Minimum 2 symbols")
	// 		// .max(50, "Maximum 50 symbols")
	// 		.required("Google Map Url / Embed is required"),
	// 	description: Yup.string().required("Description is required"), 
	// });

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues: vals, 
		// validationSchema, 
		onSubmit: (values, fn) => {
			const sliderLength = slider.length;
			let fdData = new FormData();
			console.log('slider: ', slider);

			if(sliderLength < 1){
				// swalToast({ icon:"error", text:"Please insert all slider image", timer:9000 });
				// setErrorSlider("Please insert all slider image.");
				// fn.setSubmitting(false);
				Swal.fire({
					icon: 'warning',
					title: 'Are you sure to save this ' + page.title + ' without slider?',
					showCloseButton: true,
					allowEnterKey: false,
					showCancelButton: true,
					confirmButtonText: 'Yes',
					cancelButtonText: 'No'
				}).then(r => {
					if(r.isConfirmed){
						fdData.append('files[]', null);
						onXhrSave(fdData, fn);
					}else{
						fn.setSubmitting(false);
					}
				});
			}else{
				const invalidSliders = slider.filter(f => !f.file);// f.url.length < 1
				console.log('invalidSliders: ', invalidSliders);
				
				if(invalidSliders.length > 0){// invalidSliders && 
					swalToast({ icon:"error", text:"Please insert all slider image", timer:9000 });
					setErrorSlider("Please insert all slider image.");
					fn.setSubmitting(false);
					setTimeout(() => {
						invalidSliders.forEach((n, i) => {
							let el = Q.domQ("#sliderItem" + n.id);
							Q.setClass(el, "border-danger");
							Q.setClass(Q.domQ('[for="file'+n.id+'"].custom-file-label', el), "border-danger");
							if(i + 1 === sliderLength){
								window.scrollBy({
									top: el.getBoundingClientRect().y - 110, 
									left: 0,
									behavior: 'smooth'
								});
							}
						});
					}, 9);
				}else{
					setErrorSlider(null);
					// const vals = {
					// 	slider
					// };
					// const sliderData = slider.map(f => ({ url: f.url, label: f.label, description: f.description }));// ({ file: f.file }) | f.file
					console.log('values: ', values);
					// console.log('qs: ', qs.stringify(files));
	
					// let fdData = new FormData();
					slider.forEach(f => {
						fdData.append('url[]', f.url);
						fdData.append('label[]', f.label);
						fdData.append('description[]', f.description);
						fdData.append('files[]', f.file);// , f.file.name
					});
					// for(let pair of fdData.entries()){
					// 	console.log(pair[0]+ ': ', pair[1]);
					 // }
	
					 onXhrSave(fdData, fn);
				}
			}
		},
	});

	const onXhrSave = (fdData, fn) => {
		axios.post("/set_home_page", fdData, {
			headers: {'Content-Type': 'multipart/form-data'}
		}).then(r => {
			console.log('r: ', r);
			// const datas = r.data;
			if(r.status === 200 && r.data && !r.data.error){
				swalToast({ icon:"success", text:"Success save page " + page.title + "." });
			}
		}).catch(e => {
			console.log('e: ', e);
		}).then(() => fn.setSubmitting(false));
	}

	const onAddSlider = () => {
		const id = "slider-" + Q.Qid();
		setSlider([ ...slider, { ...INIT_SLIDER, id } ]);
		setTimeout(() => {
			// scroll2Down();
			setTimeout(() => Q.domQ("#" + id)?.focus(), 200);
		}, 9);
	}

	const onRemoveSlider = (id) => {
		setSlider(slider.filter(f => f.id !== id));
		if(errorSlider){
			setErrorSlider(null);
		}
	}

	const onChangeTextSlider = (e, id, key) => {
		setSlider(slider.map(f => ( f.id === id ? { ...f, [key]: e.target.value } : f )))
	}

	const onRemoveFileSlider = (id) => {
		Swal.fire({
			icon: 'warning',
			title: 'Are you sure to remove this image slider?',
			showCloseButton: true,
			allowEnterKey: false,
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then(r => {
			if(r.isConfirmed){
				Q.domQ("#file" + id).value = null;// OPTION = ""
				setSlider(slider.map(f => ( f.id === id ? { ...f, file: null } : f )));
			}
		});
	}

	const onChangeImgSlider = (e, id) => {
		const et = e.target;
		const file = et.files[0];
		if(file){
			const pCard = et.closest("#sliderItem" + id);
			if(pCard && Q.hasClass(pCard, "border-danger")){
				Q.setClass(pCard, "border-danger", "remove");
				Q.setClass(Q.domQ('[for="file'+id+'"].custom-file-label', pCard), "border-danger", "remove");
			}

			setSlider(slider.map(f => ( f.id === id ? { ...f, file } : f )));
		}
	}

	const onShowSliderImg = (file) => {
		Swal.fire({
			title: file.name, 
			allowEnterKey: false, 
			showCloseButton: true, 
			showConfirmButton: false, 
			imageUrl: window.URL.createObjectURL(file), 
			imageHeight: 400, 
			imageAlt: file.name,
			didOpen(){
				const img = Swal.getImage();
				if(img) window.URL.revokeObjectURL(img.src);
			}
		});
	}

	return (
		<>
			{load ? 
				page ? 
					<>
						<h5 className="hr-h hr-left mb-4">{page.title}</h5>
						<Form noValidate 
							className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
							style={formik.isSubmitting ? { '--bg-i-load': '95px' } : null} 
							disabled={formik.isSubmitting} 
							onSubmit={formik.handleSubmit} 
							onReset={formik.handleReset} // Q-OPTION
						>
							<Flex className="card-header bg-light py-2 position-sticky t48 zi-3 ml-1-next">
								<Dropdown className="mr-auto">
									<Dropdown.Toggle size="sm" variant="outline-primary">Open</Dropdown.Toggle>
									<Dropdown.Menu>
										<Dropdown.Item href={Q.baseURL}>Open Page</Dropdown.Item>
										<Dropdown.Item href={Q.baseURL} target="_blank">Open Page New Tab</Dropdown.Item>
									</Dropdown.Menu>
								</Dropdown>

								<Btn type="reset" size="sm" kind="dark">Reset</Btn>
								<Btn type="submit" size="sm">Save</Btn>
							</Flex>

							<div className="card-body">
								<div className="form-group">
									<Carousel
										interval={null} 
										
 									>
										<Carousel.Item>
											<img
												className="d-block w-100"
												src="public/media/slider-main/header-1-1.jpg"
												alt="First slide"
											/>
											<Carousel.Caption>
												<h3>First slide label</h3>
												<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
											</Carousel.Caption>
										</Carousel.Item>
										<Carousel.Item>
											<img
												className="d-block w-100"
												src="public/media/slider-main/HEADER2.jpg"
												alt="Second slide"
											/>

											<Carousel.Caption>
												<h3>Second slide label</h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
											</Carousel.Caption>
										</Carousel.Item>
									</Carousel>

									<Flex className="py-1 px-2 bg-white position-sticky zi-2 shadow-sm" style={{ top:94 }}>
										Slider / Carousel 
										<Btn onClick={onAddSlider} size="xs" className="qi qi-plus ml-auto">Add</Btn>
									</Flex>

									{slider.length > 0 ? 
										<div className="bg-strip p-3 border rounded shadow-sm mt-3-next">
											{slider.map((v, i) => // 
												<div key={v.id} className="card" id={"sliderItem" + v.id}>
													<div className="badge badge-info position-absolute mt-1 ml-1">{i + 1}</div>
													<Btn onClick={() => onRemoveSlider(v.id)} kind="danger" size="sm" className="position-absolute r0 mt-1 mr-1 tip tipL qi qi-close" aria-label="Remove this slider" />
													<div className="card-body">
														<div className="form-group">
															<label htmlFor={v.id} className="small">Slider Url</label>
															<Input type="url" // required 
																id={v.id} 
																value={v.url} 
																onChange={e => onChangeTextSlider(e, v.id, "url")} 
															/>
														</div>

														<div className="form-group">
															<label htmlFor={"file" + v.id} className="small">Slider image</label>
															<div className="input-group">
																<div className="custom-file">
																	<input id={"file" + v.id} type="file" className="custom-file-input cpoin" 
																		required 
																		accept="image/*" 
																		// name="files[]" 
																		title={v.file ? v.file.name : "Choose image"} 
																		onChange={e => onChangeImgSlider(e, v.id)} 
																	/>
																	<label htmlFor={"file" + v.id} className="custom-file-label">
																		{v.file ? v.file.name : "Choose image"}
																	</label>
																</div>

																{v.file && 
																	<div className="input-group-append">
																		<Btn kind="light" className="tip tipTR qi qi-img" aria-label="Show image"
																			onClick={() => onShowSliderImg(v.file)} 
																		/>
																		<Btn kind="light" className="tip tipTR qi qi-close xx" aria-label="Remove image" 
																			onClick={() => onRemoveFileSlider(v.id)} 
																		/>
																	</div>
																}
															</div>
														</div>

														<div className="form-group">
															<label htmlFor={"label" + v.id} className="small">Slider label</label>
															<Input 
																id={"label" + v.id} 
																value={v.label} 
																onChange={e => onChangeTextSlider(e, v.id, "label")} 
															/>
														</div>

														<div className="form-group">
															<label htmlFor={"desc" + v.id} className="small">Slider description</label>
															<Textarea // rows="7" 
																id={"desc" + v.id} 
																value={v.description} 
																onChange={e => onChangeTextSlider(e, v.id, "description")} 
															/>
														</div>
													</div>
												</div>
											)}
										</div>
										: 
										<div className="alert alert-danger">Slider is empty</div>
									}

									{errorSlider && 
										<div className="invalid-feedback d-block">{errorSlider}</div>
									}
								</div>

								<div className="form-group">
									<label htmlFor="gmap">Google Map Url / Embed</label>
									<div className="input-group">
										<Input id="gmap" required 
											className={Q.formikValidClass(formik, "gmap")}  
											value={formik.values.gmap} 
											onChange={formik.handleChange} 
										/>
										<div className="input-group-append">
											<Btn As="a" kind="light" href="https://maps.google.com/maps" rel="noopener noreferrer" target="_blank" >Gmap</Btn>
										</div>
									</div>

									<small className="form-text qi qi-info"> Guide</small>
									
									{(formik.touched.gmap && formik.errors.gmap) && 
										<div className="invalid-feedback d-block">{formik.errors.gmap}</div>
									}
								</div>

								<div className="form-group">
									<label htmlFor="description">Description</label>
									<Textarea rows="7" 
										id="description" 
										className={Q.formikValidClass(formik, "description")} 
										value={formik.values.description} 
										onChange={formik.handleChange} 
									/>

									{(formik.touched.description && formik.errors.description) && 
										<div className="invalid-feedback">{formik.errors.description}</div>
									}
								</div>
							</div>
						</Form>
					</>
					: 
					<div className="alert alert-danger">
						Page Not Found
					</div>
				: 
				<div>LOADING</div>
			}
		</>
	);
}

