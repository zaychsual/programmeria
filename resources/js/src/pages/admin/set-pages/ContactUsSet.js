import React, { useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo, Fragment }
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik, Formik, Field
import Dropdown from 'react-bootstrap/Dropdown';

import Head from '../../../components/q-ui-react/Head';
import Flex from '../../../components/q-ui-react/Flex';
import Form from '../../../components/q-ui-react/Form';
import Btn from '../../../components/q-ui-react/Btn';
import Input from '../../../components/q-ui-react/Input';
import Textarea from '../../../components/q-ui-react/Textarea';
import getEmbedUrl from '../../../utils/getEmbedUrl';

// const getEmbedUrl = (s) => {
// 	const getSrc = /<iframe.*?src=\s*(["'])(.*?)\1/.exec(s);
// 	console.log('getSrc: ', getSrc);

// 	if(getSrc && getSrc[2]){
// 		// console.log(getSrc[2]);
// 		return getSrc[2];
// 	}
// 	return false;
// }

export default function ContactUsSet(){
	const initValues = {
		gmap: "", 
		description: ""
	};
	
	const [load, setLoad] = useState(false);
  const [vals, setVals] = useState(initValues);
	const [page, setPage] = useState();
	
	useEffect(() => {
		console.log('%cuseEffect in ContactUsSet','color:yellow;');
		axios.get("/get_where_by/pages/name/contact_us/row").then(r => {
			const datas = r.data;
			console.log('r: ', r);
			if(r.status === 200 && datas && !datas.error){
				setPage(datas);
				
				if(datas.data){
					setVals(JSON.parse(datas.data));
				}
			}
			setLoad(true);
		}).catch(e => {
			console.log('e: ', e);
		});
	}, []);

	const validationSchema = Yup.object().shape({
		gmap: Yup.string()
			// .min(2, "Minimum 2 symbols")
			// .max(50, "Maximum 50 symbols")
			.required("Google Map Url / Embed is required"),
		description: Yup.string().required("Description is required"), 
	});

	const formik = useFormik({
		enableReinitialize: true, 
		initialValues: vals, 
		validationSchema, 
		onSubmit: (values, fn) => {
			const url = Q.isAbsoluteUrl(values.gmap) ? values.gmap : getEmbedUrl(values.gmap);
			console.log('values: ', values);
			console.log('url: ', url);
			if(url){
				fn.setFieldValue("gmap", url);
				const vals = {
					...page, 
					data: JSON.stringify({ ...values, gmap: url })
				};
				console.log('values: ', values);
				console.log('vals: ', vals);

				axios.post("/set_page", Q.obj2formData(vals)).then(r => {
					console.log('r: ', r);
					const datas = r.data;
					if(r.status === 200 && datas && !datas.error){
						swalToast({ icon:"success", text:"Success save page " + page.title + "." });
					}
				}).catch(e => {
					console.log('e: ', e);
				}).then(() => fn.setSubmitting(false));
			}else{
				fn.setFieldError("gmap", "Google Map Url / Embed must from embed code from google map.");
				fn.setSubmitting(false);
			}
		},
	});

	return (
		<div className="container-fluid py-3">
			{load ? 
				page ? 
					<>
						<Head title={page.title} />
						<h5 className="hr-h hr-left mb-4">{page.title}</h5>
						<Form noValidate 
							className={"card shadow-sm" + (formik.isSubmitting ? " i-load cwait" : "")} 
							style={formik.isSubmitting ? { '--bg-i-load': '95px' } : null} 
							disabled={formik.isSubmitting} 
							onSubmit={formik.handleSubmit} 
							onReset={formik.handleReset} // Q-OPTION
						>
							<Flex className="card-header bg-light py-2 position-sticky t48 zi-3 ml-1-next">
								<Dropdown className="mr-auto">
									<Dropdown.Toggle size="sm" variant="outline-primary">Open</Dropdown.Toggle>
									<Dropdown.Menu>
										<Dropdown.Item href={Q.baseURL + "/hubungi-kami"}>Open Page</Dropdown.Item>
										<Dropdown.Item href={Q.baseURL + "/hubungi-kami"} target="_blank">Open Page New Tab</Dropdown.Item>
									</Dropdown.Menu>
								</Dropdown>

								<Btn type="reset" size="sm" kind="dark">Reset</Btn>
								<Btn type="submit" size="sm">Save</Btn>
							</Flex>

							<div className="card-body">
								<div className="form-group">
									<label htmlFor="gmap">Google Map Url / Embed</label>
									<div className="input-group">
										<Input required 
											className={Q.formikValidClass(formik, "gmap")} 
											id="gmap" 
											value={formik.values.gmap} 
											onChange={formik.handleChange} 
											// {...formik.getFieldProps("gmap")} 
										/>
										<div className="input-group-append">
											<Btn As="a" kind="light" href="https://maps.google.com/maps" className="tip tipTR qi qi-map-marker" aria-label="Open Google Map" rel="noopener noreferrer" target="_blank" />
										</div>
									</div>

									<small className="form-text qi qi-info"> Guide</small>
									
									{(formik.touched.gmap && formik.errors.gmap) && 
										<div className="invalid-feedback d-block">{formik.errors.gmap}</div>
									}
								</div>

								<div className="form-group">
									<label htmlFor="description">Description</label>
									<Textarea rows="7" 
										id="description" 
										className={Q.formikValidClass(formik, "description")} 
										value={formik.values.description} 
										onChange={formik.handleChange} 
									/>

									{(formik.touched.description && formik.errors.description) && 
										<div className="invalid-feedback">{formik.errors.description}</div>
									}
								</div>
							</div>
						</Form>
					</>
					: 
					<div className="alert alert-danger">
						<Head title="Page Not Found" />
						
						Page Not Found
					</div>
				: 
				<div>LOADING</div>
			}
		</div>
	);
}

