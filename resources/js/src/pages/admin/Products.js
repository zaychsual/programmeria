import React, { useState, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import Dropdown from 'react-bootstrap/Dropdown';
// import Modal from 'react-bootstrap/Modal';

import Head from '../../components/q-ui-react/Head';
import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute';
import Img from '../../components/q-ui-react/Img';
import Btn from '../../components/q-ui-react/Btn';
import Table from '../../components/q-ui-react/Table';
import ModalQ from '../../components/q-ui-react/ModalQ';
import Iframe from '../../components/q-ui-react/Iframe';
import axios from 'axios';

function groupBy(arr, key){
	return arr.reduce((hash, obj) => {
		if(obj[key] === undefined || obj[key] === null) return hash;
		return Object.assign(hash, { [obj[key]]:( hash[obj[key]] || [] ).concat(obj)});
	}, {});
}

export default function Products(){
	const [load, setLoad] = useState(false);
  const [data, setData] = useState([]);
	const [byCategory, setByCategory] = useState([]);
	const [view, setView] = useState("Grid");
	const [showBy, setShowBy] = useState("Category");// All
	const [modal, setModal] = useState(false);
	const [dataModal, setDataModal] = useState();
	const [loadDisplay, setLoadDisplay] = useState();
	
	useEffect(() => {
		// console.log('%cuseEffect in Products','color:yellow;');
		axios.post("/get_all/products", Q.obj2formData({ select: "id,name,slug,category_id,category_name,product_type_id,product_type_name,img,publish,display" })).then(r => {
		// axios.get("/products").then(r => {
			console.log('products r: ', r);
			const data1 = r.data;
			if(data1 && !data1.error){// r.status === 200 && 
				setData(data1);
				const groups = groupBy(data1, "category_name");// category_id
				console.log('products groups: ', groups);
				setByCategory(groups);
				// const data2 = data1.map(f => ({ ...f, children: null }))
				// const groups = listToTree(data2);
			}
		}).catch(e => {
			console.log('e: ', e);
		}).then(() => {
			setTimeout(() => setLoad(true), 9)
		});
	}, []);

	const onModal = (e, v) => {
		Q.preventQ(e);
		setDataModal(v);
		setModal(true);
	}

	const onDel = (v) => {
		console.log('onDel v: ', v);
		Swal.fire({
			icon: "question", // warning
			title: "Are you sure to delete product " + v.name + "?", 
			showCloseButton: true, 
			allowEnterKey: false, 
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes'
		}).then(r => {
			if(r.isConfirmed){
				axios.delete("/delete_by/products/id/" + v.id).then(r => {
					console.log('r: ', r);
					if(r.data && !r.data.error){// r.status === 200
						swalToast({ icon:"success", text:"Success delete product" + v.name });
						setData(data.filter(f => f.id !== v.id));// Remove item
					}
				}).catch(e => {
					console.log('e: ', e);
				});
			}
		});
	}

	// const onChangeDisplay = (v, ) => {

	// }

	const renderBtnDisplay = (v, m = "mb-2", d = " d-block ") => {
		return (
			<Btn size="xs" aria-label="Change" 
				kind={(v.display && v.display === "1") ? "primary" : "light"} 
				className={m + d + "text-left bold qi qi-" + (v.display && v.display === "1" ? "check":"close") + (loadDisplay === v.id ? " cwait":" tip tipTL")} 
				disabled={loadDisplay === v.id} 
				onClick={() => {
					setLoadDisplay(v.id);
					const isDisplay = v.display && v.display === "1";
					axios.post("/change-display/" + v.id, Q.obj2formData({ display: isDisplay ? 0:1 }))
					.then(r => {
						console.log('r: ', r);
						if(r.data && !r.data.error){
							const newData = data.map((f) => ( f.id === v.id ? { ...f, display: isDisplay ? "0":"1" } : f ));
			
							setData(newData);
							setByCategory(groupBy(newData, "category_name"));
						}
					})
					.catch(e => console.log('e: ', e))
					.then(() => setLoadDisplay(null));
				}} 
			>
				Display in Home
				{loadDisplay === v.id && 
					<b className="spinner-border spinner-border-sm ml-2" role="status" aria-hidden="true" />
				}
			</Btn>
		)
	}

	return (
		<div className="container-fluid py-3">
			<Head title="Products" />
			<h4 className="hr-h hr-left mb-3">Products</h4>

			<Flex wrap justify="between" align="center" className="py-2 mb-3 bg-white border-bottom position-sticky t48 zi-4 ml-1-next">
				<h6 className="m-0">Total Products : {data.length}</h6>
				<div className="input-group input-group-sm w-auto ml-auto">
					<div className="input-group-prepend">
						<div className="input-group-text">Show By</div>
					</div>
					<select className="custom-select" 
						value={showBy} 
						onChange={e => setShowBy(e.target.value)}
					>
						{["All","Category"].map((v, i) => <option key={i} value={v}>{v}</option>)}
					</select>
				</div>

				<div className="btn-group btn-group-sm">
					{[
						{txt:"Grid", icon:'grid'}, 
						{txt:"Table", icon:'table'} // byCategory
					].map((v) => 
						<Btn key={v.txt} onClick={() => setView(v.txt)} active={v.txt === view} kind="light" 
							className={"tip tipBR qi qi-" + v.icon} 
							aria-label={"View as " + v.txt} 
						/>
					)}
				</div>

				{USER_DATA[0]?.isAdmin && <Aroute to="/product/add" btn="primary" size="sm">Add Product</Aroute>}
			</Flex>

			{load && 
				<>
					{view === "Grid" ? 
						data.length > 0 ? 
							showBy === "All" ? 
								<div className="row">
									{data.map((v, i) => 
										<div key={i} className="col-md-3 mb-3">
											<div className="card shadow-sm h-100">
												{/* {(v.display && v.display === "1") && <b className="badge badge-primary position-absolute mt-1 ml-1 zi-1 shadow qi qi-check"> Display in Home</b>} */}
												<Img 
													frame 
													wrapAs="a" 
													wrapProps={{
														href: Q.baseURL + "/produk/" + v.slug, 
														onClick: (e) => onModal(e, { src: Q.baseURL + "/produk/" + v.slug, title: v.name })
													}} 
													frameClassPrefix="" 
													frameClass="embed-responsive embed-responsive-4by3 block" //  rounded-top
													alt={v.name} 
													className="embed-responsive-item card-img-top of-cov ratio-4-3" // 16-9
													src={v.img ? "public/media/products/" + v.img : ""} 
												/>

												<Flex dir="column" align="start" className="card-body p-3">
													<h1 className="h6 card-title">
														<a className="text-dark" 
															href={Q.baseURL + "/produk/" + v.slug} 
															onClick={(e) => onModal(e, { src: Q.baseURL + "/produk/" + v.slug, title: v.name })}
														>{v.name}</a>

														<div className="small mt-1">Category : {v.category_name}</div>
													</h1>

													{renderBtnDisplay(v)}

													<p className={"card-text bold qi qi-" + (v.publish === "1" ? "check iblue":"close ired")}> Publish</p>

													<div className="w-100 text-right mt-auto">
														<Btn kind="info" size="sm" 
															href={Q.baseURL + "/produk/" + v.slug} 
															onClick={(e) => onModal(e, { src: Q.baseURL + "/produk/" + v.slug, title: v.name })}
														>Detail</Btn>{" "}

														<Aroute to={"/product/edit/" + v.id} btn="warning" size="sm">Edit</Aroute>{" "}
														<Btn onClick={() => onDel(v)} kind="danger" size="sm">Delete</Btn>
													</div>
												</Flex>
											</div>
										</div>
									)}
								</div> 
								:
								Object.entries(byCategory).map((v1, i1) => 
									<div key={i1} className="card shadow-sm mb-3">
										<div className="card-header bold py-2 position-sticky shadow-sm bg-light zi-2" style={{ top:92 }}>
											{v1[0]} :
										</div>
										
										<div className="card-body row">
											{v1[1].map((v, i) => 
												<div key={i} className="col-md-3 mb-3">
													<div className="card shadow-sm h-100">
														{/* {(v.display && v.display === "1") && 
															<Btn size="xs" className="position-absolute mt-1 ml-1 zi-2 shadow qi qi-check bold tip tipBL"
																aria-label="Change"
															>Display in Home</Btn>
														} */}

														<Img 
															frame 
															wrapAs="a" 
															wrapProps={{
																href: Q.baseURL + "/produk/" + v.slug, 
																onClick: (e) => onModal(e, { src: Q.baseURL + "/produk/" + v.slug, title: v.name })
															}} 
															frameClassPrefix="" 
															frameClass="embed-responsive embed-responsive-4by3 block" //  rounded-top
															alt={v.name} 
															className="embed-responsive-item card-img-top of-cov ratio-4-3" // 16-9
															src={v.img ? "public/media/products/" + v.img : ""} 
														/>

														<Flex dir="column" align="start" className="card-body p-3">
															<h1 className="h6 card-title">
																<a className="text-dark" 
																	href={Q.baseURL + "/produk/" + v.slug} 
																	onClick={(e) => onModal(e, { src: Q.baseURL + "/produk/" + v.slug, title: v.name })}
																>{v.name}</a>

																<div className="small mt-1">Category : {v.category_name}</div>
															</h1>

															{renderBtnDisplay(v)}

															<p className={"card-text bold qi qi-" + (v.publish === "1" ? "check iblue":"close ired")}> Publish</p>

															<div className="w-100 text-right mt-auto">
																<Btn kind="info" size="sm" 
																	href={Q.baseURL + "/produk/" + v.slug} 
																	onClick={(e) => onModal(e, { src: Q.baseURL + "/produk/" + v.slug, title: v.name })}
																>Detail</Btn>{" "}

																<Aroute to={"/product/edit/" + v.id} btn="warning" size="sm">Edit</Aroute>{" "}

																<Btn onClick={() => onDel(v)} kind="danger" size="sm">Delete</Btn>
															</div>
														</Flex>
													</div>
												</div>
											)}
										</div>
									</div>
								)
							:
							<div className="alert alert-info">
								No Data
							</div>
						: 
						<Table 
							fixThead 
							customScroll hover border sm // strip
							// responsiveStyle={{ maxHeight: 'calc(100vh - 170px)' }} 
							// layout="auto" // "auto" | "fix"
							// id,name,slug,category_id,category_name,product_type_id,product_type_name,img_thumb,publish
							thead={
								<tr>
									{showBy === "All" ? 
										[
											{ t:"No." }, 
											{ t:"ID" }, 
											{ t:"Name" }, 
											{ t:"Category" }, 
											{ t:"Publish" }, 
											{ t:"Display" }, 
											{ t:"Action", w:145 }
										].map((v, i) => 
											<th key={i} scope="col" style={{ width: v.w }}>{v.t}</th>
										)
										:
										[
											{ t:"No.", w:60 }, 
											{ t:"Category" }, 
											{ t:"Products" } // Action , w:62
										].map((v, i) => 
											<th key={i} scope="col" style={{ width: v.w }}>{v.t}</th>
										)
									}
								</tr>
							}
							tbody={
								showBy === "All" ? 
									data.map((v, i) => 
										<tr key={i}>
											<th scope="row">{i + 1}</th>
											<td>{v.id}</td>
											<td>
												<a className="text-dark"
													href={Q.baseURL + "/produk/" + v.slug} 
													onClick={(e) => onModal(e, { src: Q.baseURL + "/produk/" + v.slug, title: v.name })} 
												>{v.name}</a>
											</td>
											<td>{v.category_name}</td>
											<td className={"qi qi-" + (v.publish === "1" ? "check iblue":"close ired")}></td>
											{/* <td>{(v.display && v.display === "1") && <div className="qi qi-check iblue"> Display in Home</div>}</td> */}
											<td>
												{renderBtnDisplay(v, "", "")}
											</td>
											<td>
												<Btn size="xs" 
													href={Q.baseURL + "/produk/" + v.slug} 
													onClick={(e) => onModal(e, { src: Q.baseURL + "/produk/" + v.slug, title: v.name })}
												>Detail</Btn>{" "}

												{USER_DATA[0]?.isAdmin && 
													<>
														<Aroute to={"/product/edit/" + v.id} btn="warning" size="xs">Edit</Aroute>{" "}
														<Btn onClick={() => onDel(v)} size="xs" kind="danger">Delete</Btn>
													</>
												}
											</td>
										</tr>
									)
									: 
									Object.entries(byCategory).map((v1, i1) => 
										<tr key={i1}>
											<th scope="row">{i1 + 1}</th>
											<td>{v1[0]}</td>
											<td className="p-1 small">
												Total : {v1[1].length}
												<table className="table mt-1 mb-0">
													<tbody>
														{v1[1].map((v, i) => 
															<tr key={i} className="bg-white">
																<td>{i + 1}</td>
																<td>{v.name}</td>
																<td className={"bold qi qi-" + (v.publish === "1" ? "check iblue":"close ired")}> Publish
																	{/* {(v2.display && v2.display === "1") && <div className="qi qi-check iblue"> Display in Home</div>} */}
																	{renderBtnDisplay(v, "mt-1")}
																</td>
															</tr>
														)}
													</tbody>
												</table>
											</td>
										</tr>
									)
							}
						/>
					}

					<ModalQ 
						open={modal} 
						position="center"  
						// scrollable 
						// unmountOnClose={false} 
						size="xl" // lg 
						title={dataModal && dataModal.title} 
						toggle={() => setModal(!modal)} 
						bodyClass="p-0 ovhide rounded-bottom" 
						body={
							<Iframe 
								bs 
								ratio="21by9" 
								src={dataModal && dataModal.src} 
								title="Product Detail" 
								loading="lazy" 
							/>
						}
						// foot={
						// 	<>
						// 		<Btn onClick={() => setModal(false)} kind="dark" size="xs">Close</Btn>
						// 	</>
						// }
					/>
				</>
			}
		</div>
	);
}


