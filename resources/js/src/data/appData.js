const APP_NAME = 'Programmeria';

const APP_DOMAIN = ["https://programmeria.com", "https://www.programmeria.com"];

const APP_LANG = [
	{value:"en", txt:"English"},
	{value:"id", txt:"Indonesia"}
];

const EXTERNAL_VIDEO_KEY = "external-video";
const EXTERNAL_IMG_KEY = "external-image";
const EXTERNAL_SRC_KEY = "external-src";// eg: Google Map, iframe

export {
  APP_NAME, 
  APP_DOMAIN, 
  APP_LANG, 
  EXTERNAL_VIDEO_KEY, 
  EXTERNAL_IMG_KEY, 
  EXTERNAL_SRC_KEY, 

}