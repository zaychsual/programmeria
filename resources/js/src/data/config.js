// https://github.com/primefaces/primereact/blob/master/src/components/api/PrimeReact.js

export default class AppConfig {
  static ripple = false;
  static locale = 'en';
  static zIndex = 1021;// 1000
}

