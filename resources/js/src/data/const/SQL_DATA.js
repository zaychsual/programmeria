const DATA_TYPE = [
    // {
    // v: "INT",
    // t: "A 4-byte integer, signed range is -2,147,483,648 to 2,147,483,647, unsigned range is 0 to 4,294,967,295",
    // l: "INT"
    // }, {
    // v: "VARCHAR",
    // t: "A variable-length (0-65,535) string, the effective maximum length is subject to the maximum row size",
    // l: "VARCHAR"
    // }, {
    // v: "TEXT",
    // t: "A TEXT column with a maximum length of 65,535 (2^16 - 1) characters, stored with a two-byte prefix indicating the length of the value in bytes",
    // l: "TEXT"
    // }, {
    // v: "DATE",
    // t: "A date, supported range is 1000-01-01 to 9999-12-31",
    // l: "DATE"
    // }, 
{
  l: "Numeric", // label
  options: [{
      v: "TINYINT",
      t: "A 1-byte integer, signed range is -128 to 127, unsigned range is 0 to 255",
      l: "TINYINT"
  }, {
      v: "SMALLINT",
      t: "A 2-byte integer, signed range is -32,768 to 32,767, unsigned range is 0 to 65,535",
      l: "SMALLINT"
  }, {
      v: "MEDIUMINT",
      t: "A 3-byte integer, signed range is -8,388,608 to 8,388,607, unsigned range is 0 to 16,777,215",
      l: "MEDIUMINT"
  }, {
      v: "INT",
      t: "A 4-byte integer, signed range is -2,147,483,648 to 2,147,483,647, unsigned range is 0 to 4,294,967,295",
      l: "INT"
  }, {
      v: "BIGINT",
      t: "An 8-byte integer, signed range is -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807, unsigned range is 0 to 18,446,744,073,709,551,615",
      l: "BIGINT"
  }, {
      v: "DECIMAL",
      t: "A fixed-point number (M, D) - the maximum number of digits (M) is 65 (default 10), the maximum number of decimals (D) is 30 (default 0)",
      l: "DECIMAL"
  }, {
      v: "FLOAT",
      t: "A small floating-point number, allowable values are -3.402823466E+38 to -1.175494351E-38, 0, and 1.175494351E-38 to 3.402823466E+38",
      l: "FLOAT"
  }, {
      v: "DOUBLE",
      t: "A double-precision floating-point number, allowable values are -1.7976931348623157E+308 to -2.2250738585072014E-308, 0, and 2.2250738585072014E-308 to 1.7976931348623157E+308",
      l: "DOUBLE"
  }, {
      v: "REAL",
      t: "Synonym for DOUBLE (exception: in REAL_AS_FLOAT SQL mode it is a synonym for FLOAT)",
      l: "REAL"
  }, {
      v: "BIT",
      t: "A bit-field type (M), storing M of bits per value (default is 1, maximum is 64)",
      l: "BIT"
  }, {
      v: "BOOLEAN",
      t: "A synonym for TINYINT(1), a value of zero is considered false, nonzero values are considered true",
      l: "BOOLEAN"
  }, {
      v: "SERIAL",
      t: "An alias for BIGINT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE",
      l: "SERIAL"
  }]
}, {
  l: "Date and time",
  options: [{
      v: "DATE",
      t: "A date, supported range is 1000-01-01 to 9999-12-31",
      l: "DATE"
  }, {
      v: "DATETIME",
      t: "A date and time combination, supported range is 1000-01-01 00:00:00 to 9999-12-31 23:59:59",
      l: "DATETIME"
  }, {
      v: "TIMESTAMP",
      t: "A timestamp, range is 1970-01-01 00:00:01 UTC to 2038-01-09 03:14:07 UTC, stored as the number of seconds since the epoch (1970-01-01 00:00:00 UTC)",
      l: "TIMESTAMP"
  }, {
      v: "TIME",
      t: "A time, range is -838:59:59 to 838:59:59",
      l: "TIME"
  }, {
      v: "YEAR",
      t: "A year in four-digit (4, default) or two-digit (2) format, the allowable values are 70 (1970) to 69 (2069) or 1901 to 2155 and 0000",
      l: "YEAR"
  }]
}, {
  l: "String",
  options: [{
      v: "CHAR",
      t: "A fixed-length (0-255, default 1) string that is always right-padded with spaces to the specified length when stored",
      l: "CHAR"
  }, {
      v: "VARCHAR",
      t: "A variable-length (0-65,535) string, the effective maximum length is subject to the maximum row size",
      l: "VARCHAR"
  }, {
      v: "TINYTEXT",
      t: "A TEXT column with a maximum length of 255 (2^8 - 1) characters, stored with a one-byte prefix indicating the length of the value in bytes",
      l: "TINYTEXT"
  }, {
      v: "TEXT",
      t: "A TEXT column with a maximum length of 65,535 (2^16 - 1) characters, stored with a two-byte prefix indicating the length of the value in bytes",
      l: "TEXT"
  }, {
      v: "MEDIUMTEXT",
      t: "A TEXT column with a maximum length of 16,777,215 (2^24 - 1) characters, stored with a three-byte prefix indicating the length of the value in bytes",
      l: "MEDIUMTEXT"
  }, {
      v: "LONGTEXT",
      t: "A TEXT column with a maximum length of 4,294,967,295 or 4GiB (2^32 - 1) characters, stored with a four-byte prefix indicating the length of the value in bytes",
      l: "LONGTEXT"
  }, {
      v: "BINARY",
      t: "Similar to the CHAR type, but stores binary byte strings rather than non-binary character strings",
      l: "BINARY"
  }, {
      v: "VARBINARY",
      t: "Similar to the VARCHAR type, but stores binary byte strings rather than non-binary character strings",
      l: "VARBINARY"
  }, {
      v: "TINYBLOB",
      t: "A BLOB column with a maximum length of 255 (2^8 - 1) bytes, stored with a one-byte prefix indicating the length of the value",
      l: "TINYBLOB"
  }, {
      v: "BLOB",
      t: "A BLOB column with a maximum length of 65,535 (2^16 - 1) bytes, stored with a two-byte prefix indicating the length of the value",
      l: "BLOB"
  }, {
      v: "MEDIUMBLOB",
      t: "A BLOB column with a maximum length of 16,777,215 (2^24 - 1) bytes, stored with a three-byte prefix indicating the length of the value",
      l: "MEDIUMBLOB"
  }, {
      v: "LONGBLOB",
      t: "A BLOB column with a maximum length of 4,294,967,295 or 4GiB (2^32 - 1) bytes, stored with a four-byte prefix indicating the length of the value",
      l: "LONGBLOB"
  }, {
      v: "ENUM",
      t: "An enumeration, chosen from the list of up to 65,535 values or the special '' error value",
      l: "ENUM"
  }, {
      v: "SET",
      t: "A single value chosen from a set of up to 64 members",
      l: "SET"
  }]
}, {
  l: "Spatial",
  options: [{
      v: "GEOMETRY",
      t: "A type that can store a geometry of any type",
      l: "GEOMETRY"
  }, {
      v: "POINT",
      t: "A point in 2-dimensional space",
      l: "POINT"
  }, {
      v: "LINESTRING",
      t: "A curve with linear interpolation between points",
      l: "LINESTRING"
  }, {
      v: "POLYGON",
      t: "A polygon",
      l: "POLYGON"
  }, {
      v: "MULTIPOINT",
      t: "A collection of points",
      l: "MULTIPOINT"
  }, {
      v: "MULTILINESTRING",
      t: "A collection of curves with linear interpolation between points",
      l: "MULTILINESTRING"
  }, {
      v: "MULTIPOLYGON",
      t: "A collection of polygons",
      l: "MULTIPOLYGON"
  }, {
      v: "GEOMETRYCOLLECTION",
      t: "A collection of geometry objects of any type",
      l: "GEOMETRYCOLLECTION"
  }]
}, {
  l: "JSON",
  options: [{
      v: "JSON",
      t: "Stores and enables efficient access to data in JSON (JavaScript Object Notation) documents",
      l: "JSON"
  }]
}];

const COLLATIONS = [{
  l: "armscii8", // label
  options: [{
      v: "armscii8_bin",
      t: "Armenian, binary",
      l: "armscii8_bin"
  }, {
      v: "armscii8_general_ci",
      t: "Armenian, case-insensitive",
      l: "armscii8_general_ci"
  }]
}, {
  l: "ascii",
  options: [{
      v: "ascii_bin",
      t: "West European, binary",
      l: "ascii_bin"
  }, {
      v: "ascii_general_ci",
      t: "West European, case-insensitive",
      l: "ascii_general_ci"
  }]
}, {
  l: "big5",
  options: [{
      v: "big5_bin",
      t: "Traditional Chinese, binary",
      l: "big5_bin"
  }, {
      v: "big5_chinese_ci",
      t: "Traditional Chinese, case-insensitive",
      l: "big5_chinese_ci"
  }]
}, {
  l: "binary",
  options: [{
      v: "binary",
      t: "Binary",
      l: "binary"
  }]
}, {
  l: "cp1250",
  options: [{
      v: "cp1250_bin",
      t: "Central European, binary",
      l: "cp1250_bin"
  }, {
      v: "cp1250_croatian_ci",
      t: "Croatian, case-insensitive",
      l: "cp1250_croatian_ci"
  }, {
      v: "cp1250_czech_cs",
      t: "Czech, case-sensitive",
      l: "cp1250_czech_cs"
  }, {
      v: "cp1250_general_ci",
      t: "Central European, case-insensitive",
      l: "cp1250_general_ci"
  }, {
      v: "cp1250_polish_ci",
      t: "Polish, case-insensitive",
      l: "cp1250_polish_ci"
  }]
}, {
  l: "cp1251",
  options: [{
      v: "cp1251_bin",
      t: "Cyrillic, binary",
      l: "cp1251_bin"
  }, {
      v: "cp1251_bulgarian_ci",
      t: "Bulgarian, case-insensitive",
      l: "cp1251_bulgarian_ci"
  }, {
      v: "cp1251_general_ci",
      t: "Cyrillic, case-insensitive",
      l: "cp1251_general_ci"
  }, {
      v: "cp1251_general_cs",
      t: "Cyrillic, case-sensitive",
      l: "cp1251_general_cs"
  }, {
      v: "cp1251_ukrainian_ci",
      t: "Ukrainian, case-insensitive",
      l: "cp1251_ukrainian_ci"
  }]
}, {
  l: "cp1256",
  options: [{
      v: "cp1256_bin",
      t: "Arabic, binary",
      l: "cp1256_bin"
  }, {
      v: "cp1256_general_ci",
      t: "Arabic, case-insensitive",
      l: "cp1256_general_ci"
  }]
}, {
  l: "cp1257",
  options: [{
      v: "cp1257_bin",
      t: "Baltic, binary",
      l: "cp1257_bin"
  }, {
      v: "cp1257_general_ci",
      t: "Baltic, case-insensitive",
      l: "cp1257_general_ci"
  }, {
      v: "cp1257_lithuanian_ci",
      t: "Lithuanian, case-insensitive",
      l: "cp1257_lithuanian_ci"
  }]
}, {
  l: "cp850",
  options: [{
      v: "cp850_bin",
      t: "West European, binary",
      l: "cp850_bin"
  }, {
      v: "cp850_general_ci",
      t: "West European, case-insensitive",
      l: "cp850_general_ci"
  }]
}, {
  l: "cp852",
  options: [{
      v: "cp852_bin",
      t: "Central European, binary",
      l: "cp852_bin"
  }, {
      v: "cp852_general_ci",
      t: "Central European, case-insensitive",
      l: "cp852_general_ci"
  }]
}, {
  l: "cp866",
  options: [{
      v: "cp866_bin",
      t: "Russian, binary",
      l: "cp866_bin"
  }, {
      v: "cp866_general_ci",
      t: "Russian, case-insensitive",
      l: "cp866_general_ci"
  }]
}, {
  l: "cp932",
  options: [{
      v: "cp932_bin",
      t: "Japanese, binary",
      l: "cp932_bin"
  }, {
      v: "cp932_japanese_ci",
      t: "Japanese, case-insensitive",
      l: "cp932_japanese_ci"
  }]
}, {
  l: "dec8",
  options: [{
      v: "dec8_bin",
      t: "West European, binary",
      l: "dec8_bin"
  }, {
      v: "dec8_swedish_ci",
      t: "Swedish, case-insensitive",
      l: "dec8_swedish_ci"
  }]
}, {
  l: "eucjpms",
  options: [{
      v: "eucjpms_bin",
      t: "Japanese, binary",
      l: "eucjpms_bin"
  }, {
      v: "eucjpms_japanese_ci",
      t: "Japanese, case-insensitive",
      l: "eucjpms_japanese_ci"
  }]
}, {
  l: "euckr",
  options: [{
      v: "euckr_bin",
      t: "Korean, binary",
      l: "euckr_bin"
  }, {
      v: "euckr_korean_ci",
      t: "Korean, case-insensitive",
      l: "euckr_korean_ci"
  }]
}, {
  l: "gb18030",
  options: [{
      v: "gb18030_bin",
      t: "Chinese, binary",
      l: "gb18030_bin"
  }, {
      v: "gb18030_chinese_ci",
      t: "Chinese, case-insensitive",
      l: "gb18030_chinese_ci"
  }, {
      v: "gb18030_unicode_520_ci",
      t: "Chinese (UCA 5.2.0), case-insensitive",
      l: "gb18030_unicode_520_ci"
  }]
}, {
  l: "gb2312",
  options: [{
      v: "gb2312_bin",
      t: "Simplified Chinese, binary",
      l: "gb2312_bin"
  }, {
      v: "gb2312_chinese_ci",
      t: "Simplified Chinese, case-insensitive",
      l: "gb2312_chinese_ci"
  }]
}, {
  l: "gbk",
  options: [{
      v: "gbk_bin",
      t: "Simplified Chinese, binary",
      l: "gbk_bin"
  }, {
      v: "gbk_chinese_ci",
      t: "Simplified Chinese, case-insensitive",
      l: "gbk_chinese_ci"
  }]
}, {
  l: "geostd8",
  options: [{
      v: "geostd8_bin",
      t: "Georgian, binary",
      l: "geostd8_bin"
  }, {
      v: "geostd8_general_ci",
      t: "Georgian, case-insensitive",
      l: "geostd8_general_ci"
  }]
}, {
  l: "greek",
  options: [{
      v: "greek_bin",
      t: "Greek, binary",
      l: "greek_bin"
  }, {
      v: "greek_general_ci",
      t: "Greek, case-insensitive",
      l: "greek_general_ci"
  }]
}, {
  l: "hebrew",
  options: [{
      v: "hebrew_bin",
      t: "Hebrew, binary",
      l: "hebrew_bin"
  }, {
      v: "hebrew_general_ci",
      t: "Hebrew, case-insensitive",
      l: "hebrew_general_ci"
  }]
}, {
  l: "hp8",
  options: [{
      v: "hp8_bin",
      t: "West European, binary",
      l: "hp8_bin"
  }, {
      v: "hp8_english_ci",
      t: "English, case-insensitive",
      l: "hp8_english_ci"
  }]
}, {
  l: "keybcs2",
  options: [{
      v: "keybcs2_bin",
      t: "Czech-Slovak, binary",
      l: "keybcs2_bin"
  }, {
      v: "keybcs2_general_ci",
      t: "Czech-Slovak, case-insensitive",
      l: "keybcs2_general_ci"
  }]
}, {
  l: "koi8r",
  options: [{
      v: "koi8r_bin",
      t: "Russian, binary",
      l: "koi8r_bin"
  }, {
      v: "koi8r_general_ci",
      t: "Russian, case-insensitive",
      l: "koi8r_general_ci"
  }]
}, {
  l: "koi8u",
  options: [{
      v: "koi8u_bin",
      t: "Ukrainian, binary",
      l: "koi8u_bin"
  }, {
      v: "koi8u_general_ci",
      t: "Ukrainian, case-insensitive",
      l: "koi8u_general_ci"
  }]
}, {
  l: "latin1",
  options: [{
      v: "latin1_bin",
      t: "West European, binary",
      l: "latin1_bin"
  }, {
      v: "latin1_danish_ci",
      t: "Danish, case-insensitive",
      l: "latin1_danish_ci"
  }, {
      v: "latin1_general_ci",
      t: "West European, case-insensitive",
      l: "latin1_general_ci"
  }, {
      v: "latin1_general_cs",
      t: "West European, case-sensitive",
      l: "latin1_general_cs"
  }, {
      v: "latin1_german1_ci",
      t: "German (dictionary order), case-insensitive",
      l: "latin1_german1_ci"
  }, {
      v: "latin1_german2_ci",
      t: "German (phone book order), case-insensitive",
      l: "latin1_german2_ci"
  }, {
      v: "latin1_spanish_ci",
      t: "Spanish (modern), case-insensitive",
      l: "latin1_spanish_ci"
  }, {
      v: "latin1_swedish_ci",
      t: "Swedish, case-insensitive",
      l: "latin1_swedish_ci"
  }]
}, {
  l: "latin2",
  options: [{
      v: "latin2_bin",
      t: "Central European, binary",
      l: "latin2_bin"
  }, {
      v: "latin2_croatian_ci",
      t: "Croatian, case-insensitive",
      l: "latin2_croatian_ci"
  }, {
      v: "latin2_czech_cs",
      t: "Czech, case-sensitive",
      l: "latin2_czech_cs"
  }, {
      v: "latin2_general_ci",
      t: "Central European, case-insensitive",
      l: "latin2_general_ci"
  }, {
      v: "latin2_hungarian_ci",
      t: "Hungarian, case-insensitive",
      l: "latin2_hungarian_ci"
  }]
}, {
  l: "latin5",
  options: [{
      v: "latin5_bin",
      t: "Turkish, binary",
      l: "latin5_bin"
  }, {
      v: "latin5_turkish_ci",
      t: "Turkish, case-insensitive",
      l: "latin5_turkish_ci"
  }]
}, {
  l: "latin7",
  options: [{
      v: "latin7_bin",
      t: "Baltic, binary",
      l: "latin7_bin"
  }, {
      v: "latin7_estonian_cs",
      t: "Estonian, case-sensitive",
      l: "latin7_estonian_cs"
  }, {
      v: "latin7_general_ci",
      t: "Baltic, case-insensitive",
      l: "latin7_general_ci"
  }, {
      v: "latin7_general_cs",
      t: "Baltic, case-sensitive",
      l: "latin7_general_cs"
  }]
}, {
  l: "macce",
  options: [{
      v: "macce_bin",
      t: "Central European, binary",
      l: "macce_bin"
  }, {
      v: "macce_general_ci",
      t: "Central European, case-insensitive",
      l: "macce_general_ci"
  }]
}, {
  l: "macroman",
  options: [{
      v: "macroman_bin",
      t: "West European, binary",
      l: "macroman_bin"
  }, {
      v: "macroman_general_ci",
      t: "West European, case-insensitive",
      l: "macroman_general_ci"
  }]
}, {
  l: "sjis",
  options: [{
      v: "sjis_bin",
      t: "Japanese, binary",
      l: "sjis_bin"
  }, {
      v: "sjis_japanese_ci",
      t: "Japanese, case-insensitive",
      l: "sjis_japanese_ci"
  }]
}, {
  l: "swe7",
  options: [{
      v: "swe7_bin",
      t: "Swedish, binary",
      l: "swe7_bin"
  }, {
      v: "swe7_swedish_ci",
      t: "Swedish, case-insensitive",
      l: "swe7_swedish_ci"
  }]
}, {
  l: "tis620",
  options: [{
      v: "tis620_bin",
      t: "Thai, binary",
      l: "tis620_bin"
  }, {
      v: "tis620_thai_ci",
      t: "Thai, case-insensitive",
      l: "tis620_thai_ci"
  }]
}, {
  l: "ucs2",
  options: [{
      v: "ucs2_bin",
      t: "Unicode, binary",
      l: "ucs2_bin"
  }, {
      v: "ucs2_croatian_ci",
      t: "Croatian, case-insensitive",
      l: "ucs2_croatian_ci"
  }, {
      v: "ucs2_czech_ci",
      t: "Czech, case-insensitive",
      l: "ucs2_czech_ci"
  }, {
      v: "ucs2_danish_ci",
      t: "Danish, case-insensitive",
      l: "ucs2_danish_ci"
  }, {
      v: "ucs2_esperanto_ci",
      t: "Esperanto, case-insensitive",
      l: "ucs2_esperanto_ci"
  }, {
      v: "ucs2_estonian_ci",
      t: "Estonian, case-insensitive",
      l: "ucs2_estonian_ci"
  }, {
      v: "ucs2_general_ci",
      t: "Unicode, case-insensitive",
      l: "ucs2_general_ci"
  }, {
      v: "ucs2_general_mysql500_ci",
      t: "Unicode (MySQL 5.0.0), case-insensitive",
      l: "ucs2_general_mysql500_ci"
  }, {
      v: "ucs2_german2_ci",
      t: "German (phone book order), case-insensitive",
      l: "ucs2_german2_ci"
  }, {
      v: "ucs2_hungarian_ci",
      t: "Hungarian, case-insensitive",
      l: "ucs2_hungarian_ci"
  }, {
      v: "ucs2_icelandic_ci",
      t: "Icelandic, case-insensitive",
      l: "ucs2_icelandic_ci"
  }, {
      v: "ucs2_latvian_ci",
      t: "Latvian, case-insensitive",
      l: "ucs2_latvian_ci"
  }, {
      v: "ucs2_lithuanian_ci",
      t: "Lithuanian, case-insensitive",
      l: "ucs2_lithuanian_ci"
  }, {
      v: "ucs2_persian_ci",
      t: "Persian, case-insensitive",
      l: "ucs2_persian_ci"
  }, {
      v: "ucs2_polish_ci",
      t: "Polish, case-insensitive",
      l: "ucs2_polish_ci"
  }, {
      v: "ucs2_roman_ci",
      t: "West European, case-insensitive",
      l: "ucs2_roman_ci"
  }, {
      v: "ucs2_romanian_ci",
      t: "Romanian, case-insensitive",
      l: "ucs2_romanian_ci"
  }, {
      v: "ucs2_sinhala_ci",
      t: "Sinhalese, case-insensitive",
      l: "ucs2_sinhala_ci"
  }, {
      v: "ucs2_slovak_ci",
      t: "Slovak, case-insensitive",
      l: "ucs2_slovak_ci"
  }, {
      v: "ucs2_slovenian_ci",
      t: "Slovenian, case-insensitive",
      l: "ucs2_slovenian_ci"
  }, {
      v: "ucs2_spanish2_ci",
      t: "Spanish (traditional), case-insensitive",
      l: "ucs2_spanish2_ci"
  }, {
      v: "ucs2_spanish_ci",
      t: "Spanish (modern), case-insensitive",
      l: "ucs2_spanish_ci"
  }, {
      v: "ucs2_swedish_ci",
      t: "Swedish, case-insensitive",
      l: "ucs2_swedish_ci"
  }, {
      v: "ucs2_turkish_ci",
      t: "Turkish, case-insensitive",
      l: "ucs2_turkish_ci"
  }, {
      v: "ucs2_unicode_520_ci",
      t: "Unicode (UCA 5.2.0), case-insensitive",
      l: "ucs2_unicode_520_ci"
  }, {
      v: "ucs2_unicode_ci",
      t: "Unicode, case-insensitive",
      l: "ucs2_unicode_ci"
  }, {
      v: "ucs2_vietnamese_ci",
      t: "Vietnamese, case-insensitive",
      l: "ucs2_vietnamese_ci"
  }]
}, {
  l: "ujis",
  options: [{
      v: "ujis_bin",
      t: "Japanese, binary",
      l: "ujis_bin"
  }, {
      v: "ujis_japanese_ci",
      t: "Japanese, case-insensitive",
      l: "ujis_japanese_ci"
  }]
}, {
  l: "utf16",
  options: [{
      v: "utf16_bin",
      t: "Unicode, binary",
      l: "utf16_bin"
  }, {
      v: "utf16_croatian_ci",
      t: "Croatian, case-insensitive",
      l: "utf16_croatian_ci"
  }, {
      v: "utf16_czech_ci",
      t: "Czech, case-insensitive",
      l: "utf16_czech_ci"
  }, {
      v: "utf16_danish_ci",
      t: "Danish, case-insensitive",
      l: "utf16_danish_ci"
  }, {
      v: "utf16_esperanto_ci",
      t: "Esperanto, case-insensitive",
      l: "utf16_esperanto_ci"
  }, {
      v: "utf16_estonian_ci",
      t: "Estonian, case-insensitive",
      l: "utf16_estonian_ci"
  }, {
      v: "utf16_general_ci",
      t: "Unicode, case-insensitive",
      l: "utf16_general_ci"
  }, {
      v: "utf16_german2_ci",
      t: "German (phone book order), case-insensitive",
      l: "utf16_german2_ci"
  }, {
      v: "utf16_hungarian_ci",
      t: "Hungarian, case-insensitive",
      l: "utf16_hungarian_ci"
  }, {
      v: "utf16_icelandic_ci",
      t: "Icelandic, case-insensitive",
      l: "utf16_icelandic_ci"
  }, {
      v: "utf16_latvian_ci",
      t: "Latvian, case-insensitive",
      l: "utf16_latvian_ci"
  }, {
      v: "utf16_lithuanian_ci",
      t: "Lithuanian, case-insensitive",
      l: "utf16_lithuanian_ci"
  }, {
      v: "utf16_persian_ci",
      t: "Persian, case-insensitive",
      l: "utf16_persian_ci"
  }, {
      v: "utf16_polish_ci",
      t: "Polish, case-insensitive",
      l: "utf16_polish_ci"
  }, {
      v: "utf16_roman_ci",
      t: "West European, case-insensitive",
      l: "utf16_roman_ci"
  }, {
      v: "utf16_romanian_ci",
      t: "Romanian, case-insensitive",
      l: "utf16_romanian_ci"
  }, {
      v: "utf16_sinhala_ci",
      t: "Sinhalese, case-insensitive",
      l: "utf16_sinhala_ci"
  }, {
      v: "utf16_slovak_ci",
      t: "Slovak, case-insensitive",
      l: "utf16_slovak_ci"
  }, {
      v: "utf16_slovenian_ci",
      t: "Slovenian, case-insensitive",
      l: "utf16_slovenian_ci"
  }, {
      v: "utf16_spanish2_ci",
      t: "Spanish (traditional), case-insensitive",
      l: "utf16_spanish2_ci"
  }, {
      v: "utf16_spanish_ci",
      t: "Spanish (modern), case-insensitive",
      l: "utf16_spanish_ci"
  }, {
      v: "utf16_swedish_ci",
      t: "Swedish, case-insensitive",
      l: "utf16_swedish_ci"
  }, {
      v: "utf16_turkish_ci",
      t: "Turkish, case-insensitive",
      l: "utf16_turkish_ci"
  }, {
      v: "utf16_unicode_520_ci",
      t: "Unicode (UCA 5.2.0), case-insensitive",
      l: "utf16_unicode_520_ci"
  }, {
      v: "utf16_unicode_ci",
      t: "Unicode, case-insensitive",
      l: "utf16_unicode_ci"
  }, {
      v: "utf16_vietnamese_ci",
      t: "Vietnamese, case-insensitive",
      l: "utf16_vietnamese_ci"
  }]
}, {
  l: "utf16le",
  options: [{
      v: "utf16le_bin",
      t: "Unicode, binary",
      l: "utf16le_bin"
  }, {
      v: "utf16le_general_ci",
      t: "Unicode, case-insensitive",
      l: "utf16le_general_ci"
  }]
}, {
  l: "utf32",
  options: [{
      v: "utf32_bin",
      t: "Unicode, binary",
      l: "utf32_bin"
  }, {
      v: "utf32_croatian_ci",
      t: "Croatian, case-insensitive",
      l: "utf32_croatian_ci"
  }, {
      v: "utf32_czech_ci",
      t: "Czech, case-insensitive",
      l: "utf32_czech_ci"
  }, {
      v: "utf32_danish_ci",
      t: "Danish, case-insensitive",
      l: "utf32_danish_ci"
  }, {
      v: "utf32_esperanto_ci",
      t: "Esperanto, case-insensitive",
      l: "utf32_esperanto_ci"
  }, {
      v: "utf32_estonian_ci",
      t: "Estonian, case-insensitive",
      l: "utf32_estonian_ci"
  }, {
      v: "utf32_general_ci",
      t: "Unicode, case-insensitive",
      l: "utf32_general_ci"
  }, {
      v: "utf32_german2_ci",
      t: "German (phone book order), case-insensitive",
      l: "utf32_german2_ci"
  }, {
      v: "utf32_hungarian_ci",
      t: "Hungarian, case-insensitive",
      l: "utf32_hungarian_ci"
  }, {
      v: "utf32_icelandic_ci",
      t: "Icelandic, case-insensitive",
      l: "utf32_icelandic_ci"
  }, {
      v: "utf32_latvian_ci",
      t: "Latvian, case-insensitive",
      l: "utf32_latvian_ci"
  }, {
      v: "utf32_lithuanian_ci",
      t: "Lithuanian, case-insensitive",
      l: "utf32_lithuanian_ci"
  }, {
      v: "utf32_persian_ci",
      t: "Persian, case-insensitive",
      l: "utf32_persian_ci"
  }, {
      v: "utf32_polish_ci",
      t: "Polish, case-insensitive",
      l: "utf32_polish_ci"
  }, {
      v: "utf32_roman_ci",
      t: "West European, case-insensitive",
      l: "utf32_roman_ci"
  }, {
      v: "utf32_romanian_ci",
      t: "Romanian, case-insensitive",
      l: "utf32_romanian_ci"
  }, {
      v: "utf32_sinhala_ci",
      t: "Sinhalese, case-insensitive",
      l: "utf32_sinhala_ci"
  }, {
      v: "utf32_slovak_ci",
      t: "Slovak, case-insensitive",
      l: "utf32_slovak_ci"
  }, {
      v: "utf32_slovenian_ci",
      t: "Slovenian, case-insensitive",
      l: "utf32_slovenian_ci"
  }, {
      v: "utf32_spanish2_ci",
      t: "Spanish (traditional), case-insensitive",
      l: "utf32_spanish2_ci"
  }, {
      v: "utf32_spanish_ci",
      t: "Spanish (modern), case-insensitive",
      l: "utf32_spanish_ci"
  }, {
      v: "utf32_swedish_ci",
      t: "Swedish, case-insensitive",
      l: "utf32_swedish_ci"
  }, {
      v: "utf32_turkish_ci",
      t: "Turkish, case-insensitive",
      l: "utf32_turkish_ci"
  }, {
      v: "utf32_unicode_520_ci",
      t: "Unicode (UCA 5.2.0), case-insensitive",
      l: "utf32_unicode_520_ci"
  }, {
      v: "utf32_unicode_ci",
      t: "Unicode, case-insensitive",
      l: "utf32_unicode_ci"
  }, {
      v: "utf32_vietnamese_ci",
      t: "Vietnamese, case-insensitive",
      l: "utf32_vietnamese_ci"
  }]
}, {
  l: "utf8",
  options: [{
      v: "utf8_bin",
      t: "Unicode, binary",
      l: "utf8_bin"
  }, {
      v: "utf8_croatian_ci",
      t: "Croatian, case-insensitive",
      l: "utf8_croatian_ci"
  }, {
      v: "utf8_czech_ci",
      t: "Czech, case-insensitive",
      l: "utf8_czech_ci"
  }, {
      v: "utf8_danish_ci",
      t: "Danish, case-insensitive",
      l: "utf8_danish_ci"
  }, {
      v: "utf8_esperanto_ci",
      t: "Esperanto, case-insensitive",
      l: "utf8_esperanto_ci"
  }, {
      v: "utf8_estonian_ci",
      t: "Estonian, case-insensitive",
      l: "utf8_estonian_ci"
  }, {
      v: "utf8_general_ci",
      t: "Unicode, case-insensitive",
      l: "utf8_general_ci"
  }, {
      v: "utf8_general_mysql500_ci",
      t: "Unicode (MySQL 5.0.0), case-insensitive",
      l: "utf8_general_mysql500_ci"
  }, {
      v: "utf8_german2_ci",
      t: "German (phone book order), case-insensitive",
      l: "utf8_german2_ci"
  }, {
      v: "utf8_hungarian_ci",
      t: "Hungarian, case-insensitive",
      l: "utf8_hungarian_ci"
  }, {
      v: "utf8_icelandic_ci",
      t: "Icelandic, case-insensitive",
      l: "utf8_icelandic_ci"
  }, {
      v: "utf8_latvian_ci",
      t: "Latvian, case-insensitive",
      l: "utf8_latvian_ci"
  }, {
      v: "utf8_lithuanian_ci",
      t: "Lithuanian, case-insensitive",
      l: "utf8_lithuanian_ci"
  }, {
      v: "utf8_persian_ci",
      t: "Persian, case-insensitive",
      l: "utf8_persian_ci"
  }, {
      v: "utf8_polish_ci",
      t: "Polish, case-insensitive",
      l: "utf8_polish_ci"
  }, {
      v: "utf8_roman_ci",
      t: "West European, case-insensitive",
      l: "utf8_roman_ci"
  }, {
      v: "utf8_romanian_ci",
      t: "Romanian, case-insensitive",
      l: "utf8_romanian_ci"
  }, {
      v: "utf8_sinhala_ci",
      t: "Sinhalese, case-insensitive",
      l: "utf8_sinhala_ci"
  }, {
      v: "utf8_slovak_ci",
      t: "Slovak, case-insensitive",
      l: "utf8_slovak_ci"
  }, {
      v: "utf8_slovenian_ci",
      t: "Slovenian, case-insensitive",
      l: "utf8_slovenian_ci"
  }, {
      v: "utf8_spanish2_ci",
      t: "Spanish (traditional), case-insensitive",
      l: "utf8_spanish2_ci"
  }, {
      v: "utf8_spanish_ci",
      t: "Spanish (modern), case-insensitive",
      l: "utf8_spanish_ci"
  }, {
      v: "utf8_swedish_ci",
      t: "Swedish, case-insensitive",
      l: "utf8_swedish_ci"
  }, {
      v: "utf8_turkish_ci",
      t: "Turkish, case-insensitive",
      l: "utf8_turkish_ci"
  }, {
      v: "utf8_unicode_520_ci",
      t: "Unicode (UCA 5.2.0), case-insensitive",
      l: "utf8_unicode_520_ci"
  }, {
      v: "utf8_unicode_ci",
      t: "Unicode, case-insensitive",
      l: "utf8_unicode_ci"
  }, {
      v: "utf8_vietnamese_ci",
      t: "Vietnamese, case-insensitive",
      l: "utf8_vietnamese_ci"
  }]
}, {
  l: "utf8mb4",
  options: [{
      v: "utf8mb4_bin",
      t: "Unicode (UCA 4.0.0), binary",
      l: "utf8mb4_bin"
  }, {
      v: "utf8mb4_croatian_ci",
      t: "Croatian (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_croatian_ci"
  }, {
      v: "utf8mb4_czech_ci",
      t: "Czech (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_czech_ci"
  }, {
      v: "utf8mb4_danish_ci",
      t: "Danish (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_danish_ci"
  }, {
      v: "utf8mb4_esperanto_ci",
      t: "Esperanto (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_esperanto_ci"
  }, {
      v: "utf8mb4_estonian_ci",
      t: "Estonian (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_estonian_ci"
  }, {
      v: "utf8mb4_general_ci",
      t: "Unicode (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_general_ci"
  }, {
      v: "utf8mb4_german2_ci",
      t: "German (phone book order) (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_german2_ci"
  }, {
      v: "utf8mb4_hungarian_ci",
      t: "Hungarian (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_hungarian_ci"
  }, {
      v: "utf8mb4_icelandic_ci",
      t: "Icelandic (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_icelandic_ci"
  }, {
      v: "utf8mb4_latvian_ci",
      t: "Latvian (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_latvian_ci"
  }, {
      v: "utf8mb4_lithuanian_ci",
      t: "Lithuanian (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_lithuanian_ci"
  }, {
      v: "utf8mb4_persian_ci",
      t: "Persian (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_persian_ci"
  }, {
      v: "utf8mb4_polish_ci",
      t: "Polish (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_polish_ci"
  }, {
      v: "utf8mb4_roman_ci",
      t: "West European (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_roman_ci"
  }, {
      v: "utf8mb4_romanian_ci",
      t: "Romanian (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_romanian_ci"
  }, {
      v: "utf8mb4_sinhala_ci",
      t: "Sinhalese (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_sinhala_ci"
  }, {
      v: "utf8mb4_slovak_ci",
      t: "Slovak (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_slovak_ci"
  }, {
      v: "utf8mb4_slovenian_ci",
      t: "Slovenian (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_slovenian_ci"
  }, {
      v: "utf8mb4_spanish2_ci",
      t: "Spanish (traditional) (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_spanish2_ci"
  }, {
      v: "utf8mb4_spanish_ci",
      t: "Spanish (modern) (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_spanish_ci"
  }, {
      v: "utf8mb4_swedish_ci",
      t: "Swedish (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_swedish_ci"
  }, {
      v: "utf8mb4_turkish_ci",
      t: "Turkish (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_turkish_ci"
  }, {
      v: "utf8mb4_unicode_520_ci",
      t: "Unicode (UCA 5.2.0), case-insensitive",
      l: "utf8mb4_unicode_520_ci"
  }, {
      v: "utf8mb4_unicode_ci",
      t: "Unicode (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_unicode_ci"
  }, {
      v: "utf8mb4_vietnamese_ci",
      t: "Vietnamese (UCA 4.0.0), case-insensitive",
      l: "utf8mb4_vietnamese_ci"
  }]
}];

const DEFAULTS = [
    { l: "None" }, // None
    { v: "AS DEFINED", l:"As defined:" }, 
    { v: "NULL", l:"NULL" }, 
    { v: "CURRENT_TIMESTAMP", l:"CURRENT_TIMESTAMP" }, 
];

const ATTRIBUTES = [
    {  }, 
    { v: "BINARY", l: "BINARY" }, 
    { v: "UNSIGNED", l: "UNSIGNED" }, 
    { v: "UNSIGNED ZEROFILL", l: "UNSIGNED ZEROFILL" }, 
    { v: "on update CURRENT_TIMESTAMP", l: "on update CURRENT_TIMESTAMP" }
];

const INDEXS = [
    { l: "---" }, 
    // { v: "PRIMARY", l: "PRIMARY" }, 
    { v: "UNIQUE", l: "UNIQUE" }, 
    { v: "INDEX", l: "INDEX" }, 
    { v: "FULLTEXT", l: "FULLTEXT" }, 
    { v: "SPATIAL", l: "SPATIAL" }, 
];

export {
  DATA_TYPE, 
  COLLATIONS, 
  DEFAULTS, 
  ATTRIBUTES, 
  INDEXS, 
};

/*
						<option>---</option>
						<option value="PRIMARY" title="Primary">PRIMARY</option>
						<option value="UNIQUE" title="Unique">UNIQUE</option>
						<option value="INDEX" title="Index">INDEX</option>
						<option value="FULLTEXT" title="Fulltext">FULLTEXT</option>
						<option value="SPATIAL" title="Spatial">SPATIAL</option>

						<option />
						<option value="BINARY">BINARY</option>
						<option value="UNSIGNED">UNSIGNED</option>
						<option value="UNSIGNED ZEROFILL">UNSIGNED ZEROFILL</option>
						<option value="on update CURRENT_TIMESTAMP">
							on update CURRENT_TIMESTAMP
						</option>

						<option>None</option>
						<option value="As defined">As defined</option>
						<option value="NULL">NULL</option>
						<option value="CURRENT_TIMESTAMP">CURRENT_TIMESTAMP</option>

<select>
  <option value="INT" title="A 4-byte integer, signed range is -2,147,483,648 to 2,147,483,647, unsigned range is 0 to 4,294,967,295">INT</option>
  <option value="VARCHAR" title="A variable-length (0-65,535) string, the effective maximum length is subject to the maximum row size">VARCHAR</option>
  <option value="TEXT" title="A TEXT column with a maximum length of 65,535 (2^16 - 1) characters, stored with a two-byte prefix indicating the length of the value in bytes">TEXT</option>
  <option value="DATE" title="A date, supported range is 1000-01-01 to 9999-12-31">DATE</option>
  <optgroup label="Numeric">
    <option value="TINYINT" title="A 1-byte integer, signed range is -128 to 127, unsigned range is 0 to 255">TINYINT</option>
    <option value="SMALLINT" title="A 2-byte integer, signed range is -32,768 to 32,767, unsigned range is 0 to 65,535">SMALLINT</option>
    <option value="MEDIUMINT" title="A 3-byte integer, signed range is -8,388,608 to 8,388,607, unsigned range is 0 to 16,777,215">MEDIUMINT</option>
    <option value="INT" title="A 4-byte integer, signed range is -2,147,483,648 to 2,147,483,647, unsigned range is 0 to 4,294,967,295">INT</option>
    <option value="BIGINT" title="An 8-byte integer, signed range is -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807, unsigned range is 0 to 18,446,744,073,709,551,615">BIGINT</option>
    <option disabled>-</option>
    <option value="DECIMAL" title="A fixed-point number (M, D) - the maximum number of digits (M) is 65 (default 10), the maximum number of decimals (D) is 30 (default 0)">DECIMAL</option>
    <option value="FLOAT" title="A small floating-point number, allowable values are -3.402823466E+38 to -1.175494351E-38, 0, and 1.175494351E-38 to 3.402823466E+38">FLOAT</option>
    <option value="DOUBLE" title="A double-precision floating-point number, allowable values are -1.7976931348623157E+308 to -2.2250738585072014E-308, 0, and 2.2250738585072014E-308 to 1.7976931348623157E+308">DOUBLE</option>
    <option value="REAL" title="Synonym for DOUBLE (exception: in REAL_AS_FLOAT SQL mode it is a synonym for FLOAT)">REAL</option>
    <option disabled>-</option>
    <option value="BIT" title="A bit-field type (M), storing M of bits per value (default is 1, maximum is 64)">BIT</option>
    <option value="BOOLEAN" title="A synonym for TINYINT(1), a value of zero is considered false, nonzero values are considered true">BOOLEAN</option>
    <option value="SERIAL" title="An alias for BIGINT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE">SERIAL</option>
  </optgroup>
  <optgroup label="Date and time">
    <option value="DATE" title="A date, supported range is 1000-01-01 to 9999-12-31">DATE</option>
    <option value="DATETIME" title="A date and time combination, supported range is 1000-01-01 00:00:00 to 9999-12-31 23:59:59">DATETIME</option>
    <option value="TIMESTAMP" title="A timestamp, range is 1970-01-01 00:00:01 UTC to 2038-01-09 03:14:07 UTC, stored as the number of seconds since the epoch (1970-01-01 00:00:00 UTC)">TIMESTAMP</option>
    <option value="TIME" title="A time, range is -838:59:59 to 838:59:59">TIME</option>
    <option value="YEAR" title="A year in four-digit (4, default) or two-digit (2) format, the allowable values are 70 (1970) to 69 (2069) or 1901 to 2155 and 0000">YEAR</option>
  </optgroup>
  <optgroup label="String">
    <option value="CHAR" title="A fixed-length (0-255, default 1) string that is always right-padded with spaces to the specified length when stored">CHAR</option>
    <option value="VARCHAR" title="A variable-length (0-65,535) string, the effective maximum length is subject to the maximum row size">VARCHAR</option>
    <option disabled>-</option>
    <option value="TINYTEXT" title="A TEXT column with a maximum length of 255 (2^8 - 1) characters, stored with a one-byte prefix indicating the length of the value in bytes">TINYTEXT</option>
    <option value="TEXT" title="A TEXT column with a maximum length of 65,535 (2^16 - 1) characters, stored with a two-byte prefix indicating the length of the value in bytes">TEXT</option>
    <option value="MEDIUMTEXT" title="A TEXT column with a maximum length of 16,777,215 (2^24 - 1) characters, stored with a three-byte prefix indicating the length of the value in bytes">MEDIUMTEXT</option>
    <option value="LONGTEXT" title="A TEXT column with a maximum length of 4,294,967,295 or 4GiB (2^32 - 1) characters, stored with a four-byte prefix indicating the length of the value in bytes">LONGTEXT</option>
    <option disabled>-</option>
    <option value="BINARY" title="Similar to the CHAR type, but stores binary byte strings rather than non-binary character strings">BINARY</option>
    <option value="VARBINARY" title="Similar to the VARCHAR type, but stores binary byte strings rather than non-binary character strings">VARBINARY</option>
    <option disabled>-</option>
    <option value="TINYBLOB" title="A BLOB column with a maximum length of 255 (2^8 - 1) bytes, stored with a one-byte prefix indicating the length of the value">TINYBLOB</option>
    <option value="BLOB" title="A BLOB column with a maximum length of 65,535 (2^16 - 1) bytes, stored with a two-byte prefix indicating the length of the value">BLOB</option>
    <option value="MEDIUMBLOB" title="A BLOB column with a maximum length of 16,777,215 (2^24 - 1) bytes, stored with a three-byte prefix indicating the length of the value">MEDIUMBLOB</option>
    <option value="LONGBLOB" title="A BLOB column with a maximum length of 4,294,967,295 or 4GiB (2^32 - 1) bytes, stored with a four-byte prefix indicating the length of the value">LONGBLOB</option>
    <option disabled>-</option>
    <option value="ENUM" title="An enumeration, chosen from the list of up to 65,535 values or the special '' error value">ENUM</option>
    <option value="SET" title="A single value chosen from a set of up to 64 members">SET</option>
  </optgroup>
  <optgroup label="Spatial">
    <option value="GEOMETRY" title="A type that can store a geometry of any type">GEOMETRY</option>
    <option value="POINT" title="A point in 2-dimensional space">POINT</option>
    <option value="LINESTRING" title="A curve with linear interpolation between points">LINESTRING</option>
    <option value="POLYGON" title="A polygon">POLYGON</option>
    <option value="MULTIPOINT" title="A collection of points">MULTIPOINT</option>
    <option value="MULTILINESTRING" title="A collection of curves with linear interpolation between points">MULTILINESTRING</option>
    <option value="MULTIPOLYGON" title="A collection of polygons">MULTIPOLYGON</option>
    <option value="GEOMETRYCOLLECTION" title="A collection of geometry objects of any type">GEOMETRYCOLLECTION</option>
  </optgroup>
  <optgroup label="JSON">
    <option value="JSON" title="Stores and enables efficient access to data in JSON (JavaScript Object Notation) documents">JSON</option>
  </optgroup>
</select>
*/

/*
<select 
    id={"collation" + i1} 
    name="collation" 
    className={"custom-select custom-select-sm" + Q.formikValidClass(form, label)} 
    value={form.values.collation} 
    onChange={form.handleChange} 
>
    <option />
    <optgroup label="armscii8" title="ARMSCII-8 Armenian">
        <option value="armscii8_bin" title="Armenian, binary">armscii8_bin</option>
        <option value="armscii8_general_ci" title="Armenian, case-insensitive">armscii8_general_ci</option>
    </optgroup>
    <optgroup label="ascii" title="US ASCII">
        <option value="ascii_bin" title="West European, binary">ascii_bin</option>
        <option value="ascii_general_ci" title="West European, case-insensitive">ascii_general_ci</option>
    </optgroup>
    <optgroup label="big5" title="Big5 Traditional Chinese">
        <option value="big5_bin" title="Traditional Chinese, binary">big5_bin</option>
        <option value="big5_chinese_ci" title="Traditional Chinese, case-insensitive">big5_chinese_ci</option>
    </optgroup>
    <optgroup label="binary" title="Binary pseudo charset">
        <option value="binary" title="Binary">binary</option>
    </optgroup>
    <optgroup label="cp1250" title="Windows Central European">
        <option value="cp1250_bin" title="Central European, binary">cp1250_bin</option>
        <option value="cp1250_croatian_ci" title="Croatian, case-insensitive">cp1250_croatian_ci</option>
        <option value="cp1250_czech_cs" title="Czech, case-sensitive">cp1250_czech_cs</option>
        <option value="cp1250_general_ci" title="Central European, case-insensitive">cp1250_general_ci</option>
        <option value="cp1250_polish_ci" title="Polish, case-insensitive">cp1250_polish_ci</option>
    </optgroup>
    <optgroup label="cp1251" title="Windows Cyrillic">
        <option value="cp1251_bin" title="Cyrillic, binary">cp1251_bin</option>
        <option value="cp1251_bulgarian_ci" title="Bulgarian, case-insensitive">cp1251_bulgarian_ci</option>
        <option value="cp1251_general_ci" title="Cyrillic, case-insensitive">cp1251_general_ci</option>
        <option value="cp1251_general_cs" title="Cyrillic, case-sensitive">cp1251_general_cs</option>
        <option value="cp1251_ukrainian_ci" title="Ukrainian, case-insensitive">cp1251_ukrainian_ci</option>
    </optgroup>
    <optgroup label="cp1256" title="Windows Arabic">
        <option value="cp1256_bin" title="Arabic, binary">cp1256_bin</option>
        <option value="cp1256_general_ci" title="Arabic, case-insensitive">cp1256_general_ci</option>
    </optgroup>
    <optgroup label="cp1257" title="Windows Baltic">
        <option value="cp1257_bin" title="Baltic, binary">cp1257_bin</option>
        <option value="cp1257_general_ci" title="Baltic, case-insensitive">cp1257_general_ci</option>
        <option value="cp1257_lithuanian_ci" title="Lithuanian, case-insensitive">cp1257_lithuanian_ci</option>
    </optgroup>
    <optgroup label="cp850" title="DOS West European">
        <option value="cp850_bin" title="West European, binary">cp850_bin</option>
        <option value="cp850_general_ci" title="West European, case-insensitive">cp850_general_ci</option>
    </optgroup>
    <optgroup label="cp852" title="DOS Central European">
        <option value="cp852_bin" title="Central European, binary">cp852_bin</option>
        <option value="cp852_general_ci" title="Central European, case-insensitive">cp852_general_ci</option>
    </optgroup>
    <optgroup label="cp866" title="DOS Russian">
        <option value="cp866_bin" title="Russian, binary">cp866_bin</option>
        <option value="cp866_general_ci" title="Russian, case-insensitive">cp866_general_ci</option>
    </optgroup>
    <optgroup label="cp932" title="SJIS for Windows Japanese">
        <option value="cp932_bin" title="Japanese, binary">cp932_bin</option>
        <option value="cp932_japanese_ci" title="Japanese, case-insensitive">cp932_japanese_ci</option>
    </optgroup>
    <optgroup label="dec8" title="DEC West European">
        <option value="dec8_bin" title="West European, binary">dec8_bin</option>
        <option value="dec8_swedish_ci" title="Swedish, case-insensitive">dec8_swedish_ci</option>
    </optgroup>
    <optgroup label="eucjpms" title="UJIS for Windows Japanese">
        <option value="eucjpms_bin" title="Japanese, binary">eucjpms_bin</option>
        <option value="eucjpms_japanese_ci" title="Japanese, case-insensitive">eucjpms_japanese_ci</option>
    </optgroup>
    <optgroup label="euckr" title="EUC-KR Korean">
        <option value="euckr_bin" title="Korean, binary">euckr_bin</option>
        <option value="euckr_korean_ci" title="Korean, case-insensitive">euckr_korean_ci</option>
    </optgroup>
    <optgroup label="gb18030" title="China National Standard GB18030">
        <option value="gb18030_bin" title="Chinese, binary">gb18030_bin</option>
        <option value="gb18030_chinese_ci" title="Chinese, case-insensitive">gb18030_chinese_ci</option>
        <option value="gb18030_unicode_520_ci" title="Chinese (UCA 5.2.0), case-insensitive">gb18030_unicode_520_ci</option>
    </optgroup>
    <optgroup label="gb2312" title="GB2312 Simplified Chinese">
        <option value="gb2312_bin" title="Simplified Chinese, binary">gb2312_bin</option>
        <option value="gb2312_chinese_ci" title="Simplified Chinese, case-insensitive">gb2312_chinese_ci</option>
    </optgroup>
    <optgroup label="gbk" title="GBK Simplified Chinese">
        <option value="gbk_bin" title="Simplified Chinese, binary">gbk_bin</option>
        <option value="gbk_chinese_ci" title="Simplified Chinese, case-insensitive">gbk_chinese_ci</option>
    </optgroup>
    <optgroup label="geostd8" title="GEOSTD8 Georgian">
        <option value="geostd8_bin" title="Georgian, binary">geostd8_bin</option>
        <option value="geostd8_general_ci" title="Georgian, case-insensitive">geostd8_general_ci</option>
    </optgroup>
    <optgroup label="greek" title="ISO 8859-7 Greek">
        <option value="greek_bin" title="Greek, binary">greek_bin</option>
        <option value="greek_general_ci" title="Greek, case-insensitive">greek_general_ci</option>
    </optgroup>
    <optgroup label="hebrew" title="ISO 8859-8 Hebrew">
        <option value="hebrew_bin" title="Hebrew, binary">hebrew_bin</option>
        <option value="hebrew_general_ci" title="Hebrew, case-insensitive">hebrew_general_ci</option>
    </optgroup>
    <optgroup label="hp8" title="HP West European">
        <option value="hp8_bin" title="West European, binary">hp8_bin</option>
        <option value="hp8_english_ci" title="English, case-insensitive">hp8_english_ci</option>
    </optgroup>
    <optgroup label="keybcs2" title="DOS Kamenicky Czech-Slovak">
        <option value="keybcs2_bin" title="Czech-Slovak, binary">keybcs2_bin</option>
        <option value="keybcs2_general_ci" title="Czech-Slovak, case-insensitive">keybcs2_general_ci</option>
    </optgroup>
    <optgroup label="koi8r" title="KOI8-R Relcom Russian">
        <option value="koi8r_bin" title="Russian, binary">koi8r_bin</option>
        <option value="koi8r_general_ci" title="Russian, case-insensitive">koi8r_general_ci</option>
    </optgroup>
    <optgroup label="koi8u" title="KOI8-U Ukrainian">
        <option value="koi8u_bin" title="Ukrainian, binary">koi8u_bin</option>
        <option value="koi8u_general_ci" title="Ukrainian, case-insensitive">koi8u_general_ci</option>
    </optgroup>
    <optgroup label="latin1" title="cp1252 West European">
        <option value="latin1_bin" title="West European, binary">latin1_bin</option>
        <option value="latin1_danish_ci" title="Danish, case-insensitive">latin1_danish_ci</option>
        <option value="latin1_general_ci" title="West European, case-insensitive">latin1_general_ci</option>
        <option value="latin1_general_cs" title="West European, case-sensitive">latin1_general_cs</option>
        <option value="latin1_german1_ci" title="German (dictionary order), case-insensitive">latin1_german1_ci</option>
        <option value="latin1_german2_ci" title="German (phone book order), case-insensitive">latin1_german2_ci</option>
        <option value="latin1_spanish_ci" title="Spanish (modern), case-insensitive">latin1_spanish_ci</option>
        <option value="latin1_swedish_ci" title="Swedish, case-insensitive">latin1_swedish_ci</option>
    </optgroup>
    <optgroup label="latin2" title="ISO 8859-2 Central European">
        <option value="latin2_bin" title="Central European, binary">latin2_bin</option>
        <option value="latin2_croatian_ci" title="Croatian, case-insensitive">latin2_croatian_ci</option>
        <option value="latin2_czech_cs" title="Czech, case-sensitive">latin2_czech_cs</option>
        <option value="latin2_general_ci" title="Central European, case-insensitive">latin2_general_ci</option>
        <option value="latin2_hungarian_ci" title="Hungarian, case-insensitive">latin2_hungarian_ci</option>
    </optgroup>
    <optgroup label="latin5" title="ISO 8859-9 Turkish">
        <option value="latin5_bin" title="Turkish, binary">latin5_bin</option>
        <option value="latin5_turkish_ci" title="Turkish, case-insensitive">latin5_turkish_ci</option>
    </optgroup>
    <optgroup label="latin7" title="ISO 8859-13 Baltic">
        <option value="latin7_bin" title="Baltic, binary">latin7_bin</option>
        <option value="latin7_estonian_cs" title="Estonian, case-sensitive">latin7_estonian_cs</option>
        <option value="latin7_general_ci" title="Baltic, case-insensitive">latin7_general_ci</option>
        <option value="latin7_general_cs" title="Baltic, case-sensitive">latin7_general_cs</option>
    </optgroup>
    <optgroup label="macce" title="Mac Central European">
        <option value="macce_bin" title="Central European, binary">macce_bin</option>
        <option value="macce_general_ci" title="Central European, case-insensitive">macce_general_ci</option>
    </optgroup>
    <optgroup label="macroman" title="Mac West European">
        <option value="macroman_bin" title="West European, binary">macroman_bin</option>
        <option value="macroman_general_ci" title="West European, case-insensitive">macroman_general_ci</option>
    </optgroup>
    <optgroup label="sjis" title="Shift-JIS Japanese">
        <option value="sjis_bin" title="Japanese, binary">sjis_bin</option>
        <option value="sjis_japanese_ci" title="Japanese, case-insensitive">sjis_japanese_ci</option>
    </optgroup>
    <optgroup label="swe7" title="7bit Swedish">
        <option value="swe7_bin" title="Swedish, binary">swe7_bin</option>
        <option value="swe7_swedish_ci" title="Swedish, case-insensitive">swe7_swedish_ci</option>
    </optgroup>
    <optgroup label="tis620" title="TIS620 Thai">
        <option value="tis620_bin" title="Thai, binary">tis620_bin</option>
        <option value="tis620_thai_ci" title="Thai, case-insensitive">tis620_thai_ci</option>
    </optgroup>
    <optgroup label="ucs2" title="UCS-2 Unicode">
        <option value="ucs2_bin" title="Unicode, binary">ucs2_bin</option>
        <option value="ucs2_croatian_ci" title="Croatian, case-insensitive">ucs2_croatian_ci</option>
        <option value="ucs2_czech_ci" title="Czech, case-insensitive">ucs2_czech_ci</option>
        <option value="ucs2_danish_ci" title="Danish, case-insensitive">ucs2_danish_ci</option>
        <option value="ucs2_esperanto_ci" title="Esperanto, case-insensitive">ucs2_esperanto_ci</option>
        <option value="ucs2_estonian_ci" title="Estonian, case-insensitive">ucs2_estonian_ci</option>
        <option value="ucs2_general_ci" title="Unicode, case-insensitive">ucs2_general_ci</option>
        <option value="ucs2_general_mysql500_ci" title="Unicode (MySQL 5.0.0), case-insensitive">ucs2_general_mysql500_ci</option>
        <option value="ucs2_german2_ci" title="German (phone book order), case-insensitive">ucs2_german2_ci</option>
        <option value="ucs2_hungarian_ci" title="Hungarian, case-insensitive">ucs2_hungarian_ci</option>
        <option value="ucs2_icelandic_ci" title="Icelandic, case-insensitive">ucs2_icelandic_ci</option>
        <option value="ucs2_latvian_ci" title="Latvian, case-insensitive">ucs2_latvian_ci</option>
        <option value="ucs2_lithuanian_ci" title="Lithuanian, case-insensitive">ucs2_lithuanian_ci</option>
        <option value="ucs2_persian_ci" title="Persian, case-insensitive">ucs2_persian_ci</option>
        <option value="ucs2_polish_ci" title="Polish, case-insensitive">ucs2_polish_ci</option>
        <option value="ucs2_roman_ci" title="West European, case-insensitive">ucs2_roman_ci</option>
        <option value="ucs2_romanian_ci" title="Romanian, case-insensitive">ucs2_romanian_ci</option>
        <option value="ucs2_sinhala_ci" title="Sinhalese, case-insensitive">ucs2_sinhala_ci</option>
        <option value="ucs2_slovak_ci" title="Slovak, case-insensitive">ucs2_slovak_ci</option>
        <option value="ucs2_slovenian_ci" title="Slovenian, case-insensitive">ucs2_slovenian_ci</option>
        <option value="ucs2_spanish2_ci" title="Spanish (traditional), case-insensitive">ucs2_spanish2_ci</option>
        <option value="ucs2_spanish_ci" title="Spanish (modern), case-insensitive">ucs2_spanish_ci</option>
        <option value="ucs2_swedish_ci" title="Swedish, case-insensitive">ucs2_swedish_ci</option>
        <option value="ucs2_turkish_ci" title="Turkish, case-insensitive">ucs2_turkish_ci</option>
        <option value="ucs2_unicode_520_ci" title="Unicode (UCA 5.2.0), case-insensitive">ucs2_unicode_520_ci</option>
        <option value="ucs2_unicode_ci" title="Unicode, case-insensitive">ucs2_unicode_ci</option>
        <option value="ucs2_vietnamese_ci" title="Vietnamese, case-insensitive">ucs2_vietnamese_ci</option>
    </optgroup>
    <optgroup label="ujis" title="EUC-JP Japanese">
        <option value="ujis_bin" title="Japanese, binary">ujis_bin</option>
        <option value="ujis_japanese_ci" title="Japanese, case-insensitive">ujis_japanese_ci</option>
    </optgroup>
    <optgroup label="utf16" title="UTF-16 Unicode">
        <option value="utf16_bin" title="Unicode, binary">utf16_bin</option>
        <option value="utf16_croatian_ci" title="Croatian, case-insensitive">utf16_croatian_ci</option>
        <option value="utf16_czech_ci" title="Czech, case-insensitive">utf16_czech_ci</option>
        <option value="utf16_danish_ci" title="Danish, case-insensitive">utf16_danish_ci</option>
        <option value="utf16_esperanto_ci" title="Esperanto, case-insensitive">utf16_esperanto_ci</option>
        <option value="utf16_estonian_ci" title="Estonian, case-insensitive">utf16_estonian_ci</option>
        <option value="utf16_general_ci" title="Unicode, case-insensitive">utf16_general_ci</option>
        <option value="utf16_german2_ci" title="German (phone book order), case-insensitive">utf16_german2_ci</option>
        <option value="utf16_hungarian_ci" title="Hungarian, case-insensitive">utf16_hungarian_ci</option>
        <option value="utf16_icelandic_ci" title="Icelandic, case-insensitive">utf16_icelandic_ci</option>
        <option value="utf16_latvian_ci" title="Latvian, case-insensitive">utf16_latvian_ci</option>
        <option value="utf16_lithuanian_ci" title="Lithuanian, case-insensitive">utf16_lithuanian_ci</option>
        <option value="utf16_persian_ci" title="Persian, case-insensitive">utf16_persian_ci</option>
        <option value="utf16_polish_ci" title="Polish, case-insensitive">utf16_polish_ci</option>
        <option value="utf16_roman_ci" title="West European, case-insensitive">utf16_roman_ci</option>
        <option value="utf16_romanian_ci" title="Romanian, case-insensitive">utf16_romanian_ci</option>
        <option value="utf16_sinhala_ci" title="Sinhalese, case-insensitive">utf16_sinhala_ci</option>
        <option value="utf16_slovak_ci" title="Slovak, case-insensitive">utf16_slovak_ci</option>
        <option value="utf16_slovenian_ci" title="Slovenian, case-insensitive">utf16_slovenian_ci</option>
        <option value="utf16_spanish2_ci" title="Spanish (traditional), case-insensitive">utf16_spanish2_ci</option>
        <option value="utf16_spanish_ci" title="Spanish (modern), case-insensitive">utf16_spanish_ci</option>
        <option value="utf16_swedish_ci" title="Swedish, case-insensitive">utf16_swedish_ci</option>
        <option value="utf16_turkish_ci" title="Turkish, case-insensitive">utf16_turkish_ci</option>
        <option value="utf16_unicode_520_ci" title="Unicode (UCA 5.2.0), case-insensitive">utf16_unicode_520_ci</option>
        <option value="utf16_unicode_ci" title="Unicode, case-insensitive">utf16_unicode_ci</option>
        <option value="utf16_vietnamese_ci" title="Vietnamese, case-insensitive">utf16_vietnamese_ci</option>
    </optgroup>
    <optgroup label="utf16le" title="UTF-16LE Unicode">
        <option value="utf16le_bin" title="Unicode, binary">utf16le_bin</option>
        <option value="utf16le_general_ci" title="Unicode, case-insensitive">utf16le_general_ci</option>
    </optgroup>
    <optgroup label="utf32" title="UTF-32 Unicode">
        <option value="utf32_bin" title="Unicode, binary">utf32_bin</option>
        <option value="utf32_croatian_ci" title="Croatian, case-insensitive">utf32_croatian_ci</option>
        <option value="utf32_czech_ci" title="Czech, case-insensitive">utf32_czech_ci</option>
        <option value="utf32_danish_ci" title="Danish, case-insensitive">utf32_danish_ci</option>
        <option value="utf32_esperanto_ci" title="Esperanto, case-insensitive">utf32_esperanto_ci</option>
        <option value="utf32_estonian_ci" title="Estonian, case-insensitive">utf32_estonian_ci</option>
        <option value="utf32_general_ci" title="Unicode, case-insensitive">utf32_general_ci</option>
        <option value="utf32_german2_ci" title="German (phone book order), case-insensitive">utf32_german2_ci</option>
        <option value="utf32_hungarian_ci" title="Hungarian, case-insensitive">utf32_hungarian_ci</option>
        <option value="utf32_icelandic_ci" title="Icelandic, case-insensitive">utf32_icelandic_ci</option>
        <option value="utf32_latvian_ci" title="Latvian, case-insensitive">utf32_latvian_ci</option>
        <option value="utf32_lithuanian_ci" title="Lithuanian, case-insensitive">utf32_lithuanian_ci</option>
        <option value="utf32_persian_ci" title="Persian, case-insensitive">utf32_persian_ci</option>
        <option value="utf32_polish_ci" title="Polish, case-insensitive">utf32_polish_ci</option>
        <option value="utf32_roman_ci" title="West European, case-insensitive">utf32_roman_ci</option>
        <option value="utf32_romanian_ci" title="Romanian, case-insensitive">utf32_romanian_ci</option>
        <option value="utf32_sinhala_ci" title="Sinhalese, case-insensitive">utf32_sinhala_ci</option>
        <option value="utf32_slovak_ci" title="Slovak, case-insensitive">utf32_slovak_ci</option>
        <option value="utf32_slovenian_ci" title="Slovenian, case-insensitive">utf32_slovenian_ci</option>
        <option value="utf32_spanish2_ci" title="Spanish (traditional), case-insensitive">utf32_spanish2_ci</option>
        <option value="utf32_spanish_ci" title="Spanish (modern), case-insensitive">utf32_spanish_ci</option>
        <option value="utf32_swedish_ci" title="Swedish, case-insensitive">utf32_swedish_ci</option>
        <option value="utf32_turkish_ci" title="Turkish, case-insensitive">utf32_turkish_ci</option>
        <option value="utf32_unicode_520_ci" title="Unicode (UCA 5.2.0), case-insensitive">utf32_unicode_520_ci</option>
        <option value="utf32_unicode_ci" title="Unicode, case-insensitive">utf32_unicode_ci</option>
        <option value="utf32_vietnamese_ci" title="Vietnamese, case-insensitive">utf32_vietnamese_ci</option>
    </optgroup>
    <optgroup label="utf8" title="UTF-8 Unicode">
        <option value="utf8_bin" title="Unicode, binary">utf8_bin</option>
        <option value="utf8_croatian_ci" title="Croatian, case-insensitive">utf8_croatian_ci</option>
        <option value="utf8_czech_ci" title="Czech, case-insensitive">utf8_czech_ci</option>
        <option value="utf8_danish_ci" title="Danish, case-insensitive">utf8_danish_ci</option>
        <option value="utf8_esperanto_ci" title="Esperanto, case-insensitive">utf8_esperanto_ci</option>
        <option value="utf8_estonian_ci" title="Estonian, case-insensitive">utf8_estonian_ci</option>
        <option value="utf8_general_ci" title="Unicode, case-insensitive">utf8_general_ci</option>
        <option value="utf8_general_mysql500_ci" title="Unicode (MySQL 5.0.0), case-insensitive">utf8_general_mysql500_ci</option>
        <option value="utf8_german2_ci" title="German (phone book order), case-insensitive">utf8_german2_ci</option>
        <option value="utf8_hungarian_ci" title="Hungarian, case-insensitive">utf8_hungarian_ci</option>
        <option value="utf8_icelandic_ci" title="Icelandic, case-insensitive">utf8_icelandic_ci</option>
        <option value="utf8_latvian_ci" title="Latvian, case-insensitive">utf8_latvian_ci</option>
        <option value="utf8_lithuanian_ci" title="Lithuanian, case-insensitive">utf8_lithuanian_ci</option>
        <option value="utf8_persian_ci" title="Persian, case-insensitive">utf8_persian_ci</option>
        <option value="utf8_polish_ci" title="Polish, case-insensitive">utf8_polish_ci</option>
        <option value="utf8_roman_ci" title="West European, case-insensitive">utf8_roman_ci</option>
        <option value="utf8_romanian_ci" title="Romanian, case-insensitive">utf8_romanian_ci</option>
        <option value="utf8_sinhala_ci" title="Sinhalese, case-insensitive">utf8_sinhala_ci</option>
        <option value="utf8_slovak_ci" title="Slovak, case-insensitive">utf8_slovak_ci</option>
        <option value="utf8_slovenian_ci" title="Slovenian, case-insensitive">utf8_slovenian_ci</option>
        <option value="utf8_spanish2_ci" title="Spanish (traditional), case-insensitive">utf8_spanish2_ci</option>
        <option value="utf8_spanish_ci" title="Spanish (modern), case-insensitive">utf8_spanish_ci</option>
        <option value="utf8_swedish_ci" title="Swedish, case-insensitive">utf8_swedish_ci</option>
        <option value="utf8_turkish_ci" title="Turkish, case-insensitive">utf8_turkish_ci</option>
        <option value="utf8_unicode_520_ci" title="Unicode (UCA 5.2.0), case-insensitive">utf8_unicode_520_ci</option>
        <option value="utf8_unicode_ci" title="Unicode, case-insensitive">utf8_unicode_ci</option>
        <option value="utf8_vietnamese_ci" title="Vietnamese, case-insensitive">utf8_vietnamese_ci</option>
    </optgroup>
    <optgroup label="utf8mb4" title="UTF-8 Unicode">
        <option value="utf8mb4_bin" title="Unicode (UCA 4.0.0), binary">utf8mb4_bin</option>
        <option value="utf8mb4_croatian_ci" title="Croatian (UCA 4.0.0), case-insensitive">utf8mb4_croatian_ci</option>
        <option value="utf8mb4_czech_ci" title="Czech (UCA 4.0.0), case-insensitive">utf8mb4_czech_ci</option>
        <option value="utf8mb4_danish_ci" title="Danish (UCA 4.0.0), case-insensitive">utf8mb4_danish_ci</option>
        <option value="utf8mb4_esperanto_ci" title="Esperanto (UCA 4.0.0), case-insensitive">utf8mb4_esperanto_ci</option>
        <option value="utf8mb4_estonian_ci" title="Estonian (UCA 4.0.0), case-insensitive">utf8mb4_estonian_ci</option>
        <option value="utf8mb4_general_ci" title="Unicode (UCA 4.0.0), case-insensitive">utf8mb4_general_ci</option>
        <option value="utf8mb4_german2_ci" title="German (phone book order) (UCA 4.0.0), case-insensitive">utf8mb4_german2_ci</option>
        <option value="utf8mb4_hungarian_ci" title="Hungarian (UCA 4.0.0), case-insensitive">utf8mb4_hungarian_ci</option>
        <option value="utf8mb4_icelandic_ci" title="Icelandic (UCA 4.0.0), case-insensitive">utf8mb4_icelandic_ci</option>
        <option value="utf8mb4_latvian_ci" title="Latvian (UCA 4.0.0), case-insensitive">utf8mb4_latvian_ci</option>
        <option value="utf8mb4_lithuanian_ci" title="Lithuanian (UCA 4.0.0), case-insensitive">utf8mb4_lithuanian_ci</option>
        <option value="utf8mb4_persian_ci" title="Persian (UCA 4.0.0), case-insensitive">utf8mb4_persian_ci</option>
        <option value="utf8mb4_polish_ci" title="Polish (UCA 4.0.0), case-insensitive">utf8mb4_polish_ci</option>
        <option value="utf8mb4_roman_ci" title="West European (UCA 4.0.0), case-insensitive">utf8mb4_roman_ci</option>
        <option value="utf8mb4_romanian_ci" title="Romanian (UCA 4.0.0), case-insensitive">utf8mb4_romanian_ci</option>
        <option value="utf8mb4_sinhala_ci" title="Sinhalese (UCA 4.0.0), case-insensitive">utf8mb4_sinhala_ci</option>
        <option value="utf8mb4_slovak_ci" title="Slovak (UCA 4.0.0), case-insensitive">utf8mb4_slovak_ci</option>
        <option value="utf8mb4_slovenian_ci" title="Slovenian (UCA 4.0.0), case-insensitive">utf8mb4_slovenian_ci</option>
        <option value="utf8mb4_spanish2_ci" title="Spanish (traditional) (UCA 4.0.0), case-insensitive">utf8mb4_spanish2_ci</option>
        <option value="utf8mb4_spanish_ci" title="Spanish (modern) (UCA 4.0.0), case-insensitive">utf8mb4_spanish_ci</option>
        <option value="utf8mb4_swedish_ci" title="Swedish (UCA 4.0.0), case-insensitive">utf8mb4_swedish_ci</option>
        <option value="utf8mb4_turkish_ci" title="Turkish (UCA 4.0.0), case-insensitive">utf8mb4_turkish_ci</option>
        <option value="utf8mb4_unicode_520_ci" title="Unicode (UCA 5.2.0), case-insensitive">utf8mb4_unicode_520_ci</option>
        <option value="utf8mb4_unicode_ci" title="Unicode (UCA 4.0.0), case-insensitive">utf8mb4_unicode_ci</option>
        <option value="utf8mb4_vietnamese_ci" title="Vietnamese (UCA 4.0.0), case-insensitive">utf8mb4_vietnamese_ci</option>
    </optgroup>
</select>
*/