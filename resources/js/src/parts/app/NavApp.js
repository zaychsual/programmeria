import React, { useContext } from 'react';// , { useState, useContext, useEffect }
// import { useHistory } from 'react-router-dom';// , Route
// import Dropdown from 'react-bootstrap/Dropdown';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';

// import { AuthContext } from '../../context/AuthContext';
import Aroute from '../../components/q-ui-react/Aroute';
// import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
import Img from '../../components/q-ui-react/Img';
// import Cookie from '../../utils/cookie';
import { APP_NAME } from '../../data/appData';// , APP_LANG
import { AppConfigContext } from '../../context/AppContext';

// const APP_MENUS = [
// 	{txt:"Home", to:"/", icon:"home", exact:true, strict:true},
// 	{txt:"About", to:"/about"},
// 	{txt:"Projects", to:"/projects"},
// 	// {txt:"Laravel", to:"/laravel"}, 
// ];

const THEMES = [
	{ bg: "main", hex: "#e3f2fd" },
	{ bg: "light", hex: "#f8f9fa" },
	{ bg: "dark", hex: "#343a40" }
];

export default function NavApp(){
	const { config, setAppConfig } = useContext(AppConfigContext);

	return (
		<Navbar 
			expand="lg" 
			sticky="top" 
			bg={config.theme?.bg} 
			variant={config.theme?.bg ?? "none"} // text
			// className={
			// 	Q.Cx("py-1 shadow-sm border-bottom", {
			// 		["bg-" + config.theme.bg]: config.theme.bg, 
			// 		// ["navbar-" + config.theme?.text]: config.theme?.text, 
			// 	})
			// } 
			id="navMain" 
			className="py-1 shadow-sm border-bottom" 
		>
			<Aroute exact to="/" className="navbar-brand">
				<Img alt={APP_NAME} src="/logo/logo-36x36.png" w="28" h="28" />
			</Aroute>
			<Navbar.Toggle aria-controls="navbarScroll" />
			<Navbar.Collapse id="navbarScroll">
				<Nav
					className="mr-auto my-2 my-lg-0 link-sm"
					// style={{ maxHeight: '100px' }} 
					navbarScroll
				>
					<Aroute exact nav to="/">Home</Aroute>
					<Aroute nav to="/about">About</Aroute>

					<NavDropdown title="Admin" id="navbarScrollingDropdown">
						<a href="/admin" className="dropdown-item">Admin</a>
						{/* <NavDropdown.Divider />
						<NavDropdown.Item href="#action5">Something else here</NavDropdown.Item> */}
					</NavDropdown>
				</Nav>

				{/* <Form className="d-flex">
					<FormControl
						type="search"
						placeholder="Search"
						className="mr-2"
						aria-label="Search"
					/>
					<Button variant="outline-success">Search</Button>
				</Form> */}

				<div className="form-inline ml-1-next">
					<select className="custom-select custom-select-sm text-uppercase"
						value={config?.lang} // config.theme?.lang
						onChange={e => {
							setAppConfig({ ...config, lang: e.target.value });
						}} 
					>
						{[
							"en","id"
						].map(v => 
							<option key={v} value={v}>{v}</option>
						)}
					</select>

					<select className="custom-select custom-select-sm text-capitalize" 
						value={config.theme.bg} 
						onChange={e => {
							const bg = e.target.value;
							const hex = THEMES.find(f => f.bg === bg)?.hex || "#ffffff";
							setAppConfig({ ...config, theme: { ...config.theme, bg, hex } })
						}} 
					>
						{THEMES.map(v => 
							<option key={v.bg} value={v.bg}>{v.bg}</option>
						)}
					</select>

					{!config.user && 
						<div className={"mb-p-lp-2 btn-group btn-group-sm"}>
							<Aroute to="/login" btn="light">Login</Aroute>
							<Aroute to="/register" btn="primary">Register</Aroute>
						</div>
					}

					<form className="input-group input-group-sm">
						<input type="search" className="form-control" placeholder="Search" />
						<div className="input-group-append">
							<Btn kind="light" className="qi qi-search" />
						</div>
					</form>
				</div>
			</Navbar.Collapse>
		</Navbar>
	);
}

/*
		<nav className="navbar navbar-expand-lg navbar-light bg-main py-1 shadow-sm border-bottom h-48px sticky-top" id="navApp">
			<a href="/" className="navbar-brand">
				<img width="28" height="28" loading="lazy" alt={APP_NAME} src="/icon/android-icon-36x36.png" />
			</a>

			<Btn onClick={onClickSearch} outline className="d-lg-none ml-auto q-fw qi qi-search" />

			
		</nav>
*/
