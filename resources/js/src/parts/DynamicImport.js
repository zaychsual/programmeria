import React, { useEffect } from 'react';// { useState, useEffect, Suspense }
import loadable from '@loadable/component';
// import { Switch, Route } from 'react-router-dom';// useParams
// import Btn from '../../components/q-ui-bootstrap/Btn';

// const OtherComponent = loadable(() => import('./OtherComponent'));
const AsyncPage = loadable(props => import(`./${props.app}`)); // `./${props.page}`

export default function DynamicImport({ 
	// pathApp, 
	app, 
	// ...etc 
}){
  // const [data, setData] = useState();
	
	useEffect(() => {
		console.log('%cuseEffect in DynamicImport','color:yellow;');
	}, []);

// <PageLoader bottom left className="w-100" />
	return (
		<Suspense fallback={<div>Loading...</div>}>
			<AsyncPage app={app} />
		</Suspense>
	);
}

/*
		<Suspense fallback={<div>Loading...</div>}>
			<Switch>
				<Route {...etc}>
					<AsyncPage app={app} />
				</Route>
			</Switch>
		</Suspense>

 + "/:id"
<React.Fragment></React.Fragment>
*/
