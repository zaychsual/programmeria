import React, { useEffect, useRef, useState } from 'react';
// import { useMediaQuery } from 'react-responsive';
// import { useLocation } from 'react-router-dom';// , useHistory, useRouteMatch
import Dropdown from 'react-bootstrap/Dropdown';

import Placeholder from '../../components/q-ui-react/Placeholder';
import Btn from '../../components/q-ui-react/Btn';
import InputGroup from '../../components/q-ui-react/InputGroup';// OPTION
import Input from '../../components/q-ui-react/Input';
import Aroute from '../../components/q-ui-react/Aroute';
import Flex from '../../components/q-ui-react/Flex';
import ModalQ from '../../components/q-ui-react/ModalQ';

export default function AsideAdmin({
  load, 
  isMobile, 
  basename = "", 
  headText, 
  asideMin = false, 
  menus = [], 
  onToggleAside, 
}){
  // const isMobile = useMediaQuery({ query: '(max-width: 767px)' });// (max-width: 1224px)
  const asideMenuCol = useRef(null);
  const [ddMenuOpen, setDdMenuOpen] = useState(false);
  const [findMenus, setFindMenus] = useState([]);
  const [findVal, setFindVal] = useState("");

  const scrollMenu = (dd, behavior = "smooth") => {
    setTimeout(() => {
      let rect = dd.getBoundingClientRect();
      asideMenuCol.current.scrollBy({
        top: rect.y - rect.height, 
        left: 0,
        behavior
      });
    }, 1);
  }

	useEffect(() => {
    // console.log('%cuseEffect in AsideMain location: ','color:yellow;');
    const path = window.location.pathname;

    // console.log('path: ', path);
    // console.log('basename: ', basename);
    // console.log('IS ROOT: ', (path === basename || (path.endsWith("/") || path === basename + "/"))); // path !== basename || path !== basename + "/"

    if(!(path === basename || (path.endsWith("/") || path === basename + "/"))){
      const findActive = menus.findIndex(v => v.menus && v.menus.find(v2 => (basename + v2.to === path || path.includes(basename + v2.to) || path.includes(basename + v2.href) || path.includes(v2.href)) || v2.href === path));
      const aNative = Q.domQ(`a[href="${path}"].menu-item.native`);
      // console.log('findActive: ', findActive);
      // console.log('aNative: ', aNative);

      if(findActive > -1 && !aNative){
        // console.log('is findActive: ', findActive);
        setDdMenuOpen(findActive);
        setTimeout(() => {
          const dd = Q.domQ(".menu-dd.show");
          // console.log('dd: ', dd);
          if(dd){
            // const ahref = Q.domQ(`a[href="${path}"].menu-item.native`, dd);// Q.domQall("a.menu-item.native", dd);
            // console.log('aNative: ', aNative);
            if(aNative){
              Q.setClass(aNative, "active");
            }else{
              const ahref = Q.domQ(`a[href="${path}"].menu-item.native`, dd);
              if(ahref){
                Q.setClass(ahref, "active");
              }
            }
            // aNative.forEach(a => {
            //   console.log('with a native: ', a);
            //   if(a.href === window.location.href){
            //     // console.log('with a native if: ', a);
            //     Q.setClass(a, "active");
            //   }
            // });

            scrollMenu(dd, "auto");
          }
        }, 9);
      }
      else if(aNative){
        Q.setClass(aNative, "active");
      }
      // else{
      //   const menuCol = Q.domQ(`.aside-menu-col a[href="${path}"].native`); // Q.domQ('.aside-menu-col');
      //   console.log('No findActive: ', findActive);
      //   console.log('No findActive menuCol: ', menuCol);
      //   // if(menuCol){
      //   //   Q.setClass(menuCol, "active");
      //   //   // console.log('menuCol OK: ');
      //   // }
      // }
    }
  }, []); // menus
  
  // useEffect(() => {
  //   window.addEventListener('keyup', (e) => {
  //     // console.log(ddMenuOpen);
  //     if(e.key === "Escape"){// ddMenuOpen && 
  //       console.log(e.key);
  //       e.preventDefault();
  //       e.stopPropagation();
  //       setDdMenuOpen(ddMenuOpen);
  //     }
  //   })
  // }, []);

  const onScrollAsideMenuCol = (e) => {
    e.stopPropagation();
    if(Q.hasClass(e.target, "aside-menu-col") && asideMin && !isMobile && ddMenuOpen){
      let dd = document.activeElement;
      if(Q.getAttr(dd, 'aria-expanded') === 'true'){
        dd.click();
        dd.blur();
      }
    }
  }

  const onToggleMenuDropdown = (open, e, metadata, i) => {
    // setDdMenuOpen(open ? i : false);

    if(!asideMin && open && e){
      let dd = e.target.parentElement;
      if(dd) scrollMenu(dd);
    }
    // Prevent Esc close
    if(!open && (e.type === "keydown" || e.type === "keyup") && metadata.source === "rootClose"){
      return;
    }
    setDdMenuOpen(open ? i : false);
  }

  const onClickDropMenuItem = e => {
    if(asideMin && !isMobile){
      let dd = Q.domQ('[aria-expanded]', e.target.closest('.dropright'));
      if(dd) dd.click();
    }
    if(isMobile) onToggleAside();
  }

// DEV Use Debounce / Throttled
  const onFind = e => {
    let val = e.target.value;
    let vlow = val.toLowerCase();

    setFindVal(val);

    let dataFind = [];
    if(val.length > 0){
      menus.forEach(f => {
        if(f.label.toLowerCase().includes(vlow)){
          if(f.menus){
            let child = f.menus.filter(f2 => f2.label.toLowerCase().includes(vlow));
            dataFind = [ ...dataFind, ...child ];
          }
          dataFind.push(f);
        }
      });
    }else{
      dataFind = menus;
    }
    
    // console.log('dataFind: ', dataFind);
    setFindMenus(dataFind);// val.length > 0 ? dataFind : menus
  };

  const onClearFind = () => {
    setFindVal("");
    setFindMenus([]);
  }

  const renderSearchMenu = () => {
    const inDrawer = asideMin && !isMobile;
    return (
      <InputGroup 
        size={!isMobile ? "sm" : null} 
        className={"p-2 bg-strip" + (!isMobile ? " border-bottom" : "")} 
        append={
          <Btn As="div" kind="light" tabIndex="0" 
            // "qi qi-search" + (inDrawer ? " rounded":"")
            className={
              Q.Cx("q-fw qi qi-" + (findVal.length > 0 && !inDrawer ? "close xx":"search"), {
                "rounded": inDrawer
              })
            } 
            onClick={inDrawer ? onToggleAside : onClearFind}
          />
        }
      >
        <Input onChange={onFind} value={findVal} placeholder="Search" hidden={inDrawer} disabled={!load || inDrawer} />
      </InputGroup>
    );
  }

  const renderMenu = () => {
    const isMin = asideMin && !isMobile;
    const isFinds = findMenus.length > 0;
    const menusData = isFinds ? findMenus : menus;
    return (
      <div ref={asideMenuCol} 
        className={
          Q.Cx("nav flex-column flex-nowrap py-1 border-top-next ovyscroll q-scroll aside-menu-col", {
            "scroll-hide": asideMin,
            "position-absolute position-full": isMobile
          })
        }
        onScroll={onScrollAsideMenuCol}
      >
        {(!isFinds && findVal.length > 0) && 
          <Flex As="h5" dir="column" justify="center" align="center" className="position-absolute inset-0 qi qi-ban qi-2x ired hide-next">NOT FOUND</Flex>
        }

        {load ? 
          menusData.map((v, i) => {
            if(v.menus){
              return (
                <Dropdown key={i} 
                  show={ddMenuOpen === i} 
                  drop={isMin ? "right":"down"} 
                  className="menu-dd" // qi 
                  onToggle={(open, e, metadata) => onToggleMenuDropdown(open, e, metadata, i)}
                >
                  <Dropdown.Toggle {...Q.DD_BTN} 
                    bsPrefix={"menu-item nav-link btn w-100 rounded-0 shadow-none q-mr dd-toggle-menu qia q-fw qi qi-" + v.icon}
                    title={v.label}
                  >
                    {v.label}
                  </Dropdown.Toggle>

                  <Dropdown.Menu 
                    flip={isMin} // false
                    // Prevent close with click outside, null, "mousedown"
                    rootCloseEvent={isMin ? "mousedown" : null} 
                    className={"px-2 qi dd-menu-asidemain" + (isMin ? " ovyauto q-scroll scroll-fff":"")} 
                    popperConfig={{
                      strategy: "fixed", // asideMin ? "fixed":"relative", // "relative", // fixed
                      modifiers: [{
                        name: "offset",
                        options: {
                          offset: isMin ? [0, -0.05] : [0, 0]  
                        }
                      }]
                    }}
                  >
                    {(v.menus && v.menus.length) && v.menus.map((v2, i2) => 
                      React.createElement(
                        v2.to ? Aroute : v2.href ? "a" : "button", 
                        {
                          key: i2,
                          // dropdown: Boolean(v.to), 
                          // closeDropdown: asideMin,
                          as: !asideMin && (v2.to || v2.href) ? null : "button",
                          type: v2.to || v2.href ? null : "button",
                          to: v2.to,
                          href: v2.href, // v2.icon ? "q-mr fal fa-" + v2.icon : undefined
                          className: "dropdown-item rounded text-truncate menu-item" + (v2.icon ? " q-mr q-fw qi qi-" + v2.icon : "") + (v2.href ? " native":""), 
                          title: v2.label, 
                          onClick: onClickDropMenuItem
                        }, v2.label)
                      )
                    }
                  </Dropdown.Menu>
                </Dropdown>
              );
            }

            return React.createElement(
              v.to ? Aroute : "a", 
              {
                key: i,
                to: v.to, 
                href: v.href,
                exact: v.exact, 
                strict: v.strict,
                className: "menu-item nav-link q-mr q-fw qi qi-" + v.icon + (v.href ? " native":""), 
                onClick: isMobile ? onToggleAside : undefined,
                title: v.label
            }, v.label);
          })
          : 
          <Placeholder length={5} />
        }
      </div>
    );    
  }

  if(isMobile){
    return (
      <ModalQ 
        // backdrop="static" 
        // keyboard={false} 
        // unmountOnClose={false} // OPTION
        open={asideMin} 
        size="sm" // sm | lg | xl
        toggle={onToggleAside} 
        // scrollable 
        className="asideModalApp" // mx-0 | modal-full-width 
        contentClassName="w-256px vh-100" // {modalPosition === "modal-full" ? "vh-100" : undefined} 

        // path="#modal-hash" 
        position="modal-left" // {modalPosition} 
        close={false}  
        // headClass="headClass" 
        head={
          <div className="modal-header p-0 border-0 position-relative">
            {renderSearchMenu()}
            
            <Btn onClick={onToggleAside} kind="fff" size="lg" className="h-100 qi qi-close position-absolute btnToogleAsideMain" aria-label="Close"  />
            {/* <button onClick={onToggleAside}  type="button" className="close" aria-label="Close">&times;</button> */}
          </div>
        }
        bodyClass="p-0" 
        body={renderMenu()} 
        // foot={
        // 	<Btn onClick={toggleModal}>Close</Btn>
        // }
      />
    );
  }

  return (
    <aside className="flexno border-right shadow-sm" id="asideAdmin">
      <header className={"bg-main d-flex align-items-center p-2" + (asideMin ? " justify-content-center":"")}>
        <Aroute to="/home" className="navbar-brand mr-0 py-1 d-flex align-items-center" title={headText}>
          <img width="28" height="28" 
            className="d-inline-block flexno" 
            alt="Programmeria" 
            src="/logo/logo-36x36.png" 
          />

          {(!asideMin && headText) && 
            <strong className="d-inline-block text-truncate flexno ml-2 headText">{headText}</strong>
          }
        </Aroute>

      </header>

      {renderSearchMenu()}

      <div className="asidemain-menu-wrap">
        {renderMenu()}
      </div>

      <div className="p-2 border-top asidemain-foot">
        <a href="https://programmeria.com" target="_blank" className="btn btn-sm btn-flat px-1 w-100 text-truncate">@Programmeria Admin</a>
      </div>
    </aside>
  );
}
