import React, { useState } from 'react';// , { useEffect, useState }
// import { BroadcastChannel } from 'broadcast-channel';
import { useFullscreen } from 'ahooks';
import Dropdown from 'react-bootstrap/Dropdown';
import { Dropdown as DdPrime } from 'primereact/dropdown';

import Aroute from '../../components/q-ui-react/Aroute';
import Btn from '../../components/q-ui-react/Btn';
import Flex from '../../components/q-ui-react/Flex';
import Ava from '../../components/q-ui-react/Ava';
import Img from '../../components/q-ui-react/Img';

export default function NavAdmin({
  isMobile, 
  asideMin, 
  onToggleAside, //  = () => {}
  onLogOut, 
}){
  const [isFullscreen, { toggleFull }] = useFullscreen(document.documentElement);// setFull, exitFull, 
  const [lang, setLang] = useState({ name:"Indonesia", code: "id", icon:"indonesia" });

  return (
    <nav id="navAdmin" 
      className="navbar navbar-light bg-main py-1 pl-0 shadow-sm border-bottom sticky-top" // navbar-expand-lg 
    >
      {/* <a href="/" className="navbar-brand py-1">
        <img 
          src="/icon/programmeria-36x36.png" 
          width="28" height="28" 
          className="d-inline-block" // align-top 
          alt="Programmeria" 
          loading="lazy" 
        />
      </a> */}

      <Btn As="div" 
        size="sm" 
        outline 
        className={"pl-5px rounded-circle round-left-0 mr-lg-2 qi qi-chevron-2-" + (isMobile || asideMin ? "right":"left")} 
        // className="pl-5px rounded-circle round-left-0 mr-lg-2 fal fa-chevron-double-left btnToggleAsideMain" 
        onClick={(e) => {
          let dd = Q.domQ('.dd-toggle-menu[aria-expanded="true"]');
          if(dd){
            dd.click();
            dd.blur();
          }
          document.body.classList.toggle('aside-min');
          onToggleAside(e);
        }}
      />

      {/* <div className="collapse navbar-collapse" id="navAdminCollapse">
        <div className="navbar-nav link-sm ml-1-next-lg">
          <Aroute exact strict nav to="/" className="qi qi-home" />
          <Aroute nav to="/settings">Settings</Aroute>
        </div>
      </div> */}

      <Flex className="ml-auto ml-1-next">
        <Dropdown alignRight drop="down">
          <Dropdown.Toggle as="div" bsPrefix="btn-group btn-group-sm">
            <Ava 
              round 
              // w={32} 
              // h={32} 
              wrapClass="btn btn-light bg-inherit p-0 flexno" 
              className="of-cov round-right-0" //  border border-right-0 
              alt={USER_DATA.first_name + " " + USER_DATA.last_name} 
              src={"/media/users/" + USER_DATA.avatar} // Q.baseURL +  
            />
            <Btn As="div" kind="light" className="round-left-0 text-truncate" style={{ maxWidth: 120 }}>{USER_DATA.username}</Btn>
          </Dropdown.Toggle>
          <Dropdown.Menu flip={false} className="fs90p">
            <Aroute dropdown to={"/user/detail/" + USER_DATA.id} title={USER_DATA.username} >Profile</Aroute>
            <Dropdown.Item onClick={onLogOut} {...Q.DD_BTN}>Logout</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>

        <DdPrime 
          value={lang} 
          options={[
            { name:"English", code: "en", icon:"united-kingdom" }, 
            { name:"Indonesia", code: "id", icon:"indonesia" }
          ]} 
          onChange={(e) => setLang(e.value)} 
          optionLabel="name" 
          // filter 
          // showClear 
          showFilterClear 
          resetFilterOnHide 
          filterBy="name" 
          filterPlaceholder="Search" // placeholder 
          // emptyFilterMessage="Not Found" 
          panelClassName="zi-1021 w-auto" 
          panelStyle={{
            width: 240 
          }}
          valueTemplate={(option, props) => {
            if (option) {
              return (
                <Img w={15} 
                  alt={option.name} 
                  src={"/media/svg/flags/" + option.icon + ".svg"} 
                  className="d-block border"
                />
              );
            }
            return null;
          }} 
          itemTemplate={(option) => (
            <Flex align="center">
              <Img w={18} 
                alt={option.name} 
                src={"/media/svg/flags/" + option.icon + ".svg"} 
                className="flexno d-block mr-2" 
              />
              {option.name}
            </Flex>
          )} 
        />

        {/* NOT FIX: must disabled popper js */}
        <Dropdown alignRight drop="down">
          <Dropdown.Toggle bsPrefix="tip tipBR qi qi-browser" size="sm" variant="light" aria-label="Open App" />
          <Dropdown.Menu flip={false} className="fs90p">
            <Dropdown.Item href={Q.baseURL}>Open App</Dropdown.Item>
            <Dropdown.Item href={Q.baseURL} target="_blank" rel="noopener">Open App New Tab</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>

        <Btn size="sm" kind="light" id="btnToggleFullscreenPage" 
          className={"tip tipBR qi qi-fullscreen" + (isFullscreen ? "-exit" : "")} 
          aria-label={(isFullscreen ? "Exit " : "") + "Full screen"} 
          onClick={toggleFull} 
        />

        {/* <Btn onClick={onLogOut} size="sm">Logout</Btn> */}

        {/* <InputGroup 
          size="sm"  
          className="w-auto" 
          append={
            <Btn As="div" outline className="qi qi-search" tabIndex="0" />
          }
        >
          <input type="search" // text
            className="form-control" 
            placeholder="Search" 
          />
        </InputGroup> */}
      </Flex>
    </nav>
  );
}