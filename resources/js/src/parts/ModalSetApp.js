import React, { useState, Fragment } from 'react';// { useState, useEffect }
import Modal from 'reactstrap/es/Modal';
import ModalHeader from 'reactstrap/es/ModalHeader';
// import { Modal, ModalHeader } from 'reactstrap';

import NotifApi from '../components/q-ui-react/NotifApi';
import Switch from '../components/q-ui-react/Switch';
import Btn from '../components/q-ui-react/Btn';

/** @NOTE: Set to context */
export default function ModalSetApp({
	open, 
	toggle, 
}){
	const [ignore, setIgnore] = useState(true);
	const [title, setTitle] = useState("");
	const [options, setOptions] = useState({});

	const [notif, setNotif] = useState(localStorage.getItem('notif') === "1");
	const [audio, setAudio] = useState(null);
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in ComponentName','color:yellow;');
	// }, []);
	
  const handlePermissionGranted = () => {
    console.log('Permission Granted');
    setIgnore(false);
	}
	
  const handlePermissionDenied = () => {
    console.log('Permission Denied');
    setTitle(true);
	}
	
  const handleNotSupported = () => {
    console.log('Web Notification not Supported');
    setIgnore(true);
  }

  const handleNotificationOnClick = (e, tag) => {
		// e.preventDefault(); // prevent the browser from focusing the Notification's tab
		// window.open('http://www.mozilla.org', '_blank');
	
		if(document.visibilityState === "hidden"){
			console.log(e, 'Notification clicked tag:' + tag);
			window.focus();// window.parent.focus();
		}
  }

  const handleNotificationOnError = (e, tag) => {
    console.log(e, 'Notification error tag:' + tag);
  }

  const handleNotificationOnClose = (e, tag) => {
    console.log(e, 'Notification closed tag:' + tag);
  }

  const handleNotificationOnShow = (e, tag) => {
    playSound();
    console.log(e, 'Notification shown tag:' + tag);
  }

  const playSound = () => {
		// document.getElementById('sound').play();
		if(audio){
			audio.play();
		}else{
			// bell.mp3 | intuition.mp3 | bip.wav 
			const sound = new Audio('/media/sound/tug.wav');
			setAudio(sound);
			console.log(sound);

			// isMuted(sound);

			if(sound) sound.play();
		}
	}
	
	// const isMuted = (s) => {
	// 	s.oncanplay = function(){
	// 		console.log("Can start playing audio");
	// 	};
	// }

	const onSetNotif = e => {
		let c = e.target.checked;
		// if(!notif){
			setIgnore(c);
			setNotif(c);
			c ? localStorage.setItem('notif', 1) : localStorage.removeItem('notif');
		// }
	}

  const handleButtonClick = () => {
    if(ignore) {
      return;
    }

    const now = Date.now();

    const title = 'React-Web-Notification' + now;
    const body = 'Hello' + new Date();
    const tag = now;
    // const icon = 'http://mobilusoss.github.io/react-web-notification/example/Notifications_button_24.png';
    // const icon = 'http://localhost:3000/Notifications_button_24.png';

    // Available options
    // See https://developer.mozilla.org/en-US/docs/Web/API/Notification/Notification
    const opts = {
      tag: tag,
      body: body,
      // icon: icon,
      lang: 'en',
			dir: 'ltr',
			silent: true,
      // sound: './sound.mp3'  // no browsers supported https://developer.mozilla.org/en/docs/Web/API/notification/sound#Browser_compatibility
		}
		
		setTitle(title);
		setOptions(opts);

    // this.setState({
    //   title: title,
    //   options: options
    // });
  }

  // const handleButtonClick2 = () => {
  //   this.props.swRegistration.getNotifications({}).then(function(notifications) {
  //     console.log(notifications);
  //   });
  // }

	return (
		<Fragment>
      <Modal 
				backdrop="static" 
				keyboard={false} 
				unmountOnClose={false} // OPTION
				isOpen={open} 
				// toggle={toggle} 
				// wrapClassName="modalSetApp" 
				className="drop-static" 
				modalClassName="modalSetApp" 
			>
        <ModalHeader toggle={toggle}>
					Modal title
				</ModalHeader>
				
        <div className="modal-body">
					<Switch label="Notif" 
						checked={notif} 
						onChange={onSetNotif} 
					/>
        </div>
				
        <div className="modal-footer">
					<Btn onClick={handleButtonClick} kind="info">Test Notif</Btn>
          <Btn onClick={toggle}>Save</Btn>
        </div>
      </Modal>
		
			{notif && 
				<NotifApi 
					ignore={ignore && title !== ''}
					notSupported={handleNotSupported}
					onPermissionGranted={handlePermissionGranted}
					onPermissionDenied={handlePermissionDenied}
					onShow={handleNotificationOnShow}
					onClick={handleNotificationOnClick}
					onClose={handleNotificationOnClose}
					onError={handleNotificationOnError}
					timeout={5000}
					title={title}
					options={options}
					// swRegistration={swRegistration} 
				/>
			}
		</Fragment>
	);
}

/*
<React.Fragment></React.Fragment>
*/
