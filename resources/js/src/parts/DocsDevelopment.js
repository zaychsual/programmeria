import React, { useState, lazy, Suspense } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import Draggable from 'react-draggable';
import Dropdown from 'react-bootstrap/Dropdown';

import Flex from '../components/q-ui-react/Flex';
import Btn from '../components/q-ui-react/Btn';
import ModalQ from '../components/q-ui-react/ModalQ';

const Devs = lazy(() => import(/* webpackChunkName: "DocsDevelopmentDevs" */'./docs/Devs'));

export default function DocsDevelopment({
	asideMin, 

}){
	// const [ddMenu, setDdmenu] = useState(false);
	const [modal, setModal] = useState({ open: false, pos: "center" });// false
	const [drag, setDrag] = useState(false);

// console.log('modal.pos: ', modal.pos);
	return (
		<Flex align="baseline" // calc(100% - 220px) 
			// className={"position-absolute t0 l0 r0 zi-1001 p-3 h-full-nav" + (drag ? "" : " point-no")}
			className={"position-fixed b0 r0 l0 zi-1001 h-full-navmain p-3 w-wrap-app" + (asideMin ? "-min" : "") + (drag ? "" : " point-no")}
		>
			<Draggable 
				bounds="parent" 
				handle=".badge" 
				onStart={() => setDrag(true)} 
				onStop={() => setDrag(false)} 
			>
				<div className="point-auto position-absolute" style={{ left: 16, bottom: 16 }}>{/* right: 16, top: 16 */}
					<i 
						className={"badge badge-light px-1 border border-dark position-absolute qi qi-arrows-alt zi-1 cgrab" + (drag ? "bing" : "")} // 
						style={{ top: -10, right: -7 }}
					>{" "}</i>

					<Dropdown className="shadow-sm">
						<Dropdown.Toggle variant="info" size="sm" bsPrefix="qi qi-info" tabIndex={-1} />
						<Dropdown.Menu>
							<Dropdown.Item id="docsDevToggle" onClick={() => setModal({ ...modal, open: true })} {...Q.DD_BTN}>Documentation</Dropdown.Item>
							<Dropdown.Item {...Q.DD_BTN}>Another action</Dropdown.Item>
						</Dropdown.Menu>
					</Dropdown>
				</div>
			</Draggable>

			<ModalQ 
				open={modal.open} 
				onHide={() => setModal({ ...modal, open: !modal.open })} // toggle
				// unmountOnClose={false} 
				size={modal.pos === "center" ? "xl" : ""} 
				scrollable 
				position={modal.pos} 
				close={false} 
				// className={
				// 	Q.Cx({
				// 		"w-50": modal.pos === "modal-left" || modal.pos === "modal-right"
				// 	})
				// } 
				// headClass="d-flex align-items-center" 
				// headProps={{
				// 	tag: "div" 
				// }}
				head={
					<Flex align="center" className="modal-header">
						<h6 className="mb-0">Documentation</h6>
						
						<div>
							<select className="custom-select custom-select-sm w-auto text-capitalize"
								value={modal.pos} // modalPos
								onChange={e => setModal({ ...modal, pos: e.target.value })}
							>
								<option value="center">center</option>{/* , "up", "down" */}
								{["full", "left", "right"].map(v => <option key={v} value={"modal-" + v}>{v}</option>)}
							</select>

							<Btn onClick={() => setModal({ ...modal, open: !modal.open })} kind="flat" size="sm" className="qi qi-close xx" />
						</div>
					</Flex>
				}
				bodyClass="p-0" 
				body={
					<Suspense fallback={<div className="embed-responsive embed-responsive-16by9 cwait" title="Loading" />}>
						<Devs />
					</Suspense>
				}
			/>
		</Flex>
	)
}

/*

*/
