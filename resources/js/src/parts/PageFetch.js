import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';// useRouteMatch, Redirect

import Img from '../components/q-ui-react/Img';
import Placeholder from '../components/q-ui-react/Placeholder';

export default function PageFetch(){
  const { id } = useParams();
  const [data, setData] = useState(null);

  // console.log('id: ',id);

  useEffect(() => {
    // &callback=test
    if(id){
      axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=1b5adf76a72a13bad99b8fc0c68cb085`)
      .then(r => {
        console.log('r: ',r);
        if(r.status === 200){
          console.log('if r: ',r);
          setData(r.data);
        }
        else{
          console.log('else r: ', r)
        }
      }).catch(e => {
        console.log(e);
      });
    }
  }, [id]);

  return (
    <article className="container py-3">		
      {data ? 
        <div>
					{data.backdrop_path ? 
						<Img 
							frame 
							frameClass="w-100 text-center holder thumb" 
							round // thumb // fluid 
							// className="img-" 
							alt={data.title} 
							// backdrop_path | poster_path
							src={"https://image.tmdb.org/t/p/w220_and_h330_face/" + data.backdrop_path} 
							
							style={{ height:'60vh' }}
							onLoad={e => {
								Q.setClass(e.target.parentElement, "holder thumb", "remove")
							}}
						/>
						: 
						<Placeholder type="thumb" className="rounded no-animate cauto fal fa-9x" 
							h="60vh" 
							label="&#xF03E;" // {data.title} 
						/>
					}

          <h1>{data.title}</h1>

          <p>{data.overview}</p>
        </div>
        : 
        <div>Loading</div>
      }
    </article>
  );
}

/*

*/
