import React, { useState, useEffect } from 'react';
// import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Nav from 'react-bootstrap/Nav';
import ListGroup from 'react-bootstrap/ListGroup';
import ReactJson from 'react-json-view';
// import Editor, { loader } from '@monaco-editor/react';// useMonaco

// import Head from '../../components/q-ui-react/Head';
import Flex from '../../components/q-ui-react/Flex';
import Placeholder from '../../components/q-ui-react/Placeholder';
// import ModalQ from '../../components/q-ui-react/ModalQ';
import MonacoEditor from '../../components/monaco-editor/MonacoEditor';
import * as MONACO from '../../data/monaco';
import { getExt, setExt } from '../../utils/file/getSetExt';
import incId from '../../utils/UniqueComponentId';

/** 
 * Change ModalQ with primereact Dialog = https://primefaces.org/primereact/showcase/#/dialog 
*/

export default function Devs(){
  // loader.config({ paths: { vs: "public/js/libs/monaco-editor/min/vs" } });

  const [load, setLoad] = useState(false);
  const [tab, setTab] = useState("notes");// packageJson
  const [tabData, setTabData] = useState({});
  const [editorOptions, setEditorOptions] = useState({ lang: "javascript", theme: MONACO.THEMES[0] });
  const [fileOpen, setFileOpen] = useState("");
  // const fileOpen = files[fileShow];

  useEffect(() => {
    console.log('%cuseEffect in Devs','color:yellow;');
    // "/app-info"
    axios.get("/app_modules").then(r => {
      console.log('r: ', r);
      // console.log('r: ', typeof r.data);
      Q.isObj(r.data) && setTabData({ packageJson: r.data });
    })
    .catch(e => console.log('e: ', e))
    .then(() => setLoad(true));

    // "/get-dir-info" | Q.baseURL + "/public/js/Q.js"
    // axios.get("/get-dir-info").then(r => {
    //   console.log('r: ', r);
    //   // console.log('r: ', typeof r.data);
    // })
    // .catch(e => console.log('e: ', e));
  }, []);

  const onEnterCssColors = () => {
    if(!tabData.cssColors){
      // console.log('onEnter tab cssColors');
      import(/* webpackIgnore:true */Q.baseURL + "/js/esm/const/CSS_COLORS.js")
      .then(m => {
        // console.log('m: ', m);
        if(m && m.default){
          setTabData({ ...tabData, cssColors: m.default });
        }
      }).catch(e => console.warn('e: ', e));
    }
  }

  const parseTree = (data) => {
    const cls = "py-1 px-3 text-truncate q-fw qi qi-";
    return Object.entries(data).map(v => {
      // const size = Q.bytes2Size(v[1].size);
      // const item = {
      //   ...v[1], 
      //   title: "Name: " + v[0] + (size ? "\nSize: " + size : "")
      // };
      // const title = "Name: " + v[0] + "\nDate created: " + Q.dateIntl(v[1].date, LANG_CODE, {year:'numeric',month:'long',day:'numeric',hour:'numeric',minute:'numeric',second:'numeric'});
      const meta = "Name: " + v[0];

      if(getExt(v[0], 1)){
        const ex = setExt(v[0]);// getExt(v[0])
        return {
          ...v[1], // item
          type: "file", 
          ex, 
          meta: meta + "\nSize: " + Q.bytes2Size(v[1].size),  // title + "\nSize: " + Q.bytes2Size(v[1].size),
          cls: cls + ex + " i-color"
        }
      }
      return {
        ...v[1], 
        type: "directory", 
        meta, // : "Name: " + v[0], 
        cls: cls + "folder isDir"
      }
    });
  }

  const renderTree = (data) => {
    const arr = parseTree(data);

    return arr.map((v, i) => 
      <React.Fragment key={i}>
        <ListGroup.Item action type="button" 
          className={v.cls} 
          title={v.meta} 
          onClick={(e) => {
            console.log('onClick tree item v: ', v);
            const path = v.relative_path + (v.relative_path.split("/").pop().length > 0 ? "/" : "") + v.name;
            
            if(v.type === "file"){
              axios.get(Q.baseURL + "/" + path).then(r => {
                console.log('r: ', r);
                if(r.data){
                  const lang = MONACO.LANGS.find(f => f.ex === v.ex);
                  const obj = Q.isObj(r.data);
                  const fixData = obj ? JSON.stringify(r.data, null, 2) : r.data;
                  console.log('lang: ', lang);
                  setEditorOptions({ ...editorOptions, lang: obj ? "json" : lang.name });
                  
                  setFileOpen(fixData);
                }
              })
              .catch(e => console.warn('e: ', e));
            }else{
              if(!v.children){
                axios.post("/get-dir-info", Q.obj2formData({ path }))
                .then(r => {
                  console.log('r: ', r);
                  if(r.data){
                    const jsFiles = tabData.jsFiles;
                    let newData = {};
                    let item;
                    for(let key in jsFiles){
                      item = jsFiles[key];
                      newData[key] = item.relative_path + key === path  ? {
                        ...item,
                        children: r.data
                      }
                      : 
                      item;
                    }
  
                    console.log('jsFiles: ', jsFiles);
                    console.log('newData: ', newData);
                    setTabData({ ...tabData, jsFiles: newData });
                    // Q.setClass(et, "open");
                  }
                })
                .catch(e => console.warn('e: ', e));
              }

              ["qi-folder","qi-folder-open"].forEach(v => e.target.classList.toggle(v));
            }
          }}
        > {v.name}
        </ListGroup.Item>

        {v.children && 
          <ListGroup variant="flush" className="pl-2 border-bottom q-stree-child">
            {renderTree(v.children)}
          </ListGroup>
        }
      </React.Fragment>
    )
  }

  return (
    <Tab.Container mountOnEnter 
      id={"doc-tab" + incId()} 
      activeKey={tab} 
      onSelect={(k) => setTab(k)}
    >
      <Nav variant="pills" className="bg-white position-sticky t0 zi-2">
        {[
          { e: "notes", l: "Notes" }, 
          { e: "packageJson", l: "package.json", disabled: !load }, 
          { e: "phpinfo", l: "Php info" }, 
          { e: "jsFiles", l: "JS Files" }, 
          { e: "cssColors", l: "CSS Colors" }, 
          { e: "icons", l: "Icons" }, 
        ].map(v => 
          <Nav.Link key={v.e} eventKey={v.e} disabled={v.disabled} {...Q.DD_BTN} className="btn py-1">{v.l}</Nav.Link>
        )}
      </Nav>

      <Tab.Content style={{ height: '80vh' }}>{/*  className="border border-top-0" */}
        <Tab.Pane eventKey="notes">
          <div className="p-3">
            <h6>Develop Components:</h6>
            <ol className="pl-3 mt-3-next">
              {[
                { t: "Video Player", d: "Descriptions" }, 
                { t: "Audio Player", d: "Descriptions" }, 
                { t: "Sortable list", d: "Descriptions" }, 
                { t: "Image Map", d: "Descriptions" }, 
              ].map((v, i) => 
                <li key={i}>
                  <div className="card">
                    <div className="card-header">{v.t}</div>
                    <div className="card-body">
                      <pre className="mb-0">{v.d}</pre>
                    </div>
                  </div>
                </li>
              )}
            </ol>
          </div>
        </Tab.Pane>

        <Tab.Pane eventKey="packageJson">
          {load && 
            <div>{/*  className="py-2" */}
              <ReactJson 
                style={{ padding: 15 }}
                name={null} 
                theme="solarized" 
                // enableClipboard={false} // DEFAULT = true
                // displayObjectSize={false} // DEFAULT = true
                displayDataTypes={false} // DEFAULT = true
                src={tabData.packageJson} 
              />
            </div>
          }
        </Tab.Pane>

        <Tab.Pane eventKey="phpinfo" className="h-100">
          <div className="embed-responsive h-100">
            <iframe className="w-100 h-100" src={Q.baseURL + "/phpinfo"} title="phpinfo" />
          </div>
        </Tab.Pane>

        <Tab.Pane eventKey="jsFiles" 
          onEnter={() => {
            if(!tabData.jsFiles){
              console.log('onEnter tab jsFiles');
              axios.post("/get-dir-info", Q.obj2formData({ path:"public/js/" }))
              .then(r => {
                console.log('r: ', r);
                if(r.data){
                  setTabData({ ...tabData, jsFiles: r.data });
                }
                // console.log('r: ', typeof r.data);
              })
              .catch(e => console.log('e: ', e));
            }
          }}
        >
          <Flex dir="row" align="stretch">{/*  className="py-2" */}
            <ListGroup variant="flush" className="flexno w-200px border ovauto">
              {tabData.jsFiles ? 
                renderTree(tabData.jsFiles)
                : 
                <Placeholder length={5} />
              }
            </ListGroup>

            <Flex grow="1">
              {/* <Editor 
                theme={editorOptions.theme} // "vs-dark" 
                height="70vh" 
                // path={fileOpen.name} 
                value={fileOpen} // defaultValue="// some comment" 
                language={editorOptions.lang} // defaultLanguage="javascript" 
              /> */}
              <MonacoEditor 
                wrapClass="w-100" 
                theme={editorOptions.theme} // "vs-dark" 
                height="70vh" 
                value={fileOpen} 
                language={editorOptions.lang} // defaultLanguage="javascript" 
              />
            </Flex>
          </Flex>
        </Tab.Pane>

        <Tab.Pane eventKey="cssColors"
          onEnter={onEnterCssColors}
        >
          <div className="py-2 px-3">
            {tabData.cssColors ?  
              tabData.cssColors.map((v, i) => 
                <div key={i} className="table-responsive">
                  <strong>{v.categories} :</strong>
                  <table className="table table-sm table-bordered">
                    <thead>
                      <tr>
                        <th scope="col" />
                        <th scope="col">Name</th>
                        <th scope="col">Hexa</th>
                        <th scope="col">Rgb</th>
                      </tr>
                    </thead>
                    <tbody>
                      {v.colors.map((v1, i1) => 
                        <tr key={i1}>
                          <td style={{ backgroundColor: v1.hex }}></td>
                          <td>{v1.name}</td>
                          <td>{v1.hex}</td>
                          <td>{v1.rgb}</td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </div>
              )
              : 
              <Placeholder length={5} />
            }
          </div>
        </Tab.Pane>

        <Tab.Pane eventKey="icons"
          // onEnter={onEnterCssColors}
        >
          <div>
            <div className="alert alert-info">
              <h6>Separate & Add options {"<IconPicker />"}</h6>
              1. Standalone<br/>
              2. With Dropdown<br/>
              3. Custom render icon item<br/>
              4. Tabs for icons brand / type<br/>
              5. Groups icon type (like emoji-mart)<br/>
              6. Option to get icon data with ESM / Fetch
            </div>
          </div>
        </Tab.Pane>
      </Tab.Content>
    </Tab.Container>
  );
}

/*
			<ModalQ 
				open={modal} 
				toggle={() => setModal(!modal)} 
				// position="center" 
				// scrollable 
				// size="xl" 
				modalClassName="modal-full" 
        title="Php info" 
        bodyClass="p-0" 
				body={
					<>
						{phpinfo ? 
							<div className="embed-responsive embed-responsive-16by9">
								<iframe className="embed-responsive-item" src={phpinfo} title="phpinfo" />
							</div>
							: 
							<div>More</div>
						}
					</>
				} 
				// foot={
				// 	<Btn kind="dark">Close</Btn>
				// }
			/>
*/