import React, { useState } from 'react';// , { createRef }
import Dropdown from 'react-bootstrap/Dropdown';
// import Nav from 'react-bootstrap/Nav';
import { HexColorPicker, HexColorInput } from 'react-colorful';// RgbaStringColorPicker, RgbaColorPicker
import ImageBlobReduce from 'image-blob-reduce';/** DEVS OPTION: use dynamic import */
// import filenamify from 'filenamify';

import Input from '../components/q-ui-react/Input';
import Btn from '../components/q-ui-react/Btn';
import Flex from '../components/q-ui-react/Flex';
import Textarea from '../components/q-ui-react/Textarea';
import Img from '../components/q-ui-react/Img';
import ModalQ from '../components/q-ui-react/ModalQ';
import ImgEditor from '../apps/image-editor/ImgEditor';
// import { fileRead } from '../utils/file/fileRead';
// import { clipboardCopy } from '../utils/clipboard';
// import { validFilename } from '../utils/string';
import darkOrLight from '../utils/darkOrLight';
// import ImgBlob from '../components/libraries/ImgBlob';

// const SIZES = [16, 32, 36, 48, 57, 60, 70, 72, 76, 96, 114, 120, 144, 150, 152, 180, 192, 310];
const SIZES = [
	{size: 16, ext: "ico"},
	{size: 16}, 
	{size: 32}, 
	{size: 36}, 
	{size: 48}, 
	{size: 57}, 
	{size: 60}, 
	{size: 70}, 
	{size: 72}, 
	{size: 76}, 
	{size: 96}, 
	{size: 114}, 
	{size: 120}, 
	{size: 144}, 
	{size: 150}, 
	{size: 152}, 
	{size: 180}, 
	{size: 192}, 
	{size: 310}, 
];

function setSrcDoc({
	title, 
	theme, 
	body, 
} = {}){
	return (`<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="msapplication-TileColor" content="${theme}">
	<meta name="theme-color" content="${theme}">
	<link rel="shortcut icon" href="./favicon.ico">
	<title>${title}</title>
	<style>body{margin:0;padding:1rem}.icon{max-width:100%;height:auto}</style>
</head>
<body>
${body}
</body></html>`);
}

function makeManifestJson(imgArr, {
	name, // = "App"
	short_name, // = "short_name", 
	description, // = "description", 
	display, // = "standalone", 
	theme_color, // = "#ffffff", 
	background_color, // = "#ffffff", 
	start_url, // ="./", 
	scope, // = "/", 

	zipFolder, // = "icons", 
	// jsonTab, // = 2
}){
	// return JSON.stringify({
	return {
		name, 
		short_name, 
		description, 
		display, 
		theme_color, 
		background_color, 
		// gcm_sender_id: "122867383838", 
		start_url, 
		scope, 
		icons: imgArr.map(v => ({ 
			src: zipFolder + "/" + v.name, 
			type: v.blob.type, 
			sizes: v.size + "x" + v.size, 
			// density: "0.75"
		})), // .filter(f => !f.ext)

		// related_applications: [
		// 	{
		// 		platform: "play",
		// 		id: "org.telegram.messenger",
		// 		url: "https://telegram.org/dl/android?ref=webmanifest"
		// 	}, {
		// 		platform: "itunes",
		// 		url: "https://telegram.org/dl/ios?ref=webmanifest"
		// 	}
		// ]
	};
}

function makeBrowserConfigXml({ 
	path, 
	imgSrc, 
	tileColor
} = {}){
	return (`<?xml version="1.0" encoding="utf-8"?>
<browserconfig>
	<msapplication>
		<tile>
			${imgSrc.map(v => `<square${v.size}x${v.size}logo src="/${path}/${v.name}"/>`).join("\n\t\t\t")}
			<TileColor>${tileColor}</TileColor>
		</tile>
	</msapplication>
</browserconfig>`);
}

// const DUMMY_FILENAME = "    foo";
// console.log("filenamify", filenamify(DUMMY_FILENAME));
// console.log("trim", DUMMY_FILENAME.trim());

export default function ManifestGenerator({
	className, 
	prepend, 
	asideStyle, 
	asideClass, 
	download = true, 
	manifestData = {
		name: "", 
		short_name: "", 
		description: "", 
	}, 
	edtitableFilename = true, 
	zipNameInputProps = {
		value: "icon",
	}, 
	btnDownloadLabel = "Download", 
	btnSaveLabel = "Save", 
	onSave, // function
}){
	const [name, setName] = useState(manifestData.name);// ""
	const [short_name, setShortName] = useState(manifestData.short_name);// ""
	const [description, setDescription] = useState(manifestData.description);// ""
	const [display, setDisplay] = useState("standalone");
	const [start_url, setStartUrl] = useState("./");
	const [scope, setScope] = useState("/");
	const [theme_color, setTheme] = useState("#ffffff");
	const [background_color, setBgColor] = useState("#ffffff");
	const [sizes, setSizes] = useState(SIZES);
	const [customSizeVal, setCustomSizeVal] = useState("");

	const [themePick, setThemePick] = useState(false);
	const [bgPick, setBgPick] = useState(false);
	const [zipFolder, setZipFolder] = useState(zipNameInputProps.value);// icons
	const [jsonTab, setJsonTab] = useState(2);

	const [fileData, setFileData] = useState(null);
	const [img, setImg] = useState([]);
	const [modal, setModal] = useState(false);
	const [fileNameBefore, setFileNameBefore] = useState();
	const [loadSave, setLoadSave] = useState(false);
	
	const onToggleTheme = (see, key) => {
		const docActive = document.activeElement;
		// console.log('onToggleTheme docActive: ', docActive);
		// console.log('onToggleTheme docActive.dataset.color: ', docActive.dataset.color);
		// console.log('onToggleTheme key: ', key);
		// console.log('onToggleTheme see: ', see);
		// console.log('onToggleTheme themePick: ', themePick);
		if(themePick && docActive.dataset.color === key){ //  && see && docActive 
			setThemePick(true);
		}else{
			setThemePick(see);
		}
	}

	const onToggleBg = (see, key) => {
		const docActive = document.activeElement;
		
		if(bgPick && docActive.dataset.color === key){
			setBgPick(true);
		}else{
			setBgPick(see);
		}
	}
	
	const isDark = (c) => darkOrLight(c.replace("#",""));

	const onZip = (zip, action) => {
		const imgNoFavicon = img.filter(v => !v.ext);// JSON.stringify(, null, jsonTab);
		const manifest = makeManifestJson(imgNoFavicon, {
			name, 
			short_name, 
			description, 
			display, 
			start_url, 
			scope, 
			theme_color, 
			background_color, 
			zipFolder, 
			// jsonTab
		});

		const browserConfig = makeBrowserConfigXml({ 
			path: zipFolder, 
			imgSrc: imgNoFavicon.filter(v => [70, 150, 310].includes(v.size)), 
			tileColor: theme_color
		});

		const indexHtml = setSrcDoc({
			title: name, 
			theme: theme_color, 
			body: imgNoFavicon.map(v => `<img src="./${v.name}" alt="${v.name}" class="icon"/>`).join("\n"), 
		});

		zip.file(zipFolder + "/manifest.json", JSON.stringify(manifest, null, jsonTab));
		zip.file(zipFolder + "/browserconfig.xml", browserConfig);
		zip.file(zipFolder + "/index.html", indexHtml);
		
		img.forEach((v) => {
			zip.file(zipFolder + "/" + v.name, v.blob); // "icons/" + v.name, v.blob
		});
		
		zip.generateAsync({ type: "blob" }).then((blob) => {
			console.log("zip: ", blob);

			if(download && action === "download"){
				let a = Q.makeEl("a");
				a.href = URL.createObjectURL(blob);
				a.rel = "noopener";
				a.download = zipFolder;

				setTimeout(() => a.click(), 1);
				setTimeout(() => {
					URL.revokeObjectURL(a.href);
					// a = null;// OPTION
				}, 4E4); // 40s
			}
			
			if(onSave && action === "save"){
				onSave({ 
					manifest, 
					// images: img.map(v => ({ size: v.size, name: v.name, blob: v.blob })), 
					zip: new File([blob], zipFolder + "." + blob.type.replace("application/", "").toLowerCase(), { // file.name
						lastModified: Date.now(), 
						type: blob.type 
					})
				})
			}
		});
	}

	const onClickSave = (action) => {
		if((fileNameBefore && fileData && fileNameBefore === fileData.name)){
			import("jszip").then(JSZip => {
				// console.log('JSZip: ', JSZip);
				if(JSZip && JSZip.default){ // navigator.onLine
					onZip(new JSZip.default(), action);
				}else{
					swalToast({ icon:"error", text:"Failed " + action + " file" });
				}
			});
		}
	}

	const onSaveIcons = val => {
		console.log('ImgEditor onSaveIcons val: ', val);
		setLoadSave(true);

		const { blob } = val;// , file
		const ext = blob.type.replace("image/", "").toLowerCase();// 
		// const fname = file ? file.name.replace(/ /g, "-").split(".")[0] : val.name ? val.name.replace(/ /g, "-") : "Untitled";
		// const fname = zipNameInputProps?.value ? zipNameInputProps.value.replace(/ /g, "-") : file ? file.name.replace(/ /g, "-").split(".")[0] : "logo";
		const fname = "logo";
		const files = new File([blob], fname + "." + ext, { // file.name
			lastModified: Date.now(), 
			type: blob.type // file.type
		});
		
		/** DEVS OPTION: use dynamic import  ImageBlobReduce */
		// let arr = [];
		// sizes.forEach((v) => { // SIZES
		// 	ImageBlobReduce().toBlob(files, {
		// 		max: v.size, 
		// 		unsharpAmount: 80, 
		// 		unsharpRadius: 0.6,
		// 		unsharpThreshold: 2
		// 	}).then(blob => {
		// 		arr.push({ 
		// 			...v, 
		// 			blob, 
		// 			src: URL.createObjectURL(blob), 
		// 			name: v.ext ? "favicon." + v.ext : fname + "-" + v.size + "x" + v.size + "." + ext
		// 		});
		// 	});
		// });

		// setFileData({ blob, ext, name: fname });
		// setFileNameBefore(fname);
		
		// setTimeout(() => {
		// 	arr.sort((a, b) => (a.size > b.size) ? 1 : -1)
		// 	setImg(arr);
		// 	setModal(false);
		// 	setLoadSave(false);
		// }, 3000);

		// let arr = [];
		let promises = [];
		
		sizes.forEach((v) => { // SIZES
			promises.push(
				new Promise((resolve, reject) => {
					ImageBlobReduce().toBlob(files, {
						max: v.size, 
						unsharpAmount: 80, 
						unsharpRadius: 0.6,
						unsharpThreshold: 2
					}).then(blob => {
						resolve({ 
							...v, 
							blob, 
							src: URL.createObjectURL(blob), 
							name: v.ext ? "favicon." + v.ext : fname + "-" + v.size + "x" + v.size + "." + ext
						});
					}).catch((e) => reject(e));
				})
			);
		});

		setFileData({ blob, ext, name: fname });
		setFileNameBefore(fname);

		const errMsg = () => swalToast({ title:"Failed generate icons", icon:"error" });
		Promise.all(promises).then(res => {
			console.log('res: ', res);
			if(res){
				let arr = [ ...res ];
				arr.sort((a, b) => (a.size > b.size) ? 1 : -1)
				setImg(arr);
				setModal(false);
				setLoadSave(false);
			}else{
				errMsg();
			}
		}).catch(() => errMsg());// e
	}

	const onAddCustomSize = () => {
		// console.log('customSizeVal: ', customSizeVal > 0);
		// console.log('typeof customSizeVal: ', typeof customSizeVal);
		// console.log('!isNaN(customSizeVal): ', !isNaN(customSizeVal));
		if(customSizeVal.length && customSizeVal > 0){ //  > 0
			const size = Number(customSizeVal);
			const availSize = sizes.find(f => f.size === size);//  || f.size === customSizeVal
			if(availSize){
				// console.log('availSize: ', availSize);
				swalToast({ icon:"error", text:"Size is available, please add other size" });
			}else{
				setSizes([...sizes, { size, custom: 1 }]);
				setCustomSizeVal("");
				setTimeout(() => Q.domQall("#ddIconSize .dropdown-item")[sizes.length - 1]?.focus(), 9);
			}
		}else{
			swalToast({ icon:"error", text:"Please input decimal size minimum 1" });
		}
	}
	
	const renderSup = () => <sup className="text-danger bold">*</sup>;
	
	const renderBtnSaveDownload = (label, action) => {
		return (
			<Btn 
				disabled={zipFolder.length < 1 || img.length < 1 || (!name || !short_name) || (fileNameBefore && fileData && fileNameBefore !== fileData.name)} //  && fileNameBefore && fileData && fileNameBefore !== fileData.name
				onClick={() => onClickSave(action)}>{label}</Btn>
		);
	}

	return (
		<div className={Q.Cx("manifest-generator", className)}>
			{prepend} 

			<div className="row">
				<div className="col-md-3 mb-4">
					<aside 
						className={Q.Cx("card shadow-sm", asideClass)} 
						style={asideStyle}
					>
						<div className="card-body bg-light">
							<div className="form-group">
								<label htmlFor="icons_folder">Icons folder name {renderSup()}</label>
								<Input isize="sm" id="icons_folder" 
									{...zipNameInputProps} 
									value={zipFolder} 
									onChange={e => setZipFolder(e.target.value)}
								/>
							</div>

							<div className="form-group">
								<label htmlFor="jsonTab">manifest.json tab size</label>
								<select className="custom-select custom-select-sm" id="jsonTab" 
									value={jsonTab} 
									onChange={e => setJsonTab(e.target.value)} // , 6, 8
								>
									{[2, 4].map((v, i) => <option key={i} value={v}>{v}</option>)}
									<option value={0}>Minify</option>
								</select>
							</div>

							{/* (imgOri && img.length > 0) */}
							{/* (fileNameBefore && fileNameBefore !== fileData.name) */}
							{/* {img.length > 0 && <Btn onClick={onClickSave}>Download</Btn>} */}

							{download && renderBtnSaveDownload(btnDownloadLabel, "download")}
							{" "}
							{onSave && renderBtnSaveDownload(btnSaveLabel, "save")}
						</div>
					</aside>
				</div>

				<div className="col-md-9">
					<div className="card shadow-sm">
						<div className="card-body">
							<div className="form-row">
								<div className="col-md-6 form-group">
									<label htmlFor="name">App name {renderSup()}</label>
									<Input id="name" 
										value={name} 
										onChange={e => setName(e.target.value)}
									/>
								</div>

								<div className="col-md-6 form-group">
									<label htmlFor="short_name">Short name {renderSup()}</label>
									<Input id="short_name" 
										value={short_name} 
										onChange={e => setShortName(e.target.value)}
									/>
								</div>

								<div className="col-md-12 form-group">
									<label htmlFor="description">Description</label>
									<Textarea id="description" //  type="text"
										value={description} 
										onChange={e => setDescription(e.target.value)}
									/>
								</div>

								<div className="col-md-4 form-group">
									<label htmlFor="display">Display</label>
									<select className="custom-select" id="display" 
										value={display} 
										onChange={e => setDisplay(e.target.value)}
									>
										{["browser", "standalone", "minimal-ui", "fullscreen"].map((v, i) => <option key={i} value={v}>{v}</option>)}
									</select>
								</div>

								<div className="col-md-4 form-group">
									<label htmlFor="start_url">Start url {renderSup()}</label>
									<Input id="start_url" 
										value={start_url} 
										onChange={e => setStartUrl(e.target.value)}
									/>
								</div>

								<div className="col-md-4 form-group">
									<label htmlFor="scope">Scope</label>
									<Input id="scope" 
										value={scope} 
										onChange={e => setScope(e.target.value)}
									/>
								</div>

								<div className="col-md-6 form-group">
									<label htmlFor="theme_color">Theme Color {renderSup()}</label>
									<Dropdown tabIndex="0" 
										show={themePick} 
										onToggle={(isOpen) => onToggleTheme(isOpen, "theme_color")} 
										onBlur={() => setThemePick(false)} // DEVS
									>
										<Dropdown.Toggle as="div" bsPrefix="wrap-input-color input-group" tabIndex="0" data-color="theme_color">
											<label className="input-group-prepend mb-0" htmlFor="theme_color">
												<div className="input-group-text">#</div>
											</label>
											<HexColorInput 
												color={theme_color} 
												onChange={setTheme} 
												type="text" 
												data-color="theme_color" 
												// inputMode="search" // 
												id="theme_color" 
												className={
													Q.Cx("form-control text-monospace", { 
														["text-" + (isDark(theme_color) === "dark" ? "white":"dark")] : theme_color !== ""
													})
												} 
												style={{ backgroundColor: theme_color }}
											/>
										</Dropdown.Toggle>
										
										<Dropdown.Menu className="p-1" onContextMenu={Q.preventQ}>
											<HexColorPicker color={theme_color} onChange={setTheme} />
										</Dropdown.Menu>
									</Dropdown>
								</div>

								<div className="col-md-6 form-group">
									<label htmlFor="background_color">Background Color {renderSup()}</label>
									<Dropdown tabIndex="0" 
										show={bgPick} 
										onToggle={(isOpen) => onToggleBg(isOpen, "background_color")} 
										onBlur={() => setBgPick(false)} // DEVS
									>
										<Dropdown.Toggle as="div" bsPrefix="wrap-input-color input-group" tabIndex="0" data-color="background_color">
											<label className="input-group-prepend mb-0" htmlFor="background_color">
												<div className="input-group-text">#</div>
											</label>
											<HexColorInput 
												color={background_color} 
												onChange={setBgColor} 
												type="text" 
												data-color="background_color" 
												id="background_color" 
												className={
													Q.Cx("form-control text-monospace", {
														["text-" + (isDark(background_color) === "dark" ? "white":"dark")] : background_color !== ""
													})
												} 
												style={{ backgroundColor: background_color }}
											/>
										</Dropdown.Toggle>
										
										<Dropdown.Menu className="p-1" onContextMenu={Q.preventQ}>
											<HexColorPicker color={background_color} onChange={setBgColor} />
										</Dropdown.Menu>
									</Dropdown>
								</div>

								<div className="col-md-12 form-group">
									<label htmlFor="icons">Icons {renderSup()}</label>
									{fileData ? 
										<Flex>
											<div className="input-group mr-1" // tip tipTL
												// aria-label="Change icon name"
											>
												{/* <input className="form-control" type="text" id="icons" 
													title={fileData.name} 
													// readOnly 
													value={fileData.name} 
													onChange={e => {
														const val = e.target.value.trim();
														const isValid = validFilename(val);
														console.log('isValid: ', isValid);

														if(isValid){
															const newFileData = { ...fileData, name: val };

															setFileData(newFileData);
														}
													}}
													// setFileNameBefore(fname);
												/> */}
												<div className="form-control">{fileNameBefore}</div>

												<div className="input-group-append">
													<b className="input-group-text">.{fileData.ext}</b>
												</div>
											</div>

											{/* {(fileNameBefore && fileNameBefore !== fileData.name) && 
												<Btn kind="light" className="flexno mr-1 tip tipTR qi qi-check" aria-label="Save icon name" 
													onClick={() => {
														if(img.length > 0) setImg([]);
														onSaveIcons(fileData);
													}}
												/>
											} */}
											
											<Btn className="flexno tip tipTR qi qi-edit" aria-label="Change icon" 
												onClick={() => setModal(true)}
											/>
										</Flex>
										: 
										<Btn block outline kind="secondary" 
											className="text-left" 
											id="icons" 
											onClick={() => setModal(true)}
										>
											Choose file
										</Btn>
									}
								</div>

								{/* CUSTOM ICON SIZE: */}
								<div className="col-md-12 form-group">
									<label htmlFor="iconsSize">Icons size</label>
									<Dropdown>
										<Dropdown.Toggle block variant="light" id="iconsSize" className="text-left">Icons size</Dropdown.Toggle>
										<Dropdown.Menu className="py-0 w-100" id="ddIconSize">
											<div className="input-group input-group-sm p-2 bg-white shadow-sm">
												<Input // id="icustomIconSize" 
													type="number" 
													min={1} 
													placeholder="Insert Size" 
													value={customSizeVal} 
													onChange={e => setCustomSizeVal(e.target.value)}
												/>
												<div className="input-group-append">
													<Btn blur onClick={onAddCustomSize} className="qi qi-plus" title="Add size" />
												</div>
											</div>

											<div className="mxh-50vh ovyauto py-2">
												{sizes.filter(f => !f.ext).map((v, i) => // cauto
													<button key={i} className="dropdown-item d-flex small" type="button">
														{v.size + " x " + v.size}

														{v.custom && 
															<Btn As="div" onClick={() => setSizes(sizes.filter((f) => f.size !== v.size))} size="xs" kind="light" className="ml-auto qi qi-close xx" title="Remove" />
														}
													</button>
												)}
											</div>
											
										</Dropdown.Menu>
									</Dropdown>
								</div>
								
								{img.length > 0 && //  ml-2-next
									<div className="col-md-12">
										<div className="card-columns text-center">
											{img.filter(v => !v.ext).map((v, i) => 
												<div key={i} className="card shadow-sm">
													<Img 
														frame="bs" 
														frameClass="card-body p-2 mb-0" 
														className="flexno" 
														fluid 
														src={v.src} // v.blob && URL.createObjectURL(v.blob) 
														alt={fileData?.name} //  + " - " + v.size
														caption={v.size + "x" + v.size} 
														onLoad={e => URL.revokeObjectURL(e.target.src)}
													/>
												</div>
											)}
										</div>
									</div>
								}
							</div>
						</div>
					</div>
				</div>
			</div>

			<ModalQ 
				open={modal} 
				// toggle
				size="xl" // lg 
				contentClassName="border-0 ovhide" 
				position="center" 
				close={false} 
				bodyClass="p-0" 
				body={
					<fieldset 
						disabled={loadSave} 
						className={loadSave ? " i-load cwait" : undefined} 
						style={loadSave ? { '--bg-i-load': '95px' } : null} 
					>
						<ImgEditor 
							edtitableFilename={edtitableFilename} 
							// pasteFromClipboard={false} 
							h={450} 
							// wrapClass="rounded" 
							captureImage={false} 
							captureScreen={false} 
							onSave={onSaveIcons}
						/>

						<Btn onClick={() => setModal(false)} kind="dark" size="sm" className="my-1 mr-2 float-right">Cancel</Btn>
					</fieldset>
				}
				// foot={
				// 	<Btn onClick={() => setModal(false)} kind="dark" size="sm">Cancel</Btn>
				// }
			/>
		</div>
	);
}

/*
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

			// if(window.JSZip){
			// 	onZip(new JSZip());
			// }else{
			// 	Q.getScript({src:"/js/libs/jszip/jszip.min.js","data-js":"JSZip"}).then(() => {
			// 		if(window.JSZip){
			// 			onZip(new JSZip());
			// 		}
			// 		else{
			// 			console.log("%cjszip failed", "color:yellow");
			// 		}
			// 	}).catch(e => console.log(e));
			// }

<div className="input-group">
	<div className="custom-file text-truncate">
		<input type="file" className="custom-file-input" id="inputile" 
			onChange={onChangeIcon} 
		/>
		<label className="custom-file-label" htmlFor="inputile">
			{fileData?.name || "Choose file"}
		</label>
	</div>
</div>

{(imgOri && img.length > 0) && 
	<div className="col-md-12">
		<div className="row mb-3">
			<div className="col">
				<Img fluid src={imgOri} alt={fileData?.name} 
					onLoad={e => {
						URL.revokeObjectURL(e.target.src)
					}}
				/>
			</div>
			
			<div className="col ml-2-next">
				{img.map((v, i) => 
					<Img key={i} fluid src={URL.createObjectURL(v.blob)} alt={fileData?.name + " - " + v.size} 
						onLoad={e => {
							URL.revokeObjectURL(e.target.src)
						}}
					/>
				)}
			</div>
		</div>
	</div>
}
*/