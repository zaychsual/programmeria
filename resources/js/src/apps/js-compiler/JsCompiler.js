import React, { useRef, useState, useEffect } from 'react';// , {  }

import Btn from '../../components/q-ui-react/Btn';
import MonacoEditor from '../../components/monaco-editor/MonacoEditor';

// Override default console functions for our custom Dev Console = https://blog.usejournal.com/how-to-build-your-custom-js-ide-in-the-browser-838a51c654bd
function consoles(el){
  // store default console functionality before changing them
  let defaultLog = console.log;
  let defaultError = console.error;
  // let defaultClear = console.clear;
  // let defaultWarn = console.warn;
  // let defaultInfo = console.info;

  console.log = function (...args){
    let res = "";
    for (let arg of args) {
      if(Q.isObj(arg)){ // typeof arg == 'object'
        // $("#console").append((JSON && JSON.stringify ? JSON.stringify(arg, undefined, 2) : arg) + ' ');
        res = (JSON && JSON.stringify ? JSON.stringify(arg, undefined, 2) : arg) + " ";
      }else{
        // $("#console").append(arg + ' ');
        res = arg + " ";
      }
    }
    // Console prompt
    // $("#console").append('\n&raquo;  ');
    res += "\n&raquo;  ";
    el.innerHTML = el.innerHTML + res;

    // So console is always scrolled to the bottom
    // $("#console").get(0).scrollTop = $("#console").get(0).scrollHeight;
    
    defaultLog(...args);// // Allow the default console action to happen
  }
  console.error = function(e){
    let res = "Error: " + e;
    // $("#console").append("Error: " + e);

    // Console prompt
    // $("#console").append('\n&raquo;  ');
    res += "\n&raquo;  ";
    el.innerHTML = el.innerHTML + res;

    // So console is always scrolled to the bottom
    // $("#console").get(0).scrollTop = $("#console").get(0).scrollHeight;
    
    defaultError(e);// Allow the default console action to happen
  }
}

export default function JsCompiler(){
  const preRef = useRef(null);
  const [code, setCode] = useState("");

  useEffect(() => {
    if(preRef.current) consoles(preRef.current);
  }, [preRef]);

  const onCompile = () => {
    // eval(code);
    new Function("return " + code)();
  }

  return (
    <div>
      <MonacoEditor 
        // theme="vs-dark" 
        // height="40vh" 
        // disabled // Custom
        // required // Custom
        // defaultValue="// some comment" 
        changeLang={false} 
        language="javascript" 
        nav={(theme) => (
          <Btn onClick={onCompile} className="border-0 qi qi-code" kind={theme} />
        )}
        value={code} 
        onChange={(val) => {
          // console.log('onChange MonacoEditor val: ', val);
          setCode(val);
        }}
      />

      <pre ref={preRef}>» </pre>
    </div>
  )
}