import React, { useRef, useState, useEffect } from 'react';
import { UncontrolledTooltip } from 'reactstrap';

import { useFetch } from '../../utils/hooks/useFetch';
import Img from '../../components/q-ui-react/Img';

export function ImageMapDesigner({

}){
  const [data, setData] = useState([]);
  const [objects, setObjects] = useState([]);
  const [img, setImg] = useState();// {}
  
	const isLoad = useFetch(
		async (cancelToken) => {
			const req = await axios.get(Q.baseURL + "/public/DUMMY/people_map.json", { cancelToken });
			console.log('req: ', req);
			if(req.data && !req.data.error){// r.status === 200 && 
        const objs = req.data.objects;
        const isImg = objs.find(f => f.type === "image" && f.id === "workarea");
        
        if(isImg){
          setImg(isImg);
        }

        const dataObj = objs.filter(f => f.type !== "image" && f.id !== "workarea");
        setObjects(dataObj);
        setData(req.data);

        console.log('isImg: ', isImg);
        console.log('dataObj: ', dataObj);
			}else{
				console.log('handle error server e: ', e);
			}
		}, 
		(err) => {
			console.log('err: ', err);
		}
	);

  useEffect(() => {
    console.log('%cuseEffect in ImageMapDesigner','color:yellow');
    
  }, []);

  const renderSvgChild = () => {
    // <polygon points="537,357 981,675 496,799 537,357" />

    return objects.map((v, i) => {
      return (
        <React.Fragment key={i}>
          <a // xlink:href=""
            href={v.link.url} 
            target={v.link.state === "new" ? "_blank" : undefined} 
            // rel={} 
            title={v.name} 
            id={"tipMap-" + i} // {v.id} 
          >
            {v.type === "circle" && 
              <circle 
                cx={v.left} // width
                cy={542.78} // width
                r="50" 
                fill={v.fill} 
                stroke={v.stroke} 
                strokeWidth={v.strokeWidth} 
              />
            }
          </a>

          <UncontrolledTooltip 
            autohide={false} 
            placement="auto" // right
            target={"tipMap-" + i} // {v.id} 
          >
            <h1>{v.name}</h1>
          </UncontrolledTooltip>
        </React.Fragment>
      );
    });
  }

  return (
    <div className="img-map-designer">
      <div className="row no-gutters">
        <aside className="col-3">

        </aside>

        <div className="col-9 position-relative">
          {isLoad &&  
            <>
              {img && 
                <Img 
                  alt={img.name}  
                  src={img.src} 
                  className="img-main" 
                  fluid={img.layout === "responsive"} 
                />
              }
              
              {objects && 
                <svg 
                  // version="1.1" 
                  xmlns="http://www.w3.org/2000/svg" 
                  // xmlns:xlink="http://www.w3.org/1999/xlink" 
                  className="w-100 h-100 position-absolute inset-0 zi-2 svg-img-map" 
                >
                  {renderSvgChild()}
                </svg>
              }
              
            </>
          }
        </div>
      </div>
    </div>
  );
}

/*
            <img 
              alt="A_Dramatic_Turn_Of_Events" 
              src="public/DUMMY/A_Dramatic_Turn_Of_Events.jpg" 
              loading="lazy" 
              className="img-fluid" 
            />
            <svg 
              // version="1.1" 
              xmlns="http://www.w3.org/2000/svg" 
              // xmlns:xlink="http://www.w3.org/1999/xlink" 
              width="1425" 
              height="1425" 
              className="w-100 h-100 position-absolute inset-0 zi-2" 
            >

            </svg>
*/