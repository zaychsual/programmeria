import React, { useState, Fragment } from 'react';
import Frame, { FrameContextConsumer } from 'react-frame-component';
import Alert from 'react-bootstrap/Alert';
import Badge from 'react-bootstrap/Badge';
import Card from 'react-bootstrap/Card';
import Dropdown from 'react-bootstrap/Dropdown';
import Modal from 'react-bootstrap/Modal';

import Btn from '../../components/q-ui-react/Btn';
import Input from '../../components/q-ui-react/Input';
import Img from '../../components/q-ui-react/Img';
import bootstrap_4 from './themes/bootstrap_4';
// import copyStyles from '../../utils/css/copyStyles';

const COLORS = ["primary", "secondary", "success", "danger", "warning", "info", "light", "dark"];

// <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
const srcDoc = ({ fontFamily, fontSize, fontWeight, lineHeight, color, backgroundColor, palette }) => `<!DOCTYPE html><html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <style>
  ${bootstrap_4({
    fontFamily, 
    fontSize: fontSize.size + fontSize.unit, 
    fontWeight, 
    lineHeight: lineHeight.size + lineHeight.unit, 
    color, 
    backgroundColor, 
    primary: palette.primary.default
  })}
  .t0{top:0;}
  .zi-1{z-index:1;}
  .mt-2-next > * + * {
    margin-top: .5rem !important;
  }
  </style>
  </head>
<body><div id="root"></div></body></html>`;

export default function DemoComponents({ 
  fontFamily, fontSize, fontWeight, lineHeight, color, backgroundColor, palette 
}){
  const [seeAlert, setSeeAlert] = useState(true);
  const [showModal, setShowModal] = useState(false);

  return (
    <div className="embed-responsive embed-responsive-1by1">
      <Frame 
        className="embed-responsive-item" 
        loading="lazy" 
        initialContent={srcDoc({ fontFamily, fontSize, fontWeight, lineHeight, color, backgroundColor, palette })} 
        mountTarget="#root" 
      >
        <FrameContextConsumer>
          {frame => {
            // console.log('frame.document: ', frame.document);
            // console.log('frame.window: ', frame.window);
            // setTimeout(() => copyStyles(document, frame.document), 0);
            return (
              <div className="mt-2-next">
                <Card className="bw-y1 border-top-0 rounded-0">
                  <Card.Header className="rounded-0 position-sticky t0 zi-1 bg-light">Alerts</Card.Header>
                  <Card.Body>
                    {COLORS.map(v => 
                      <Alert key={v} variant={v}>
                        This is a {v} alert with <Alert.Link onClick={e => e.preventDefault()} href="/">an example link</Alert.Link>. Give it a click if you like.
                      </Alert>
                    )}
                    <hr/>
                    <h6>Alerts - Additional content</h6>
                    <Alert variant="success">
                      <Alert.Heading>Hey, nice to see you</Alert.Heading>
                      <p>
                        Aww yeah, you successfully read this important alert message. This example
                        text is going to run a bit longer so that you can see how spacing within an
                        alert works with this kind of content.
                      </p>
                      <hr />
                      <p className="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
                    </Alert>
                    <hr/>
                    <h6>Dismissing</h6>
                    <Alert show={seeAlert} variant="danger" onClose={() => setSeeAlert(false)} dismissible>
                      <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
                      <p>
                        Change this and that and try again. Duis mollis, est non commodo
                        luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
                        Cras mattis consectetur purus sit amet fermentum.
                      </p>
                    </Alert>

                    {!seeAlert && <Btn onClick={() => setSeeAlert(true)}>Show Alert</Btn>}
                  </Card.Body>
                </Card>

                <Card className="bw-y1 rounded-0">
                  <Card.Header className="rounded-0 position-sticky t0 zi-1 bg-light">Badge</Card.Header>
                  <Card.Body>
                    {[1,2,3,4,5,6].map(v => 
                      <div key={v} className={"h" + v}>Example heading <Badge variant="secondary">New</Badge></div>
                    )}
                    <hr/>
                    <h6>Badges can be used as part of links or buttons to provide a counter.</h6>
                    <Btn>
                      Profile <Badge variant="light">9</Badge>
                      <span className="sr-only">unread messages</span>
                    </Btn>
                    <hr/>
                    <h6>Pill</h6>
                    {COLORS.map(v => <Badge key={v} pill variant={v} className="mr-1">{v}</Badge>)}
                  </Card.Body>
                </Card>

                <Card className="bw-y1 rounded-0">
                  <Card.Header className="rounded-0 position-sticky t0 zi-1 bg-light">Breadcrumbs</Card.Header>
                  <Card.Body>
                    <nav aria-label="breadcrumb">
                      <ol className="breadcrumb">
                        <li className="breadcrumb-item active" aria-current="page">Home</li>
                      </ol>
                    </nav>

                    <nav aria-label="breadcrumb">
                      <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a onClick={e => e.preventDefault()} href="/">Home</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Library</li>
                      </ol>
                    </nav>

                    <nav aria-label="breadcrumb">
                      <ol className="breadcrumb mb-0">
                        <li className="breadcrumb-item"><a onClick={e => e.preventDefault()} href="/">Home</a></li>
                        <li className="breadcrumb-item"><a onClick={e => e.preventDefault()} href="/">Library</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Data</li>
                      </ol>
                    </nav>
                  </Card.Body>
                </Card>

                <Card className="bw-y1 rounded-0">
                  <Card.Header className="rounded-0 position-sticky t0 zi-1 bg-light">Buttons</Card.Header>
                  <Card.Body>
                    {COLORS.map(v => <Btn key={v} kind={v} className="mr-1">{v}</Btn>)}
                    <Btn kind="link" className="mr-1">link</Btn>
                    <Btn kind="flat" className="mr-1">flat</Btn>

                    <hr/>
                    <h6>Outline buttons</h6>
                    {COLORS.map(v => <Btn key={v} outline kind={v} className="mr-1">{v}</Btn>)}

                    <hr/>
                    <h6>Button tags</h6>
                    <Btn As="a" href="/">Link</Btn>{" "}
                    <Btn>Button</Btn>{" "}
                    <Btn As="input" type="button" defaultValue="Input" />{" "}
                    <Btn As="input" type="submit" defaultValue="Submit" />{" "}
                    <Btn As="input" type="reset" defaultValue="Reset" />{" "}

                    <hr/>
                    <h6>Sizes</h6>
                    <Btn size="lg">Large</Btn>{" "}
                    <Btn>Default</Btn>{" "}
                    <Btn size="sm">Small</Btn>

                    <hr/>
                    <h6>Block</h6>
                    <Btn block>Block level button</Btn>
                    <Btn block kind="secondary">Block level button</Btn>

                    <hr/>
                    <h6>Active state</h6>
                    
                      {COLORS.map(v => <Btn key={v} active kind={v} className="mr-1">{v}</Btn>)}
                    
                    <h6 className="mt-2">Active state outline</h6>
                    {COLORS.map(v => <Btn key={v} active outline kind={v} className="mr-1">{v}</Btn>)}

                    <hr/>
                    <h6>Disabled state</h6>
                    <Btn disabled>Disabled</Btn>{" "}
                    <Btn disabled kind="secondary">Disabled</Btn>

                    <h6 className="mt-2">Disabled state link</h6>
                    <Btn As="a" onClick={e => e.preventDefault()} href="/" disabled>Disabled</Btn>{" "}
                    <Btn As="a" onClick={e => e.preventDefault()} href="/" disabled kind="secondary">Disabled</Btn>
                  </Card.Body>
                </Card>

                <Card className="bw-y1 rounded-0">
                  <Card.Header className="rounded-0 position-sticky t0 zi-1 bg-light">Button groups</Card.Header>
                  <Card.Body>
                    <div className="btn-group" role="group">
                      {["Left", "Middle", "Right"].map(v => <Btn key={v} kind="secondary">{v}</Btn>)}
                    </div>

                    <hr/>
                    <h6>Button toolbar</h6>
                    <div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                      <div className="btn-group mr-2" role="group" aria-label="First group">
                        {[1,2,3,4].map(v => <Btn key={v}>{v}</Btn>)}
                      </div>
                      <div className="btn-group mr-2" role="group" aria-label="Second group">
                        {[5,6,7].map(v => <Btn key={v}>{v}</Btn>)}
                      </div>
                      <div className="btn-group" role="group" aria-label="Third group">
                        <Btn>8</Btn>
                      </div>
                    </div>

                    <hr/>
                    <p>Feel free to mix input groups with button groups in your toolbars. Similar to the example above, you’ll likely need some utilities though to space things properly.</p>
                    <div className="btn-toolbar mb-3" role="toolbar" aria-label="Toolbar with button groups">
                      <div className="btn-group mr-2" role="group" aria-label="First group">
                        {[1,2,3,4].map(v => <Btn key={v} kind="secondary">{v}</Btn>)}
                      </div>
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <div className="input-group-text" id="btnGroupAddon">@</div>
                        </div>
                        <Input placeholder="Input group example" aria-label="Input group example" aria-describedby="btnGroupAddon" />
                      </div>
                    </div>
                    <div className="btn-toolbar justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
                      <div className="btn-group" role="group" aria-label="First group">
                        {[1,2,3,4].map(v => <Btn key={v} kind="secondary">{v}</Btn>)}
                      </div>
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <div className="input-group-text" id="btnGroupAddon2">@</div>
                        </div>
                        <Input placeholder="Input group example" aria-label="Input group example" aria-describedby="btnGroupAddon2" />
                      </div>
                    </div>

                    <hr/>
                    <h6>Sizing</h6>
                    <div className="btn-group btn-group-lg" role="group">
                      {["Left", "Middle", "Right"].map(v => <Btn key={v} kind="secondary">{v}</Btn>)}
                    </div>
                    <br/>
                    <div className="btn-group my-2" role="group">
                      {["Left", "Middle", "Right"].map(v => <Btn key={v} kind="secondary">{v}</Btn>)}
                    </div>
                    <br/>
                    <div className="btn-group btn-group-sm" role="group">
                      {["Left", "Middle", "Right"].map(v => <Btn key={v} kind="secondary">{v}</Btn>)}
                    </div>

                    <hr/>
                    <h6>Nesting</h6>
                    <div className="btn-group" role="group">
                      <Btn>1</Btn>
                      <Btn>2</Btn>

                      <Dropdown bsPrefix="btn-group">
                        <Dropdown.Toggle>Dropdown</Dropdown.Toggle>
                        <Dropdown.Menu>
                          <Dropdown.Item onClick={e => e.preventDefault()} href="/">Dropdown item link</Dropdown.Item>
                          <Dropdown.Item {...Q.DD_BTN}>Dropdown item button</Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                    </div>

                    <hr/>
                    <h6>Vertical variation</h6>
                    <div className="btn-group-vertical">
                      {[1,2].map(v => 
                        <Fragment key={v}>
                          <Btn>Button</Btn>
                          <Btn>Button</Btn>
                          <Dropdown bsPrefix="btn-group">
                            <Dropdown.Toggle>Dropdown</Dropdown.Toggle>
                            <Dropdown.Menu>
                              <Dropdown.Item onClick={e => e.preventDefault()} href="">Dropdown item link</Dropdown.Item>
                              <Dropdown.Item {...Q.DD_BTN}>Dropdown item button</Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>
                        </Fragment>
                      )}
                    </div>
                  </Card.Body>
                </Card>

                <Card className="bw-y1 rounded-0">
                  <Card.Header className="rounded-0 position-sticky t0 zi-1 bg-light">Card</Card.Header>
                  <Card.Body>
                    <Card style={{ width: '18rem' }}>
                      <Img src="holder.js/100px180" className="card-img-top" alt="Image cap" />
                      <Card.Body>
                        <Card.Title>Card Title</Card.Title>
                        <Card.Text>Some quick example text to build on the card title and make up the bulk of the card's content.</Card.Text>
                        <Btn>Go somewhere</Btn>
                      </Card.Body>
                    </Card>

                    <hr/>
                    <h6>Titles, text, and links</h6>
                    <Card style={{ width: '18rem' }}>
                      <Card.Body>
                        <Card.Title>Card Title</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>
                        <Card.Text>Some quick example text to build on the card title and make up the bulk of the card's content.</Card.Text>
                        <Card.Link onClick={e => e.preventDefault()} href="#">Card Link</Card.Link>
                        <Card.Link onClick={e => e.preventDefault()} href="#">Another Link</Card.Link>
                      </Card.Body>
                    </Card>


                  </Card.Body>
                </Card>

                {/* Carousel */}
                {/* Collapse */}

                <Card className="bw-y1 rounded-0">
                  <Card.Header className="rounded-0 position-sticky t0 zi-1 bg-light">Dropdown</Card.Header>
                  <Card.Body>

                  </Card.Body>
                </Card>

                <Card className="bw-y1 rounded-0">
                  <Card.Header className="rounded-0 position-sticky t0 zi-1 bg-light">Modal</Card.Header>
                  <Card.Body>
                    <Btn onClick={() => setShowModal(true)}>
                      Launch demo modal
                    </Btn>

                    <Modal 
                      container={frame.document.body} 
                      show={showModal} 
                      onHide={() => setShowModal(false)}
                    >
                      <Modal.Header closeButton>
                        <Modal.Title>Modal heading</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
                      <Modal.Footer>
                        <Btn onClick={() => setShowModal(false)} kind="secondary">Close</Btn>
                        <Btn onClick={() => setShowModal(false)}>Save Changes</Btn>
                      </Modal.Footer>
                    </Modal>
                  </Card.Body>
                </Card>

                
              </div>
            )
          }}
        </FrameContextConsumer>
      </Frame>
    </div>
  );
}