import React, { useState, lazy, Suspense } from 'react';// , { useState, lazy, Suspense }
// import Alert from 'react-bootstrap/Alert';
// import Badge from 'react-bootstrap/Badge';
import Dropdown from 'react-bootstrap/Dropdown';
import Card from 'react-bootstrap/Card';
// import Modal from 'react-bootstrap/Modal';
import * as Yup from 'yup';
import { useFormik } from 'formik'; // , Form

import { Resizer, ResizePanel } from '../../components/q-ui-react/Resizer';
import Flex from '../../components/q-ui-react/Flex';
import Form from '../../components/q-ui-react/Form';
// import Img from '../../components/q-ui-react/Img';
import Btn from '../../components/q-ui-react/Btn';
import Input from '../../components/q-ui-react/Input';
import { ColorPicker } from '../../components/q-ui-react/color-picker/ColorPicker';

const DemoComponents = lazy(() => import(/* webpackChunkName: "DemoComponents" */"./DemoComponents"));

const DEFAULT_SIZES = {
  lg: { s: 1.25, u: "rem" }, 
  default: { s: 1, u: "rem" }, 
  sm: { s: 0.875, u: "rem" }, 
  xs: { s: 0.5, u: "rem" }
};
const DEFAULT_GENERAL = {
  fontFamily: [
    "-apple-system", "BlinkMacSystemFont", "Segoe UI", "Roboto", "Helvetica Neue", "Arial", "Noto Sans", "Liberation Sans", "sans-serif", "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"
  ], 
  fontSize: { size: 1, unit: "rem" }, 
  fontWeight: 400, 
  lineHeight: { size: 1.5, unit: "" }, 
  color: "#212529", 
  backgroundColor: "#fff"
};
const FONT_WEIGHT = [300, 400, 500, 600, 700, 800, 900, "normal", "bold", "bolder"];
const CSS_UNITS = ["rem","em","px"];
const DEFAULT_PALETTE = { 
  primary: {
    default: "#007bff", 
    hover: "#0062cc", 
    active: "#005cbf", 
    focus: "#0062cc"
  }, 
  secondary: {
    default: "#6c757d", 
    hover: "#545b62", 
    active: "#4e555b", 
    focus: "#5a6268"
  }, 
  success: {
    default: "#28a745", 
    hover: "#1e7e34", 
    active: "#1c7430", 
    focus: "#218838"
  }, 
  danger: {
    default: "#dc3545", 
    hover: "#c82333", 
    active: "#bd2130", 
    focus: "#c82333"
  }, 
  warning: {
    default: "#ffc107", 
    hover: "#e0a800", 
    active: "#d39e00", 
    focus: "#e0a800"
  }, 
  info: {
    default: "#17a2b8", 
    hover: "#138496", 
    active: "#117a8b", 
    focus: "#138496"
  }, 
  light: {
    default: "#f8f9fa", 
    hover: "#e2e6ea", 
    active: "#dae0e5", 
    focus: "#e2e6ea"
  }, 
  dark: {
    default: "#343a40", 
    hover: "#23272b", 
    active: "#1d2124", 
    focus: "#23272b"
  }
};

// forms, 
export default function ThemeDesigner(){
  const [fontFamilyVal, setFontFamilyVal] = useState("");
  const [theme, setTheme] = useState("Q-UI Bootstrap 4");
  const [themeOptions, setThemeOptions] = useState(["New","Q-UI Bootstrap 4", "Bootstrap 4"]);
  // const [general, setGeneral] = useState(DEFAULT_GENERAL);
  // const [palette, setPalette] = useState(DEFAULT_PALETTE);

  const sizesSchema = () => {
    let obj = {};
    for(let key in DEFAULT_SIZES){
      obj[key] = Yup.object({
        s: Yup.number().positive().required("is required")
      });
    }
    return obj;
  }

	const formik = useFormik({
		enableReinitialize: true,
		initialValues: {
      ...DEFAULT_GENERAL, 
      sizes: DEFAULT_SIZES, 
      palette: DEFAULT_PALETTE, 
    }, // forms || initialValues
		validationSchema: Yup.object().shape({
      // Object.entries(DEFAULT_SIZES).map(v => ({ [v[0]]: { s: Yup.number().positive().required("is required") } }) )
      sizes: Yup.object(sizesSchema()), 
      fontFamily: Yup.array().of(Yup.string().min(1)).required("Font Family is required"),
      fontSize: Yup.object({
        size: Yup.number().min(0.5)
      }), 
      lineHeight: Yup.object({
        size: Yup.number().min(0.5)
      }), 
			// lg: Yup.number()
			// 	.min(1, "Minimum 1")
			// 	// .max(50, "Maximum 50 symbols")
			// 	.required("lg is required"), 
			// password: Yup.string()
			// 	.min(MIN_PASS, "Minimum " + MIN_PASS + " symbols") // .max(50, "Maximum 50 symbols")
			// 	.required("Password is required"),
			// password_confirm: Yup.string()
			// 	.required('Confirm Password must same')
			// 	// .test('passwords-match', 'Password must match', v => v === Yup.ref("password")),
			// 	.test('passwords-match', 'Passwords must match', function(value){
			// 		return this.parent.password === value;
			// 	}),
			// term: Yup.bool().oneOf([true], 'Terms & Conditions must be accepted')
		}), 
		onSubmit: (values, fn) => { // setSubmitting, resetForm, setStatus, 
      console.log('values: ', values);
      fn.setSubmitting(false);
    }
  });

  const onAddFontFamily = () => {
		if(fontFamilyVal.length > 0){
      const { fontFamily } = formik.values;
			const availFont = fontFamily.find(f => f === fontFamilyVal);//  || f.size === customSizeVal
			if(availFont){
				// console.log('availSize: ', availSize);
				swalToast({ icon:"error", text:"Font Family is available, please add other font" });
			}else{
        formik.setFieldValue("fontFamily", [...fontFamily, fontFamilyVal]);// setGeneral({ ...general, fontFamily: [...fontFamily, fontFamilyVal] });
				setFontFamilyVal("");
				setTimeout(() => Q.domQall("#ddFontFamily .dropdown-item")[fontFamily.length - 1]?.focus(), 9);
			}
		}else{
			swalToast({ icon:"error", text:"Please insert font name" });
		}
	}

  // const formikError = (fname, cls = "is-invalid") => formik.touched[fname] && formik.errors[fname] ? " " + cls : "";

  const onAddTheme = (e) => {
    const val = e.target.value;
    setTheme(val);
    // if(val === "New"){
    //   setThemeOptions([...themeOptions, ]);
    // }
  }

  const { sizes, fontFamily, fontSize, fontWeight, lineHeight, color, backgroundColor, palette } = formik.values;
  // console.log('formik: ', formik);

  return (
    <Resizer className="border" 
      style={{ height: 'calc(100vh - 80px)' }}  
    >
      <ResizePanel size={25} minSize={15}>
        <Form noValidate 
          className="w-100 h-100 ovyauto q-scroll set-style" 
          disabled={formik.isSubmitting} 
          onSubmit={formik.handleSubmit} 
          onReset={formik.handleReset} 
        >
          <div className="bg-strip p-1 position-sticky t0 zi-1021 shadow-sm border-bottom">
            <Btn size="sm" kind="dark" type="reset">Reset</Btn>{" "}
            <Btn size="sm" type="submit">Save</Btn>
          </div>

          <div className="p-2 mt-3-next">
            <Card>
              <Card.Header className="py-1 px-2">Sizes</Card.Header>
              <Card.Body className="p-2 mt-2-next">
                {Object.entries(sizes).map((v, i) => 
                  <div key={i}>
                    <small className="text-capitalize">{v[0]}</small>
                    <div className="input-group input-group-sm">
                      <Input type="number" step={0.01} min={0.5} 
                        // className={formikError("s")} 
                        className={v[1].s < 0.5 ? "is-invalid" : undefined} 
                        // name="sizes.lg" 
                        // name="sizes" // {`sizes[${i}].s`} // 
                        value={v[1].s} 
                        // sizes.map((v2, i2) => (i === i2 ? { ...v2, s: e.target.value } : v2))
                        onChange={e => formik.setFieldValue("sizes", { ...sizes, [v[0]]: { ...v[1], s: Number(e.target.value) } })} 
                      />
                      <select className="custom-select w-auto flexno" 
                        // name="size_unit" 
                        value={v[1].u} 
                        // sizes.map((v2, i2) => (i === i2 ? { ...v2, u: e.target.value } : v2))
                        onChange={e => formik.setFieldValue("sizes", { ...sizes, [v[0]]: { ...v[1], u: e.target.value } })} 
                      >
                        {CSS_UNITS.map(v => <option key={v} value={v}>{v}</option>)}
                      </select>
                    </div>
                  </div>
                )}
              </Card.Body>
            </Card>

            <div className="mt-2-next">
              <h6>General</h6>
              <div>
                <label htmlFor="fontFamily" className="mb-0 small">Font Family</label>
                <Dropdown>
                  <Dropdown.Toggle block size="sm" variant="light" id="fontFamily" bsPrefix="text-left ws-normal">{fontFamily.join(", ")}</Dropdown.Toggle>
                  <Dropdown.Menu className="py-0 w-100" id="ddFontFamily">
                    <div className="input-group input-group-sm p-2 bg-white shadow-sm">
                      <Input // id="icustomIconSize" 
                        placeholder="Font name" 
                        value={fontFamilyVal} 
                        onChange={e => setFontFamilyVal(e.target.value)} 
                      />
                      <div className="input-group-append">
                        <Btn blur onClick={onAddFontFamily} className="qi qi-plus" title="Add size" />
                      </div>
                    </div>

                    <div className="mxh-50vh ovyauto q-scroll py-2">
                      {fontFamily.map((v, i) => // cauto
                        <button key={i} className="dropdown-item d-flex small px-2" type="button">
                          {v} 
                          {v !== "sans-serif" && 
                            <Btn As="div" size="xs" kind="light" className="ml-auto qi qi-close xx" title="Remove" 
                              onClick={() => formik.setFieldValue("fontFamily", fontFamily.filter((f) => f !== v))} // () => setGeneral({ ...general, fontFamily: general.fontFamily.filter((f) => f !== v)})
                            />
                          }
                        </button>
                      )}
                    </div>
                    
                  </Dropdown.Menu>
                </Dropdown>
              </div>

              <div>
                <small>Font Size</small>
                <div className="input-group input-group-sm">
                  <Input type="number" step={0.01} min={0.5} 
                    className={fontSize.size < 0.5 ? "is-invalid" : undefined} 
                    value={fontSize.size} 
                    onChange={e => formik.setFieldValue("fontSize", { ...fontSize, size: Number(e.target.value) })} // setGeneral({ ...general, fontSize: { ...general.fontSize, size: e.target.value } })
                  />
                  <select className="custom-select w-auto flexno"
                    value={fontSize.unit} 
                    // setGeneral({ ...general, fontSize: { ...general.fontSize, unit: e.target.value } })
                    onChange={e => formik.setFieldValue("fontSize", { ...fontSize, unit: e.target.value })} 
                  >
                    {CSS_UNITS.map(v => <option key={v} value={v}>{v}</option>)}
                  </select>
                </div>
              </div>

              <div>
                <label htmlFor="fontWeight" className="mb-0 small">Font Weight</label>
                <select id="fontWeight" className="custom-select"
                  value={fontWeight} 
                  onChange={e => {
                    const val = e.target.value;
                    formik.setFieldValue("fontWeight", isNaN(val) ? val : Number(val));
                  }} // setGeneral({ ...general, fontWeight: e.target.value })
                >
                  {FONT_WEIGHT.map(v => <option key={v} value={v}>{v}</option>)}
                </select>
              </div>

              <div>
                <small>Line Height</small>
                <div className="input-group input-group-sm">
                  <Input type="number" step={0.01} min={0.5} 
                    className={lineHeight.size < 0.5 ? "is-invalid" : undefined} 
                    value={lineHeight.size} 
                    // setGeneral({ ...general, lineHeight: { ...general.lineHeight, size: e.target.value } })
                    onChange={e => formik.setFieldValue("lineHeight", { ...lineHeight, size: Number(e.target.value) })} 
                  />
                  <select className="custom-select w-auto flexno"
                    value={lineHeight.unit} 
                    // setGeneral({ ...general, lineHeight: { ...general.lineHeight, unit: e.target.value } })
                    onChange={e => formik.setFieldValue("lineHeight", { ...lineHeight, unit: e.target.value })} 
                  >
                    <option value=""></option>
                    {CSS_UNITS.map(v => <option key={v} value={v}>{v}</option>)}
                  </select>
                </div>
              </div>

              <div>
                <small>Color</small>
                <ColorPicker 
                  toggleClass="input-group-sm" 
                  inputProps={{
                    className: "text-monospace"
                  }} 
                  // dropdownMenuProps={{
                  //   // className: "w-100"
                  //   popperConfig: {
                  //     strategy: "fixed"
                  //   }
                  // }} 
                  value={color} 
                  onChange={val => formik.setFieldValue("color", val)} // setGeneral({ ...general, color })
                />
              </div>

              <div>
                <small>Background Color</small>
                <ColorPicker 
                  toggleClass="input-group-sm" 
                  inputProps={{
                    className: "text-monospace"
                  }} 
                  value={backgroundColor} 
                  onChange={val => formik.setFieldValue("backgroundColor", val)} // setGeneral({ ...general, backgroundColor: val })
                />
              </div>
            </div>

            <div className="mt-2-next">
              <h6>Palette</h6>
              {Object.entries(palette).map((v, i) => 
                <Card key={i}>
                  <Card.Header className="py-1 px-2">{v[0]}</Card.Header>
                  <Card.Body className="p-2 mt-2-next">
                    {Object.entries(v[1]).map((v2, i2) => 
                      <div key={i2}>
                        <small>{v2[0]}</small>
                        <ColorPicker 
                          toggleClass="input-group-sm" 
                          inputProps={{
                            className: "text-monospace"
                          }} 
                          value={v2[1]} 
                          onChange={val => formik.setFieldValue("palette", { ...palette, [v[0]]: { ...v[1], [v2[0]]: val } })} // setPalette({ ...palette, [v[0]]: { ...v[1], [v2[0]]: val } })
                        />
                      </div>
                    )}

                  </Card.Body>
                </Card>
              )}
              
            </div>
          </div>
        </Form>
      </ResizePanel>

      <ResizePanel size={75} minSize={60} 
        // className="h-100 ovyauto p-3 view-components" // view-app
      >
        <Flex dir="column" className="w-100 view-components">
          <Flex align="center" className="flexno bg-strip p-1 border-bottom">
            <label htmlFor="themes" className="mb-0 mr-1">Theme :</label>
            <select className="custom-select custom-select-sm w-auto" id="themes"
              value={theme} // setThemeOptions
              onChange={onAddTheme}
            >
              {themeOptions.map((v, i) => <option key={i} value={v}>{v}</option>)}
            </select>
          </Flex>

          <Suspense fallback={<div>LOADING</div>}>
            <DemoComponents 
              fontFamily={fontFamily} 
              fontSize={fontSize} 
              fontWeight={fontWeight} 
              lineHeight={lineHeight} 
              color={color} 
              backgroundColor={backgroundColor}  
              palette={palette} 
            />
          </Suspense>
        </Flex>
      </ResizePanel>
    </Resizer>
  );
}

/*
    <Flex dir="row" className="theme-designer">
      <Flex className="flexno w-350px border-right">
        Aside
      </Flex>

      <Flex grow={1}>

      </Flex>
    </Flex>
*/