import React from "react";
import { useNode } from '@craftjs/core';

import Btn from '../../components/q-ui-react/Btn';

export const Button = ({ ...etc }) => {
	const { connectors: { connect, drag } } = useNode();
	return (
		<Btn {...etc} 
			ref={ref=> connect(drag(ref))}
		/>
	)
}

Button.craft = {
  // ...
  rules: {
    canDrag: (node) => node.data.props.draggable
  }
}


