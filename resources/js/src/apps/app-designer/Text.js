import React, { useState } from 'react';// useRef, 
import { useNode } from '@craftjs/core';// , Element
// import Dropdown from 'react-bootstrap/Dropdown';
// import { UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';// UncontrolledPopover | Popover

import Btn from '../../components/q-ui-react/Btn';

export const Text = ({ 	
	As = "p", // As
	tabIndex = 0, // -1
	// id, 
	onDelete = Q.noop, 
	children, 
  // text, 
  ...etc
}) => {
  const { connectors: { connect, drag } } = useNode();
	// const ID = id ? id : Q.Qid();
	
	const [active, setActive] = useState(false);
	
	const onFocus = (e) => {
		// console.log('onFocus e.target: ', e.target);
		e.stopPropagation();
		
		setActive(true);
		// console.log('onFocus active: ', active);
	}
	
	const onBlur = () => {
		setActive(false);
	}
	
  return (
		<As 
			{...etc} 
			ref={ref => connect(drag(ref))} 
			tabIndex={tabIndex} 
			// className={Q.Cx("ad-el", { "active": active })} 
			className="ad-el" 
			onClick={onFocus} 
			// onClick={onFocus} 
		>
			<span 
				// id={ID} 
				
			>
				{children} 
			</span>
			
			<span className="btn-group btn-group-sm shadow-sm ad-tool"
				onClick={e => e.stopPropagation()} // onClick
			>
				<Btn kind="light" className="qi qi-edit" />
				<Btn kind="light" className="qi qi-trash" 
					onClick={onDelete} 
				/>
			</span>
		</As>
  )
}

Text.craft = {
  // ...
  rules: {
    canDrag: (node) => node.data.props.draggable // node.data.props.text !== "Drag"
  }
}

/*
				<UncontrolledPopover 
					// isOpen={active} 
					// toggle={() => setActive(!active)} 
					// toggle={() => console.log('toggle')}
					trigger="legacy" 
					placement="bottom" 
					target={ID} 
					className={"ad-pop-" + ID} 
					// popperClassName="popperClassName" 
					// innerClassName="innerClassName" 
				>
					<PopoverHeader>Edit</PopoverHeader>
					<PopoverBody>
						Legacy is a reactstrap special trigger value (outside of bootstrap's spec/standard). 
						Before reactstrap correctly supported click and focus, 
						it had a hybrid which was very useful and has been brought back as trigger="legacy". 
						One advantage of the legacy trigger is that it allows the popover text to be selected while also closing when clicking outside the triggering element and popover itself.
					</PopoverBody>
				</UncontrolledPopover>

				<Dropdown 
					as="span" 
					className="ad-dd-tool"
				>
					<Dropdown.Toggle as="span">
						Edit
					</Dropdown.Toggle>
					<Dropdown.Menu
						as="span" 
					>
						<Dropdown.Item {...Q.DD_BTN}>Action</Dropdown.Item>
						<Dropdown.Item {...Q.DD_BTN}>Another</Dropdown.Item>
					</Dropdown.Menu>
				</Dropdown>
*/

