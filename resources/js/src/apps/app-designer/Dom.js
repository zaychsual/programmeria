import React from "react";
import { useNode } from '@craftjs/core';

// import Btn from '../../components/q-ui-react/Btn';

export const Dom = ({ 
	As = "div", 
	...etc 
}) => {
	const { connectors: { connect, drag } } = useNode();
	return (
		<As {...etc} 
			ref={ref=> connect(drag(ref))}
		/>
	)
}

Dom.craft = {
  // ...
  rules: {
    canDrag: (node) => node.data.props.draggable
  }
}


