import React, { useState } from 'react';// , { useEffect }
// import { InputNumber } from 'primereact/inputnumber';
 
import Flex from '../../components/q-ui-react/Flex';
import Input from '../../components/q-ui-react/Input';
import Btn from '../../components/q-ui-react/Btn';
import { cssUnitConverter } from '../../utils/css/cssUnitConverter';

export default function CssUnitConverter({
  type = "px2rem", // px2rem | rem2px | 
}){
  const [converTo, setConverTo] = useState(type);
  const [title, setTitle] = useState({ v1: "PX", v2: "REM" });
  const [val1, setVal1] = useState("");// 0
  const [val2, setVal2] = useState("");

  const onSetConvert = (e) => {
    const val = e || converTo;

    let ty, t;
    switch(val){
      case "px2rem":
        ty = "rem2px";
        t = { v1:"REM", v2:"PX" };// "REM to PX"
        break;
      case "px2cm":
        ty = "px2cm";
        t = { v1:"PX", v2:"CM" };// "REM to PX"
        break;
      default:
        ty = "px2rem";
        t = { v1:"PX", v2:"REM" };// "PX to REM";
        break;
    }

    setConverTo(ty);
    setTitle(t);
  }

  const convert = (val, input) => {
    if(val.length < 1){
      return "";
    }

    const vFloat = parseFloat(val);

    let res;
    switch(converTo){
      case "px2rem": // px2rem | rem2px
        res = input === 1 ? (vFloat / 16).toFixed(2) : parseFloat(vFloat * 16);
        break;
      case "px2cm":
        res = cssUnitConverter(parseFloat(val), 'px', 'cm');
        break;
      default:
        res = input === 2 ? parseFloat(vFloat * 16) : (vFloat / 16).toFixed(2);
        break;
    }

    return res;
  }

  const onChange = (e, input) => {
    const val = e.target.value;
    if(val === ""){ // val === "" || val === "e" | val.includes("e")
      e.preventDefault();
      return;
    }

    let res = convert(val, input);// Number(convert(val))
    if(input === 1){
      // res = convert(val);
      setVal1(val);
      setVal2(res);
    }else{
      setVal2(val);
      setVal1(res);
    }

    console.log('val: ', val);
    console.log('res: ', res);
  }

  return (
    <div className="container-fluid q-css-unit-converter">
      <Flex As="h4">
        {title.v1} to {title.v2}

        <select className="custom-select custom-select-sm w-auto ml-auto"
          value={converTo} 
          onChange={e => onSetConvert(e.target.value)} 
        >
          {[
            { value: "px2rem", t:"PX to REM" }, 
            { value: "rem2px", t:"REM to PX" },
            { value: "px2cm", t:"PX to CM" },
          ].map(v => 
            <option key={v.value} value={v.value}>{v.t}</option>
          )}
        </select>
      </Flex>

      <div className="form-row">
        <Flex As="label" className="col mb-0">
          <Input type="number" className="round-right-0 zi-1" 
            // inputMode="numeric" 
            value={val1} 
            onChange={e => onChange(e, 1)} 
          />
          <Btn As="div" kind="light" className="round-left-0 ml-n1px cauto text-lowercase">{title.v1}</Btn>
        </Flex>

        <Btn onClick={() => onSetConvert()} kind="flat" className="flexno qi qi-arrows-h" />
        
        <Flex As="label" className="col mb-0">
          <Input type="number" className="round-right-0 zi-1" 
            // inputMode="numeric" 
            value={val2} 
            onChange={e => onChange(e, 2)} 
          />
          <Btn As="div" kind="light" className="round-left-0 ml-n1px cauto text-lowercase">{title.v2}</Btn>
        </Flex>
      </div>
    </div>
  );
}

/*
          <InputNumber 
            className="w-100" 
            inputClassName="text-lowercase" 
            showButtons 
            mode="decimal" 
            // min={0} 
            decrementButtonClassName="p-button-outlined p-button-secondary" // 
            incrementButtonClassName="p-button-outlined p-button-secondary" 
            // suffix={" " + title.v1} 
            value={val1} 
            onValueChange={(e) => onChange(e.value, 1)} 
          />

          <InputNumber 
            className="w-100" 
            inputClassName="text-lowercase" 
            showButtons 
            mode="decimal" 
            // min={0} 
            decrementButtonClassName="p-button-outlined p-button-secondary" 
            incrementButtonClassName="p-button-outlined p-button-secondary" 
            // suffix={" " + title.v2} 
            // spellCheck={false} 
            value={val2} 
            onValueChange={(e) => onChange(e.value, 2)} 
          />
*/