import React, { useState, useEffect } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import QRCode from "react-qr-code";

import Form from "../../components/q-ui-react/Form";
import Input from "../../components/q-ui-react/Input";
import Btn from "../../components/q-ui-react/Btn";

export default function Pos({
  className, 

}){
  const [loadData, setLoadData] = useState(true);
  const [formData, setFormData] = useState({
    product: "", 
    qty: "", 

  });

  useEffect(() => {
    console.log('%cuseEffect in Pos', 'color:yellow');
    setTimeout(() => { // DUMMY Xhr
      setLoadData(false);
      // setFormData(); // Store Edit data here
    }, 2000);
  }, []);

  const formik = useFormik({
    enableReinitialize: true, 
    initialValues: formData, 
    validationSchema: Yup.object({
      product: Yup.string().required("Is Required"), 
      qty: Yup.number().required("Is Required"), 
    }),
    onSubmit: (values, fn) => {
      console.log("values: ", values);
      setTimeout(() => { // DUMMY Xhr
        fn.setSubmitting(false);
      }, 2000);
    }
  });

  const { handleSubmit, handleReset, handleChange, isSubmitting, values, errors, touched } = formik;

  const renderInvalidMsg = (key) => (errors[key] && touched[key]) && <div className="invalid-feedback">{errors[key]}</div>;

  return (
    <div className={Q.Cx("card pos", className)}>
      <div className="card-header">
        Point Of Sale
      </div>

      <div className="card-body">
        <div className="row no-gutters">
          <div className="col-md-6">
            <h6>Products</h6>
            <QRCode 
              value="Programmeria" 
              // bgColor="red" 
              // fgColor="" 
              // level="" // DEFAULT = "L" | string ('L' 'M' 'Q' 'H')
              // size={256} // DEFAULT = 256 | Number 
              // title="" 
            />
          </div>

          <div className="col-md-6">
            {/* <h6>Form Pos</h6> */}
            <Form
              noValidate 
              disabled={loadData || isSubmitting} 
              fieldsetClass="mt-3-next" 
              onSubmit={handleSubmit} 
              onReset={handleReset}
            >
              <div>
                <label htmlFor="product">Product</label>
                <Input 
                  id="product" 
                  className={Q.formikValidClass(formik, "product")} 
                  value={values.product} 
                  onChange={handleChange} 
                />
                {renderInvalidMsg("product")} 
                {/* {(errors.product && touched.product) && 
                  <div className="invalid-feedback">{errors.product}</div>
                } */}
              </div>

              <div>
                <label htmlFor="qty">Quantity</label>
                <Input 
                  id="qty" 
                  type="number" 
                  min={1} 
                  className={Q.formikValidClass(formik, "qty")} 
                  value={values.qty} 
                  onChange={handleChange} 
                />
                {renderInvalidMsg("qty")} 
              </div>

              <div className="text-center">
                <Btn type="reset" kind="light">Reset</Btn>{" "}
                <Btn type="submit">Save</Btn>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
