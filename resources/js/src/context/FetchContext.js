import React, { createContext, useContext } from 'react';
import axios from 'axios';

import { AuthContext } from './AuthContext';

const FetchContext = createContext();
const { Provider } = FetchContext;

const FetchProvider = ({ children }) => {
  const authContext = useContext(AuthContext);

  const authAxios = axios.create({
    // baseURL: window.location.origin + "/" // process.env.REACT_APP_API_URL
    baseURL: Q.baseURL
  });

  authAxios.interceptors.request.use(
    config => {
      config.headers.Authorization = `Bearer ${authContext.authState.token}`;
      return config;
    },
    err => {
      return Promise.reject(err);
    }
  );

  authAxios.interceptors.response.use(
    res => {
      return res;
    },
    err => {
      const code = err && err.response ? err.response.status : 0;
      if (code === 401 || code === 403) {
        console.log('Error code: ', code);
      }
      return Promise.reject(err);
    }
  );

  return (
    <Provider
      value={{
        authAxios
      }}
    >
      {children}
    </Provider>
  );
};

export { FetchContext, FetchProvider };
