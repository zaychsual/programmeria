import React, { createContext, useState } from 'react';// , useEffect

const AppConfigContext = createContext();
// const { Provider } = AppConfigContext;

const INIT_CONFIG = {
  // identity: ["email"], 
  // passwordMinLength: 6, 
  // confirm: "email", 

  lang: "en", 
  currency: "USD", 
  theme: {
    bg: "main", // // main, light, dark 
    hex: "#e3f2fd", 
    // text: "", 
  }, 
};

const AppConfigProvider = ({ children }) => {
	// const theme = localStorage.getItem("theme");
  const [config, setConfig] = useState(INIT_CONFIG);// {}

  // useEffect(() => {
  //   let token = localStorage.getItem("t");
  //   if(token){
  //     setConfig({ ...config, token });
  //   }
  // }, []);
	
	const setData = (val) => { // { theme, view, data }
		// localStorage.setItem("theme", theme);
    if(val){
      setConfig(val);
      if(val.lang){ // OPTION
        document.documentElement.lang = val.lang;
      }

      if(val.theme){ // OPTION
        Q.domQall('meta[name="theme-color"],meta[name="msapplication-TileColor"]').forEach(n => {
          n.content = val.theme.hex;// bg
        })
      }
    }
  }
	
  return (
    <AppConfigContext.Provider
      value={{
        config, 
				setAppConfig: val => setData(val), 
      }}
    >
      {children}
    </AppConfigContext.Provider>
  );
};

export { AppConfigContext, AppConfigProvider };
