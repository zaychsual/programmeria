import React from 'react';

const useFavicon = (path , type = 'image/x-icon') => {
  React.useEffect(() => {
    const link = document.querySelector("link[rel*='icon']") || document.createElement('link');
    link.rel = 'shortcut icon';
    link.type = type;
    link.href = path;
		// querySelector('head')
    document.head.appendChild(link);
  }, [path, type]);
};

export default useFavicon;