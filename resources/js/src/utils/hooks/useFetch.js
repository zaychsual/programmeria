import { useState, useEffect } from 'react';// React, { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

export function useFetch(
	fn = Q.noop, 
	// onSuccess, 
	onError = Q.noop
){
  const [load, setLoad] = useState(false);
	
	useEffect(() => {
		// console.log('%cuseEffect in ComponentName','color:yellow;');
		const CancelToken = axios.CancelToken;
		const source = CancelToken.source();
		
		(async () => {
			try{
				await fn(source.token);
				
				setLoad(true);
			}catch(e){
				if(axios.isCancel(e)){
					console.log('Request canceled e: ', e);
					onError({ type: "abort", e });
				}else{
					// console.log('handle error e: ', e);
					onError({ type: "error", e });
				}
			}
		})();
		
		return () => {
			source.cancel('Operation canceled by the user.');// cancel the request (the message parameter is optional)
		}
	}, []);
	
	return load;
}


