// https://github.com/primefaces/primereact/blob/master/src/components/utils/UniqueComponentId.js
let lastId = 0;

// prefix = 'pr_id_'
export default function(prefix = 'qid_'){
  lastId++;
  return prefix + lastId;// `${prefix}${lastId}`
}

