// import Import from '../../component-etc/route/Import';

// == Copy ==
async function Copy(target, {
	onOk = () => {},
  onErr = () => {}
} = {}){
	if(navigator.clipboard){
		if(!target){
			console.warn('Text to copy or target DOM is required.');
			return;
		}

		let txt;
		if(typeof target === 'string' && !target.tagName) txt = target;
		else txt = target.textContent; // target.innerText.trim()

		// console.log(txt.replace(/\n/gm, ''));
		
		navigator.clipboard.writeText(txt)
			.then(() => onOk(txt))
			.catch(err => onErr(err));
	}
	else{
		// const m = await Import('/storage/esm/copySelection.js');
		// console.log(m);
		console.log('%cnavigator.clipboard NOT SUPPORT','color:yellow');
	}
}

// For npm
export { Copy };
