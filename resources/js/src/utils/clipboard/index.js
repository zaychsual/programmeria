// Copy to clipboard:
function clipboardCopy(target){
	return new Promise((resolve, reject) => {
		let clipboard = navigator.clipboard,
				txt;
		
		if(typeof target === 'string' && !target.tagName) txt = target;
		else if(target?.tagName) txt = target.textContent; // target.innerText
		else reject(new Error('Text or target DOM to copy is required.'));

		if(clipboard){
			clipboard.writeText(txt).then(() => resolve(txt)).catch(e => reject(e));
		}
		else{
			console.log('%cnavigator.clipboard NOT SUPPORT','color:yellow');
			copyFn(txt, {
				onOk(){
					resolve(txt);
				},
				onErr(e){
					reject(e);
				}
			});
		}
	});
}

function copyFn(str, {
	onOk = () => {}, 
	onErr = () => {} 
}){
	let DOC = document,
			el = DOC.createElement("textarea"),
			iOS = window.navigator.userAgent.match(/ipad|iphone/i);
	
	el.className = "sr-only sr-only-focusable";
  el.contentEditable = true; // needed for iOS >= 10
  el.readOnly = false; // needed for iOS >= 10
  el.value = str;
  // el.style.border = "0";
  // el.style.padding = "0";
  // el.style.margin = "0";
  // el.style.position = "absolute";
	
  // sets vertical scroll
  // let yPosition = window.pageYOffset || DOC.documentElement.scrollTop;
  // el.style.top = `${yPosition}px`;

  DOC.body.appendChild(el);
	
	if(iOS){
		let range = DOC.createRange();
		range.selectNodeContents(el);
		
		let selection = window.getSelection();
		selection.removeAllRanges();
		selection.addRange(range);
		el.setSelectionRange(0, 999999);
	}else{
		el.focus({preventScroll:false});
		el.select();
	}
	
	let OK = DOC.execCommand("copy");
	if(OK){
		onOk(str);
	}else{
		onErr('Failed to copy');
	}
	
	el.remove();// DOC.body.removeChild(el);
}

// 
// const pasteBlob = async (src) => {
// 	try{
// 		// const imgURL = '/images/generic/file.png';
// 		const data = await fetch(src);
// 		const blob = await data.blob();
// 		await navigator.clipboard.write([
// 			new ClipboardItem({
// 				[blob.type]: blob
// 			})
// 		]);
// 		console.log('Image copied.');
// 	}catch(e){
// 		console.error(e.name, e.message);
// 	}
// }

// async function pasteBlob(){
//   try{
// 		if(!navigator.clipboard.read) return false;

// 		const clipboardItems = await navigator.clipboard.read();
// 		// console.log('clipboardItems: ', clipboardItems);

// 		let data = false;

//     for(const item of clipboardItems){
//       for(const type of item.types){
// 				const blob = await item.getType(type);
// 				// console.log('blob: ', blob);

// 				if(blob.type.startsWith("image/")){
// 					// const objURL = window.URL.createObjectURL(blob);
// 					// console.log('objURL: ', objURL);
// 					data = blob;
// 				}
//       }
// 		}
		
// 		return data;
//   }catch(e){
// 		console.error(e.name, e.message);
// 		return false;
//   }
// }

async function pasteBlob(e){
	try{
		//  === false
		if(!e.clipboardData){
			return;
		}

		let items = e.clipboardData.items;
		if(!items){// items === undefined
			return;
		}

		let lng = items.length;
		for(let i = 0; i < lng; i++){
			// Skip content if not image
			if(items[i].type.indexOf("image/") === -1) continue;
			// Retrieve image on clipboard as blob
			// let blob = items[i].getAsFile();
			return items[i].getAsFile();
		}
  }catch(e){
		console.log('Error pasteBlob e: ', e);
		return false;
  }
}

// CHECK Permisson paste
// clipboard-write - granted by default
async function permissions(){
	try {
		const query = await navigator.permissions.query({ name: "clipboard-read", allowWithoutGesture: false });
		// Will be 'granted', 'denied' or 'prompt':
		console.log(query.state);
		
		// Listen for changes to the permission state
		query.onchange = () => {
			console.log(query.state);
		};

		return query;
	}catch(e){
		return false;
	}
}

export { clipboardCopy, copyFn, pasteBlob, permissions };
