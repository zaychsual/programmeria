// == FileReader API ==
export function fileReaderApi(file, {
	readAs = "DataURL", // ArrayBuffer | BinaryString | DataURL | Text
  // onLoadStart = Q.noop,
  onProgress, //  = Q.noop
  onLoad, //  = Q.noop
  // onLoadedEnd = Q.noop,
  // onAbort = Q.noop,
  // onError = Q.noop
} = {}){
	if(file){
		const reader = new FileReader();
		// let CHUNK_SIZE = 10 * 1024;
		// let startTime, endTime;
		// let reverse = false;
		
		// 1.161.111.475

	// FileReader.EMPTY | FileReader.DONE | FileReader.LOADING
		return new Promise((resolve, reject) => {
			/* reader.onerror = (e) => {
				reader.abort();
				// onError(e, reader.readyState);
				reject(e);// new DOMException("Problem parsing file.")
			}; */
			reader.addEventListener('error', e => {
				reader.abort();
				reject(e);
			});
			
			// reader.onloadstart = (e) => { // event.type
				// onLoadStart(e, reader.readyState);
			// };
			
			// reader.onprogress = (e) => { // event.type
				// onProgress(e, reader.readyState);
			// };
			if(onProgress){
				reader.addEventListener('progress', onProgress);
			}
			
			reader.addEventListener('load', (e) => {
				if(onLoad) onLoad(reader, e);
				
				resolve({
					file: file,
					result: reader.result
				});
			});

			/* reader.onload = () => {
				// console.log('onload: ', reader.readyState);
				// let buffer = new Uint8Array(reader.result);
				// let timeReg = /\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}/;
				// let bufLength = buffer.length;
				// for (let i = reverse ? bufLength - 1 : 0; reverse ? i > -1 : i < bufLength; reverse ? i-- : i++) {
					// if (buffer[i] === 10) {
						// let snippet = new TextDecoder('utf-8').decode(buffer.slice(i + 1, i + 20));
						// if (timeReg.exec(snippet)) {
							// if (!reverse) {
								// startTime = snippet;
								// reverse = true;
								// seek();
							// } else {
								// endTime = snippet;
								// console.log(`Log time range: ${startTime} ~ ${endTime}`);
							// }
							// break;
						// }
					// }
				// }
				
				resolve({
					file: file,
					result: reader.result
				});// ...data
			}; */
			
			// reader.onloadend = (e) => { // event.type
				// onLoadedEnd(e, reader.readyState);
			// };
			
			/* reader.onabort = (e) => { // event.type
				// onAbort(e, reader.readyState);
				console.log(reader.error.message);
				reject(e, reader.error);
			}; */
			reader.addEventListener('abort', () => {
				reject(e, reader.error);
			});
			
			// seek();
			// function seek(){
				// let start = reverse ? file.size - CHUNK_SIZE : 0;
				// let end = reverse ? file.size : CHUNK_SIZE;
				// let slice = file.slice(start, end);
				// reader["readAs" + readAs](slice);// fr.readAsArrayBuffer(slice);
			// }
			
			reader["readAs" + readAs](file);
		});
	}
}

