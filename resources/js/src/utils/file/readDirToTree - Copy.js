// === Read Directory Browser API === 
function filePromise(entry){
  return new Promise((resolve, reject) => entry.file(resolve, reject));
}

function traverseDirectory(entry){
  let reader = entry.createReader();

  // Resolved when the entire directory is traversed
  return new Promise((resolveDir) => {
    let iterationAttempts = [];
    (function read_entries(){
      // According to the FileSystem API spec, readEntries() must be called until
      // it calls the callback with an empty array.  Seriously??
      reader.readEntries((entries) => {
        if(!entries.length){
          // Done iterating this particular directory
          resolveDir(Promise.all(iterationAttempts));
        }else{
          // Add a list of promises for each directory entry.  If the entry is itself
          // a directory, then that promise won't resolve until it is fully traversed.
          iterationAttempts.push(Promise.all(entries.map(async (entry) => {
            if(entry.isFile){
              // console.log('entry: ', entry);
              let getFile = await filePromise(entry).then(v => v);// console.log(v)

              // entry.file((file) => {
              //   return {
              //     path: '/' + entry.name,
              //     type: "file",
              //     // contentType: getFile.type,
              //     // integrity: "sha384-587fmomseJLRYDx7qDgCb06KZbMzfwfciFVnsTI64sEqecGBQVHIMIm3ZUMzNOwl",
              //     // lastModified: "Sat, 26 Oct 1985 08:15:00 GMT",
              //     size: file.size
              //   };
              // });
              // console.log('getFile: ', getFile);

              return {
                path: '/' + entry.name,
                type: "file",
                // contentType: getFile.type,
                // integrity: "sha384-587fmomseJLRYDx7qDgCb06KZbMzfwfciFVnsTI64sEqecGBQVHIMIm3ZUMzNOwl",
                // lastModified: "Sat, 26 Oct 1985 08:15:00 GMT",
                // size: getFile.size
                file: getFile // entry
              };
            }else{
              let loop = await traverseDirectory(entry);
              return {
                path: entry.fullPath,
                type: "directory",
                files: loop[0]
              };
            }
          })));
          read_entries();// Try calling readEntries() again for the same dir, according to spec
        }
      }, errorHandler);
    })();
  });
}

function errorHandler(e){
  console.log(e);
}


// USAGE:
/* dropzone.addEventListener("dragover",function(event){
  event.preventDefault();
}, false);

dropzone.addEventListener("drop",async function(event){
  let items = event.dataTransfer.items;

  event.preventDefault();// listing.innerHTML = "";

  for(let i=0; i<items.length; i++){
    let item = items[i].webkitGetAsEntry();

    if(item){
      // scanFiles(item, listing);
      let res = await traverseDirectory(item);
      console.log(res);
    }
  }

  // traverseDirectory(items).then(() => {
  //   // AT THIS POINT THE DIRECTORY SHOULD BE FULLY TRAVERSED.
  //   console.log();
  // });
}, false); */