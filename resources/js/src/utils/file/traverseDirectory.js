// import {toFileWithPath} from '../dropzone/file';

// https://stackoverflow.com/questions/18815197/javascript-file-dropping-and-reading-directories-asynchronous-recursion

// === Read Directory Browser API === 
function fileEntry(entry){
  return new Promise((resolve, reject) => entry.file(resolve, reject));
}

function traverseDirectory(entry){
  let reader = entry.createReader();

	function errorHandler(e){
		console.log(e);
	}	
	
// Resolved when the entire directory is traversed
  return new Promise((resolveDir) => {
    let iterationAttempts = [];
    (function ReadEntries(){
      // According to the FileSystem API spec, readEntries() must be called until
      // it calls the callback with an empty array. Seriously??
      reader.readEntries((entries) => {
        if(!entries.length){
          resolveDir(Promise.all(iterationAttempts));// Done iterating this particular directory
        }else{
          // Add a list of promises for each directory entry.  If the entry is itself
          // a directory, then that promise won't resolve until it is fully traversed.
          iterationAttempts.push(Promise.all(entries.map(async (entry) => {
            if(entry.isFile){
              let getFile = await fileEntry(entry).then(v => v);

							// let parseEntry = toFileWithPath(getFile, entry.fullPath);
							// let getAll = {...parseEntry, isFile: entry.isFile, isDirectory: entry.isDirectory};
              // console.log('parseEntry: ', parseEntry);

              return {
								type: "file",
								isFile: entry.isFile, 
								isDirectory: entry.isDirectory,
                path: '/' + entry.name,
								fullPath: entry.fullPath,
								name: entry.name,
                // contentType: getFile.type,
                // integrity: "sha384-587fmomseJLRYDx7qDgCb06KZbMzfwfciFVnsTI64sEqecGBQVHIMIm3ZUMzNOwl",
                // lastModified: "Sat, 26 Oct 1985 08:15:00 GMT",
                // size: getFile.size
                file: getFile // entry
              };
            }else{
              let loop = await traverseDirectory(entry);
              return {
								isFile: entry.isFile, 
								isDirectory: entry.isDirectory,
                path: entry.fullPath,
                type: "directory",
                files: loop[0]
              };
            }
          })));
          ReadEntries();// Try calling readEntries() again for the same dir, according to spec
        }
      }, errorHandler);
    })();
  });
}

export { traverseDirectory, fileEntry };


// USAGE:
/* dropzone.addEventListener("dragover",function(event){
  event.preventDefault();
}, false);

dropzone.addEventListener("drop",async function(event){
  let items = event.dataTransfer.items;

  event.preventDefault();// listing.innerHTML = "";

  for(let i=0; i<items.length; i++){
    let item = items[i].webkitGetAsEntry();

    if(item){
      // scanFiles(item, listing);
      let res = await traverseDirectory(item);
      console.log(res);
    }
  }

  // traverseDirectory(items).then(() => {
  //   // AT THIS POINT THE DIRECTORY SHOULD BE FULLY TRAVERSED.
  //   console.log();
  // });
}, false); */