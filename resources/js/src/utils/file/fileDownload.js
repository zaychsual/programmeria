// === File Download ====
// https://github.com/kennethjiang/js-file-download/blob/master/file-download.js

// import { isStr, setAttr } from '../Q';// ../../utils

export default function fileDownload(data, filename, mime, bom){
	if(data){
		let A = Q.makeEl("a");// document.createElement('a');

		function setLink(url){
			Q.setAttr(A, {
				// display: "none", 
				// id: "FUCKYOU", 
				hidden: "", 
				href: url, 
				download: filename || ""
			});
			// A.style.display = 'none';
			// A.href = blobURL;
			// A.setAttribute('download', filename);

			// Safari thinks _blank anchor are pop ups. We only want to set _blank
			// target if the browser does not support the HTML5 download attribute.
			// This allows you to download files in desktop safari if pop up blocking is enabled.
			if(typeof A.download === "undefined") Q.setAttr(A, { target: "_blank" }); // A.setAttribute('target', '_blank');

			// console.log('A: ', A);
			document.body.appendChild(A);
			A.click();
		}
		
		if(data.includes(location.origin)){ // Q.isStr(data)
			setLink(data);
			if(A) document.body.removeChild(A);
		}else{
			/* const html = `<html><body><a href="${data}" download>LINK</a><script>
window.addEventListener('load',function(){
	document.querySelector('a').click();
	setTimeout(function(){
		window.close();
	}, 5000);
});
</script></body></html>`;
			const blobHtml = URL.createObjectURL(new Blob([html], { type: "text/html" }))
			
			console.log('blobHtml: ', blobHtml);
			console.log('html: ', html);
			
			window.open(blobHtml, "_blank");// 
			// URL.revokeObjectURL(blobHtml); */
			
			
			let blobData = (typeof bom !== "undefined") ? [bom, data] : [data];
			let blob = new Blob(blobData, { type: mime || "application/octet-stream" });
			
			if(typeof window.navigator.msSaveBlob !== "undefined"){
				// IE workaround for "HTML7007: One or more blob URLs were 
				// revoked by closing the blob for which they were created. 
				// These URLs will no longer resolve as the data backing the URL has been freed."
				window.navigator.msSaveBlob(blob, filename);
			}
			else{
				let blobURL = URL.createObjectURL(blob);
				
				setLink(blobURL);

				// Fixes "webkit blob resource error 1"
				setTimeout(() => {
					document.body.removeChild(A);
					URL.revokeObjectURL(blobURL);
				}, 1)
			}
		}
	}
}
