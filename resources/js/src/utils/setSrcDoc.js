// 
export default function setSrcDoc({ 
	lang = "en", 
	title, 
	head, 
	body, 
	bodyClass 
} = {}){
	return (`<!DOCTYPE html>
<html lang="${lang}">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
	<title>${title}</title>
	${head ? head : ""}
</head>
<body class="${bodyClass ? " " + bodyClass : ""}">
${body ? body : ""}
</body></html>`);
}

/*
<style>
html{background:#ddd}
body{display:none}
@media print{
	html{background:transparent}
	body{display:block}
	a:not(.btn){
		color:#212529;
		text-decoration:none !important
	}
}
</style>

<script>
window.addEventListener('load',function(){
	window.print();

	function beforePrint(){
		console.log('Functionality to run before printing');
	}

	function afterPrint(){
		console.log('Functionality to run after printing');
		window.close();
	}

	if('matchMedia' in window){
		// Chrome, Firefox, and IE 10 support mediaMatch listeners
		window.matchMedia('print').addListener(function(media){
			if(media.matches){
				beforePrint();
			}else{
				// Fires immediately, so wait for the first mouse movement
				document.addEventListener('mouseover', afterPrint, { once: true, passive: true, capture: true });
				document.addEventListener('mousemove', afterPrint, { once: true, passive: true, capture: true });
			}
		});
	}else{
		// IE and Firefox fire before/after events
		window.addEventListener('beforeprint', beforePrint);
		window.addEventListener('afterprint', beforePrint);
	}
}, false);
</script>
*/