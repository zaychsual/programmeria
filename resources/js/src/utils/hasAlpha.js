// Check image has alpha
export function hasAlpha(file){
	return new Promise((resolve, reject) => {
		let alpha = false;
		const canvas = Q.makeEl('canvas');// document.querySelector
		const ctx = canvas.getContext('2d');
	
		const img = new Image();
		img.crossOrigin = 'anonymous';
		img.onerror = reject;
		img.onload = function(){
			canvas.width = img.width;
			canvas.height = img.height;
		
			ctx.drawImage(img, 0, 0);
			const imgData = ctx.getImageData(0, 0, canvas.width, canvas.height).data;
		
			for(let j = 0; j < imgData.length; j += 4){
				if(imgData[j + 3] < 255){
					alpha = true;
					break;
				}
			}
			resolve(alpha);
		};
		img.src = URL.createObjectURL(file);
	});
}

