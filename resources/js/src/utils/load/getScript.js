import {isStr, domQ, makeEl, setAttr} from '../Q';
import { omit } from '../reactstrap-utils';

/** DEV Load Script */
// getCssJs
// {
//   to = 'body',
//   tag = 'script'
// } = {}
export default function getScript(obj, to = 'body'){
  return new Promise((resolve, reject) => {
    let el = isStr(to) ? document[to] : to;
    let urlSrc = obj.src || obj.href;
    // let As = "stylesheet preload".includes(obj.rel) && (urlSrc.endsWith('.css') || obj.type === "text/css" || obj.as === "style") ? 'link' : 'script';
    let tag = obj.tag ? obj.tag : 'script';

		if(domQ(`${tag}[${tag === 'link' ? 'href':'src'}="${urlSrc}"]`, el)) return;/* MAKE SURE */
		
		let dom = makeEl(tag);// document.createElement(tag);
		function loadend(){
			dom.onerror = dom.onload = null;
		}
		dom.onload = function(e){
			loadend();
			resolve({e, dom});
		}
		dom.onerror = function(){
			loadend();
			dom.remove();
			reject(new Error('Failed to load '+ urlSrc));// obj.src
		}

		window.onerror = function(msg, url, line, column, errObj){
			let str = msg.toLowerCase(),
					scriptErr = "script error";
			if(str.indexOf(scriptErr) > -1){
				dom.remove();
				reject({
					msg: 'Script Error: See browser console for detail',
					/* type: 'Script Error', */
					path: urlSrc /* OPTION */
				});
			}else{
				dom.remove();
				reject({msg, url, line, column,
					errorObj: JSON.stringify(errObj),
					path: obj.src /* OPTION */
				});
			}
			window.onerror = null;
			return false;
		}
		
		/* Set more valid attributes to script tag */
		setAttr(dom, omit(obj, ['tag','onerror','onload','innerText','innerHTML']));// 'async', 'crossOrigin', 'src','text'
		/* // Available attributes script tag
		{
			type: "",
			noModule: false,
			async: true,
			defer: false,
			crossOrigin: null,
			text: "",
			referrerPolicy: null,
			event: "",
			integrity: ""
		} */
		
		if(obj.innerText) dom.innerText = obj.innerText;
		if(obj.innerHTML) dom.innerHTML = obj.innerHTML;
		
		let noInner = !obj.innerText && !obj.innerHTML;
    if(tag !== 'link' && !obj.async && noInner) dom.async = 1;
    if((!obj.crossOrigin || tag !== 'style') && noInner) dom.crossOrigin = "anonymous";
		el.appendChild(dom);
  });
}