// Screen Capture API
const screenCaptureAPI = navigator?.mediaDevices?.getDisplayMedia;// For detect / checking support
const getUserMediaAPI = navigator?.mediaDevices?.getUserMedia;

/* {
  "aspectRatio": 1.7777777777777777,
  "deviceId": "screen:1:0",
  "frameRate": 30,
  "height": 720,
  "resizeMode": "crop-and-scale",
  "width": 1280,
  "cursor": "always",
  "displaySurface": "monitor",
  "logicalSurface": true
} */
const displayMediaOptions = {
  video: {
		cursor: "never" // always | motion | never | ["motion", "always"]
		// cursor: {
			// exact: "none"
		// }
	},
  audio: false
};

const userMediaOptions = {// constraints
	video: true,
	// audio: false
};

function getDisplayMedia(options = displayMediaOptions){// User
	// let captureStream = null;
	return navigator.mediaDevices.getDisplayMedia(options);
	
	// reader["readAs" + readAs](file);
	// return navigator.mediaDevices[`get${type}Media`](mediaOptions);
}

function getUserMedia(options = userMediaOptions){
	return navigator.mediaDevices.getUserMedia(options);
}

function stopCapture(video){ // evt
	let tracks = video.srcObject.getTracks();

	tracks.forEach(track => track.stop());
	video.srcObject = null;
}

// function dumpOptionsInfo(video){ // , cb
// 	const videoTrack = video.srcObject.getVideoTracks()[0];

// 	const trackSettings = JSON.stringify(videoTrack.getSettings(), null, 2);
// 	const getConstraints = JSON.stringify(videoTrack.getConstraints(), null, 2);

// 	console.log("%cvideoTrack:","color:yellow", videoTrack);

// 	console.log("%cTrack settings:","color:yellow", trackSettings);
// 	console.log("%cTrack constraints:","color:yellow", getConstraints);

// 	// cb({ trackSettings, getConstraints });
// }

export { 
	screenCaptureAPI, 
	getUserMediaAPI, 
	displayMediaOptions, 
	userMediaOptions, 
	getDisplayMedia, 
	getUserMedia, 
	stopCapture 
};















