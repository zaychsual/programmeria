// https://github.com/ilkkao/capture-video-frame

export default function captureVideoFrame(video, {
	ext = "jpeg", // format
	flip = false, 
	quality = 0.92
} = {}){
	if(Q.isStr(video)){// typeof video === 'string'
		video = Q.domQ(video);// document.getElementById(video);
	}

	// format = format || 'jpeg';
	// quality = quality || 0.92;

	if(!video || (ext !== 'png' && ext !== 'jpeg')){
		return false;
	}
	// canvas
	let cvs = Q.makeEl("canvas");// document.createElement("CANVAS");

	cvs.width = video.videoWidth;
	cvs.height = video.videoHeight;

	let ctx = cvs.getContext('2d');
	
	// Flip image:
	if(flip){
		ctx.setTransform(-1, 0, 0, 1, cvs.width, 0);
	}
	
	ctx.drawImage(video, 0, 0);

	let type = 'image/' + ext;
	let uri = cvs.toDataURL(type, quality);// dataUri
	// let data = uri.split(',')[1];
	// let type = uri.split(';')[0].slice(5);// mimeType

	let bytes = window.atob(uri.split(',')[1]);// data
	let bl = bytes.length;
	let buf = new ArrayBuffer(bl);// bytes.length
	let arr = new Uint8Array(buf);

	for(let i = 0; i < bl; i++){
		arr[i] = bytes.charCodeAt(i);
	}

	let blob = new Blob([arr], { type });
	return { blob, uri, ext };
}
