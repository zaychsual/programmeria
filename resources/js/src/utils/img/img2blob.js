/**
 * Converts an image to a Blob.
 * @param {HTMLImageElement} img - Image element.
 * @return {Blob} Resulting Blob.
 */
// const imageToBlob = async (img) => {
//   return new Promise((resolve) => {
//     const canvas = Q.makeEl("canvas");// document.createElement

//     canvas.width = img.naturalWidth;
//     canvas.height = img.naturalHeight;

//     const ctx = canvas.getContext('2d');

//     ctx.drawImage(img, 0, 0);

//     canvas.toBlob((blob) => {
//       resolve(blob);
//     });
//   });
// };

const makeCanvas = async (img, cb) => {
  const canvas = Q.makeEl("canvas");// document.createElement

  canvas.width = img.naturalWidth;
  canvas.height = img.naturalHeight;

  const ctx = canvas.getContext("2d");

  ctx.drawImage(img, 0, 0);
  
  canvas.toBlob((blob) => cb(blob));
};

async function imgToBlob(src){
  return new Promise((resolve, reject) => {
    if(Q.isStr(src)){
      const img = new Image();// 100, 200

      img.src = src;
      img.hidden = 1;
      img.onload = function(){
        makeCanvas(img, (blob) => {
          resolve(blob);
          img.remove();
        });
      }
      img.onerror = function(e){
        reject(e);
        img.onerror = null;// OPTIONS
        img.remove();
        return;// OPTIONS
      }
  
      document.body.appendChild(img);
    }else{
      makeCanvas(src, (blob) => resolve(blob));
    }
  });
};

// export { imageToBlob, imgToBlob };