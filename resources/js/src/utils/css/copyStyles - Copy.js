/** Copy styles from a source document to a target.
 * @param {Object} source
 * @param {Object} target */

function copyStyles(source, target, cb){
  Array.from(source.styleSheets).forEach(styleSheet => {
    let rules;// For <style> elements
    // console.log('styleSheet: ', styleSheet);
		try{
      rules = styleSheet.cssRules;
    }catch(e){
      console.error(e);
    }
		
    if(rules){
      const style = source.createElement('style');

      // Write the text of each rule into the body of the style element
      Array.from(styleSheet.cssRules).forEach(cssRule => {
        const { cssText, type } = cssRule;
        let returnText = cssText;
        // Check if the cssRule type is CSSImportRule (3) or CSSFontFaceRule (5) to handle local imports on a about:blank page
        // '/custom.css' turns to 'http://my-site.com/custom.css'
        if([3, 5].includes(type)){
          returnText = cssText.split('url(').map(line => {
            if(line[1] === '/'){
              return `${line.slice(0, 1)}${window.location.origin}${line.slice(1)}`
            }
            return line
          }).join('url(');
        }
        style.appendChild(source.createTextNode(returnText));
      })
			
      target.head.appendChild(style);
			
			if(cb) cb();
    }else if(styleSheet.href){
      const Link = source.createElement('link');// for <link> elements loading CSS from a URL

      Link.rel = 'stylesheet';
      Link.href = styleSheet.href;
      target.head.appendChild(Link);
			
			if(cb) cb();
    }
  })
}

export {copyStyles};