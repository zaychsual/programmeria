/**
 * Make keyframe rules.
 * @param {CSSRule} cssRule
 * @return {String}
 * @private
*/
function getKeyFrameText(cssRule) {
  const tokens = ['@keyframes', cssRule.name, '{'];
  Array.from(cssRule.cssRules).forEach(cssRule => {
    // type === CSSRule.KEYFRAME_RULE should always be true
    tokens.push(cssRule.keyText, '{', cssRule.style.cssText, '}');
  })
  tokens.push('}');
  return tokens.join(' ');
}

/**
 * Handle local import urls.
 * @param {CSSRule} cssRule
 * @return {String}
 * @private
*/
function fixUrlForRule(cssRule) {
  return cssRule.cssText
    .split('url(')
    .map(line => {
      if (line[1] === '/') {
        return `${line.slice(0, 1)}${window.location.origin}${line.slice(1)}`
      }
      return line
    })
    .join('url(');
}

/** Copy styles from a source document to a target.
 * @param {Object} source
 * @param {Object} target */
 
export default function copyStyles(source, target) {
  // Store style tags, avoid reflow in the loop
  const headFrag = target.createDocumentFragment()

  Array.from(source.styleSheets).forEach(styleSheet => {
    let rules;// For <style> elements
    try {
      rules = styleSheet.cssRules;
    } catch (err) {
      console.error(err);
    }
    if (rules) {
      const ruleText = [];// IE11 is very slow for appendChild, so use plain string here

      // Write the text of each rule into the body of the style element
      Array.from(styleSheet.cssRules).forEach(cssRule => {
        const { type } = cssRule;

        // Skip unknown rules
        if (type === CSSRule.UNKNOWN_RULE) {
          return;
        }

        let returnText = '';

        if(type === CSSRule.KEYFRAMES_RULE){
          // IE11 will throw error when trying to access cssText property, so we
          // need to assemble them
          returnText = getKeyFrameText(cssRule);
        }
        else if ([CSSRule.IMPORT_RULE, CSSRule.FONT_FACE_RULE].includes(type)){
          // Check if the cssRule type is CSSImportRule (3) or CSSFontFaceRule (5)
          // to handle local imports on a about:blank page
          // '/custom.css' turns to 'http://my-site.com/custom.css'
          returnText = fixUrlForRule(cssRule);
        }else{
          returnText = cssRule.cssText;
        }
        ruleText.push(returnText);
      })

      const newStyleEl = target.createElement('style');
      newStyleEl.textContent = ruleText.join('\n');
      headFrag.appendChild(newStyleEl);
    } else if (styleSheet.href) {
      const newLinkEl = target.createElement('link');// // for <link> elements loading CSS from a URL

      newLinkEl.rel = 'stylesheet';
      newLinkEl.href = styleSheet.href;
      headFrag.appendChild(newLinkEl);
    }
  })

  target.head.appendChild(headFrag);
}

/* function copyStyles(source, target, cb){
  Array.from(source.styleSheets).forEach(styleSheet => {
    let rules;// For <style> elements
    
		try{
      rules = styleSheet.cssRules;
    }catch(e){
      console.error(e);
    }
		
    if(rules){
      const style = source.createElement('style');

      // Write the text of each rule into the body of the style element
      Array.from(styleSheet.cssRules).forEach(cssRule => {
        const { cssText, type } = cssRule;
        let returnText = cssText;
        // Check if the cssRule type is CSSImportRule (3) or CSSFontFaceRule (5) to handle local imports on a about:blank page
        // '/custom.css' turns to 'http://my-site.com/custom.css'
        if([3, 5].includes(type)){
          returnText = cssText.split('url(').map(line => {
            if(line[1] === '/'){
              return `${line.slice(0, 1)}${window.location.origin}${line.slice(1)}`
            }
            return line
          }).join('url(');
        }
        style.appendChild(source.createTextNode(returnText));
      })
			
      target.head.appendChild(style);
			
			if(cb && style) cb();
    }else if(styleSheet.href){
      const Link = source.createElement('link');// for <link> elements loading CSS from a URL

      Link.rel = 'stylesheet';
      Link.href = styleSheet.href;
      target.head.appendChild(Link);
			
			if(cb && Link) cb();
    }
  })
}

export {copyStyles}; */