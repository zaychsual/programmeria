// https://github.com/npm/validate-npm-package-name
const scopedPackagePattern = new RegExp('^(?:@([^/]+?)[/])?([^/]+?)$')
// const builtins = require('builtins')
const blacklist = [
  'node_modules',
  'favicon.ico'
];

const PN = "Package name";

// const validate = module.exports = function (name) {
const validatePackageName = (name) => {
  let warnings = [];
  let errors = [];

  if (name === null) {
    errors.push(PN + ' cannot be null')
    return done(warnings, errors)
  }

  if (name === undefined) {
    errors.push(PN + ' cannot be undefined')
    return done(warnings, errors)
  }

  if (typeof name !== 'string') {
    errors.push(PN + ' must be a string')
    return done(warnings, errors)
  }

  if (!name.length) {
    errors.push(PN + ' length must be greater than zero')
  }

  if (name.match(/^\./)) {
    errors.push(PN + ' cannot start with a period')
  }

  if (name.match(/^_/)) {
    errors.push(PN + ' cannot start with an underscore')
  }

  if (name.trim() !== name) {
    errors.push(PN + ' cannot contain leading or trailing spaces')
  }

  // No funny business
  blacklist.forEach((blacklistedName) => {
    if (name.toLowerCase() === blacklistedName) {
      errors.push(blacklistedName + ' is a blacklisted name')
    }
  })

  // Generate warnings for stuff that used to be allowed

  // core module names like http, events, util, etc
  // builtins.forEach(function (builtin) {
  //   if (name.toLowerCase() === builtin) {
  //     warnings.push(builtin + ' is a core module name')
  //   }
  // })

  // really-long-package-names-------------------------------such--length-----many---wow
  // the thisisareallyreallylongpackagenameitshouldpublishdowenowhavealimittothelengthofpackagenames-poch.
  if (name.length > 214) {
    warnings.push(PN + ' can no longer contain more than 214 characters')
  }

  // mIxeD CaSe nAMEs
  if (name.toLowerCase() !== name) {
    warnings.push(PN + ' can no longer contain capital letters')
  }

  if (/[~'!()*]/.test(name.split('/').slice(-1)[0])) {
    warnings.push(PN + ' can no longer contain special characters ("~\'!()*")')
  }

  if (encodeURIComponent(name) !== name) {
    // Maybe it's a scoped package name, like @user/package
    let nameMatch = name.match(scopedPackagePattern)
    if (nameMatch) {
      let user = nameMatch[1]
      let pkg = nameMatch[2]
      if (encodeURIComponent(user) === user && encodeURIComponent(pkg) === pkg) {
        return done(warnings, errors)
      }
    }

    errors.push(PN + ' can only contain URL-friendly characters')
  }

  return done(warnings, errors)
}

validatePackageName.scopedPackagePattern = scopedPackagePattern

const done = (warnings, errors) => {
  let result = {
    validForNewPackages: errors.length === 0 && warnings.length === 0,
    validForOldPackages: errors.length === 0,
    warnings: warnings,
    errors: errors
  }
  if (!result.warnings.length) delete result.warnings
  if (!result.errors.length) delete result.errors
  return result
}

export default validatePackageName;

