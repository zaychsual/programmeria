// https://blog.logrocket.com/javascript-typeof-2511d53a1a62/
// export default function typeOf(val){
//   let matches = Object.prototype.toString.call(val).match(/^[object (S+?)]$/) || [];
//   return (matches[1] || 'undefined').toLowerCase();
// }

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
export default function typeOf(obj, showFullClass){
  // get toPrototypeString() of obj (handles all types)
  if (showFullClass && typeof obj === "object") {
    return Object.prototype.toString.call(obj);
  }
  if (obj == null) { // implicit toString() conversion
    return (obj + '').toLowerCase();
  }

  let deepType = Object.prototype.toString.call(obj).slice(8, -1).toLowerCase();
  if (deepType === 'generatorfunction') { return 'function' }

  // Prevent overspecificity (for example, [object HTMLDivElement], etc).
  // Account for functionish Regexp (Android <=2.3), functionish <object> element (Chrome <=57, Firefox <=52), etc.
  // String.prototype.match is universally supported.
  return deepType.match(/^(array|bigint|date|error|function|generator|regexp|symbol)$/) ? deepType : (typeof obj === 'object' || typeof obj === 'function') ? 'object' : typeof obj;
}
