// 
export default function getEmbedUrl(s){
	const getSrc = /<iframe.*?src=\s*(["'])(.*?)\1/.exec(s);
	// console.log('getSrc: ', getSrc);

	if(getSrc && getSrc[2]){
		// console.log(getSrc[2]);
		return getSrc[2];
	}
	return false;
}
