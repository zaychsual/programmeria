// === FROM: https://stackoverflow.com/questions/43431829/split-array-of-file-paths-into-hierarchical-object-in-javascript

// Extract a filename from a path
// function getFilename(path){
//     return path.split("/").filter(v => {
//         return v && v.length;
//     }).reverse()[0];
// }

// // Find sub paths
// function findSubPaths(path){
//     // slashes need to be escaped when part of a regexp
//     let rePath = path.replace("/", "\\/");
//     let re = new RegExp("^" + rePath + "[^\\/]*\\/?$");
//     return paths.filter(i => i !== path && re.test(i));
// }

// // Build tree recursively
// function buildTree(path){
//     path = path || "";
//     let nodeList = [];
//     findSubPaths(path).forEach(subPath => {
//         let nodeName = getFilename(subPath);
//         if(/\/$/.test(subPath)){
//             let node = {};
//             node[nodeName] = buildTree(subPath);
//             nodeList.push(node);
//         }else{
//             nodeList.push(nodeName);
//         }
//     });

//     return nodeList;
// }

// === FROM: https://stackoverflow.com/questions/26645220/build-nested-folder-structure-from-path-strings/26652662
// OR: https://github.com/vasilionjea/treepath

// function pathString2Dir(){
//     let tree = {
//     // Represents the "root" directory, like in a filesystem.
//         root: {
//             absolute_path: '',
//             files: []
//         }
//     };

//     function buildTree(parts){
//         let lastDir = 'root';
//         let abs_path = '';

//         parts.forEach(name => {
//             // It's a directory
//             if(name.indexOf('.') === -1){
//                 lastDir = name;
//                 abs_path += lastDir + '/';

//                 if(!tree[name]){
//                     tree[name] = {
//                         absolute_path: abs_path,
//                         files: []
//                     };
//                 }
//             }else{
//                 tree[lastDir].files.push(name);
//             }
//         });
//     }

//     paths.forEach((path, index, array) => {
//         buildTree(path.split('/'));
//     });

//     return tree;
// }

/**
 * Treepath takes an array of pathnames and gives you back
 * an object representation of that path hierarchy.
 */

const treePath = (function(){
	function buildTree(tree, parts){
		let lastDir = "root";
		let dirPath = "/"; // ORI: "" | Q-EDIT = "/"
		
		parts.forEach((part) => {
			let name = part.trim();

			// In case we have a single `/`
			if(!name || !!name.match(/^\/$/)){
			  return;
			}

			// console.log('tree: ', tree);
			// console.log('part: ', part);
			// console.log('name: ', name);

			// It's a directory
			if(name.indexOf('.') === -1){
				lastDir = name;
				dirPath += lastDir + '/';
				
				console.log('lastDir: ', lastDir);
				console.log('dirPath: ', dirPath);
				console.log('tree[lastDir]: ', tree[lastDir]);

				if(!tree[name]){
					tree[name] = {
						isDirectory: true, // OPTION
						path: dirPath,
						files: [] // "HELL" // []
					};
				}
			}else{
				// It's a file
				// console.log('name: ',name);
				// tree[lastDir].files.push(name);
				tree[lastDir].files.push({
					path: '/'+ lastDir +'/'+ name,
					name, 
					type: "file",
					isFile: true // OPTION
				});
			}
		});
	}

	return (paths) => {
		if(!paths || !Array.isArray(paths)){
			throw new TypeError('Expected paths to be an array of strings but received: ' + (typeof paths));
		}

		let tree = {
			root: {
				path: "/", // ORI: "" | Q-EDIT = "/"
				files: []
			}
		};

		paths.forEach(function(path){
			buildTree(tree, path.split("/"));
		});

		return tree;
	};
}());

export { treePath };


