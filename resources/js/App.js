import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React, { Component, lazy, Suspense } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';// axios | redaxios
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { LastLocationProvider } from 'react-router-last-location';
// import { BroadcastChannel }from 'broadcast-channel';
// import { ErrorBoundary } from 'react-error-boundary';

import { AppConfigContext, AppConfigProvider } from './src/context/AppContext';//
import Mq from './src/parts/Mq';
import PageLoader from './src/components/PageLoader';
import RouteLazy from './src/components/RouteLazy'; // ErrorBoundary | RouteLazy
import NavApp from './src/parts/app/NavApp';
import { ScrollTo } from './src/components/q-ui-react/ScrollTo';
// import { APP_NAME } from './src/data/appData';

// PAGES :
import NotFound from './src/pages/public/NotFound';

// ==================================================
// DEV OPTION: Store to window object / global
window.React = React;
window.ReactDOM = ReactDOM;
window.axios = axios;
// window.axios.defaults = {
//   ...axios.defaults,
//   baseURL: Q.baseURL + "/api", 
//   timeout: 30000, 
//   withCredentials: true, 
//   headers: {
//     ...axios.defaults.headers, 
//     // common: {
//     //   'X-Requested-With': 'XMLHttpRequest'
//     // }
//     'X-Requested-With': 'XMLHttpRequest'
//   }
// };

window.axios.defaults.baseURL = Q.baseURL + "/api";
window.axios.defaults.timeout = 30000;
window.axios.defaults.withCredentials = true;// DEFAULT: false
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

// NOT WORK to store in window ???
// const instance = axios.create({
//   baseURL: Q.baseURL + "/api",
//   // timeout: 1000, 
//   withCredentials: true, 
//   headers: {
//     'X-Requested-With': 'XMLHttpRequest'
//   }
// });
// window.axios = instance;
// END DEV OPTION: Store to window object / global
// ==================================================

// function ErrorFallback({ error, resetErrorBoundary }){
//   return (
//     <div role="alert" 
//       className="alert alert-danger mb-0 rounded-0 pre-wrap ovauto" // ff-inherit fs-inherit
//     >&#9888; Something went wrong. 
//       <p>{navigator.onLine ? error.message : "Your internet connection is offline."}</p>
//       <Btn onClick={resetErrorBoundary}>Try again</Btn>
//     </div>
//   )
// }

// lazy:
const Home = lazy(() => import(/* webpackChunkName: "Home" */'./src/pages/public/Home'));
const About = lazy(() => import(/* webpackChunkName: "Home" */'./src/pages/public/About'));
const Login = lazy(() => import(/* webpackChunkName: "Login" */'./src/pages/public/auth/Login'));
const Register = lazy(() => import(/* webpackChunkName: "Register" */'./src/pages/public/auth/Register'));
const ForgotPassword = lazy(() => import(/* webpackChunkName: "ForgotPassword" */'./src/pages/public/auth/ForgotPassword'));

// const bcAuth = new BroadcastChannel('AUTH');
// const SM_DEVICE = Q_appData.UA.platform.type === "mobile" || screen.width <= 480; // Q_UA.platform.type === "mobile" && screen.width < 480;

class Root extends Component{
  static contextType = AppConfigContext;

	constructor(props){
		super(props);
    Q.setUpDOM();
    // if(SM_DEVICE) Q.setClass(document.body, "isMobile");

    this.state = {
      // load: false,
      err: false,
      // app: { ...INIT_CONFIG }
    };
  }

  componentDidMount(){
    ["dragover","drop"].forEach(v => window.addEventListener(v, Q.preventQ));// Prevent Drag & Drop File
    
    const { config, setAppConfig } = this.context;
    // console.log('this.context: ', this.context);

    let token = localStorage.getItem("t");
		if(token){
			setAppConfig({ ...config, token });// "Bearer " + token
		}

    const getAppConfig = () => {
      axios.get(Q.baseURL + "/DUMMY/json/APP_CONFIG.json")
      .then(res => {
        console.log('APP_CONFIG res: ', res);
        if(res?.data?.error){// res.data && !res.data.error
          console.log('handle error server e: ', e);
          this.setState({ err: true });
        }else{
          let dataConfig = {
            ...config,
            ...res.data
          };
          setAppConfig(dataConfig);
          localStorage.setItem("appConfig", JSON.stringify(dataConfig));
        }
      })
      .catch(e => {
        console.log('e: ', e);
        this.setState({ err: true });
      })
      .then(() => {
        Q.setClass(Q.domQ('#QloadStartUp'), "d-none");// Hide Loader Splash / start up
      });
    }

    axios.get(Q.baseURL + "/DUMMY/json/APP_VERSION.json") // , { responseType: "text" }
    .then(r => {
      console.log('APP_VERSION.json r: ', r);
      if(r && r.data){
        let appData = localStorage.getItem("appConfig");
        if(appData){
          let app = JSON.parse(appData);
          if(app.version !== r.data){
            console.log('app: ', app);
            getAppConfig();
          }else{
            setAppConfig({ ...config, ...app });
            Q.setClass(Q.domQ('#QloadStartUp'), "d-none");// Hide Loader Splash / start up
          }
        }else{
          getAppConfig();
        }
      }
    });

		// console.log('%cWelcome to Programmeria','color:#666;font-family:sans-serif;letter-spacing:2px;font-size:26px;font-weight:700;text-shadow:1px 1px 0 #A0E7FE,2px 2px 1px rgba(0,0,0,.3)');
		// console.log('%cDevelopment by: https://programmeria.com','font-size:15px');
  }

  render(){
    const { config } = this.context;// setAppConfig
    const { err } = this.state;// app, load

    if(!config.lang) return null;

    return (
			<Mq>
				{(isMobile) => (
          <>
            <NavApp

            />

            <main>
              {err ? 
                <div className="alert alert-danger">
                  Error {err}
                </div>
                : 
                <Suspense fallback={<PageLoader bottom className="h-full-navmain" />}>
                  <Switch>
                    <RouteLazy exact strict path="/" component={Home} />
                    <RouteLazy path="/about" component={About} />
                    
                    {!config.user && 
                      [
                        { p: "/login", c: Login }, 
                        { p: "/register", c: Register }, 
                        { p: "/forgot-password", c: ForgotPassword }, 
                      ].map((v) => 
                        <RouteLazy key={v.p} path={v.p} component={v.c} />
                      )
                    }

                    <Route path="*" component={NotFound} />
                  </Switch>
                </Suspense>
              }
            </main>

            <ScrollTo
              target="window"
              threshold={95} // 150
              btnProps={{
                size: "sm",
                "aria-label": "Back To top",
                className: "position-fixed b8 r8 zi-1001 tip tipL qi qi-arrow-up" // up | down
              }}
            />
          </>
				)}
      </Mq>
    )
  }
}

const App = () => (
	<BrowserRouter>{/*  basename={BASENAME} */}
    <LastLocationProvider>
      <AppConfigProvider>
        <Route component={Root} />
      </AppConfigProvider>
    </LastLocationProvider>
	</BrowserRouter>
);

ReactDOM.render(<App />, Q.domQ('#root'));
