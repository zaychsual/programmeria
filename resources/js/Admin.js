import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React, { Component, lazy, Suspense } from 'react';// { useEffect }
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { LastLocationProvider } from 'react-router-last-location';
import { BroadcastChannel }from 'broadcast-channel';
// import { ErrorBoundary } from 'react-error-boundary';
// import { lazy as lazyLoadable } from '@loadable/component';// loadable
import Swal from 'sweetalert2/dist/sweetalert2.min.js';// sweetalert2.js
import axios from 'axios';

// import { APP_NAME } from './src/data/appData';
import { AppConfigContext, AppConfigProvider } from './src/context/AppContext';
import Mq from './src/parts/Mq';
import AsideAdmin from './src/parts/admin/AsideAdmin';
import NavAdmin from './src/parts/admin/NavAdmin';
import RouteLazy from './src/components/RouteLazy'; // ErrorBoundary | RouteLazy
import PageLoader from './src/components/PageLoader';
// import Placeholder from './src/components/q-ui-react/Placeholder';
// import Btn from './src/components/q-ui-react/Btn';
import Flex from './src/components/q-ui-react/Flex';
// import Head from './src/components/q-ui-react/Head';
import { ScrollTo } from './src/components/q-ui-react/ScrollTo';
// import DocsDevelopment from './src/parts/DocsDevelopment';// { DocsDevelopment }

// PAGES : 
import NotFound from './src/pages/public/NotFound';

// ==================================================
// DEV OPTION: Store to window object / global
window.React = React;
window.ReactDOM = ReactDOM;
window.Swal = Swal;
window.axios = axios;
window.axios.defaults.baseURL = Q.baseURL + "/api";
window.axios.defaults.withCredentials = true;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
// END DEV OPTION: Store to window object / global
// ==================================================

// function ErrorFallback({ error, resetErrorBoundary }){
//   return (
//     <div role="alert" 
//       className="alert alert-danger mb-0 rounded-0 pre-wrap ovauto" // ff-inherit fs-inherit
//     >&#9888; Something went wrong. 
//       <p>{navigator.onLine ? error.message : "Your internet connection is offline."}</p>
//       <Btn onClick={resetErrorBoundary}>Try again</Btn>
//     </div>
//   )
// }

// lazy:
const HomeAdmin = lazy(() => import(/* webpackChunkName: "HomeAdmin" */'./src/pages/admin/HomeAdmin'));
const Settings = lazy(() => import(/* webpackChunkName: "Settings" */'./src/pages/admin/settings'));// /Settings
const MenusSettings = lazy(() => import(/* webpackChunkName: "MenusSettings" */'./src/pages/admin/Menus'));
// PAGES SETTINGS : 
const SettingPage = lazy(() => import(/* webpackChunkName: "SettingPage" */'./src/pages/admin/SettingPage'));
const HomeSet = lazy(() => import(/* webpackChunkName: "HomeSet" */'./src/pages/admin/set-pages/HomeSet'));
const AboutUsSet = lazy(() => import(/* webpackChunkName: "AboutUsSet" */'./src/pages/admin/set-pages/AboutUsSet'));
const ContactUsSet = lazy(() => import(/* webpackChunkName: "ContactUsSet" */'./src/pages/admin/set-pages/ContactUsSet'));
const GallerySet = lazy(() => import(/* webpackChunkName: "GallerySet" */'./src/pages/admin/set-pages/GallerySet'));

const Users = lazy(() => import(/* webpackChunkName: "Users" */'./src/pages/admin/Users'));
// const UserAdd = lazy(() => import(/* webpackChunkName: "UserAdd" */'./src/pages/admin/UserAdd'));
// const UserEdit = lazy(() => import(/* webpackChunkName: "UserEdit" */'./src/pages/admin/UserEdit'));
const UserDetail = lazy(() => import(/* webpackChunkName: "UserDetail" */'./src/pages/admin/UserDetail'));
const Visitor = lazy(() => import(/* webpackChunkName: "Visitor" */'./src/pages/admin/Visitor'));
// const Levels = lazy(() => import(/* webpackChunkName: "Levels" */'./src/pages/admin/Levels'));
// const AddLevel = lazy(() => import(/* webpackChunkName: "AddLevel" */'./src/pages/admin/user/AddLevel'));
const ChangePassword = lazy(() => import(/* webpackChunkName: "ChangePassword" */'./src/pages/admin/user/ChangePassword'));

const ContactUs = lazy(() => import(/* webpackChunkName: "ContactUs" */'./src/pages/admin/ContactUs'));
// const Products = lazy(() => import(/* webpackChunkName: "Products" */'./src/pages/admin/Products'));
// const ProductAdd = lazy(() => import(/* webpackChunkName: "ProductAdd" */'./src/pages/admin/ProductAdd'));
// const ProductEdit = lazy(() => import(/* webpackChunkName: "ProductEdit" */'./src/pages/admin/ProductEdit'));
const Files = lazy(() => import(/* webpackChunkName: "Files" */'./src/pages/admin/Files'));
// const Merchants = lazy(() => import(/* webpackChunkName: "Merchants" */'./src/pages/admin/Merchants'));

// MODULES / APPS:
const PosPage = lazy(() => import(/* webpackChunkName: "PosPage" */'./src/pages/apps/PosPage'));

// TOOLS:
const ManifestGeneratorPage = lazy(() => import(/* webpackChunkName: "ManifestGeneratorPage" */'./src/pages/admin/tools/ManifestGeneratorPage'));
const EmailEditorPage = lazy(() => import(/* webpackChunkName: "EmailEditorPage" */'./src/pages/apps/EmailEditorPage'));
const Base64EncoderPage = lazy(() => import(/* webpackChunkName: "Base64EncoderPage" */'./src/pages/apps/tools/Base64EncoderPage'));
const CssUnitConverterPage = lazy(() => import(/* webpackChunkName: "CssUnitConverterPage" */'./src/pages/apps/tools/CssUnitConverterPage'));
const ThemeDesignerPage = lazy(() => import(/* webpackChunkName: "ThemeDesignerPage" */'./src/pages/admin/tools/ThemeDesignerPage'));
const IconMakerPage = lazy(() => import(/* webpackChunkName: "IconMakerPage" */'./src/pages/apps/IconMakerPage'));

// DOCS:
const DocumentationPage = lazy(() => import(/* webpackChunkName: "DocumentationPage" */'./src/pages/admin/Documentation'));
const ChartJSPage = lazy(() => import(/* webpackChunkName: "ChartJSPage" */'./src/pages/admin/docs/components/ChartJSPage'));
const HighchartsPage = lazy(() => import(/* webpackChunkName: "HighchartsPage" */'./src/pages/admin/docs/components/HighchartsPage'));
const PrimeReactDocs = lazy(() => import(/* webpackChunkName: "PrimeReactDocs" */'./src/pages/admin/devs/PrimeReact'));

// DEVS:
const DevsPage = lazy(() => import(/* webpackChunkName: "DevsPage" */'./src/pages/admin/devs/DevsPage'));
const ScrollToPage = lazy(() => import(/* webpackChunkName: "ScrollToPage" */'./src/pages/admin/devs/ScrollToPage'));
const AppDesignerPage = lazy(() => import(/* webpackChunkName: "AppDesignerPage" */'./src/pages/admin/devs/AppDesignerPage'));
const ImageMapDesignerPage = lazy(() => import(/* webpackChunkName: "ImageMapDesignerPage" */'./src/pages/admin/devs/ImageMapDesignerPage'));

// Parts:
const DocsDev = lazy(() => import(/* webpackChunkName: "DocsDev" */'./src/parts/DocsDevelopment'));

// const UpgradeApp = ({ label }) => {
//   return (
//     <Flex dir="column" justify="center" align="center" className="mh-full-navmain h3 ired qi qi-info qi-2x">
// 			<Head title={label} />
//       {label}
// 			<div className="small bold mt-2">Must upgrade to use This feature</div>
//     </Flex>
//   )
// };

function toggleActive(on){
	const EVTS = ["click","contextmenu","keydown"];
	Q.domQall(Q.FOCUSABLE).forEach(n => {
    if(on){
      Q.setClass(n, "point-no", "remove");
      Q.setAttr(n, "tabindex");
			EVTS.forEach(ev => n.removeEventListener(ev, Q.preventQ));
    }else{
      Q.setClass(n, "point-no");
      n.tabIndex = "-1";
			EVTS.forEach(ev => n.addEventListener(ev, Q.preventQ));
    }
  });
}

const bcAuth = new BroadcastChannel('AUTH');
const SM_DEVICE = Q_appData.UA.platform.type === "mobile" || screen.width <= 480; // Q_UA.platform.type === "mobile" && screen.width < 480;
const FAV = "/logo/favicon.ico";

const TOOLS = [
  { label:"Icon Maker", to:"/tools/icon-maker", icon:"code" }, 
  // { label:"Base64 encoder", to:"/tools/base64-encoder", icon:"code" }, 
  // { label:"Manifest Generator", to:"/tools/manifest-generator", icon:"code" }, 
  // { label:"Email Editor", to:"/tools/email-editor", icon:"edit" }, 
];

const ROOT_PATH = "/settings-pages";
const SET_PAGES = [
	{ label:"Settings", to: ROOT_PATH + "/settings", icon:"plus", c: SettingPage }, 
	{ label:"Home", to: ROOT_PATH + "/home", icon:"browser", c: HomeSet }, 
	{ label:"Tentang Kami", to: ROOT_PATH + "/tentang-kami", icon:"browser", c: AboutUsSet }, // , exact:true, strict:true
	{ label:"Hubungi Kami", to: ROOT_PATH + "/hubungi-kami", icon:"browser", c: ContactUsSet },
	{ label:"Gallery", to: ROOT_PATH + "/gallery", icon:"browser", c: GallerySet }
];

const MENUS = [
	{ label:"Home", to:"/", icon:"home", exact:true, strict:true }, // home
  { label:"Settings", icon:"cog", 
    menus: [
      { label:"Settings App", to:"/settings/app", icon:"cog" }, 
			{ label:"Settings Menus", to:"/settings/menus", icon:"cog" }, 
			// { label:"Test native url", href:"/intanpertiwinusantara/admin/test-native", icon:"cog" }
    ]
  }, 
	{ label:"Setting Pages", icon:"cog", menus: SET_PAGES }, 
	{ label: "User", icon: "user",  
		menus: [
			{ label:"Users", to:"/user/all", icon:"table" }, 
			{ label:"Visitor", to:"/user/visitor", icon:"table" }, 
		]
	}, 
	
	{ label:"Contact Us", to:"/contact-us", icon:"mail" }, 
	// { label:"Product", icon:"list", 
	// 	menus: [
	// 		{ label: "Products", to:"/product/all", icon:"list" }, 
	// 		{ label: "Add Product", to:"/product/add", icon:"plus" }
	// 	]
	// }, 
	{ label:"Files", to:"/files", icon:"file" }, 
	{ label:"Modules", icon:"code", 
		menus: [
			{ label:"Point Of Sale", to:"/modules/pos", icon:"code" }, 
			// { label:"HR", to:"/modules/hr", icon:"code" }, 
		]
	}, 
	// { label:"Merchants", to:"/merchants", icon:"link" }, 
  { label:"Tools", icon:"wrench", 
		menus: [
			{ label:"Manifest Generator", to:"/tools/manifest-generator", icon:"code" }, 
			{ label:"Email Editor", to:"/tools/email-editor", icon:"edit" }, 
			{ label:"Base64 encoder", to:"/tools/base64-encoder", icon:"code" }, 
			{ label:"Css Unit Converter", to:"/tools/css-unit-converter", icon:"wrench" }, 
			{ label:"Theme Designer", to:"/tools/theme-designer", icon:"code" }, 
			// { label:"Icon Maker", to:"/tools/icon-maker", icon:"code" }, 
			...TOOLS
		] 
	},
	// { label:"Test native url root", href:"/intanpertiwinusantara/admin/test-native-root", icon:"cog" }, 
	{ label:"Documentation", to:"/documentation", icon:"book" }, 
	{ label:"Components", icon:"code", 
		menus: [
			{ label:"ChartJS", to:"/components/chartjs", icon:"code" }, 
			{ label:"Highcharts", to:"/components/highcharts", icon:"code" }, 
		]
	}, 
	{ label:"Devs", icon:"code", 
		menus: [
			{ label:"Devs Test", to:"/devs/tests", icon:"code" }, 
			{ label:"ScrollTo", to:"/devs/scrollto", icon:"code" }, 
			{ label:"App Designer", to:"/devs/app-designer", icon:"code" }, 
			{ label:"Image Map Designer", to:"/devs/image-map-designer", icon:"code" }, 
		]
	},
	{ label:"Primereact", to:"/primereact", icon:"code" }
];

// if(USER_DATA.isAdmin){
// 	const menuUser = MENUS.find(f => f.label === "User");
// 	menuUser.menus.unshift(
// 		{ label:"Add User", to:"/user/add", icon:"plus" }, 
// 		{ label:"Add Level", to:"/user/create-level", icon:"plus" },
// 		// { label:"Levels", to:"/user/levels", icon:"table" }
// 	);
// 	menuUser.menus.push({ label:"Levels", to:"/user/levels", icon:"table" });
// }

const QloadStartUp = Q.domQ('#QloadStartUp');

class Root extends Component{
	static contextType = AppConfigContext;

	constructor(props){
		super(props);

		// SETUP DOM 
		//if(!document.documentElement?.lang?.length > 0) document.documentElement.lang = Q.lang();

		Q.setUpDOM();

		if(SM_DEVICE) Q.setClass(document.body, "isMobile");

		// DEVS NOT FIX: Store favicon offline file
		const faviconOffline = 'data:image/svg+xml,%3Csvg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"%3E%3Cpath fill="red" d="M633.99 471.02L36 3.51C29.1-2.01 19.03-.9 13.51 6l-10 12.49C-2.02 25.39-.9 35.46 6 40.98l598 467.51c6.9 5.52 16.96 4.4 22.49-2.49l10-12.49c5.52-6.9 4.41-16.97-2.5-22.49zM80 384H48c-8.84 0-16 7.16-16 16v96c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-96c0-8.84-7.16-16-16-16zm400-272c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v127.66l64 50.04V112zm128-96c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v323.73l64 50.04V16zM416 496c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-23.52l-64-50.04V496zm-128 0c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V372.41l-64-50.04V496zm-80-208h-32c-8.84 0-16 7.16-16 16v192c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V304c0-8.84-7.16-16-16-16z"%3E%3C/path%3E%3C/svg%3E';
		if(!localStorage.getItem("favicon-offline")){
			localStorage.setItem('favicon-offline', faviconOffline);
		}
		Q_appData.faviconOffline = faviconOffline; // Q.FAV_OFF
		// END SETUP DOM

		this.state = {
			err: false, 
			// user: null, 
			loadMenus: false, 
			menus: [], 
			asideMin: false, 
		};
	}

	componentDidMount(){
		Q.bindFuncs.call(this,['onToggleAside']);

		['dragover','drop'].forEach(v => window.addEventListener(v, Q.preventQ));// Prevent Drag & Drop File
		['online','offline'].forEach(v => window.addEventListener(v, this.onConnection));
		// window.addEventListener('keydown', this.onCtrlSaveNPrint);// DEV OPTION: for prevent ctrl + S, ctrl + P
		this.onConnection();// Local / Dev Only

    bcAuth.addEventListener('message', (e) => {
      // console.log('e', e);
      const { action } = e;// , location | NATIVE = e.data

      switch(action){
        case 'LOGOUT':
          // if(location !== window.location.pathname){ // this.props.location.pathname
						toggleActive();

            Swal.fire({
              icon: "warning",
              title: "You have logged out of another page.", // You're logged out
              allowOutsideClick: false, 
              allowEscapeKey: false, 
              confirmButtonText: "Login"
            }).then((s) => { // s
              if(s.isConfirmed){
                window.location.replace("/login");// Q.baseURL + "/admin/login"
              }
            });
          // }
          break;
				case 'LOGIN':
					if(Swal.isVisible()){//  && location !== window.location.pathname
						Swal.close();
						toggleActive(true);
					}
					break;
        default:
          break;
      }
    });

		if(token){ // Swal.isVisible()
			bcAuth.postMessage({ 
				action: "LOGIN", 
				// location: window.location.pathname
			});
		}

		// const { config, setAppConfig } = this.context;
		let token = localStorage.getItem("t");
		if(token){
			const { config, setAppConfig } = this.context;
			window.axios.defaults.headers.common.Authorization = "Bearer " + token;
			setAppConfig({ ...config, token });// "Bearer " + token
		}else{
			window.location.replace("/login");
		}
    // console.log('token: ', token);
		// console.log('config: ', config);
		// console.log('this.context: ', this.context);

		// axios.get("/auth").then(r => {
		// 	console.log('%cAUTH r: ', 'color:yellow', r);
		// 	if(r.data){//  && !r.data.error
		// 		// const user = Q.omit(r.data.user, ["password","salt","remember_code","forgotten_password_code","forgotten_password_time","activation_code"]);
		// 		const user = r.data.user;
		
		// 		// if(user.isAdmin){
		// 		// 	const menuUser = MENUS.find(f => f.label === "User");
		// 		// 	menuUser.menus.unshift(
		// 		// 		{ label:"Add User", to:"/user/add", icon:"plus" }, 
		// 		// 		{ label:"Add Level", to:"/user/create-level", icon:"plus" },
		// 		// 		// { label:"Levels", to:"/user/levels", icon:"table" }
		// 		// 	);
		// 		// 	menuUser.menus.push({ label:"Levels", to:"/user/levels", icon:"table" });
		// 		// }

		// 		if(user.isAdmin){
		// 			const menuUser = MENUS.find(f => f.label === "User");
		// 			menuUser.menus.unshift(
		// 				{ label:"Add User", to:"/user/add", icon:"plus" }, 
		// 				{ label:"Add Level", to:"/user/create-level", icon:"plus" },
		// 				// { label:"Levels", to:"/user/levels", icon:"table" }
		// 			);
		// 			menuUser.menus.push({ label:"Levels", to:"/user/levels", icon:"table" });
		// 		}
				
		// 		// USER_DATA.push(user);
		// 		for(let k in user){
		// 			USER_DATA[k] = user[k];
		// 		}
		// 		// this.setState({ user });
		// 	}
		// })
		// .catch(e => console.log('e: ', e))
		// .then(() => Q.setClass(Q.domQ('#QloadStartUp'),'d-none'));

		// const { history } = this.props;
		// console.log('componentDidMount in Admin window.history: ', window.history);

		setTimeout(() => { // DUMMY Xhr
			this.setState({ loadMenus: true, menus: MENUS });
		}, 1500);

		// DEVS OPTION: Prevent Zoom when ctrl + wheel
		// let ROOT = Q.domQ("#root");
		// document.addEventListener("wheel", e => {
		// 	// console.log('onWheel e: ', e.ctrlKey);
		// 	if(e.ctrlKey){
		// 		Q.preventQ(e);
		// 		return;
		// 	}
		// }, {
		// 	passive: false,
		// 	capture: true
		// });
		
		// https://dev.jspm.io/react-router-dom | https://unpkg.com/react-router-dom?module
		// import(/*webpackIgnore:true*/'https://dev.jspm.io/reactstrap').then(v => {
			// console.log(v);
		// }).catch(e => console.error(e));

    // window.onbeforeunload = function(e){
    //  let sb = navigator.sendBeacon(
    //    'https://reqres.in/api/users',
    //    JSON.stringify({
		// 		name: 'Husein',
		// 		job: 'Programmer'
		// 	})
    //  );
    //  console.log('sb: ', sb);
     // empty return is required to not show popup
    //  return;
		// }
		
		// console.log('%cProgrammeria Admin','color:#666;font-family:sans-serif;letter-spacing:2px;font-size:26px;font-weight:700;text-shadow:1px 1px 0 #A0E7FE,2px 2px 1px rgba(0,0,0,.3)');
		// console.log('%cDevelopment by: https://programmeria.com','font-size:15px');
		Q.setClass(QloadStartUp,'d-none');// Hide Loader Splash / start up
	}

	// componentDidUpdate(prevProps){
		// const { location } = this.props;
		// // console.log('componentDidUpdate in Admin prevProps: ',prevProps);
		// // console.log('componentDidUpdate in Admin location: ',location);

		// if(location.pathname !== prevProps.location.pathname){ //   && !location.state | && !location.pathname.includes('/detail/')
    //   window.scrollTo(0, 0);
    //   // DEV to set previous url FOR Not Found page
    //   // this.setState({backUrl: prevProps.location.pathname});
		// }
		
		// 	// FOR close dropdown-menu with dropdown-item react-router NavLink / Link component
		// 	let btn = document.activeElement;
		// 	if(Q.getAttr(btn, 'aria-expanded') === 'true'){ // hasClass(btn, "ddRoute") ||
		// 		btn.click();
		// 		btn.blur();
		// 	}
	// }
	
// DEVS NOT FIX:
	onConnection(){ // e
		const ico = Q.domQ('link[rel="shortcut icon"]');
		const href = Q.getAttr(ico, "href");
		if(navigator.onLine){
			if(href !== FAV) ico.href = FAV;
		}else{
			if(href === FAV) ico.href = Q_appData.faviconOffline || "/favicon-offline.png";
		}
	}
	
// DEV OPTION: Prevent ctrl + S, ctrl + P
  // onCtrlSaveNPrint = e => {
	// 	console.log('key: ',e.key);
	// 	console.log('keyCode: ',e.keyCode);
  //   e.stopPropagation();// OPTION

	// 	let kc = e.keyCode, 
	// 			ctrl = e.ctrlKey;

  //   // 80 = P
  //   if(((ctrl && kc === 80) && !Q.hasClass(document.documentElement, 'print-on')) || ctrl && kc === 83){
  //     e.preventDefault();
  //   }
  //   // 83 = S,
  //   // if(ctrl && kc === 83){
  //     // e.preventDefault();
  //   // }
  // }

  onToggleAside(){
    if(this.state.asideMin){
      this.setState({ asideMin: false });
      document.body.classList.toggle('aside-min');
    }
  }

  onLogOut(token){
		Q.setClass(QloadStartUp,'bg-rgba-light-5');
		Q.setClass(QloadStartUp,'d-none','remove');

		axios.post("/logout", null, {
			headers: {
				Authorization: "Bearer " + token
			}
		}).then(r => {
			console.log('/logout r: ', r);
			if(r && r.data){
				localStorage.removeItem('t');
				bcAuth.postMessage({ 
					action: "LOGOUT", 
					// location: window.location.pathname
				});
				window.location.replace("/login");
			}else{
				Q.setClass(QloadStartUp,'d-none');
				Q.setClass(QloadStartUp,'bg-rgba-light-5','remove');
			}
		})
		.catch(e => {
			if(e.response.status === 401){
				localStorage.removeItem("t");
				window.location.replace("/login");
			}
		});
  }

	render(){
		const { config } = this.context;// setAppConfig
		// const { location } = this.props;
		const { loadMenus, menus, asideMin } = this.state;// err, user, 

		// if(!user){
		// 	return (
		// 		<Flex dir="column" className="w-100 vh-100">{" "}</Flex>
		// 	);
		// }

		if(!config.token) return null;

		// <PageVisibilityAuth>
		return (
			<Mq>
				{(isMobile) => (
					<Flex dir="row" className="layout">
						<AsideAdmin 
							load={loadMenus} 
							isMobile={isMobile} // SM_DEVICE 
							basename={BASENAME} 
							headText={APP_NAME} // "Programmeria" 
							asideMin={asideMin} 
							menus={menus} // MENUS
							onToggleAside={this.onToggleAside} 
						/>
						
						<Flex dir="column" className="min-vh-100 wrap-app" id="wrapApp">{/* flex-auto pancake-stack */}
							<NavAdmin 
								isMobile={isMobile} // SM_DEVICE 
								asideMin={asideMin} 
								onToggleAside={() => this.setState({asideMin: !asideMin})} 
								onLogOut={() => this.onLogOut(config.token)} 
							/>

							<main>
								<Suspense 
									fallback={<PageLoader bottom left={isMobile} w={isMobile ? null : "calc(100% - 220px)"} h={Q.PAGE_FULL_NAV} />}
								>
									<Switch>
										<RouteLazy exact strict path="/" component={HomeAdmin} />
										<RouteLazy path="/settings/app" component={Settings} />
										{/* <RouteLazy path="/settings-pages" component={SettingsPages} /> */}
										<RouteLazy path="/settings/menus" component={MenusSettings} />

										{SET_PAGES.map((v, i) => 
											<RouteLazy key={i} path={v.to} component={v.c} />
										)}

										<RouteLazy path="/user/all" component={Users} />
										<RouteLazy path="/user/visitor" component={Visitor} />
										
										{/* {USER_DATA.isAdmin && 
											[
												{ p:"/user/add", c: UserAdd }, 
												{ p:"/user/edit/:id", c: UserEdit }, 
												{ p:"/user/levels", c: Levels }, 
												{ p:"/user/create-level", c: AddLevel }
											].map(v => 
												<RouteLazy key={v.p} path={v.p} component={v.c}  />
											)
										} */}
										
										<RouteLazy path="/user/detail/:id" component={UserDetail} />
										<RouteLazy path="/user/change-password" component={ChangePassword} />

										{/* <RouteLazy path="/product/all" component={Products} />
										<RouteLazy path="/product/add" component={ProductAdd} />
										<RouteLazy path="/product/edit/:id" component={ProductEdit} /> */}
										<RouteLazy path="/contact-us" component={ContactUs} />
										<RouteLazy path="/files" component={Files} />
										<RouteLazy path="/modules/pos" component={PosPage} />
										{/* <RouteLazy path="/merchants" component={Merchants} /> */}

										{
											[
												/* COMPONENTS: */
												{ p: "/components/chartjs", c: ChartJSPage }, 
												{ p: "/components/highcharts", c: HighchartsPage }, 
												{ p: "/primereact", c: PrimeReactDocs }, 
												/* TOOLS: */
												{ p: "/tools/manifest-generator", c: ManifestGeneratorPage }, 
												{ p: "/tools/email-editor", c: EmailEditorPage }, 
												{ p: "/tools/base64-encoder", c: Base64EncoderPage}, 
												{ p: "/tools/css-unit-converter", c: CssUnitConverterPage }, 
												{ p: "/tools/theme-designer", c: ThemeDesignerPage }, 
												/* DEVS: */
												{ p: "/devs/tests", c: DevsPage }, 
												{ p: "/devs/scrollto", c: ScrollToPage }, 
												{ p: "/devs/app-designer", c: AppDesignerPage }, 
												{ p: "/devs/image-map-designer", c: ImageMapDesignerPage }, 
												/* ETC: */
												{ p: "/documentation", c: DocumentationPage }, 
											].map(v => 
												<RouteLazy key={v.p} path={v.p} component={v.c} />
											)
										}
										
										{/** ERROR icomoon:  */}
										<RouteLazy path="/tools/icon-maker" component={IconMakerPage} />

										{/* {TOOLS.map((v, i) => 
											<Route key={i} path={v.to}>
												<UpgradeApp label={v.label} />
											</Route>
										)} */}

										<Route path="*" component={NotFound} />
									</Switch>

									{/* Only User is admin */}
									{/* {USER_DATA.isAdmin && */}
										<DocsDev // DocsDevelopment 
											asideMin={asideMin} 
											
										/>
									{/* } */}
								</Suspense>
							</main>
						</Flex>

						<ScrollTo 
							target="window" 
							threshold={70} // 150
							btnProps={{ 
								// blur: true, 
								// As: "div", 
								size: "sm", 
								"aria-label": "Back To top", 
								className: "position-fixed b8 r8 zi-1001 tip tipL qi qi-arrow-up" // up | down
							}}
						/>
					</Flex>
				)}
			</Mq>
		);
	}
} // </PageVisibilityAuth>

const Admin = () => (
	<BrowserRouter basename={BASENAME}>
    <LastLocationProvider>
			<AppConfigProvider>
      	<Route component={Root} />
			</AppConfigProvider>
    </LastLocationProvider>
	</BrowserRouter>
);

{/* <BrowserRouter basename={BASENAME}>
<AuthProvider>
  <FetchProvider>
    <LastLocationProvider>
      <Route component={Root} />
    </LastLocationProvider>
  </FetchProvider>
</AuthProvider>
</BrowserRouter> */}

// let ROOT = document.getElementById('root');
// Q.setAttr(ROOT,"hidden");
// ROOT.removeAttribute("hidden");
ReactDOM.render(<Admin />, Q.domQ('#root'));